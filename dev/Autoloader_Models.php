<?php
/**
 * Example 2: Using a string as the single parameter for `spl_autoload_register`
 *
 * We need to define a function with the same name as the string we provide
 *
 * @see	http://php.net/manual/en/language.types.callable.php
 * Additional info: https://supunkavinda.blog/php/autoload-classes-namespaces
 */
spl_autoload_register( 'Model_Class_Autoload' );

/**
 * This function has the same name as the string we passed in above, and acts 
 * exactly the same as the anonymous function from Example 1.
 */
function Model_Class_Autoload( $class_name ) {
	/**
	 * Note that actual usage may require some string operations to specify the filename
	 */
    global $REGISTER ; 
	$file_name = $class_name . '.php';
    $file_name = str_replace("\\", DIRECTORY_SEPARATOR, $file_name); // Adapt for namespace slashes
	if( file_exists( MODELS_PATH.'/'.$file_name ) ) {
		require MODELS_PATH.'/'.$file_name;
	}
}
/**
 * ... anywhere in our code, we can now get a new My_Class object without caring whether
 * the class's file has been required or not
 * 
 * Note in this case we need to have a file called `My_Class.php` in the same directory as this script.
 */


// this is the same as use My\Full\NSname as NSname
// use My\Full\NSname;
// Instead of new My\Full\NSname() you can instead write:
// new NSname()


function Package_Class_Autoload( $class_name ) {
	/**
	 * Note that actual usage may require some string operations to specify the filename
	 */
    
    $class_array = explode('\\',$class_name) ;

    global $REGISTER ; 
	$file_name = end($class_array) . '.php';
    if( file_exists( MODELS_PATH.'/'.$file_name ) ) {
		require MODELS_PATH.'/'.$file_name;
	}
}