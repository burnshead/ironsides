<?php 
    // Define session variables
    require_once CONFIG_PATH.'/Session.php';

    ini_set('default_charset', 'UTF-8') ;

    $TIMESTAMP = time() ;
    $MICRO_TIMESTAMP = microtime(true) ;  // Initialize second counter

    define('GLOBAL_NO_NETWORK_CONNECTION', 0); // Set to 1 if you're off internet and need to run the app  

    define('TIMESTAMP', $TIMESTAMP);   
    define('MICRO_TIMESTAMP', $MICRO_TIMESTAMP);   
    define('SYSTEM_TIMEZONE', 'America/Chicago');   
    define('APP_VERSION', $version);
    $server_config['app_version'] = $version ; 
    // Client Side Unique Class IDs
    $ucid_i = 1 ; // Identification number that can be incremented within a single PHP instances
    $ucid = MICRO_TIMESTAMP.'_' ;   // Concatentated Unique Class ID number that forces uniqueness between PHP instances leveraging the microtimestamp
    // Within view, concatenate these two variables, and then increment $ucid_i to create truly unique IDs
    // EXAMPLE: $ucid.$ucid_i 
    // Should be added as a class as: 'uniqueclass_'.$ucid

    

    // Auto-versioning CSS and JS files, sourced from:
    // https://stackoverflow.com/questions/118884/how-to-force-the-browser-to-reload-cached-css-js-files
    function File_Auto_Version($file) {
        if(strpos($file, '/') !== 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file))
        return $file;

        $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] . $file);
        return preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
        }




        
 

    // TEST VERSION ID
    // Make sure the version is valid. If not, dump out...
    $system_version_test = new System_Config();
    $version_test = $system_version_test->Test_Version($_SESSION['version']) ; 

    if ($version_test['result_count'] == 1) {
        
        // AUTOLOADERS
        require_once HOMEPATH.'/.php/composer/vendor/autoload.php'; // Autoload composer classes
        require_once CONFIG_PATH.'/Autoloader_Models.php'; // Models


        // Autoload public javascript files
        $app_js_file_array = scandir(SYSTEM_JS_MANUAL_PATH);
        
        $data['app_js_files'] = array(); 
        foreach ($app_js_file_array as $file) {
            $split_file = explode(".",$file) ; 
            if ($split_file[1] == 'js') {
                $data['app_js_files'][] = array(
                    'full_path' => $file,
                    'filename' => str_replace(SYSTEM_JS_MANUAL_PATH,"",$file)
                    );
                }
            } 


        // Set system domains
        
        // Get the current domain
        $system = new System() ; 

        $query_options = array(
            'hostname' => $public_home_host
            ) ;

        $system->Set_System_List('list_domains',$query_options) ; 
        $server_config['domain_query'] = $system->system_query_result ; 
        $server_config['domain']['active_domain'] = $system->Get_System_List('list_domains')[0] ; 


        // Get default app domain
        $query_options = array(
            'app_domain' => 1,
            'default_domain' => 1
            ) ;

        switch (URL_MODE) {
            case 'system_live':
                $query_options['url_live'] = 1 ; 
                break ;
            case 'system_test':
                $query_options['url_live'] = 0 ;  
                break ;
            }

        $system->Set_System_List('list_domains',$query_options) ; 

        $server_config['domain']['app_domain'] = $system->Get_System_List('list_domains')[0] ; // Default app domain (depenedent on live or test)

        
        // Get default account public domain
        $query_options = array(
            'account_public_domain' => 1,
            'default_domain' => 1
            ) ;

        switch (URL_MODE) {
            case 'system_live':
                $query_options['url_live'] = 1 ; 
                break ;
            case 'system_test':
                $query_options['url_live'] = 0 ;  
                break ;
            }

        $system->Set_System_List('list_domains',$query_options) ; 

        $server_config['domain']['account_public_domain'] = $system->Get_System_List('list_domains')[0] ; // Default account public domain (depenedent on live or test)
            
        $system->Set_System_List('list_defaults') ; 
        $server_config['system_defaults'] = $system->Get_System_List('list_defaults') ;         
        
        
        // Administrator Account Profiles (Profile ID Numbers)
        define('APP_ACCOUNT_ADMIN', Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'app_account_admin')['value']);
        define('APP_ACCOUNT_SUPPORT', Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'app_account_support')['value']);            
        
        
        
        // API KEYS
        require_once CONFIG_PATH.'/API_Keys.php'; // 

        
        // Parse the URL...
        $server_config['page_url_array'] = System_Config::Config_Parse_URL($_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]) ;
        
        
        
        // Get Cloudinary URL constants        
        $cloudinary_images = new Asset() ; 
        $cloudinary_images->Set_System_Vendor(5) ; 
        define('CLOUDINARY_SECURE_DELIVERY_URL', $cloudinary_images->vendor_keys->Cloudinary_Secure_Delivery_URL); 
        define('CLOUDINARY_DELIVERY_FOLDER', $cloudinary_images->vendor_keys->Cloudinary_Delivery_Folder); 

        $system = new System() ; 
        $server_config['timestamp_system'] = TIMESTAMP ; 
        $server_config = $system->Action_Time_Territorialize_Dataset($server_config) ;
        $data['timestamp_system_compiled'] = $server_config['timestamp_system_compiled'] ; 
        
        // Log the URL & IP address request pair
        
        
//        echo '<pre>' ; print_r($server_config) ; echo '</pre>' ;
        $data['server_config'] = $server_config ; 
        // LOAD CONTROLLERS        
        require_once CONTROLLERS_PATH.'/controller_master.php'; 
        
        
        
        } else {
            // If $version_test['result_count'] != 1 ... 
        
            $api_result = array(
                'http_response_code' => http_response_code(), // PHP 5.4 or greater
                'message' => 'Invalid request. System version could not be verified.',
                'environment' => ENVIRONMENT,
                'version' => $_SESSION['version'] 
                );
        
            echo json_encode($api_result);
        
            }
 


