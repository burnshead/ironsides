<?php

namespace Ragnar\Ironsides ;

class Account extends User {
 
    public $account_id;
    public $account;
    public $account_list;
    
    public $account_billing_id ; // Stripe needs this. Use Set_Account to set it.
    public $account_subscriptions_list ; 
    public $account_marketing_credits ; 
    
    public $account_paging ; 
    public $page_increment = 12 ; // # of account items to be pulled per page (e.g. Users)
    
    public $account_user_list ; 
//    public $full_account_user_list ;
    
    public $account_user_full_list ; 
    public $account_user_uber_list ; 
    
    public $account_user_profile_full_list ; 
    public $account_user_profile_uber_list ; 
    
    public $product_feature ; 
    
    public $product_feature_set_list ;    
    public $product_feature_list ;
    
    public $product_feature_type_set = array(
        array(
            'title' => 'n/a',
            'name' => ''
            ),
        array(
            'title' => 'Toggle',
            'name' => 'toggle'
            ),
        array(
            'title' => 'Limit',
            'name' => 'limit'
            )       
        ) ;
    
    public $profile_id;
    public $profile;
    public $profile_list ; 
    public $messaging_numbers_list ; 
    
    public $stripe_id;
    public $stripe_customer;
    
    public $account_query_result ;
    
    
    public function __construct($user_id = 'ignore') {

        global $DB ;  
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            } 

        }
    

    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    public function Set_Active_Account($new_account_id) {
        
        $this->Update_Active_Account($new_account_id) ; // Updates in the database
        $this->account_id = $new_account_id ;
        $this->active_account_id = $new_account_id ;
        $this->Set_Account_By_ID() ; 
        
        $version = System_Config::Create_Version_Path($this->account['version']) ;
        $_SESSION['version'] = $version['version']['version'] ; 
        
        return $this;
        }    
    
    
    public function Set_Account_ID($account_id) {
        $this->account_id = $account_id ; 
        return $this;
        }
    
    public function Set_Account_By_ID($account_id = 'internal',$additional_parameters = array()) {
        
        switch ($additional_parameters['set_by']) {
            case 'account_username': 

                $this->Set_Account_By_Username($account_id) ; 
                
            case 'billing_id':

                $query_array = array(
                    'table' => 'accounts',
                    'fields' => "*",
                    'where' => "billing_id='$account_id'"
                    );

                $result = $this->DB->Query('SELECT',$query_array);

                if ($result['result_count'] == 1) {
                    $this->account_id = $result['results']['account_id'];
                    $this->Set_Account($this->account_id) ;
                    } else {
                        $this->account_id = 'error' ; 
                        $this->account = 'error' ; 

                        $this->Set_Alert_Response(227) ; //
                        } 
                
                break ;                
            case 'id':
            default:
                
                if ('internal' === $account_id) {
                    $account_id = $this->account_id ; 
                    } else {
                        $this->account_id = $account_id ; 
                        }

                if ($account_id != 0) {
                    $this->Set_Account($this->account_id) ; 
                    } else {
                        $this->account = 'error' ; 
                        }
            }
        
        return $this;
        }
    
    public function Set_Account_By_Username($account_username) {

        $query_array = array(
            'table' => 'account_usernames',
            'fields' => "*",
            'where' => "account_username='$account_username'"
            );
        
        $result = $this->DB->Query('SELECT',$query_array);
        
        if ($result['result_count'] == 1) {
            $this->account_id = $result['results']['account_id'];
            $this->Set_Account($this->account_id) ;
            } else {
                $this->account_id = 'error' ; 
                $this->account = 'error' ; 
                
                $this->alert_id = 21 ; 
                $alert = Utilities::System_Alert($this->alert_id) ; 
                $this->response = $alert ; 
                }
 
        
        return $this;        
        }
    
    public function Set_Account_Billing_ID($account_billing_id) {
        $this->account_billing_id = $account_billing_id ; 
        return $this;
        }
    
    
    
    // Sets account and TESTS for user authorization on the account
    public function Set_Account($account_id = 'internal',$show_user = 'no') {

        global $server_config ; 
        
        if ('internal' === $account_id) {
            $account_id = $this->account_id ; 
            } else {
                $this->account_id = $account_id ; 
                }
        
        
        // Show user 'yes' defines whether or not to pull the associated user profile w/ the account
        $account_record = $this->Retrieve_Account($account_id,$show_user) ; 

        // Convert json encoded string into array of additional account data                
        $account_data_01 = json_decode($account_record['results']['structured_data_account_01'],true) ; 
        if (is_array($account_data_01)) {
            foreach ($account_data_01 as $key => $value) {
                $account_record['results'][$key] = $value ;
                }
            }        
        $account_data_02 = json_decode($account_record['results']['structured_data_account_02'],true) ; 
        if (is_array($account_data_02)) {
            foreach ($account_data_02 as $key => $value) {
                $account_record['results'][$key] = $value ;
                }
            }
        
        // Retrieve and set account public domain
        // Custom setting if available or use the default domain (as set by the database)
        if (isset($account_record['results']['account_public_domain_id'])) {
            $public_domain_query_options = array(
                'domain_id' => $account_record['results']['account_public_domain_id']
                ) ;            
            } else {
                $public_domain_query_options = array(
                    'account_public_domain' => 1,
                    'default_domain' => 1
                    ) ;            
                switch (URL_MODE) {
                    case 'system_test':
                        $public_domain_query_options['url_live'] = 0 ; 
                        break ;
                    case 'system_live':
                        $public_domain_query_options['url_live'] = 1 ; 
                        break ;
                    }
                }
        
        $this->Set_System_List('list_domains',$public_domain_query_options) ; 
        $account_record['results']['account_public_domain'] = $this->Get_System_List('list_domains')[0] ; 
        
        
        if ($account_record['results']['billing_id']) {
            
            switch (BILLING_MODE) {
                case 'system_test':
                    $account_record['results']['billing_id_link'] = 'https://dashboard.stripe.com/test/customers/'.$account_record['results']['billing_id'] ; 
                    break ;
                case 'system_live':
                    $account_record['results']['billing_id_link'] = 'https://dashboard.stripe.com/customers/'.$account_record['results']['billing_id'] ; 
                    break ;
                }            
                        
            }
        
        
        
        // Retrieve and set email domain
        if (isset($account_record['results']['email_domain_id'])) {
            $email_domain_query_options['filter_by_domain_id'] = $account_record['results']['email_domain_id'] ; 
            } else {
                $email_domain_query_options['filter_by_account_default_domain'] = 'yes' ; // This implies the email domain set up for emails from accounts (not the domain set as the global marketing site default domain)             
                }
        
        $email = new Email() ;
        $email_domain = $email->Retrieve_Domain_List($email_domain_query_options) ;
        $account_record['results']['email_domain'] = $email_domain['results'][0] ; 
        
        
        // Set account customizations...
        if (isset($account_record['results']['customization_marketing_site_background_image']) AND ($account_record['results']['customization_marketing_site_background_image'] > 0)) {
            $asset = new Asset() ;
            $asset->Set_Asset_By_ID($account_record['results']['customization_marketing_site_background_image']) ; 
            $account_record['results']['marketing_site_background_image'] = $asset->Get_Asset() ; 
            } 
        
        if (isset($account_record['results']['customization_marketing_site_custom_bundles_background_image']) AND ($account_record['results']['customization_marketing_site_custom_bundles_background_image'] > 0)) {
            $asset = new Asset() ;
            $asset->Set_Asset_By_ID($account_record['results']['customization_marketing_site_custom_bundles_background_image']) ; 
            $account_record['results']['marketing_site_custom_bundles_background_image'] = $asset->Get_Asset() ; 
            }
        
//        else {
//                                
//                $archived_item_metadata_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'archived_item')['value'] ; 
//                
//                }
        
        
        // Territorialize timestamps...
        $account_record['results'] = $this->Action_Time_Territorialize_Dataset($account_record['results']) ;
        
        // Retrieve and map Feature Set & Operations Allowed
        if ($account_record['results']['product_id']) {
            $feature_result = $this->Action_Map_Feature_Set($account_record['results']['product_id']) ; 
            $account_record['results']['feature_set'] = $feature_result['feature_set'] ;
            $account_record['results']['operations_allowed'] = $feature_result['operations_allowed'] ;
            }
        
        $account_owner = $this->Retrieve_Account_Users($query_options = array(
            'account_auth_role_name_array' => array('account_owner'),
            'override_paging' => 'yes'
            )) ;

        
        if ($account_owner != 'error') {

            $account_owner_record = $account_owner['results'][0]; 
            
            $owner_additional_parameters['skip_set_account'] = 'yes' ; 
            $profile = new Account()  ;
            $profile->Set_Profile_By_ID($account_owner_record['profile_id'],$owner_additional_parameters) ; 
            $data['account_owner_record'] = $profile->Get_Profile() ; 
            
            
            
            // Territorialize timestamps and phone numbers...
//            $account_owner_record['timestamp_current'] = time() ; 
//            $account_owner_record = $this->Action_Time_Territorialize_Dataset($account_owner_record) ;            
//            $account_owner_record = $this->Action_Phone_Territorialize_Dataset($account_owner_record) ;            
            $account_record['results']['account_owner'] = $data['account_owner_record'] ; 
            }
        
        
        $this->account = $account_record['results'];
        $this->account_billing_id = $account_record['results']['billing_id'] ;
        
        
        // If no timezone set for the parent user, then set the timezone as the account owner's timezone
        if (!$this->user['timezone']) {
            $this->user['timezone'] = $this->account['account_owner']['timezone'] ;
            }        
        
        
        $this->Action_Set_Auth_Permissions() ;  // Reset permissions based on allowances of account.        
        return $this;
        }
    
    
    public function Set_Account_List($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        if (!isset($query_options['filter_by_user_auth'])) {
            $query_options['filter_by_user_auth'] = 'yes' ; // 'yes' uses the internally defined properties of user_id to pull matching account list
            }

        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }        
        
        $result = $this->Retrieve_Account_List($query_options) ; 
        
        if ($result['result_count'] == 0) {
            $this->account_list = 'error' ; 
            } else {        
        
            // Mark the currently active account
            // If no active account exists for the user, then set the active account in the database.
            if ($query_options['filter_by_user_auth'] == 'yes') {
                $i=0;
                foreach ($result['results'] as $this_result) {

                    if (($this->active_account_id == 0) AND ($i == 0)) {
                        $this->Set_Active_Account($this_result['account_id']) ; 
                        }

                    if ($this_result['account_id'] == $this->active_account_id) {
                        $result['results'][$i]['active_account'] = 1 ; 
                        } else {
                            $result['results'][$i]['active_account'] = 0 ; 
                            }
                    $i++;
                    }
                }

            $a = 0 ; 
            foreach ($result['results'] as $account_record) {
                $result['results'][$a] = $this->Action_Time_Territorialize_Dataset($account_record) ;
                $a++ ; 
                }


            $this->account_list = $result['results'];
            
            }
        
        return $this ;           
        }

    
    
    public function Set_Profile_By_Account_Owner() {

        $data['account_record'] = $this->Get_Account() ; 
        
        if ($data['account_record']['account_owner']['profile_id']) {
            $this->Set_User_By_Profile_ID($data['account_record']['account_owner']['profile_id']) ; 
            }
        
        return $this;
        }
    
    
    public function Set_Profile_ID($profile_id) {

        $this->profile_id = $profile_id ;
        return $this;
        } 
    
    
    public function Set_Profile_By_ID($profile_id = 'internal',$additional_parameters = array()) {

        if ('internal' === $profile_id) {
            $profile_id = $this->profile_id ; 
            } else {            
                $this->profile_id = $profile_id ;
                }
        
        $this->Set_Profile($this->profile_id,$additional_parameters) ; 
        return $this;
        }  
    
    
    public function Set_Profile($profile_id = 'internal',$additional_parameters = array()) {

        global $server_config ; // To create the profile referral link
        
        if ('internal' === $profile_id) {
            $profile_id = $this->profile_id ; 
            } else {            
                $this->profile_id = $profile_id ;
                }
        
        $result = $this->Retrieve_Profile($profile_id) ; 

        
        if ($result['result_count'] == 1) {
            
            if ($additional_parameters['skip_profile_image'] != 'yes') {
                if ($result['results']['profile_image_id'] > 0) {
                    
                    $asset_parameters['skip_history'] = 'yes' ;
                    $asset_parameters['skip_profile_image'] = 'yes' ;
                    $asset_parameters['condense_asset'] = 'yes' ;
                    $asset = new Asset() ; 
                    $asset->Set_Asset($result['results']['profile_image_id'],$asset_parameters) ; 
                    $result['results']['profile_image'] = $asset->Get_Asset() ; 
                    }        
                }


            $metadata_query = array(
                'relationship_type' => 'profile',
                'metadata_type' => 'tag,category',
                'allow_global_metadata' => 'yes',
                'visibility_name' => 'visible'
                ) ;  
                        
            $result['results']['tag'] = array() ;
            $result['results']['category'] = array() ;
            
            $metadata_results = $this->Retrieve_Metadata_Relationship_List($profile_id,$metadata_query) ; 
            if ($metadata_results['result_count'] > 0) {
                
                foreach ($metadata_results['results'] as $metadata_result) {
                    switch ($metadata_result['metadata_type']){
                        case 'tag':
                            $result['results']['tag'][] = $metadata_result ; 
                            break ;
                        case 'category':
                            $result['results']['category'][] = $metadata_result ; 
                            break ;                            
                        }
                    }
                }            
            
            
            
            // Convert json encoded string into array of additional account data                
            $profile_data_01 = json_decode($result['results']['structured_data_profile_01'],true) ; 
            if (is_array($profile_data_01)) {
                foreach ($profile_data_01 as $key => $value) {
                    $result['results'][$key] = $value ;
                    }
                }
            
            // Convert json encoded string into array of additional account data                
            $account_data_01 = json_decode($result['results']['structured_data_account_01'],true) ; 
            if (is_array($account_data_01)) {
                foreach ($account_data_01 as $key => $value) {
                    $result['results'][$key] = $value ;
                    }
                }            
            
            // Territorialize timestamps and phone numbers...
            $result['results']['timestamp_current'] = time() ; 
            $result['results'] = $this->Action_Time_Territorialize_Dataset($result['results']) ;            
            $result['results'] = $this->Action_Phone_Territorialize_Dataset($result['results']) ;        


            // Retrieve messaging numbers assigned to profile
            $messaging_numbers_query_options = array(
                'filter_by_profile_id' => 'yes',
                'profile_messaging_number_id' => $result['results']['messaging_number_id'] 
                ) ; 


            $this->Set_Messaging_Numbers_List($messaging_numbers_query_options) ; 
            $result['results']['messaging_numbers_list'] = $this->Get_Messaging_Number_List() ;      


            
            // Tally up numbers that are complete, received, failed            
            $result['results']['messaging_number_order_count']['failed'] = 0  ;
            $result['results']['messaging_number_order_count']['complete'] = 0  ;
            $result['results']['messaging_number_order_count']['received'] = 0  ;
            $result['results']['messaging_number_order_count']['total'] = 0  ;
                            
            foreach ($result['results']['messaging_numbers_list'] as $messaging_number) {
                switch ($messaging_number['number_order_status']) {
                    case 'FAILED':
                    case 'PARTIAL': // 
                    case 'BACKORDERED': //
                        $result['results']['messaging_number_order_count']['failed']++ ; 
                        break ; 
                    case 'COMPLETE':
                        $result['results']['messaging_number_order_count']['complete']++ ; 
                        break ; 
                    case 'RECEIVED':
                        $result['results']['messaging_number_order_count']['received']++ ; 
                        break ;  
                    }
                $result['results']['messaging_number_order_count']['total']++ ; 
                }            
            
            

            
            $this->Set_User_Email_List('verified') ; 
            $result['results']['user_email_list'] = $this->Get_User_Email_List() ;
            
            // Create referral link...
            $result['results']['referral_link'] = 'https://'.$server_config['domain']['app_domain']['hostname'].'/?refer='.$result['results']['profile_username'] ; 
            if ($result['results']['affiliate_program_email_id']) {
                $affiliate_program_email_addess = Utilities::Deep_Array($result['results']['user_email_list'],'email_address_id',$result['results']['affiliate_program_email_id']) ; 
                $result['results']['affiliate_program_email_address'] = $affiliate_program_email_addess['email_address'] ; 
                }
            
            

            if (isset($result['results']['account_id'])) {
                $this->account_id = $result['results']['account_id'] ; 
                }            
            
            $this->profile = $result['results'] ;
            
            
            if ($additional_parameters['skip_set_account'] != 'yes') {
                
                $this->Set_Account($this->profile['account_id']) ;

                // Turn the permission json string into an array of allowed Operations
                $this->Action_Set_Auth_Permissions() ; // Reset permissions based on allowances of profile + account.
                }
            
            $this->Set_Alert_Response(217) ; // Profile successfully retrieved
            
            } else {
                $this->profile = 'error' ;
                
                }

        return $this;
        }
    

    

    public function Set_User_By_Profile_ID($profile_id = 'internal',$additional_parameters = array()) {

        switch ($additional_parameters['set_by']) {
            case 'referral_id':
            case 'username':                
                $asset = $this->Retrieve_Asset_ID_By_Slug($asset_id) ; 
                if ($asset['result_count'] == 0) {
                    $this->asset_id = 'error' ; 
                    } else {
                        $this->asset_id = $asset['results']['asset_id'] ;
                        }                
                break ;               
            case 'id':
            default:
                if ('internal' === $profile_id) {
                    $profile_id = $this->profile_id ; 
                    } else {            
                        $this->profile_id = $profile_id ;
                        }
            }

         
        $this->Set_Profile($profile_id,$additional_parameters) ; 
        $this->Set_User($this->profile['user_id']) ; 
        $this->Set_Account($this->profile['account_id']) ;

        return $this;
        }    
    
    public function Set_Profile_By_User_Account($user_id = 'internal',$account_id = 'internal',$additional_parameters = array()) {

        if ('internal' === $user_id) {
            $user_id = $this->user_id ; 
            } else {            
                $this->user_id = $user_id ;
                }
        
        if ('internal' === $account_id) {
            $account_id = $this->account_id ; 
            } else {            
                $this->account_id = $account_id ;
                }
        
        $profile_id_record = $this->Retrieve_Profile_ID_By_User_Account($user_id,$account_id) ; 
                
        if (isset($profile_id_record['results']['profile_id'])) {
            $this->Set_Profile_By_ID($profile_id_record['results']['profile_id'],$additional_parameters) ;
            } else {
                $this->profile = 'error' ; 
                }
        
        return $this;
        }
    
    
    public function Set_Replicate_User_Account_Profile($source_object) {
        
        $this->Set_Model_Timings('Account.Set_Replicate_User_Account_Profile_begin') ; 
                
        $this->user_id = $source_object->user['user_id'] ; 
        $this->user = $source_object->user ; 
        
        $this->account_id = $source_object->account['account_id'] ; 
        $this->account = $source_object->account ; 
        
        $this->profile_id = $source_object->profile['profile_id'] ; 
        $this->profile = $source_object->profile ; 
        
        $this->Set_Model_Timings('Account.Set_Replicate_User_Account_Profile_complete') ; 
        
        return $this ; 
        }
    
    
    
    public function Set_Profile_List($user_id = 'internal') {

        if ('internal' === $user_id) {
            $user_id = $this->user_id ; 
            } else {            
                $this->user_id = $user_id ;
                }

        
        $profile_list = $this->Retrieve_Profile_List($user_id) ; 
        
        
        // Add profile image to the array
        if (count($profile_list) > 0) {
            $i = 0 ; 
            foreach ($profile_list['results'] as $profile) {
        
                if ($profile['profile_image_id'] > 0) {            
                    $asset = new Asset() ; 
                    $asset->Set_Asset($profile['profile_image_id']) ; 
                    $profile_list['results'][$i]['profile_image'] = $asset->Get_Asset() ; 
                    } 
                
                
                
                $metadata_query = array(
                    'relationship_type' => 'profile',
                    'metadata_type' => 'tag,category',
                    'allow_global_metadata' => 'yes',
                    'visibility_name' => 'visible'
                    ) ; 

                $profile_list['results'][$i]['tag'] = array() ;
                $profile_list['results'][$i]['category'] = array() ;

                $metadata_results = $this->Retrieve_Metadata_Relationship_List($profile['profile_id'],$metadata_query) ; 
                $profile_list['results'][$i]['metadata_query'] = $metadata_results ; 
                if ($metadata_results['result_count'] > 0) {

                    foreach ($metadata_results['results'] as $metadata_result) {
                        switch ($metadata_result['metadata_type']){
                            case 'tag':
                                $profile_list['results'][$i]['tag'][] = $metadata_result ; 
                                break ;
                            case 'category':
                                $profile_list['results'][$i]['category'][] = $metadata_result ; 
                                break ;                            
                            }
                        }
                    }                
                
                
                
                $i++;
                }
            }
        
        $this->profile_list = $profile_list['results'] ;
        return $this;
        }
    
    
    
    
    // Sets a list of messaging numbers based on on parameters supplied in query array
    // filter_by_profile_id = yes will refer to $this->profile_id set in class
    public function Set_Messaging_Numbers_List($messaging_numbers_query_options = array()) {
        
        $messaging_number_result = $this->Retrieve_Messaging_Numbers_List($messaging_numbers_query_options) ; 
        
        $messaging_numbers_compiled = array() ; 
        
        if (count($messaging_number_result['result_count']) > 0) {
            
            foreach ($messaging_number_result['results'] as $messaging_number) {
                $messaging_number = $this->Action_Phone_Territorialize_Dataset($messaging_number) ;
                
                if ($messaging_number['messaging_number_id'] == $messaging_numbers_query_options['profile_messaging_number_id']) {
                    $messaging_number['primary_number'] = 1 ; 
                    } else {
                        $messaging_number['primary_number'] = 0 ; 
                        }
                $messaging_numbers_compiled[] = $messaging_number ;
                }
            } 
        
        $this->messaging_numbers_list = $messaging_numbers_compiled ; 
        
        return $this ; 
        }
    
    
    
    // Sets account and TESTS for user authorization on the account
    public function Set_Account_User_List($query_options = array(), $account_auth_role_name_array = array()) {

        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        if (!isset($query_options['filter_by_account_id'])) {
            $query_options['filter_by_account_id'] = 'yes' ;
            }        
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        
        if (isset($query_options['account_auth_id'])) {
            $query_options['filter_by_account_auth_id'] = $query_options['account_auth_id'] ;
            } 
        if (isset($query_options['user_auth_id'])) {
            $query_options['filter_by_user_auth_id'] = $query_options['user_auth_id'] ;
            }
        
        $query_options['account_auth_role_name_array'] = $account_auth_role_name_array ; 
            
        $result = $this->Retrieve_Account_Users($query_options) ;         
        
        if ($result['result_count'] == 0) {
            $this->account_user_list = 'error' ; 
            } else { 
            
                $i = 0 ; 
                foreach ($result['results'] as $user) {

                    // Territorialize timestamps and phone numbers...
                    $user = $this->Action_Time_Territorialize_Dataset($user) ;            
                    $user = $this->Action_Phone_Territorialize_Dataset($user) ;   
                    $result['results'][$i] = $user ; 

                    $full_result_set_compiled .= $user['user_id'].',' ; 
                    $profile_full_result_set_compiled .= $user['profile_id'].',' ; 
                    
                    
                    
                    // Add tags
                    $tag_metadata_query_options['metadata_type_id'] = 1 ; // Tags
                    $tag_metadata_query_options['relationship_type'] = 'user' ; 
                    $tag_list = $this->Retrieve_Metadata_Relationship_List($user['user_id'],$tag_metadata_query_options) ; 
                    $result['results'][$i]['tags'] = $tag_list['results'] ; 
                                        
                    $i++ ; 
                    }

                $this->account_user_list = $result['results'] ;            
                }
        
        return $this;
        }    

    
    // Sets account and TESTS for user authorization on the account
    public function Set_Account_Subscriptions_List(
            $query_options = array()) {

        $account_subscriptions_list = $this->Retrieve_Account_Subscriptions_List($query_options) ; 
        $this->account_subscriptions_list = $account_subscriptions_list['results'] ; 
        $this->account_query_result = $account_subscriptions_list ; 
        
        return $this;
        }
    
    
    
    public function Set_Product_Feature_Set_List($query_options = array()) {
    
        if (isset($query_options['filter_by_feature_set_id'])) {
            $query_options['filter_by_feature_set_id'] = Utilities::Process_Comma_Separated_String($query_options['filter_by_feature_set_id']) ;
            }
        
        $result = $this->Retrieve_Product_Feature_Set_List($query_options) ; 
        
        if ($result['results'] != 'error') {
            
            if ($query_options['map_features'] == 'yes') {
                
                $feature_sets = $result['results'] ; 
                
                $feature_query_options = array() ; 
                if (isset($query_options['filter_by_feature_set_id'])) {
                    $feature_query_options['filter_by_feature_set_id'] = $query_options['filter_by_feature_set_id'] ; 
                    }

                $feature_result = $this->Retrieve_Product_Feature_List($feature_query_options) ; 
                
                if ($feature_result['results'] != 'error') {
                    foreach ($feature_result['results'] as $feature) {
                        
                        $feature_set_key = Utilities::Deep_Array_Key($feature_sets,'feature_set_id', $feature['feature_set_id']) ;
                        $feature_sets[$feature_set_key]['features'][] = $feature ; 
                        } 
                    
                    $result['results'] = $feature_sets ;
                    }
                }
                    
            $this->product_feature_set_list = $result['results'] ;     
            }
        
        return $this ; 
        }
    
    public function Set_Product_Feature_By_ID($feature_id) {
        
        $query_options['filter_by_feature_id'] = Utilities::Process_Comma_Separated_String($feature_id) ;
            
        $result = $this->Retrieve_Product_Feature_List($query_options) ; 
        
        if ($result['results'] != 'error') {
            
            $data['product_feature'] = $result['results'][0] ; 
            
            if ($data['product_feature']['marketing_asset_slug']) {     
                
                $additional_parameters['format'] = 'yes' ; 
                $additional_parameters['set_by'] = 'slug' ;     
                
                $asset = new Asset() ; 
                $asset->Set_Asset_By_ID($data['product_feature']['marketing_asset_slug'],$additional_parameters) ; 
                $data['product_feature']['marketing'] = $asset->Get_Asset() ; 
                }
            
            $this->product_feature = $data['product_feature'] ;
            
            }
                
        return $this ;         
        }
    
    
    public function Get_Product_Feature() {
        
        return $this->product_feature ; 
        }
    
    
    public function Set_Product_Feature_List($query_options = array()) {
    
        if (!isset($query_options['group_by'])) {
            $query_options['group_by'] = 'system_features.feature_id' ; 
            }
        if ($query_options['group_by'] == 'ignore') {
            unset($query_options['group_by']) ; 
            }
        
        
        if (isset($query_options['filter_by_feature_set_id'])) {
            $query_options['filter_by_feature_set_id'] = Utilities::Process_Comma_Separated_String($query_options['filter_by_feature_set_id']) ;
            }
        
        if (isset($query_options['filter_by_feature_id'])) {
            $query_options['filter_by_feature_id'] = Utilities::Process_Comma_Separated_String($query_options['filter_by_feature_id']) ;
            }
        

        
        $result = $this->Retrieve_Product_Feature_List($query_options) ; 
        
        if ($result['results'] != 'error') {
            $this->product_feature_list = $result['results'] ;     
            }
        
        return $this ; 
        }
    
    
    public function Set_Page_Increment($page_increment) {
        $this->page_increment = $page_increment ; 
        return $this ; 
        }
        
    
    
    // Process a set of account results and separate into paging components to use for site navigation
    public function Set_Account_Paging($results_array) {

        if (!isset($this->account_paging)) {
            $this->account_paging = $this->Set_Default_Paging_Object() ; 
            }       
        
        $this->account_paging->page_increment = $this->Get_Page_Increment() ;
        
        $this->account_paging = $this->Action_Process_Paging($this->account_paging,$results_array) ; 

        return $this ; 
        }
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_Account_ID() {
        
        return $this->account_id;       
        
        }
    
    public function Get_Account() {
        
        return $this->account;       
        }
    
    public function Get_Account_Billing_ID() {
        
        return $this->account_billing_id;       
        }
    
    public function Get_Account_Marketing_Credits() {
        
        return $this->account_marketing_credits ; 
        }
    
    public function Get_Account_List() {
        
        return $this->account_list;       
        
        }
    
    public function Get_Account_Paging() {
        
        return $this->account_paging ;       
        
        }
    
    
    public function Get_Account_Username() {

        return $this->account['account_username'];               
        }   
    
    public function Get_Profile_ID() {
        
        return $this->profile_id;       
        
        }
    
    public function Get_Profile() {
        
        return $this->profile;       
        
        }    
    
    public function Get_Profile_List() {
        
        return $this->profile_list;       
        
        }
    
    public function Get_Messaging_Number_List() {
        
        return $this->messaging_numbers_list ; 
        }
    
    public function Get_Account_User_List() {
        
        return $this->account_user_list;       
        
        }        

    public function Get_Account_User_Full_List() {
        
        return $this->account_user_full_list;       
        
        }
    
    public function Get_Account_User_Uber_List() {
        
        return $this->account_user_uber_list;       
        
        }
    
    public function Get_Account_User_Profile_Full_List() {
        
        return $this->account_user_profile_full_list;       
        
        }
    
    public function Get_Account_User_Profile_Uber_List() {
        
        return $this->account_user_profile_uber_list;       
        
        }
    
    public function Get_Account_Subscriptions_List() {
        
        return $this->account_subscriptions_list ;       
        
        }     
    
    public function Get_Product_Feature_Set_List() {
    
        return $this->product_feature_set_list ; 
        }
    
    public function Get_Product_Feature_List() {
    
        return $this->product_feature_list ; 
        }
    
    public function Get_Product_Feature_Type_List() {
    
        return $this->product_feature_type_set ; 
        }
    
    public function Get_Account_Query() {
    
        return $this->account_query_result ;               
        } 
    
    public function Get_Page_Increment() {
        return $this->page_increment ; 
        }
    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    
    
    public function Action_Add_Custom_Bundle() {
        
        $account_record = $this->Get_Account() ; 

        if (!isset($account_record['custom_bundles'])) {
            $bundle_input = array() ; 
            } else {
                $bundle_input = $account_record['custom_bundles'] ; 
                }
        
        $bundle_input[] = array(
            'id' => 'bundle_'.time(),
            'title' => 'My new bundle',
            'description' => '',
            'button_text' => 'Click Here',
            'button_url' => ''
            ) ;
        
        $account_input['structured_data_account_02']['custom_bundles'] = $bundle_input ; 
        
        $this->Action_Update_Account($account_input) ; 
        
        return ; 
        }
    
    public function Action_Update_Custom_Bundle($update_input) {
        
        $account_record = $this->Get_Account() ; 
        $bundle_input = $account_record['custom_bundles'] ; 
        
        $bundle_key_id = Utilities::Deep_Array_Key($bundle_input, 'id', $update_input['id']) ; 
        
        if ($update_input['title']) {
            $bundle_input[$bundle_key_id]['title'] = $update_input['title'] ; 
            }
        if ($update_input['description']) {
            $bundle_input[$bundle_key_id]['description'] = $update_input['description'] ; 
            }
        if ($update_input['button_text']) {
            $bundle_input[$bundle_key_id]['button_text'] = $update_input['button_text'] ; 
            }
        if ($update_input['button_url']) {
            $bundle_input[$bundle_key_id]['button_url'] = $update_input['button_url'] ; 
            }
        
        if ($update_input['custom_bundle'] == 'delete') {
            unset($bundle_input[$bundle_key_id]); // remove item at index 0
            $bundle_input = array_values($bundle_input); // 'reindex' array
            }
        
        $account_input['structured_data_account_02']['custom_bundles'] = $bundle_input ; 
        
        $this->Action_Update_Account($account_input) ; 
        
        return ; 
        }
    
    public function Action_Update_Custom_Bundle_Order($update_input) {
        
        $account_record = $this->Get_Account() ; 
        $bundle_original = $account_record['custom_bundles'] ; 
        $bundle_input = array() ; 
        
        foreach ($update_input['ordered_child_assets'] as $new_order) {
            
            $bundle_id = str_replace("sortid_", "", $new_order);
            $this_bundle = Utilities::Deep_Array($bundle_original, 'id', $bundle_id) ; 
            $bundle_input[] = $this_bundle ; 
            }
        

        $account_input['structured_data_account_02']['custom_bundles'] = $bundle_input ; 
        $this->Action_Update_Account($account_input) ; 
        
        return ; 
        } 
    
    public function Action_Create_Admin_Campaign($message_options = array()) {
    
        $continue = 1 ; 
        
        $admin_campaign = new Asset() ; 
        
        // Set the admin profile
        // Name example: app_account_support
        if (isset($message_options['admin_profile_name'])) {
            $admin_profile_id = $admin_campaign->Set_System_Default_Value($message_options['admin_profile_name'])->Get_System_Default_Value() ;    
            $admin_campaign->Set_User_By_Profile_ID($admin_profile_id) ;
            $data['profile_record'] = $admin_campaign->Get_Profile() ;
            $data['user_record'] = $admin_campaign->Get_User() ;
            } else {
                $continue = 0 ; 
                }
        

        // Set the delivery asset
        // Name example: distro_list_all_account_profiles
        if (isset($message_options['delivery_asset_name'])) {
            $delivery_asset_id = $admin_campaign->Set_System_Default_Value($message_options['delivery_asset_name'])->Get_System_Default_Value() ;    
            
            $delivery_asset_options = array(
                'skip_history' => 'yes'
                ) ; 
            $admin_campaign->Set_Asset_By_ID($delivery_asset_id,$delivery_asset_options) ;
            $data['delivery_asset_record'] = $admin_campaign->Get_Asset() ;
            } else {
                $continue = 0 ; 
                }
        
        if (isset($message_options['content_asset_name'])) {
            
            $content_asset = new Asset() ;
            $content_asset_id = $content_asset->Set_System_Default_Value($message_options['content_asset_name'])->Get_System_Default_Value() ;    
            
            $delivery_asset_options = array(
                'skip_history' => 'yes'
                ) ; 
            $content_asset->Set_Asset_By_ID($content_asset_id,$delivery_asset_options) ;
            $data['content_asset_record'] = $content_asset->Get_Asset() ;
            } else {
                $continue = 0 ; 
                }
        
        
        // Set an array of recipient IDs 
        // This takes an incoming array of profiles or users and extracts the appropriate member_id_value into $message_options['member_id_value']
        if (($continue == 1) AND (isset($message_options['member_set']))) {
            
            $message_options['member_id_value'] = array() ; 
            
            if (count($message_options['member_set']) > 0) {
                foreach ($message_options['member_set'] as $member) {
                    
                    switch ($data['delivery_asset_record']['type_name']) {
                        case 'distribution_list_users':
                            $message_options['member_id_value'][] = $member['user_id'] ; 
                            break ; 
                        case 'distribution_list_profiles':
                            $message_options['member_id_value'][] = $member['profile_id'] ; 
                            break ; 
                        }
                    
                    } 
                }
            
            }
        
        
        // Set an array of recipient IDs 
        // Input is the profile_id / user_id / contact_id => output is the delivery asset member_id
        if (($continue == 1) AND (isset($message_options['member_id_value']))) {
        
            $recipient_id_array = array() ;         
            
            // Filter by an array of member_id_values's (which could be contact id's, user id's, or profile id's)
            $member_query = array(
                'filter_by_user_id' => 'no',
                'filter_by_account_id' => 'no',
                'filter_by_asset_id' => 'yes',
                'filter_by_member_id_value' => $message_options['member_id_value']
                ) ; 
            $admin_campaign->Set_Asset_Members_List($member_query) ;
            $members = $admin_campaign->Get_Asset_Members_List() ;

            foreach ($members as $member) {
                $recipient_id_array[] = $member['member_id'] ; 
                }
            
            } 
            
        
        
        if (!isset($recipient_id_array)) {
            $continue = 0 ; 
            }
        
        // Create the campaign
        if ($continue == 1) {
            
            $campaign_options = array(
                'asset_id' => $content_asset_id,
                'delivery_asset_id' => $delivery_asset_id, 
                'automation_status_name' => 'scheduled',
                'timestamp_campaign_scheduled' => TIMESTAMP,
                'timestamp_next_action' => TIMESTAMP
                ) ; 

            if (isset($message_options['timestamp_next_action'])) {
                $campaign_options['timestamp_next_action'] = $message_options['timestamp_next_action'] ;
                } 
            
            
            if ($data['profile_record']['messaging_number_id']) {
                $campaign_options['messaging_number_id'] = $data['profile_record']['messaging_number_id'] ; 
                }


            $campaign_options['campaign_title'] = 'Admin Message: '.$data['content_asset_record']['asset_title'] ;
            $campaign_options['delivery_trigger'] = 'delivery_list_custom' ;
            $campaign_options['archive_campaign'] = 'yes' ;
            $campaign_options['recipients'] = array(
                'to' =>  array(
                    'delivery_list_custom' => array(
                        'member_id' => $recipient_id_array
                        )
                    )
                ) ;

            
            
            switch ($data['content_asset_record']['type_name']) {
                case 'email':
                    switch ($message_options['subscription_status']) {
                        case 'all':
                            $campaign_options['recipients']['to']['delivery_list_custom']['distribution_sublist_email_metadata_id'] = array(46,47,51) ; // subscribed, unsubscribed, inactive
                            break ; 
                        default:    
                            $campaign_options['recipients']['to']['delivery_list_custom']['distribution_sublist_email_metadata_id'] = array(46) ; // subscribed                    
                        }
                    break; 
                case 'sms':
                    switch ($message_options['subscription_status']) {
                        case 'all':
                            $campaign_options['recipients']['to']['delivery_list_custom']['distribution_sublist_sms_metadata_id'] = array(46,47,51) ; // subscribed, unsubscribed, inactive
                
                            break ; 
                        default:    
                            $campaign_options['recipients']['to']['delivery_list_custom']['distribution_sublist_sms_metadata_id'] = array(46) ; // subscribed
                        }                    
                    break ; 
                }
            
            
            if (isset($message_options['personalization_overrides'])) {
                $campaign_options['personalization_overrides'] = $message_options['personalization_overrides'] ; 
                }
            
            
            $create_campaign_result = $admin_campaign->Action_Create_Campaign($campaign_options) ; 
            }
        
               
        
        
        $result = array(
//            'profile_record' => $data['profile_record'],
//            'delivery_asset_record' => $data['delivery_asset_record'],
            'recipient_id_array' => $recipient_id_array,
            'campaign_options' => $campaign_options,
            'campaign_create' => $create_campaign_result,
//            'user_record' => $data['user_record']
            ) ; 
        
        return $result ; 
        
        }
    
    
    
    public function Action_Calculate_Marketing_Credits($query_options = array()) {
        
        $continue = 1 ; 
        
        if (isset($query_options['marketing_credit_balance'])) { 
            $result['current_balance'] = $query_options['marketing_credit_balance'] ; 
            
            } else if (isset($this->account['marketing_credit_balance'])) {
            $result['current_balance'] = $this->account['marketing_credit_balance'] ; 
            
            } else {
            $continue = 0 ; 
            }
            
         
        if (!isset($query_options['action'])) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {

            $result['recipient_count'] = $query_options['recipient_count'] ;  

            
            if (isset($this->account_marketing_credits['projected_balance'])) {
                $calculate_from_balance = $this->account_marketing_credits['projected_balance'] ; 
                } else {
                    $calculate_from_balance = $result['current_balance'] ; 
                    }
            

            $result['rate'] = 0 ; // The cost to send 1 message or 1 entire series
            
            switch ($query_options['action']) {
                case 'count':
                    // Nothing needed
                    break ; 
                case 'email':

                    $result['rate'] = 1 ; 
                    break ; 
                case 'sms':
                case 'mms':

                    $result['rate'] = 10 ;                
                    break ; 
                case 'series':
                    
                    foreach ($query_options['asset']['structure'] as $structure) {
                        switch ($structure['type_name']) {
                            case 'email':
                                if (in_array($structure['type_name'],$query_options['asset_action'])) {
                                    $result['rate'] = $result['rate'] + 1 ;     
                                    }
                                
                                
                                break ; 
                            case 'sms':
                            case 'mms':
                                if (in_array($structure['type_name'],$query_options['asset_action'])) {
                                    $result['rate'] = $result['rate'] + 10 ;     
                                    }
                                break ; 
                            }
                        }
                    
                    break ;
                }

            $result['action'] = $query_options['action'] ; 
            $result['cost'] = $result['rate'] * $result['recipient_count'] ; 
            $result['projected_balance'] = $calculate_from_balance - $result['cost'] ;            

            if (isset($this->account_marketing_credits['total_cost'])) {
                $result['total_cost'] = $this->account_marketing_credits['total_cost'] + $result['cost'] ; 
                } else {
                    $result['total_cost'] = $result['cost'] ; 
                    }
            
            } 
        
 
        $this->account_marketing_credits = $result ; 
        
        return $this ; 
        }
    
    
    public function Action_Update_Feature($query_options = array()) {
        
        $continue = 1 ; 
        
        if (!isset($query_options['feature_id'])) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {                        
            
            if ($query_options['feature_id'] == 'new') {
                
                unset($query_options['feature_id']) ;
                
                $query_options['feature_name'] = Utilities::Format_Convert_Lowercase($query_options['feature_name']) ; // Use the title as the slug
                $query_options['feature_name'] = str_replace(" ","_",$query_options['feature_name']) ; // Replace space w/ period                
                                   
                $result = $this->Create_Feature($query_options) ;      
                
                } else {
                
                    $result = $this->Update_Feature($query_options) ;                      
                    }
            
            if (!$result['error']) {
                $this->Set_Alert_Response(201) ; // Privileges updated
                } else {
                    $this->Set_Alert_Response(202) ; // Error updating privileges
                    }             
            }
        
        return $this ; 
        }
    
    
    
    public function Create_Feature($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_features',
            'values' => $query_options
            ) ; 
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->account_query_result = $result ; 
        
        $history_input = array(
            'function' => 'create_application_feature',
            'notes' => json_encode($query_options)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        }
    
    
    public function Update_Feature($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
                    
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $query_array = array(
            'table' => 'system_features',
            'values' => $values_array,
            'where' => "system_features.feature_id='$query_options->feature_id'"
            );            
            
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->account_query_result = $result ; 

        return $result ;         
        }
    
    
    public function Action_Update_Feature_Set($query_options = array()) {
        
        $continue = 1 ; 
        
        if (!isset($query_options['feature_set_id'])) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {                        
            
            if ($query_options['feature_set_id'] == 'new') {
                
                unset($query_options['feature_set_id']) ; 
                
                $query_options['feature_set_name'] = Utilities::Format_Convert_Lowercase($query_options['feature_set_name']) ; // Use the title as the slug
                $query_options['feature_set_name'] = str_replace(" ","_",$query_options['feature_set_name']) ; // Replace space w/ period                
                                
                $result = $this->Create_Feature_Set($query_options) ;
                
                } else {
                    $result = $this->Update_Feature_Set($query_options) ; 
                    }
            
                                                
            if (!$result['error']) {
                $this->Set_Alert_Response(201) ; // Privileges updated
                } else {
                    $this->Set_Alert_Response(202) ; // Error updating privileges
                    }             
            }
        
        return $this ; 
        }
    
    
    public function Create_Feature_Set($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_feature_sets',
            'values' => $query_options
            ) ; 
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->account_query_result = $result ; 
        
        $history_input = array(
            'function' => 'create_application_feature_set',
            'notes' => json_encode($query_options)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        }
    
    
    public function Update_Feature_Set($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
                    
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $query_array = array(
            'table' => 'system_feature_sets',
            'values' => $values_array,
            'where' => "system_feature_sets.feature_set_id='$query_options->feature_set_id'"
            );            
            
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->account_query_result = $result ; 

        return $result ;         
        }
    
    
    
    
    // $do_not_contact_status should be 1 or 0. Will default to 1, which means do not contact this entry
    public function Action_Update_Profile_Restriction($restriction_input,$do_not_contact_status = 1) {
        
        $continue = 1 ; 
        
        if ((!$this->user_id) OR (!$this->profile_id) OR (!$this->account_id)) {            
            $continue = 0 ;             
            }
        
        if ((!isset($restriction_input['email_address'])) AND (!isset($restriction_input['phone_number_international']))) {            
            $continue = 0 ;             
            }
        
        
        if ($continue == 1) {                        
            $restriction_input['do_not_contact'] = $do_not_contact_status ; 
            $result = $this->Create_Profile_Restriction($restriction_input) ;                         
            }
        
        return $this ; 
        }
    
    
    // 1 = do_not_contact is active (not allowed to contact this destination); 0 = free to contact
    public function Create_Profile_Restriction($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $query_array = array(
            'table' => "user_profile_restrictions",
            'values' => array(
                'user_id' => $this->user_id,
                'account_id' => $this->account_id,
                'profile_id' => $this->profile_id,
                'do_not_contact' => $query_options->do_not_contact
                ),
            'where' => "user_profile_restrictions.user_id='$this->user_id' AND user_profile_restrictions.account_id='$this->account_id' AND user_profile_restrictions.profile_id='$this->profile_id'"
            ) ;

        if (isset($query_options->email_address)) {
            $query_array['values']['email_address'] = $query_options->email_address ;
            $query_array['where'] .= " AND user_profile_restrictions.email_address='$query_options->email_address'" ;
            }
        if (isset($query_options->phone_number_international)) {
            $query_array['values']['phone_number_international'] = $query_options->phone_number_international ;
            $query_array['where'] .= " AND user_profile_restrictions.phone_number_international='$query_options->phone_number_international'" ;
            }
                
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        $this->account_query_result = $result ; 
        
        
        if (!isset($result['error'])) {
            
            // Create History Entry
            $history_input = array(
                'account_id' => $this->account_id,
                'function' => 'update_user_restrictions',
                'notes' => json_encode($query_array['values'])
                ) ; 
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;        
            }
        
        return $result ;         
        }
    
    
    
    public function Action_Update_Account($account_input) {
        
        $continue = 1 ;
        
        if (isset($account_input['account_username'])) {
            $format_username = $this->Action_Format_Account_Username($account_input['account_username']) ; 
            $format_result = $this->Get_Response() ; 
            
            $account_input['account_username'] = $format_username['account_username'] ; 
            
            if ($format_result['result'] == 'error') {
                $continue = 0 ; 
                } else {
                
                    $check_username = $this->Action_Check_Username($account_input['account_username']) ;  
                    $check_result = $this->Get_Response() ; 
                
                    if ($check_result['result'] == 'error') {
                        $continue = 0 ; 
                        }                
                    } 
            }
        
        // Global blog posts
        if (isset($account_input['structured_data_account_01']['customization_allow_global_blog_posts'])) {
            switch ($account_input['structured_data_account_01']['customization_allow_global_blog_posts']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_allow_global_blog_posts'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_allow_global_blog_posts'] = 'yes' ; 
                    break ; 
                }            
            }
        
        // Global blog post comments
        if (isset($account_input['structured_data_account_01']['customization_allow_global_blog_post_comments'])) {
            switch ($account_input['structured_data_account_01']['customization_allow_global_blog_post_comments']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_allow_global_blog_post_comments'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_allow_global_blog_post_comments'] = 'yes' ; 
                    break ; 
                }            
            }        
        
        
        
        // Global product post
        if (isset($account_input['structured_data_account_01']['customization_allow_global_product_posts'])) {
            switch ($account_input['structured_data_account_01']['customization_allow_global_product_posts']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_allow_global_product_posts'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_allow_global_product_posts'] = 'yes' ; 
                    break ; 
                }            
            }
        
        // Global product post comments
        if (isset($account_input['structured_data_account_01']['customization_allow_global_product_post_comments'])) {
            switch ($account_input['structured_data_account_01']['customization_allow_global_product_post_comments']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_allow_global_product_post_comments'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_allow_global_product_post_comments'] = 'yes' ; 
                    break ; 
                }            
            } 
        

        // ALL Global post comments
        if (isset($account_input['structured_data_account_01']['customization_disable_global_post_comments'])) {
            switch ($account_input['structured_data_account_01']['customization_disable_global_post_comments']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_disable_global_post_comments'] = 'no' ; 
                    $account_input['structured_data_account_01']['customization_allow_global_blog_post_comments'] = 'yes' ; 
                    $account_input['structured_data_account_01']['customization_allow_global_product_post_comments'] = 'yes' ;
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_disable_global_post_comments'] = 'yes' ; 
                    $account_input['structured_data_account_01']['customization_allow_global_blog_post_comments'] = 'no' ; 
                    $account_input['structured_data_account_01']['customization_allow_global_product_post_comments'] = 'no' ;
                    break ; 
                }            
            }        
        
        
        // ALL Global post comments
        if (isset($account_input['structured_data_account_01']['customization_disable_asset_comments'])) {
            switch ($account_input['structured_data_account_01']['customization_disable_asset_comments']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_disable_asset_comments'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_disable_asset_comments'] = 'yes' ; 
                    break ; 
                }            
            }
        
        
        if (isset($account_input['structured_data_account_01']['customization_marketing_site_background_show_account_name'])) {
            switch ($account_input['structured_data_account_01']['customization_marketing_site_background_show_account_name']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_marketing_site_background_show_account_name'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_marketing_site_background_show_account_name'] = 'yes' ; 
                    break ; 
                }            
            }
        if (isset($account_input['structured_data_account_01']['customization_marketing_site_background_show_account_tagline'])) {
            switch ($account_input['structured_data_account_01']['customization_marketing_site_background_show_account_tagline']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_marketing_site_background_show_account_tagline'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_marketing_site_background_show_account_tagline'] = 'yes' ; 
                    break ; 
                }            
            }
        if (isset($account_input['structured_data_account_01']['customization_marketing_site_background_show_cta'])) {
            switch ($account_input['structured_data_account_01']['customization_marketing_site_background_show_cta']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['customization_marketing_site_background_show_cta'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['customization_marketing_site_background_show_cta'] = 'yes' ; 
                    break ; 
                }            
            }
        
        if (isset($account_input['structured_data_account_01']['affiliate_program_active'])) {
            switch ($account_input['structured_data_account_01']['affiliate_program_active']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['affiliate_program_active'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['affiliate_program_active'] = 'yes' ; 
                    break ; 
                }
            
            }        
        
        if (isset($account_input['structured_data_account_01']['event_calendar_beta_active'])) {
            switch ($account_input['structured_data_account_01']['event_calendar_beta_active']) {
                case 'false':
                case 'no':
                    $account_input['structured_data_account_01']['event_calendar_beta_active'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $account_input['structured_data_account_01']['event_calendar_beta_active'] = 'yes' ; 
                    break ; 
                }
            
            }        
        
        if ($continue == 1) {
            $update_account = $this->Update_Account($account_input) ;
            
            if (isset($account_input['account_username'])) {
                $account_username_input = array(
                    'account_username' => $account_input['account_username']
                    ) ; 
                $update_account_username = $this->Create_Account_Username($account_username_input) ;
                }
            
            if ($update_account['results'] == true) {

                $this->Set_Alert_Response(18) ; // Account updated

                // I think we should only update Stripe during monthly billing.
                //$stripe = new Stripe($this->user_id) ; 
                //$stripe_response = $stripe->Connect(BILLING_MODE)->Set_Account_By_ID($this->account_id)->Action_Update_System_Customer()->Get_Response() ; 
                //$this->alert_id = $stripe_response['alert_id'] ; 
                //$this->response = $stripe_response ; 

                } else {

                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($update_account)
                        )) ;             

                    }             
            }

        return $this ; 
        }
    
    
    // Transition accounts running version x or greater to version y
    public function Action_Update_Account_Version($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $values_array = array() ; 
        $values_array['version'] = $query_options->new_version ; 
        
        
        $query_array = array(
            'table' => 'accounts',
            'values' => $values_array,
            'where' => "accounts.version>='$query_options->current_version'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array) ;
        $this->account_query_result = $result ;        

        
        if (!$result['error']) {
            $this->Set_Alert_Response(279) ; // Accounts updated
            } else {
                $this->Set_Alert_Response(280) ; // Accounts update error
                }        
        
        return $this ; 
        }
    
    
    // Create a permanent record of all account usernames created for the account
    public function Create_Account_Username($query_options) {
    
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ;             
            }
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => "account_usernames",
            'values' => $values_array,
            'where' => "account_usernames.account_username='$query_options->account_username' AND account_usernames.account_id='$this->account_id'"
            );

        $query_array['values']['account_id'] = $this->account_id ;
        $query_array['values']['timestamp_updated'] = TIMESTAMP ;
        $query_array['values']['timestamp_created'] = array(
            'value' => TIMESTAMP,
            'statement' => "CASE WHEN timestamp_created = 0 THEN 'VALUE' ELSE timestamp_created END"
            ) ;        
        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        
        return $result ; 
        }
    
    
    // Create a permanent record of all profile usernames created for the profile
    public function Create_Profile_Username($query_options) {
    
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ;             
            }
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => "user_profile_usernames",
            'values' => $values_array,
            'where' => "user_profile_usernames.profile_username='$query_options->profile_username' AND (user_profile_usernames.profile_id='$this->profile_id' OR user_profile_usernames.profile_id='0')"
            );

        $query_array['values']['profile_id'] = $this->profile_id ;
        $query_array['values']['timestamp_updated'] = TIMESTAMP ;
        $query_array['values']['timestamp_created'] = array(
            'value' => TIMESTAMP,
            'statement' => "CASE WHEN timestamp_created = 0 THEN 'VALUE' ELSE timestamp_created END"
            ) ;        
        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        
        return $result ; 
        }    
    
    
    public function Action_Map_Feature_Set($product_id,$query_options = array()) { 
        
        $stripe = new Stripe() ; 
        $stripe->Set_Product_By_ID($product_id) ; 
        $product = $stripe->Get_Product() ; 

        $product_feature_set = json_decode($product['feature_set'],1) ;
        

        $query_options['filter_by_feature_set_id'] = $product_feature_set['feature_set'] ; 
        $feature_results = $this->Retrieve_Product_Feature_Operations_List($query_options) ;                
        
        $feature_set_array = array() ; 
        $operations_allowed = array() ; 

        if (count($feature_results['result_count'] > 0)) {
            foreach ($feature_results['results'] as $operation) {

                // This is for operations...
                $operation_set = array(
                    'operation_id' => $operation['operation_id'],
                    'operation_title' => $operation['operation_title'],
                    'operation_name' => $operation['operation_name'],
                    'operation_description' => $operation['operation_description']
                    ) ;

                $feature_set = array(
                    'feature_id' => $operation['feature_id'],
                    'feature_title' => $operation['feature_title'],
                    'feature_name' => $operation['feature_name'],
                    'feature_description' => $operation['feature_description'],
                    'feature_set' => array(
                        'feature_set_id' => $operation['feature_set_id'],
                        'feature_set_name' => $operation['feature_set_name'],
                        'feature_set_title' => $operation['feature_set_title'],
                        'feature_set_description' => $operation['feature_set_description']
                        )
                    ) ;

                $operation_array = array(
                    'effect' => 'allow',
//                    'operation' => $operation_set,
//                    'feature' => $feature_set
                    ) ; 

                $operations_allowed[$operation['operation_name']] = $operation_array ; 

                
                // This is for building the feature sets...
                $feature = array(
                    'effect' => 'allow',
                    'feature_id' => $operation['feature_id'],
                    'feature_title' => $operation['feature_title'],
                    'feature_name' => $operation['feature_name'],
                    'feature_description' => $operation['feature_description']
                    ) ; 
                
                
                if (!isset($feature_set_array[$operation['feature_set_name']])) {
                        $feature_set_array[$operation['feature_set_name']] = array(
                            'effect' => 'allow',
                            'feature_set_id' => $operation['feature_set_id'],
                            'feature_set_name' => $operation['feature_set_name'],
                            'feature_set_title' => $operation['feature_set_title'],
                            'feature_set_description' => $operation['feature_set_description'],
                            'features' => array()
                            ) ; 
                        }
                
                $feature_set_array[$operation['feature_set_name']]['features'][$operation['feature_name']] = $feature ; 
                
                }
            }
        

        $result = array(
            'product_id' => $product['product_id'],
            'operations_allowed' => $operations_allowed,
            'feature_set' => $feature_set_array
            ) ; 
        
        return $result ;
        }
    
    
    
    
    
    
    public function Action_Compile_Account_List() {
        
        $final_account_set = array() ; 
        
        if (($this->account_list != 'error') AND count($this->account_list > 0)) {        
            foreach ($this->account_list as $account) {

                $search_account = new Account($this->user_id) ; 
                $search_account->Set_Account_ID($account['account_id']) ; 
                
                $user_list = $search_account->Retrieve_Account_Users($query_options = array(
                    'account_auth_role_name_array' => array(),
                    'override_paging' => 'yes'
                    )) ; 
                
                if ($user_list != 'error') {
                    $account['total_user_count'] = $user_list['total_count'] ; 
                    }
                
                unset($user_list) ;
                
                $account_owner = $search_account->Retrieve_Account_Users($query_options = array(
                    'account_auth_role_name_array' => array('account_owner'),
                    'override_paging' => 'yes'
                    )) ;
                
                if ($account_owner != 'error') {
                    
                    
                    $account_owner = $this->Action_Time_Territorialize_Dataset($account_owner['results'][0]) ;
                    $account['account_owner'] = $this->Action_Phone_Territorialize_Dataset($account_owner) ;
                    }
                
                $final_account_set[] = $account ;
                } 
            
            $this->account_list = $final_account_set ; 
            } 
        
        return $this ; 
        }
    
    
    
    
    public function Action_Create_New_Account($account_input) {
            
        $continue = 1 ;                 
        
        if (!$this->user_id) {
            $continue = 0 ; 
            $this->Set_Alert_Response(19) ; // Generic account creation error. Missing a User ID to associate
            $this->Append_Alert_Response($personalize,array(
                'location' => __METHOD__,
                'admin_context' => 'No User ID associated with account creation request'
                )) ;             
            }
        
        if (!$account_input['account_username']) {
            $continue = 0 ; 
            $this->Set_Alert_Response(127) ; // Account username blank
            }
        
        
        if ($continue == 1) {
            
            $submitted_account_username = $account_input['account_username'] ; 
            $format_username = $this->Action_Format_Account_Username($account_input['account_username']) ;
    
            $account_input['account_username'] = $format_username['account_username'] ; 
            
            if ($user_input['account_username'] != $post_data_array['account_username']) {
                $continue = 0 ; 
                $personalize['account_record']['account_username_alternate'] = $account_input['account_username'] ; 
                $this->Set_Alert_Response(91) ; // Improperly formatted account username
                $this->Append_Alert_Response($personalize,array(
                    'location' => __METHOD__
                    )) ;                
                } else {
                    $this->Set_Alert_Response(20) ; // Username approved - formatting only.
                    }             
            }
               
        
        if ($continue == 1) {        
            
            $check_username = $this->Action_Check_Username($account_input['account_username']) ;    
            $result = $this->Get_Response() ; 
            if ($result['result'] == 'error') {
                $continue = 0 ; 
                } 
            }
        
        
        if ($continue == 1) {            
            
            $account_record = $this->Create_Account($account_input) ; 
        
            // If new record was created, then create new profile to associate with user & new account
            if ($account_record['insert_id']) {

                $this->Set_Account_By_ID($account_record['insert_id']) ; 
                $this->Set_Active_Account($this->account_id) ; 


                $account_username_input = array(
                    'account_username' => $account_input['account_username']
                    ) ; 
                $update_account_username = $this->Create_Account_Username($account_username_input) ;

                
                
                $account_metadata_update['marketing_credit_balance'] = 500 ; 
                
                // Update account metadata
                if (isset($account_input['structured_data_account_01'])) {
                    $account_metadata_update['structured_data_account_01'] = $account_input['structured_data_account_01'] ; 
                    }
                if (isset($account_input['structured_data_account_02'])) {
                    $account_metadata_update['structured_data_account_02'] = $account_input['structured_data_account_02'] ; 
                    }
                
                $this->Action_Update_Account($account_metadata_update) ; 
                
                // Associate the account authorization with a new profile
                $this->Action_Create_Profile($account_input) ;
                $profile_record = $this->Get_Profile() ; 

                
                // Create a customer in Stripe and a contact entry for the owner in the account
                if ($profile_record != 'error') { 

                    $stripe = new Stripe($this->user_id) ; 
                    $stripe->Connect(BILLING_MODE)->Set_Account_By_ID($this->account_id)->Action_Create_System_Customer() ; 

                    // Create new contact to tie to the profile
                    $contact = new Contact($this->user_id) ; 
                    $contact_input = array(
                        'first_name' => $profile_record['first_name'],
                        'last_name' => $profile_record['last_name'],
                        'email_address' => $profile_record['email_address'],
                        'phone_country_dialing_code' => $profile_record['phone_country_dialing_code'],
                        'phone_number' => $profile_record['phone_number'],
                        'city' => $profile_record['city'],
                        'state' => $profile_record['state'],
                        'country_id' => $profile_record['country_id'],
                        'visibility_id' => 5
                        ) ;

                    $contact->Set_Account_ID($this->account_id) ;
                    $contact_record = $contact->Create_Contact($contact_input) ;
                    
                    // Update the profile w/ the associated contact_id
                    $profile_update['profile_contact_id'] = $contact_record['success_set'][0]['contact_id'] ; 
                    
                    $this->Action_Update_Profile($profile_update) ;  
                    
                    // Create placeholder blog post as example for using blog editor
                    $asset_first_blog_post_id = $this->Set_System_Default_Value('asset_first_blog_post')->Get_System_Default_Value() ;
                    $asset = new Asset($this->user_id) ;
                    $asset->Set_Account_By_ID($this->account_id)->Set_Profile_By_User_Account()->Set_Asset_By_ID($asset_first_blog_post_id)->Action_Duplicate_Asset() ;
                    }
                

                $this->Set_Alert_Response(17) ; // Account created
                
                } else {

                    $this->Set_Alert_Response(16) ; // Account username taken
                    $this->Append_Alert_Response('none',array(
                        'location' => __METHOD__
                        )) ;                  
                
                    }
            } 
        
        
        return $this ;
        
        }    
    
    public function Action_Join_Account(
        $profile_input = array(), 
        $user_input = array()
        ) {
    
        
        // Authorization Defaults
        if (!isset($profile_input['account_auth_id'])) {
            $profile_input['account_auth_id'] = 3 ; // Default to pending account member
            }        
        
        // User Defaults
        if (!isset($user_input['email_address_id'])) {
            $user_input['email_address_id'] = $this->user['email_address_id'] ;
            }        
        
        
        $account_list_query = array(
            'override_paging' => 'no',
            'filter_by_account_id' => 'yes',
            'filter_by_user_id' => $this->user_id
            ) ; 

        $this->Set_Account_User_List($account_list_query) ; 
        $account_user_list = $this->Get_Account_User_List() ; 


        
        // Check to see if the user already exists 
        if (($account_user_list != 'error') AND (isset($account_user_list[0]) AND ($account_user_list[0]['user_id'] == $this->user_id))) {

            // Possible errors associated w/ a duplicate user > account association
            if ($account_user_list[0]['account_auth_id'] == 3) {
                $this->Set_Alert_Response(22) ;  // Pending approval by admin
                } elseif ($account_user_list[0]['user_id'] == $this->user_id) {
                $this->Set_Alert_Response(29) ; // Profile exists and not pending; this is a true duplicate
                } else {
                    $this->Set_Alert_Response(15) ; // generic error
                    }

            } else {

                $profile_input['email_address_id'] = $user_input['email_address_id'] ; 

                // Create profile and account authorization
                $this->Action_Create_Profile($profile_input) ;
                $create_profile_response = $this->Get_Response() ;
            
                if ($create_profile_response['result'] == 'success') {
                    $this->Set_Alert_Response(28) ; 
                    
                    if ($this->user['active_account_id'] == 0) {
                        $user_update_array['active_account_id'] = $this->account_id ; 
                        $this->Update_User($user_update_array) ;                     
                        }                     
                    }
                }        
        
        return $this ; 
        }
    
    public function Action_Create_Profile($profile_input) {

        $profile_input['user_id'] = $this->user_id ;
        $profile_input['account_id'] = $this->account_id ;
        
        // If explicit inputs given, use them... otherwise pull from the master user profile
        if (!isset($profile_input['email_address_id'])) {
            $profile_input['email_address_id'] = $this->user['email_address_id'] ; 
            }

        if (!isset($profile_input['phone_number_id'])) {
            $profile_input['phone_number_id'] = $this->user['phone_number_id'] ; 
            }
        
        if (!isset($profile_input['first_name'])) {
            $profile_input['first_name'] = $this->user['first_name'] ; 
            }
        
        if (!isset($profile_input['last_name'])) {
            $profile_input['last_name'] = $this->user['last_name'] ; 
            }        
                
        if (!isset($profile_input['profile_display_name'])) {
            $profile_input['profile_display_name'] = $profile_input['first_name'].' '.$profile_input['last_name'] ; 
            }        
        
        if (!isset($profile_input['profile_username'])) {
            $profile_input['profile_username'] = $profile_input['profile_display_name'] ; 
            }
        
        if (!isset($profile_input['account_auth_id'])) {
            $account_auth_input['auth_role_name'] = 'account_owner' ; 
            $this->Set_System_Auth_List($account_auth_input) ; 
            $submitted_role_list = $this->Get_System_Auth_List() ; 
            $profile_input['account_auth_id'] = $submitted_role_list[0]['auth_id'] ;             
            }
        
        
        $profile_input['profile_username'] = $this->Action_Format_Profile_Username($profile_input['profile_username']) ;
        $check_profile = $this->Action_Check_Profile_Username($profile_input['profile_username']) ; 
        $check_profile_username = $this->Get_Response() ;
        
        
        if ($check_profile_username['result'] == 'success') {

            $new_profile = $this->Create_Profile($profile_input) ; 
                    
            if ($new_profile['insert_id']) {
                
                $this->Set_User_By_Profile_ID($new_profile['insert_id']) ; 
                
                // Create the profile username entry
                if (isset($profile_input['profile_username'])) {
                    $profile_username_input = array(
                        'profile_username' => $profile_input['profile_username']
                        ) ; 
                    $update_profile_username = $this->Create_Profile_Username($profile_username_input) ;
                    } 

                // Create a new contact distribution list
                $asset_input = array(
                    'type_id' => 19,
                    'list_title' => $profile_input['profile_display_name']."'s Master Subscription List",
                    'list_description' => 'Add contacts to this subscription list to send them emails & text messages.'
                    ) ; 

                $asset = new Asset() ; 
                $asset->Set_User_By_Profile_ID($new_profile['insert_id']) ; 
                $asset->Action_Create_Asset($asset_input) ; 
                $data['new_list_asset'] = $asset->Get_Asset() ; 
                $asset_id = $data['new_list_asset']['asset_id'] ; 

                $profile_update['profile_contact_delivery_asset_id'] = $asset_id ; 
                $this->Action_Update_Profile($profile_update) ;                
                
                
                // Add new profile to the News & Updates distribution list...
                $invite_input['profile_id'] = $new_profile['insert_id'] ; 
                $options_input['require_optin'] = 'false' ;   
                $options_input['subscribe_type_email'] = 'true' ;
//                $options_input['subscribe_type_sms'] = 'true' ;
                
                // Get the asset ID for the distro list...
                $distro_list_all_account_profiles = $this->Set_System_Default_Value('distro_list_all_account_profiles')->Get_System_Default_Value() ;
                
                $new_profile_distro = new Asset() ; 
                $new_profile_distro->Set_Asset_By_ID($distro_list_all_account_profiles) ; 
                $new_profile_distro->Action_Asset_Member_Process($invite_input,$options_input) ;
                
                
                // Set profile and return response
                $this->Set_Profile_By_ID($new_profile['insert_id']) ; 
                $this->Set_Alert_Response(160) ; // Profile successfully created
                } else {
                    $this->Set_Alert_Response(161) ; // Profile creation error
                    }
            }
        
        return $this ;
        } 
    
    
    
    
    public function Action_Format_Profile_Username($profile_username) {

        $profile_username = Utilities::Format_Convert_Lowercase($profile_username) ; // Use the title as the slug
        $profile_username = str_replace(" ",".",$profile_username) ; // Replace space w/ period
        $profile_username = preg_replace("/\.+$/","",$profile_username); // Remove trailing periods
        $profile_username = preg_replace('/[^\w-.]/', '', $profile_username); // Allow alphanumeric, dash, underscore and period
        $profile_username = str_replace("..",".",$profile_username) ; // Replace double period w/ single period
        
        return $profile_username ;
        }
    
    public function Action_Format_Account_Username($account_username) { 
        
        $account_username = Utilities::Format_Convert_Lowercase($account_username) ; // Use the title as the slug
        
        $account_username_original = $account_username ; // Converts to lowercase first so switching to lower won't throw error.
        
        $account_username = str_replace(" ","-",$account_username) ; // Replace space w/ dash
        $account_username = preg_replace('/[^\w-]/', '', $account_username); // Allow alphanumeric, dash, and underscore
        
        // Check to see if username is long enough
        if (strlen($account_username) < 4) {
            $l = 0 ;  // $l=0 means length not long enough
            do {
                $account_username = $account_username.'-'.$account_username ; // lengthen the username
                if (strlen($account_username) >= 4) {
                    $l = 1 ; // Length 4 characters or more; $l=1 to stop loop
                    }
                
                } while ($l == 0);      
            }        
        
                
        if ($account_username != $account_username_original) {
                $this->Set_Alert_Response(91) ; // Account username taken  
                $personalization = array(
                    'account_record' => array(
                        'account_username' => $account_username_original,
                        'account_username_alternate' => $account_username
                        )
                    ) ;
                $this->Append_Alert_Response($personalization,array(
                    'location' => __METHOD__
                    )) ;            
            } else {
                $this->Set_Alert_Response(20) ; // Account username approved (formatting only)
                }

        
        $check_result = array(
            'account_username' => $account_username,
            'account_username_original' => $account_username_original,
            'result' => $this->Get_Response()
            ) ; 
        
        return $check_result ;
        }     

    
    // Check the account_username input
    public function Action_Check_Username($account_username) {
        
         
        global $server_config ; // To personalize the error for an application name, if desired.
        
        $format_username = $this->Action_Format_Account_Username($account_username) ; 
        $account_username = $format_username['account_username'] ; 
        
        $account_username_original = $account_username ;
        
        
        // Loop through alternatives for the asset slug to ensure the DB only holds unique slugs
        $i = date("md",TIMESTAMP) ; // Inititalize the alternative name counter
        $duplicate = 0 ; 
        do {
            
            $continue = 1 ; 
            
            // Check existing account usernames
            $query_array = array(
                'table' => 'account_usernames',
                'values' => '*',
                'where' => "account_usernames.account_username='$account_username'"
                );

            $result = $this->DB->Query('SELECT',$query_array) ;

            // Check protected values
            $protected_values_query = array(
                'value_name' => $account_username,
                'value_type' => 'account'
                ) ;
            
            $system_lists = new System() ;
            $system_lists->Set_System_List('list_protected_values',$protected_values_query) ; 
            $data['list_protected_values'] = $system_lists->Get_System_List('list_protected_values') ;             
            
            
            // Check to see if username taken by another account
            if (($result['result_count'] > 0) AND ($result['results']['account_id'] != $this->account_id)) {
                $continue = 0  ;
                $duplicate = 1 ; 
                }            
            
            if (($continue == 1) AND (count($data['list_protected_values']) > 0)) {
                $continue = 0  ; 
                $duplicate = 1 ; 
                }
            
            if ($continue == 0) {
                $account_username = $account_username_original.'-'.$i ; // Add an integer to the slug if there is a duplicate
                $i++ ;    
                } else {
                    $i = 0 ; 
                    }                  
            } while ($i > 0);         
        
        
        switch ($duplicate) {
            case 1:
                $this->Set_Alert_Response(16) ; // Account username taken  
                $personalization = array(
                    'account_record' => array(
                        'account_username' => $account_username_original,
                        'account_username_alternate' => $account_username
                        )
                    ) ;
                $this->Append_Alert_Response($personalization,array(
                    'location' => __METHOD__
                    )) ; 
                break ; 
            case 0:
                $this->Set_Alert_Response(20) ; // Account username approved
                break ; 
            }
        
        
        $check_result = array(
            'duplicate' => $duplicate,
            'account_username' => $account_username,
            'account_username_original' => $account_username_original,
            'result' => $this->Get_Response()
            ) ; 
        
        return $check_result ;
        }
    
    
    
    public function Retrieve_Profile_ID_By_Username($profile_username) {
        
        $profile_username = $this->Action_Format_Profile_Username($profile_username) ; 
        
        // Check existing account usernames
        $query_array = array(
            'table' => 'user_profile_usernames',
            'values' => '*',
            'where' => "user_profile_usernames.profile_username='$profile_username'"
            );

        $result = $this->DB->Query('SELECT',$query_array) ;  
        
        return $result ; 
        }
    
    
    public function Action_Check_Profile_Username($profile_username) {
        
        global $server_config ; // To personalize the error for an application name, if desired.
        
        $continue = 1 ; 
        
        $profile_username = $this->Action_Format_Profile_Username($profile_username) ; 
        
        $profile_username_original = $profile_username ;
        
        
        // Loop through alternatives for the asset slug to ensure the DB only holds unique slugs
        $i = date("md",TIMESTAMP) ; // Inititalize the alternative name counter
        
        $duplicate = 0 ; 
        
        do {
            
            $continue = 1 ; 

            $result = $this->Retrieve_Profile_ID_By_Username($profile_username) ; 
            
            
            // Check protected values
            $protected_values_query = array(
                'value_name' => $profile_username,
                'value_type' => 'profile'
                ) ;
            
            $system_lists = new System() ;
            $system_lists->Set_System_List('list_protected_values',$protected_values_query) ; 
            $data['list_protected_values'] = $system_lists->Get_System_List('list_protected_values') ;
            
            
            // Check to see if username taken by another profile
            if (($result['result_count'] > 0) AND ($result['results']['profile_id'] != $this->profile_id) AND ($result['results']['profile_id'] != 0)) {
                $continue = 0  ;
                $duplicate = 1 ; 
                if ($result['result_count'] == 1) {
                    $duplicate_profile = $result['results'] ;
                    } else {
                        $duplicate_profile = $result['results'][0] ;
                        }
                }            

            if (($continue == 1) AND (count($data['list_protected_values']) > 0)) {
                $continue = 0  ; 
                $duplicate = 1 ; 
                }
            
            if ($continue == 0) {
                $profile_username = $profile_username_original.$i ; // Add an integer to the slug if there is a duplicate
                $i++ ;    
                } else {
                    $i = 0 ; 
                    }                  
            } while ($i > 0);         
        
        
        switch ($duplicate) {
            case 1:
                $this->Set_Alert_Response(159) ; // Profile username taken
                $personalization_array['profile_record']['profile_username_alternate'] = $profile_username ;
                $this->Append_Alert_Response($personalization_array) ; 
                break ; 
            case 0:
                $this->Set_Alert_Response(157) ; // Profile username approved
                break ; 
            }
        
        
        $check_result = array(
            'duplicate' => $duplicate,
            'duplicate_profile' => $duplicate_profile,
            'profile_username' => $profile_username,
            'profile_username_original' => $profile_username_original,
            'result' => $this->Get_Response()
            ) ; 
        
        return $check_result ;        
        }
    
    
    
    // Set the profile by ID before updating
    public function Action_Update_Profile($profile_input,$account_auth_input = array()) {
        
        $continue = 1 ; 
        
        if (isset($profile_input['profile_username'])) {
            $profile_input['profile_username'] = $this->Action_Format_Profile_Username($profile_input['profile_username']) ;
            $this->Action_Check_Profile_Username($profile_input['profile_username']) ;             
            $check_profile_username = $this->Get_Response() ;
            if ($check_profile_username['result'] != 'success') {        
                $continue = 0 ; 
                }
            }
        
        
        if ($continue == 1) {
        
            if (isset($profile_input['profile_contact_delivery_asset_id'])) {
                $profile_input['structured_data_profile_01']['profile_contact_delivery_asset_id'] = $profile_input['profile_contact_delivery_asset_id'] ; 
                }
            if (isset($profile_input['profile_contact_id'])) {
                $profile_input['structured_data_profile_01']['profile_contact_id'] = $profile_input['profile_contact_id'] ; 
                }            
            if (isset($profile_input['profile_title'])) {
                $profile_input['structured_data_profile_01']['profile_title'] = $profile_input['profile_title'] ;                  
                }             
            if (isset($profile_input['profile_headline'])) {
                $profile_input['structured_data_profile_01']['profile_headline'] = $profile_input['profile_headline'] ;                  
                }     
            if (isset($profile_input['email_address_private'])) {
                $profile_input['structured_data_profile_01']['email_address_private'] = $profile_input['email_address_private'] ;                  
                }  
            if (isset($profile_input['phone_number_private'])) {
                $profile_input['structured_data_profile_01']['phone_number_private'] = $profile_input['phone_number_private'] ;                  
                }  
            if (isset($profile_input['affiliate_program_email_id'])) {
                $profile_input['structured_data_profile_01']['affiliate_program_email_id'] = $profile_input['affiliate_program_email_id'] ;                  
                }            
            unset($profile_input['email_address_private'],
                  $profile_input['phone_number_private'],
                  $profile_input['profile_title'],
                  $profile_input['profile_headline'],
                  $profile_input['profile_contact_delivery_asset_id'],
                  $profile_input['profile_contact_id'],
                  $profile_input['affiliate_program_email_id']) ;

            
            $result = $this->Update_Profile($profile_input) ; 
            $this->Set_Model_Timings('Profile.Action_Update_Profile_core_update_complete') ;
            
            if (isset($profile_input['profile_username'])) {
                $profile_username_input = array(
                    'profile_username' => $profile_input['profile_username']
                    ) ; 
                $update_profile_username = $this->Create_Profile_Username($profile_username_input) ;
                }        


            // Profile update needs to have passed in order to access Authorization update.
            if (isset($account_auth_input['account_auth_id']) AND ($result['results'] == 1)) {
                
                $validation_response = $this->Action_Validate_Account_Auth($account_auth_input) ;     

                if ($validation_response['validation'] == 1) {
                    
                    $auth_input = array(
                        'account_auth_id' => $account_auth_input['account_auth_id']
                        ) ; 
                    
                    $result = $this->Update_Profile($auth_input) ; 
                    
//                    $this->test_data = $validation_response ; 
                    
                    foreach ($validation_response['followup_actions'] as $action) {

                        // Perform required followup actions based on results of Action_Validate_Account_Auth()
                        switch($action['action']) {
                            case 'update_user_account_auth':

                                $account_input['account_auth_id'] = 9 ; // Account Admin
                                $prior_owner = new Account() ;
                                $update_prior_owner = $prior_owner->Set_Profile_By_ID($action['profile_id'])->Update_Profile($account_input) ; 
                                
                                break ; 
                            }

                        }

                    } else {
                        $this->Set_Alert_Response($validation_response['alert_id']) ; 
                        }
                
                $this->Set_Model_Timings('Profile.Action_Update_Profile_account_auth_update_complete') ;
                }
            
            $this->Set_Profile_By_ID() ; 
            $this->Set_Model_Timings('Profile.Action_Update_Profile_profile_reset_complete') ;
            
            // Set the result from initial profile update
            if ($result['results'] == 1) {
                $this->Set_Alert_Response(23) ; // Succesfully updated  
                
                $profile_record = $this->Get_Profile() ; 
                
                $update_subscription_lists = 1 ; 
//                foreach ($profile_input as $input_key => $input_value) {
//                    switch ($input_key) {
//                        case 'first_name':
//                        case 'last_name':
//                        case 'email_address_id':
//                        case 'phone_number_id':    
//                        case 'account_auth_id':
//                            $update_subscription_lists = 1 ; 
//                            break ; 
//                        }
//                    }


                if ($update_subscription_lists == 1) {

                    // Pull broadcast & series list subscriptions
                    $profile_record['profile_subscriptions_list'] = array() ; 

                    $subscriptions_query = array(
                        'filter_by_asset_id' => 'no',
                        'filter_by_user_id' => 'no',
                        'filter_by_account_id' => 'no',
                        'filter_by_type_name' => array('distribution_list_profiles','series_list_profiles'),
                        'filter_by_member_id_value' => $profile_record['profile_id'],
                        'filter_by_visibility_name' => 'visible',
                        'override_paging' => 'yes'
                        ) ;

                    $subscriptions = new Asset() ; 
                    $subscriptions->Set_Asset_Members_List($subscriptions_query) ; 
                    $subscriptions_list = $subscriptions->Get_Asset_Members_List() ;

                    $profile_record['profile_subscriptions_list'] = array() ; 
                    foreach ($subscriptions_list as $list_record) {
                        $profile_record['profile_subscriptions_list'][] = $list_record['asset_id'] ;                        
                        }
                                        
                    $task_values = array(
                        'task_action' => 'admin_distribution_list_timestamp_last_member_update',
                        'timestamp_action' => TIMESTAMP - 3,
                        'structured_data' => array(
                            'user_id' => $profile_record['user_id'],
                            'account_id' => $profile_record['account_id'],
                            'asset_id' => $profile_record['profile_subscriptions_list']
                            )        
                        ) ;
                    $this->Action_Create_Cron_Task_Automation($task_values) ; 
                    $this->Set_Model_Timings('Profile.Action_Update_Profile_admin_cron_created') ;

                    }                
                
                
                } else {
                    $this->Set_Alert_Response(26) ; // Error
                    }            
            }
        
        
        return $result ; 
        }
    
    
    public function Action_Validate_Account_Auth($account_auth_input) {
        
        if (!isset($account_auth_input['auth_role_name'])) {
            if (isset($account_auth_input['auth_id'])) {
                $account_auth_input['auth_id'] = $account_auth_input['auth_id'] ; 
                } elseif (isset($account_auth_input['account_auth_id'])) {
                $account_auth_input['auth_id'] = $account_auth_input['account_auth_id'] ; 
                } elseif (isset($account_auth_input['user_auth_id'])) {
                    $account_auth_input['auth_id'] = $account_auth_input['user_auth_id'] ; 
                    }
            
            $this->Set_System_Auth_List($account_auth_input) ; 
            $submitted_role_list = $this->Get_System_Auth_List() ; 
            $account_auth_input['auth_role_name'] = $submitted_role_list[0]['auth_role_name'] ; 
            }
            
        $validation_response = array(
            'validation' => 1,
            'alert_id' => 0,
            'followup_actions' => array(),
            'new_auth_role_name' => $account_auth_input['auth_role_name'],
            'new_auth_id' => $account_auth_input['auth_id']
            ) ; 
        
        
        // Retrieve the current account owner
        $user_list_options = array(
            'override_paging' => 'yes'
            ) ;
        
        $account_auth_role_name_array = array('account_owner') ; // Level 6 - Account Owner
        $this->Set_Account_User_List($user_list_options,$account_auth_role_name_array) ; 
        $account_owner = $this->account_user_list[0] ; 
        $validation_response['current_owner'] = $account_owner ; 
        
        
        $continue_script = 1 ; 
        
        // Test to make sure the current Account Owner isn't being removed as owner, lacking a replacement
        if (($this->profile_id == $account_owner['profile_id']) AND ($account_auth_input['auth_role_name'] != 'account_owner')) {
            
            $continue_script = 0 ;
            $validation_response['validation'] = 0 ;
            $validation_response['alert_id'] = 25 ; // Cannot remove account owner w/out replacement                            
            } 
        
        
        // Test to see if THIS user is not the current Account Owner and is trying to assume that position.
        // If so, the current account owner needs to move to Account Admin position
        if (($continue_script == 1) AND ($this->profile_id != $account_owner['profile_id']) AND ($account_auth_input['auth_role_name'] == 'account_owner')) {
            
            $validation_response['followup_actions'][] = array(
                'action' => 'update_user_account_auth',
                'profile_id' => $account_owner['profile_id'],
                'auth_role_name' => 'account_admin'
                ) ; 
            
            }
        
        return $validation_response ; 
        }
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    ////////////////////// 
    
    
    public function Create_Account($account_input) {
        
        $version_options = array(
            'supported' => 'yes',
            'stable' => 'yes'
            ) ;
        
        $system_version_list = $this->Set_System_Version_List($version_options)->Get_System_Version_List() ; 
        $app_recent_stable = $system_version_list[0]['version'] ; 
        
    
        // Create account ID string. We can use this to uniquely identify accounts w/out revealing public username
        $account_id_string = $this->DB->Simple_Hash(array($account_input['account_username'],TIMESTAMP));
        
        $query_array = array(
            'table' => "accounts",
            'values' => array(
                'account_display_name' => $account_input['account_display_name'],
                'account_username' => $account_input['account_username'],
                'timestamp_created' => TIMESTAMP,
                'timestamp_created' => TIMESTAMP,
                'version' => $app_recent_stable,
                'account_id_string' => $account_id_string
                ),
            'where' => "accounts.account_username='$new_account_username'"
            );

        $account_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
        
        // Create History Entry
        $history_input = array(
            'account_id' => $account_record['insert_id'],
            'function' => 'create_account',
            'notes' => json_encode($query_array['values'])
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;
        
        return $account_record ; 
        
        }
    

    
    
    public function Create_Profile($profile_input) {
        

        $query_array = array(
            'table' => "user_profiles",
            'values' => array(
                'timestamp_profile_created' => TIMESTAMP,
                'timestamp_profile_updated' => TIMESTAMP
                )
            );

        $query_array['values']['user_id'] = $profile_input['user_id'] ; 
        $query_array['values']['account_id'] = $profile_input['account_id'] ; 
        
        if (isset($profile_input['account_auth_id'])) {
            $query_array['values']['account_auth_id'] = $profile_input['account_auth_id'] ; 
            }
        
        if (isset($profile_input['email_address_id'])) {
            $query_array['values']['email_address_id'] = $profile_input['email_address_id'] ; 
            }
        
        if (isset($profile_input['phone_number_id'])) {
            $query_array['values']['phone_number_id'] = $profile_input['phone_number_id'] ; 
            }
        
        if (isset($profile_input['first_name'])) {
            $query_array['values']['first_name'] = $profile_input['first_name'] ; 
            }
        if (isset($profile_input['last_name'])) {
            $query_array['values']['last_name'] = $profile_input['last_name'] ; 
            } 
        
        $query_array['values']['profile_display_name'] = $profile_input['first_name'].' '.$profile_input['last_name'] ;
        
        if (isset($profile_input['profile_username'])) {
            $query_array['values']['profile_username'] = $profile_input['profile_username'] ; 
            }
        if (isset($profile_input['profile_role'])) {
            $query_array['values']['profile_role'] = $profile_input['profile_role'] ; 
            }        
        
        
        $account_record = $this->DB->Query('INSERT',$query_array) ;
        $this->account_query_result = $account_record ; 
        
        // Create History Entry
        $history_input = array(
            'profile_id' => $account_record['insert_id'],
            'function' => 'create_profile',
            'notes' => json_encode($query_array['values'])
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;        
        
        return $account_record ; 
        
        }
    
    public function Retrieve_Account($account_id,$show_user) {

        // DISABLING BECAUSE THIS SEEMS REDUNDANT AND UNECESSARY...
        // Retrieve the current account owner        
//        $account_owner = new Account($this->user_id) ; 
//        $account_owner->Set_Account_ID($account_id) ; 

        
        $user_list_options = array(
            'override_paging' => 'yes'
            ) ; 


        // Show user true defines whether or not to pull the associated user profile w/ the account
        switch ($show_user) {
            case 'yes':
                $query_array = array(
                    'table' => 'account_auth',
                    'join_tables' => array(),
                    'fields' => "accounts.*, user_profiles.*, 
                        account_billing.subscription_id, account_billing.subscription_item_id, account_billing.plan_id, account_billing.coupon_id, 
                            account_billing.timestamp_billing_period_end, account_billing.timestamp_cancel_at, account_billing.billing_status, 
                        system_billing_status_levels.billing_status_order, 
                        organizations.organization_title, organizations.organization_name, 
                        system_billing_products.product_id, system_billing_products.product_name, 
                        system_billing_products.monthly_marketing_credits, system_billing_products.registration_marketing_credits, system_billing_products.max_marketing_credits, 
                        system_billing_plans.product_id, system_billing_plans.plan_name, 
                        user_emails.email_address, user_emails.email_verified, 
                        user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                        user_profiles.first_name, user_profiles.last_name, users.city, users.state, users.country_id, list_countries.country_value",
                    'where' => "account_auth.account_id='$account_id' AND account_auth.user_id='$this->user_id'"
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'accounts',
                    'on' => 'account_auth.account_id',
                    'match' => 'accounts.account_id'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'account_billing',
                    'on' => 'account_billing.account_id',
                    'match' => 'accounts.account_id',
                    'type' => 'left'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'system_billing_status_levels',
                    'on' => 'system_billing_status_levels.billing_status_name',
                    'match' => 'account_billing.billing_status',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'system_billing_plans',
                    'on' => 'system_billing_plans.plan_id',
                    'match' => 'account_billing.plan_id',
                    'type' => 'left'
                    );                  

                $query_array['join_tables'][] = array(
                    'table' => 'system_billing_products',
                    'on' => 'system_billing_products.product_id',
                    'match' => 'system_billing_plans.product_id',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'users',
                    'on' => 'account_auth.user_id',
                    'match' => 'users.user_id'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'list_countries',
                    'on' => 'users.country_id',
                    'match' => 'list_countries.country_id'
                    );                
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_profiles',
                    'on' => 'account_auth.account_auth_id',
                    'match' => 'user_profiles.account_auth_id'
                    ); 
                $query_array['join_tables'][] = array(
                    'table' => 'user_emails',
                    'on' => 'user_profiles.email_address_id',
                    'match' => 'user_emails.email_address_id'
                    );                

                $query_array['join_tables'][] = array(
                    'table' => 'user_phone_numbers',
                    'on' => 'user_profiles.phone_number_id',
                    'match' => 'user_phone_numbers.phone_number_id',
                    'type' => 'left'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'organizations',
                    'on' => 'organizations.organization_id',
                    'match' => 'accounts.organization_id',
                    'type' => 'left'
                    );
                
                $result = $this->DB->Query('SELECT_JOIN',$query_array) ; 
                
                
                break ;
            case 'no':    
            default:
                $query_array = array(
                    'table' => 'accounts',
                    'fields' => "accounts.*, 
                        organizations.organization_title, organizations.organization_name, 
                        account_billing.subscription_id, account_billing.subscription_item_id, account_billing.plan_id, account_billing.coupon_id, 
                            account_billing.timestamp_billing_period_end, account_billing.timestamp_cancel_at, account_billing.billing_status, 
                        system_billing_status_levels.billing_status_order,
                        system_billing_products.product_id, system_billing_products.product_name, 
                        system_billing_products.monthly_marketing_credits, system_billing_products.registration_marketing_credits, system_billing_products.max_marketing_credits, 
                        system_billing_plans.product_id, system_billing_plans.plan_name",
                    'where' => "accounts.account_id='$account_id'"
                    ); 

                $query_array['join_tables'][] = array(
                    'table' => 'account_billing',
                    'on' => 'account_billing.account_id',
                    'match' => 'accounts.account_id',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'system_billing_status_levels',
                    'on' => 'system_billing_status_levels.billing_status_name',
                    'match' => 'account_billing.billing_status',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'system_billing_plans',
                    'on' => 'system_billing_plans.plan_id',
                    'match' => 'account_billing.plan_id',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'system_billing_products',
                    'on' => 'system_billing_products.product_id',
                    'match' => 'system_billing_plans.product_id',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'organizations',
                    'on' => 'organizations.organization_id',
                    'match' => 'accounts.organization_id',
                    'type' => 'left'
                    );
                
                $result = $this->DB->Query('SELECT_JOIN',$query_array) ;
 
            }

                
        $this->account_query_result = $result ;        
        
//        $this->test_data = $result ;        
        return $result ;         
        }
    
    
    
    
    public function Retrieve_Account_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        
        $query_array = array(
            'table' => 'accounts',
            'fields' => "accounts.*, 
                account_billing.subscription_id, account_billing.subscription_item_id, account_billing.plan_id, account_billing.timestamp_billing_period_end, account_billing.timestamp_cancel_at, account_billing.billing_status, 
                system_billing_products.monthly_marketing_credits, system_billing_products.registration_marketing_credits, system_billing_products.max_marketing_credits, 
                system_billing_plans.product_id, 
                system_billing_plans.amount, system_billing_plans.billing_interval, system_billing_plans.billing_interval_count",
            'where' => "accounts.account_id>0 ",
            'group_by' => "accounts.account_id"
            );
        
        
        switch($query_options->filter_by_user_auth) {
            case 'yes':    
                $query_array['join_tables'][] = array(
                    'table' => 'user_profiles',
                    'on' => 'user_profiles.account_id',
                    'match' => 'accounts.account_id'
                    );
                $query_array['where'] .= "AND user_profiles.user_id='$this->user_id'" ;
                break ;
            case 'inactive': // Excludes any accounts for which the active user_id is already a member
                $query_array['join_tables'][] = array(
                    'table' => "(
                        SELECT account_auth.*   
                        FROM account_auth
                        WHERE account_auth.user_id='$this->user_id' 
                        ) account_auth",
                    'on' => 'account_auth.account_id',
                    'match' => 'accounts.account_id',
                    'type' => 'left'
                    );

                $query_array['where'] .= "AND account_auth.user_id IS NULL" ; 
                break ;
            case 'no': 
            default:    
            }
        
        

        // Include billing tables as a left join
        $query_array['join_tables'][] = array(
            'table' => 'account_billing',
            'on' => 'account_billing.account_id',
            'match' => 'accounts.account_id',
            'type' => 'left'
            ); 

        $query_array['join_tables'][] = array(
            'table' => 'system_billing_plans',
            'on' => 'system_billing_plans.plan_id',
            'match' => 'account_billing.plan_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_billing_products',
            'on' => 'system_billing_products.product_id',
            'match' => 'system_billing_plans.product_id',
            'type' => 'left'
            );        
    
        
        
        // Account Owner subquery    
        $subquery_account_owner_input = array(
            'skip_query' => 1,
            'table' => 'user_profiles',
            'fields' => "
                CONCAT(user_profiles.first_name,' ',user_profiles.last_name) AS account_owner_display_name, 
                accounts.*, 
                system_auth_roles.*,
                user_phone_numbers.phone_country_dialing_code AS phone_country_dialing_code_account_owner, 
                user_phone_numbers.phone_number AS phone_number_account_owner, 
                user_phone_numbers.phone_verified, 
                user_emails.email_address AS email_address_account_owner, 
                user_emails.email_verified, 
                users.user_id, 
                users.city, 
                users.state, 
                users.country_id, 
                list_countries.country_value,
                user_profiles.profile_id AS account_owner_profile_id,
                user_profiles.timestamp_profile_updated
                ",
            'where' => "system_auth_roles.auth_role_name='account_owner'"
            );       

        
        $subquery_account_owner_input['join_tables'][] = array(
            'table' => 'users',
            'on' => 'users.user_id',
            'match' => 'user_profiles.user_id'
            );
        
        $subquery_account_owner_input['join_tables'][] = array(
            'table' => 'accounts',
            'on' => 'accounts.account_id',
            'match' => 'user_profiles.account_id'
            ); 
        
        $subquery_account_owner_input['join_tables'][] = array(
            'table' => 'system_auth_roles',
            'on' => 'system_auth_roles.auth_id',
            'match' => 'user_profiles.account_auth_id'
            ); 
        
        $subquery_account_owner_input['join_tables'][] = array(
            'table' => 'user_emails',
            'on' => 'user_emails.email_address_id',
            'match' => 'user_profiles.email_address_id',
            'type' => 'left'
            );
        
        $subquery_account_owner_input['join_tables'][] = array(
            'table' => 'user_phone_numbers',
            'on' => 'user_phone_numbers.phone_number_id',
            'match' => 'user_profiles.phone_number_id',
            'type' => 'left'
            );

        $subquery_account_owner_input['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'users.country_id',
            'match' => 'list_countries.country_id'
            );        
        

        $subquery_account_owner_result = $this->DB->Query('SELECT_JOIN',$subquery_account_owner_input);        
        $subquery_account_owner = $subquery_account_owner_result['query'] ; 

        $subquery_account_owner_statement = "(".$subquery_account_owner.") AS account_owner_profile " ;        

        $query_array['join_tables'][] = array(
            'table' => $subquery_account_owner_statement,
            'on' => 'account_owner_profile.account_id',
            'match' => 'accounts.account_id',
            'type' => 'left'
            );                

        $query_array['fields'] .= " 
            , account_owner_profile.account_owner_profile_id,
            account_owner_profile.account_owner_display_name, 
            account_owner_profile.phone_country_dialing_code_account_owner,
            account_owner_profile.phone_number_account_owner,
            account_owner_profile.email_address_account_owner, 
            account_owner_profile.timestamp_profile_updated, " ;
        
        // END: Account Owner subquery 
        
        
        
        // Timestamp of the date the account was created before
        if (isset($query_options->filter_by_search_parameter)) {
            $query_array['where'] .= "AND (
                accounts.account_username LIKE '%$query_options->filter_by_search_parameter%' OR 
                accounts.account_display_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                accounts.account_tagline LIKE '%$query_options->filter_by_search_parameter%' OR 
                accounts.account_id LIKE '%$query_options->filter_by_search_parameter%' OR 
                accounts.billing_id LIKE '%$query_options->filter_by_search_parameter%' OR 
                account_owner_profile.account_owner_display_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                account_owner_profile.email_address_account_owner LIKE '%$query_options->filter_by_search_parameter%'
                )" ;               
            
            }
        
        
        // Timestamp of the date the account was created before
        if (isset($query_options->created_before)) {
            $query_array['where'] .= "AND accounts.timestamp_created<='$query_options->created_before'" ;            
            }
        
        // Timestamp of the date the account was created before
        if (isset($query_options->created_after)) {
            $query_array['where'] .= "AND accounts.timestamp_created>='$query_options->created_after'" ;            
            }
                
        if ($query_options->filter_by_account_id) {
 
            $account_query_string = '' ; 
            $query_options->filter_by_account_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_account_id) ;
            foreach ($query_options->filter_by_account_id as $this_account_id) {
                $account_query_string .= "accounts.account_id='$this_account_id' OR " ; 
                }
            $account_query_string = rtrim($account_query_string," OR ") ;                        
            
            $query_array['where'] .= "AND ($account_query_string) " ;
            }        
        
        
        if ($query_options->filter_by_account_public_domain_id) {
 
            $account_public_domain_search_string = '"account_public_domain_id":"'.$query_options->filter_by_account_public_domain_id.'"' ; 
            
            $account_domain_query_string= "accounts.structured_data_account_01 LIKE '%$account_public_domain_search_string%' " ; 
                
            $query_array['where'] .= "AND ($account_domain_query_string) " ;
            }
        
        
        // Scrub for whether or not the plan the account is on is paid
        if (isset($query_options->paid_account)) {
            switch ($query_options->paid_account) {
                case 0: // Is free
                    $query_array['where'] .= "AND system_billing_plans.amount=0" ; 
                    break ;
                case 1: // Is paid  
                    $query_array['where'] .= "AND system_billing_plans.amount>0" ; 
                    break ;   
                }
            }
        
        

        // Account billing status filter
        if (isset($query_options->filter_by_account_billing_status)) {
            $query_options->filter_by_account_billing_status = Utilities::Process_Comma_Separated_String($query_options->filter_by_account_billing_status) ;

            foreach ($query_options->filter_by_account_billing_status as $account_billing_status) {
                $account_billing_status_query_string .= "account_billing.billing_status='$account_billing_status' OR " ; 
                }

            $account_billing_status_query_string = rtrim($account_billing_status_query_string," OR ") ;
            $query_array['where'] .= ' AND ('.$account_billing_status_query_string.') ' ;            
            }
        
        
        // Timestamp of the date the account was created before
        if (isset($query_options->filter_by_marketing_autodelivery)) {
            switch ($query_options->filter_by_marketing_autodelivery) {
                case 'yes':
                    $query_array['where'] .= " AND accounts.marketing_autodelivery='1'" ;
                    break ;
                case 'no':
                    $query_array['where'] .= " AND accounts.marketing_autodelivery='0'" ;
                    break ;
                }
            }
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        

        // Add paging constraints
//        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
//        $query_options = $paging_constraints['query_options'] ;
//        $query_array = $paging_constraints['query_array'] ;   
//            
//
//        // Pull visible results
//        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
//        $this->account_query_result =  $result ; 
//        
//        
//        // Pull total results
//        $query_options->value_name = 'account_id' ; 
//        $query_array['fields'] = "COUNT(accounts.$query_options->value_name) as total_count, accounts.$query_options->value_name" ; 
//        $result = $this->Retrieve_Total_Results($result,$query_array,$query_options) ; 
//        
//        $this->Set_Account_Paging($result) ;  

        
        

        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
 
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');

        $query_options->value_name = 'account_id' ; 
        
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Account_Paging($result) ;         
        $this->account_query_result = $result ;        
        
        
        return $result ; 
        }
    
    
    
    
    
    public function Retrieve_Account_Subscriptions_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $query_array = array(
            'table' => 'account_billing',
            'fields' => "account_billing.*, accounts.billing_id",
            'where' => "account_billing.account_id='$this->account_id' "
            );               
        
        // Match billing table with parent account
        $query_array['join_tables'][] = array(
            'table' => 'accounts',
            'on' => 'accounts.account_id',
            'match' => 'account_billing.account_id'
            );         
        
        if (isset($query_options->primary_subscription)) {
            $query_array['where'] .= " AND account_billing.primary_subscription='$query_options->primary_subscription' " ; 
            }
              
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        return $result ; 
        }
    
    
    
    
    
    public function Retrieve_Account_Users($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write


        // Base query
        $query_array = array(
            'table' => 'users',
            'join_tables' => array(),
            'fields' => "
                user_emails.email_address, user_emails.email_verified, 
                user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                users.user_id, users.timezone, users.city, users.state, 
                users.country_id, users.referring_profile_id, 
                list_countries.country_value, ",
            'where' => "",
            'order_by' => "users.user_id",
            'order' => "ASC",
            'group_by' => "users.user_id"
            );

        
        // Match users with country
        $query_array['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'users.country_id',
            'match' => 'list_countries.country_id',
            'type' => 'left'
            ); 
        

        
        
        // User Auth Role subquery    
        $subquery_user_auth_role_input = array(
            'skip_query' => 1,
            'table' => 'system_auth_roles',
            'fields' => "
                system_auth_roles.auth_id AS user_auth_id, 
                system_auth_roles.auth_role_name AS user_auth_role_name, 
                system_auth_roles.auth_role_title AS user_auth_role_title, 
                system_auth_roles.auth_role_description AS user_auth_role_description,
                system_auth_roles.auth_role_permissions AS user_auth_role_permissions
                "
            );       

        $subquery_user_auth_role_result = $this->DB->Query('SELECT',$subquery_user_auth_role_input);        
        $subquery_user_auth_role = $subquery_user_auth_role_result['query'] ; 

        $sub_user_auth_role_statement = "(".$subquery_user_auth_role.") AS user_auth_role " ;        

        $query_array['join_tables'][] = array(
            'table' => $sub_user_auth_role_statement,
            'on' => 'user_auth_role.user_auth_id',
            'match' => 'users.user_auth_id'
            );                

        $query_array['fields'] .= " 
            user_auth_role.user_auth_id,
            user_auth_role.user_auth_role_name,
            user_auth_role.user_auth_role_title,
            user_auth_role.user_auth_role_description,
            user_auth_role.user_auth_role_permissions, " ;
        
        // END: User Auth Role subquery    
        
        
        
        // Determine whether or not results should be restricted to a specific account_id
        // Default is to restrict to $this->account_id
        switch ($query_options->filter_by_account_id) {
            case 'no':
                $record_type = 'user_record' ;
                $query_array['where'] .= "users.user_id>0 " ; 
                $query_array['group_by'] = "users.user_id" ;    
                break ;
            case 'profile':
                $record_type = 'profile_record' ;
                $query_array['where'] .= "user_profiles.profile_id>0 " ; 
                $query_array['group_by'] = "user_profiles.profile_id" ;    
                break ; 
            case 'yes':
            default:
                $record_type = 'profile_record' ;
                $query_array['where'] .= "user_profiles.account_id='$this->account_id' " ;      
                break ;      
            }
        
        
        // If we're pulling only users, then pull user level record
        // If pulling account users or a global list of profiles, pull profile records
        switch ($record_type) {
            case 'user_record':
                
                $query_array['fields'] .= "
                    users.first_name, users.last_name, 
                    users.email_address_id, users.phone_number_id, 
                    users.timestamp_created, users.timestamp_updated, " ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_emails',
                    'on' => 'user_emails.email_address_id',
                    'match' => 'users.email_address_id',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_phone_numbers',
                    'on' => 'user_phone_numbers.phone_number_id',
                    'match' => 'users.phone_number_id',
                    'type' => 'left'
                    );                 
                
                break ;
            case 'profile_record':
                
                $query_array['fields'] .= "
                    accounts.account_display_name, accounts.account_username, 
                    accounts.timestamp_created, accounts.timestamp_updated, 
                    accounts.marketing_credit_balance, 
                    account_billing.*, 
                    user_profiles.*, " ; 
                
                // NOTE: user_profiles and user_emails is a left join (so values will be set to null if a profile / email address doesn't exist)
                // ????
                $query_array['join_tables'][] = array(
                    'table' => 'user_profiles',
                    'on' => 'user_profiles.user_id',
                    'match' => 'users.user_id'
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'accounts',
                    'on' => 'accounts.account_id',
                    'match' => 'user_profiles.account_id'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'account_billing',
                    'on' => 'account_billing.account_id',
                    'match' => 'accounts.account_id',
                    'type' => 'left'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_emails',
                    'on' => 'user_profiles.email_address_id',
                    'match' => 'user_emails.email_address_id',
                    'type' => 'left'
                    );                 
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_phone_numbers',
                    'on' => 'user_phone_numbers.phone_number_id',
                    'match' => 'users.phone_number_id',
                    'type' => 'left'
                    ); 
                

                // Profile Auth Role subquery    
                $subquery_account_auth_role_input = array(
                    'skip_query' => 1,
                    'table' => 'system_auth_roles',
                    'fields' => "
                        system_auth_roles.auth_id AS account_auth_id, 
                        system_auth_roles.auth_role_name AS account_auth_role_name, 
                        system_auth_roles.auth_role_title AS account_auth_role_title, 
                        system_auth_roles.auth_role_description AS account_auth_role_description,
                        system_auth_roles.auth_role_permissions AS account_auth_role_permissions
                        "
                    );       


                $subquery_account_auth_role_result = $this->DB->Query('SELECT',$subquery_account_auth_role_input);        
                $subquery_account_auth_role = $subquery_account_auth_role_result['query'] ; 

                $sub_account_auth_role_statement = "(".$subquery_account_auth_role.") AS account_auth_role " ;        

                $query_array['join_tables'][] = array(
                    'table' => $sub_account_auth_role_statement,
                    'on' => 'account_auth_role.account_auth_id',
                    'match' => 'user_profiles.account_auth_id'
                    );                
                
                $query_array['fields'] .= " 
                    account_auth_role.account_auth_id,
                    account_auth_role.account_auth_role_name,
                    account_auth_role.account_auth_role_title,
                    account_auth_role.account_auth_role_description,
                    account_auth_role.account_auth_role_permissions, " ;
                     
            }

        
        
        
        
        // This part of the query is problematic
        // The 'default' option is filter based off of account_auth, which I suppose is needed to bring up a specific profile in some views
        // The 'list' option is based off of users.user_id and is forcing an array on user_id input. This is similar to Contacts search filters, but there
        // are differences because of the Profiles element.  Same for 'yes' option.
        if (isset($query_options->filter_by_user_id)) {
            switch ($query_options->filter_by_user_id) {
                case 'no':
                    // blank
                    break ;   
                case 'yes':                 
                    $query_array['where'] .= " AND users.user_id='$this->user_id' " ;
                    break ; 
                case 'list':

                    $user_id_array = Utilities::Array_Force($query_options->user_id) ; 

                    $user_id_query_string = '' ; 
                    foreach ($user_id_array as $user_id) {

                        $user_id_query_string .= "users.user_id='$user_id' OR " ; 
                        }
                    $user_id_query_string = rtrim($user_id_query_string," OR ") ; 
                    $query_array['where'] .= "AND ($user_id_query_string) " ;
                    break ;
                default:                
                    $query_array['where'] .= " AND user_profiles.user_id='$query_options->filter_by_user_id' " ;
                    break ;                    
                }
            }
            
        
        // Filter by profile_id
        if (isset($query_options->filter_by_profile_id)) {
            switch ($query_options->filter_by_profile_id) {
                case 'no':
                    // blank
                    break ;   
                case 'yes':                 
                    $query_array['where'] .= " AND user_profiles.profile_id='$this->filter_by_profile_id' " ;
                    break ; 
                case 'list':

                    $profile_id_array = Utilities::Array_Force($query_options->profile_id) ; 

                    $profile_id_query_string = '' ; 
                    foreach ($profile_id_array as $profile_id) {

                        $profile_id_query_string .= "user_profiles.profile_id='$profile_id' OR " ; 
                        }
                    $profile_id_query_string = rtrim($profile_id_query_string," OR ") ; 
                    $query_array['where'] .= "AND ($profile_id_query_string) " ;
                    break ;                    
                }
            }        
        
        
        // Filter by tags
        if (isset($query_options->filter_by_tag_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_tag_metadata_id,","),",") ;
            $query_options->filter_by_tag_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;
            $count_metadata = count($query_options->filter_by_tag_metadata_id) ; 
                        
            $join_table_name = 'tag_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->tag_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            switch ($record_type) {
                case 'user_record':
                    $parent_table_name = 'users' ;        
                    $parent_record_key = 'user_id' ;      
                    $relationship_type = 'user' ;        
                    break ; 
                case 'profile_record':
                    $parent_table_name = 'user_profiles' ;        
                    $parent_record_key = 'profile_id' ;
                    $relationship_type = 'profile' ;     
                    break ;                     
                }
            
            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='$relationship_type' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        ($parent_table_name.$parent_record_key=$join_table_name.item_id)"
                );                         
            }
        
        
        // Filter by categories
        if (isset($query_options->filter_by_category_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_category_metadata_id,","),",") ;
            $query_options->filter_by_category_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
            $count_metadata = count($query_options->filter_by_category_metadata_id) ; 
                        
            $join_table_name = 'category_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->category_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            switch ($record_type) {
                case 'user_record':
                    $parent_table_name = 'users' ;        
                    $parent_record_key = 'user_id' ;      
                    $relationship_type = 'user' ;        
                    break ; 
                case 'profile_record':
                    $parent_table_name = 'user_profiles' ;        
                    $parent_record_key = 'profile_id' ;
                    $relationship_type = 'profile' ;     
                    break ;                     
                }
            
            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='$relationship_type' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        ($parent_table_name.$parent_record_key=$join_table_name.item_id)"
                );                         
            }
        
        
        
        
        // Filter by account billing status
        if (isset($query_options->filter_by_account_billing_status)) {
            $query_options->filter_by_account_billing_status = Utilities::Process_Comma_Separated_String($query_options->filter_by_account_billing_status) ;

            foreach ($query_options->filter_by_account_billing_status as $account_billing_status) {
                $account_billing_status_query_string .= "account_billing.billing_status='$account_billing_status' OR " ; 
                }

            $account_billing_status_query_string = rtrim($account_billing_status_query_string," OR ") ;
            $account_billing_status_query_string = rtrim($account_billing_status_query_string," AND ") ;
            $query_array['where'] .= ' AND ('.$account_billing_status_query_string.') ' ;            
            } 
        
        
        
        // Filter by marketing_credit_balance
        if ($query_options->filter_by_marketing_credit_balance == 'active') {
            
            switch ($query_options->marketing_credit_balance_filter_type) {
                case 'value_greater':
                    
                    $query_array['where'] .= " AND (accounts.marketing_credit_balance>$query_options->marketing_credit_balance_value_greater) " ;             
                    break ; 
                case 'value_less':
                    
                    $query_array['where'] .= " AND (accounts.marketing_credit_balance<$query_options->marketing_credit_balance_value_less) " ; 
            
                    break ;                    
                case 'range_between':
                    
                    $query_array['where'] .= " AND (accounts.marketing_credit_balance<$query_options->marketing_credit_balance_value_less AND accounts.marketing_credit_balance>$query_options->marketing_credit_balance_value_greater) " ; 
                    
                break ;                                        
                }             
            }
        
        
        // Filter by user phone_number
        if (isset($query_options->filter_by_user_phone_number)) {        
            switch ($query_options->filter_by_user_phone_number) {
                case 'yes':
                    $query_array['where'] .= "AND (user_phone_numbers.phone_country_dialing_code='$query_options->phone_country_dialing_code' AND user_phone_numbers.phone_number='$query_options->phone_number') " ; 
                    break ;
                default:    
                }        
            }
        
    
        
        
        
        // Add filter by search parameter
        if (isset($query_options->filter_by_search_parameter)) {
            
            // Only search profile names / accounts if searching by profiles
            switch ($record_type) {
                case 'user_record':
                    $query_array['where'] .= " AND (
                        users.user_id LIKE '%$query_options->filter_by_search_parameter%' OR 
                        users.first_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        users.last_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        user_phone_numbers.phone_number LIKE '%$query_options->filter_by_search_parameter%' OR 
                        user_emails.email_address LIKE '%$query_options->filter_by_search_parameter%' OR 
                        users.referring_profile_id LIKE '%$query_options->filter_by_search_parameter%' 
                        )" ; 
                    break ;
                case 'profile_record':
                    $query_array['where'] .= " AND (
                        users.first_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        users.last_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        user_phone_numbers.phone_number LIKE '%$query_options->filter_by_search_parameter%' OR 
                        user_emails.email_address LIKE '%$query_options->filter_by_search_parameter%' OR 
                        user_profiles.profile_id LIKE '%$query_options->filter_by_search_parameter%' OR 
                        user_profiles.first_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        user_profiles.last_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        accounts.account_username LIKE '%$query_options->filter_by_search_parameter%' OR 
                        accounts.account_display_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        users.referring_profile_id LIKE '%$query_options->filter_by_search_parameter%' 
                    )" ;              
                }        
            }
        
    
       
        
        
        // USER AUTHORIZATION FILTER
        // Add USER authorization level filter: e.g. Only get active user, pending user ... etc.
        // Submit as an array of auth_role_name strings
        if (count($query_options->user_auth_role_name_array) > 0) {   
            $user_auth_role_name_query_string = '' ; 
            foreach ($query_options->user_auth_role_name_array as $user_auth_name) {
                $user_auth_role_name_query_string .= "user_auth_role.user_auth_role_name='$user_auth_name' OR" ; 
                }
            $user_auth_role_name_query_string = rtrim($user_auth_role_name_query_string," OR ") ;
            $query_array['where'] .= " AND ($user_auth_role_name_query_string)" ;
            }
        
        // Add USER authorization level filter: Submit as comma separated auth_role_id string
        if (isset($query_options->filter_by_user_auth_id)) {
            $query_options->filter_by_user_auth_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_user_auth_id) ;
 
            foreach ($query_options->filter_by_user_auth_id as $user_auth_id) {
                $user_auth_id_query_string .= "user_auth_role.user_auth_id='$user_auth_id' OR " ; 
                }
            
            $user_auth_id_query_string = rtrim($user_auth_id_query_string," OR ") ;
            $user_auth_id_query_string = rtrim($user_auth_id_query_string," AND ") ;
            $query_array['where'] .= ' AND ('.$user_auth_id_query_string.') ' ;            
            }
        
        
        // ACCOUNT AUTHORIZATION FILTER
        // Add ACCOUNT authorization level filter: e.g. Only get members, or admins, or owner... etc.
        // Submit as an array of auth_role_name strings
        if (count($query_options->account_auth_role_name_array) > 0) {   
            $account_auth_role_name_query_string = '' ; 
            foreach ($query_options->account_auth_role_name_array as $account_auth_name) {
                $account_auth_role_name_query_string .= "account_auth_role.account_auth_role_name='$account_auth_name' OR " ; 
                }
            $account_auth_role_name_query_string = rtrim($account_auth_role_name_query_string," OR ") ;
            $query_array['where'] .= " AND ($account_auth_role_name_query_string)" ;
            }
        
        // Add ACCOUNT authorization level filter: Submit as comma separated auth_role_id string
        if (isset($query_options->filter_by_account_auth_id)) {
            $query_options->filter_by_account_auth_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_account_auth_id) ;
 
            foreach ($query_options->filter_by_account_auth_id as $account_auth_id) {
                $account_auth_id_query_string .= "account_auth_role.account_auth_id='$account_auth_id' OR " ; 
                }
            
            $account_auth_id_query_string = rtrim($account_auth_id_query_string," OR ") ;
            $account_auth_id_query_string = rtrim($account_auth_id_query_string," AND ") ;
            $query_array['where'] .= ' AND ('.$account_auth_id_query_string.') ' ;            
            }

        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
 
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');

        
        switch ($record_type) {
            case 'user_record':
                $query_options->value_name = 'user_id' ; 
                break ;                    
            case 'profile_record':                    
                $query_options->value_name = 'profile_id' ; 
                break ; 
            }
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Account_Paging($result) ;         
        $this->account_query_result = $result ;
        
        
        return $result ;         
        }
    
    

    
    public function Retrieve_Product_Feature_Set_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => 'system_feature_sets',
            'fields' => "system_feature_sets.*",
            'where' => "system_feature_sets.feature_set_id>'0' "
            );               
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        if (isset($query_options->filter_by_feature_set_id)) { // This needs to be an array of feature set IDs            
            $feature_set_query_string = '' ; 
            foreach ($query_options->filter_by_feature_set_id as $feature_set_id) {
                $feature_set_query_string .= "system_feature_sets.feature_set_id='$feature_set_id' OR " ; 
                }
            $feature_set_query_string = rtrim($feature_set_query_string," OR ") ;
            $query_array['where'] .= " AND ($feature_set_query_string) " ; 
            }
        
        
        $result = $this->DB->Query('SELECT',$query_array,'force');        
        return $result ; 
        }
    
    
    public function Retrieve_Product_Feature_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => 'system_features',
            'fields' => "system_features.*, 
                system_feature_sets.*",
            'where' => "system_features.feature_id>'0' "
            );               
        
        $query_array['join_tables'][] = array(
            'table' => 'system_feature_sets',
            'on' => "system_feature_sets.feature_set_id",
            'match' => 'system_features.feature_set_id',
            'type' => 'left'
            );
        
        
        
        if (isset($query_options->filter_by_feature_id)) { // This needs to be an array of feature set IDs            
            $feature_query_string = '' ; 
            foreach ($query_options->filter_by_feature_id as $feature_id) {
                $feature_query_string .= "system_features.feature_id='$feature_id' OR " ; 
                }
            $feature_query_string = rtrim($feature_query_string," OR ") ;
            $query_array['where'] .= " AND ($feature_query_string) " ; 
            }
        
        if (isset($query_options->filter_by_feature_set_id)) { // This needs to be an array of feature set IDs            
            $feature_set_query_string = '' ; 
            foreach ($query_options->filter_by_feature_set_id as $feature_set_id) {
                $feature_set_query_string .= "system_feature_sets.feature_set_id='$feature_set_id' OR " ; 
                }
            $feature_set_query_string = rtrim($feature_set_query_string," OR ") ;
            $query_array['where'] .= " AND ($feature_set_query_string) " ; 
            }
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ;
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $query_options->value_name = 'feature_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ;
        
        return $result ; 
        }
    
    
    
    public function Retrieve_Product_Feature_Operations_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => 'system_features',
            'fields' => "system_features.*, 
                system_feature_sets.*,
                system_auth_operations.*",
            'where' => "system_features.feature_id>'0' "
            );               
        
        $query_array['join_tables'][] = array(
            'table' => 'system_auth_operations',
            'on' => "system_features.feature_id",
            'match' => 'system_auth_operations.feature_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_feature_sets',
            'on' => "system_feature_sets.feature_set_id",
            'match' => 'system_features.feature_set_id'
            );
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        if (isset($query_options->group_by)) {
            $query_array['group_by'] = $query_options->group_by ; 
            }
        
        if (isset($query_options->filter_by_feature_id)) {            
            $query_array['where'] .= " AND system_features.feature_id='$query_options->filter_by_feature_id' " ; 
            }
        
        if (isset($query_options->filter_by_feature_set_id)) { // This needs to be an array of feature set IDs            
            $feature_set_query_string = '' ; 
            foreach ($query_options->filter_by_feature_set_id as $feature_set_id) {
                $feature_set_query_string .= "system_features.feature_set_id='$feature_set_id' OR " ; 
                }
            $feature_set_query_string = rtrim($feature_set_query_string," OR ") ;
            $query_array['where'] .= " AND ($feature_set_query_string) " ; 
            }
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');        
        return $result ; 
        }
    
    
    
    
    public function Retrieve_Profile_List() {
        

        // Retrieve all profiles for the user
        $query_array = array(
            'table' => 'user_profiles',
            'fields' => "user_profiles.*, 
                CONCAT(user_profiles.first_name,' ',user_profiles.last_name) AS full_name, 
                accounts.*, 
                system_auth_roles.*,
                user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                user_emails.email_address, user_emails.email_verified, 
                users.user_id, users.city, users.state, users.country_id, list_countries.country_value",
            'where' => "user_profiles.user_id='$this->user_id'"
            );       
        
        $query_array['join_tables'][] = array(
            'table' => 'users',
            'on' => 'users.user_id',
            'match' => 'user_profiles.user_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'accounts',
            'on' => 'accounts.account_id',
            'match' => 'user_profiles.account_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'system_auth_roles',
            'on' => 'system_auth_roles.auth_id',
            'match' => 'user_profiles.account_auth_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'user_emails',
            'on' => 'user_emails.email_address_id',
            'match' => 'user_profiles.email_address_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_phone_numbers',
            'on' => 'user_phone_numbers.phone_number_id',
            'match' => 'user_profiles.phone_number_id',
            'type' => 'left'
            );

        $query_array['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'users.country_id',
            'match' => 'list_countries.country_id'
            ); 
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->account_query_result = $result ;
        
        return $result;        
        
        }
    
    
    public function Retrieve_Profile($profile_id) {
        

        $query_array = array(
            'table' => 'user_profiles',
            'join_tables' => array(),
                'fields' => "
                    user_profiles.*, 
                    CONCAT(user_profiles.first_name,' ',user_profiles.last_name) AS full_name, 
                    accounts.*, 
                    system_auth_roles.*,
                    user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                    user_emails.email_address, user_emails.email_verified, 
                    users.user_id, users.city, users.state, users.country_id, users.timezone, list_countries.country_value, 
                    user_messaging_numbers.messaging_number_id, user_messaging_numbers.messaging_phone_country_id, 
                    user_messaging_numbers.phone_number_messaging, user_messaging_numbers.messaging_phone_vendor_id, user_messaging_numbers.messaging_notify_service_id, user_messaging_numbers.messaging_phone_active  
                    ",
            'where' => "user_profiles.profile_id='$profile_id'"
            );


        $query_array['join_tables'][] = array(
            'table' => 'accounts',
            'on' => 'accounts.account_id',
            'match' => 'user_profiles.account_id'
            );         
        
        $query_array['join_tables'][] = array(
            'table' => 'system_auth_roles',
            'on' => 'system_auth_roles.auth_id',
            'match' => 'user_profiles.account_auth_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_emails',
            'on' => 'user_profiles.email_address_id',
            'match' => 'user_emails.email_address_id',
            'type' => 'left'
            );
            
        $query_array['join_tables'][] = array(
            'table' => 'user_phone_numbers',
            'on' => 'user_profiles.phone_number_id',
            'match' => 'user_phone_numbers.phone_number_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'users',
            'on' => 'users.user_id',
            'match' => 'user_profiles.user_id'
            );

        $query_array['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'users.country_id',
            'match' => 'list_countries.country_id'
            );                
                
        $query_array['join_tables'][] = array(
            'table' => 'user_messaging_numbers',
            'on' => 'user_profiles.messaging_number_id',
            'match' => 'user_messaging_numbers.messaging_number_id',
            'type' => 'left'
            );
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array) ;
        $this->account_query_result = $result ;
        return $result ;         
        }
    
    
    
    public function Retrieve_Messaging_Numbers_List($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        
        

        $query_array = array(
            'table' => 'user_messaging_numbers',
            'join_tables' => array(),
            'fields' => "
                user_profiles.profile_id, 
                user_messaging_numbers.*, 
                list_countries.*",
            'where' => "user_messaging_numbers.messaging_number_id>'0'"
            );

        
        $query_array['join_tables'][] = array(
            'table' => 'user_profiles',
            'on' => 'user_messaging_numbers.profile_id',
            'match' => 'user_profiles.profile_id'
            );

        $query_array['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'user_messaging_numbers.messaging_phone_country_id',
            'match' => 'list_countries.country_id'
            );                
            
        
        
        // Find set of specific asset IDs
        if (isset($query_options->filter_by_profile_id)) {
            switch ($query_options->filter_by_profile_id) {
                case 'no':
                    
                    break ;
                case 'yes':
                default:
                    $query_array['where'] .= "AND (user_messaging_numbers.profile_id='$this->profile_id') " ;                      
                }  
            }        
        
        // Filter by phone active (1) / inactive (0)
        if(isset($query_options->filter_by_messaging_phone_active)) {
            $query_array['where'] .= " AND user_messaging_numbers.messaging_phone_active='$query_options->filter_by_messaging_phone_active'"  ; 
            }
        

        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force') ;

        $this->account_query_result = $result ;
        return $result ;         
        }
    
    
    
    public function Retrieve_Profile_ID_By_User_Account($user_id = 'internal',$account_id = 'internal') {

        if ('internal' === $user_id) {
            $user_id = $this->user_id ; 
            } else {            
                $this->user_id = $user_id ;
                }
        
        if ('internal' === $account_id) {
            $account_id = $this->account_id ; 
            } else {            
                $this->account_id = $account_id ;
                }

        
        $query_array = array(
            'table' => 'user_profiles',
            'join_tables' => array(),
            'fields' => "user_profiles.profile_id",
            'where' => "user_profiles.user_id='$this->user_id' AND user_profiles.account_id='$this->account_id'"
            );            

        
        $result = $this->DB->Query('SELECT_JOIN',$query_array) ;
        $this->account_query_result = $result ;
        return $result ;         
        }
    
    
    
    
    // account_id must be set to update the record for an account
    public function Update_Account($input) {         

        $preupdate_account_record = $this->Get_Account() ;             
        
        $values_array = array() ; 
        $preupdate_values_array = array() ; 
        foreach ($input as $key => $value) {            
            $values_array[$key] = $value ;  
            $preupdate_values_array[$key] = $preupdate_account_record[$key] ; 
            }
            
        
        // Retrieve the existing metadata entries and make sure they don't get overwritten while adding new key => values
        if (isset($values_array['structured_data_account_01'])) {
            $account_record = $this->Retrieve_Account($this->account_id,'false') ; 
            
            $existing_metadata_01 = json_decode($account_record['results']['structured_data_account_01'],true) ;
            foreach ($values_array['structured_data_account_01'] as $key => $value) {
                $existing_metadata_01[$key] = $value ;         
                }
            
            $values_array['structured_data_account_01'] = json_encode($existing_metadata_01) ; 
            }
        
        if (isset($values_array['structured_data_account_02'])) {
            $account_record = $this->Retrieve_Account($this->account_id,'false') ; 
            
            $existing_metadata_02 = json_decode($account_record['results']['structured_data_account_02'],true) ;
            foreach ($values_array['structured_data_account_02'] as $key => $value) {
                $existing_metadata_02[$key] = $value ;         
                }
            
            $values_array['structured_data_account_02'] = json_encode($existing_metadata_02) ; 
            }
        
        
        // Updates the new account account in the database
        $query_array = array(
            'table' => 'accounts',
            'values' => $values_array,
            'where' => "account_id='$this->account_id'"
            );

        $query_array['values']['timestamp_updated'] = TIMESTAMP ; 

        $result = $this->DB->Query('UPDATE',$query_array) ;
//        $this->test_data = $result ; 

        if (!$result['error']) {
            
            // Create History Entry
            $history_input = array(
                'account_id' => $this->account_id,
                'function' => 'update_account',
                'notes' => json_encode(array('update_values' => $query_array['values'],'preupdate_values' => $preupdate_values_array))
                ) ; 
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;
            } else {
                $this->Set_Alert_Response(19) ; // Account update failed            
                }
        
        return $result ; 
        
        }
    

    
    public function Update_Active_Account($new_account_id) {
        
        
        
        // Updates the new account account in the database
        $query_array = array(
            'table' => 'users',
            'values' => array(
                'active_account_id' => $new_account_id
                ),
            'where' => "user_id='$this->user_id'"
            );
            
        $this->account_query_result = $this->DB->Query('UPDATE',$query_array);
        
        $history_input = array(
            'function' => 'update_user',
            'notes' => json_encode($query_array['values'])
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ; 
        
        return $this;   
        
        } 
    
    public function Update_Profile($input) {

        $values_array = array() ; 
        foreach ($input as $key => $value) {            
            $values_array[$key] = $value ;             
            }
            
        
        // Retrieve the existing metadata entries and make sure they don't get overwritten while adding new key => values
        if (isset($values_array['structured_data_profile_01'])) {
            $profile_record = $this->Retrieve_Profile($this->profile_id) ; 
            
            $existing_metadata_01 = json_decode($profile_record['results']['structured_data_profile_01'],true) ;
            foreach ($values_array['structured_data_profile_01'] as $key => $value) {
                $existing_metadata_01[$key] = $value ;         
                }
            
            $values_array['structured_data_profile_01'] = json_encode($existing_metadata_01) ; 
            }        
        
        
        
        $values_array['timestamp_profile_updated'] = TIMESTAMP ; 
        
        // Updates the new account account in the database
        $query_array = array(
            'table' => 'user_profiles',
            'values' => $values_array,
            'where' => "profile_id='$this->profile_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array); 

        
        $history_input = array(
            'profile_id' => $this->profile_id,
            'function' => 'update_profile',
            'notes' => json_encode($values_array)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        } 
    
    
    
    
    }


