<?php

namespace Ragnar\Ironsides ;

// THIS FILE SHOULD BE DELETED -- this is all moved into the Asset class


class Event extends Account {
    
    public $event_id ;
    public $event ;
    public $event_list ;

    public $event_attendees_list ; // This will be an array of attending, not_attending, maybe_attending, invited
    public $event_history_list ; 
    
    public $event_create_set ; 
    
    public $page_increment = 50 ; // # of contact items to be pulled per page
    public $event_paging ; 
    
    public $event_event_set ; 
    public $event_query_result ;     
    
    
    public function __construct($user_id = 'ignore') {

        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            } 

        }
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    

    
    
    // Set event_id
    public function Set_Event_ID($event_id) {
        
        $this->event_id = $event_id ; 
        return $this ; 
        
        }
    
    
    // Set top level event info
    public function Set_Event($event_id = 'internal',$query_options = array()) {
        
        if ('internal' === $event_id) {
            
            } else {
                $this->event_id = $event_id ;      
                } 
        
        $query_options['filter_by_event_id'] = 'yes' ; 
        
        $result = $this->Retrieve_Event_List($query_options) ;         
        
        if ($result['result_count'] == 0) {
            $this->event = 'error' ; 
            } else {

                // Get event image
                if ($result['results'][0]['event_image_id'] > 0) {
                    $asset = new Asset() ; 
                    $asset->Set_Asset($result['results'][0]['event_image_id']) ; 
                    $result['results'][0]['event_image'] = $asset->Get_Asset() ; 
                    } 
            
                $this->event = $this->Action_Compile_Event($result['results'][0]) ; 
                }

        return $this ;         
        }    
    
    
    public function Set_Event_By_ID($event_id,$query_options = array()) {
        
        $this->Set_Event_ID($event_id)->Set_Event($event_id,$query_options) ;         
        return $this ;         
        }
    
    
    
    
    // Set a list of events assigned to a user / account
    public function Set_Event_List($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'yes' ; // 'yes' uses the internally defined properties of profile_id to pull matching event list
            }
        if (!isset($query_options['filter_by_account_id'])) {
            $query_options['filter_by_account_id'] = 'yes' ; // 'yes' uses the internally defined properties of account_id to pull matching event list
            }
        
        
        $result = $this->Retrieve_Event_List($query_options) ; 
        
        $i = 0 ; 
        foreach ($result['results'] as $event) {
            
            $result['results'][$i] = $this->Action_Compile_Event($event) ; 
            $i++ ; 
            }
        
        $this->event_list = $result['results'];
        
        return $this ;
        
        }
    
    
    
    // Set a list of events assigned to a user / account
    public function Set_Event_Attendees($query_options = array()) {
        
        $continue = 1 ; 
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'yes' ; // 'yes' uses the internally defined properties of profile_id to pull matching event list
            }
        if (!isset($query_options['filter_by_account_id'])) {
            $query_options['filter_by_account_id'] = 'yes' ; // 'yes' uses the internally defined properties of account_id to pull matching event list
            }
        
        
        if (!$this->event_id) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            $result = $this->Retrieve_Event_Attendees($query_options) ; 

            $i = 0 ; 
            foreach ($result['results'] as $attendee) {
                $attendee = $this->Action_Time_Territorialize_Dataset($attendee) ;
                $attendee['event'] = $this->event ; 
                $result['results'][$i] = $attendee ; 
                $i++ ; 
                }
            
            $this->event_attendees_list = $result['results'] ; 
            
            }

        return $this ;
        
        }
    

    // Set the tag list array
    public function Set_Event_Tag_List($filter_array = array(
            'asset_id' => 'none'
            )) {
        
        $tag_list = $this->Retrieve_Event_Metadata_List('tag',$filter_array) ;         
        
        if ($tag_list['result_count'] == 0) {
            $this->tag_list = 'error' ; 
            } else {
                $this->tag_list = $tag_list['results'] ;
                }

        return $this ;         
        }
    
    
    
    // Set the category list array
    public function Set_Event_Category_List($filter_array = array(
            'asset_id' => 'none'
            )) {
        
        $category_list = $this->Retrieve_Event_Metadata_List('category',$filter_array) ;         
        
        if ($category_list['result_count'] == 0) {
            $this->category_list = 'error' ; 
            } else {
                $this->category_list = $category_list['results'] ;
                }

        return $this ;         
        }
    
    
    
    
    
    
    public function Set_Event_History_List($event_id = 'internal',$query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ; 
            }        
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'yes' ; // 'yes' uses the internally defined properties of profile_id to pull matching contact list
            }
        if (!isset($query_options['error'])) {
            $query_options['error'] = 'no' ; 
            }
        
        
        $continue = 1 ; 
        
        if (!$this->event_id) {
            $continue = 0 ; 
            } 
        
        if ($continue == 1) {
            
            $query_options['event_id'] = $this->event_id ;
            $result = $this->Retrieve_Event_History($query_options) ; 
            }
        
        if (!$result['error']) {
            $this->event_history_list = $result['results'] ;
            } else {
                $this->event_history_list = 'error' ; 
                }     
        
        return $this ; 
        
        }
    
    
    
    // Process a set of event results and separate into paging components to use for site navigation
    public function Set_Event_Paging($results_array) {

        if (!isset($this->event_paging)) {
            $this->event_paging = $this->Set_Default_Paging_Object() ; 
            }        
        
        if (isset($results_array['url_hash'])) {
            $this->event_paging->url_hash = '#'.$results_array['url_hash'] ; 
            }
        
        $this->event_paging->total_count = $results_array['total_count'] ; 
        
        $this->event_paging->current = Utilities::Offset_To_Start_Page($results_array['offset_page']) ; 
        $this->event_paging->last = ceil($this->event_paging->total_count / $this->page_increment) ;
        $this->event_paging->total_pages = $this->event_paging->last ;
        
        if (($this->event_paging->current - 1) < 1) {
            $this->event_paging->previous = 1 ; 
            } else {
                $this->event_paging->previous = $this->event_paging->current - 1 ; 
                }

        if (($this->event_paging->current + 1) > $this->event_paging->last) {
            $this->event_paging->next = $this->event_paging->last ; 
            } else {
                $this->event_paging->next = $this->event_paging->current + 1 ; 
                }
        
        return $this ; 
        }
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_Event() {
        
        return $this->event ;
                
        } 
    
    public function Get_Event_List() {
        
        return $this->event_list ;
                
        }     

    public function Get_Event_Attendees() {
        
        return $this->event_attendees_list ;
        }
    
    public function Get_Event_History_List() {
        
        return $this->event_history_list ;
                
        }
    
    public function Get_Event_Create_Set() {
        return $this->event_create_set ; 
        }
    
    public function Get_Event_Paging() {
        
        return $this->event_paging ;
                
        } 
    
    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    
    
    // Important to feed this method w/ country_list and other ancilary data to compile event
    public function Action_Compile_Event($event,$data = array()) {
        
        $event = $this->Action_Time_Territorialize_Dataset($event) ;
        
        // Assign the appropriate tags and categories to an asset
        $this->Set_Event_Tag_List(array('event_id' => $event['event_id']))->Set_Event_Category_List(array('event_id' => $event['event_id'])) ; 
        $event['tag_list'] = $this->Get_Tag_List() ; 
        $event['category_list'] = $this->Get_Category_List() ;         
        
        
//        if ($event['phone_number']) {
//            
//            $event['international_phone_number'] = Utilities::Format_Phone_Number($event['phone_number'],$event['country_dialing_code']) ; 
//                
//            }
            

        return $event ; 
        }
    
    
    public function Action_Compile_Event_History_List($event_id = 'internal',$query_options = array()) {
        
        $this->Set_Event_History_List($event_id,$query_options) ; 
        
        $history_list = $this->Get_Event_History_List() ; 
        
        $final_history_list = $this->Action_Process_History_List($history_list) ; // System
        
        $this->event_history_list = $final_history_list ; 
        
        return $this ; 
        }
    
    
    public function Action_Validate_Event_Authorization(
        $pass = 1,
        $master_user = array(
            'view_level' => 7,
            'edit_level' => 5
            ),
        $user_profile = array(
            'view_level' => 3,
            'edit_level' => 7
            ),
        $optional_params = array()
        ) {
        
        $test = array(
            'pass' => $pass,
            'privilege' => 'none'
            ) ; 
        
        ///// RUN ACCOUNT LEVEL TESTS /////
        if ($this->account_id == 0) {
            $test['pass'] = 0 ; 
            $test['privilege'] = 'none' ; 
            } 
        
        
        ///// IF ACCOUNT LEVEL TESTS PASS, RUN EVENT LEVEL TESTS /////
        if ($test['pass'] == 1) {

            $continue = 1 ; 
            
            if ($this->account_id != $this->event['account_id']) {
                $test['pass'] = 0 ; 
                $test['privilege'] = 'none' ; 
                $continue = 0 ;
                }
            
            if ($this->user_id == $this->event['user_id']) {
                $test['privilege'] = 'edit' ; 
                $continue = 0 ;
                }
            
            if (($continue == 1) AND ($this->event['visibility_id'] < 6)) {
                $test['pass'] = 0 ; 
                $test['privilege'] = 'none' ; 
                $continue = 0 ;
                }
            
            if (($continue == 1) AND ($this->event['visibility_id'] == 6)) {
                $test['privilege'] = 'edit' ; 
                $continue = 0 ;
                }            
     
                        
            if ($test['pass'] == 1) {
                
                $this->Set_Profile_By_User_Account($this->master_user_id) ; 
                $profile = $this->Get_Profile() ;  
                if ($profile != 'error') {

                    // Then test the profile authorization
                    if ($profile['auth_level'] < $user_profile['view_level']) {
                        $test['pass'] = 0 ; 
                        $test['privilege'] = 'none' ; 
                        }                
                    if ($profile['auth_level'] >= $user_profile['view_level']) {
                        $test['privilege'] = 'view' ; 
                        }
                    if ($profile['auth_level'] >= $user_profile['edit_level']) {
                        $test['privilege'] = 'edit' ; 
                        } 
                    } else {
                        $test['pass'] = 0 ; 
                        $test['privilege'] = 'none' ; 
                        }
                                
                }
            
            
            // Then test the global user authorization
            // If the Profile test fails, then test the global master user to see if rights there override
            if ($test['pass'] == 0) {
                $test = $this->Action_Validate_Master_User_Authorization($test,$master_user) ;  
                }

            }
            

        $results = array(
            'pass' => $test['pass'],
            'privilege' => $test['privilege'],
            'master_user' => $master_user,
            'user_profile' => $user_profile,
            'profile' => $profile
            ) ;
            
        return $results ; 
        }
    
    
    public function Action_Test_Event_Authorization($auth_name,$auth_input = array()) {
        
        $validate_result['pass'] = 1 ; // Default pass
        $validate_result['privilege'] = 'none' ;         
        $reroute_url = '/app/events' ; 
        $rights = 'view' ; 
        
        switch ($auth_name) {                
            case 'event_edit':

                $this->Set_Event_By_ID($auth_input['event_id']) ; 

                $validate_result = $this->Action_Validate_Event_Authorization($validate_result['pass'],
                    $master_user = array(
                        'view_level' => 7,
                        'edit_level' => 7
                        ),
                    $user_profile = array(
                        'view_level' => 4,
                        'edit_level' => 4
                        ),
                    $optional_params) ; 
                break ;
            case 'event_list_edit':
                
                // NOT SURE HOW TO VALIDATE THE event list yet
                
//                $validate_result = $this->Action_Validate_Event_Authorization($validate_result['pass'],
//                    $master_user = array(
//                        'view_level' => 7,
//                        'edit_level' => 7
//                        ),
//                    $user_profile = array(
//                        'view_level' => 4,
//                        'edit_level' => 4
//                        ),
//                    $optional_params) ;                 
                break ;
            default:
                $this->Action_Test_Account_Authorization($auth_name) ; 
                $validate_result = $this->Get_Authorization() ;
                $reroute_url = $validate_result['reroute_url'] ; 
                
            }
           
        
        switch ($validate_result['pass']) {
            case 1:
                $this->Set_Alert_Response(34) ; // pass
                break ; 
            case 0:
            default:
                $this->Set_Alert_Response(35) ; // fail
            }

        
        $result = $this->Get_Response() ; 
        $result['reroute_url'] = $reroute_url ; 
        $result['privilege'] = $validate_result['privilege'] ; 
        $result['test'] = $validate_result ; 
        $result['view'] = $auth_name ; 

        $this->Set_Authorization($result) ; 
                        
        return $this ; 
        }
    
    

    public function Action_Create_Event($event_input) {

        $error_set = array() ; 
        $event_set = array() ; 
        
        $event_input = Utilities::Array_Force_Multidimensional($event_input) ;
        $original_set_count = count($event_input) ; 
        
        $i = 0 ; 
        foreach ($event_input as $event) {

            $continue = 1 ; 
            
            if (($event['event_title'] == '') AND ($continue == 1)) {
                $continue = 0 ; 
                $this->Set_Alert_Response(95) ; // New contact invalid email
                }
            
            if ($continue == 1) {
                $event_set[] = $event ;                 
                }
            
            if ($continue == 0) {
                $event['error'] = $this->Get_Response() ;
                $error_set[] = $event ;                 
                }
                            
            
            $i++ ; 
            }
        
        
        $result = $this->Create_Event($event_set,$error_set) ;
        
        
        // If only one event submitted, then provide a specific result for that event
        if ($original_set_count == 1) {
            
            if ($original_set_count == $result['success_count']) { 
                
                $this->Set_Alert_Response($result['success'][0]['success']['alert_id']) ; // Contact creation failed w/ specific contact error

                $personalize['event'] = $event ;
                $this->Append_Alert_Response($personalize,array(
                    'admin_context' => json_encode($result),
                    'location' => __METHOD__
                    )) ;
                
                } else {

                    $this->Set_Alert_Response($result['error'][0]['error']['alert_id']) ; // Contact creation failed w/ specific contact error
                    
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;             

                    }             
            }        
        
        // If multiple events submitted, provide a general response based on the overall
        // success of the import w/ counts.
        if ($original_set_count > 1) {
            
            
            
            }
                
        $this->event_event_set = $result ;         
        
        return $this ; 
        }
    
    
    
    public function Action_Update_Event($event_input) {

        $result = $this->Update_Event($event_input) ;

        
        if (!$result['error']) {
            
            $this->Set_Alert_Response(98) ; // Event updated

            
            } else {
            
                $this->Set_Alert_Response(99) ; // Event update failed
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($result),
                    'location' => __METHOD__
                    )) ;             
            
                }        
        
        return $this ; 
        }
    
    

    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    //////////////////////  
    
    
    // Enables creation of multiple events at once with event set delivered as an array of events
    public function Create_Event($event_set,$error_set = array()) {
        
        global $DB ; 

        $event_set = Utilities::Array_Force_Multidimensional($event_set) ; 
            
        $result_set = array() ; 
        $result_set['submitted_count'] = count($event_set) ;        
        $result_set['success_count'] = 0 ; 
        $result_set['error_count'] = $error_set ; 
        $result_set['error_count'] = count($error_set) ; 
        
        foreach ($event_set as $event) {
            
//            $update_duplicates = $contact['update_duplicates'] ; 
//            unset($contact['update_duplicates']) ; 
            

            $event['timestamp_updated'] = TIMESTAMP ;
            $event['timestamp_created'] = array(
                'value' => TIMESTAMP,
                'statement' => "CASE WHEN timestamp_created = 0 THEN 'VALUE' ELSE timestamp_created END"
                ) ;
            
            
            if ($event['country_value']) {

                $country_value = $event['country_value'] ; 

                $query_array = array(
                    'table' => 'list_countries',
                    'fields' => "country_id",
                    'where' => "country_value='$country_value'"
                    );

                $result = $DB->Query('SELECT',$query_array);

                $event['country_id'] = $result['results']['country_id'] ; 
                unset($event['country_value']) ; 
                }

            

            $values_array = array() ; 
            foreach ($event as $key => $value) {
                $values_array[$key] = $value ; 
                }

        
            $values_array['account_id'] = $this->account_id ;
            $values_array['user_id'] = $this->user_id ; 
            
            if (!isset($this->visibility_id)) {
                $values_array['visibility_id'] = 5 ; 
                }
        
            
            
            // Initialize create query
            $query_array = array(
                'table' => "events",
                'values' => $values_array
                );
            
            
           
            $result = $DB->Query('INSERT',$query_array) ;    

            $this->event_query_result = $result ; 

            
            if (!isset($result['error'])) {

                if ($result['insert_id']) {
                    
                    $this->Set_Alert_Response(93) ; // Contact created
                    
                    // Create History Entry
                    $history_input = array(
                        'account_id' => $this->account_id,
                        'event_id' => $result['insert_id'],
                        'function' => SCRIPT_ACTION,
                        'notes' => json_encode($query_array['values'])
                        ) ; 
                    $user_history = $this->Create_User_History($this->user_id,$history_input) ; 
                
                    } else {
                        if ($update_duplicates == 'true') {
                            $this->Set_Alert_Response(70) ; // Contact updated    
                            } else {
                                $this->Set_Alert_Response(97) ; // Contact match found, not updated  
                                }                        
                        }
                    
                $result_set['success_count']++ ; 
                $result_set['success'][] = array(
                    'event_id' => $result['insert_id'],
                    'event' => $event,
                    'success' => $this->Get_Response(),
                    'result' => $result
                    ) ;                 

                } else {
                    $this->Set_Alert_Response(94) ; // General contact save error
                
                    $result_set['error_count']++ ; 
                    $result_set['error'][] = array(
                        'event' => $event,
                        'error' => $this->Get_Response(),
                        'result' => $result
                        ) ; 
                    }
                } 
            
        
        return $result_set ;      
        
        }
    

    
    public function Retrieve_Event_Attendees($query_options = array(
        'override_paging' => 'no'        
        )) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        global $DB ;
        

        $query_array = array(
            'table' => 'events',
            'fields' => "event_attendees.*, 
                contacts.*, 
                metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description
                ",
            'where' => "events.event_id='$this->event_id' "
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'event_attendees',
            'on' => 'event_attendees.event_id',
            'match' => 'events.event_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'contacts',
            'on' => 'contacts.contact_id',
            'match' => 'event_attendees.contact_id'
            );
        
        // Merge the attendee list with the status list (defined in metadata)
         $query_array['join_tables'][] = array(
            'table' => 'metadata',
            'on' => 'metadata.metadata_id',
            'match' => 'event_attendees.metadata_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            ) ;


        switch($query_options->filter_by_user_id) {                    
            case 'no':
                
                break ;   
            case 'yes':                
            default: 
                $query_array['where'] .= "AND events.user_id='$this->user_id' " ;
            }        
        
        switch($query_options->filter_by_account_id) {
            case 'no':    
                
                break ;
            case 'yes': 
            default: 
                $query_array['where'] .= "AND events.account_id='$this->account_id' " ;
            } 
        
        switch($query_options->filter_by_rsvp_status) {                    
            case 15: // attending
            case 17: // maybe attending                 
            case 16: // not attending
            case 18: // invited                
                $query_array['where'] .= "AND event_attendees.metadata_id='$query_options->filter_by_rsvp_status' " ;
                break ;                   
            default: 
                
            }        
        
        
//        
//        switch($query_options->visibility) {
//            case 'deleted':
//                $query_array['where'] .= "AND events.visibility_id='1' " ;                
//                break ;                
//            case 'private':
//                $query_array['where'] .= "AND events.visibility_id='5' " ;                
//                break ;
//            case 'shared':
//                $query_array['where'] .= "AND events.visibility_id='6' " ;                
//                break ;                
//            case 'account':                
//            default: 
//                $query_array['where'] .= "AND (events.visibility_id='5' OR events.visibility_id='6') " ;                
//                
//            }
        
        if ($query_options->filter_by_search_parameter) {
            $query_array['where'] .= "AND (contacts.first_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                contacts.last_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                contacts.email_address LIKE '%$query_options->filter_by_search_parameter%' OR 
                contacts.phone_number LIKE '%$query_options->filter_by_search_parameter%') " ; 
            }
        

        
        // Set order parameters
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = "events.$query_options->order_by" ;  
            $query_array['order'] = $query_options->order ;  
            }        
        
        
        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        
        $result = $DB->Query('SELECT_JOIN',$query_array,'force');
        $this->event_query_result = $result ; 
        
        
        // Get the total count of users in this query
        if (isset($query_options->total_count)) { 
            
            $result['total_count'] = $query_options->total_count ; 
            
            } else {                
                    
                $query_array['fields'] = "COUNT(event_attendees.attendee_id) as total_count" ; 
                unset($query_array['limit'],$query_array['offset']) ;
            
                $count = $DB->Query('SELECT_JOIN',$query_array);

                $result['total_count'] = $count['results']['total_count'] ;

                }
        
        
        $result['offset_page'] = $query_options->offset_page ;         
        $this->Set_Event_Paging($result) ;         
        
        return $result ;               
        }
    
    
    
    public function Retrieve_Event_List($query_options = array(
        'override_paging' => 'no'        
        )) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        global $DB ;
        

        $query_array = array(
            'table' => 'events',
            'fields' => "events.*",
            'where' => "events.event_id>'0' "
            );        
        
//        $query_array['join_tables'][] = array(
//            'table' => 'system_visibility_levels',
//            'on' => 'system_visibility_levels.visibility_id',
//            'match' => 'events.visibility_id'
//            );
//        
//        $query_array['join_tables'][] = array(
//            'table' => 'list_countries',
//            'on' => 'list_countries.country_id',
//            'match' => 'events.country_id',
//            'type' => 'left'
//            );
//        
        
        
        switch($query_options->filter_by_event_id) {                    
            case 'no':
                
                break ;   
            case 'yes':                 
                $query_array['where'] .= "AND events.event_id='$this->event_id' " ;
                break ; 
            }
        
        
        switch($query_options->filter_by_user_id) {                    
            case 'no':
                
                break ;   
            case 'yes':                
            default: 
                $query_array['where'] .= "AND events.user_id='$this->user_id' " ;
            }        
        
        switch($query_options->filter_by_account_id) {
            case 'no':    
                
                break ;
            case 'yes': 
            default: 
                $query_array['where'] .= "AND events.account_id='$this->account_id' " ;
            } 
        
        switch($query_options->visibility) {
            case 'deleted':
                $query_array['where'] .= "AND events.visibility_id='1' " ;                
                break ;                
            case 'private':
                $query_array['where'] .= "AND events.visibility_id='5' " ;                
                break ;
            case 'shared':
                $query_array['where'] .= "AND events.visibility_id='6' " ;                
                break ;                
            case 'account':                
            default: 
                $query_array['where'] .= "AND (events.visibility_id='5' OR events.visibility_id='6') " ;                
                
            }
        
        if ($query_options->filter_by_search_parameter) {
            $query_array['where'] .= "AND (events.event_title LIKE '%$query_options->filter_by_search_parameter%' OR 
                events.event_description LIKE '%$query_options->filter_by_search_parameter%' OR 
                events.event_location_url LIKE '%$query_options->filter_by_search_parameter%') " ; 
            }
        
        
        // Timestamp of the date the event was created before
        if (isset($query_options->created_before)) {
            $query_array['where'] .= "AND events.timestamp_created<='$query_options->created_before' " ;            
            }
        
        // Timestamp of the date the event was created after
        if (isset($query_options->created_after)) {
            $query_array['where'] .= "AND events.timestamp_created>='$query_options->created_after' " ;            
            }
        
        
        
        // Set order parameters
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = "events.$query_options->order_by" ;  
            $query_array['order'] = $query_options->order ;  
            }        
        
        
        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        
        $result = $DB->Query('SELECT',$query_array,'force');
        $this->event_query_result = $result ; 
        
        
        // Get the total count of users in this query
        if (isset($query_options->total_count)) { 
            
            $result['total_count'] = $query_options->total_count ; 
            
            } else {                
                    
                $query_array['fields'] = "COUNT(events.event_id) as total_count" ; 
                unset($query_array['limit'],$query_array['offset']) ;
            
                $count = $DB->Query('SELECT_JOIN',$query_array);

                $result['total_count'] = $count['results']['total_count'] ;

                }
        
        
        $result['offset_page'] = $query_options->offset_page ;         
        $this->Set_Event_Paging($result) ;         
        
        return $result ;       
        
        }
    
    
    
    public function Retrieve_Event_Metadata_List($metadata_type_name,$filter_array = array(
        'event_id' => 'none'
        )) {
        
        global $DB ; 
        
        $query_array = array(
            'table' => 'metadata',
            'join_tables' => array(),
            'fields' => "metadata.*",
            'where' => "(metadata.account_id='$this->account_id' OR metadata.account_id='0') AND metadata_type.metadata_type_name='$metadata_type_name'"
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            );
        
        if ($filter_array['event_id'] != 'none') {
            $event_id = $filter_array['event_id'] ; 
            $query_array['fields'] .= ", event_metadata_relationships.relationship_id, event_metadata_relationships.event_id" ; 
            $query_array['where'] .= " AND event_metadata_relationships.event_id='$event_id'" ; 
            
            $query_array['join_tables'][] = array(
                'table' => 'event_metadata_relationships',
                'on' => 'event_metadata_relationships.metadata_id',
                'match' => 'metadata.metadata_id'
                );            
            }
        
        
        // Optional filter to add for event type or event visibliity at some point...
//        if (isset($filter_array['asset_type'])) {
//            
//            $asset_type = $filter_array['asset_type'] ; 
//            $query_array['fields'] .= ", asset_metadata_relationships.relationship_id, asset_metadata_relationships.asset_id" ;
//            $query_array['where'] .= " AND asset_type.type_name='$asset_type'" ; 
//            $query_array['group_by'] = "metadata.metadata_id" ; 
//            $query_array['order_by'] = "metadata.metadata_name" ; 
//            $query_array['order'] = "ASC" ; 
//            
//            $query_array['join_tables'][] = array(
//                'table' => 'asset_metadata_relationships',
//                'on' => 'asset_metadata_relationships.metadata_id',
//                'match' => 'metadata.metadata_id'
//                ); 
//            
//            $query_array['join_tables'][] = array(
//                'table' => 'assets',
//                'on' => 'assets.asset_id',
//                'match' => 'asset_metadata_relationships.asset_id'
//                );
//            
//            $query_array['join_tables'][] = array(
//                'table' => 'asset_type',
//                'on' => 'asset_type.type_id',
//                'match' => 'assets.type_id'
//                );            
//            }
        
        $result = $DB->Query('SELECT_JOIN',$query_array,'force');
        $this->asset_query_result = $result ;
        
        return $result ;               
        }
    
    
    
    
    public function Update_Event($input) {
        
        global $DB ; 
        $values_array = array() ; 
        
        if (count($input) > 0) {
            
            foreach ($input as $key => $value) {

                $values_array[$key] = $value ; 

                }
            }
        
        // Updates the event in the database
        $query_array = array(
            'table' => 'events',
            'values' => $values_array,
            'where' => "event_id='$this->event_id'"
            );

        $result = $DB->Query('UPDATE',$query_array);
        $this->event_query_result = $result ; 
        
        if ($result['results'] == true) {
            
            $history_input = array(
                'event_id' => $this->event_id,
                'function' => 'update_event',
                'notes' => json_encode($values_array)
                ) ; 
        
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;             
            }
        
        
                
        return $result ; 
        
        }
   

    }


