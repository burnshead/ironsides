<?php

namespace Ragnar\Ironsides ;

class Contact extends Account {
    
    public $contact_id ;
    public $contact ;
    public $contact_phone_number ; 
    
    public $contact_list ;
    public $contact_full_list ; // All results within the current "page"
    public $contact_uber_list ; // All results from all pages
    
    public $contact_note_id ;
    public $contact_note ;    
    public $contact_notes_list ;    
    
    public $contact_history_list ; 
    
    public $contact_import_log ;
    public $contact_import_record ;    
    
    public $contact_create_set ; 
    
    public $page_increment = 24 ; // # of contact items to be pulled per page
    public $contact_paging ; 
    
    public $do_not_contact ;
    
    public $contact_query_result ;     
    public $contact_query_result2 ; 
    

    
    public function __construct($user_id = 'ignore') {

        global $DB ;   
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            } 

        }
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    

    
    
    public function Set_Contact_By_Phone_Number($query_parameters) {
        
        $this->Set_Contact_Phone_Number($query_parameters) ; 
        $data['contact_phone_number'] = $this->Get_Contact_Phone_Number() ; 
        
        $contact_query_options = array(
            'filter_by_contact_id' => 'no',
            'filter_by_user_id' => 'yes',
            'filter_by_phone_number' => 'yes',
            'phone_country_dialing_code' => $data['contact_phone_number']['phone_country_dialing_code'],
            'phone_number' => $data['contact_phone_number']['local']
            ) ; 
        $this->Set_Contact('internal',$contact_query_options) ; 
    

        return $this ; 
        }
    
    
    
    public function Set_Contact_Phone_Number($query_parameters) {
        
        if (isset($query_parameters['phone_number'])) {
            
            // Set the default country dialing code 
            if ($query_parameters['phone_country_dialing_code']) {
                $country_dialing_code = $query_parameters['phone_country_dialing_code'] ; 
                } elseif (!$country_dialing_code) {
                    $country_dialing_code = $this->user['country_dialing_code'] ; 
                } else {
                    $country_dialing_code = '+1' ; // Default to US if none can be provided.
                    }            
            
            $phone_number_options = array(
                'phone_country_dialing_code' => $country_dialing_code
                ) ;
            
            $this->Action_Validate_Phone_Number($query_parameters['phone_number'],$phone_number_options) ; 
            $phone_number_formatted = $this->Get_Phone_Number_Validation() ; 
            
            $this->contact_phone_number = $phone_number_formatted ; 
            } else {
                $this->contact_phone_number = 'error' ; 
                }

        return $this ; 
        }
    
    public function Get_Contact_Phone_Number() {
        
        return $this->contact_phone_number ;
        }
    
    
    // Set contact_id
    public function Set_Contact_ID($contact_id) {
        
        $this->contact_id = $contact_id ; 
        return $this ; 
        
        }
    
    
    // Set top level contact info
    public function Set_Contact($contact_id = 'internal',$query_options = array()) {
        
        if ('internal' === $contact_id) {
            
            } else {
                $this->contact_id = $contact_id ;      
                } 
        
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'no' ;     
            }        
        if (!isset($query_options['filter_by_contact_id'])) {
            $query_options['filter_by_contact_id'] = 'yes' ;     
            }
        
        
        $result = $this->Retrieve_Contact_List($query_options) ;         
        
        if ($result['result_count'] == 0) {
            $this->contact = 'error' ; 
            } else {

                $working_contact = $result['results'][0] ; 
            
                // Convert json encoded string into array of additional account data                
                $structured_data_contact_01 = json_decode($working_contact['structured_data_contact_01'],true) ; 
                if (is_array($structured_data_contact_01)) {
                    foreach ($structured_data_contact_01 as $key => $value) {
                        
                        switch ($key) {
                            case 'timestamp_yl_join_date':
                            case 'yl_enroller_name':
                            case 'yl_sponsor_name': 
                                
                                // Skip fields that now have their own SQL field
                                break ; 
                            default:    
                                $working_contact[$key] = $value ;       

                            }                        
                        }
                    }
            
                // Get contact image
                if ($working_contact['contact_image_id'] > 0) {
                    $asset = new Asset() ; 
                    $asset->Set_Asset($working_contact['contact_image_id']) ; 
                    $working_contact['contact_image'] = $asset->Get_Asset() ; 
                    } 
            
                $tag_metadata_query_options['metadata_type_id'] = 1 ; // Tags
                $tag_list = $this->Retrieve_Contact_Metadata_List('internal',$tag_metadata_query_options) ; 
                $working_contact['tag'] = $tag_list['results'] ; 
            
                $category_metadata_query_options['metadata_type_id'] = 2 ; // Categories
                $category_list = $this->Retrieve_Contact_Metadata_List('internal',$category_metadata_query_options) ; 
                $working_contact['category'] = $category_list['results'] ;             
            
            
                // Pull broadcast list subscriptions
                $working_contact['broadcast_subscriptions_list'] = array() ; 
                $subscriptions_query = array(
                    'filter_by_asset_id' => 'no',
                    'filter_by_user_id' => 'no',
                    'filter_by_account_id' => 'no',
                    'type_id' => 19,
                    'contact_id' => $working_contact['contact_id']
                    ) ;

                $broadcast = new Asset() ; 
                $broadcast->Set_Asset_Members_List($subscriptions_query) ; 
                $broadcast_subscriptions_list = $broadcast->Get_Asset_Members_List() ;
            
                foreach ($broadcast_subscriptions_list as $list_record) {   
                    $working_contact['broadcast_subscriptions_list'][] = array(
                        'asset_id' => $list_record['asset_id'],
                        'visibility_id' => $list_record['visibility_id'],
                        'type_name' => $list_record['type_name'],
                        'type_display_name' => $list_record['type_display_name'],
                        'asset_title' => $list_record['asset_title'],
                        'email_address' => $list_record['email_address'],
                        'phone_number' => $list_record['phone_number'],
                        
                        'member_id' => $list_record['member_id'],
                        'member_id_value' => $list_record['member_id_value'],
                        'metadata_id' => $list_record['metadata_id'],
                        'email_metadata_id' => $list_record['email_metadata_id'],
                        'sms_metadata_id' => $list_record['sms_metadata_id'],
                        'campaign_id' => $list_record['campaign_id'],
                        'automation_id' => $list_record['automation_id'],
                        'member_status_id' => $list_record['member_status_id'],
                        'timestamp_member_created' => $list_record['timestamp_member_created'],
                        'timestamp_member_updated' => $list_record['timestamp_member_updated'],
                        'email_metadata_name' => $list_record['email_metadata_name'],
                        'email_metadata_slug' => $list_record['email_metadata_slug'],
                        'email_metadata_description' => $list_record['email_metadata_description'],
                        'sms_metadata_name' => $list_record['sms_metadata_name'],
                        'sms_metadata_slug' => $list_record['sms_metadata_slug'],
                        'sms_metadata_description' => $list_record['sms_metadata_description'],
                        'campaign_position' => $list_record['campaign_position'],
                        'automation_status_id' => $list_record['automation_status_id'],
                        'timestamp_next_action' => $list_record['timestamp_next_action'],
                        'campaign_title' => $list_record['campaign_title'],
                        'automation_status_name' => $list_record['automation_status_name'],
                        'automation_status_title' => $list_record['automation_status_title'],
                        'automation_asset_id' => $list_record['automation_asset_id'],
                        'automation_asset_title' => $list_record['automation_asset_title'],
                        'automation_asset_type_id' => $list_record['automation_asset_type_id'],
                        'automation_asset_type_name' => $list_record['automation_asset_type_name'],
                        'automation_asset_type_display_name' => $list_record['automation_asset_type_display_name'],
                        'automation_asset_type_icon' => $list_record['automation_asset_type_icon'],
                        'member_status_name' => $list_record['member_status_name'],
                        'member_status_title' => $list_record['member_status_title'],
                        'timestamp_next_action_compiled' => $list_record['timestamp_next_action_compiled'],
                        'timestamp_member_created' => $list_record['timestamp_member_created'],
                        'timestamp_member_updated' => $list_record['timestamp_member_updated'],
                        'timestamp_member_created_compiled' => $list_record['timestamp_member_created_compiled'],
                        'timestamp_member_updated_compiled' => $list_record['timestamp_member_updated_compiled']
                        ) ; 

                    }
            
            
                // Pull series list subscriptions
                $working_contact['series_subscriptions_list'] = array() ; 
                $subscriptions_query = array(
                    'filter_by_asset_id' => 'no',
                    'filter_by_user_id' => 'no',
                    'filter_by_account_id' => 'no',
                    'type_id' => 26,
                    'contact_id' => $working_contact['contact_id']
                    ) ;

                $series = new Asset() ; 
                $series->Set_Asset_Members_List($subscriptions_query) ; 
                $series_subscriptions_list = $series->Get_Asset_Members_List() ;
            
                foreach ($series_subscriptions_list as $list_record) {
                    $working_contact['series_subscriptions_list'][] = array(
                        'asset_id' => $list_record['asset_id'],
                        'visibility_id' => $list_record['visibility_id'],
                        'type_name' => $list_record['type_name'],
                        'type_display_name' => $list_record['type_display_name'],
                        'asset_title' => $list_record['asset_title'],
                        'email_address' => $list_record['email_address'],
                        'phone_number' => $list_record['phone_number'],
                        
                        'member_id' => $list_record['member_id'],
                        'member_id_value' => $list_record['member_id_value'],
                        'metadata_id' => $list_record['metadata_id'],
                        'email_metadata_id' => $list_record['email_metadata_id'],
                        'sms_metadata_id' => $list_record['sms_metadata_id'],
                        'campaign_id' => $list_record['campaign_id'],
                        'automation_id' => $list_record['automation_id'],
                        'member_status_id' => $list_record['member_status_id'],
                        'timestamp_member_created' => $list_record['timestamp_member_created'],
                        'timestamp_member_updated' => $list_record['timestamp_member_updated'],
                        'email_metadata_name' => $list_record['email_metadata_name'],
                        'email_metadata_slug' => $list_record['email_metadata_slug'],
                        'email_metadata_description' => $list_record['email_metadata_description'],
                        'sms_metadata_name' => $list_record['sms_metadata_name'],
                        'sms_metadata_slug' => $list_record['sms_metadata_slug'],
                        'sms_metadata_description' => $list_record['sms_metadata_description'],
                        'campaign_position' => $list_record['campaign_position'],
                        'automation_status_id' => $list_record['automation_status_id'],
                        'timestamp_next_action' => $list_record['timestamp_next_action'],
                        'campaign_title' => $list_record['campaign_title'],
                        'automation_status_name' => $list_record['automation_status_name'],
                        'automation_status_title' => $list_record['automation_status_title'],
                        'automation_asset_id' => $list_record['automation_asset_id'],
                        'automation_asset_title' => $list_record['automation_asset_title'],
                        'automation_asset_type_id' => $list_record['automation_asset_type_id'],
                        'automation_asset_type_name' => $list_record['automation_asset_type_name'],
                        'automation_asset_type_display_name' => $list_record['automation_asset_type_display_name'],
                        'automation_asset_type_icon' => $list_record['automation_asset_type_icon'],
                        'member_status_name' => $list_record['member_status_name'],
                        'member_status_title' => $list_record['member_status_title'],
                        'timestamp_next_action_compiled' => $list_record['timestamp_next_action_compiled'],
                        'timestamp_member_created' => $list_record['timestamp_member_created'],
                        'timestamp_member_updated' => $list_record['timestamp_member_updated'],
                        'timestamp_member_created_compiled' => $list_record['timestamp_member_created_compiled'],
                        'timestamp_member_updated_compiled' => $list_record['timestamp_member_updated_compiled']
                        ) ;                     
                    }
            
            
            

                $contact_compiled = $this->Action_Compile_Contact($working_contact) ;
            
                if ($query_options['minify'] == 'yes') {
                    $minified_contact = array() ; 
                    foreach ($contact_compiled as $key => $value) {
                        switch ($key) {
                            case 'contact_id':
                            case 'first_name':
                            case 'last_name':
                            case 'full_name':
                            case 'email_address':
                            case 'phone_number':
                            case 'country_id':
                            case 'phone_number_compiled':
                                $minified_contact[$key] = $value ; 
                                break ; 
                            }
                        }
                    $contact_compiled = $minified_contact ; 
                    }
                
                $this->contact = $contact_compiled ; 
            
            
                }

        return $this ; 
        
        }    
    
    
    public function Set_Contact_By_ID($contact_id = 'internal',$query_options = array()) {
        
        if ('internal' === $contact_id) {
            $contact_id = $this->contact_id ; 
            } else {
                $this->contact_id = $contact_id ;      
                }
        
        $query_options['filter_by_user_id'] = 'no' ; 
        $query_options['filter_by_contact_id'] = 'yes' ;
        
        $this->Set_Contact($contact_id,$query_options) ; 
        
        return $this ; 
        
        }
    
    
    public function Set_Contact_By_Email_Address($email_address,$query_options) {
        
        $query_options['filter_by_email_address'] = $email_address ; 
        
        $result = $this->Retrieve_Contact_List($query_options) ;         
        
        if ($result['result_count'] == 0) {
            $this->contact = 'error' ; 
            } else {

                // Get contact image
                if ($result['results'][0]['contact_image_id'] > 0) {
                    $asset = new Asset() ; 
                    $asset->Set_Asset($result['results'][0]['contact_image_id']) ; 
                    $result['results'][0]['contact_image'] = $asset->Get_Asset() ; 
                    } 
            
                $this->contact = $this->Action_Compile_Contact($result['results'][0]) ;
                }

        return $this ; 
        
        }    
    
    
    // 
    public function Set_Contact_List($query_options = array()) {
        
        $this->Set_Model_Timings('Contact.Set_Contact_List_begin') ;
        
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'yes' ; // 'yes' uses the internally defined properties of profile_id to pull matching contact list
            }
        if (!isset($query_options['filter_by_account_id'])) {
            $query_options['filter_by_account_id'] = 'yes' ; // 'yes' uses the internally defined properties of account_id to pull matching contact list
            }
        if (!isset($query_options['search_contact_notes'])) {
            $query_options['search_contact_notes'] = 'no' ; // 'no' ignores contact_notes in search results; 'yes' will search notes
            }        
                    
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        
        $result = $this->Retrieve_Contact_List($query_options) ; 
        $this->contact_query_result2 = $result ; 
        $this->Set_Model_Timings('Contact.Set_Contact_List_list_retrieved') ;
        
        $system_lists = new System() ; 
        $system_lists->Set_System_List('list_usa_states')->Set_System_List('list_canada_provinces')->Set_System_List('list_countries') ; 
        $data['state_list'] = $system_lists->Get_System_List('list_usa_states') ; 
        $data['province_list'] = $system_lists->Get_System_List('list_canada_provinces') ; 
        $data['country_list'] = $system_lists->Get_System_List('list_countries') ; 
        
        $i = 0 ; 
        foreach ($result['results'] as $contact) {
            
            // Map structured data
            $structured_data_01_array = json_decode($contact['structured_data_contact_01'],true) ;
            
            if (is_array($structured_data_01_array) AND (count($structured_data_01_array) > 0)) {
                foreach ($structured_data_01_array as $structured_key => $structured_value) {

                    switch ($structured_key) {
                        case 'timestamp_yl_join_date':
                        case 'yl_enroller_name':
                        case 'yl_sponsor_name':     
                            // Skip fields that now have their own SQL field
                            break ; 
                        default:    
                            $contact[$structured_key] = $structured_value ;             
                                                    
                        }
                                
                    }
                }            
            
            
            
            if ($query_options['skip_compile_contact'] != 'yes') {
                $contact = $this->Action_Compile_Contact($contact,$data) ; 
                }
            
            $result['results'][$i] = $contact ; 
            $full_result_set_compiled .= $contact['contact_id'].',' ; 
            
            if ($query_options['skip_compile_contact'] != 'yes') {
                $tag_metadata_query_options['metadata_type_id'] = 1 ; // Tags
                $tag_list = $this->Retrieve_Contact_Metadata_List($contact['contact_id'],$tag_metadata_query_options) ; 
                $result['results'][$i]['tags'] = $tag_list['results'] ; 
                }

            $i++ ; 
            }
        
        $this->Set_Model_Timings('Contact.Set_Contact_List_contacts_compiled') ;
        
        $i = 0 ; 
        $uber_result_set_compiled = '' ; 
        foreach ($result['uber_result_set'] as $contact) {
            $uber_result_set_compiled .= $contact['value_id'].',' ; 
            }

        $full_result_set_compiled = rtrim($full_result_set_compiled,",") ;
        $uber_result_set_compiled = rtrim($uber_result_set_compiled,",") ;             

        $this->contact_full_list = $full_result_set_compiled ; // IDs only
        $this->contact_uber_list = $uber_result_set_compiled ; // IDs only        
        
        
        $this->contact_list = $result['results'];
//        $this->test_data = $query_options ; 
        
        $this->Set_Model_Timings('Contact.Set_Contact_List_complete') ;
        
        return $this ;
        }
    
    

    
    
    public function Set_Contact_Note_ID($contact_note_id) {
        
        $this->contact_note_id = $contact_note_id ; 
        
        return $this ; 
        
        }
    
    public function Set_Contact_Note_By_ID($contact_note_id) {
        
        $this->Set_Contact_Note_ID($contact_note_id)->Set_Contact_Note() ; 
        
        return $this ; 
        
        }
    
    public function Set_Contact_Note($contact_note_id = 'internal') {
        
        if ('internal' === $contact_note_id) {
            
            } else {
                $this->contact_note_id = $contact_note_id ;      
                } 
        
        $result = $this->Retrieve_Contact_Notes('internal', array(
                'note_id' => $this->contact_note_id
                )
            ) ; 
        
        if ($result['result_count'] == 1) {
            $this->contact_note = $this->Action_Time_Territorialize_Dataset($result['results'][0]) ;
            } else {
                $this->contact_note = 'error' ; 
                }        
        
        return $this ; 
        
        }
    
    
    public function Set_Contact_Notes_List($contact_id = 'internal',$query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'yes' ;
            }
        if (!isset($query_options['filter_by_account_id'])) {
            $query_options['filter_by_account_id'] = 'yes' ;
            }        
        
        if ('internal' === $contact_id) {
            $contact_id = $this->contact_id ; 
            } else {
                $this->contact_id = $contact_id ;      
                }
        
        
        $continue = 1 ; 
        
        if (!$this->contact_id) {
            $continue = 0 ; 
            } 
        
        if ($continue == 1) {
            $result = $this->Retrieve_Contact_Notes('internal',$query_options) ; 
            }
        
        if (!$result['error']) {
            $i = 0 ; 
            foreach ($result['results'] as $note) {
                $result['results'][$i] = $this->Action_Time_Territorialize_Dataset($note) ;
                $i++ ; 
                }
            $this->contact_notes_list = $result['results'] ;
            } else {
                $this->contact_notes_list = 'error' ; 
                }     
        
        return $this ; 
        }    
    
    

    
    
    public function Set_Contact_History_List($contact_id = 'internal',$query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ; 
            }        
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'yes' ; // 'yes' uses the internally defined properties of profile_id to pull matching contact list
            }
        if (!isset($query_options['error'])) {
            $query_options['error'] = 'no' ; 
            }
        
        
        $continue = 1 ; 
        
        if (!$this->contact_id) {
            $continue = 0 ; 
            } 
        
        if ($continue == 1) {
            
            $query_options['contact_id'] = $this->contact_id ;
            
            $result = $this->Retrieve_User_History($query_options) ; 
            }
        
        if (!$result['error']) {
            $this->contact_history_list = $result['results'] ;
            } else {
                $this->contact_history_list = 'error' ; 
                }     
        
        return $this ; 
        
        }
    
    
    public function Set_Contact_Import_Record($query_options = array()) {
        
        $this->Set_Contact_Import_Log($query_options) ; 
        
        $data['contact_import_log'] = $this->Get_Contact_Import_Log() ; 
        
        if (isset($data['contact_import_log'][0])) {
            $this->contact_import_record = $data['contact_import_log'][0] ; 
            } 
        
        return $this ; 
        }
    
    
    
    public function Get_Contact_Import_Record() {
        return $this->contact_import_record ; 
        }
    
    
    
    public function Set_Contact_Import_Log($query_options = array()) {
        
        $continue = 1 ; 
        $contact_import_log = array() ; 
        
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }        
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        
        if (!isset($query_options['filter_by_file_task_id'])) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            
            $result = $this->Retrieve_Contact_Import_Log($query_options) ; 
            $preserve_paging = $this->contact_paging ; 
            
            
            if ($result['result_count'] > 0) {
            
                $match_contact = new Contact($this->user_id) ; 
                $match_contact->Set_Account_By_ID($this->account_id) ; 
                
                $i = 0 ; 
                foreach ($result['results'] as $import_item) {

                    
                    if ($query_options['skip_processing'] != 'yes') {
                        $import_item['alert'] = json_decode($import_item['alert'],true) ; 
                        $import_item['submitted_contact'] = json_decode($import_item['submitted_contact'],true) ; 

                        $import_item = $this->Action_Phone_Territorialize_Dataset($import_item,$import_item['phone_country_dialing_code']) ;
                        $import_item['submitted_contact'] = $this->Action_Compile_Contact($import_item['submitted_contact']) ; 
                        if ($import_item['submitted_contact']['email_address']) {
                            $validate_email = new User() ; 
                            $import_item['email_validation'] = $validate_email->Action_Validate_Email_Address($import_item['submitted_contact']['email_address'])->Get_Email_Address_Validation() ; 
                            }

                        $import_item['submitted_contact']['contact_details'] = $this->Action_Compile_Contact($import_item['submitted_contact']['contact_details']) ; 


                        if ($import_item['duplicate_contact_id'] > 0) {
                            $import_item['duplicate_contact_record'] = $match_contact->Set_Contact_By_ID($import_item['duplicate_contact_id'])->Get_Contact() ;
                            }
                        $import_item['duplicate_logic'] = $this->Action_Duplicate_Contact_Logic($import_item['submitted_contact'],array('update_duplicates' => 'true')) ;
                        }

                    
                    $result['results'][$i] = $import_item ; 
                    $i++ ; 
                    }
                }
            
            
            
            if ($query_options['set_adjacent'] == 'yes') {
                
                $query_previous_options = $query_options ; 
                $query_previous_options['adjacent_item'] = 'previous' ; 
                $query_previous_options['order_by'] = 'import_log_id' ; 
                $query_previous_options['order'] = 'DESC' ; 
                unset($query_previous_options['set_adjacent']) ;
                $previous_result = $this->Set_Contact_Import_Log($query_previous_options)->Get_Contact_Import_Log() ; 
                $result['results'][0]['previous'] = $previous_result[0] ;
                
                $query_next_options = $query_options ; 
                $query_next_options['adjacent_item'] = 'next' ; 
                $query_next_options['order_by'] = 'import_log_id' ; 
                $query_next_options['order'] = 'ASC' ; 
                unset($query_next_options['set_adjacent']) ;
                $next_result = $this->Set_Contact_Import_Log($query_next_options)->Get_Contact_Import_Log() ; 
                $result['results'][0]['next'] = $next_result[0] ;
                }
            
            
            
            
            }
        
        
        $this->contact_import_log = $result['results'] ; 
        $this->contact_query_result = $preserve_paging ; 
        $this->contact_paging = $preserve_paging ; 
        
        return $this ; 
        }
    
       
    
    public function Action_Update_Contact_Import_Log($query_options = array()) {
        
        $continue = 1 ; 
        
        $data['import_log_record'] = $this->Get_Contact_Import_Record() ;
        
        if ($data['import_log_record']['import_log_id'] != $query_options['import_log_id']) {
            $continue = 0 ; 
            }
            
        
        if ($continue == 1) {
            
            $result = $this->Update_Contact_Import_Log($query_options) ;
            
            if (!$result['error']) {
                $this->Set_Alert_Response(73) ; // Contact note created
                
                $post_update_query_array = array(
                    'filter_by_import_log_id' => $data['import_log_record']['import_log_id'],
                    'filter_by_file_task_id' => $data['import_log_record']['file_task_id']
                    ) ; 
                
                $this->Set_Contact_Import_Record($post_update_query_array) ; 
                } else {

                    $this->Set_Alert_Response(72) ; // Error saving contact note.
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;            

                    }            
            }
        
        return $this ; 
        }
    
    
    
    public function Update_Contact_Import_Log($query_options = array()) {

        $values_array = array() ; 
        
        if (count($query_options) > 0) {

            $import_log_id_array = Utilities::Array_Force($query_options['import_log_id']) ; 
            $import_log_id_string = '' ; 
            
            foreach ($import_log_id_array as $import_log_id) {
                $import_log_id_string .= "contact_import_log.import_log_id='$import_log_id' OR " ; 
                }
            
            $import_log_id_string = rtrim($import_log_id_string," OR ") ;     
            
            // For bulk updates. Mark duplicate / error as processed
            if ($query_options['mark_processed'] == 'yes') {
                $values_array['import_status'] = array(
                    'statement' => "CASE WHEN import_status = 'duplicate_unprocessed' THEN 'duplicate_processed' WHEN import_status = 'error_unprocessed' THEN 'error_processed' ELSE import_status END"
                    ) ;             
                unset($query_options['mark_processed']) ;
                }
            
            foreach ($query_options as $key => $value) {
                $values_array[$key] = $value ; 
                }
            
            }
        
        unset($values_array['import_log_id']) ; 
        
        // Executed database update
        $query_array = array(
            'table' => 'contact_import_log',
            'values' => $values_array,
            'where' => "$import_log_id_string"
            );

        
        $result = $this->DB->Query('UPDATE',$query_array);
//        $this->contact_query_result = $query_array ;         
        
        return $result ;         
        }    
    
    
    
    
    public function Set_Page_Increment($page_increment) {
        $this->page_increment = $page_increment ; 
        return $this ; 
        }
    
    
    
    // Process a set of contact results and separate into paging components to use for site navigation
    public function Set_Contact_Paging($results_array) {

        if (!isset($this->contact_paging)) {
            $this->contact_paging = $this->Set_Default_Paging_Object() ; 
            }        

        $this->contact_paging->page_increment = $this->Get_Page_Increment() ;
        
        $this->contact_paging = $this->Action_Process_Paging($this->contact_paging,$results_array) ; 
                
        return $this ; 
        }

    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_Contact() {
        
        return $this->contact ;
                
        } 
    
    public function Get_Contact_List() {
        
        return $this->contact_list ;
                
        }     
   
    public function Get_Contact_Full_List() {
        
        return $this->contact_full_list ;
        }
    
    public function Get_Contact_Uber_List() {
        
        return $this->contact_uber_list ;
        }
    
    
    public function Get_Contact_Note_ID() {
        
        return $this->contact_note_id ;
                
        }
    
    public function Get_Contact_Note() {
        
        return $this->contact_note ;
                
        }
    
    public function Get_Contact_Notes_List() {
        
        return $this->contact_notes_list ;
                
        }
    
    public function Get_Contact_History_List() {
        
        return $this->contact_history_list ;
                
        }
    
    public function Get_Contact_Create_Set() {
        return $this->contact_create_set ; 
        }
    
    public function Get_Contact_Import_Log() {
        return $this->contact_import_log ; 
        }
    
    
    public function Get_Do_Not_Contact($retrieve = 'first') {
        
        switch ($retrieve) {
            case 'all':
                return $this->do_not_contact ;
                break ;            
            case 'first':
            default:
                return $this->do_not_contact[0] ;
            }
        
                
        }
    
    public function Get_Contact_Paging() {
        
        return $this->contact_paging ;                
        } 
    
    public function Get_Page_Increment() {
        return $this->page_increment ; 
        }    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    
    
    
    // Check to see if an email address is on the do not contact list
    public function Action_Check_Do_Not_Contact($email_address,$query_options = array()) {
        
        $query_options['filter_by_email_address'] = $email_address ; 
        
        $result = $this->Retrieve_Do_Not_Contact($query_options) ;         
        
        if ($result['result_count'] == 0) {
            $this->do_not_contact = 'error' ; 
            } else {
            
                $this->do_not_contact = $result['results'] ; 
            
                }
            
        return $this ; 
        }
    
    
    
    // Important to feed this method w/ country_list and other ancilary data to compile contact
    public function Action_Compile_Contact($contact,$data = array()) {
        
        $contact = $this->Action_Time_Territorialize_Dataset($contact) ;
        
//        error_log('Contact.Action_Compile_Contact() post time territorialize '.$contact['member_id'])  ;
                
        
        if ($contact['phone_number']) {
            
            if ($contact['phone_country_dialing_code']) {
                $country_dialing_code = $contact['phone_country_dialing_code'] ; 
                } else {
                    $country_dialing_code = $contact['country_dialing_code'] ; 
                    }
            
            $contact = $this->Action_Phone_Territorialize_Dataset($contact,$country_dialing_code) ; 
            
            }
//        error_log('Contact.Action_Compile_Contact() post phone territorialize '.$contact['member_id'])  ;
        

        // Used to validate actions based on contact id such as accessing preferences page
        if ((isset($contact['contact_id'])) AND (isset($contact['timestamp_contact_created']))) {
            $contact_token_array = array($contact['contact'],$contact['timestamp_contact_created']) ; 
            $contact['contact_token'] = $this->DB->Simple_Hash($contact_token_array) ;
            }

        return $contact ; 
        }
    
    
    public function Action_Compile_Contact_History_List($contact_id = 'internal',$query_options = array()) {
        
        $this->Set_Contact_History_List($contact_id,$query_options) ; 
        
        $history_list = $this->Get_Contact_History_List() ; 
        
        $final_history_list = $this->Action_Process_History_List($history_list) ; // System
        
        $this->contact_history_list = $final_history_list ; 
        
        return $this ; 
        }
    
    
//    public function Action_Validate_Contact_Authorization(
//        $pass = 1,
//        $master_user = array(
//            'view_level' => 7,
//            'edit_level' => 5
//            ),
//        $user_profile = array(
//            'view_level' => 3,
//            'edit_level' => 7
//            ),
//        $optional_params = array()
//        ) {
//        
//        $test = array(
//            'pass' => $pass,
//            'privilege' => 'none'
//            ) ; 
//        
//        ///// RUN ACCOUNT LEVEL TESTS /////
//        if ($this->account_id == 0) {
//            $test['pass'] = 0 ; 
//            $test['privilege'] = 'none' ; 
//            } 
//        
//        
//        ///// IF ACCOUNT LEVEL TESTS PASS, RUN CONTACT LEVEL TESTS /////
//        if ($test['pass'] == 1) {
//
//            $continue = 1 ; 
//            
//            if ($this->account_id != $this->contact['account_id']) {
//                $test['pass'] = 0 ; 
//                $test['privilege'] = 'none' ; 
//                $continue = 0 ;
//                }
//            
//            if ($this->user_id == $this->contact['user_id']) {
//                $test['privilege'] = 'edit' ; 
//                $continue = 0 ;
//                }
//            
//            if (($continue == 1) AND ($this->contact['visibility_id'] < 6)) {
//                $test['pass'] = 0 ; 
//                $test['privilege'] = 'none' ; 
//                $continue = 0 ;
//                }
//            
//            if (($continue == 1) AND ($this->contact['visibility_id'] == 6)) {
//                $test['privilege'] = 'edit' ; 
//                $continue = 0 ;
//                }            
//     
//                        
//            if ($test['pass'] == 1) {
//                
//                $this->Set_Profile_By_User_Account($this->master_user_id) ; 
//                $profile = $this->Get_Profile() ;  
//                if ($profile != 'error') {
//
//                    // Then test the profile authorization
//                    if ($profile['auth_level'] < $user_profile['view_level']) {
//                        $test['pass'] = 0 ; 
//                        $test['privilege'] = 'none' ; 
//                        }                
//                    if ($profile['auth_level'] >= $user_profile['view_level']) {
//                        $test['privilege'] = 'view' ; 
//                        }
//                    if ($profile['auth_level'] >= $user_profile['edit_level']) {
//                        $test['privilege'] = 'edit' ; 
//                        } 
//                    } else {
//                        $test['pass'] = 0 ; 
//                        $test['privilege'] = 'none' ; 
//                        }
//                                
//                }
//            
//            
//            // Then test the global user authorization
//            // If the Profile test fails, then test the global master user to see if rights there override
//            if ($test['pass'] == 0) {
//                $test = $this->Action_Validate_Master_User_Authorization($test,$master_user) ;  
//                }
//
//            }
//            
//
//        $results = array(
//            'pass' => $test['pass'],
//            'privilege' => $test['privilege'],
//            'master_user' => $master_user,
//            'user_profile' => $user_profile,
//            'profile' => $profile
//            ) ;
//            
//        return $results ; 
//        }
//    
//    
//    public function Action_Test_Contact_Authorization($auth_name,$auth_input = array()) {
//        
//        $validate_result['pass'] = 1 ; // Default pass
//        $validate_result['privilege'] = 'none' ;         
//        $reroute_url = '/app/contacts' ; 
//        $rights = 'view' ; 
//        
//        switch ($auth_name) {                
//            case 'contacts_edit':
//
//                $this->Set_Contact_By_ID($auth_input['contact_id']) ; 
//
//                $validate_result = $this->Action_Validate_Contact_Authorization($validate_result['pass'],
//                    $master_user = array(
//                        'view_level' => 7,
//                        'edit_level' => 7
//                        ),
//                    $user_profile = array(
//                        'view_level' => 4,
//                        'edit_level' => 4
//                        ),
//                    $optional_params) ; 
//                break ;
//            case 'contact_list_edit':
//                
//                // NOT SURE HOW TO VALIDATE THE contact list yet
//                
////                $validate_result = $this->Action_Validate_Contact_Authorization($validate_result['pass'],
////                    $master_user = array(
////                        'view_level' => 7,
////                        'edit_level' => 7
////                        ),
////                    $user_profile = array(
////                        'view_level' => 4,
////                        'edit_level' => 4
////                        ),
////                    $optional_params) ;                 
//                break ;
//            default:
//                $this->Action_Test_Account_Authorization($auth_name) ; 
//                $validate_result = $this->Get_Authorization() ;
//                $reroute_url = $validate_result['reroute_url'] ; 
//                
//            }
//           
//        
//        switch ($validate_result['pass']) {
//            case 1:
//                $this->Set_Alert_Response(34) ; // pass
//                break ; 
//            case 0:
//            default:
//                $this->Set_Alert_Response(35) ; // fail
//            }
//
//        
//        $result = $this->Get_Response() ; 
//        $result['reroute_url'] = $reroute_url ; 
//        $result['privilege'] = $validate_result['privilege'] ; 
//        $result['test'] = $validate_result ; 
//        $result['view'] = $auth_name ; 
//
//        $this->Set_Authorization($result) ; 
//                        
//        return $this ; 
//        }
    
    

    public function Action_Create_Contact($contact_input) {

        if ($this->user['timezone']) {
            $timezone = $this->user['timezone'] ; 
            } else {
                $timezone = $this->system_timezone ; 
                }
        
        if ((isset($contact_input['view_names'])) OR (isset($contact_input['view_item_set']))) {
            unset($contact_input['view_names'],$contact_input['view_item_set']) ;
            }
        

        // Capture metadata values...
        $yl_rank_metadata_list_options['filter_by_metadata_type'] = 'yl_rank' ; 
        $yl_rank_metadata_list_options['allow_global_metadata'] = 'ignore' ;
        $yl_rank_metadata_list_options['order_by'] = 'metadata.relative_order' ; 
        $yl_rank_metadata_list_options['order'] = 'ASC' ; 

        $this->Set_Metadata_List($yl_rank_metadata_list_options) ;          
        $data['yl_rank_metadata_list'] = $this->Get_Metadata_List() ;        
        
        $er_metadata_list_options['filter_by_metadata_type'] = 'yl_essential_rewards_status' ; 
        $er_metadata_list_options['allow_global_metadata'] = 'ignore' ;
        $er_metadata_list_options['order_by'] = 'metadata.relative_order' ; 
        $er_metadata_list_options['order'] = 'ASC' ; 

        $this->Set_Metadata_List($er_metadata_list_options) ;          
        $data['yl_essential_rewards_status_metadata_list'] = $this->Get_Metadata_List() ;
        
        
        $yl_account_status_metadata_list_options['filter_by_metadata_type'] = 'yl_account_status' ; 
        $yl_account_status_metadata_list_options['allow_global_metadata'] = 'ignore' ;
        $yl_account_status_metadata_list_options['order_by'] = 'metadata.relative_order' ; 
        $yl_account_status_metadata_list_options['order'] = 'ASC' ; 

        $this->Set_Metadata_List($yl_account_status_metadata_list_options) ;          
        $data['yl_account_status_metadata_list'] = $this->Get_Metadata_List() ;        
        
        
        $status_metadata_list_options['filter_by_metadata_type'] = 'contact_status' ; 
        $status_metadata_list_options['allow_global_metadata'] = 'ignore' ;
        $status_metadata_list_options['order_by'] = 'metadata.relative_order' ; 
        $status_metadata_list_options['order'] = 'ASC' ; 

        $this->Set_Metadata_List($status_metadata_list_options) ;          
        $data['contact_status_metadata_list'] = $this->Get_Metadata_List() ; 
        
        
        $rating_metadata_list_options['filter_by_metadata_type'] = 'contact_rating' ;
        $rating_metadata_list_options['allow_global_metadata'] = 'ignore' ;            
        $rating_metadata_list_options['order_by'] = 'metadata.metadata_name' ; 
        $rating_metadata_list_options['order'] = 'ASC' ; 

        $this->Set_Metadata_List($rating_metadata_list_options) ;   
        $data['contact_rating_metadata_list'] = $this->Get_Metadata_List() ;
        
        
        // Set result arrays for contact create
        $create_set = array() ; 
        $error_set = array() ; 
        
        
        $contact_input = Utilities::Array_Force_Multidimensional($contact_input) ;
        $original_set_count = count($contact_input) ; 
 
        
        $i = 0 ; 
        foreach ($contact_input as $contact) {

                
            $continue = 1 ; 
            
            // Set default update_duplicates to false if not set in input
            if (!isset($contact['update_duplicates'])) {
                $contact['update_duplicates'] = 'false' ;
                }
            if (!isset($contact['auto_capitalize_names'])) {
                $contact['auto_capitalize_names'] = 'false' ;
                }           
            
            
            if (isset($contact['country_unprocessed'])) {

                $data['country_list'] = $this->Get_System_List('list_countries') ; 
                if (!isset($data['country_list'])) {
                    $query_options = array(
                        'override_paging' => 'yes'
                        ) ; 
                    $this->Set_System_List('list_countries',$query_options) ; 
                    $data['country_list'] = $this->Get_System_List('list_countries') ;
                    }                
                
                foreach ($data['country_list'] as $country_map) {
                    if (strtolower($contact['country_unprocessed']) == strtolower($country_map['country_id'])) {
                        $contact['country_id'] = $country_map['country_id'] ; 
                        } else if (strtolower($contact['country_unprocessed']) == strtolower($country_map['country_value'])) {
                        $contact['country_id'] = $country_map['country_id'] ; 
                        } else if (strtolower($contact['country_unprocessed']) == strtolower($country_map['country_name'])) {
                        $contact['country_id'] = $country_map['country_id'] ; 
                        }
                    }

                unset($contact['country_unprocessed']) ;
                }
            
            // Validate incoming email_address
            if ($contact['email_address'] == 'opted-out') {
                unset($contact['email_address']) ; 
                }
            // Unset blank email addresses
            if ($contact['email_address'] == '') {
                unset($contact['email_address']) ; 
                }            
            
            // Validate incoming phone_number
            if ($contact['phone_number']) {
                
                if (!isset($contact['phone_country_dialing_code'])) {
                    if (isset($contact['country_id'])) {
                        
                        $query_parameters = array(
                            'country_id' => $contact['country_id']
                            ) ; 
                        $this->Action_Validate_Phone_Number('',$query_parameters) ;
                        $country_code_phone_validation = $this->Get_Phone_Number_Validation() ;
                        $contact['phone_country_dialing_code'] = $country_code_phone_validation['phone_country_dialing_code'] ;
                            
                        } else {
                            $contact['phone_country_dialing_code'] = $this->user['phone_country_dialing_code'] ;     
                            }
                    }                
                
                $phone_number_options = array(
                    'phone_country_dialing_code' => $contact['phone_country_dialing_code']
                    ) ;
                
                $validate = new Contact() ; 
                $validate->Action_Validate_Phone_Number($contact['phone_number'],$contact) ;                 
                $validate_phone_number = $validate->Get_Phone_Number_Validation() ; 
                
                if ($validate_phone_number['valid'] == 'no') {
                    
                    // FUTURE: Add something to the new contact history that the submitted phone number was invalid.
//                    unset($contact['phone_number'],$contact['phone_country_dialing_code']) ; 
                    $continue = 0 ; 
                    $this->Set_Alert_Response(204) ; // New contact invalid phone number
                    
                    } else {
                        
                        if (!isset($contact['phone_type'])) {
                            $contact['phone_type'] = 'home' ;
                            }
                        $contact['phone_number'] = $validate_phone_number['unformatted'] ; 
                        $contact['phone_country_dialing_code'] = $validate_phone_number['phone_country_dialing_code'] ; 
                            
                        }                
                                        
                } else {
                    unset($contact['phone_number'],$contact['phone_country_dialing_code']) ; 
                    }
        
            
            if ($contact['email_address']) {
                $validate_email_address = Utilities::Validate_Email_Address($contact['email_address']) ; 
                if ($validate_email_address['valid'] == 'no') {
                    $continue = 0 ; 
                    $this->Set_Alert_Response(96) ; // New contact invalid email
                    } else {
                        $contact['email_address'] = $validate_email_address['email_address'] ; 
                        }                
                }
            
            
            
            // Should we put duplicate contact logic here??
            
            
            
            
            
            
            if ($contact['inverted_full_name']) {                
                $full_name = explode(',',$contact['inverted_full_name']) ;
                $contact['full_name'] = $full_name[1].' '.$full_name[0] ;
                unset($contact['inverted_full_name']) ;
                }
        
            
            // Split a full name submission into first & last names
            if ($contact['full_name']) {
                $split_name_array = $this->Action_Split_Names($contact['full_name']) ; 
                $contact['first_name'] = $split_name_array['first_name'] ; 
                $contact['last_name'] = $split_name_array['last_name'] ; 
                unset($contact['full_name']) ; 
                } 
            
            
            if ((isset($contact['first_name'])) AND ($contact['auto_capitalize_names'] == 'yes')) {
                $name_parts = explode('-',$contact['first_name']) ;                 
                $l = 0 ; 
                foreach ($name_parts as $name_part) {
                    $name_part = ucwords(strtolower($name_part)) ; 
                    if ($l > 0) {
                        $name_part = '-'.$name_part ;
                        }
                    $contact_first_name .= $name_part ; 
                    $l++ ;
                    }
                
                $contact['first_name'] = $contact_first_name ; 
                unset($contact_first_name) ;
                } 
            if ((isset($contact['last_name'])) AND ($contact['auto_capitalize_names'] == 'yes')) {                
                $name_parts = explode('-',$contact['last_name']) ;                 
                $l = 0 ; 
                foreach ($name_parts as $name_part) {
                    $name_part = ucwords(strtolower($name_part)) ; 
                    if ($l > 0) {
                        $name_part = '-'.$name_part ;
                        }
                    $contact_last_name .= $name_part ; 
                    $l++ ;
                    } 
                
                $contact['last_name'] = $contact_last_name ; 
                unset($contact_last_name) ;
                }                
            
            
            // Check to see that at least a first_name or last_name was submitted
            if ((($contact['first_name'] == '') AND ($contact['last_name'] == '')) AND ($continue == 1)) {
//                $continue = 0 ; 
//                $this->Set_Alert_Response(95) ; // Missing first and last name; requires one
//                $this->Append_Alert_Response('none',array(
//                    'admin_context' => json_encode($contact)
//                    )) ;
                }
            
            
            if ($contact['birthday']) {
                
                try {
                    
                    if (preg_match("/[a-z]/i", $contact['birthday'])) {
                        // Intentionally blank
                        } else {
                            $contact['birthday'] = str_replace('-','/',$contact['birthday']) ;   
                            }
    
                    $new_time = new DateTime($contact['birthday'],new DateTimeZone($timezone)) ; 
                    $contact['date_birthday'] = $new_time->format('Y-m-d') ;


                    // Set the next and last birthdays...
                    $date = explode('-',$contact['date_birthday']) ; 
                    $new_time->setDate(date('Y',TIMESTAMP),$date[1],$date[2]) ; 
                    $contact['timestamp_birthday_next'] = $new_time->getTimestamp() ;
                    $contact['timestamp_birthday_last'] = $new_time->getTimestamp() ;


                    $tomorrow = new DateTime() ; 
                    $tomorrow->setTimezone(new DateTimeZone($timezone)) ;  
                    $tomorrow_midnight_timestamp = $tomorrow->setTimestamp(TIMESTAMP)->add(new DateInterval('P1D'))->setTime( 0, 0, 0 )->getTimestamp() ;

                    if ($contact['timestamp_birthday_next'] < $tomorrow_midnight_timestamp) {
                        $new_time->setDate(date('Y',TIMESTAMP) + 1,$date[1],$date[2]) ; 
                        $contact['timestamp_birthday_next'] = $new_time->getTimestamp() ;
                        }
                    if ($contact['timestamp_birthday_last'] > $tomorrow_midnight_timestamp) {
                        $new_time->setDate(date('Y',TIMESTAMP) - 1,$date[1],$date[2]) ; 
                        $contact['timestamp_birthday_last'] = $new_time->getTimestamp() ;
                        } 
                    } catch (Exception $e) {
                        // Cannot process date
                        }

                }
            
            

            
            
            if (isset($contact['state_unprocessed'])) {

                $data['state_list'] = $this->Get_System_List('list_usa_states') ; 
                if (!isset($data['state_list'])) {
                    $query_options = array(
                        'override_paging' => 'yes'
                        ) ; 
                    $this->Set_System_List('list_usa_states',$query_options) ; 
                    $data['state_list'] = $this->Get_System_List('list_usa_states') ;
                    }
                
                $data['province_list'] = $this->Get_System_List('list_canada_provinces') ;
                if (!isset($data['province_list'])) {
                    $query_options = array(
                        'override_paging' => 'yes'
                        ) ; 
                    $this->Set_System_List('list_canada_provinces',$query_options) ; 
                    $data['province_list'] = $this->Get_System_List('list_canada_provinces') ;
                    }                
                
                foreach ($data['state_list'] as $state_map) {
                    if (strtolower($contact['state_unprocessed']) == strtolower($state_map['state_id'])) {
                        $contact['state'] = $state_map['state_id'] ; 
                        } else if (strtolower($contact['state_unprocessed']) == strtolower($state_map['state_name'])) {
                        $contact['state'] = $state_map['state_id'] ; 
                        }
                    }
                foreach ($data['province_list'] as $province_map) {
                    if (strtolower($contact['state_unprocessed']) == strtolower($province_map['province_id'])) {
                        $contact['state'] = $province_map['province_id'] ; 
                        } else if (strtolower($contact['state_unprocessed']) == strtolower($province_map['province_name'])) {
                        $contact['state'] = $province_map['province_id'] ; 
                        }
                    }
                
                unset($contact['state_unprocessed']) ;
                }            
            
         
            
            if (isset($contact['contact_rating_unprocessed'])) {             
                
                foreach ($data['contact_rating_metadata_list'] as $rating_metadata) {
                    if ($contact['contact_rating_unprocessed'] == $rating_metadata['metadata_name']) {
                        $contact['contact_rating_metadata_id'] = $rating_metadata['metadata_id'] ; 
                        } 
                    }
                unset($contact['contact_rating_unprocessed']) ;
                }            

    
            
            // Pre format incoming YL info... 
            if (isset($contact['yl_join_date_unprocessed'])) {

                try {
                    
                    if (preg_match("/[a-z]/i", $contact['yl_join_date_unprocessed'])) {
                        // Intentionally blank
                        } else {
                            $contact['yl_join_date_unprocessed'] = str_replace('-','/',$contact['yl_join_date_unprocessed']) ;   
                            }
                    
                    $new_time = new DateTime($contact['yl_join_date_unprocessed'],new DateTimeZone($timezone)) ; 
                    $contact['timestamp_yl_join_date'] = $new_time->getTimestamp() ;

                    } catch (Exception $e) {
                        // Cannot process date
                        }                
                unset($contact['yl_join_date_unprocessed']) ;
                }            
            
            
            if (isset($contact['yl_activation_date_unprocessed'])) {

                try {
                    
                    if (preg_match("/[a-z]/i", $contact['yl_activation_date_unprocessed'])) {
                        // Intentionally blank
                        } else {
                            $contact['yl_activation_date_unprocessed'] = str_replace('-','/',$contact['yl_activation_date_unprocessed']) ;   
                            }
                    
                    $new_time = new DateTime($contact['yl_activation_date_unprocessed'],new DateTimeZone($timezone)) ; 
                    $contact['timestamp_yl_activation_date'] = $new_time->getTimestamp() ;

                    } catch (Exception $e) {
                        // Cannot process date
                        }                
                unset($contact['yl_activation_date_unprocessed']) ;
                }            
            
            
            if (isset($contact['rank_current_unprocessed'])) {

                $data['list_yl_rank_map'] = $this->Get_System_List('list_yl_rank_map') ; 
                if (!isset($data['list_yl_rank_map'])) {
                    $query_options = array(
                        'override_paging' => 'yes'
                        ) ; 
                    $this->Set_System_List('list_yl_rank_map',$query_options) ; 
                    $data['list_yl_rank_map'] = $this->Get_System_List('list_yl_rank_map') ;
                    }                
                
                foreach ($data['list_yl_rank_map'] as $rank_map) {
                    if ($contact['rank_current_unprocessed'] == $rank_map['yl_rank_id']) {
                        $contact['rank_current_unprocessed'] = $rank_map['system_rank_slug'] ; 
                        } else if (strtolower($contact['rank_current_unprocessed']) == strtolower($rank_map['yl_rank_title'])) {
                        $contact['rank_current_unprocessed'] = $rank_map['system_rank_slug'] ; 
                        }
                    }
                
                foreach ($data['yl_rank_metadata_list'] as $rank_metadata) {
                    if ($contact['rank_current_unprocessed'] == $rank_metadata['metadata_slug']) {
                        $contact['rank_current'] = $rank_metadata['metadata_id'] ; 
                        }
                    }

                unset($contact['rank_current_unprocessed']) ;
                } 
            
            if (isset($contact['rank_highest_unprocessed'])) {

                $data['list_yl_rank_map'] = $this->Get_System_List('list_yl_rank_map') ; 
                if (!isset($data['list_yl_rank_map'])) {
                    $query_options = array(
                        'override_paging' => 'yes'
                        ) ; 
                    $this->Set_System_List('list_yl_rank_map',$query_options) ; 
                    $data['list_yl_rank_map'] = $this->Get_System_List('list_yl_rank_map') ;
                    }                
                
                foreach ($data['list_yl_rank_map'] as $rank_map) {
                    if ($contact['rank_highest_unprocessed'] == $rank_map['yl_rank_id']) {
                        $contact['rank_highest_unprocessed'] = $rank_map['system_rank_slug'] ; 
                        } else if (strtolower($contact['rank_highest_unprocessed']) == strtolower($rank_map['yl_rank_title'])) {
                        $contact['rank_highest_unprocessed'] = $rank_map['system_rank_slug'] ; 
                        } 
                    }
                
                foreach ($data['yl_rank_metadata_list'] as $rank_metadata) {
                    if ($contact['rank_highest_unprocessed'] == $rank_metadata['metadata_slug']) {
                        $contact['rank_highest'] = $rank_metadata['metadata_id'] ; 
                        } 
                    }

                unset($contact['rank_highest_unprocessed']) ;
                }            
            

            if (isset($contact['yl_essential_rewards_status_unprocessed'])) {
                switch (strtolower($contact['yl_essential_rewards_status_unprocessed'])) {
                    case 'y':
                    case 'yes':
                        $yl_essential_rewards_status = Utilities::Deep_Array($data['yl_essential_rewards_status_metadata_list'],'metadata_slug','yl-essential-rewards-active') ;
                        $contact['yl_essential_rewards_status_metadata_id'] = $yl_essential_rewards_status['metadata_id'] ; 
                        break ; 
                    case 'n':
                    case 'no':
                        $yl_essential_rewards_status = Utilities::Deep_Array($data['yl_essential_rewards_status_metadata_list'],'metadata_slug','yl-essential-rewards-inactive') ;
                        $contact['yl_essential_rewards_status_metadata_id'] = $yl_essential_rewards_status['metadata_id'] ; 
                        break ; 
                    }
                unset($contact['yl_essential_rewards_status_unprocessed']) ;
                }

            
            if (isset($contact['yl_account_status_unprocessed'])) {
                switch (strtolower($contact['yl_account_status_unprocessed'])) {
                    case 'ok':
                    case 'active':    
                        $yl_account_status = Utilities::Deep_Array($data['yl_account_status_metadata_list'],'metadata_slug','account-status-active') ;
                        $contact['yl_account_status_metadata_id'] = $yl_account_status['metadata_id'] ; 
                        break ; 
                    case 'hold':
                    case 'on hold':    
                        $yl_account_status = Utilities::Deep_Array($data['yl_account_status_metadata_list'],'metadata_slug','account-status-on-hold') ;
                        $contact['yl_account_status_metadata_id'] = $yl_account_status['metadata_id'] ; 
                        break ;
                    case 'inactive':
                        $yl_account_status = Utilities::Deep_Array($data['yl_account_status_metadata_list'],'metadata_slug','account-status-inactive') ;
                        $contact['yl_account_status_metadata_id'] = $yl_account_status['metadata_id'] ; 
                        break ;                        
                    }
                unset($contact['yl_account_status_unprocessed']) ;
                }

            
            if (isset($contact['contact_status_unprocessed'])) {
                switch (strtolower($contact['contact_status_unprocessed'])) {
                    case 'member': // Member
                    case 'wholesale member': 
                    case 'product customer':  
                    case 'customer':      
                        $contact_status_selected = Utilities::Deep_Array($data['contact_status_metadata_list'],'metadata_slug','contact-status-member') ;
                        $contact['contact_status_metadata_id'] = $contact_status_selected['metadata_id'] ; 
                        break ; 
                    case 'business builder':
                    case 'business_builder':   
                    case 'brand partner':   
                    case 'brand_partner':       
                        $contact_status_selected = Utilities::Deep_Array($data['contact_status_metadata_list'],'metadata_slug','contact-status-brand-partner') ;
                        $contact['contact_status_metadata_id'] = $contact_status_selected['metadata_id'] ; 
                        break ;     
                    case 'professional': // Professional 
                    case 'professional account':    
                        $contact_status_selected = Utilities::Deep_Array($data['contact_status_metadata_list'],'metadata_slug','contact-status-professional') ;
                        $contact['contact_status_metadata_id'] = $contact_status_selected['metadata_id'] ; 
                        break ;
                    case 'retail': // Retail
                    case 'retail customer':    
                        $contact_status_selected = Utilities::Deep_Array($data['contact_status_metadata_list'],'metadata_slug','contact-status-retail') ;
                        $contact['contact_status_metadata_id'] = $contact_status_selected['metadata_id'] ; 
                        break ;
                    case 'lead': // Lead
                    case 'prospect':
                    case 'business prospect':
                    case 'product prospect':    
                        $contact_status_selected = Utilities::Deep_Array($data['contact_status_metadata_list'],'metadata_slug','contact-status-lead') ;
                        $contact['contact_status_metadata_id'] = $contact_status_selected['metadata_id'] ; 
                        break ;
                    case 'upline/crossline': // Upline / Crossline
                    case 'upline crossline':
                    case 'support team':
                    case 'support_team':    
                        $contact_status_selected = Utilities::Deep_Array($data['contact_status_metadata_list'],'metadata_slug','contact-status-upline-crossline') ;
                        $contact['contact_status_metadata_id'] = $contact_status_selected['metadata_id'] ; 
                        break ;                        
                    }
                unset($contact['contact_status_unprocessed']) ;
                }
            
                        
            
            
            
            
            
            // If a visibility name was submitted, use that as the visiblity id
            if (isset($contact['visibility_name'])) {
                $visibility_query_options['filter_by_visibility_name'] = $contact['visibility_name'] ;
                $visibility_query_options = (object) $visibility_query_options ; 
                $visibility_query_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options) ;
                $contact['visibility_id'] = $visibility_query_result[0]['visibility_id'] ;                    
                }
            // If no visibilty id or name was submitted, default to published status
            if (!isset($contact['visibility_id'])) {
                $visibility_query_options = array() ; 
                $visibility_query_options['filter_by_visibility_name'] = 'visibility_published' ;
                $visibility_query_options = (object) $visibility_query_options ; 
                $visibility_query_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options) ;
                $contact['visibility_id'] = $visibility_query_result[0]['visibility_id'] ;                     
                }
            
            // If no errors, add as a contact to be created
            if ($continue == 1) {
                
                
                $contact_create_array = array() ; 
                $contact_details_create_array = array() ; 
                $contact_structured_data = array() ; 
                $contact_post_create_data = array() ; 
                
                foreach ($contact as $contact_key => $contact_value) {
                    switch ($contact_key) {
                        case 'update_duplicates':
                        case 'duplicate_match_override':
                        case 'contact_id':
                            if (($contact_value) AND ($contact_value != 'new')) {
                                $contact_create_array[$contact_key] = $contact_value ;     
                                $contact_details_create_array[$contact_key] = $contact_value ;     
                                }                            
                            break ; 
                        case 'first_name':
                        case 'last_name': 
                        case 'role': 
                        case 'contact_description': 
                        case 'phone_type':
                        case 'phone_country_dialing_code': 
                        case 'phone_number': 
                        case 'email_address': 
                        case 'contact_image_id': 
                        case 'address_01': 
                        case 'address_02': 
                        case 'city': 
                        case 'state': 
                        case 'postal_code': 
                        case 'country_id': 
                        case 'date_birthday':
                        case 'timestamp_birthday_next':
                        case 'timestamp_birthday_last': 
                        case 'created_source':   
                        case 'import_file_task_id':       
                        case 'visibility_id':
                            if ($contact_value) {
                                $contact_create_array[$contact_key] = $contact_value ;     
                                }
                            break ; 
                        case 'rank_current':
                        case 'rank_highest':    
                        case 'yl_member_number':
                        case 'timestamp_yl_join_date': 
                        case 'timestamp_yl_activation_date':     
                        case 'yl_enroller_name':
                        case 'yl_enroller_member_number':    
                        case 'yl_sponsor_name':    
                        case 'yl_sponsor_member_number':        
                            if ($contact_value) {
                                $contact_details_create_array[$contact_key] = $contact_value ;     
                                }                             
                            break ; 
//                        case 'yl_essential_rewards_setting':  
                        case 'yl_go_setting':      
                            if ($contact_value) {
                                $contact_structured_data[$contact_key] = $contact_value ;     
                                }                            
                            break ; 
                        case 'contact_status_metadata_id':
                        case 'contact_rating_metadata_id':
                        case 'yl_essential_rewards_status_metadata_id': 
                        case 'yl_account_status_metadata_id':    
                        case 'metadata_id':    
                        case 'note_title':     
                        case 'note_content':
                        case 'tags':    
                            if ($contact_value) {
                                $contact_post_create_data[$contact_key] = $contact_value ;     
                                }                            
                            break ;
                        case 'contact_details':
                            if (is_array($contact_value)) {
                                foreach ($contact_value as $this_key => $this_value) {
                                    $contact_details_create_array[$this_key] = $this_value ; 
                                    }
                                }                            
                            break ; 
                        }
                    }
                
                $contact_create_array['contact_details'] = $contact_details_create_array ; 
                $contact_create_array['structured_data_contact_01'] = json_encode($contact_structured_data) ; 
                $contact_create_array['contact_post_create_data'] = $contact_post_create_data ; 

                
                $create_set[] = $contact_create_array ;                 
                }
            
            if ($continue == 0) {
                $contact['error'] = $this->Get_Response() ;
                $error_set[] = $contact ;                 
                }            
            
            $i++ ; 
            }
        
        
        $result = $this->Create_Contact($create_set,$error_set) ;        
        $response = $this->Get_Response() ;

        
        // If only one contact submitted, then provide a specific result for that contact
        // The result would have already been set in $this->Create_Contact(), so no need to reset here.
        if ($result['submitted_count'] == 1) {
            
            if ($result['success_count'] == 1) {
                if ($result['success_set'][0]['contact_id']) {
                    $this->Set_Contact_By_ID($result['success_set'][0]['contact_id']) ;      
                    $this->Set_Alert_Response($response['alert_id']) ; // Missing first and last name; requires one
                    $personalize['contact_record'] = $this->Get_Contact() ; 
                    $this->Append_Alert_Response($personalize) ;                    
                    }                
                } 
            
            if ($result['alert_count'] == 1) {
                if ($result['alert_set'][0]['contact_id']) {
                    $this->Set_Contact_By_ID($result['alert_set'][0]['contact_id']) ;      
                    $this->Set_Alert_Response($response['alert_id']) ; // Missing first and last name; requires one
                    $personalize['contact_record'] = $this->Get_Contact() ; 
                    $this->Append_Alert_Response($personalize) ;                    
                    }                
                }            
            }        
        
        // If multiple contacts submitted, provide a general response based on the overall
        // success of the import w/ counts.
        if ($result['submitted_count'] > 1) {
            
            if ($result['success_count'] == $result['submitted_count']) {
            
                if ($result['submitted_set'][0]['update_duplicates'] == 'true') {
                    $this->Set_Alert_Response(252) ; // Bulk contacts created and updated
                    } else {
                    $this->Set_Alert_Response(251) ; // Bulk contacts created
                    } 
                
                $personalize['success_count'] = $result['success_count'] ; 
                $this->Append_Alert_Response($personalize) ;                
                }
                        
            }

        
        $this->contact_create_set = $result ;         
        
        return $this ; 
        }
    
    
    
    public function Action_Process_Contact_Import_Log_Record($query_options = array()) {
        
        $continue = 1 ; 
        
        $import_record_array = array(
            'filter_by_file_task_id' => $query_options['file_task_id'],
            'filter_by_import_log_id' => $query_options['import_log_id']
            ) ;
        
        if ($query_options['import_log_record']['import_log_id'] == $query_options['import_log_id']) {
            $this->contact_import_record = $query_options['import_log_record'] ; 
            $data['import_log_record'] = $this->Get_Contact_Import_Record() ;        
            } else {
                $data['import_log_record'] = $this->Set_Contact_Import_Record($import_record_array)->Get_Contact_Import_Record() ;        
                }
        
        if (!isset($data['import_log_record']['import_log_id'])) {
            $continue = 0 ; 
            }
        
        
        // Update the file task record with new result counts...
        $file_task = new File() ; 
        $data['file_task_record'] = $file_task->Set_File_Task_By_ID($data['import_log_record']['file_task_id'])->Get_File_Task() ; 

        $task_update['file_task_id'] = $data['file_task_record']['file_task_id'] ;
        $task_update['created_count'] = $data['file_task_record']['structured_data_02']['created_count'] ; 
        $task_update['updated_count'] = $data['file_task_record']['structured_data_02']['updated_count'] ; 
        $task_update['success_count'] = $data['file_task_record']['structured_data_02']['success_count'] ; 
        $task_update['alert_count'] = $data['file_task_record']['structured_data_02']['alert_count'] ; 
                  
        
        $import_log_update['import_log_id'] = $data['import_log_record']['import_log_id'] ;   
        
        
        if ($continue == 1) {
            switch ($query_options['import_action']) {
                case 'update_duplicates':    

                    $this->Set_Contact_By_ID($data['import_log_record']['duplicate_contact_id']) ;

                    $contact_details_input = $data['import_log_record']['submitted_contact'] ; 
                    $contact_details_input['contact_id'] = $data['import_log_record']['duplicate_contact_id'] ; 
                    $contact_details_input['update_duplicates'] = 'true' ; 
                    $contact_details_input['duplicate_match_override'] = 'true' ;

                    $contact_update_set = array() ;
                    $contact_update_set[] = $contact_details_input ; 
                    
                    $this->Action_Update_Contact($contact_update_set) ; 

                    $result = $this->Get_Response() ; 

                    if ($result['result'] == 'success') {
                        $task_update['updated_count']++ ;  
                        $task_update['success_count']++ ; 
                        
                        switch ($data['import_log_record']['import_status']) { 
                            case 'duplicate_unprocessed':
                                $task_update['alert_count']-- ;
                                $import_log_update['import_status'] = 'duplicate_processed' ;
                                break ; 
                            case 'error_unprocessed':
                                $task_update['error_count']-- ;
                                $import_log_update['import_status'] = 'error_processed' ;
                                break ; 
                            }

                        $import_log_update['action_status'] = 'updated' ;  
                        
                        $import_log_update['processed_contact_id'] = $data['import_log_record']['duplicate_contact_id'] ; 
                        }


                    break ; 
                case 'create_new': 

                    $contact_details_input = $data['import_log_record']['submitted_contact'] ; 
                    $contact_details_input['duplicate_match_override'] = 'true' ; 
                
                    
                    $contact_update_set = array() ;
                    $contact_update_set[] = $contact_details_input ;
                    
                    $this->Action_Create_Contact($contact_update_set) ;

                    $result = $this->Get_Response() ; 
                   
                    if ($result['result'] == 'success') {                    
                        
                        $task_update['created_count']++ ; 
                        $task_update['success_count']++ ; 
                        
                        switch ($data['import_log_record']['import_status']) { 
                            case 'duplicate_unprocessed':
                                $task_update['alert_count']-- ;
                                $import_log_update['import_status'] = 'duplicate_processed' ;
                                break ; 
                            case 'error_unprocessed':
                                $task_update['error_count']-- ;
                                $import_log_update['import_status'] = 'error_processed' ;
                                break ; 
                            }

                        $import_log_update['action_status'] = 'new' ;    
                        
                        $contact_create_set = $this->Get_Contact_Create_Set() ; 
                        $import_log_update['processed_contact_id'] = $contact_create_set['success_set'][0]['contact_id'] ;    
                        }  
                    
                    break ; 
                case 'ignore':                    
                    
                    $import_log_update['action_status'] = 'ignored' ; 
                    
                    switch ($data['import_log_record']['import_status']) {
                        case 'error_unprocessed':
                            $import_log_update['import_status'] = 'error_processed' ;
                            break ;
                        case 'duplicate_unprocessed':
                            $import_log_update['import_status'] = 'duplicate_processed' ;
                            break ; 
                        }
                            
                    
                    break ; 
                }



            $file_task->Action_Update_File_Task($task_update) ; 
            $this->Action_Update_Contact_Import_Log($import_log_update)->Get_Contact_Import_Record() ; 
            
            $this->Set_Alert_Response(253) ; // Contacts updated
            }
        
        
        return $this ; 
        }
    
    
    public function Action_Update_Contact($contact_input) {

        global $server_config ; 
        
        if ($this->user['timezone']) {
            $timezone = $this->user['timezone'] ; 
            } else {
                $timezone = $this->system_timezone ; 
                }
        
        $create_set = array() ; 
        $error_set = array() ; 
        $update_subscription_assets = array() ; // Array that we will place asset IDs in purely for updating timestamp_last_member_update
        
        // If this line throws an error, check to see if your input includes parameters that would force 
        // it to become a multimensional array prematurely.
        unset($contact_input['preresponses'],$contact_input['view_names_unprocessed'],$contact_input['view_item_set'],$contact_input['view_names']) ; // Need to unset before multidimensional and once inside foreach loop

        $contact_input = Utilities::Array_Force_Multidimensional($contact_input) ;
        $original_set_count = count($contact_input) ; 


        $i = 0 ; 
        foreach ($contact_input as $contact) {
            
            $continue = 1 ; 

            // If this line throws an error, check to see if your input includes parameters that would force 
            // it to become a multimensional array prematurely.
            if (isset($contact['contact_details'])) {
                foreach ($contact['contact_details'] as $key => $value) {
                    $contact[$key] = $value ; 
                    }
                }
            if (isset($contact['contact_post_create_data'])) {
                foreach ($contact['contact_post_create_data'] as $key => $value) {
                    $contact[$key] = $value ; 
                    }
                }             
            
            unset($contact['contact_details'],$contact['contact_post_create_data']) ;
            unset($contact_input['view_names_unprocessed'],$contact['view_item_set'],$contact['view_names']) ;            
                        
            $contact['update_duplicates'] = 'true' ; 

            
            // Convert datetime_ inputs to timestamps...
            $contact = $this->Action_Convert_Datetime_To_Timestamp($contact) ;

            // Validate incoming phone_number
            if ($contact['phone_number']) { 
                
                if (!isset($contact['phone_country_dialing_code'])) {
                    $contact['phone_country_dialing_code'] = $this->user['phone_country_dialing_code'] ; 
                    }                
                
                $phone_number_options = array(
                    'phone_country_dialing_code' => $contact['phone_country_dialing_code'],
                    'phone_owner' => 'contact',
                    'contact_id' => $contact['contact_id'],
                    'user_id' => $this->user_id
                    ) ;
                
                $validate = new Contact() ; 
                $validate->Action_Validate_Phone_Number($contact['phone_number'],$phone_number_options) ;                 
                $validate_phone_number = $validate->Get_Phone_Number_Validation() ; 
                
                
                if ($validate_phone_number['valid'] == 'no') { 
                    
                    // Perhaps add something to the new contact history that the submitted phone number was invalid.
                    unset($contact['phone_type'],$contact['phone_number'],$contact['phone_country_dialing_code']) ; 
                    $continue = 0 ; 
                    $this->Set_Alert_Response(204) ; // Contact: Invalid phone number
                    
                    } else {
                        
                        if (!isset($contact['phone_type'])) {
                                $contact['phone_type'] = 'home' ;
                                }
                        $contact['phone_number'] = $validate_phone_number['unformatted'] ; 
                        $contact['phone_country_dialing_code'] = $validate_phone_number['phone_country_dialing_code'] ; 
                        }                
                                        
                } else {
                
                    if ((isset($contact['phone_number'])) AND ($contact['phone_number'] == '')) {
                        $contact['phone_type'] = '' ; 
                        $contact['phone_number'] = '' ; 
                        $contact['phone_country_dialing_code'] = '' ;    
                        } else {
                            unset($contact['phone_type'],$contact['phone_number'],$contact['phone_country_dialing_code']) ; 
                            }
                    }
            
            
            // Validate incoming email_address
            if ($contact['email_address']) { 
                $validate_email_address = Utilities::Validate_Email_Address($contact['email_address']) ; 
                if ($validate_email_address['valid'] == 'no') {
                    $continue = 0 ; 
                    $this->Set_Alert_Response(96) ; // New contact invalid email
                    } else {
                        $contact['email_address'] = $validate_email_address['email_address'] ; 
                        }                
                }
            
            
            // Set errors if mismatched duplicates...
            $duplicate_logic = $this->Action_Duplicate_Contact_Logic($contact,array()) ; 
            
//            print_r('<pre>') ;  
//            print_r($duplicate_logic) ; 
//            print_r('</pre>') ; 
            
            if (($contact['email_address']) AND ($duplicate_logic['dup_score_email_address'] > 0) AND ($contact['contact_id'] != $duplicate_logic['contact_id_email_match'])) {
                
                // If the matching email address is a deleted contact, then delete the email address 
                // from the deleted contact and allow the submitted update to proceed
                switch ($duplicate_logic['contact_email_match']['visibility_name']) {
                    case 'visibility_hidden':
                    case 'visibility_deleted':    
                        
                        $deleted_contact_details = array(
                            'contact_id' => $duplicate_logic['contact_email_match']['contact_id'],
                            'email_address' => ''
                            ) ;
                        
                        $update_deleted_contact = new Contact($this->user) ; 
                        $update_deleted_contact->Set_Account_By_ID($this->account_id) ; 
                        $update_deleted_contact->Set_Contact_By_ID($duplicate_logic['contact_email_match']['contact_id'])->Action_Update_Contact($deleted_contact_details) ;
                        break ; 
                    default:    
                        unset($contact['email_address']) ; 
                        $this->Set_Alert_Response(268) ; // Contact: Email address in use by different contact
                        if ($contact['duplicate_match_override'] != 'true') { // true allows update to continue but unsets the matched field
                            $continue = 0 ;
                            }
                         
                    }                
                }            
            if (($contact['phone_number']) AND ($duplicate_logic['dup_score_phone_number'] > 0) AND ($contact['contact_id'] != $duplicate_logic['contact_id_phone_match'])) {
                
                // If the matching phone number is a deleted contact, then delete the phone number 
                // from the deleted contact and allow the submitted update to proceed
                switch ($duplicate_logic['contact_phone_match']['visibility_name']) {
                    case 'visibility_hidden':
                    case 'visibility_deleted':    
                        
                        $deleted_contact_details = array(
                            'contact_id' => $duplicate_logic['contact_phone_match']['contact_id'],
                            'phone_type' => '',
                            'phone_country_dialing_code' => '',
                            'phone_number' => ''
                            ) ;
                        
                        $update_deleted_contact = new Contact($this->user) ; 
                        $update_deleted_contact->Set_Account_By_ID($this->account_id) ; 
                        $update_deleted_contact->Set_Contact_By_ID($duplicate_logic['contact_phone_match']['contact_id'])->Action_Update_Contact($deleted_contact_details) ;
                        break ; 
                    default:    
                        unset($contact['phone_number'],$contact['phone_country_dialing_code'],$contact['phone_type']) ; 
                        $this->Set_Alert_Response(267) ; // Contact: Phone number in use by different contact
                        if ($contact['duplicate_match_override'] != 'true') { // true allows update to continue but unsets the matched field
                            $continue = 0 ;
                            }
                    }                
                }
            if (($contact['yl_member_number']) AND ($duplicate_logic['dup_score_yl_member_number'] > 0) AND ($contact['contact_id'] != $duplicate_logic['contact_id_yl_member_number_match'])) {
                
                // If the matching phone number is a deleted contact, then delete the phone number 
                // from the deleted contact and allow the submitted update to proceed
                switch ($duplicate_logic['contact_yl_member_number_match']['visibility_name']) {
                    case 'visibility_hidden':
                    case 'visibility_deleted':    
                        
                        $deleted_contact_details = array(
                            'contact_id' => $duplicate_logic['contact_yl_member_number_match']['contact_id'],
                            'yl_member_number' => ''
                            ) ;
                        
                        $update_deleted_contact = new Contact($this->user) ; 
                        $update_deleted_contact->Set_Account_By_ID($this->account_id) ; 
                        $update_deleted_contact->Set_Contact_By_ID($duplicate_logic['contact_yl_member_number_match']['contact_id'])->Action_Update_Contact($deleted_contact_details) ;
                        break ; 
                    default:    
                        unset($contact['yl_member_number']) ; 
                        $this->Set_Alert_Response(269) ; // Contact: YL member number in use by by different contact
                        if ($contact['duplicate_match_override'] != 'true') { // true allows update to continue but unsets the matched field
                            $continue = 0 ;
                            }
                    }                
                }            
         
            
            if ($contact['inverted_full_name']) {                
                $full_name = explode(',',$contact['inverted_full_name']) ;
                $contact['full_name'] = $full_name[1].' '.$full_name[0] ;
                unset($contact['inverted_full_name']) ;
                }
            
            // Split a full name submission into first & last names
            if ($contact['full_name']) {
                $split_name_array = $this->Action_Split_Names($contact['full_name']) ; 
                $contact['first_name'] = $split_name_array['first_name'] ; 
                $contact['last_name'] = $split_name_array['last_name'] ; 
                unset($contact['full_name']) ; 
                } 
            
            // Check to see that at least a first_name or last_name was submitted
            if ((($contact['first_name'] == '') AND ($contact['last_name'] == '')) AND ($continue == 1)) {
//                $continue = 0 ; 
//                $this->Set_Alert_Response(95) ; // Missing first and last name; requires one
                }
            
            
            if ($contact['birthday']) { 
                
                switch ($contact['birthday']) {
                    case 'remove':
                        
                        $contact['date_birthday'] = 'NULL' ; 
                        $contact['timestamp_birthday_next'] = 0 ; 
                        $contact['timestamp_birthday_last'] = 0 ;
                        
                        break ; 
                    default:    
                        
                        if (preg_match("/[a-z]/i", $contact['birthday'])) {
                            // Intentionally blank
                            } else {
                                $contact['birthday'] = str_replace('-','/',$contact['birthday']) ;   
                                }
                        
                        $new_time = new DateTime($contact['birthday'],new DateTimeZone($timezone)) ; 
                        $contact['date_birthday'] = $new_time->format('Y-m-d') ; 

                        // Set the next and last birthdays...
                        $date = explode('-',$contact['date_birthday']) ; 
                        $new_time->setDate(date('Y',TIMESTAMP),$date[1],$date[2]) ; 
                        $contact['timestamp_birthday_next'] = $new_time->getTimestamp() ;
                        $contact['timestamp_birthday_last'] = $new_time->getTimestamp() ;


                        $tomorrow = new DateTime() ; 
                        $tomorrow->setTimezone(new DateTimeZone($timezone)) ;  
                        $tomorrow_midnight_timestamp = $tomorrow->setTimestamp(TIMESTAMP)->add(new DateInterval('P1D'))->setTime( 0, 0, 0 )->getTimestamp() ;

                        if ($contact['timestamp_birthday_next'] < $tomorrow_midnight_timestamp) {
                            $new_time->setDate(date('Y',TIMESTAMP) + 1,$date[1],$date[2]) ; 
                            $contact['timestamp_birthday_next'] = $new_time->getTimestamp() ;
                            }
                        if ($contact['timestamp_birthday_last'] > $tomorrow_midnight_timestamp) {
                            $new_time->setDate(date('Y',TIMESTAMP) - 1,$date[1],$date[2]) ; 
                            $contact['timestamp_birthday_last'] = $new_time->getTimestamp() ;
                            }                        
                    }
                                
                }
            
            if ($contact['state'] == 'remove') {
                $contact['state'] = '' ;                
                }
            if ($contact['country_id'] == 'remove') {
                $contact['country_id'] = '' ;                
                }
            
            
            // If a visibility name was submitted, use that as the visiblity id
            if (isset($contact['visibility_name'])) {
                $visibility_query_options['filter_by_visibility_name'] = $contact['visibility_name'] ;
                $visibility_query_options = (object) $visibility_query_options ; 
                $visibility_query_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options) ;
                $contact['visibility_id'] = $visibility_query_result[0]['visibility_id'] ;                    
                }
            
            
            // Split a full name submission into first & last names
            if ($contact['full_name']) { 
                $split_name_array = $this->Action_Split_Names($contact['full_name']) ; 
                $contact['first_name'] = $split_name_array['first_name'] ; 
                $contact['last_name'] = $split_name_array['last_name'] ; 
                unset($contact['full_name']) ; 
                }            
            
            
            
            // If no errors, add as a contact to be created
            if ($continue == 1) {
                
                $contact_update_array = array() ; 
                $contact_details_update_array = array() ; 
                $contact_structured_data = array() ; 
                
                foreach ($contact as $contact_key => $contact_value) {
                    switch ($contact_key) {
                        case 'contact_id':
                        case 'update_duplicates':    
                            $contact_update_array[$contact_key] = $contact_value ; 
                            $contact_details_update_array[$contact_key] = $contact_value ;
                            break ;                         
                        case 'first_name':
                        case 'last_name': 
                        case 'role': 
                        case 'contact_description':    
                        case 'phone_type':    
                        case 'phone_country_dialing_code': 
                        case 'phone_number': 
                        case 'email_address': 
                        case 'contact_image_id': 
                        case 'address_01': 
                        case 'address_02': 
                        case 'city': 
                        case 'state': 
                        case 'postal_code': 
                        case 'country_id': 
                        case 'date_birthday':
                        case 'timestamp_birthday_next':
                        case 'timestamp_birthday_last':    
                        case 'visibility_id':    
                            $contact_update_array[$contact_key] = $contact_value ; 
                            break ; 
                        case 'rank_current':
                        case 'rank_highest':    
                        case 'yl_member_number':
                        case 'timestamp_yl_join_date':
                        case 'timestamp_yl_activation_date':    
                        case 'yl_enroller_name':
                        case 'yl_enroller_member_number':    
                        case 'yl_sponsor_name':    
                        case 'yl_sponsor_member_number':    
                            $contact_details_update_array[$contact_key] = $contact_value ;
                            break ; 
//                        case 'yl_essential_rewards_setting': 
                        case 'yl_go_setting': 
                            $contact_structured_data[$contact_key] = $contact_value ; 
                            break ; 
                        }
                    }
                
                $contact_update_array['structured_data_contact_01'] = $contact_structured_data ; 
                
                $this->Set_Contact_By_ID($contact_update_array['contact_id']) ; 
                $check_contact_data = $this->Get_Contact() ; 
                
                // Retrieve the existing metadata entries and make sure they don't get overwritten while adding new key => values
                if (isset($contact_update_array['structured_data_contact_01'])) {
                    
                    $existing_structured_data_contact_01 = json_decode($check_contact_data['structured_data_contact_01'],true) ;
                    foreach ($contact_update_array['structured_data_contact_01'] as $key => $value) {
                        $existing_structured_data_contact_01[$key] = $value ;         
                        }

                    $contact_update_array['structured_data_contact_01'] = json_encode($existing_structured_data_contact_01) ; 
                    }                
                
                
                $contact_update_array['timestamp_contact_updated'] = TIMESTAMP ;
                
                $update_set[] = $contact_update_array ;         
                $update_details_set[] = $contact_details_update_array ;      
                
                // Create a list of subscription assets to update w/ timestamp_last_member_update
                foreach ($check_contact_data['broadcast_subscriptions_list'] as $broadcast_list) {
                    $check_subscription = Utilities::Deep_Array($update_subscription_assets,'asset_id',$broadcast_list['asset_id']) ; 
                    if (!$check_subscription) {
                        $update_subscription_assets[] = $broadcast_list ; 
                        }
                    }                
                foreach ($check_contact_data['series_subscriptions_list'] as $series_list) {
                    $check_subscription = Utilities::Deep_Array($update_subscription_assets,'asset_id',$series_list['asset_id']) ; 
                    if (!$check_subscription) {
                        $update_subscription_assets[] = $series_list ; 
                        }
                    }
                
                }
            
            if ($continue == 0) {
                $contact['error'] = $this->Get_Response() ;
                $error_set[] = $contact ;                 
                }            
            
                   

            // Updates to be made to existing contacts only...
            if ((isset($contact['contact_id'])) AND ($continue == 1)) {

                                
                foreach ($check_contact_data['broadcast_subscriptions_list'] as $broadcast_list) {
                    
                    }
                
                
                // Generic TAG metadata submitted as comma separated string
                if (isset($contact['tags'])) {
                    
                    $submitted_metadata_array = Utilities::Process_Comma_Separated_String($contact['tags']) ; 

                    $metadata_query = array(
                        'allow_global_metadata' => 'yes',
                        'filter_by_metadata_type' => 'tag',
                        'filter_by_visibility_name' => 'visibility_published',
                        'filter_by_search_parameter' => implode(",",$submitted_metadata_array)  
                        ) ;
                    
                    
                    $existing_metadata_list = $this->Set_Metadata_List($metadata_query)->Get_Metadata_List() ; 

                    
                    if ($existing_metadata_list != 'error') {
                        
                        foreach ($submitted_metadata_array as $submitted_metadata) {
                            
                            $found_metadata = Utilities::Deep_Array($existing_metadata_list,'metadata_name',$submitted_metadata) ;
                            
                            if ($found_metadata) {
                                

                                $contact['metadata_id'] .= ','.$found_metadata['metadata_id'] ; 
                                } else {
                                                            
                                    $metadata_input = array(
                                        'account_id' => $this->account_id,
                                        'metadata_name' => $submitted_metadata,
                                        'metadata_type' => 'tag'
                                        ) ;                   

                                    // Create the metadata name & slug
                                    $this->Action_Create_Metadata($metadata_input) ;                                
                                    $new_metadata = $this->Get_Metadata() ;
                                
                                    $contact['metadata_id'] .= ','.$new_metadata['metadata_id'] ; 
                                    }
                            
                            unset($found_metadata,$new_metadata) ;
                            }
                        
                        } else {
                        
                            foreach ($submitted_metadata_array as $submitted_metadata) {

                                $metadata_input = array(
                                    'account_id' => $this->account_id,
                                    'metadata_name' => $submitted_metadata,
                                    'metadata_type' => 'tag'
                                    ) ;                   

                                // Create the metadata name & slug
                                $this->Action_Create_Metadata($metadata_input) ;                                
                                $new_metadata = $this->Get_Metadata() ;

                                $contact['metadata_id'] .= ','.$new_metadata['metadata_id'] ;
                                unset($new_metadata) ;
                                } 
                        
                            }
                    }
                

                // General metadata...
                if (isset($contact['metadata_id'])) {
                    
                    
                    $metadata_array = Utilities::Process_Comma_Separated_String($contact['metadata_id']) ; 
                    
                    foreach ($metadata_array as $metadata_id) {
                        if ($metadata_id) {
                            
                            $contact_metadata = array(
                                'item_id' => $contact['contact_id'],
                                'metadata_id' => $metadata_id
                                ) ; 
                            $contact_metadata_result = $this->Create_Contact_Metadata($contact_metadata) ;
                            } 
                        } 
                    }
                

                
                // Starred favorite metadata
                if (isset($contact['starred_favorite'])) {
                    switch ($contact['starred_favorite']) {
                        case 'on': // Turn starred_favorite on for the contact
                            
                            $favorite_metadata = array(
                                'item_id' => $contact['contact_id'],
                                'metadata_id' => Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'starred_favorite')['value']        
                                ) ; 
                            $favorite_result = $this->Create_Contact_Metadata($favorite_metadata) ; 
                            
                            break ; 
                        case 'off': // Turn starred_favorite OFF for the contact
                            $favorite_result = $this->Delete_Contact_Metadata($contact['starred_favorite_relationship_id']) ; 
                            
                            break ; 
                        }
                    } 
                
                
                // Additional metadata
                foreach ($contact as $update_key => $update_value) {
                    switch ($update_key) {
                        case 'contact_status_metadata_id':
                        case 'contact_rating_metadata_id':
                        case 'yl_contact_group_metadata_id':
                        case 'yl_member_type_metadata_id':
                        case 'yl_team_status_metadata_id':    
                        case 'yl_essential_rewards_status_metadata_id':    
                        case 'yl_account_status_metadata_id':    
                        
                            $metadata_prefix = str_replace('_metadata_id',"",$update_key);
                            
                            if ($check_contact_data['contact_id'] != $contact['contact_id']) {
                                $check_contact_data = $this->Set_Contact_By_ID($contact['contact_id'])->Get_Contact() ; 
                                }
                            if (isset($check_contact_data[$metadata_prefix.'_relationship_id'])) {
                                $contact[$metadata_prefix.'_relationship_id'] = $check_contact_data[$metadata_prefix.'_relationship_id'] ; 
                                }

                            if ($contact[$metadata_prefix.'_metadata_id'] == 'delete') {
                                if ($contact[$metadata_prefix.'_relationship_id']) {
                                    $metadata_result = $this->Delete_Contact_Metadata($contact[$metadata_prefix.'_relationship_id']) ; 
                                    
                                    }                        
                                } else {
                                    $update_metadata_array = array(
                                        'item_id' => $contact['contact_id'],
                                        'metadata_id' => $contact[$metadata_prefix.'_metadata_id']
                                        ) ; 
                                    if ($contact[$metadata_prefix.'_relationship_id']) {
                                        $update_metadata_array['relationship_id'] = $contact[$metadata_prefix.'_relationship_id'] ; 
                                        }

                                    $metadata_result = $this->Create_Contact_Metadata($update_metadata_array) ;                        
                                    } 

                            break ; 
                        }
                    }
                
           
                }
            
            
            $i++ ;
            }
        

        if ($continue == 1) {
            
            $result = $this->Update_Contact($update_set) ;
            $result_details = $this->Update_Contact_Details($update_details_set) ;
            
            if (!$result['error']) {

                if (count($update_set) == 1) {
                    $personalize['contact_record'] = $this->Set_Contact_By_ID($update_set[0]['contact_id'])->Get_Contact() ; 
                    $this->Set_Alert_Response(70) ; // Contact updated
                    $this->Append_Alert_Response($personalize) ;                     
                    } else {
                        $this->Set_Alert_Response(253) ; // Contacts updated
                        }

                
                // Update all subscription lists that were affected by this update with timestamp_last_member_update
                foreach ($update_subscription_assets as $subscription_asset) {
                    
                    $asset_update = new Asset() ;
                    
                    $update_asset_pull = array(
                        'auth_override' => 'yes',
                        'asset_input_info' => array(
                            'rewrite_latest_history' => 'yes'
                            ),
                        'asset_id' => $subscription_asset['asset_id'],
                        'visibility_id' => $subscription_asset['visibility_id'],
                        'timestamp_last_member_update' => time()
                        ) ; 
                    $asset_update->Action_Update_Content($update_asset_pull) ; 
                    }
                
                
                } else {

                    $this->Set_Alert_Response(71) ; // Contact update failed
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;             

                    }
            }
        
        $this->contact_query_result = $result ; 
        
        return $this ; 
        }
    
    
    public function Action_Delete_Contact($contact_input) {
        
        global $server_config ; 
        
        $create_set = array() ; 
        $error_set = array() ; 
        
        // If this line throws an error, check to see if your input includes parameters that would force 
        // it to become a multimensional array prematurely.
        unset($contact_input['view_names_unprocessed'],$contact_input['view_item_set'],$contact_input['view_names']) ; // Need to unset before multidimensional and once inside foreach loop
        
        $contact_input = Utilities::Array_Force_Multidimensional($contact_input) ;
        $original_set_count = count($contact_input) ; 

        
        $i = 0 ;
        $s = 0 ; // success counter
        $e = 0 ; // error counter
        foreach ($contact_input as $contact) {
            
            $this_contact_id = $contact['contact_id'] ; 
            
            $continue = 1 ;
            
            $delete_contact = array() ; 
            $delete_contact[] = array(
                'contact_id' => $this_contact_id,
                'visibility_name' => 'visibility_deleted'
                ) ; 
            
            $this->Action_Update_Contact($delete_contact) ;
            
            
            // Set contact notes as deleted
            $query_array = array(
                'table' => 'contact_notes',
                'values' => array(
                    'visibility_id' => 2,
                    'timestamp_updated' => TIMESTAMP
                    ),
                'where' => "contact_id='$this_contact_id'"
                );


            $result = $this->DB->Query('UPDATE',$query_array);
            
            
            
            // Delete contact metadata relationships...
            $query_array = array(
                'table' => "nonasset_metadata_relationships",
                'where' => "nonasset_metadata_relationships.item_id='$this_contact_id' 
                AND nonasset_metadata_relationships.relationship_type='contact' "
                );
        
            $result = $this->DB->Query('DELETE',$query_array);
            
            
            // Remove all broadcast list subscriptions
            // Currently we are not preserving unsubscribes
            $subscriptions_query = array(
                'filter_by_asset_id' => 'no',
                'filter_by_user_id' => 'no',
                'filter_by_account_id' => 'no',
                'type_id' => 19,
                'contact_id' => $this_contact_id
                ) ;
            
            
            $asset = new Asset() ; 
            $asset->Set_Asset_Members_List($subscriptions_query) ; 
            $data['broadcast_subscriptions_list'] = $asset->Get_Asset_Members_List() ;             
            
            foreach ($data['broadcast_subscriptions_list'] as $broadcast_list) {
                
                switch ($broadcast_list['email_metadata_id']) {
                    case 47: // Currently unsubscribed
                    case 49: // Currently cleaned    
                        // Leave as current status
                        break ; 
                    default:
                        // Update to inactive...
                        
                        $member_id = $broadcast_list['member_id'] ; 
                        
                        $broadcast_email_query_array = array(
                            'table' => "asset_members",
                            'values' => array(
                                'metadata_id' => 51,
                                'timestamp_member_updated' => TIMESTAMP
                                ),
                            'where' => "asset_members.member_id='$member_id'"
                            );

                        $update_broadcast_email_subs = $this->DB->Query('UPDATE',$broadcast_email_query_array);                                                
                    }
                
                switch ($broadcast_list['sms_metadata_id']) {
                    case 47: // Currently unsubscribed
                    case 49: // Currently cleaned    
                        // Leave as current status
                        break ; 
                    default:
                        // Update to inactive...
                        
                        $member_id = $broadcast_list['member_id'] ; 
                        
                        $broadcast_sms_query_array = array(
                            'table' => "asset_members",
                            'values' => array(
                                'metadata_id' => 51,
                                'timestamp_member_updated' => TIMESTAMP
                                ),
                            'where' => "asset_members.member_id='$member_id'"
                            );

                        $update_broadcast_sms_subs = $this->DB->Query('UPDATE',$broadcast_sms_query_array);                                                
                    }                
                }
            
            
            
            $response = $this->Get_Response() ;
            if ($response['result'] == 'success') {
                $s++ ; // increment success
                } else {
                    $e++ ; // increment error
                    }
            }
        
        
        
        
        if ($original_set_count == $s) {

            if ($original_set_count == 1) {
                $personalize['contact_record'] = $this->Set_Contact_By_ID($contact_input[0]['contact_id'])->Get_Contact() ; 
                $this->Set_Alert_Response(245) ; // Contact deleted
                $this->Append_Alert_Response($personalize) ;                     
                } else {
                    $this->Set_Alert_Response(246) ; // Multiple contacts deleted
                    }

            } else {

                $this->Set_Alert_Response(71) ; // Contact update failed
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($result),
                    'location' => __METHOD__
                    )) ;             

                }
        
        
        $this->contact_query_result = $result ; 
        
        return $this ;
        
        }
    
    
    
    // Send an email to a contact outside of a subscription list...
    public function Action_Trigger_Contact_Email($email_options = array()) {
        
        switch ($email_options['email_asset']) {
            case 'contact_preferences_email_notification':
                $email_asset_id = $this->Set_System_Default_Value('contact_preferences_email_notification')->Get_System_Default_Value() ;
                break ;
            }        
        


        
        // Get app default email domain...
        $domain_query_options = array(
            'app_default_domain' => 1
            ) ;
        
        $this->Set_System_List('app_email_domains',$domain_query_options) ; 
        $email_domain = $this->Get_System_List('app_email_domains')[0] ; // Email app domain
        
        $email = new Email() ;
        $email->Set_Sending_Domain($email_domain['domain_display_name'])->Set_User_By_Profile_ID($this->profile_id) ; 
        $email->Action_Compile_Email($email_asset_id)->Action_Personalize_Email() ;
        
 

        $campaign_options = array(
            'asset_id' => $email_asset_id,
            'campaign_status_name' => 'deleted',
            'automation_status_name' => 'scheduled',
            'timestamp_campaign_scheduled' => TIMESTAMP,
            'campaign_title' => 'Contact Preferences Link: '.$this->contact['email_address'],
            'timestamp_next_action' => TIMESTAMP,
            'delivery_trigger' => 'static',
            'auto_delete' => 'yes'
            ) ; 

        $email->Action_Create_Campaign($campaign_options) ;
        $email_campaign = $email->Get_Campaign() ;
        
        // Update the campaign with this specific static recipient...
        $recipient_variables = array() ; 
        $recipient_variables = array(
            'email_address' => $this->contact['email_address'],
            'first_name' => $this->contact['first_name'],
            'last_name' => $this->contact['last_name'],
            'contact_id' => $this->contact['contact_id'],
            'contact_token' => $this->contact['contact_token']
            ) ;
        
        
        $campaign_update_options['campaign_id'] = $email_campaign['campaign_id'] ; 
        $campaign_update_options['structured_data_01'] = $email_campaign['structured_data_01'] ; 
        $campaign_update_options['structured_data_01']['recipients']['to']['static']['email_address'][] = $this->contact['email_address'] ;
        $campaign_update_options['structured_data_01']['recipients']['to']['static']['recipient_variables'][$this->contact['email_address']] = $recipient_variables ;

        
//        if ($verification_options['verification_type'] == 'new_user_registration') {
//            $campaign_update_options['structured_data_01']['personalization_overrides']['user_record']['user_id'] = $this->user['user_id'] ;
//            $campaign_update_options['structured_data_01']['personalization_overrides']['user_record']['registration_token'] = $this->user['registration_token'] ; 
//            }
        
                
        $email->Action_Update_Campaign($campaign_update_options) ; 
        
        $automation_record = $email->Get_Automation() ; 
        $email->Action_Trigger_Automation_Queue($automation_record['automation_id']) ; 
        

        
        $delivery_response = $email->Get_Response() ;
        $this->Set_Alert_Response($delivery_response['alert_id']) ; 
        
        return $this ; 
        }
    
    
    
    
    // Send an email to a contact outside of a subscription list...
    public function Action_Trigger_Contact_Message($message_options = array()) {
        
        switch ($message_options['message_asset']) {
            case 'contact_preferences_sms_notification':
                $sms_asset_id = $this->Set_System_Default_Value('contact_preferences_sms_notification')->Get_System_Default_Value() ;
                break ;
            } 
        
        
        $data['profile_record'] = $this->Get_Profile() ; 
        $data['contact_record'] = $this->Get_Contact() ; 
        
        
        $asset = new Asset($data['profile_record']['user_id']) ; 
        $asset->Set_Account_By_ID($data['profile_record']['account_id']) ; 
            
        $campaign_options = array(
            'asset_id' => $sms_asset_id, 
            'campaign_status_name' => 'deleted',
            'campaign_title' => 'Contact Preferences Link: '.$data['contact_record']['first_name'].' '.$data['contact_record']['last_name'].' ('.$data['contact_record']['phone_number'].')',
            'timestamp_next_action' => TIMESTAMP,
            'timestamp_campaign_scheduled' => TIMESTAMP,
            'messaging_number_id' => $data['profile_record']['messaging_number_id'],
            'automation_status_name' => 'scheduled',
            'delivery_trigger' => 'static',
            'auto_delete' => 'yes',
            'recipients' => array(
                'to' => array(
                    'static' => array(
                        'phone_number' => array()
                        ),
                    ),
                ), 
            'personalization_overrides' => array(
                'asset_record' => array(
                    'asset_title' => $data['delivery_asset']['asset_title']
                    )
                )
            ) ;  



        $asset->Action_Create_Campaign($campaign_options) ;
        $text_campaign = $asset->Get_Campaign() ;
        $response_campaign = $asset->Get_Response() ;
        
        
        
        // Update the campaign with this specific static recipient...
        $recipient_variables = array() ; 
        $recipient_variables = array(
            'phone_number' => $this->contact['phone_number_compiled']['local'],
            'phone_country_dialing_code' => $this->contact['phone_number_compiled']['phone_country_dialing_code'],
            'first_name' => $this->contact['first_name'],
            'last_name' => $this->contact['last_name'],
            'contact_id' => $this->contact['contact_id'],
            'contact_token' => $this->contact['contact_token']
            ) ;
        
        
        $campaign_update_options['campaign_id'] = $text_campaign['campaign_id'] ; 
        $campaign_update_options['structured_data_01'] = $text_campaign['structured_data_01'] ; 
        $campaign_update_options['structured_data_01']['recipients']['to']['static']['phone_number'][] = $this->contact['phone_number_compiled']['international'] ;
        $campaign_update_options['structured_data_01']['recipients']['to']['static']['recipient_variables'][$this->contact['phone_number_compiled']['international']] = $recipient_variables ;

                
        $asset->Action_Update_Campaign($campaign_update_options) ;        
        
        
        if ($response_campaign['result'] == 'success') {
            $response_campaign['automation'] = $asset->Get_Automation() ;     
            $asset->Action_Trigger_Automation_Queue($response_campaign['automation']['automation_id']) ;
            }        
        
        return $this ; 
        }
    
    
    
    // Send Recipient Confirmation:
                    
                    
    
    
    
    public function Create_Contact_Metadata($query_options) {
    
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ;             
            }
        $values_array['relationship_type'] = 'contact' ; 
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => "nonasset_metadata_relationships",
            'values' => $values_array,
            'where' => "nonasset_metadata_relationships.metadata_id='$query_options->metadata_id' 
                AND nonasset_metadata_relationships.relationship_type='contact' "
            );

        if (isset($query_options->item_id)) {
            $query_array['where'] .= " AND nonasset_metadata_relationships.item_id='$query_options->item_id'" ; 
            }
        if (isset($query_options->contact_id)) {
            $query_array['where'] .= " AND nonasset_metadata_relationships.item_id='$query_options->contact_id'" ; 
            }      
        if (isset($query_options->relationship_id)) {
            $query_array['where'] = "nonasset_metadata_relationships.relationship_id='$query_options->relationship_id'" ; 
            }
        
        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;

        return $result ; 
        }
    
    
    public function Action_Delete_Contact_Metadata($contact_metadata_relationship_id) {
        
        $result = $this->Delete_Contact_Metadata($contact_metadata_relationship_id) ; 
        
        $this->Set_Contact($this->contact_id) ; 
        return $this ; 
        }
    
    
    public function Delete_Contact_Metadata($contact_metadata_relationship_id) {
        
        $query_array = array(
            'table' => "nonasset_metadata_relationships",
            'where' => "nonasset_metadata_relationships.relationship_id='$contact_metadata_relationship_id'"
            );
        
        $result = $this->DB->Query('DELETE',$query_array);
            
        $this->contact_query_result = $result ;        
        
        return $result ;
        }
    
    
    public function Action_Update_Contact_Note($note_input) {

        $continue = 1 ; 
        
        if (!$this->contact_id) {
            $continue = 0 ; 
            $this->Set_Alert_Response(72) ; // Error saving contact note.
            $this->Append_Alert_Response('none',array(
                'admin_context' => 'No contact_id provided.',
                'location' => __METHOD__
                )) ;             
            }
        
        if ($continue == 1) {
            if (isset($note_input['note_id'])) {

                $result = $this->Set_Contact_Note_ID($note_input['note_id'])->Update_Contact_Note($note_input) ;

                } else {

                    $result = $this->Create_Contact_Note($note_input) ;

                    }
            
            
            // Assuming we attempted to update or create the note 
            // Provide a success or error
            if (!$result['error']) {

                $this->Set_Alert_Response(73) ; // Contact note created


                } else {

                    $this->Set_Alert_Response(72) ; // Error saving contact note.
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;            

                    }
            
            }
                
        return $this ; 
        }
    
    

    

    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    //////////////////////  
    
    

    
    // Decision tree for looking at contact submissions and determing how to create/update & manage duplicates
    // https://docs.google.com/presentation/d/1pt7xIPJdHraIC3G9QxrOwMoudqTh72o0ynBjrVb-iEA/edit#slide=id.p
    public function Action_Duplicate_Contact_Logic($submitted_contact,$additional_details) {
        
                
        $give_alt_id_precedence = 0 ; // This is set to 1 if yl_member_id scores a match, circumventing email / phone logic.

        if (($submitted_contact['contact_details']['yl_member_number']) AND (!$submitted_contact['yl_member_number'])) {
            $submitted_contact['yl_member_number'] = $submitted_contact['contact_details']['yl_member_number'] ; 
            }
        
        
        // Find duplicates 
        $submitted_contact_object = (object) $submitted_contact ; // Cast the options array as an object to make queries easier to write
        $submitted_contact_object->phone_number_international = $submitted_contact_object->phone_country_dialing_code.$submitted_contact_object->phone_number ; 
        if (!$submitted_contact_object->yl_member_number) {
            $submitted_contact_object->yl_member_number = $submitted_contact_object->contact_details['yl_member_number'] ;
            }
        
        
        $query_array_duplicates = array(
            'table' => "contacts",
            'join_tables' => array(),
            'fields' => "contacts.contact_id, contacts.user_id, contacts.account_id, 
                contacts.first_name, contacts.last_name, 
                contacts.visibility_id, 
                contacts.email_address, contacts.phone_country_dialing_code, contacts.phone_number, 
                CONCAT(contacts.phone_country_dialing_code,contacts.phone_number) AS phone_number_international, 
                contact_details.yl_member_number,
                system_visibility_levels.visibility_name, system_visibility_levels.visibility_title",
            'where' => "account_id='$this->account_id' AND user_id='$this->user_id' "
                ) ; 
        
        // Add in params for email and phone and yl number matching...
        $duplicate_search_string = ' AND (' ;
        if ($submitted_contact_object->email_address) {
            $duplicate_search_string .= " (contacts.email_address='$submitted_contact_object->email_address' AND contacts.email_address!='') OR " ; 
            }
        if ($submitted_contact_object->phone_number) {
            $duplicate_search_string .= " (contacts.phone_country_dialing_code='$submitted_contact_object->phone_country_dialing_code' AND contacts.phone_number='$submitted_contact_object->phone_number' AND contacts.phone_number!='') OR " ; 
            }        
        if ($submitted_contact_object->yl_member_number) {
            $duplicate_search_string .= " (contact_details.yl_member_number='$submitted_contact_object->yl_member_number') OR " ; 
            }
        

        $duplicate_search_string = rtrim($duplicate_search_string," OR ") ; 
        $duplicate_search_string .= ")" ; 
        
        $query_array_duplicates['where'] .= $duplicate_search_string ; 
        

        $query_array_duplicates['join_tables'][] = array(
            'table' => 'contact_details',
            'on' => 'contacts.contact_id',
            'match' => 'contact_details.contact_id',
            'type' => 'left'
            ); 
        
        $query_array_duplicates['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'contacts.visibility_id'
            );
        
        
        // Skip duplicate search if no match fields submitted
        if ((!$submitted_contact_object->email_address) AND (!$submitted_contact_object->phone_number) AND (!$submitted_contact_object->yl_member_number)) {
            $duplicate_result = array(
                'result_count' => 0,
                'results' => array(),
                'query' => 'Query skipped - no match fields submitted'
                ) ;
            } else {
                $duplicate_result = $this->DB->Query('SELECT_JOIN',$query_array_duplicates,'force') ;
                }
        
        

        $result = array(
            'action' => 'error',
            'dup_score_phone_number' => 0,
            'dup_score_email_address' => 0,
            'dup_score_yl_member_number' => 0,
            'dup_results' => $duplicate_result, // Commented out -- use this only for testing.
            'update_duplicates' => $additional_details['update_duplicates']
            ) ; 
        
        
        if ($duplicate_result['result_count'] > 0) {

            $i = 0 ; 
            foreach ($duplicate_result['results'] as $duplicate) {

                // Need to make sure pulled results are properly formatted (lowercase)
                $duplicate['email_address'] = Utilities::Validate_Email_Address($duplicate['email_address'])['email_address'] ; 
                
                if (($submitted_contact_object->email_address) AND ($duplicate['email_address'] == $submitted_contact_object->email_address)) {
                    $result['contact_id_email_match'] = $duplicate['contact_id'] ; 
                    $result['dup_score_email_address']++ ; 
                    }
                if (($submitted_contact_object->phone_number_international) AND ($duplicate['phone_number_international'] == $submitted_contact_object->phone_number_international)) {
                    $result['contact_id_phone_match'] = $duplicate['contact_id'] ; 
                    $result['dup_score_phone_number']++ ; 
                    }                                               
                if (($submitted_contact_object->yl_member_number) AND ($duplicate['yl_member_number'] == $submitted_contact_object->yl_member_number)) {
                    $result['contact_id_yl_member_number_match'] = $duplicate['contact_id'] ; 
                    $result['dup_score_yl_member_number']++ ; 
                    }                
                $i++ ; 
                }
            }        
        
        
        
        if ($result['contact_id_email_match']) {
            $result['contact_email_match'] = Utilities::Deep_Array($duplicate_result['results'], 'contact_id', $result['contact_id_email_match']) ; 
            $result['duplicate_contact_id'] = $result['contact_email_match']['contact_id'] ;
            switch ($result['contact_email_match']['visibility_name']) {
                case 'visibility_hidden':
                case 'visibility_deleted':    
                    $result['update_duplicates'] = 'true' ; // Since the duplicate contact is deleted, override the duplicate
                    break ; 
                }
            }
        if ($result['contact_id_phone_match']) {
            $result['contact_phone_match'] = Utilities::Deep_Array($duplicate_result['results'], 'contact_id', $result['contact_id_phone_match']) ; 
            $result['duplicate_contact_id'] = $result['contact_phone_match']['contact_id'] ;
            switch ($result['contact_phone_match']['visibility_name']) {
                case 'visibility_hidden':
                case 'visibility_deleted':    
                    $result['update_duplicates'] = 'true' ; // Since the duplicate contact is deleted, override the duplicate
                    break ; 
                }            
            }
        if ($result['contact_id_yl_member_number_match']) {
            $result['contact_yl_member_number_match'] = Utilities::Deep_Array($duplicate_result['results'], 'contact_id', $result['contact_id_yl_member_number_match']) ; 
            $result['duplicate_contact_id'] = $result['contact_yl_member_number_match']['contact_id'] ;
            switch ($result['contact_yl_member_number_match']['visibility_name']) {
                case 'visibility_hidden':
                case 'visibility_deleted':    
                    $result['update_duplicates'] = 'true' ; // Since the duplicate contact is deleted, override the duplicate
                    break ; 
                }            
            
            $give_alt_id_precedence = 1 ; 
            }        
        
        
        
        
        if ($give_alt_id_precedence == 1) { // %L40%
            
            switch ($result['dup_score_yl_member_number']) {
                case 1: 
                default:    
                    
                    if ($result['update_duplicates'] == 'true') { // %L32% <- %L40%   Is updating duplicates enabled? YES

                        if ($result['contact_yl_member_number_match']['email_address'] != '') {
                            unset($submitted_contact['email_address']) ;
                            }
                        if ($result['contact_yl_member_number_match']['phone_number'] != '') {
                            unset($submitted_contact['phone_type'],$submitted_contact['phone_country_dialing_code'],$submitted_contact['phone_number']) ;
                            }
                        
                        // If email or phone of the submission matches a contact OTHER than the YL number match, then unset those fields
                        if (($result['dup_score_email_address'] > 0) AND ($result['contact_id_email_match'] != $result['contact_id_yl_member_number_match'])) {
                            unset($submitted_contact['email_address']) ;
                            }   
                        if (($result['dup_score_phone_number'] > 0) AND ($result['contact_id_phone_match'] != $result['contact_id_yl_member_number_match'])) {
                            unset($submitted_contact['phone_type'],$submitted_contact['phone_country_dialing_code'],$submitted_contact['phone_number']) ;
                            }
                        
                        
                        if ($submitted_contact_object->contact_id) { // %L32% <- %L40%  Was a contact_id submitted? YES

                            // Does submitted contact_id match contact_id_yl_member_number_match? 
                            if ($submitted_contact_object->contact_id == $result['contact_id_yl_member_number_match']) {
                                // YES
                                $result['action'] = 'update' ;
                                $result['alert_id'] = 70 ; // Update successful. No email / phone change.
                                $result['update_contact_id'] = $result['contact_id_yl_member_number_match'] ;
                                $result['admin_message'] = '%L32% <- %L40% Update contact based on YL Member ID match with Contact ID submitted;  No change allowed to EXISTING phone number and email address.' ; 
                                } else {
                                    // NO
                                    $result['action'] = 'error' ; 
                                    $result['alert_id'] = 270 ; // YL Member ID used by another contact, different contact_id
                                    $result['admin_message'] = '%L33% <- %L40% YL Member ID already used by another contact' ; 
                                    }

                            } else { // %L32% <- %L40%  Was a contact_id submitted? NO Updating based on YL Member ID

                                // No contact_id submitted. Update on YL Member ID with no email / phone change.
                                $result['action'] = 'update' ; 
                                $result['alert_id'] = 70 ; // Update successful. No email / phone change.
                                $result['update_contact_id'] = $result['contact_id_yl_member_number_match'] ; 
                                $result['admin_message'] = '%L32% <- %L40% Update contact based on YL Member #, no Contact ID submitted; No change allowed to EXISTING phone number and email address.' ;
                                } 
                        }                    
                    
                    if ($result['update_duplicates'] == 'false') { // %L33% <- %L40%  Is updating duplicates enabled? NO
                        $result['action'] = 'alert' ; 
                        $result['alert_id'] = 171 ; 
                        $result['admin_message'] = '%L33% <- %L40% Not updating duplicates' ;
                        }
                    
                    break ; 
                }
            
            } else {
        

                // There's an email address match
                if ($result['contact_id_email_match']) { // %L1% Is there an email_address match? YES

                    if ($submitted_contact_object->phone_number_international) { // %L3% Was a phone_number submitted? YES

                        if ($result['contact_id_phone_match']) { // %L11% Is there a phone_number match?  YES

                            if ($result['contact_id_email_match'] == $result['contact_id_phone_match']) { // %L12% Does matching contact_id_phone_match == contact_id_email_match? // YES

                                    if ($result['update_duplicates'] == 'true') { // %L6% <- %L12%   Is updating duplicates enabled? YES

                                        if ($submitted_contact_object->contact_id) { // %L7% <- %L6% <- %L12%  Was a contact_id submitted? YES

                                            // Does submitted contact_id match contact_id_email_match? 
                                            if ($submitted_contact_object->contact_id == $result['contact_id_email_match']) {
                                                // %L9% <- %L7% <- %L6%  YES
                                                $result['action'] = 'update' ;
                                                $result['alert_id'] = 70 ; // Update successful. No email / phone change.
                                                $result['update_contact_id'] = $result['contact_id_email_match'] ;
                                                $result['admin_message'] = '%L9% Update contact based on email match; No change to phone number.' ; 
                                                } else {
                                                    // %L10% <- %L7% <- %L6%  NO
                                                    $result['action'] = 'error' ; 
                                                    $result['alert_id'] = 97 ; // Email address used by another contact, different contact_id
                                                    $result['admin_message'] = '%L10% <- %L7% <- %L6% Email address already used by another contact' ; 
                                                    }

                                            } else { // %L8% <- %L6% <- %L12%  Was a contact_id submitted? NO

                                                if ($result['contact_email_match']['phone_number'] != '') { // %L36% <- %L8% <- %L6% <- %L12%  Did the email match contact have a phone number? YES
                                                    // UPDATE THE EMAIL MATCH BUT REMOVE THE PHONE NUMBER (just for safety - it ought to be the same)

                                                    $result['action'] = 'update' ; 
                                                    $result['alert_id'] = 70 ; // Update successful. No email / phone change.
                                                    $result['update_contact_id'] = $result['contact_id_email_match'] ; 
                                                    $result['admin_message'] = '%L36% from %L12% Update contact based on email address; remove the phone number' ;
                                                    unset($submitted_contact['phone_country_dialing_code'],$submitted_contact['phone_number']) ;

                                                    } else {  // %L37% <- %L8% <- %L6% <- %L12%  Did the email match contact have a phone number? NO
                                                        $result['action'] = 'update' ; 
                                                        $result['alert_id'] = 70 ; // Update successful. No email / phone change.
                                                        $result['update_contact_id'] = $result['contact_id_email_match'] ; 
                                                        $result['admin_message'] = '%L37% from %L12% Update contact based on email address; no phone number submitted' ;
                                                        }

                                                } 
                                        }                        

                                    if ($result['update_duplicates'] == 'false') { // %L5% <- %L12%   Is updating duplicates enabled? NO
                                        $result['action'] = 'alert' ; 
                                        $result['alert_id'] = 171 ; 
                                        $result['admin_message'] = '%L5% <- %L12% Not updating duplicates' ;
                                        }




                                } else { // %L13% Does matching contact_id_phone_match == contact_id_email_match? // NO

                                    // Below here, there was a phone_numnber match found, but it doesn't match the email_address match

                                    if ($result['update_duplicates'] == 'true') { // %L14%  Is updating duplicates enabled? YES

                                        if ($submitted_contact_object->contact_id) { // %L16% Was a contact_id submitted? YES

                                            // Does submitted contact_id match contact_id_email_match? 
                                            if ($submitted_contact_object->contact_id == $result['contact_id_email_match']) {
                                                // %L18% <- %L16% YES
                                                // UPDATE THE EMAIL MATCH BUT REMOVE THE PHONE NUMBER  
                                                unset($submitted_contact['phone_country_dialing_code'],$submitted_contact['phone_number']) ; 
                                                $result['action'] = 'update' ; 
                                                $result['alert_id'] = 172 ; // Update successful via email_address match; remove phone
                                                $result['update_contact_id'] = $result['contact_id_email_match'] ;
                                                $result['admin_message'] = '%L18% <- %L16% Update contact based on email, removed phone input because it is a non-match duplicate' ; 
                                                } else {
                                                    // %L19% <- %L16% NO
                                                    $result['action'] = 'error' ; 
                                                    $result['alert_id'] = 97 ; // Update successful via email_address match; remove phone
                                                    $result['admin_message'] = '%L19% <- %L16% Email address already used by another contact' ; 
                                                    }

                                            } else { // %L17% Was a contact_id submitted? NO
                                                // UPDATE THE EMAIL MATCH BUT REMOVE THE PHONE NUMBER
                                                $phone_number_formatted = Utilities::Format_Phone_Number($submitted_contact['phone_number'],$submitted_contact['phone_country_dialing_code']) ;
                                                $result['action'] = 'update' ; 
                                                $result['alert_id'] = 172 ; // Update successful via email_address match; remove phone
                                                $result['update_contact_id'] = $result['contact_id_email_match'] ;
                                                $result['admin_message'] = '%L17% Update contact based on email, removed phone input because it is a non-match duplicate: '.$phone_number_formatted['international_pretty'] ; 
                                                unset($submitted_contact['phone_country_dialing_code'],$submitted_contact['phone_number']) ; 
                                                }                                
                                        }

                                    if ($result['update_duplicates'] == 'false') { // %L15% <- %L13%  Is updating duplicates enabled? NO
                                        $result['action'] = 'alert' ; 
                                        $result['alert_id'] = 171 ; 
                                        $result['admin_message'] = '%L15% <- %L13% Not updating duplicates' ;
                                        }                        
                                    }                    
                            }


                        // Below here, there is an email match but no existing phone match
                        // A phone number was submitted
                        if (!$result['contact_id_phone_match']) { // %L4% <- %L3% Is there a phone_number match?  NO

                            if ($result['update_duplicates'] == 'true') { // %L6% <- %L4% <- %L3%  Is updating duplicates enabled? YES
                                if ($submitted_contact_object->contact_id) { // %L7% <- %L6% <- %L4%  Was a contact_id submitted? YES

                                    // Does submitted contact_id match contact_id_email_match? 
                                    if ($submitted_contact_object->contact_id == $result['contact_id_email_match']) {
                                        //  %L9% <- %L7% <- %L6%  YES
                                        $result['action'] = 'update' ; 
                                        $result['alert_id'] = 70 ; // Update successful. No email / phone change allowed.
                                        $result['update_contact_id'] = $result['contact_id_email_match'] ;
                                        $result['admin_message'] = '%L9% <- %L7% <- %L6% Update contact based on contact_id and email match; all phone updates allowed if no duplicates exist' ;
                                        } else {
                                            // %L10% <- %L7% <- %L6%  NO
                                            $result['action'] = 'alert' ; 
                                            $result['alert_id'] = 174 ; // Update successful, but removed email_address change from set.
                                            $result['update_contact_id'] = $submitted_contact_object->contact_id ;
                                            $result['admin_message'] = '%L10% from %L6% Update successful; remove email address input because email used by another contact: '.$submitted_contact['email_address'] ; 
                                            unset($submitted_contact['email_address']) ;
                                            }

                                    } else { // %L8% <- %L6% <- %L4% <- %L3%   Was a contact_id submitted? NO

                                        if ($result['contact_email_match']['phone_number'] != '') { // %L36% <- %L8% <- %L6% <- %L4% <- %L3%  Does the email match contact have a phone number? YES

                                            // UPDATE THE EMAIL MATCH BUT REMOVE THE PHONE NUMBER
                                            // Removing the phone number because there is already a phone set for the contact email match,
                                            // but since we're not matching by contact_id, we don't want to accidently override the existing 
                                            // phone_number based off of what could be a bulk update.
                                            $phone_number_formatted = Utilities::Format_Phone_Number($submitted_contact['phone_number'],$submitted_contact['phone_country_dialing_code']) ;
                                            $result['action'] = 'update' ; 
                                            $result['alert_id'] = 173 ; // Update successful via email_address match; removed phone
                                            $result['update_contact_id'] = $result['contact_id_email_match'] ;
                                            $result['admin_message'] = '%L36% from %L3% Update contact based on email address; removed phone input because a phone number already exists for the contact: '.$phone_number_formatted['international_pretty'] ; 
                                            unset($submitted_contact['phone_country_dialing_code'],$submitted_contact['phone_number']) ;

                                            } else {  // %L37% <- %L8% <- %L6% <- %L4% <- %L3%  Was an email_address submitted? NO
                                                $result['action'] = 'update' ; 
                                                $result['alert_id'] = 70 ; // Update successful. Could be a phone number change here if prior phone was blank.
                                                $result['update_contact_id'] = $result['contact_id_email_match'] ; 
                                                $result['admin_message'] = '%L37% from %L3% Update contact based on email address; phone number update allowed from blank' ;
                                                }

                                        }                        
                                }

                            if ($result['update_duplicates'] == 'false') { // %L5% <- %L4% <- %L3% Is updating duplicates enabled? NO
                                $result['action'] = 'alert' ; 
                                $result['alert_id'] = 171 ; 
                                $result['admin_message'] = '%L5% <- %L4% <- %L3% Not updating duplicates' ;
                                }

                            }

                        } 


                    // Below here, there is an email match. No phone number submitted.
                    if (!$submitted_contact_object->phone_number_international) { // %L4% Was a phone_number submitted? NO

                        if ($result['update_duplicates'] == 'true') { // %L6% <- %L4% Is updating duplicates enabled? YES
                            if ($submitted_contact_object->contact_id) { // %L7% <- %L6% <- %L4%  Was a contact_id submitted? YES

                                // Does submitted contact_id match contact_id_email_match? 
                                if ($submitted_contact_object->contact_id == $result['contact_id_email_match']) {
                                    // %L9% YES
                                    $result['action'] = 'update' ; 
                                    $result['alert_id'] = 70 ; // Update successful.
                                    $result['update_contact_id'] = $result['contact_id_email_match'] ;
                                    $result['admin_message'] = '%L9% <- %L7% <- %L6% <- %L4% Update successful' ;
                                    } else {
                                        // %L10% NO
                                        $result['action'] = 'update' ; 
                                        $result['alert_id'] = 174 ; // Update successful; email removed.
                                        $result['admin_message'] = '%L10% <- %L7% <- %L6% <- %L4% Update contact but email address removed; Email address already used by another contact: '.$submitted_contact['email_address'] ; 
                                        unset($submitted_contact['email_address']) ;
                                        }

                                } else { // %L8% <- %L6% <- %L4%  Was a contact_id submitted? NO

                                    if ($result['contact_email_match']['phone_number'] != '') { // %L36% <- %L8% <- %L6% <- %L4%  Does the email match contact have an existing phone_number? YES
                                        // UPDATE THE EMAIL MATCH BUT REMOVE THE PHONE NUMBER
                                        $result['action'] = 'update' ; 
                                        $result['alert_id'] = 70 ; // Update successful.
                                        $result['update_contact_id'] = $result['contact_id_email_match'] ; 
                                        $result['admin_message'] = '%L36% from %L4% Update contact based on email address; remove phone number just to clear country dialing code.' ;
                                        unset($submitted_contact['phone_number'],$submitted_contact['phone_country_dialing_code']) ; 

                                        } else {  // %L37% <- %L8% <- %L6% <- %L4%  Does the email match contact have an existing phone_number? NO
                                            $result['action'] = 'update' ; 
                                            $result['alert_id'] = 70 ; // Update successful.
                                            $result['update_contact_id'] = $result['contact_id_email_match'] ; 
                                            $result['admin_message'] = '%L37% from %L4% Update contact based on email address; New phone number allowed but was not submitted.' ;
                                            unset($submitted_contact['phone_number'],$submitted_contact['phone_country_dialing_code']) ; 
                                            }                        

                                    }
                            }


                        if ($result['update_duplicates'] == 'false') { // %L5% <- %L4% Is updating duplicates enabled? NO
                            $result['action'] = 'alert' ; 
                            $result['alert_id'] = 171 ; 
                            $result['admin_message'] = ' %L5% <- %L4% Not updating duplicates' ; 
                            }

                        }            

                    }


                if (!$result['contact_id_email_match']) { // %L2% Is there an email_address match? NO


                    // Below here, a phone_number was submitted
                    if ($submitted_contact_object->phone_number_international) { // %L20%  Was a phone_number submitted? YES


                        // Below here, phone match found
                        if ($result['contact_id_phone_match']) { // %L22%  Is there a phone_number match? YES

                            if ($result['update_duplicates'] == 'true') { // %L24%  Is updating duplicates enabled? YES

                                if ($submitted_contact_object->contact_id) { // %L26%  Was a contact_id submitted? YES

                                    // Does submitted contact_id match contact_id_phone_match? 
                                    if ($submitted_contact_object->contact_id == $result['contact_id_phone_match']) {
                                        // %L28%  YES
                                        $result['action'] = 'update' ; 
                                        $result['alert_id'] = 70 ; // Update successful. May be an email change / no phone change.
                                        $result['update_contact_id'] = $result['contact_id_phone_match'] ;
                                        $result['admin_message'] = '%L28% Update contact based on phone number' ; 
                                        } else {
                                            // %L29%  NO
                                            $phone_number_formatted = Utilities::Format_Phone_Number($submitted_contact['phone_number'],$submitted_contact['phone_country_dialing_code']) ;

                                            $result['action'] = 'update' ; 
                                            $result['alert_id'] = 172 ; // Update contact, but need to remove phone number; in use by another contact
                                            $result['admin_message'] = '%L29% Contact updated; removed phone input because number already used by a different contact: '.$phone_number_formatted['international_pretty'] ; 
                                            unset($submitted_contact['phone_number'],$submitted_contact['phone_country_dialing_code']) ;
                                            }                            
                                    } 

                                if (!$submitted_contact_object->contact_id) { // %L27%  Was a contact_id submitted? NO

                                    if (($result['contact_phone_match']['email_address'] != '') AND (isset($submitted_contact['email_address']))) { // %L34% Does the phone match contact have an email address? YES
                                        // UPDATE THE PHONE MATCH BUT REMOVE THE EMAIL ADDRESS (protects the existing email address)

                                        $result['action'] = 'update' ;
                                        $result['alert_id'] = 175 ; // Update successful. Remove email address to protect existing contact
                                        $result['update_contact_id'] = $result['contact_id_phone_match'] ; 
                                        $result['admin_message'] = '%L34% Update contact based on phone number; remove email address to protect existing' ;
                                        unset($submitted_contact['email_address']) ; 

                                        } else {  // %L35% Was an email_address submitted? NO
                                            $result['action'] = 'update' ; 
                                            $result['alert_id'] = 70 ; // Update successful. No phone change; email may be submitted
                                            $result['update_contact_id'] = $result['contact_id_phone_match'] ; 
                                            $result['admin_message'] = '%L35% Update contact based on phone number' ;
                                            }

                                    }

                                }

                            if ($result['update_duplicates'] == 'false') { // %L25%  Is updating duplicates enabled? NO
                                $result['action'] = 'alert' ; 
                                $result['alert_id'] = 171 ; 
                                $result['admin_message'] = '%L25% Not updating duplicates' ; 
                                }

                            }


                        // Below here, no phone match found
                        if (!$result['contact_id_phone_match']) { // %L23% <- %L20% Is there a phone_number match? YES

                            if ($submitted_contact_object->contact_id) { // %L30% <- %L23% <- %L20% <- %L20%  Was a contact_id submitted? YES

                                if ($result['update_duplicates'] == 'true') { // %L32% <- %L30% <- %L23% <- %L20%  Is updating duplicates enabled? YES

                                    $result['action'] = 'update' ; 
                                    $result['alert_id'] = 70 ; // Update successful, matched to contact_id only. Can include new phone & email address
                                    $result['update_contact_id'] = $submitted_contact_object->contact_id ;
                                    $result['admin_message'] = '%L32% Update based on submitted contact_id' ; 

                                    }

                                if ($result['update_duplicates'] == 'false') { // %33% <- %L30% <- %L23% <- %L20%   Is updating duplicates enabled? NO
                                    $result['action'] = 'alert' ; 
                                    $result['alert_id'] = 171 ; 
                                    $result['admin_message'] = '%33% <- %L30% <- %L23% Not updating duplicates' ; 
                                    }

                                }


                            if (!$submitted_contact_object->contact_id) { // %L31% <- %L23% <- %L20%  Was a contact_id submitted? NO

                                if ($result['dup_score_yl_member_number'] > 0) {

                                    if ($result['update_duplicates'] == 'true') { // %L39% <- %L31% <- %L23% <- %L20%  Is updating duplicates enabled? YES

                                        $result['action'] = 'update' ; 
                                        $result['alert_id'] = 70 ; // Update successful, matched to contact_id only. Can include new phone & email address
                                        $result['update_contact_id'] = $result['contact_id_yl_member_number_match'] ;
                                        $result['admin_message'] = '%L39% <- %L31% <- %L23% <- %L20% Updated based on submitted yl_member_number' ;                            
                                        } 

                                    if ($result['update_duplicates'] == 'false') { // %L39% <- %L31% <- %L23% <- %L20%   Is updating duplicates enabled? NO

                                        $result['action'] = 'alert' ; 
                                        $result['alert_id'] = 171 ; 
                                        $result['admin_message'] = '%L39% <- %L31% <- %L23% <- %L20% Not updating duplicates' ; 
                                        }                        

                                    } else {

                                        $result['action'] = 'create' ; 
                                        $result['admin_message'] = '%L39% <- %L31% <- %L23% <- %L20% Create a brand new contact' ; 
                                        } 

                                }

                            }

                        } 


                    // Below here, a phone_number was NOT submitted
                    if (!$submitted_contact_object->phone_number_international) { // %L21%  Was a phone_number submitted? NO 

                        if ($submitted_contact_object->contact_id) { // %L30% <- %L21%  Was a contact_id submitted? YES

                            if ($result['update_duplicates'] == 'true') { // %32% <- %L30% <- %L21%  Is updating duplicates enabled? YES

                                $result['action'] = 'update' ; 
                                $result['alert_id'] = 70 ; // Update successful, matched to contact_id only. Can include new phone & email address
                                $result['update_contact_id'] = $submitted_contact_object->contact_id ;
                                $result['admin_message'] = '%32% <- %L30% <- %L21% Updated based on submitted contact_id' ; 
                                }

                            if ($result['update_duplicates'] == 'false') { // %33% <- %L30% <- %L21%   Is updating duplicates enabled? NO
                                $result['action'] = 'alert' ; 
                                $result['alert_id'] = 171 ; 
                                $result['admin_message'] = '%33% <- %L30% <- %L21% Not updating duplicates' ; 
                                }

                            }


                        if (!$submitted_contact_object->contact_id) { // %L31% <- %L21%  Was a contact_id submitted? NO

                            if ($result['dup_score_yl_member_number'] > 0) {

                                if ($result['update_duplicates'] == 'true') { // %32% <- %38% <- %L31% <- %L21%  Is updating duplicates enabled? YES

                                    $result['action'] = 'update' ; 
                                    $result['alert_id'] = 70 ; // Update successful, matched to contact_id only. Can include new phone & email address
                                    $result['update_contact_id'] = $result['contact_id_yl_member_number_match'] ;
                                    $result['admin_message'] = '%32% <- %38% <- %L31% <- %L21% Updated based on submitted yl_member_number' ;                            
                                    } 

                                if ($result['update_duplicates'] == 'false') { // %32% <- %38% <- %L31% <- %L21%   Is updating duplicates enabled? NO

                                    $result['action'] = 'alert' ; 
                                    $result['alert_id'] = 171 ; 
                                    $result['admin_message'] = '%32% <- %38% <- %L31% <- %L21% Not updating duplicates' ; 
                                    }                        

                                } else {

                                    $result['action'] = 'create' ; 
                                    $result['admin_message'] = '%L39% <- %L31% <- %L21% Create a brand new contact' ; 
                                    } 

                            } 

                        }
                    }        

                } // End alt_id not active switch
        
        $result['submitted_contact'] = $submitted_contact ; 
        
        $system = new System() ; 
        $system->Set_Alert_Response(70) ; // Contact: Update success
        $result['duplicate_alert'] = $system->Get_Response() ;
        
        if (($submitted_contact_object->email_address) AND ($result['dup_score_email_address'] > 0) AND ($submitted_contact_object->contact_id != $result['contact_id_email_match'])) {

            // If the matching email address is a deleted contact, then delete the email address 
            // from the deleted contact and allow the submitted update to proceed
            switch ($result['contact_email_match']['visibility_name']) {
                case 'visibility_hidden':
                case 'visibility_deleted':    

                    // The duplicate matching contact is currently deleted. 
                    // Will allow new contact be created with matching email
                    // and remove email address from the deleted contact in other scripts.
                    
                    break ; 
                default: 
                    if ($result['update_duplicates'] == 'false') {
                        $system->Set_Alert_Response(268) ; // Contact: Email address in use by different contact
                        $result['duplicate_alert'] = $system->Get_Response() ;     
                        }                    
                }                
            }            
        if (($submitted_contact_object->phone_number) AND ($result['dup_score_phone_number'] > 0) AND ($submitted_contact_object->contact_id != $result['contact_id_phone_match'])) {

            // If the matching phone number is a deleted contact, then delete the phone number 
            // from the deleted contact and allow the submitted update to proceed
            switch ($duplicate_logic['contact_phone_match']['visibility_name']) {
                case 'visibility_hidden':
                case 'visibility_deleted':    

                    // The duplicate matching contact is currently deleted. 
                    // Will allow new contact be created with matching phone
                    // and remove phone from the deleted contact in other scripts.
                    
                    break ; 
                default:   
                    if ($result['update_duplicates'] == 'false') {
                        $system->Set_Alert_Response(267) ; // Contact: Phone number in use by different contact
                        $result['duplicate_alert'] = $system->Get_Response() ;
                        }
                }                
            }
        if (($submitted_contact_object->yl_member_number) AND ($result['dup_score_yl_member_number'] > 0) AND ($submitted_contact_object->contact_id != $result['contact_id_yl_member_number_match'])) {

            // If the matching phone number is a deleted contact, then delete the phone number 
            // from the deleted contact and allow the submitted update to proceed
            switch ($duplicate_logic['contact_yl_member_number_match']['visibility_name']) {
                case 'visibility_hidden':
                case 'visibility_deleted':    

                    // The duplicate matching contact is currently deleted. 
                    // Will allow new contact be created with matching YL number
                    // and remove YL number from the deleted contact in other scripts.
                    
                    break ; 
                default: 
                    if ($result['update_duplicates'] == 'false') {
                        $system->Set_Alert_Response(269) ; // Contact: YL member number in use by by different contact
                        $result['duplicate_alert'] = $system->Get_Response() ;
                        }
                }                
            }        
        
        
        
        if ($result['duplicate_contact_id']) {
            $result['contact_id'] = $result['duplicate_contact_id'] ; 
            }            
        
        return $result ; 
        }
    
    
    
    // Enables creation of multiple contacts at once with contact set delivered as an array of contacts
    public function Create_Contact($create_set,$error_set = array(),$additional_details = array()) { 
                
        $continue = 1 ; 

        $result_set = array() ; 
        
        $result_set['submitted_count'] = count($create_set) + count($error_set) ;        
        
        $result_set['success_count'] = 0 ; 
        $result_set['alert_count'] = 0 ; 
        $result_set['error_count'] = count($error_set) ; 
            
        $result_set['created_count'] = 0 ; 
        $result_set['updated_count'] = 0 ; 
        
        $result_set['success_set'] = array() ; 
        $result_set['alert_set'] = array() ; 
        $result_set['error_set'] = $error_set ; 
        $result_set['submitted_set'] = $create_set ; 
        

        if (count($create_set) == 0) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            $create_set = Utilities::Array_Force_Multidimensional($create_set) ; 

        
            foreach ($create_set as $contact) {

                // Separate out update_duplicates tag from the input array
                $additional_details['update_duplicates'] = $contact['update_duplicates'] ; 
                unset($contact['update_duplicates']) ; 
                            
                    

                $first_name = $contact['first_name'] ;
                $last_name = $contact['last_name'] ;
                if (isset($contact['email_address'])) {
                    $email_address = $contact['email_address'] ;
                    }
                if (isset($contact['phone_number'])) {
                    $phone_number = $contact['phone_number'] ;
                    }
                
                
                // Format the phone_number (remove spaces, dashes, etc.)
                if ($contact['phone_number']) { 
                    
                    $this->Action_Validate_Phone_Number($contact['phone_number'],$contact) ;
                    $phone_number_formatted = $this->Get_Phone_Number_Validation() ; 
                    $contact['phone_number'] = $phone_number_formatted['local'] ; 
                    $contact['phone_country_dialing_code'] = $phone_number_formatted['phone_country_dialing_code'] ; 
                    }

                // If a country_value (name) is given, replace with the ISO ID
                if ($contact['country_value']) {

                    $country_value = $contact['country_value'] ; 

                    $query_array = array(
                        'table' => 'list_countries',
                        'fields' => "country_id",
                        'where' => "country_value='$country_value'"
                        );

                    $country_result = $this->DB->Query('SELECT',$query_array);

                    $contact['country_id'] = $country_result['results']['country_id'] ; 
                    unset($contact['country_value']) ; 
                    }

                
                // Check to see if a duplicate contact already exists in the database
                // Running through this will remove contact_id from the values set and set it as update_contact_id
                $duplicate_logic = $this->Action_Duplicate_Contact_Logic($contact,$additional_details) ; 
                $contact = $duplicate_logic['submitted_contact'] ; // Reset the inputs after being processed by duplicate logic
                

               
                // Unsets duplicate info 
                if (($contact['email_address']) AND ($duplicate_logic['dup_score_email_address'] > 0) AND ($contact['contact_id'] != $duplicate_logic['contact_id_email_match'])) {
                    if ($contact['duplicate_match_override'] == 'true') { // true forces a NEW contact to be created but unsets the matched field
                        $duplicate_logic['action'] = 'create' ; 
                        unset($contact['email_address']) ; 
                        }                
                    }            
                if (($contact['phone_number']) AND ($duplicate_logic['dup_score_phone_number'] > 0) AND ($contact['contact_id'] != $duplicate_logic['contact_id_phone_match'])) {
                    if ($contact['duplicate_match_override'] == 'true') { // true forces a NEW contact to be created but unsets the matched field
                        unset($contact['phone_number'],$contact['phone_country_dialing_code'],$contact['phone_type']) ; 
                        $duplicate_logic['action'] = 'create' ; 
                        }                
                    }
                if (($contact['yl_member_number']) AND ($duplicate_logic['dup_score_yl_member_number'] > 0) AND ($contact['contact_id'] != $duplicate_logic['contact_id_yl_member_number_match'])) {
                    if ($contact['duplicate_match_override'] == 'true') { // true forces a NEW contact to be created but unsets the matched field
                        unset($contact['yl_member_number'],$contact['contact_details']['yl_member_number']) ; 
                        $duplicate_logic['action'] = 'create' ; 
                        }               
                    }

                
                
                $contact_post_create_data = $contact['contact_post_create_data'] ; 
                unset($contact['contact_post_create_data']) ;
                unset($contact['duplicate_match_override']) ; 
                unset($contact['yl_member_number']) ; // Cannot be part of the core create array
                
                
                // Set post_create data sets
                $contact_update_content = array() ; 
                $contact_note_content = array() ; 

                if (count($contact_post_create_data) > 0) {
                    foreach ($contact_post_create_data as $post_create_key => $post_create_value) {
                        switch ($post_create_key) {
                            case 'contact_status_metadata_id':
                            case 'contact_rating_metadata_id':
                            case 'yl_contact_group_metadata_id':
                            case 'yl_member_type_metadata_id':
                            case 'yl_team_status_metadata_id':    
                            case 'yl_essential_rewards_status_metadata_id':
                            case 'yl_account_status_metadata_id':    
                            case 'metadata_id':      
                            case 'tags':    
                                $contact_update_content[$post_create_key] = $post_create_value ; 
                                break ;
                            case 'note_title':
                            case 'note_content':
                                $contact_note_content[$post_create_key] = $post_create_value ;    
                                break ;
                            }
                        }
                    }
                
                
                // Set the values array for the new / updated contact
                $contact_details = $contact['contact_details'] ; // Temporarily keep contact details and update after contact is created
                unset($contact['contact_details']) ;
                

                $values_array = array() ; 
                foreach ($contact as $key => $value) {
                    $values_array[$key] = $value ; 
                    }

                $values_array['account_id'] = $this->account_id ;
                $values_array['user_id'] = $this->user_id ; 
                $values_array['timestamp_contact_updated'] = TIMESTAMP ;

                
                
                
                // Run the create / update query
                switch ($duplicate_logic['action']) {
                    case 'create':
                        
                        $values_array['timestamp_contact_created'] = TIMESTAMP ;
                        
                        $query_array = array(
                            'table' => "contacts",
                            'values' => $values_array
                            );                        
                        $result = $this->DB->Query('INSERT',$query_array) ;
                        
                        if (!$result['error']) {
                            $contact_details['contact_id'] = $result['insert_id'] ; 
                            
                            $contact_details_update_set = array() ; 
                            $contact_details_update_set[] = $contact_details ; 

                            $result_details = $this->Update_Contact_Details($contact_details_update_set) ; 
                            }
                        
                        break ; 
                    case 'update':
                        
                        $update_contact_id = $duplicate_logic['update_contact_id'] ; 
                        
                        $query_array = array(
                            'table' => "contacts",
                            'values' => $values_array,
                            'where' => "contact_id='$update_contact_id'"
                            ) ; 
                        $result = $this->DB->Query('UPDATE',$query_array) ;
                        
                        
                        if (!$result['error']) {
                            $contact_details['contact_id'] = $update_contact_id ; 
                            
                            $contact_details_update_set = array() ; 
                            $contact_details_update_set[] = $contact_details ; 
                            
                            $result_details = $this->Update_Contact_Details($contact_details_update_set) ; 
                            }
                        
                        if (count($contact_update_content) > 0) {
                            $contact_update_content['contact_id'] = $update_contact_id ; 
                            $this->Set_Contact_By_ID($contact_update_content['contact_id']) ; 
                            $existing_contact_record = $this->Get_Contact() ; 
                            
                            if (isset($existing_contact_record['contact_rating_relationship_id'])) {
                                $contact_update_content['contact_rating_relationship_id'] = $existing_contact_record['contact_rating_relationship_id'] ; 
                                }
                            if (isset($existing_contact_record['contact_status_relationship_id'])) {
                                $contact_update_content['contact_status_relationship_id'] = $existing_contact_record['contact_status_relationship_id'] ; 
                                }
                            if (isset($existing_contact_record['yl_essential_rewards_status_relationship_id'])) {
                                $contact_update_content['yl_essential_rewards_status_relationship_id'] = $existing_contact_record['yl_essential_rewards_status_relationship_id'] ; 
                                }
                            
                            $this->Action_Update_Contact($contact_update_content) ; 
                            }
                        
                        if (count($contact_note_content) > 0) {
                            $this->Set_Contact_ID($update_contact_id) ; 
                            $this->Action_Update_Contact_Note($contact_note_content); 
                            }                          
                        
                        
                        
                        break ;
                    case 'alert':
                        $result['alert'] = 'alert' ;
                        
                        break ; 
                    case 'error':
                    default:
                        $result['error'] = 'error' ;
                        
                    }
                


                $this->contact_query_result = $result ; 

                
                
                if ((!isset($result['error'])) AND (!isset($result['alert']))) {

                    if ($result['insert_id']) {

                        $this_contact_id = $result['insert_id'] ; 

                        // Create History Entry
                        $history_input = array(
                            'account_id' => $this->account_id,
                            'contact_id' => $result['insert_id'],
                            'function' => SCRIPT_ACTION,
                            'notes' => json_encode($query_array['values'])
                            ) ; 
                        $user_history = $this->Create_User_History($this->user_id,$history_input) ; 

                        
                        if (count($contact_update_content) > 0) {
                            
                            $contact_update_content['contact_id'] = $this_contact_id ; 
                            $this->Action_Update_Contact($contact_update_content) ; 
                            }
                        
                        if (count($contact_note_content) > 0) {
                            $this->Set_Contact_ID($this_contact_id) ; 
                            $this->Action_Update_Contact_Note($contact_note_content); 
                            }                        
                        
                        $this->Set_Alert_Response(93) ; // Contact created
                        
                        } else {

                            if ($duplicate_logic['alert_id']) {
                                $this->Set_Alert_Response($duplicate_logic['alert_id']) ; // Supplied by duplicate logic test
                                $this_contact_id = $duplicate_logic['contact_id'] ;
                                } else {
                                    $this->Set_Alert_Response(94) ; // General contact save error
                                    }                        
                            }

                    
                    $result['contact_duplicate_logic'] = $duplicate_logic['admin_message'] ; 

                    // This will apply to a success_count, alert_count, or error_count > 0             
                    $personalize['contact_record'] = $contact ;
                    $this->Append_Alert_Response($personalize,array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;
                    
                    
                    $result_set['success_count']++ ; 
                    switch ($duplicate_logic['action']) {
                        case 'create':
                            $result_set['created_count']++ ; 
                            break ; 
                        case 'update':
                            $result_set['updated_count']++ ; 
                            break ; 
                        }
                    
                    $result_set['success_set'][] = array(
                        'contact_id' => $this_contact_id,
                        'contact' => $contact,
                        'action' => $duplicate_logic['action'],
                        'duplicate_check' => $duplicate_logic,
                        'alert' => $this->Get_Response(),
                        'result' => $result
                        ) ;                 

                    unset($this_contact_id) ; 
                    
                    } else {
                    
                        if (isset($result['alert'])) {
                            
                            if ($duplicate_logic['alert_id']) {
                                $this->Set_Alert_Response($duplicate_logic['alert_id']) ; // Supplied by duplicate logic test
                                } else {
                                    $this->Set_Alert_Response(94) ; // General contact save error
                                    }
                            
                            $result['contact_duplicate_logic'] = $duplicate_logic['admin_message'] ; 
                            
                            // This will apply to a success_count, alert_count, or error_count > 0             
                            $personalize['contact_record'] = $contact ;
                            $this->Append_Alert_Response($personalize,array(
//                                'admin_context' => json_encode($result),
                                'location' => __METHOD__
                                )) ;                             
                            
                            $result_set['alert_count']++ ; 
                            $result_set['alert_set'][] = array(
                                'contact_id' => $duplicate_logic['contact_id'],
                                'contact' => $contact,
                                'duplicate_check' => $duplicate_logic,
                                'alert' => $this->Get_Response(),
                                'result' => $result
                                ) ; 
                            }
                    
                        if (isset($result['error'])) {
                            
                            if ($duplicate_logic['alert_id']) {
                                $this->Set_Alert_Response($duplicate_logic['alert_id']) ; // Supplied by duplicate logic test
                                } else {
                                    $this->Set_Alert_Response(94) ; // General contact save error
                                    }
                            
                            
                            $result['contact_duplicate_logic'] = $duplicate_logic['admin_message'] ; 
                            
                            // This will apply to a success_count, alert_count, or error_count > 0             
                            $personalize['contact_record'] = $contact ;
                            $this->Append_Alert_Response($personalize,array(
//                                'admin_context' => json_encode($result),
                                'location' => __METHOD__
                                )) ;
                            
                            $result_set['error_count']++ ; 
                            $result_set['error_set'][] = array(
                                'contact' => $contact,
                                'duplicate_check' => $duplicate_logic,
                                'alert' => $this->Get_Response(),
                                'result' => $result
                                ) ; 

                            }
                    
                
                        }
                    }
            

            }
        

        return $result_set ;      
        
        }
    
    
    
    // $do_not_contact_status should be 1 or 0. Will default to 1, which means do not contact this entry
    public function Action_Update_Contact_Restriction($restriction_input,$do_not_contact_status = 1) {
        
        $continue = 1 ; 
        
        if ((!$this->contact_id) OR (!$this->user_id) OR (!$this->account_id)) {            
            $continue = 0 ;             
            }
        
        if ((!isset($restriction_input['email_address'])) AND (!isset($restriction_input['phone_number_international']))) {            
            $continue = 0 ;             
            }
        
        
        if ($continue == 1) {                        
            $restriction_input['do_not_contact'] = $do_not_contact_status ; 
            $result = $this->Create_Contact_Restriction($restriction_input) ;                         
            }
        
        return $this ; 
        }
    
    
    // 1 = do_not_contact is active (not allowed to contact this destination); 0 = free to contact
    public function Create_Contact_Restriction($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $query_array = array(
            'table' => "contact_restrictions",
            'values' => array(
                'user_id' => $this->user_id,
                'account_id' => $this->account_id,
                'contact_id' => $this->contact_id,
                'do_not_contact' => $query_options->do_not_contact
                ),
            'where' => "contact_restrictions.contact_id='$this->contact_id' AND contact_restrictions.account_id='$this->account_id' AND contact_restrictions.user_id='$this->user_id'"
            ) ;

        if (isset($query_options->email_address)) {
            $query_array['values']['email_address'] = $query_options->email_address ;
            $query_array['where'] .= " AND contact_restrictions.email_address='$query_options->email_address'" ;
            }
        if (isset($query_options->phone_number_international)) {
            $query_array['values']['phone_number_international'] = $query_options->phone_number_international ;
            $query_array['where'] .= " AND contact_restrictions.phone_number_international='$query_options->phone_number_international'" ;
            }
                
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        $this->contact_query_result = $result ; 
        
        
        if (!isset($result['error'])) {
            
            // Create History Entry
            $history_input = array(
                'account_id' => $this->account_id,
                'contact_id' => $this->contact_id,
                'function' => 'update_contact_restrictions',
                'notes' => json_encode($query_array['values'])
                ) ; 
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;        
            }
        
        return $result ;         
        }
    
    
    
    public function Create_Contact_Note($note_input) {
        
        $note_count = 1 ; 
        
        $query_array = array(
            'table' => "contact_notes",
            'values' => array(
                'contact_id' => $this->contact_id,      
                'visibility_id' => "5",      
                'timestamp_created' => TIMESTAMP,
                'timestamp_updated' => TIMESTAMP
                )
            ) ;

        
        if (isset($note_input['note_title'])) {
            $query_array['values']['note_title'] = $note_input['note_title'] ; 
            }
        
        // Split multiple notes if necessary
        if (isset($note_input['note_content'])) {
             
            $note_content_array = Utilities::Process_Comma_Separated_String($note_input['note_content'],'####') ; 
            $note_count = count($note_content_array) ; 
            
            if ($note_count == 1) {
                $query_array['values']['note_content'] = $note_input['note_content'] ;
                } 
            } 
            
        
        
        // Import note
        // If multiple notes, loop the insert function and change the note content each time 
        // as it was split in $note_content_array above
        // Also increment title for multiple notes.
        if ($note_count == 1) {
            $result = $this->DB->Query('INSERT',$query_array) ;
            $this->contact_query_result = $result ; 
            } else {
            
                $i = 1 ; 
                foreach ($note_content_array as $note_content) {
                    
                    $query_array['values']['note_title'] = $note_input['note_title'].' '.$i ;
                    $query_array['values']['note_content'] = $note_content ;
                    $query_array['values']['timestamp_created'] = TIMESTAMP + 1 ;
                    $query_array['values']['timestamp_updated'] = TIMESTAMP + 1 ;
                    
                    $result = $this->DB->Query('INSERT',$query_array) ;
                    $this->contact_query_result = $result ; 
                    
                    $i++ ; 
                    }
            
                }
        
        if (!isset($result['error'])) {
            
            // Create History Entry
            $history_input = array(
                'account_id' => $this->account_id,
                'contact_id' => $this->contact_id,
                'function' => SCRIPT_ACTION,
                'notes' => json_encode($query_array['values'])
                ) ; 
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;        
            }
        
        return $result ;         
        }
    
    
    
    public function Retrieve_Contact_Metadata_List($contact_id = 'internal',$query_options = array()) { 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        if ('internal' === $contact_id) {
            $contact_id = $this->contact_id ; 
            }
        
        $query_array = array(
            'table' => 'nonasset_metadata_relationships',
            'fields' => "nonasset_metadata_relationships.*, metadata.*, metadata_type.*",
            'where' => "nonasset_metadata_relationships.item_id='$contact_id' AND nonasset_metadata_relationships.relationship_type='contact'",
            'order_by' => "metadata.metadata_name",
            'order' => "ASC"
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata',
            'on' => 'metadata.metadata_id',
            'match' => 'nonasset_metadata_relationships.metadata_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            );
        
        
        if (isset($query_options->metadata_type_id)) {
            $query_array['where'] .= "AND metadata_type.metadata_type_id='$query_options->metadata_type_id' " ; 
            }

        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $this->contact_query_result = $result ;         
        
        return $result ; 
        }
    
    
    
    
    public function Retrieve_Contact_Notes($contact_id = 'internal',$query_options = array()) { 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
         
        
        if ('internal' === $contact_id) {
            $contact_id = $this->contact_id ; 
            }
        
        $query_array = array(
            'table' => 'contact_notes',
            'fields' => "contact_notes.*",
            'where' => "contact_notes.contact_id='$contact_id' "
            );        
        
        
        // Set ordering...
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = "$query_options->order_by" ;  
            $query_array['order'] = $query_options->order ;  
            }        
        
        $query_array['join_tables'][] = array(
            'table' => 'contacts',
            'on' => 'contacts.contact_id',
            'match' => 'contact_notes.contact_id'
            );
        
        if (isset($query_options->note_id)) {
            $query_array['where'] .= "AND contact_notes.note_id='$query_options->note_id' " ; 
            }
        
        if ($query_options->filter_by_search_parameter) {
            $query_array['where'] .= "AND (contact_notes.note_title LIKE '%$query_options->filter_by_search_parameter%' OR 
                contact_notes.note_content LIKE '%$query_options->filter_by_search_parameter%') " ; 
            } 
        
        if (isset($query_options->filter_by_visiblity_id)) {
            $query_array['where'] .= "AND contact_notes.visibility_id='$query_options->filter_by_visiblity_id' " ; 
            }
        
        
        
        
        
        
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        if (($query_array['order_by_relevance'] == 'yes') AND (isset($query_options->filter_by_search_parameter))) {
            
            $relevance_options = array(
                'fields' => array('note_title','note_content') 
                ) ; 
            $result = $this->Sort_Search_Relevance($result,$query_array,$query_options,$relevance_options) ; 
            } 
                
        $query_options->value_name = 'note_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Contact_Paging($result) ;         
        $this->contact_query_result = $result ;
        
        
        
        return $result ;       
        }
    
    
    
    
    public function Retrieve_Do_Not_Contact($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'yes' ; 
            }
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        

        $query_array = array(
            'table' => 'contact_restrictions',
            'fields' => "contact_restrictions.*",
            'where' => "contact_restrictions.restriction_id>'0' "
            );        

        
        switch($query_options->filter_by_contact_id) {                    
            case 'no':
                
                break ;   
            case 'yes':                 
                $query_array['where'] .= "AND contact_restrictions.contact_id='$this->contact_id' " ;
                break ; 
            }
        
        
        switch($query_options->filter_by_user_id) {                    
            case 'no':
                
                break ;   
            case 'yes':                
            default: 
                $query_array['where'] .= "AND contact_restrictions.user_id='$this->user_id' " ;
            }        
        
        switch($query_options->filter_by_account_id) {
            case 'no':    
                
                break ;
            case 'yes': 
            default: 
                $query_array['where'] .= "AND contact_restrictions.account_id='$this->account_id' " ;
            }        
        
        
        if ($query_options->filter_by_email_address) {
            $query_array['where'] .= "AND contact_restrictions.email_address='$query_options->filter_by_email_address' " ; 
            }        
        
        if ($query_options->filter_by_phone_number_international) {
            $query_array['where'] .= "AND contact_restrictions.phone_number_international='$query_options->phone_number_international' " ; 
            }         
        
        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->contact_query_result = $result ; 
        
        
        // Get the total count of users in this query
        if (isset($query_options->total_count)) { 
            
            $result['total_count'] = $query_options->total_count ; 
            
            } else {                
                    
                $query_array['fields'] = "COUNT(contacts.contact_id) as total_count" ; 
                unset($query_array['limit'],$query_array['offset']) ;
            
                $count = $this->DB->Query('SELECT_JOIN',$query_array);

                $result['total_count'] = $count['results']['total_count'] ;

                }
        
        
        $result['offset_page'] = $query_options->offset_page ;         
        $this->Set_Contact_Paging($result) ;         
        
        return $result ;         
        }
    
    
    
    public function Retrieve_Contact_Import_Log($query_options = array( 
        'override_paging' => 'no'        
        )) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $query_array = array( 
            'table' => 'contact_import_log',
            'fields' => "contact_import_log.* ",
            'where' => "contact_import_log.file_task_id='$query_options->filter_by_file_task_id' "
            );        
        
//        $query_array['join_tables'][] = array(
//            'table' => 'contact_details',
//            'on' => 'contact_details.contact_id',
//            'match' => 'contacts.contact_id',
//            'type' => 'left'
//            );
        
        
        // Add import_status filter
        if (isset($query_options->filter_by_import_status)) {
            
            $filter_by_import_status_array = Utilities::Process_Comma_Separated_String($query_options->filter_by_import_status) ;
                            
            $filter_by_import_status_string = '' ; 
            foreach ($filter_by_import_status_array as $status_name) {
                $filter_by_import_status_string .= "contact_import_log.import_status='$status_name' OR " ; 
                }
            
            $filter_by_import_status_string = rtrim($filter_by_import_status_string," OR ") ;     
            $query_array['where'] .= " AND ($filter_by_import_status_string) " ;
            }
        
        
        if (isset($query_options->filter_by_import_log_id)) {
            $this->Set_Page_Increment(1) ; 
            switch ($query_options->adjacent_item) {
                case 'previous':
                    $query_array['where'] .= " AND (contact_import_log.import_log_id<'$query_options->filter_by_import_log_id') " ;
                    break ; 
                case 'next':
                    $query_array['where'] .= " AND (contact_import_log.import_log_id>'$query_options->filter_by_import_log_id') " ;
                    break ; 
                default: 
                    
                    $filter_by_import_log_id_array = Utilities::Process_Comma_Separated_String($query_options->filter_by_import_log_id) ;

                    $filter_by_import_log_id_string = '' ; 
                    foreach ($filter_by_import_log_id_array as $log_id) {
                        $filter_by_import_log_id_string .= "contact_import_log.import_log_id='$log_id' OR " ; 
                        }

                    $filter_by_import_log_id_string = rtrim($filter_by_import_log_id_string," OR ") ;     
                    $query_array['where'] .= " AND ($filter_by_import_log_id_string) " ;            
                }            
            }        
        
        
        if (isset($query_options->filter_by_search_parameter)) {
            $query_array['where'] .= " AND (contact_import_log.email_address LIKE '%$query_options->filter_by_search_parameter%' 
                OR contact_import_log.phone_number LIKE '%$query_options->filter_by_search_parameter%' 
                OR contact_import_log.yl_member_number LIKE '%$query_options->filter_by_search_parameter%'
                ) " ;
            }        
        
                
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ;            
            }
        

        // NEW PAGING PROCESS
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
        
        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force'); 
   
        $query_options->value_name = 'import_log_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Contact_Paging($result) ;             
//        $this->contact_query_result = $result ; 
        
//        $this->test_data = $result ;         
        
        return $result ; 
        }
    
    
    
    
    public function Retrieve_Contact_List($query_options = array( 
        'override_paging' => 'no'        
        )) {
        
        global $server_config ; 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        if ($this->user['timezone']) {
            $timezone = $this->user['timezone'] ; 
            } else {
                $timezone = $this->system_timezone ; 
                }        
        

        $query_array = array( 
            'table' => 'contacts',
            'fields' => "contacts.*, 
                    contact_details.yl_member_number, 
                    contact_details.rank_current, contact_details.rank_highest, 
                    contact_details.timestamp_yl_join_date, contact_details.timestamp_yl_activation_date, 
                    contact_details.yl_enroller_name, contact_details.yl_enroller_member_number, 
                    contact_details.yl_sponsor_name, contact_details.yl_sponsor_member_number,
                    
                
                
                CONCAT(contacts.first_name,' ',contacts.last_name) AS full_name, 
                DATE_FORMAT(contacts.date_birthday, '%m/%d') AS birthday_short,
                contact_favorites_metadata_relationships.relationship_id AS starred_favorite_relationship_id, 
                list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code, 
                system_visibility_levels.visibility_name, system_visibility_levels.visibility_title ",
            'where' => "contacts.contact_id>'0' ",
            'group_by' => "contacts.contact_id"
            );        
        
//        contact_restrictions_email.do_not_contact AS do_not_contact_email_address, 
//        contact_restrictions_phone.do_not_contact AS do_not_contact_phone_number, 
        
        if (isset($query_options->group_by)) {
            $query_array['group_by'] = $query_options->group_by ; 
            } else {
                $query_array['group_by'] = "contacts.contact_id" ; 
                }
        
        $query_array['join_tables'][] = array(
            'table' => 'contact_details',
            'on' => 'contact_details.contact_id',
            'match' => 'contacts.contact_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'contacts.visibility_id'
            );
        
//        $query_array['join_tables'][] = array(
//            'table' => 'contact_restrictions AS contact_restrictions_email',
//            'statement' => '(contact_restrictions_email.email_address=contacts.email_address 
//                AND contact_restrictions_email.account_id=contacts.account_id 
//                AND contact_restrictions_email.contact_id=contacts.contact_id)',
//            'type' => 'left'
//            );
//        
//        $query_array['join_tables'][] = array(
//            'table' => 'contact_restrictions AS contact_restrictions_phone',
//            'statement' => '(contact_restrictions_phone.phone_number_international=CONCAT(contacts.phone_country_dialing_code,contacts.phone_number) 
//                    AND contact_restrictions_phone.account_id=contacts.account_id 
//                    AND contact_restrictions_phone.contact_id=contacts.contact_id)',
//            'type' => 'left'
//            );
        
        $query_array['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'list_countries.country_id',
            'match' => 'contacts.country_id',
            'type' => 'left'
            );
        
        
        // Add starred favorites metadata query
        $starred_favorite_metadata_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'starred_favorite')['value'] ; 
        
        $query_array['join_tables'][] = array(
            'table' => 'nonasset_metadata_relationships AS contact_favorites_metadata_relationships',
            'statement' => "(contact_favorites_metadata_relationships.item_id=contacts.contact_id) AND (contact_favorites_metadata_relationships.relationship_type='contact') AND (contact_favorites_metadata_relationships.metadata_id='$starred_favorite_metadata_id')",
            'type' => 'left'
            );    
        
        

        $metadata_subquery_array = array() ;
        $metadata_subquery_array[] = array(
            'subquery_prefix' => 'rank_current', // Contact Current Rank Subquery
            ) ; 
        $metadata_subquery_array[] = array(
            'subquery_prefix' => 'rank_highest', // Contact Highest Rank Subquery
            ) ; 
        
        // Process and build rank metadata subqueries
        foreach ($metadata_subquery_array as $metadata_subquery) {

            // Subquery variables
            $metdata_subquery_prefix = $metadata_subquery['subquery_prefix'] ; 
            $metdata_subquery_table_name = $metdata_subquery_prefix."_metadata_relationships" ; // Subquery table name

            // Add in Subquery fields
            $query_array['fields'] .= ", 
                $metdata_subquery_table_name.metadata_slug AS ".$metdata_subquery_prefix."_metadata_slug, 
                $metdata_subquery_table_name.metadata_name AS ".$metdata_subquery_prefix."_metadata_name" ;

            // Subquery Statement
            $query_array['join_tables'][] = array(
                'table' => "(SELECT metadata.metadata_id, metadata.metadata_name, metadata.metadata_slug 
                    FROM metadata 
                    ) AS $metdata_subquery_table_name",
                'statement' => "($metdata_subquery_table_name.metadata_id=contact_details.$metdata_subquery_prefix)",
                'type' => 'left'
                ); 
            }
        
        

        
        
        // ** METADATA SUBQUERIES **
        $metadata_subquery_array = array() ;
        $metadata_subquery_array[] = array(
            'subquery_prefix' => 'contact_rating', // Contact Rating Subquery
            ) ; 
        $metadata_subquery_array[] = array(
            'subquery_prefix' => 'contact_status', // Contact Status Subquery
            ) ; 
        $metadata_subquery_array[] = array(            
            'subquery_prefix' => 'yl_contact_group', // YL Contact Group Subquery
            ) ; 
        $metadata_subquery_array[] = array(            
            'subquery_prefix' => 'yl_member_type', // YL Member Type Subquery
            ) ; 
        $metadata_subquery_array[] = array(            
            'subquery_prefix' => 'yl_team_status', // YL Team Status Subquery
            ) ;
        $metadata_subquery_array[] = array(            
            'subquery_prefix' => 'yl_essential_rewards_status', // YL Essential Rewards Subquery
            ) ;        
        $metadata_subquery_array[] = array(            
            'subquery_prefix' => 'yl_account_status', // YL Account Status Subquery
            ) ;                        
        
        // Process and build metadata subqueries
        foreach ($metadata_subquery_array as $metadata_subquery) {

            // Subquery variables
            $metdata_subquery_prefix = $metadata_subquery['subquery_prefix'] ; 
            $metdata_subquery_metadata_type_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', $metdata_subquery_prefix.'_metadata_type')['value'] ; // Identify the metadata_type_id
            $metdata_subquery_table_name = $metdata_subquery_prefix."_metadata_relationships" ; // Subquery table name

            // Add in Subquery fields
            $query_array['fields'] .= ", 
                $metdata_subquery_table_name.relationship_id AS ".$metdata_subquery_prefix."_relationship_id, 
                $metdata_subquery_table_name.metadata_slug AS ".$metdata_subquery_prefix."_metadata_slug, 
                $metdata_subquery_table_name.metadata_id AS ".$metdata_subquery_prefix."_metadata_id, 
                $metdata_subquery_table_name.metadata_name AS ".$metdata_subquery_prefix."_metadata_name" ;

            // Subquery Statement
            $query_array['join_tables'][] = array(
                'table' => "(SELECT nonasset_metadata_relationships.*, metadata.metadata_name, metadata.metadata_slug, metadata_type.* 
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON metadata.metadata_id = nonasset_metadata_relationships.metadata_id  
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id  
                    WHERE nonasset_metadata_relationships.relationship_type='contact' AND 
                        metadata_type.metadata_type_id=$metdata_subquery_metadata_type_id) AS $metdata_subquery_table_name",
                'statement' => "($metdata_subquery_table_name.item_id=contacts.contact_id)",
                'type' => 'left'
                ); 
            }
        // ** END: METADATA SUBQUERIES ** 

        

        
        // This needs to be submitted as an array of contact_id values.
        if (isset($query_options->contact_id)) {
            // If an array wasn't submitted, turn it into an array
            $contact_id_array = Utilities::Array_Force($query_options->contact_id) ;   
            $query_options->filter_by_contact_id = 'list' ; 
            }
        
        
        
        switch($query_options->filter_by_contact_id) {                    
            case 'no':
                
                break ;   
            case 'yes':                 
                $query_array['where'] .= "AND contacts.contact_id='$this->contact_id' " ;
                break ; 
            case 'list':
                
                $contact_id_query_string = '' ; 
                foreach ($contact_id_array as $contact_id) {
                    
                    $contact_id_query_string .= "contacts.contact_id='$contact_id' OR " ; 
                    }
                $contact_id_query_string = rtrim($contact_id_query_string," OR ") ; 
                $query_array['where'] .= "AND ($contact_id_query_string) " ;
                break ;                
            }
        
        
        switch($query_options->filter_by_user_id) {                    
            case 'no':
                
                break ;
            case 'yes':                
            default: 
                $query_array['where'] .= "AND contacts.user_id='$this->user_id' " ;
            }        
        
        
        switch($query_options->filter_by_account_id) {
            case 'no':    
                
                break ;
            case 'yes': 
            default: 
                $query_array['where'] .= "AND contacts.account_id='$this->account_id' " ;
            } 
        
        
    
        
        // Submitted as an array of visibility_names's which is converted into, and added to the visibility_name array
        if (isset($query_options->filter_by_visibility_name)) {

            $visibility_name_array = Utilities::Array_Force($query_options->filter_by_visibility_name) ; 
            
            $query_visibility_array = array(
                'table' => 'system_visibility_levels',
                'fields' => "*",
                'where' => ""
                ) ;

            foreach ($visibility_name_array as $visibility_name) {
                switch ($visibility_name) {
                    case 'admin_all': // We should provide special treatment for hiddden since it's admin only...
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_hidden' OR " ;  
                    case 'all':
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_deleted' OR " ; 
                    case 'visible':    
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_draft' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_shared' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_published' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_review' OR " ; 
                        break ; 
                    default:
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='$visibility_name' OR " ; 
                    }
                }
            $query_visibility_array['where'] = rtrim($query_visibility_array['where']," OR ") ;
            $visibility_result = $this->DB->Query('SELECT',$query_visibility_array,'force');   

            foreach ($visibility_result['results'] as $visibility) {
                $visibility_array[] = $visibility ; 
                }
            }

        
        
        // Add visibility filter if there are visibility_id's to process
        $filter_by_visibility_id_string = '' ; 
        $filter_by_published_visibility_id_string = '' ; 
        if (count($visibility_array) > 0) {

            foreach ($visibility_array as $visibility) {
                
                $this_visibility_id = $visibility['visibility_id'] ; 
                $filter_by_visibility_id_string .= "contacts.visibility_id='$this_visibility_id' OR " ;
                
                switch ($visibility['visibility_name']) {
                    case 'visibility_shared':
                    case 'visibility_published':
                        $filter_by_published_visibility_id_string .= "contacts.visibility_id='$this_visibility_id' OR " ;        
                        break ; 
                    }
                
                
                }
            $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," OR ") ; 
            $filter_by_visibility_id_string = " AND ($filter_by_visibility_id_string) " ;
            
            $query_array['where'] .= " $filter_by_visibility_id_string " ; 
            
//            $filter_by_published_visibility_id_string = rtrim($filter_by_published_visibility_id_string," OR ") ; // For items in account only
//            $filter_by_published_visibility_id_string = " AND ($filter_by_published_visibility_id_string) " ;
            
            }
        
        
        
        // Filter by tags
        if (isset($query_options->filter_by_tag_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_tag_metadata_id,","),",") ;
            $query_options->filter_by_tag_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;
            $count_metadata = count($query_options->filter_by_tag_metadata_id) ; 
                        
            $join_table_name = 'tag_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->tag_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='contact' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (contacts.contact_id=$join_table_name.item_id)"
                );                         
            }
        
        
        // Filter by categories
        if (isset($query_options->filter_by_category_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_category_metadata_id,","),",") ;
            $query_options->filter_by_category_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
            $count_metadata = count($query_options->filter_by_category_metadata_id) ; 
                        
            $join_table_name = 'category_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->category_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='contact' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (contacts.contact_id=$join_table_name.item_id)"
                );                         
            }
        
        
        // Filter by contact status
        if (isset($query_options->filter_by_contact_status_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_contact_status_metadata_id,","),",") ;
            $query_options->filter_by_contact_status_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_contact_status_metadata_id) ;
            $count_metadata = count($query_options->filter_by_contact_status_metadata_id) ; 
                        
            $join_table_name = 'contact_status_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->tag_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='contact' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (contacts.contact_id=$join_table_name.item_id)"
                );                         
            }
        
        
        
        // PROCESS CONTACT STARRED FILTER
        if (isset($query_options->filter_by_starred_status)) {

            $starred_item_metadata_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'starred_favorite')['value'] ; 
            $count_metadata = 1 ; 
            
            
            $join_table_name = 'starred_item_query' ;  
            $query_array['fields'] .= "$join_table_name.relationship_id AS starred_item, " ; 
            
            // Create the operator
            switch ($query_options->filter_by_starred_status) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'live':
                case 'unstarred':    
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                case 'starred':    
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            


            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($starred_item_metadata_id) 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (contacts.contact_id=$join_table_name.item_id)"
                ); 

            }
        
        
        
        // PROCESS CONTACT RATING FILTER
        if ($query_options->rating_filter_status == 'active') {
            
 
            switch ($query_options->rating_filter_type) {
                case 'not_rated':
                    
//                    $range_next = Utilities::System_Time_Manipulate($query_options->birthday_filter_range_next_days.' d','+',$today_midnight_timestamp) ; 
//                    $range_next_timestamp = $range_next['time_manipulated'] ;
//                    $query_array['where'] .= "AND (contacts.timestamp_birthday_next>$today_midnight_timestamp AND contacts.timestamp_birthday_next<$range_next_timestamp) " ; 
            
                    $query_array['where'] .= "AND (contact_rating_metadata_relationships.metadata_name IS NULL) " ; 
                    
                    break ;                     
                case 'range_between':
                    
                    $query_array['where'] .= "AND (contact_rating_metadata_relationships.metadata_name>=$query_options->rating_filter_range_between_begin AND contact_rating_metadata_relationships.metadata_name<=$query_options->rating_filter_range_between_end)" ; 
            
                    break ; 
                }            
            }
        
        
        
        // PROCESS CONTACT BIRTHDAY FILTER
        if ($query_options->birthday_filter_status == 'active') {
            
            $today_midnight = new DateTime() ; 
            $today_midnight->setTimezone(new DateTimeZone($timezone)) ;  
            $today_midnight_timestamp = $today_midnight->setTimestamp(TIMESTAMP)->setTime( 0, 0, 0 )->getTimestamp() ;

                    
            switch ($query_options->birthday_filter_type) {
                case 'range_next':
                    
                    $range_next = Utilities::System_Time_Manipulate($query_options->birthday_filter_range_next_days.' d','+',$today_midnight_timestamp) ; 
                    $range_next_timestamp = $range_next['time_manipulated'] ;
                    $query_array['where'] .= "AND (contacts.timestamp_birthday_next>$today_midnight_timestamp AND contacts.timestamp_birthday_next<$range_next_timestamp) " ; 
            
                    break ; 
                case 'range_last':
                    
                    $range_last = Utilities::System_Time_Manipulate($query_options->birthday_filter_range_last_days.' d','-',$today_midnight_timestamp) ; 
                    $range_last_timestamp = $range_last['time_manipulated'] ;
                    $query_array['where'] .= "AND (contacts.timestamp_birthday_last>$range_last_timestamp AND contacts.timestamp_birthday_last<$today_midnight_timestamp) " ; 
            
                    break ;                    
                case 'range_between':
                    
                    $range_between_begin = new DateTime($query_options->birthday_filter_range_between_begin) ; 
                    $range_between_begin->setTimezone(new DateTimeZone($timezone)) ;  
                    $range_between_begin_timestamp = $range_between_begin->getTimestamp() ;
                    
                    $range_between_end = new DateTime($query_options->birthday_filter_range_between_end) ; 
                    $range_between_end->setTimezone(new DateTimeZone($timezone)) ;  
                    $range_between_end_timestamp = $range_between_end->getTimestamp() ;

                    $query_array['where'] .= "AND ((contacts.timestamp_birthday_last>$range_between_begin_timestamp AND contacts.timestamp_birthday_last<$range_between_end_timestamp) OR (contacts.timestamp_birthday_next>$range_between_begin_timestamp AND contacts.timestamp_birthday_next<$range_between_end_timestamp))" ; 
            
                    break ;                    
                case 'range_next_expired':
                    
                    $range_next = Utilities::System_Time_Manipulate($query_options->birthday_filter_range_next_days.' d','-',$today_midnight_timestamp) ; 
                    $range_next_timestamp = $range_next['time_manipulated'] ;
                    $query_array['where'] .= "AND (contacts.timestamp_birthday_next<$today_midnight_timestamp AND contacts.timestamp_birthday_next>$range_next_timestamp) " ; 
            
                    break ; 
                }            
            }
        
        
        
        // PROCESS CONTACT CREATED FILTER
        if ($query_options->contact_created_filter_status == 'active') { 
            
            $today_midnight = new DateTime() ; 
            $today_midnight->setTimezone(new DateTimeZone($timezone)) ;  
            $today_midnight_timestamp = $today_midnight->setTimestamp(TIMESTAMP)->setTime( 23, 59, 59 )->getTimestamp() ;

            
            switch ($query_options->contact_created_filter_type) {
                case 'range_next':
                    
                    $range_next = Utilities::System_Time_Manipulate($query_options->contact_created_filter_range_next_days.' d','+',$today_midnight_timestamp) ; 
                    $range_next_timestamp = $range_next['time_manipulated'] ;
                    $query_array['where'] .= "AND (contacts.timestamp_contact_created>$today_midnight_timestamp AND contacts.timestamp_contact_created<$range_next_timestamp) " ; 
            
                    break ; 
                case 'range_last':
                    
                    $range_last = Utilities::System_Time_Manipulate($query_options->contact_created_filter_range_last_days.' d','-',$today_midnight_timestamp) ; 
                    $range_last_timestamp = $range_last['time_manipulated'] ;
                    $query_array['where'] .= "AND (contacts.timestamp_contact_created>$range_last_timestamp AND contacts.timestamp_contact_created<$today_midnight_timestamp) " ; 
            
                    break ;                    
                case 'range_between':
                    
                    $range_between_begin = new DateTime($query_options->contact_created_filter_range_between_begin) ; 
                    $range_between_begin->setTimezone(new DateTimeZone($timezone)) ;  
                    $range_between_begin_timestamp = $range_between_begin->getTimestamp() ;
                    
                    $range_between_end = new DateTime($query_options->contact_created_filter_range_between_end) ; 
                    $range_between_end->setTimezone(new DateTimeZone($timezone)) ;  
                    $range_between_end_timestamp = $range_between_end->getTimestamp() ;

                    $query_array['where'] .= "AND (contacts.timestamp_contact_created>$range_between_begin_timestamp AND contacts.timestamp_contact_created<$range_between_end_timestamp)" ; 
            
                    break ;                    
                    
                }             
            }
        
        

        
        // ADD CONTACT SOURCE FILTER
        if ($query_options->filter_by_contact_source) {              
            
            $query_options->filter_by_contact_source = Utilities::Process_Comma_Separated_String($query_options->filter_by_contact_source) ;
            
            $contact_source_search_string = '' ; 
            
            foreach ($query_options->filter_by_contact_source as $contact_source) {
                $contact_source_search_string .= "contacts.created_source LIKE '$contact_source' OR " ; 
                }
            
            $contact_source_search_string = rtrim($contact_source_search_string," OR ") ;     
            $query_array['where'] .= " AND ($contact_source_search_string) " ; 
            }
        
        
        // ADD CONTACT STATE FILTER
        if ($query_options->filter_by_contact_state_id) {              
            
            $query_options->filter_by_contact_state_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_contact_state_id) ;
            
            $contact_state_search_string = '' ; 
            
            foreach ($query_options->filter_by_contact_state_id as $contact_state) {
                $contact_state_search_string .= "contacts.state LIKE '$contact_state' OR " ; 
                }
            
            $contact_state_search_string = rtrim($contact_state_search_string," OR ") ;     
            $query_array['where'] .= " AND ($contact_state_search_string) " ; 
            }        
        
        
        // ADD CONTACT COUNTRY FILTER
        if ($query_options->filter_by_contact_country_id) {              
            
            $query_options->filter_by_contact_country_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_contact_country_id) ;
            
            $contact_country_search_string = '' ; 
            
            foreach ($query_options->filter_by_contact_country_id as $contact_country) {
                $contact_country_search_string .= "contacts.country_id LIKE '$contact_country' OR " ; 
                }
            
            $contact_country_search_string = rtrim($contact_country_search_string," OR ") ;     
            $query_array['where'] .= " AND ($contact_country_search_string) " ; 
            }
        
        
        
        // ADD YL INFO FILTERS
        // PROCESS CONTACT YL ACTIVATION DATE FILTER
        if ($query_options->contact_yl_activation_filter_status == 'active') {
            
            $today_midnight = new DateTime() ; 
            $today_midnight->setTimezone(new DateTimeZone($timezone)) ;  
            $today_midnight_timestamp = $today_midnight->setTimestamp(TIMESTAMP)->setTime( 23, 59, 59 )->getTimestamp() ;

            
            switch ($query_options->contact_yl_activation_filter_type) {
                case 'range_next':
                    
                    $range_next = Utilities::System_Time_Manipulate($query_options->contact_yl_activation_filter_range_next_days.' d','+',$today_midnight_timestamp) ; 
                    $range_next_timestamp = $range_next['time_manipulated'] ;
                    $query_array['where'] .= "AND (contact_details.timestamp_yl_activation_date>$today_midnight_timestamp AND contact_details.timestamp_yl_activation_date<$range_next_timestamp) " ; 
            
                    break ; 
                case 'range_last':
                    
                    $range_last = Utilities::System_Time_Manipulate($query_options->contact_yl_activation_filter_range_last_days.' d','-',$today_midnight_timestamp) ; 
                    $range_last_timestamp = $range_last['time_manipulated'] ;
                    $query_array['where'] .= "AND (contact_details.timestamp_yl_activation_date>$range_last_timestamp AND contact_details.timestamp_yl_activation_date<$today_midnight_timestamp) " ; 
            
                    break ;                    
                case 'range_between':
                    
                    $range_between_begin = new DateTime($query_options->contact_yl_activation_filter_range_between_begin) ; 
                    $range_between_begin->setTimezone(new DateTimeZone($timezone)) ;  
                    $range_between_begin_timestamp = $range_between_begin->getTimestamp() ;
                    
                    $range_between_end = new DateTime($query_options->contact_yl_activation_filter_range_between_end) ; 
                    $range_between_end->setTimezone(new DateTimeZone($timezone)) ;  
                    $range_between_end_timestamp = $range_between_end->getTimestamp() ;

                    $query_array['where'] .= "AND (contact_details.timestamp_yl_activation_date>$range_between_begin_timestamp AND contact_details.timestamp_yl_activation_date<$range_between_end_timestamp)" ; 
            
                    break ;                    
                    
                }            
            }
        
        
        // FILTER BY YL ACCOUNT STATUS
        if (isset($query_options->filter_by_yl_account_status_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_account_status_metadata_id,","),",") ;
            $query_options->filter_by_yl_account_status_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_account_status_metadata_id) ;
            $count_metadata = count($query_options->filter_by_yl_account_status_metadata_id) ; 
                        
            $join_table_name = 'yl_account_status_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->tag_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='contact' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (contacts.contact_id=$join_table_name.item_id)"
                );                         
            }        
        
        
        
        if (isset($query_options->filter_by_yl_current_rank_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_current_rank_metadata_id,","),",") ;
            $query_options->filter_by_yl_current_rank_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_current_rank_metadata_id) ;
            $count_metadata = count($query_options->filter_by_yl_current_rank_metadata_id) ; 
                 
            $filter_by_yl_current_rank_string = '' ; 
            if (count($query_options->filter_by_yl_current_rank_metadata_id) > 0) {

                foreach ($query_options->filter_by_yl_current_rank_metadata_id as $rank) {

                    $filter_by_yl_current_rank_string .= "contact_details.rank_current='$rank' OR " ;
                    }
                $filter_by_yl_current_rank_string = rtrim($filter_by_yl_current_rank_string," OR ") ; 
                $filter_by_yl_current_rank_string = " AND ($filter_by_yl_current_rank_string) " ;

                $query_array['where'] .= " $filter_by_yl_current_rank_string " ; 
                }            
            }
          
           
        
        if (isset($query_options->filter_by_yl_highest_rank_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_highest_rank_metadata_id,","),",") ;
            $query_options->filter_by_yl_highest_rank_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_highest_rank_metadata_id) ;
            $count_metadata = count($query_options->filter_by_yl_highest_rank_metadata_id) ; 
                 
            $filter_by_yl_highest_rank_string = '' ; 
            if (count($query_options->filter_by_yl_highest_rank_metadata_id) > 0) {

                foreach ($query_options->filter_by_yl_highest_rank_metadata_id as $rank) {

                    $filter_by_yl_highest_rank_string .= "contact_details.rank_highest='$rank' OR " ;
                    }
                $filter_by_yl_highest_rank_string = rtrim($filter_by_yl_highest_rank_string," OR ") ; 
                $filter_by_yl_highest_rank_string = " AND ($filter_by_yl_highest_rank_string) " ;

                $query_array['where'] .= " $filter_by_yl_highest_rank_string " ; 
                }            
            }
        
        
        if (isset($query_options->filter_by_yl_enroller_member_number)) {
            
            $enroller_number_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_enroller_member_number,","),",") ;
            $query_options->filter_by_yl_enroller_member_number = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_enroller_member_number) ;
            $count_metadata = count($query_options->filter_by_yl_enroller_member_number) ; 
                 
            $filter_by_yl_enroller_member_number_string = '' ; 
            if (count($query_options->filter_by_yl_enroller_member_number) > 0) {

                foreach ($query_options->filter_by_yl_enroller_member_number as $enroller) {

                    $filter_by_yl_enroller_member_number_string .= "contact_details.yl_enroller_member_number='$enroller' OR " ;
                    }
                $filter_by_yl_enroller_member_number_string = rtrim($filter_by_yl_enroller_member_number_string," OR ") ; 
                $filter_by_yl_enroller_member_number_string = " AND ($filter_by_yl_enroller_member_number_string) " ;

                $query_array['where'] .= " $filter_by_yl_enroller_member_number_string " ; 
                }            
            }
        
        if (isset($query_options->filter_by_yl_sponsor_member_number)) {
            
            $sponsor_number_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_sponsor_member_number,","),",") ;
            $query_options->filter_by_yl_sponsor_member_number = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_sponsor_member_number) ;
            $count_metadata = count($query_options->filter_by_yl_sponsor_member_number) ; 
                 
            $filter_by_yl_sponsor_member_number_string = '' ; 
            if (count($query_options->filter_by_yl_sponsor_member_number) > 0) {

                foreach ($query_options->filter_by_yl_sponsor_member_number as $sponsor) {

                    $filter_by_yl_sponsor_member_number_string .= "contact_details.yl_sponsor_member_number='$sponsor' OR " ;
                    }
                $filter_by_yl_sponsor_member_number_string = rtrim($filter_by_yl_sponsor_member_number_string," OR ") ; 
                $filter_by_yl_sponsor_member_number_string = " AND ($filter_by_yl_sponsor_member_number_string) " ;

                $query_array['where'] .= " $filter_by_yl_sponsor_member_number_string " ; 
                }            
            }        
        
        
        // END: YL INFO FILTERS
        
        
        if ($query_options->import_file_task_id) {
            $query_array['where'] .= "AND (contacts.import_file_task_id='$query_options->import_file_task_id') " ; 
            }
        
        if ($query_options->filter_by_phone_number == 'yes') {
            $query_array['where'] .= "AND (contacts.phone_country_dialing_code='$query_options->phone_country_dialing_code' AND contacts.phone_number='$query_options->phone_number') " ; 
            }
        
        
        if ($query_options->filter_by_email_address) {
            $query_array['where'] .= "AND contacts.email_address='$query_options->filter_by_email_address' " ; 
            }
        
        
        
        // Add filter by search parameter
        if (isset($query_options->filter_by_search_parameter)) {

            $search_parameter_token = explode(" ",$query_options->filter_by_search_parameter) ; 

            $t = 0 ; 
            foreach ($search_parameter_token as $search_token) {
                if ($search_token == '') {
                    unset($search_parameter_token[$t]) ; 
                    }
                $t++ ; 
                }
            array_values($search_parameter_token) ; 

            foreach ($search_parameter_token as $search_token) {
                $phone_number = Utilities::Validate_Phone_Number($search_token) ;
                if ($phone_number['valid'] == 'yes') {
                    $search_parameter_token[] = $phone_number['phone_number'] ;    
                    }
                }

                switch($query_options->search_parameter_refine_contact_notes) { 
                    case 'yes':
                        $query_array['join_tables'][] = array(
                            'table' => 'contact_notes',
                            'on' => 'contact_notes.contact_id',
                            'match' => 'contacts.contact_id',
                            'type' => 'left'
                            );

                        break ;    
                    default:

                    }

            $compiled_search_string = '' ; 

            foreach ($search_parameter_token as $search_token) {

                $this_search_string = " ($temp_member_table.full_name LIKE '%$search_token%' OR 
                    $temp_member_table.email_address LIKE '%$search_token%' OR 
                    $temp_member_table.phone_number LIKE '%$search_token%' " ;

                $this_search_string = " (contacts.first_name LIKE '%$search_token%' OR 
                    contacts.last_name LIKE '%$search_token%' OR 
                    CONCAT(contacts.first_name,' ',contacts.last_name) LIKE '%$search_token%' OR 
                    contacts.email_address LIKE '%$search_token%' OR 
                    contacts.phone_number LIKE '%$search_token%' OR 
                    contact_details.yl_member_number LIKE '%$search_token%' " ;
                
                
                switch($query_options->search_parameter_refine_contact_notes) { 
                    case 'yes':
                        $this_search_string .= " OR contact_notes.note_title LIKE '%$search_token%' OR contact_notes.note_content LIKE '%$search_token%' " ; 
                        break ;    
                    default:
                        // blank
                    }

                switch($query_options->search_parameter_refine_contact_description) { 
                    case 'yes':
                        $this_search_string .= " OR contacts.contact_description LIKE '%$search_token%' " ; 
                        break ;    
                    default:
                        // blank
                    }                            

                $this_search_string .= ')' ; 
                $compiled_search_string .= $this_search_string.' OR ' ;                             
                }

            $compiled_search_string = rtrim($compiled_search_string," OR ") ;  
            
            $query_array['where'] .= " AND ($compiled_search_string) " ;                                    
            }
        
         
    
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ; 
            
            if ($query_array['order_by'] == 'relevance_score') {
                $query_array['order_by'] = 'contacts.last_name' ; 
                $query_array['order_by_relevance'] = 'yes' ; 
                }  
          
            }
        


        
        // NEW PAGING PROCESS
        // Add paging constraints
        $this->Set_Model_Timings('Contact.Retrieve_Contact_List_pre_query') ;
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
        $this->Set_Model_Timings('Contact.Retrieve_Contact_List_process_paging_constraints') ;
        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force'); 
        $this->Set_Model_Timings('Contact.Retrieve_Contact_List_pull_results') ;
        
        if (isset($query_options->filter_by_search_parameter)) {
            
            $relevance_options = array( 
                'fields' => array(
                    'last_name',
                    'first_name',
                    'full_name', 
                    'email_address',
                    'phone_number'
                    ) 
                ) ; 
            
            switch($query_options->search_parameter_refine_contact_description) { 
                case 'yes':
                    $relevance_options['fields'][] = 'contact_description' ; 
                    break ;    
                }
            
            $result = $this->Sort_Search_Relevance($result,$query_array,$query_options,$relevance_options) ; 
            if ($query_array['order_by_relevance'] == 'yes') {
                $query_array['order_by'] = 'relevance_score' ; 
                }            
            } 
                
        $query_options->value_name = 'contact_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 
        $this->Set_Model_Timings('Contact.Retrieve_Contact_List_process_query_results') ;
        
        $this->Set_Contact_Paging($result) ;         
//        $this->contact_query_result = $result ;         
//        $this->test_data = $result ; 
        
        return $result ;       
        }
    
    
    public function Update_Contact($update_set,$additional_details = array()) {

        foreach ($update_set as $update) {
            
            unset($update['update_duplicates']) ; 
            $update_contact_id = $update['contact_id'] ; 

            $query_array = array(
                'table' => "contacts",
                'values' => $update,
                'where' => "contact_id='$update_contact_id'"
                );                        
            $result = $this->DB->Query('UPDATE',$query_array) ;            
            }
        
        $this->test_data = $result ; 
        return $result ;         
        }
    
    public function Update_Contact_Details($update_set,$additional_details = array()) {

        foreach ($update_set as $update) {
            
            unset($update['update_duplicates'],$update['duplicate_match_override']) ; 
            $update_contact_id = $update['contact_id'] ; 

            $query_array = array(
                'table' => "contact_details",
                'values' => $update,
                'where' => "contact_id='$update_contact_id'"
                );                        
            $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;            
            }
        
        $this->test_data = $result ; 
        return $result ;         
        }
    
    public function Update_Contact_Note($input) {
        
         
        $values_array = array() ; 
        
        if (count($input) > 0) {
            
            foreach ($input as $key => $value) {

                $values_array[$key] = $value ; 

                }
            }
        
        unset($values_array['note_id']) ; 
        
        // Updates the contact in the database
        $query_array = array(
            'table' => 'contact_notes',
            'values' => $values_array,
            'where' => "contact_id='$this->contact_id' AND note_id='$this->contact_note_id'"
            );

        $query_array['values']['timestamp_updated'] = TIMESTAMP ; 
        
        $result = $this->DB->Query('UPDATE',$query_array);
        $this->contact_query_result = $result ; 
        
        if ($result['results'] == true) {
            
            $query_array['values']['note_id'] = $this->contact_note_id ;
            
            $history_input = array(
                'contact_id' => $this->contact_id,
                'function' => 'update_contact_note',
                'notes' => json_encode($query_array['values'])
                ) ; 
        
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;             
            }
        
        
                
        return $result ; 
        
        }    

    }


