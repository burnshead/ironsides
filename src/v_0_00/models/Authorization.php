<?php

namespace Ragnar\Ironsides ;

class Authorization extends Account {
    
    public $vendor;
    public $vendor_id;
    public $authorization;
    public $account;
    public $account_id;
    public $query_array_result ;
    public $stripe_account_id = array("stripe_account" => 'INVALID');
    
    
    

    public function __construct($user_id = 'ignore') {

        global $DB ;   
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            } 
       
        }
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    

    
    public function Set_Vendor_ID($vendor_id) {
        $this->vendor_id = $vendor_id ; 
        return $this;
        }
    
    
    public function Set_Vendor_By_ID($vendor_id = 'internal') {
        
        if ('internal' === $vendor_id) {
            $vendor_id = $this->vendor_id ; 
            } else {
                $this->vendor_id = $vendor_id ; 
                }
        
        if ($vendor_id != 0) {
            $this->Set_Vendor($this->vendor_id) ; 
            } else {
                $this->vendor = 'error' ; 
                }
        
        return $this;
        }    
    
    
    public function Set_Vendor($vendor_id = 'internal') {
            
        if ('internal' === $vendor_id) {
            $vendor_id = $this->vendor_id ; 
            } else {
                $this->vendor_id = $vendor_id ; 
                }
        
        $vendor_record = $this->Retrieve_Vendor($vendor_id) ;  
        $this->vendor = $vendor_record['results'];
        return $this ;
        }
    
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_Vendor_ID() {
        
        return $this->vendor_id;       
        
        }
    
    public function Get_Vendor() {
        
        return $this->vendor;       
        
        }    
    
    
    
    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    
    
    
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    ////////////////////// 
    
    
    public function Retrieve_Vendor($vendor) {
            
        
        
        $query_array = array(
            'table' => 'vendors',
            'fields' => "*",
            'where' => "vendors.vendor_id='$vendor'"
            );
        
        $result = $this->DB->Query('SELECT',$query_array);
            
        $this->vendor = $result['results'];
        
        return $result ;

        }
    
    
    
    
    public function Get_Vendor_Authorization($vendor_id,$association_id,$return_array) {
            
        $query_array = array(
            'table' => 'Vendor_Authorization',
            'join_tables' => array(),
            'fields' => "Vendor_Authorization.*, Vendor_Keys.key_id, Vendor_Keys.association_id, Vendor_Keys.key_value",
            'where' => "Vendor_Authorization.account_id='$this->account_id' AND Vendor_Authorization.vendor_id='$vendor_id' AND Vendor_Keys.association_id='$association_id'"
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'Vendor_Keys',
            'on' => 'Vendor_Authorization.account_id',
            'match' => 'Vendor_Keys.account_id'
            );
        
        $db = DAL::getInstance();
        $result = $this->DB->Query('SELECT_JOIN',$query_array);
        
        $this->authorization = $result['results'];
        $this->query_array_result = $query_array ; 
        
        if ($vendor_id == 1) {
            // This sets the Stripe Account ID array to an external Stripe-Account acct_
            $this->stripe_account_id = array("stripe_account" => $result['results']['key_value']);
            }
        
        if ($return_array == TRUE) {
            return $result['results'];
            } else {
                return $this;
                }

        }

    
    

    public function Authorize_Vendor($code) {
        
        $vendor_id = $this->vendor_id;
        $account_id = $this->account_id;
        
        switch ($vendor_id) {
            case 1:

                $url = 'https://connect.stripe.com/oauth/token';
                $fields = array(
                    'grant_type' => 'authorization_code',
                    'client_id' => '',
                    'code' => $code,
                    'client_secret' => 'sk_live_TRCZOd8UxPuHR2vcyhZWEJkB'
                    );

                $curl_response = Utilities::CurlExecute($url, $fields);
                
                $curl_results_array = json_decode($curl_response['curl_result'], true);
                // return $curl_response;
                
                $query_array = array(
                    'table' => 'Vendor_Authorization',
                    'values' => array(
                        'account_id' => $Account_ID,
                        'vendor_id' => $Vendor_ID,
                        'connection_status' => 'Connected',
                        'refresh_token' => $curl_results_array['refresh_token'],
                        'connection_date' => Utilities::Timestamp()
                        ),
                    'where' => "account_id='$account_id' AND vendor_id='$vendor_id'"
                    );
        
                $db = DAL::getInstance();
                $new_authorization = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array);                
                
                
                // Add Stripe Account ID
                $Association_ID = 5;
                $query_array = array(
                    'table' => 'Vendor_Keys',
                    'values' => array(
                        'account_id' => $account_id,
                        'vendor_id' => $vendor_id,
                        'association_id' => $association_id,
                        'key_value' => $curl_results_array['stripe_user_id']
                        ),
                    'where' => "account_id='$account_id' AND vendor_id='$vendor_id' AND association_id='$association_id'"
                    );
        
                $db = DAL::getInstance();
                $new_vendor_key = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array);                
                
                // Add Stripe Publishable ID
                $association_id = 1;
                $query_array = array(
                    'table' => 'Vendor_Keys',
                    'values' => array(
                        'account_id' => $Account_ID,
                        'vendor_id' => $Vendor_ID,
                        'association_id' => $Association_ID,
                        'key_value' => $curl_results_array['stripe_publishable_key']
                        ),
                    'where' => "account_id='$account_id' AND vendor_id='$vendor_id' AND association_id='$association_id'"
                    );
        
                $db = DAL::getInstance();
                $new_vendor_key = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array); 
                
                // Add Stripe Secret ID
                $association_id = 2;
                $query_array = array(
                    'table' => 'Vendor_Keys',
                    'values' => array(
                        'account_id' => $account_id,
                        'vendor_id' => $vendor_id,
                        'association_id' => '2',
                        'key_value' => $curl_results_array['access_token']
                        ),
                    'where' => "account_id='$account_id' AND vendor_id='$vendor_id' AND association_id='$association_id'"
                    );
        
                $db = DAL::getInstance();
                $new_vendor_key = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array); 
                
                return $curl_results_array;
                break;
            }
        

        }
    
    }