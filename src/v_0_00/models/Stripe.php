<?php

namespace Ragnar\Ironsides ;

// Accessing Stripe values: https://stackoverflow.com/questions/29494535/using-php-how-can-i-access-the-protected-values-property-returned-from-the-str


class Stripe extends Vendor {
    
    public $billing_mode ;
    
    public $customer ; 
    public $customer_list ; 
    
    public $page_increment = 12 ; // # of items to be pulled per page (e.g. )
    
    public $plan_id ;     
    public $plan ; 
    public $plan_list ;
    
    public $product_id ;    
    public $product ; 
    public $product_list ; 

    public $product_set ; // Combined details of plans + Stripe details (and placed into product collections)
    
    public $card_id ; 
    public $card ; 
    public $source_cards ; 
    

    public $coupon_id ;     
    public $coupon ; 
    public $coupon_list ;
    public $coupon_set ; // Combined details of coupon + Stripe details
    
    public $invoice ;
    public $invoice_list ;    
    public $invoice_item ; 
    
    public $subscription_id ;
    public $subscription ; 
    
    public $subscription_item_id ; 
    public $subscription_item ;
    
    public $stripe_api_event ;    
    public $stripe_query_result ;
    
    // Card acceptance settings...
    public $card_accept_prepaid = 0 ; 
    
    
    public function __construct($user_id = 'ignore') {

        global $DB ;  
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            } 
        
        }
    
        
    public function Connect($billing_mode = 'system_live') {
        
        switch ($billing_mode) {
            case 'account':
                
                break ;
            case '0':    
            case 'system_live':
                       
                // Connect the account
                $resource = 'Stripe' ; 
                $action = 'setApiKey' ; 
                $parameters = STRIPE_SECRET_KEY ; 

                $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;                

                $billing_mode = 'system_live' ; 
                break ; 
            case '1':
            case 'system_test':               
            default:
                // Set the API key
                $resource = 'Stripe' ; 
                $action = 'setApiKey' ; 
                $parameters = STRIPE_SECRET_KEY_TEST ; 
                
                $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;   
                
                $billing_mode = 'system_test' ; 
            }
    
        $this->billing_mode = $billing_mode ; 

        // Stripe links...
        switch ($this->billing_mode) {
            case 'system_live':
                $this->test_link_segment = '' ; 
                break ;
            case 'system_test':
                $this->test_link_segment = 'test/' ; 
                break ;
            }        
        
        
        if ($execute['success'] == 1) {            
            $this->Set_Alert_Response(263) ; // Connection success
            } else {
                $this->Set_Alert_Response(50) ; // Error
                }
        
        return $this ; 
        }
    
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    
    public function Set_Page_Increment($page_increment) {
        $this->page_increment = $page_increment ; 
        return $this ; 
        }
    
    // An array of 
    public function Set_Stripe_API_Event($event) {
        $this->stripe_api_event = $event ; 
        }
    
    
    // Retrieve and set the default product / plan as denoted in the database
    public function Set_Default_Plan() {
        
        $query_options['default_plan'] = 1 ; 
        $query_options['default_product'] = 1 ;
        
        switch ($this->billing_mode) {
            case 'system_live':
                $query_options['system_test'] = 0 ; 
                break ;
            case 'system_test':
                $query_options['system_test'] = 1 ;
                break ;
            }
        
        
        $plan = $this->Retrieve_Plan_List($query_options) ; 
        
        if ($plan['result_count'] > 0) {
            $this->plan_id = $plan['results'][0]['plan_id'] ; 
            $this->plan = $plan['results'][0] ; 
            } else {
                $this->plan_id = 'error' ; 
                $this->plan = 'error' ; 
                }
        
        return $this ;        
        }

    // Retrieve and set the default plan as denoted in the database
    public function Set_Default_Product() {
        
        $query_options['default_product'] = 1 ; 

        switch ($this->billing_mode) {
            case 'system_live':
                $query_options['system_test'] = 0 ; 
                break ;
            case 'system_test':
                $query_options['system_test'] = 1 ;
                break ;
            }
        
        $plan = $this->Retrieve_Plan_List($query_options) ; 
        
        
        if ($plan['result_count'] > 0) {
            $this->product_id = $plan['results'][0]['product_id'] ; 
            $this->product = $plan['results'] ; 
            } else {
                $this->plan_id = 'error' ; 
                $this->plan = 'error' ; 
                }
        
        return $this ;        
        }
    
    
    // Set the Stripe ID of the subscription plan (not the internal_plan_id)
    public function Set_Plan_ID($plan_id) {
        $this->plan_id = $plan_id ; 
        return $this;
        }
    
    
    public function Set_Plan_By_ID($plan_id = 'internal') {
        
        if ('internal' === $plan_id) {
            $plan_id = $this->plan_id ; 
            } else {
                $this->Set_Plan_ID($plan_id) ; 
                }
        
        $query_options['plan_id'] = $this->plan_id ; 
        $plan = $this->Retrieve_Plan_List($query_options) ; 
        
        if ($plan['result_count'] > 0) {
            $this->plan = $plan['results'][0] ; 
            } else {
                $this->plan = 'error' ; 
                }
        
        return $this ;        
        }    
    
    
    // This is the Stripe ID, not the internal_plan_id
    public function Set_Product_ID($product_id) {
        $this->product_id = $product_id ; 
        return $this;
        }
    
    // Set the parent product by Stripe ID (not the internal_product_id)
    public function Set_Product_By_ID($product_id = 'internal') {
        
        if ('internal' === $product_id) {
            $product_id = $this->product_id ; 
            } else {
                $this->Set_Product_ID($product_id) ; 
                }        
        
        $query_options['product_id'] = $this->product_id ; 
        
        $product = $this->Retrieve_Product_List($query_options) ; 
        
        if ($product['result_count'] > 0) {
            $this->product = $product['results'][0] ; 
            } else {
                $this->product = 'error' ; 
                }        
        
        return $this ;        
        }    
    
    
    // Set the list of plans in the internal database
    public function Set_Plan_List($query_options = array()) {
        
        switch ($this->billing_mode) {
            case 'system_live':
                $query_options['system_test'] = 0 ; 
                break ;
            case 'system_test':
                $query_options['system_test'] = 1 ;
                break ;
            }
         
        $plans = $this->Retrieve_Plan_List($query_options) ; 
        
        if ($plans['result_count'] > 0) {
            $this->plan_list = $plans['results'] ; 
            } else {
                $this->plan_list = 'error' ; 
                }
        
        return $this ;        
        }
    
    
    // Set the list of products in the internal database
    public function Set_Product_List($query_options = array()) {
        
        // $query_options['product_id'] = $this->product_id ; 
        
        switch ($this->billing_mode) {
            case 'system_live':
                $query_options['system_test'] = 0 ; 
                break ;
            case 'system_test':
                $query_options['system_test'] = 1 ;
                break ;
            }
        
        $product = $this->Retrieve_Product_List($query_options) ; 
        
        if ($product['result_count'] > 0) {
            $this->product_list = $product['results'] ; 
            } else {
                $this->product_list = 'error' ; 
                }        
        
        return $this ;        
        }    
    
 
    // Set the Stripe ID of the coupon (not the internal_coupon_id)
    public function Set_Coupon_ID($coupon_id) {
        $this->coupon_id = $coupon_id ; 
        return $this;
        }
    
    
    public function Set_Coupon_By_ID($coupon_id = 'internal') {
        
        if ('internal' === $coupon_id) {
            $coupon_id = $this->coupon_id ; 
            } else {
                $this->Set_Coupon_ID($coupon_id) ; 
                }
        
        $query_options['coupon_id'] = $this->coupon_id ; 
        $coupon = $this->Retrieve_Coupon_List($query_options) ; 
        
        if ($coupon['result_count'] > 0) {
            $this->coupon = $coupon['results'][0] ; 
            } else {
                $this->coupon = 'error' ; 
                }
        
        return $this ;        
        }    
    
    
    // Set the list of coupons in the internal database
    public function Set_Coupon_List($query_options = array()) {
        
        switch ($this->billing_mode) {
            case 'system_live':
                $query_options['system_test'] = 0 ; 
                break ;
            case 'system_test':
                $query_options['system_test'] = 1 ;
                break ;
            }
        
        $coupons = $this->Retrieve_Coupon_List($query_options) ; 
        
        if ($coupons['result_count'] > 0) {
            $this->coupon_list = $coupons['results'] ; 
            } else {
                $this->coupon_list = 'error' ; 
                }
        
        return $this ;        
        }
    
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////    
    
    
    public function Get_Page_Increment() {
        
        return $this->page_increment ;
        }
    
    public function Get_Customer() {
        
        return $this->customer ;
        } 
    
    public function Get_Customer_List() {
        
        return $this->customer_list ;
        }     
    
    public function Get_Card_ID() {
        
        return $this->card_id ;
        }
    
    public function Get_Card() {
        
        return $this->card ;
        }
    
    public function Get_Cards() {
        
        return $this->source_cards ;
        }    
    
    public function Get_Plan_ID() {
        
        return $this->plan_id ;
        } 
    
    
    public function Get_Plan() {
        
        return $this->plan ;
        } 
    
    public function Get_Plan_List() {
        
        return $this->plan_list ;
        }     
    
    
    public function Get_Product_ID() {
        
        return $this->product_id ;
        } 
    
    public function Get_Product() {
        
        return $this->product ;
        } 
    
    public function Get_Product_List() {
        
        return $this->product_list ;
        }     
    
    public function Get_Product_Set() {
        
        return $this->product_set ;
        }
    
    
    public function Get_Coupon_ID() {
        
        return $this->coupon_id ;
        } 
    
    public function Get_Coupon() {
        
        return $this->coupon ;
        } 
    
    public function Get_Coupon_List() {
        
        return $this->coupon_list ;
        }

    public function Get_Coupon_Set() {
        
        return $this->coupon_set ;
        }
    
    public function Get_Subscription() {
        
        return $this->subscription ;
        }
    
    public function Get_Stripe_API_Event() {
        
        return $this->stripe_api_event ;
        }    
    
    
    
    
    
    //////////////////////
    //                  //
    // API ACTIONS      //
    //                  //
    //////////////////////  
    
    
    // Retrieve the customer from Stripe
    public function API_Set_Customer($account_billing_id = 'internal') {
        
        $continue = 1 ;
        
        if ('internal' === $account_billing_id) {
            $account_billing_id = $this->account_billing_id ; 
            } else {
                $this->account_billing_id = $account_billing_id ; 
                } 
         
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {
            
            // Retrieve the stripe customer
            $resource = 'Customer' ; 
            $action = 'retrieve' ; 
            $parameters = $this->account_billing_id ; 

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $this->customer = $execute['event']->__toArray(true) ;
                }         

            }
        
        return $this ;        
        }
    
    
    // Retrieve the customer list from Stripe
    // Sets as an array to $this->customer_list
    public function API_Set_Customer_List($query_settings = array()) {
        
        $continue = 1 ;
        
        $query_options = array() ;
        
        if (isset($query_settings['limit'])) {
            $query_options['limit'] = $query_settings['limit'] ; 
            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {        
        
            // Retrieve the customer list
            $resource = 'Customer' ; 
            $action = 'all' ; 
            $parameters = $query_options ; 

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $this->customer_list = $execute['event']->__toArray(true) ;
                }
        
            }
        
        return $this ;        
        } 
    
    
    public function Get_Invoice_List() {
        
        return $this->invoice_list ; 
        }
    
    
    // Retrieve the customer list from Stripe
    // Sets as an array to $this->invoice_list
    public function API_Set_Invoice_List($query_settings = array()) {
        
        $continue = 1 ;
        
        $query_options = array() ;
        
//        if (isset($query_settings['limit'])) {
//            $query_options['limit'] = $query_settings['limit'] ; 
//            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {        
        
            // Retrieve the invoice list
            $resource = 'Invoice' ; 
            $action = 'all' ; 
            $parameters = $query_settings ; 

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $invoice_list = $execute['event']->__toArray(true) ;
                
                $i = 0 ;
                foreach ($invoice_list['data'] as $invoice) {
                    
                    $invoice['timestamp_created'] = $invoice['created'] ;
                    $invoice['timestamp_date'] = $invoice['date'] ;
                    $invoice['timestamp_finalized_at'] = $invoice['finalized_at'] ;
                    $invoice['timestamp_period_end'] = $invoice['period_end'] ;
                    $invoice['timestamp_period_start'] = $invoice['period_start'] ;
                    
                    $invoice = $this->Action_Time_Territorialize_Dataset($invoice) ;
                    
                    $invoice_list['data'][$i] = $invoice ; 
                    $i++ ; 
                    }

                $this->invoice_list = $invoice_list ; 
                }
           
            }
        
        return $this ;        
        }     
    
    
    
    // For reasons unknown, paying an invoice can only be done via a CURL request as the PHP library doesn't seem to work
    public function API_Pay_Invoice($invoice_id) {
        
        $continue = 1 ;
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
        
            $url = 'https://api.stripe.com/v1/invoices/'.$invoice_id.'/pay' ; 
            $fields = array() ;
            
            switch ($this->billing_mode) {
                case 'system_test':
                    $options['auth_bearer'] = STRIPE_SECRET_KEY_TEST ; // Stripe API key
                    break ; 
                case 'system_live':
                    $options['auth_bearer'] = STRIPE_SECRET_KEY ; // Stripe API key
                    break ; 
                }
            

            $execute = Utilities::CurlExecute($url, $fields, $options) ;
            
            $this->invoice = json_decode($execute['curl_result'],1) ; 
            
            }
        
        return $this ;     
        }  
    
    public function API_Create_Invoice($query_settings = array()) {
        
        $continue = 1 ;
        
        $query_options = array() ;
        
//        if (isset($query_settings['limit'])) {
//            $query_options['limit'] = $query_settings['limit'] ; 
//            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {        
        
            // Retrieve the invoice list
            $resource = 'Invoice' ; 
            $action = 'create' ; 
            $parameters = $query_settings ; 

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                
                $invoice = $execute['event']->__toArray(true) ;
                $this->invoice = $invoice ; 
                }
           
            }
        
        return $this ;        
        }
    
    
    
    public function API_Test_Query($query_settings = array()) {
        
        $continue = 1 ;
        
        $query_options = array() ;
        
//        if (isset($query_settings['limit'])) {
//            $query_options['limit'] = $query_settings['limit'] ; 
//            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {        
        
            // Set the query
            $resource = 'SubscriptionSchedule' ; 
            $action = 'create' ; 
            $parameters = $query_settings ; 

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                
                $result = $execute['event']->__toArray(true) ;
//                $this->invoice = $result ; 
                }
           
            }
        
        
        return $result ; 
        }
    
    
    
    public function API_Create_Invoice_Item($query_settings = array()) {
        
        $continue = 1 ;
        
        $query_options = array() ;
        
//        if (isset($query_settings['limit'])) {
//            $query_options['limit'] = $query_settings['limit'] ; 
//            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {        
        
            // Retrieve the invoice list
            $resource = 'InvoiceItem' ; 
            $action = 'create' ; 
            $parameters = $query_settings ; 

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $invoice_item = $execute['event']->__toArray(true) ;
                $this->invoice_item = $invoice_item ; 
                }
           
            }
        
        return $this ;        
        }
    
    
    public function Get_Invoice_Item() {
        
        return $this->invoice_item ; 
        }
    
    
    
    public function API_Set_Card_By_ID($card_id) {
        
        $continue = 1 ;
        
        if (!isset($this->account_id)) {
            $continue = 0 ; 
            }
        
        if (!isset($this->account_billing_id)) {
            $continue = 0 ; 
            } 
        
        if ($continue == 1) {
            $this->API_Set_Customer($this->account_billing_id) ;                
            }         
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            } 
        
        
        if ($continue == 1) {
        
            try {
                
                $card = $stripe_event['event']->sources->retrieve($card_id);

                $this->card_id = $card_id ;
                $this->card = $card->__toArray(true) ;

                } catch(Exception $e) {

                    $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                    $this->Set_Stripe_API_Event($stripe_event) ; 
                    
                    $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => $stripe_event['error']['message'],
                        'location' => __METHOD__
                        )) ;
                 
                    }
            }
        
        return $this ; 
        }
                                       
                                       
                                       
    public function API_Set_Cards($account_billing_id = 'internal',$query_settings = array()) {
        
        $continue = 1 ;

        $this->API_Set_Customer($account_billing_id) ;

        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {
        
            $customer = $this->Get_Customer() ;
            
            $query_options = array() ; 
            $query_options['object'] = 'card' ; 
            
            if (isset($query_settings['page_increment'])) {
                $this->Set_Page_Increment($query_settings['page_increment']) ; 
                }  
            
            $query_options['limit'] = $this->Get_Page_Increment() ; 

            try {
                
                $cards = $stripe_event['event']->sources->all($query_options);
                
                $i = 0 ;
                foreach ($cards['data'] as $card) {
                    $cards['data'][$i]['exp_date'] = $card['exp_month'].'/'.$card['exp_year'] ; 
                    if ($card['id'] == $customer['default_source']) {
                        $cards['data'][$i]['default_source'] = 1 ; 
                        } else {
                            $cards['data'][$i]['default_source'] = 0 ; 
                            }
                    $i++ ; 
                    }

                $this->source_cards = $cards->__toArray(true) ;

                } catch(Exception $e) {

                    $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                    $this->Set_Stripe_API_Event($stripe_event) ; 
                    
                    $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => $stripe_event['error']['message'],
                        'location' => __METHOD__
                        )) ;
                 
                    }
            }
        
        
        return $this ;        
        }
    
    
    public function API_Set_Plan_By_ID($plan_id = 'internal') {
        
        if ('internal' === $plan_id) {
            $plan_id = $this->plan_id ; 
            } else {
                $this->plan_id = $plan_id ; 
                } 
        
        $continue = 1 ;

        if (!$plan_id) {
            $continue = 0 ; 
            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {                
        
            // Retrieve the stripe customer
            $resource = 'Plan' ; 
            $action = 'retrieve' ; 
            $parameters = $this->plan_id ;         

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $stripe_plan = $execute['event']->__toArray(true) ;
                
                $query_options['plan_id'] = $this->plan_id ; 
                $plan = $this->Retrieve_Plan_List($query_options) ; 

                $this->plan = $this->Action_Compile_Plan_Details($plan['results'][0],$stripe_plan) ; 

                }        
        
            }
        return $this ;        
        }
    
    
    public function API_Set_Plan_List($query_settings = array()) {
        
        $continue = 1 ;
        
        $query_options = array() ;
        if (isset($query_settings['limit'])) {
            $query_options['limit'] = $query_settings['limit'] ; 
            }
        if (isset($query_settings['product'])) {
            $query_options['product'] = $query_settings['product'] ; 
            }        

        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {  
            
            // Retrieve the list of Stripe plans
            $resource = 'Plan' ; 
            $action = 'all' ; 
            $parameters = $query_options ;         

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                // STRIPE FIX
                $this->plan_list = $execute['event']->__toArray(true) ;
                }                         
            
            }

        return $this ;        
        } 
    
    
    public function Set_Subscription_ID($subscription_id) {
        
        $this->subscription_id = $subscription_id ; 
        
        return $this ; 
        }
    
    public function Set_Subscription_Item_ID($subscription_item_id) {
        
        $this->subscription_item_id = $subscription_item_id ; 
        
        return $this ; 
        }
    
    public function API_Set_Subscription_By_ID($subscription_id = 'internal') {
        
        
        $continue = 1 ;
        
        if ('internal' === $subscription_id) {
            $subscription_id = $this->subscription_id ; 
            } else {
                $this->subscription_id = $subscription_id ; 
                } 
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {  
            
            // Retrieve the list of Stripe plans
            $resource = 'Subscription' ; 
            $action = 'retrieve' ; 
            $parameters = $subscription_id ;         

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $subscription = $execute['event']->__toArray(true) ;
                
                $subscription['timestamp_start'] = $subscription['start'] ;
                $subscription['timestamp_start_date'] = $subscription['start_date'] ;
                $subscription['timestamp_billing_cycle_anchor'] = $subscription['billing_cycle_anchor'] ;
                $subscription['timestamp_current_period_start'] = $subscription['current_period_start'] ;
                $subscription['timestamp_current_period_end'] = $subscription['current_period_end'] ;
                
                $this->subscription = $this->Action_Time_Territorialize_Dataset($subscription) ;
                }         
            
            }
        
        return $this ;        
        }
    
    
    
    
    public function API_Set_Product_By_ID($product_id = 'internal') {
        
        $continue = 1 ;
        
        if ('internal' === $product_id) {
            $product_id = $this->product_id ; 
            } else {
                $this->product_id = $product_id ; 
                } 
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {  
            
            // Retrieve the list of Stripe plans
            $resource = 'Product' ; 
            $action = 'retrieve' ; 
            $parameters = $product_id ;         

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $this->product = $execute['event']->__toArray(true) ;
                }         
            
            }
        
        return $this ;        
        }
    
    
    public function API_Set_Product_List($query_settings = array()) {
        
        $continue = 1 ; 
        
        $query_options = array() ;
        
        if (isset($query_settings['limit'])) {
            $query_options['limit'] = $query_settings['limit'] ; 
            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {         
        
            // Retrieve the list of Stripe products
            $resource = 'Product' ; 
            $action = 'all' ; 
            $parameters = $query_options ;         

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                // STRIPE FIX
                $this->product_list = $execute['event']->__toArray(true) ;
                }         
        
            }

        return $this ;        
        } 
    
   

        public function API_Set_Coupon_By_ID($coupon_id = 'internal') {
        
        $continue = 1 ;
        
        if ('internal' === $coupon_id) {
            $coupon_id = $this->coupon_id ; 
            } else {
                $this->coupon_id = $coupon_id ; 
                } 
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {  
            
            $coupon_id = strtolower($coupon_id) ;            

            // Retrieve the coupon from Stripe
            $resource = 'Coupon' ; 
            $action = 'retrieve' ; 
            $parameters = $coupon_id ;         

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $stripe_coupon = $execute['event']->__toArray(true) ;
                
                $query_options['coupon_id'] = $this->coupon_id ; 
                $coupon = $this->Retrieve_Coupon_List($query_options) ; 
                    
                $this->coupon = $this->Action_Compile_Coupon_Details($coupon['results'][0],$stripe_coupon) ;                
                
                switch ($this->coupon['valid']) {
                    case 1:
                        $this->Set_Alert_Response(83) ; // Coupon is valid
                        break ;
                    default:
                        $this->Set_Alert_Response(66) ; // Coupon is NOT valid
                    }

                } else {
                
                    $this->Set_Alert_Response(66) ; // Coupon is not valid
                
                    }      
            
            }
        
        return $this ;        
        }
    
    
    public function API_Set_Coupon_List($query_settings = array()) {
        
        $continue = 1 ; 
        
        $query_options = array() ;
        
        if (isset($query_settings['limit'])) {
            $query_options['limit'] = $query_settings['limit'] ; 
            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {         
        
            // Retrieve the list of Stripe products
            $resource = 'Coupon' ; 
            $action = 'all' ; 
            $parameters = $query_options ;         

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $this->coupon_list = $execute['event']->__toArray(true) ;
                }         
        
            }

        return $this ;        
        }
    
    
    
    public function API_Set_Upcoming_Invoice($query_options = array()) {
        
        $continue = 1 ; 
        

        
//        if (isset($query_settings['limit'])) {
//            $query_options['limit'] = $query_settings['limit'] ; 
//            }
        
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {         
        
            // Set the API query
            $resource = 'Invoice' ; 
            $action = 'upcoming' ; 
            $parameters = $query_options ;         

            $execute = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($execute['success'] == 1) {
                $invoice = $execute['event']->__toArray(true) ;
                
                $invoice['timestamp_created'] = $invoice['created'] ;
                $invoice['timestamp_date'] = $invoice['date'] ;
                $invoice['timestamp_next_payment_attempt'] = $invoice['next_payment_attempt'] ;
                $invoice['timestamp_period_end'] = $invoice['period_end'] ;
                $invoice['timestamp_period_start'] = $invoice['period_start'] ;
                
                $l = 0 ; 
                foreach ($invoice['lines']['data'] as $line) {
                    $invoice['lines']['data'][$l]['timestamp_period_end'] = $line['period']['end'] ; 
                    $invoice['lines']['data'][$l]['timestamp_period_start'] = $line['period']['start'] ; 
                    
                    $invoice['lines']['data'][$l] = $this->Action_Time_Territorialize_Dataset($invoice['lines']['data'][$l]) ;
                    $l++ ; 
                    }
                
                
                
                $this->invoice = $this->Action_Time_Territorialize_Dataset($invoice) ;                
                }         
        
            }

        return $this ;        
        }
    
    
    public function Get_Invoice() {
        
        return $this->invoice ; 
        }
    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    
    
    public function Action_Parse_Stripe_Codes($error) { 
        
        switch ($error['error']['type']) {
            case 'invalid_request_error':
                switch ($error['error']['code']) {
                    case 'resource_missing':
                        if (strpos($error['error']['message'], 'No such source:') !== false) {
                            $alert_id = 57 ; // card_id was submitted that does not exist
                            } else {
                                $alert_id = 60 ; // No source card attached
                                }
                        break ;
                    case 'parameter_unknown':                        
                    default:
                        $alert_id = 49 ; 
                    }
                                
                break ;
            case 'card_error':
                switch ($error['error']['code']) {
                    case 'card_declined':
                        if ($error['error']['decline_code'] == 'insufficient_funds') {
                            $alert_id = 85 ; // Card declined due to insufficient funds
                            } elseif ($error['error']['decline_code'] == 'fraudulent') { 
                            $alert_id = 86 ; // Card declined due to insufficient funds
                            } else {
                                $alert_id = 84 ; // Card declined    
                                }                        
                        break ;
                    case 'incorrect_cvc':
                        $alert_id = 87 ; // Card declined due to cvc
                        break ; 
                    case 'expired_card':
                        $alert_id = 88 ; // Card declined due expiration date
                        break ;
                    case 'processing_error':
                        $alert_id = 89 ; // Card declined due to processing error
                        break ;                        
                    default:
                        $alert_id = 52 ; // Card decline or error
                    }
                break ;
            default:    
                $alert_id = 56 ; // Generic Stripe error (placeholder)
            }
        
        $error['alert_id'] = $alert_id ;
        return $error ;          
        
        }
    
    public function Action_Parse_Stripe_Exception_Error($error) {

        $result = array(
            'alert_id' => 56, // Generic Stripe error (placeholder)
            'success' => 0,
            'event' => array(
                'error' => $error->getJsonBody()['error']
                ),
            'error' => $error->getJsonBody()['error']
            ) ; 

        $result = $this->Action_Parse_Stripe_Codes($result) ;
        
        return $result ; 
        }
    
    public function Action_Execute_Stripe_Query($resource,$action,$parameters) {

        // Write the stripe function... 
        // Format:   \Stripe\Subscription::create(array)
        $stripe_function = '\\Stripe\\'.$resource.'::'.$action ;
                
        try {

            $execute = $stripe_function($parameters) ; 
            
            } catch(\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $error  = $body['error'];
                $parse_codes = $this->Action_Parse_Stripe_Codes($body) ; 
                $alert_id = $parse_codes['alert_id'] ; 
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $body = $e->getJsonBody();
                $error  = $body['error'];
                $alert_id = 53 ; 
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $error  = $body['error']; 
                $parse_codes = $this->Action_Parse_Stripe_Codes($body) ; 
                $alert_id = $parse_codes['alert_id'] ;            
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $e->getJsonBody();
                $error  = $body['error'];
                $alert_id = 54 ; 
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $body = $e->getJsonBody();
                $error  = $body['error'];
                $alert_id = 55 ; 
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $e->getJsonBody();
                $error  = $body['error'];
                $alert_id = 56 ; 
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $body = $e->getJsonBody();
                $error  = $body['error'];
                $alert_id = 57 ; 
            } 

        
        if ($error) { 
            $result = array(
                'success' => 0,
                'event' => $body,
                'error' => $error,
                'alert_id' => $alert_id
                ) ; 
            
            $this->Set_Alert_Response($result['alert_id']) ; // Probably error code
            $this->Append_Alert_Response('none',array(
                'admin_context' => $result['error']['message'],
                'location' => __METHOD__
                )) ;            
            
            } else {
                $result = array(
                    'success' => 1,
                    'event' => $execute
                    ) ;
                }
        
        $this->Set_Stripe_API_Event($result) ; 
        
        return $result ; 
        }
    
    

    
    public function Action_Apply_Coupon_To_Product_Set() {
        
        
        return $this ; 
        }
    
    
    
    public function Action_Process_Exceptions($error_object) {
        
        
        
        return $this ;
        }
    
    
    // Must supply internal plan and Stripe API requested plan
    public function Action_Compile_Plan_Details($plan,$stripe_plan,$plan_options = array()) { 
        
        $plan['product'] = $stripe_plan['product'] ;  // Redundant, but needed when updating the plan
            
        $plan['stripe_link'] = 'https://dashboard.stripe.com/'.$this->test_link_segment.'plans/'.$stripe_plan['id'] ;  

        // Interval fee formatting and currency
        $plan['amount'] = $stripe_plan['amount'] ;
        $plan['currency'] = strtoupper($stripe_plan['currency']) ;            


        // Interval naming and annual fee calculations...
        switch ($plan['billing_interval']) {
            case 'month':
                switch ($plan['billing_interval_count']) {
                    case 1:
                        $plan['annual_amount'] = $stripe_plan['amount'] * 12 ; 
                        $plan['quarterly_amount'] = $stripe_plan['amount'] * 3 ; 
                        $plan['monthly_amount'] = $stripe_plan['amount'] ; 
                        $plan['interval_name'] = 'month' ; 
                        $plan['interval_billing_name'] = 'monthly' ; 
                        $plan['interval_billing_name_alt'] = 'monthly' ; 
                        break ;
                    case 3:
                        $plan['annual_amount'] = $stripe_plan['amount'] * 4 ;
                        $plan['quarterly_amount'] = $stripe_plan['amount'] * 1 ; 
                        $plan['monthly_amount'] = $stripe_plan['amount']/3 ; 
                        $plan['interval_name'] = 'quarter' ; 
                        $plan['interval_billing_name'] = 'quarterly' ;
                        $plan['interval_billing_name_alt'] = 'quarterly' ; 
                        break ; 
                    }                    
                break ;
            case 'year':
                $plan['annual_amount'] = $stripe_plan['amount'] ; 
                $plan['quarterly_amount'] = $stripe_plan['amount'] / 4 ; 
                $plan['monthly_amount'] = $stripe_plan['amount']/12 ; 
                $plan['interval_name'] = 'year' ;
                $plan['interval_billing_name'] = 'yearly' ;
                $plan['interval_billing_name_alt'] = 'annually' ; 
                break ;
            }
        
        // Interval naming and annual fee calculations...
        switch ($plan['trial_plan_billing_interval']) {
            case 'month':
                switch ($plan['trial_plan_billing_interval_count']) {
                    case 1:
                        $plan['trial_plan_annual_amount'] = $plan['trial_plan_amount'] * 12 ; 
                        $plan['trial_plan_quarterly_amount'] = $plan['trial_plan_amount'] * 3 ; 
                        $plan['trial_plan_monthly_amount'] = $plan['trial_plan_amount'] ; 
                        $plan['trial_plan_interval_name'] = 'month' ; 
                        $plan['trial_plan_interval_billing_name'] = 'monthly' ; 
                        $plan['trial_plan_interval_billing_name_alt'] = 'monthly' ; 
                        break ;
                    case 3:
                        $plan['trial_plan_annual_amount'] = $plan['trial_plan_amount'] * 4 ;
                        $plan['trial_plan_quarterly_amount'] = $plan['trial_plan_amount'] * 1 ; 
                        $plan['trial_plan_monthly_amount'] = $plan['trial_plan_amount']/3 ; 
                        $plan['trial_plan_interval_name'] = 'quarter' ; 
                        $plan['trial_plan_interval_billing_name'] = 'quarterly' ;
                        $plan['trial_plan_interval_billing_name_alt'] = 'quarterly' ; 
                        break ; 
                    }                    
                break ;
            case 'year':
                $plan['trial_plan_annual_amount'] = $plan['trial_plan_amount'] ; 
                $plan['trial_plan_quarterly_amount'] = $plan['trial_plan_amount'] / 4 ; 
                $plan['trial_plan_monthly_amount'] = $plan['trial_plan_amount']/12 ; 
                $plan['trial_plan_interval_name'] = 'year' ;
                $plan['trial_plan_interval_billing_name'] = 'yearly' ;
                $plan['trial_plan_interval_billing_name_alt'] = 'annually' ; 
                break ;
            }
        
        
        switch ($stripe_plan['currency']) {
            case 'usd':
            default:
                $plan['amount_formatted'] = '$'.number_format($stripe_plan['amount']/100,2,'.',',') ; 
                $plan['annual_amount_formatted'] = '$'.number_format($plan['annual_amount']/100,2,'.',',') ;
                $plan['quarterly_amount_formatted'] = '$'.number_format($plan['quarterly_amount']/100,2,'.',',') ;
                $plan['monthly_amount_formatted'] = '$'.number_format($plan['monthly_amount']/100,2,'.',',') ;                
                
                if ($plan['trial_plan_amount']) {
                    $plan['trial_plan_amount_formatted'] = '$'.number_format($plan['trial_plan_amount']/100,2,'.',',') ; 
                    $plan['trial_plan_annual_amount_formatted'] = '$'.number_format($plan['trial_plan_annual_amount']/100,2,'.',',') ;
                    $plan['trial_plan_quarterly_amount_formatted'] = '$'.number_format($plan['trial_plan_quarterly_amount']/100,2,'.',',') ;
                    $plan['trial_plan_monthly_amount_formatted'] = '$'.number_format($plan['trial_plan_monthly_amount']/100,2,'.',',') ;                
                    }
          
                break ;
            }
    
        
        $plan = $this->Action_Apply_Coupon_Calculations('plan',$plan) ;
        
        return $plan ; 
        
        }
    
    
    
    public function Action_Apply_Coupon_Calculations($type,$data_set) {
        
        if ($this->coupon['valid'] == 1) {
            
            $data_set['coupon_applied'] = 1 ; 
            $data_set['coupon'] = $this->coupon ; 

            if ($type == 'plan') {

                if ($data_set['coupon']['percent_off']) {
                    $data_set['amount_discounted'] = $data_set['amount'] * ((100 - ($data_set['coupon']['percent_off'])) / 100) ;
                    $data_set['amount_discounted_formatted'] = '$'.number_format($data_set['amount_discounted']/100,2,'.',',') ; 
                    } 
                if ($data_set['coupon']['amount_off']) {
                    $data_set['amount_discounted'] = $data_set['amount'] - $data_set['coupon']['amount_off'] ;
                    $data_set['amount_discounted_formatted'] = '$'.number_format($data_set['amount_discounted']/100,2,'.',',') ; 
                    }

                }

            if ($type == 'product') {


                }
            
            } else {
                $data_set['coupon_applied'] = 0 ; 
                }
        
        return $data_set ; 
        
        }
    
    
    // Must supply internal plan and Stripe API requested plan
    public function Action_Compile_Coupon_Details($coupon,$stripe_coupon) {
        
        $coupon['stripe_link'] = 'https://dashboard.stripe.com/'.$this->test_link_segment.'coupons/'.$stripe_coupon['id'] ;  
        
        
        // STATUS AND VALIDITY
        $coupon['valid'] = $stripe_coupon['valid'] ; // Valid is a Stripe indicator
        
        switch ($coupon['active']) {
            case 2:
                $coupon['status'] = 'Expired' ; 
                $coupon['valid'] = 'expired' ; // Valid is a Stripe indicator
                break ;
            case 0:
                $coupon['status'] = 'Deleted' ;
                $coupon['valid'] = 'deleted' ; // Valid is a Stripe indicator
                break ; 
            case 1:
            default:
                $coupon['status'] = 'Active' ;                
            }
        
        if ($coupon['timestamp_expiration'] > 0) {
            $coupon['expiration_date_formatted'] = date("F j, Y",$coupon['timestamp_expiration']) ;
            } else {
                $coupon['expiration_date_formatted'] = '--' ;
                }
        
        
        // DURATION
        $coupon['duration'] = $stripe_coupon['duration'] ; 
        
        switch ($coupon['duration']) {
            case 'forever':
                $coupon['duration_formatted'] = 'to future invoices' ; 
                break ;
            case 'once':
                $coupon['duration_formatted'] = 'for one billing period' ; 
                break ;
            case 'repeating':
                $coupon['duration_formatted'] = 'for '.$stripe_coupon['duration_in_months'].' month(s)' ; 
                break ;
            }
        
        
        // FEE FORMATTING & CURRENCY
        if ($coupon['valid'] == 1) {
            
            $coupon['amount_off'] = $stripe_coupon['amount_off'] ; 
            
            if ($coupon['amount_off']) {
                switch ($stripe_coupon['currency']) {
                    case 'usd':
                    default:
                        $coupon['amount_off_formatted'] = '$'.number_format($coupon['amount_off']/100,2,'.',',') ; 
                        $coupon['discount_formatted'] = $coupon['amount_off_formatted'] ; 
                        break ;
                    }

                $coupon['currency'] = strtoupper($stripe_coupon['currency']) ;

                } else {
                    $coupon['percent_off_formatted'] = $stripe_coupon['percent_off'].'%' ;
                    $coupon['discount_formatted'] = $coupon['percent_off_formatted'] ; 
                    } 
            }
        
        return $coupon ; 
        
        }
    
    
    
    public function Action_Compile_Product_Set($query_options = array()) {
        
        // Get internal database plan list...
        $this->Set_Plan_List($query_options) ; 
        $plan_list = $this->Get_Plan_List() ; 
        
        
        // Get Stripe plans so we can match data
        $stripe_plan_list = $this->API_Set_Plan_List()->Get_Plan_List() ; 
        $stripe_plan_list = $stripe_plan_list['data'] ; 

 
        
        // Get Stripe products so we can match data
        $stripe_product_list = $this->API_Set_Product_List()->Get_Product_List() ; 
        $stripe_product_list = $stripe_product_list['data'] ;
        
        
        
        $product_set = array() ; 
        
        $p = 0 ; // plan counter
        $s = 0 ; // set counter
        foreach ($plan_list as $plan) {            
        
            
            $stripe_product = Utilities::Deep_Array($stripe_product_list,'id',$plan['product_id']) ; 
        
            
            
            // Include stripe plan details...
            $stripe_plan = Utilities::Deep_Array($stripe_plan_list,'id',$plan['plan_id']) ; 

            $plan = $this->Action_Compile_Plan_Details($plan,$stripe_plan,$plan_options) ; 
 
           
            
            // Create new set
            if ($plan['product_id'] != $plan_list[$p-1]['product_id']) {
                
                $feature_set = $this->Action_Map_Feature_Set($plan['product_id']) ;
                
                $new_product_set = array(
                    'product_id' => $plan['product_id'],
                    'product_name' => $plan['product_name'],
                    'product_description' => $plan['product_description'],
                    'monthly_marketing_credits' => $plan['monthly_marketing_credits'],
                    'registration_marketing_credits' => $plan['registration_marketing_credits'],
                    'max_marketing_credits' => $plan['max_marketing_credits'],
                    'statement_descriptor' => $stripe_product['statement_descriptor'],
                    'stripe_link' => 'https://dashboard.stripe.com/'.$this->test_link_segment.'subscriptions/products/'.$plan['product_id'],
                    'currency' => $plan['currency'],
                    'feature_set' => $feature_set['feature_set'],
                    'plans' => array()
                    ) ; 
                
                
                $new_product_set = $this->Action_Apply_Coupon_Calculations('product',$new_product_set) ;
                
                $product_set[] = $new_product_set ; 
                $product_set[$s]['plans'][] = $plan ; 
                $s++ ; 
                } else {
                
                    // Add to existing set
                    $product_set[$s-1]['plans'][] = $plan ; 
                    }   
            
            $p++ ; 
            }
        
        $this->product_set = $product_set ; 
        
        return $this ;
        }
    

    
    public function Action_Compile_Coupon_Set($query_options = array()) {
        
        // Get internal database plan list...
        $this->Set_Coupon_List() ; 
        $coupon_list = $this->Get_Coupon_List() ; 
        
        
        // Get Stripe plans so we can match data
        $stripe_coupon_list = $this->API_Set_Coupon_List()->Get_Coupon_List() ; 
        $stripe_coupon_list = $stripe_coupon_list['data'] ; 
              

        $coupon_set = array() ; 
        
        $c = 0 ; // coupon counter
        foreach ($coupon_list as $coupon) {            
            
            $stripe_coupon = Utilities::Deep_Array($stripe_coupon_list,'id',$coupon['coupon_id']) ; 
            
            $coupon = $this->Action_Compile_Coupon_Details($coupon,$stripe_coupon) ; 

            $coupon_set[] = $coupon ; 
 
            }
        
        $this->coupon_set = $coupon_set ; 
        
        return $this ;
        }
    
    
    
    
    public function Action_Create_System_Customer() {
        
        $continue = 1 ; 
        
        if (!isset($this->account_id)) {
            $continue = 0 ; 
            }
        
        // Ensure the prior stripe event (if it exists) succeeded
        $stripe_event = $this->Get_Stripe_API_Event() ;         
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {
            
            if (!$this->account_billing_id) {
                $customer_data['email'] = $this->account['account_owner']['email_address'] ; 
                $customer_data['description'] = $this->account['account_display_name'].' [Account ID: '.$this->account['account_id'].']' ; 
                $customer_data['metadata']['account_owner'] = $this->account['account_owner']['first_name'].' '.$this->account['account_owner']['last_name'].' [User ID: '.$this->account['account_owner']['user_id'].']' ; 

                
                // Create a system customer
                $resource = 'Customer' ; 
                $action = 'create' ; 
                $parameters = $customer_data ;         

                $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

                
                if ($stripe_event['success'] == 1) {
                    
                    $new_customer = $stripe_event['event']->__toArray(true) ;

                    $this->API_Set_Customer($new_customer['id']) ; 
                    $customer = $this->Get_Customer() ; 

                    if (isset($customer['id'])) {

                        $this->Set_Account_Billing_ID($customer['id']) ; 

                        $input = array(
                            'billing_id' => $this->account_billing_id
                            ) ; 

                        // Create History Entry
                        $history_input = array(
                            'account_id' => $this->account_id,
                            'function' => 'create_billing_customer',
                            'notes' => json_encode($input)
                            ) ; 
                        $user_history = $this->Create_User_History($this->user_id,$history_input) ;

                        // Add the billing_id to the account record
                        $update_account = $this->Update_Account($input) ;

                        // Create a new default subscription
//                        $this->Set_Default_Plan()->Action_Add_Subscription() ;
                        }
                    } 
                } else {
                    $this->Action_Update_System_Customer() ; 
                    }
            }
            
        return $this ;
        
        }
    
    
    public function Action_Update_System_Customer($account_id = 'internal',$customer_details = array()) {
        
        $continue = 1 ; 
        
        if ('internal' === $account_id) {
            $account_id = $this->account_id ; 
            } else {
                $this->account_id = $account_id ; 
                $this->Set_Account_By_ID($this->account_id) ; 
                }        
        
        if (!isset($this->account_id)) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            
            if ($this->account_billing_id) {
            
                
                // Retreive the customer from Stripe...
                $this->API_Set_Customer() ;
                $customer = $this->Get_Customer() ; 
                $stripe_event = $this->Get_Stripe_API_Event() ;                
                
                try {

                    $customer_update_entries = array(
                        'billing_id' => $this->account_billing_id,
                        'email' => $this->account['account_owner']['email_address'],
                        'description' => $this->account['account_display_name'].' [Account ID: '.$this->account['account_id'].']', 
                        'metadata_account_owner' => $this->account['account_owner']['first_name'].' '.$this->account['account_owner']['last_name'].' [User ID: '.$this->account['account_owner']['user_id'].']'
                        ) ; 

                    $stripe_event['event']->email = $customer_update_entries['email'] ; 
                    $stripe_event['event']->description = $customer_update_entries['description'] ; 
                    $stripe_event['event']->metadata->account_owner = $customer_update_entries['metadata_account_owner'] ;  
                    
                    if (isset($customer_details['default_source'])) {
                        $customer_update_entries['default_source'] = $customer_details['default_source'] ; // Should be a card ID
                        $stripe_event['event']->default_source = $customer_update_entries['default_source'] ; 
                        }
                                        
                    $stripe_event['event']->save(); // Execute the save at Stripe

                    $this->Set_Alert_Response(18) ; // Account updated
                    
                    // Create History Entry
                    $history_input = array(
                        'account_id' => $this->account_id,
                        'function' => 'update_billing_customer',
                        'notes' => json_encode($customer_update_entries)
                        ) ; 
                    $user_history = $this->Create_User_History($this->user_id,$history_input) ;                    
                    
                    } catch(Exception $e) {
                        
                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 
                    
                    
                        $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => $stripe_event['error']['message'],
                            'location' => __METHOD__
                            )) ;                    

                        }

                } else {
                
                    $this->Action_Create_System_Customer() ; 
                
                    }
            }
        
        if ($continue == 0) {
            $this->Set_Alert_Response(19) ; // Unable to update account
            }
        
        
        return $this ;
        
        }    
    
    
    
    
    public function Action_Create_Customer_Card($stripe_card_token,$card_details = array()) {
        
        $continue = 1 ; 
        
        if (!isset($this->account_id)) {
            $continue = 0 ; 
            }
        
        if (!isset($this->account_billing_id)) {
            $continue = 0 ; 
            }        
        
        if ($continue == 1) {
                
            $stripe_event = $this->API_Set_Customer()->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                }            
            
            if ($continue == 1) {                        
            
                try {

                    $card = $stripe_event['event']->sources->create(array("source" => $stripe_card_token));

                    // Create History Entry
                    $history_input = array(
                        'account_id' => $this->account_id,
                        'function' => 'create_billing_customer_card',
                        'notes' => json_encode($card)
                        ) ; 
                    $user_history = $this->Create_User_History($this->user_id,$history_input) ;
                    
                    
                    // FILTERS FOR UNACCEPTABLE CARDS
                    
                    // If we do not accept prepaid. Delete the card
                    if (($card['funding'] == 'prepaid') AND ($this->card_accept_prepaid == 0)) {
                        $continue = 0 ; 
                        $this->Action_Delete_Customer_Card($card['id']) ; 
                        $this->Set_Alert_Response(59) ; // Card was prepaid; submit an alert
                        }
                    
                    // Card country check
                    if ($continue == 1) {
                        
                        $blacklist = $this->user_create_blacklist ; 
                        foreach ($blacklist['card_country'] as $unallowed) {

                            if ($card['country'] == $unallowed) {
                                $continue = 0 ; 
                                $this->Action_Delete_Customer_Card($card['id']) ;
                                $this->Set_Alert_Response(86) ; // Fraudulent activity
                                break ; 
                                }

                            }                        

                        }                    

                    if ($continue == 1) {                    

                        // Record the event as success...
                        $stripe_event = array(
                            'success' => 1,
                            'event' => $card
                            ) ;                     
                        $this->Set_Stripe_API_Event($stripe_event) ;                         

                        $this->API_Set_Card_By_ID($card['id']) ;                                             
                        $this->Action_Update_Customer_Card($card['id'],$card_details) ; 
                        
                        $this->Set_Alert_Response(47) ; // Card successfully created  
                        }                    


                    } catch(Exception $e) {

                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 

                        $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => $stripe_event['error']['message'],
                            'location' => __METHOD__
                            )) ;                    

                        }            
            
                }
            
            }
        
        return $this ;
        }
    
    
    
    public function Action_Update_Customer_Card($card_id,$card_details) {
        
        $continue = 1 ; 
        $stripe_event = $this->Get_Stripe_API_Event() ;
        
        if (!isset($this->account_id)) {
            $continue = 0 ; 
            }
        
        if (!isset($this->account_billing_id)) {
            $continue = 0 ; 
            }        
        
        if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
                
            $stripe_event = $this->API_Set_Customer()->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                }            
            
            
            if ($continue == 1) {
                try {

                    // Retrieve the card
                    $card = $stripe_event['event']->sources->retrieve($card_id);    

                    } catch(Exception $e) {
                        $continue = 0 ; 

                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 
                        }                
                }
            
            
            if ($continue == 1) {
                                
                try {

                    if (isset($card_details['name'])) {
                        if (!$card_details['name']) {
                            $card_details['name'] = null ; 
                            }                    
                        $card->name = $card_details['name'] ; 
                        }
                    
                    if (isset($card_details['address_line1'])) {
                        if (!$card_details['address_line1']) {
                            $card_details['address_line1'] = null ; 
                            }                        
                        $card->address_line1 = $card_details['address_line1'] ; 
                        }
                    if (isset($card_details['address_line2'])) {
                        if (!$card_details['address_line2']) {
                            $card_details['address_line2'] = null ; 
                            }                         
                        $card->address_line2 = $card_details['address_line2'] ; 
                        }                      
                    if (isset($card_details['address_city'])) {
                        if (!$card_details['address_city']) {
                            $card_details['address_city'] = null ; 
                            }                         
                        $card->address_city = $card_details['address_city'] ; 
                        } 
                    if (isset($card_details['address_state'])) {
                        if (!$card_details['address_state']) {
                            $card_details['address_state'] = null ; 
                            }                         
                        $card->address_state = $card_details['address_state'] ; 
                        }                    
                    if (isset($card_details['address_country'])) {
                        if (!$card_details['address_country']) {
                            $card_details['address_country'] = null ; 
                            }                        
                        $card->address_country = $card_details['address_country'] ;  
                        }                    
                    if (isset($card_details['address_zip'])) {
                        if (!$card_details['address_zip']) {
                            $card_details['address_zip'] = null ; 
                            }                         
                        $card->address_zip = $card_details['address_zip'] ;  
                        }        
                    if (isset($card_details['exp_month'])) {
                        if (!$card_details['exp_month']) {
                            $card_details['exp_month'] = null ; 
                            }                         
                        $card->exp_month = $card_details['exp_month'] ; 
                        }
                    if (isset($card_details['exp_year'])) {
                        if (!$card_details['exp_year']) {
                            $card_details['exp_year'] = null ; 
                            }                        
                        $card->exp_year = $card_details['exp_year'] ; 
                        }

                    $card->save() ;                
                    $this->stripe_query_result = $card ;

                    
                    // Create History Entry
                    $history_notes = $card_details ;
                    $history_notes['card_id'] = $card_id ; 
                    $history_notes['account_billing_id'] = $this->account_billing_id ; 
                        
                    $history_input = array(
                        'account_id' => $this->account_id,
                        'function' => 'update_customer_card',
                        'notes' => json_encode($history_notes)
                        ) ; 
                    $user_history = $this->Create_User_History($this->user_id,$history_input) ;                    
                    
                    
                    // Record the event as success...
                    $stripe_event = array(
                        'success' => 1,
                        'event' => $card
                        ) ;                     
                    $this->Set_Stripe_API_Event($stripe_event) ; 
                    
                    $this->Set_Alert_Response(48) ; // Card successfully updated
                    
                    } catch(Exception $e) {
                        $continue = 0 ;  
                    
                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 

                        }
                
                }
            }
        
        
        if (($stripe_event['alert_id']) AND ($continue == 0)) {
            $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
            $this->Append_Alert_Response('none',array(
                'admin_context' => $stripe_event['error']['message'],
                'location' => __METHOD__
                )) ;
            
            }
        

        return $this ;
        
        }     
    

    public function Action_Delete_Customer_Card($card_id) {

        $continue = 1 ; 
        
        if (!isset($this->account_id)) {
            $continue = 0 ; 
            }
        
        if (!isset($this->account_billing_id)) {
            $continue = 0 ; 
            } 
        
        if ($continue == 1) {
                
            $stripe_event = $this->API_Set_Customer()->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                } 

            
            if ($continue == 1) {                        
            
                try {

                    $card = $stripe_event['event']->sources->retrieve($card_id)->delete();

                    // Create History Entry
                    $history_input = array(
                        'account_id' => $this->account_id,
                        'function' => 'delete_billing_customer_card',
                        'notes' => json_encode($card)
                        ) ; 
                    $user_history = $this->Create_User_History($this->user_id,$history_input) ;                    

                    // Record the event as success...
                    $stripe_event = array(
                        'success' => 1,
                        'event' => $card
                        ) ;                     
                    $this->Set_Stripe_API_Event($stripe_event) ; 

                    $this->Set_Alert_Response(58) ; // Card successfully deleted                      


                    } catch(Exception $e) {

                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 

                        $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => $stripe_event['error']['message'],
                            'location' => __METHOD__
                            )) ;                    

                        }            
            
                }            
            }
        
        return $this ;        
        }    
    
    
    public function API_Action_Update_Subscription_Item($query_options = array()) {
        
        $continue = 1 ; 
        
//        if (!isset($query_options['items'])) {
//            $continue = 0 ; 
//            }
        
        if (!isset($this->subscription_item_id)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                }         
            }
        

        if ($continue == 1) {       
            
            // Retrieve the existing subscription item
            $resource = 'SubscriptionItem' ; 
            $action = 'retrieve' ; 
            $parameters = $this->subscription_item_id ;

            $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($stripe_event['success'] == 0) {
                $continue = 0 ; 
                } 
            }
        
        

        // Update the subscription item
        if ($continue == 1) {

            try {

                $stripe_event['event']->update(
                    $this->subscription_item_id,
                    $query_options
                    );
                                
                } catch(Exception $e) {
                    $continue = 0 ; 

                    $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                    $this->Set_Stripe_API_Event($stripe_event) ; 

                    $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => $stripe_event['error']['message'],
                        'location' => __METHOD__
                        )) ;

                    }


            } 

        
        if ($continue == 1) {       
            
            // Retrieve the existing subscription item
            $resource = 'SubscriptionItem' ; 
            $action = 'retrieve' ; 
            $parameters = $this->subscription_item_id ;

            $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($stripe_event['success'] == 0) {
                $continue = 0 ; 
                } 
            }
        
        if ($continue == 1) {
        
            
            $subscription_item = $stripe_event['event']->__toArray(true) ;
                         
            $subscription = $this->API_Set_Subscription_By_ID($this->subscription_id)->Get_Subscription() ; 
            
            $subscription_item_input = array(
                'subscription_id' => $this->subscription_id,
                'subscription_item_id' => $this->subscription_item_id,
                'plan_id' => $subscription_item['plan']['id'],
                'timestamp_billing_period_end' => $subscription['current_period_end'],
                'billing_status' => $subscription['status']
                ) ;
                        
            $this->Action_Update_Subscription($subscription_item_input) ;             
            }
                
        
        return $this ; 
        }
    
    
    
    public function API_Action_Delete_Subscription_Discount($query_options = array()) {
        
        $continue = 1 ; 
        
//        if (!isset($query_options['items'])) {
//            $continue = 0 ; 
//            }
        
        if (!isset($this->subscription_id)) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                }         
            }
        

        if ($continue == 1) {       
            
            // Retrieve the existing subscription item
            $resource = 'Subscription' ; 
            $action = 'retrieve' ; 
            $parameters = $this->subscription_id ;

            $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

            if ($stripe_event['success'] == 0) {
                $continue = 0 ; 
                } 
            }
        
        

        // Update the subscription item
        if ($continue == 1) {

            try {

                $stripe_event['event']->deleteDiscount();
                                
                } catch(Exception $e) {
                    $continue = 0 ; 

                    $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                    $this->Set_Stripe_API_Event($stripe_event) ; 

                    $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => $stripe_event['error']['message'],
                        'location' => __METHOD__
                        )) ;

                    }


            } 

        return $this ; 
        }
    
    
    
    
    // Add a subscription to the customer in Stripe
    // Need to set $this->plan_id before this script can be processed.
    public function Action_Add_Subscription($query_options = array()) {
        
        $continue = 1 ; 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        if (!isset($this->account_id)) {
            $continue = 0 ; 
            }
        
        if (!isset($this->account_billing_id)) {
            $continue = 0 ; 
            }
        
        if (!isset($this->plan_id)) {
            if (isset($query_options->plan_id)) {            
                $this->plan_id = $query_options->plan_id ; 
                } else {
                    $continue = 0 ; 
                    }
            }
        
        if (!isset($this->customer)) {
            $this->API_Set_Customer() ; 
            }
    
        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                }         
            }
        
        // Validate the coupon if one was submitted...
        if (isset($query_options->coupon_id)) {
            
            // Check to see if Stripe considers the coupon valid
            $this->API_Set_Coupon_By_ID($query_options->coupon_id) ; 
            $coupon = $this->Get_Coupon() ; 
                        
            if ($coupon['valid'] != true) {
                $continue = 0 ; 
                $this->Set_Alert_Response(66) ; // Coupon is not valid
                } 
            
            if ($continue == 1) {
                
                $account_record = $this->Get_Account() ; 
                if ($account_record['coupon_id']) {
                    $continue = 0 ; 
                    $this->Set_Alert_Response(67) ; // Coupon already applied to this subscription
                    }                
                } 
            
            }
        
        
        if (($continue == 1) AND ($query_options->billing_approval != 'approved')) {
            $continue = 0 ; 
            $this->Set_Alert_Response(68) ; // Billing was not approved               
            }
        
        if ($continue == 1) {
        
            // Set the plan...
            $this->Set_Plan_By_ID($this->plan_id) ; 
            $data['plan_record'] = $this->Get_Plan() ; 
                        
            
            // Retrieve the existing subscriptions for the account.
            // Currently this is set up so that only one subscription can be applied per account.
            // $retrieve_subscriptions_query_options['primary_subscription'] = 1 ; 
            $subscriptions = $this->Retrieve_Account_Subscriptions_List() ;

            if (($subscriptions['result_count'] > 0) AND ($subscriptions['results'][0]['billing_status'] != 'canceled')) {

                if ($subscriptions['results'][0]['plan_id'] == $this->plan_id) {
                    
                    $this->Set_Alert_Response(46) ; // No changes made to plan
                    $this->Append_Alert_Response('none',array(
                                'admin_context' => json_encode($subscriptions['results'][0]),
                                'location' => __METHOD__
                                )) ;
                    
                    } else {

                        if (!isset($this->customer['default_source'])) {
                            
                            $this->Set_Alert_Response(60) ; // No credit card in Stripe
                            $this->Append_Alert_Response('none',array(
                                'admin_context' => $stripe_event['error']['message'],
                                'location' => __METHOD__
                                )) ;            
                            $continue = 0 ; 
                            
                            
                            // THIS IS OPTIONAL AND NEEDS TO BE UNCOMMENTED IF ACTIVATED...
                            // If there is a coupon set and the percent_off is 100, then allow the 
                            // subscription update to pass because the coupon will be charged.
//                            if (isset($query_options->coupon_id)) {                                
//                                $this->Set_Coupon_By_ID($query_options->coupon_id) ; 
//                                $coupon_record = $this->Get_Coupon() ; 
//                                
//                                if ($coupon_record['percent_off'] == 100) {
//                                    $continue = 1 ; 
//                                    }                                
//                                }                            
                            }
                    
                    
                        // If a valid coupon needs to be applied, add it to the subscription now...
                        // Start by retrieving the subscription.
                        if (($continue == 1) AND (isset($query_options->coupon_id))) {

                            // Update subscription #1
                            $subscription_id = $subscriptions['results'][0]['subscription_id'] ; 

                            // Retrieve the existing subscription item
                            $resource = 'Subscription' ; 
                            $action = 'retrieve' ; 
                            $parameters = $subscription_id ; 

                            $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;
                            
                            if ($stripe_event['success'] == 0) {
                                $continue = 0 ; 
                                } 
                            
                            
                            // Add the coupon to Stripe
                            if ($continue == 1) {
                                
                                try {
                                
                                    $stripe_event['event']->coupon = $query_options->coupon_id ;
                                    $stripe_event['event']->save();   

                                    } catch(Exception $e) {
                                        $continue = 0 ; 

                                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                                        $this->Set_Stripe_API_Event($stripe_event) ; 

                                    
                                        $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                                        $this->Append_Alert_Response('none',array(
                                            'admin_context' => $stripe_event['error']['message'],
                                            'location' => __METHOD__
                                            )) ;

                                        }
                                
                                
                                } // END: Add coupon / update subscription
                            
                            }                    
                    
                    
                        // Retrieve the subscription item from Stripe
                        if ($continue == 1) {
                            
                            $subscription_item_id = $subscriptions['results'][0]['subscription_item_id'] ; 

                            // Retrieve the existing subscription item
                            $resource = 'SubscriptionItem' ; 
                            $action = 'retrieve' ; 
                            $parameters = $subscription_item_id ; 

                            $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;
                            
                            if ($stripe_event['success'] == 0) {
                                $continue = 0 ; 
                                }                    
                            }
                    
                        // Update the subscription item with the updated plan_id
                        if ($continue == 1) {
                        
                            try {
                                
                                // Save the plan update to stripe
                                $stripe_event['event']->plan = $this->plan_id ;
                                
                                if (isset($query_options->proration_behavior)) {
                                    $stripe_event['event']->proration_behavior = $query_options->proration_behavior ;
                                    }

                                $stripe_event['event']->save();

                                
                                // Retrieve the parent subscription to get current end period and status...
                                $resource = 'Subscription' ; 
                                $action = 'retrieve' ; 
                                $parameters = $stripe_event['event']['subscription'] ; 

                                $execute_subscription = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;                                
                                
                                $subscription_input = array(
                                    'subscription_id' => $stripe_event['event']['subscription'],
                                    'subscription_item_id' => $stripe_event['event']['id'],
                                    'plan_id' => $this->plan_id,
                                    'timestamp_billing_period_end' => $execute_subscription['event']['current_period_end'],
                                    'billing_status' => $execute_subscription['event']['status']
                                    ) ; 
                                
                                if (isset($query_options->coupon_id)) {
                                    $subscription_input['coupon_id'] = $query_options->coupon_id ;
                                    }   
                                                            
                                    
                                
                                $record_subscription = $this->Update_Subscription($subscription_input) ; 

                                $this->Set_Alert_Response(45) ; // Subscription plan updated     

                                
                                // Add cron action for adding bonus credit...
                                if ($data['plan_record']['bonus_marketing_credits'] > 0) {
                                    
                                    $automation_values = array(
                                        'task_action' => 'add_marketing_credits',
                                        'timestamp_action' => TIMESTAMP + (60 * 60 * 24 * 30),
                                        'structured_data' => array(
                                            'account_id' => $this->account_id,
                                            'marketing_credits' => $data['plan_record']['bonus_marketing_credits']
                                            )
                                        ) ;
                                    
                                    $this->Action_Create_Cron_Task_Automation($automation_values) ; 
                                    }
                                
                                
                                } catch(Exception $e) {
                                    $continue = 0 ; 
                                
                                    $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                                    $this->Set_Stripe_API_Event($stripe_event) ; 

                                
                                    $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                                    $this->Append_Alert_Response('none',array(
                                        'admin_context' => $stripe_event['error']['message'],
                                        'location' => __METHOD__
                                        )) ;
 
                                    } 
                            } // END: Update subscription item

                        }

                } else {

                    // NO EXISTING SUBSCRIPTION!!
                    // Create new subscription
                
                    $plan_items_array = array() ; 
                    $plan_items_array['plan'] = $this->plan_id ; 

                    // Manually override the plan trial period
                    if (isset($query_options->trial_period_days)) {
                        $plan_items_array['trial_period_days'] = $query_options->trial_period_days ; 
                        }
                
                
                    // Stripe parameters
                    $resource = 'Subscription' ; 
                    $action = 'create' ; 
                    $parameters = array(
                        "customer" => $this->account_billing_id,
                        "items" => array($plan_items_array)
                        ) ; 

                    // Add coupon if provided and validated...
                    if (isset($query_options->coupon_id)) {
                        $parameters['coupon'] = $query_options->coupon_id ; 
                        }
                
                
                    $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;

                    if ($stripe_event['success'] == 1) {
                        
                        $subscription_input = array(
                            'subscription_id' => $stripe_event['event']['id'],
                            'subscription_item_id' => $stripe_event['event']['items']['data'][0]['id'],
                            'plan_id' => $stripe_event['event']['items']['data'][0]['plan']['id'],
                            'timestamp_billing_period_end' => $stripe_event['event']['current_period_end'],
                            'billing_status' => $stripe_event['event']['status']
                            ) ; 
                        $record_subscription = $this->Update_Subscription($subscription_input) ; 

                        $this->Set_Alert_Response(45) ; // Subscription plan updated 
                        
                        
                        // Add cron action for adding bonus credit...
                        if ($data['plan_record']['bonus_marketing_credits'] > 0) {

                            $automation_values = array(
                                'task_action' => 'add_marketing_credits',
                                'timestamp_action' => TIMESTAMP + (60 * 60 * 24 * 30),
                                'structured_data' => array(
                                    'account_id' => $this->account_id,
                                    'marketing_credits' => $data['plan_record']['bonus_marketing_credits']
                                    )
                                ) ;
                            
                            $this->Action_Create_Cron_Task_Automation($automation_values) ; 
                            }
                        
                        // Delete the old canceled subscription...
                        if (($subscriptions['result_count'] > 0) AND ($subscriptions['results'][0]['billing_status'] == 'canceled')) {
                            
                            $delete_query = array(
                                'internal_subscription_id' => $subscriptions['results'][0]['internal_subscription_id']
                                ) ; 
                            $delete_subscription = $this->Delete_Subscription($delete_query) ; 
                            }
                                                
                        }            
                    }
            }

        return $this ; 
        }
    
    
    
    // Remove subscription from an account
    public function Action_Remove_Subscription($query_options = array()) {
        
        $continue = 1 ; 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        if (!isset($this->account_id)) {
            $continue = 0 ; 
            }
        
        if (!isset($this->account_billing_id)) {
            $continue = 0 ; 
            }
        
        
        if (!isset($this->customer)) {
            $this->API_Set_Customer() ; 
            }
    
        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                }         
            }
        
        
        if ($continue == 1) {
        
            // Retrieve the existing subscription item
            $resource = 'Subscription' ; 
            $action = 'retrieve' ; 
            $parameters = $query_options->subscription_id ; 

            $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ; 
                                                  
            $subscription_input = array(
                'subscription_id' => $stripe_event['event']['id'],
                'subscription_item_id' => $stripe_event['event']['items']['data'][0]['id'],
                'plan_id' => $stripe_event['event']['items']['data'][0]['plan']['id'],
                'timestamp_billing_period_end' => $stripe_event['event']['current_period_end']
                ) ;
            
            try {

                switch ($query_options->cancel_at_period_end) {
                    case 'restore':                        
                        
                        $stripe_event['event']->cancel_at_period_end = false ;
                        $stripe_event['event']->save();

                        $subscription_input['timestamp_cancel_at'] = 0 ;
                        $subscription_input['billing_status'] = $stripe_event['event']['status'] ;
                        
                        break ; 
                    case 'yes':
                    case 'true':    

                        $stripe_event['event']->cancel_at_period_end = true ;
                        $stripe_event['event']->save();

                        $subscription_input['timestamp_cancel_at'] = $stripe_event['event']['cancel_at'] ;
                        $subscription_input['billing_status'] = $stripe_event['event']['status'] ;
                        
                        break ;
                    case 'no':
                    case 'false':    
                    default:

                        $stripe_event['event']->cancel() ;
                        
                        $subscription_input['timestamp_cancel_at'] = $stripe_event['event']['canceled_at'] ;
                        $subscription_input['billing_status'] = $stripe_event['event']['status'] ;
                    }

                $this->Set_Stripe_API_Event($stripe_event) ;                    

                if ($stripe_event['success'] == 1) {

 
                    $record_subscription = $this->Update_Subscription($subscription_input) ; 
                    
                    switch ($query_options->cancel_at_period_end) {
                        case 'restore':
                            $this->Set_Alert_Response(229) ; // Billing subscription has been set to cancel. 
                            break ; 
                        default: 
                            $this->Set_Alert_Response(226) ; // Billing subscription has been set to cancel. 
                        }
                    }                
                

                } catch(Exception $e) {

                    $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     
                    $this->Set_Stripe_API_Event($stripe_event) ; 

                    $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => $stripe_event['error']['message'],
                        'location' => __METHOD__
                        )) ;                    

                    }            

            
            }

        return $this ; 
        }
    
    
    
    // THIS HITS THE API...
    public function Action_Update_Product($query_options = array()) {

        if (isset($query_options['active'])) {
            switch ($query_options['active']) {
                case 'true':
                    $query_options['active'] = 1 ; 
                    break ; 
                case 'false':
                    $query_options['active'] = 0 ; 
                    break ;
                }
            }
        
        

        if (isset($query_options['feature_set_toggle'])) {
            

            $this->Set_Product_By_ID() ; 
            $product_record = $this->Get_Product() ; 

            $current_feature_set = json_decode($product_record['feature_set'],1); 
            $find_current_key = array_search($query_options['feature_set_id'], $current_feature_set['feature_set']) ; 
            

            switch ($query_options['feature_set_toggle']) {
                case 'true': // Turn the feature set on
                    
                    if (is_integer($find_current_key)) {                    
                        
                        } else {
                            $current_feature_set['feature_set'][] = $query_options['feature_set_id'] ; 
                            }
                    
                    break ; 
                case 'false': // Turn the feature set off
                    
                    if (is_integer($find_current_key)) {
                        unset($current_feature_set['feature_set'][$find_current_key]) ; 
                        foreach($current_feature_set['feature_set'] as $value) {
                            $new_feature_set[] = $value ;
                            }
                        $current_feature_set['feature_set'] = $new_feature_set ; 
                        
                        }
                                         
                    break ;
                }
            
            $query_options['feature_set'] = json_encode($current_feature_set,JSON_NUMERIC_CHECK) ;
            unset($query_options['feature_set_toggle'],$query_options['feature_set_id']) ;
            }
        
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        

        
        
        $continue = 1 ; 
        
        if (!isset($this->product_id)) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                }         
            }
        
        if ($continue == 1) {                       
            
                try {

                    if (isset($query_options->product_name)) {
                        $stripe_event['event']->name = $query_options->product_name ; 
                        }
                    if (isset($query_options->product_description) AND ($stripe_event['event']['type'] == 'good')) {
                        $stripe_event['event']->description = $query_options->product_description ; 
                        }
                    if (isset($query_options->statement_descriptor)) {
                        $stripe_event['event']->statement_descriptor = $query_options->statement_descriptor ; 
                        }
                    
                    $stripe_event['event']->save();
                    
                    // Record the event as success...
                    $stripe_event = array(
                        'success' => 1,
                        'event' => $stripe_event['event']
                        ) ; 
                    
                    $this->Set_Stripe_API_Event($stripe_event) ; 

                    
                    $update_product = $this->Update_Product($query_options) ; 
//                    print_r($update_product) ;
                    
                    
                    $this->Set_Alert_Response(61) ; // Product successfully updated                      


                    } catch(Exception $e) {

                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 
                    

                        $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => $stripe_event['error']['message'],
                            'location' => __METHOD__
                            )) ;                    

                        }            
                      
            }
        
        return $this ;        
        }
    
    
    
    public function Action_Update_Plan($query_options = array()) {

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $continue = 1 ; 
        
        if (!isset($this->plan_id)) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                } 
            
            $current_plan = $this->Get_Plan() ; 

            }
        
        if ($continue == 1) {                       
            
                try {

                    $save_additional_updates = 1 ; 
                    
                    if (isset($query_options->product_id) AND ($current_plan['product'] != $query_options->product_id)) {
                        $stripe_event['event']->product = $query_options->product_id ; 
                        $query_options = array(
                            'product_id' => $query_options->product_id
                            ) ; 
                        $save_additional_updates = 0 ; 
                        } 
                    
                    if ($save_additional_updates == 1) {
                        if (isset($query_options->plan_name)) {
                            $stripe_event['event']->nickname = $query_options->plan_name ; 
                            }
                        if (isset($query_options->trial_period_days)) {
                            $stripe_event['event']->trial_period_days = $query_options->trial_period_days ; 
                            }
                        }
                    
                    $stripe_event['event']->save();
                    
                    // Record the event as success...
                    $stripe_event = array(
                        'success' => 1,
                        'event' => $stripe_event['event']
                        ) ; 
                    
                    $this->Set_Stripe_API_Event($stripe_event) ; 

                    $update_product = $this->Update_Plan($query_options) ; 
                    
                    $this->Set_Alert_Response(62) ; // Plan successfully updated                      


                    } catch(Exception $e) {

                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 
                    

                        $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => $stripe_event['error']['message'],
                            'location' => __METHOD__
                            )) ;                    

                        }            
                      
            }
        
        return $this ;        
        }
    
    
    
    
    public function Action_Create_Coupon($query_options = array()) {

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $continue = 1 ; 

        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                } 

            }
        
        if ($continue == 1) {                       
            
                
                $coupon_data['name'] = $query_options->coupon_name ; 
                $coupon_data['duration'] = $query_options->duration ; 
            
                if ($query_options->duration == 'repeating') {
                    $coupon_data['duration_in_months'] = $query_options->duration_in_months ; 
                    }
                        
                if (isset($query_options->coupon_id)) {
                    $coupon_data['id'] = $query_options->coupon_id ; 
                    }

                if (isset($query_options->max_redemptions)) {
                    $coupon_data['max_redemptions'] = $query_options->max_redemptions ; 
                    }
            
                if (isset($query_options->redeem_by)) {
                    $coupon_data['redeem_by'] = $query_options->redeem_by ; 
                    }
            
                switch ($query_options->coupon_type) {
                    case 'amount_discount':
                        $coupon_data['amount_off'] = $query_options->amount_off ;     
                        $coupon_data['currency'] = $query_options->currency ; 
                        break ;
                    case 'percentage_discount':    
                        $coupon_data['percent_off'] = $query_options->percent_off ;                         
                        break ;
                    }
                
            
                // Create a system customer
                $resource = 'Coupon' ; 
                $action = 'create' ; 
                $parameters = $coupon_data ;         

                $stripe_event = $this->Action_Execute_Stripe_Query($resource,$action,$parameters) ;
            
                if ($stripe_event['success'] == 1) {
                    
                    $new_coupon = $stripe_event['event']->__toArray(true) ;
                    $save_coupon = $this->Create_Coupon($new_coupon) ; 
                    $this->Set_Alert_Response(64) ; // Coupon successfully created 
                    
                    }                 
                            
                      
            }
        
        return $this ;        
        }
    
    
    public function Action_Update_Coupon($query_options = array()) {

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $continue = 1 ; 
        
        if (!isset($this->coupon_id)) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                } 
            
            $current_coupon = $this->Get_Coupon() ; 

            }
        
        if ($continue == 1) {                       
            
                try {

                    if (isset($query_options->coupon_name)) {
                        $stripe_event['event']->name = $query_options->coupon_name ; 
                        }
                    
//                    if (isset($query_options->metadata)) {
//                        $stripe_event['event']->metadata = $query_options->metadata ; 
//                        }
                    
                    $stripe_event['event']->save();
                    
                    // Record the event as success...
                    $stripe_event = array(
                        'success' => 1,
                        'event' => $stripe_event['event']
                        ) ; 
                    
                    $this->Set_Stripe_API_Event($stripe_event) ; 

                    $update_coupon = $this->Update_Coupon($query_options) ; 
                    
                    $this->Set_Alert_Response(63) ; // Coupon successfully updated                      


                    } catch(Exception $e) {

                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 
                    

                        $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => $stripe_event['error']['message'],
                            'location' => __METHOD__
                            )) ;                    

                        }            
                      
            }
        
        return $this ;        
        }
    
    public function Action_Delete_Coupon($query_options = array()) {

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $continue = 1 ; 
        
        if (!isset($this->coupon_id)) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            $stripe_event = $this->Get_Stripe_API_Event() ; 

            if (isset($stripe_event['success']) AND ($stripe_event['success'] == 0)) {
                $continue = 0 ; 
                } 
            
            $current_coupon = $this->Get_Coupon() ; 

            }
        
        
        if ($query_options->active != 0) {
            $continue = 0 ;    
            }
        
        if ($continue == 1) {                       
            
                try {

                    $stripe_event['event']->delete();
                    
                    // Record the event as success...
                    $stripe_event = array(
                        'success' => 1,
                        'event' => $stripe_event['event']
                        ) ; 
                    
                    $this->Set_Stripe_API_Event($stripe_event) ; 

                    $update_coupon = $this->Update_Coupon($query_options) ; 
                    
                    $this->Set_Alert_Response(65) ; // Coupon successfully updated                      


                    } catch(Exception $e) {

                        $stripe_event = $this->Action_Parse_Stripe_Exception_Error($e) ;                     

                        $this->Set_Stripe_API_Event($stripe_event) ; 
                    

                        $this->Set_Alert_Response($stripe_event['alert_id']) ; // Probably error code
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => $stripe_event['error']['message'],
                            'location' => __METHOD__
                            )) ;                    

                        }            
                      
            }
        
        return $this ;        
        }    
    
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    //////////////////////     
    
    
    
    
    public function Create_Coupon($query_options = array()) {
        
         
        
        $query_array = array(
            'table' => "system_billing_coupons",
            'values' => array(
                'coupon_name' => $query_options['name'],
                'coupon_id' => $query_options['id'],
                'active' => 1,
                'percent_off' => $query_options['percent_off'],
                'amount_off' => $query_options['amount_off'],
                'timestamp_created' => $query_options['created'],
                'timestamp_updated' => TIMESTAMP,
                'timestamp_expiration' => $query_options['redeem_by']
                ),
            'where' => "system_billing_coupons.coupon_id='$new_account_username'"
            );

        $account_record = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        
        return $result ; 
        }
    
    
    
    public function Retrieve_Product_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        

        $query_array = array(
            'table' => 'system_billing_products',
            'fields' => "system_billing_products.*, ",
            'where' => "system_billing_products.product_id_internal>'0' ",
            'order_by' => "system_billing_products.order_by",
            'order' => "ASC"
            );               
        

        if (isset($query_options->product_id)) {
            $query_array['where'] .= " AND system_billing_products.product_id='$query_options->product_id'" ; 
            }
            
        if (isset($query_options->system_test)) {
            $query_array['where'] .= " AND system_billing_products.system_test='$query_options->system_test'" ; 
            }        
        
        if (isset($query_options->active)) {
            switch ($query_options->active) {
                case 'both':
                    $query_array['where'] .= " AND (system_billing_products.active='0' OR system_billing_products.active='1')" ; 
                    break ;                    
                case 0:
                case 'inactive':
                    $query_array['where'] .= " AND system_billing_products.active='0'" ; 
                    break ;                    
                case 1:
                case 'active':
                default;    
                    $query_array['where'] .= " AND system_billing_products.active='1'" ; 
                    break ;                     
                }
            }
        
        
        
        // upgrade_to subquery
        $subquery_upgrade_to_input = array(
            'skip_query' => 1,
            'table' => 'system_billing_products',
            'fields' => "
                system_billing_products.product_id_internal AS upgrade_product_id_internal, 
                system_billing_products.product_id AS upgrade_product_id, 
                system_billing_products.product_name AS upgrade_product_name, 
                system_billing_products.feature_set AS upgrade_feature_set,
                system_billing_products.upgrade_to_product_id AS upgrade_upgrade_to_product_id
                "
            );       

        $subquery_upgrade_result = $this->DB->Query('SELECT',$subquery_upgrade_to_input);        
        $subquery_upgrade_product = $subquery_upgrade_result['query'] ; 

        $sub_upgrade_product_statement = "(".$subquery_upgrade_product.") AS upgrade_product " ;        

        $query_array['join_tables'][] = array(
            'table' => $sub_upgrade_product_statement,
            'on' => 'upgrade_product.upgrade_product_id_internal',
            'match' => 'system_billing_products.upgrade_to_product_id',
            'type' => 'left'
            );                

        $query_array['fields'] .= " 
            upgrade_product.upgrade_product_id_internal,
            upgrade_product.upgrade_product_id,
            upgrade_product.upgrade_product_name,
            upgrade_product.upgrade_feature_set,
            upgrade_product.upgrade_upgrade_to_product_id " ;
        
        
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        return $result ; 
        }
    
    
    public function Retrieve_Plan_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
 
        $query_array = array(
            'table' => 'system_billing_plans',
            'fields' => "system_billing_plans.*, 
                system_billing_products.*,
                system_billing_products.order_by as product_order_by,
                system_billing_plans.order_by as plan_order_by,
                system_billing_plans.active as billing_plan_active,
                system_billing_products.active as billing_product_active ",
            'where' => "system_billing_products.product_id_internal>'0' ",
            'order_by' => array(),
            'order' => array()
            );        
        
        $query_array['order_by'][] = "system_billing_products.order_by" ; 
        $query_array['order'][] = "ASC" ; 
        
        $query_array['order_by'][] = "system_billing_plans.order_by" ; 
        $query_array['order'][] = "ASC" ;
        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_billing_products',
            'on' => 'system_billing_products.product_id',
            'match' => 'system_billing_plans.product_id'
            );        
        
        
        
        // Join trial plans. This is not a filter.
        $trial_plan_table = 'trial_plan' ; 
        
        $query_array['fields'] .= ", $trial_plan_table.plan_name AS trial_plan_name, 
            $trial_plan_table.plan_name AS trial_plan_name,
            $trial_plan_table.amount AS trial_plan_amount, 
            $trial_plan_table.billing_interval AS trial_plan_billing_interval, 
            $trial_plan_table.billing_interval_count AS trial_plan_billing_interval_count 
            " ; 

        $query_array['join_tables'][] = array(
            'table' => "(SELECT 
                system_billing_plans.* 
                FROM system_billing_plans  
                JOIN system_billing_products ON system_billing_products.product_id = system_billing_plans.product_id 
                ) AS $trial_plan_table",
            'statement' => "(system_billing_plans.trial_plan_id=$trial_plan_table.plan_id)",
            'type' => 'left'
            );         
        
        
        
        if (isset($query_options->plan_id)) {
            $query_array['where'] .= " AND system_billing_plans.plan_id='$query_options->plan_id'" ; 
            }
        
        if (isset($query_options->product_id)) {
            $query_array['where'] .= " AND system_billing_products.product_id='$query_options->product_id'" ; 
            }
        
        if (isset($query_options->default_plan)) {
            $query_array['where'] .= " AND system_billing_plans.default_plan='$query_options->default_plan'" ; 
            }        
        
        if (isset($query_options->default_product)) {
            $query_array['where'] .= " AND system_billing_products.default_product='$query_options->default_product'" ; 
            } 
        

        
        if (isset($query_options->active)) {
            switch ($query_options->active) {
                case 'both':
                    $query_array['where'] .= " AND (system_billing_plans.active=0 OR system_billing_plans.active=1)" ; 
                    break ;                    
                case 'inactive': 
                    $query_array['where'] .= " AND system_billing_plans.active=0" ; 
                    break ;                    
                case 'active':  
                default:  
                    $query_array['where'] .= " AND system_billing_plans.active=1" ; 
                    break ;                     
                }
            }
        
        if (isset($query_options->product_active)) {
            switch ($query_options->product_active) {
                case 'both':
                    $query_array['where'] .= " AND (system_billing_products.active=0 OR system_billing_products.active=1)" ; 
                    break ;                    
                case 'inactive': 
                    $query_array['where'] .= " AND system_billing_products.active=0" ; 
                    break ;                    
                case 'active':  
                default:  
                    $query_array['where'] .= " AND system_billing_products.active=1" ; 
                    break ;                     
                }
            }
        
        if (isset($query_options->system_test)) {
            $query_array['where'] .= " AND system_billing_plans.system_test='$query_options->system_test'" ; 
            }
        
    
        
        
 
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->stripe_query_result = $result ; 
        
        
        return $result ; 
        }

    
    public function Retrieve_Coupon_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
         

        
        $query_array = array(
            'table' => 'system_billing_coupons',
            'fields' => "system_billing_coupons.*",
            'where' => "system_billing_coupons.coupon_id_internal>'0' " 
            );               
        
        if (isset($query_options->coupon_id)) {
            $query_array['where'] .= " AND system_billing_coupons.coupon_id='$query_options->coupon_id'" ; 
            }
        
        if (isset($query_options->system_test)) {
            $query_array['where'] .= " AND system_billing_coupons.system_test='$query_options->system_test'" ; 
            }
        
        if (isset($query_options->active)) {
            switch ($query_options->active) {
                case 'both':
                    $query_array['where'] .= " AND (system_billing_coupons.active='0' OR system_billing_coupons.active='1')" ; 
                    break ;                    
                case 0:
                case 'inactive':
                    $query_array['where'] .= " AND system_billing_coupons.active='0'" ; 
                    break ;                    
                case 1:
                case 'active':
                default;    
                    $query_array['where'] .= " AND system_billing_coupons.active='1'" ; 
                    break ;                     
                }
            }        
        
        // Initial result pull
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        
        // Run a check on coupon expiration dates to determine whether or not any are expired and need to be updated as expired
        $repull = 0 ; 
        foreach ($result['results'] as $coupon) {
            
            if (($coupon['timestamp_expiration'] < TIMESTAMP) AND ($coupon['timestamp_expiration'] > 0) AND ($coupon['active'] == 1)) {
                
                $input = array(
                    'active' => 2 // Expired
                    ) ;                 
                $update_coupon = $this->Set_Coupon_ID($coupon['coupon_id'])->Update_Coupon($input) ; // Marks coupon as expired in database
                $repull = 1 ; // Instructs script to repull results since they're now inaccurate
                }
            }
        
        // Repulls results in the event of a coupon update
        if ($repull == 1) {
            $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
            }
        
        $this->stripe_query_result = $result ; 
                
        return $result ; 
        }  
    
    
    
    public function Update_Coupon($input) {
        
         
        
        $values_array = array() ; 
        foreach ($input as $key => $value) {
            
            $values_array[$key] = $value ; 
            
            }
            
        $values_array['timestamp_updated'] = TIMESTAMP ; 
        
        // Updates the coupon in the database
        $query_array = array(
            'table' => 'system_billing_coupons',
            'values' => $values_array,
            'where' => "coupon_id='$this->coupon_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array); 
        $this->stripe_query_result = $result ;        
        
        return $result ; 
        
        }
    
    
    public function Update_Product($input) {
        
         
        
        $values_array = array() ; 
        foreach ($input as $key => $value) {
            
            $values_array[$key] = $value ; 
            
            }
            
        $values_array['timestamp_updated'] = TIMESTAMP ; 
        
        // Updates the new account account in the database
        $query_array = array(
            'table' => 'system_billing_products',
            'values' => $values_array,
            'where' => "product_id='$this->product_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array); 
        $this->stripe_query_result = $result ;        
        
        return $result ; 
        
        }
    
    public function Update_Plan($input) {
        
         
        
        $values_array = array() ; 
        foreach ($input as $key => $value) {
            
            $values_array[$key] = $value ; 
            
            }
            
        $values_array['timestamp_updated'] = TIMESTAMP ; 
        
        // Updates the new account account in the database
        $query_array = array(
            'table' => 'system_billing_plans',
            'values' => $values_array,
            'where' => "plan_id='$this->plan_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array); 
        $this->stripe_query_result = $result ;        
        
        return $result ; 
        
        }    
    
    
    // Update the internal database entry of an account's subscription
    // Required Inputs: subscription_id, subscription_item_id
    public function Action_Update_Subscription($subscription_input,$additional_parameters = array()) {
    
        $continue = 1 ;
        
        // Required input: Provided by Stripe
        if (!isset($subscription_input['subscription_id'])) {
            $continue = 0 ;
            }
        // Required input: Provided by Stripe
        if (!isset($subscription_input['subscription_item_id'])) {
            $continue = 0 ;
            }        
        
        if ($continue == 1) {
            
            $result = $this->Update_Subscription($subscription_input,$additional_parameters) ; 
            
            if ($result['results'] == true) {

                $this->Set_Alert_Response(45) ; // Success updating subscription
                } else {

                    $this->Set_Alert_Response(228) ; // Error updating subscription
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result)
                        )) ;             

                    }            
            }
        
        
        return $this ; 
        } 
    
    
    public function Delete_Subscription($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
                
        $query_array = array(
            'table' => "account_billing",
            );
        
        if (isset($query_options->internal_subscription_id)) {
            $query_array['where'] = "account_billing.internal_subscription_id='$query_options->internal_subscription_id'" ; 
            } else if (isset($query_options->subscription_item_id)) {
                $query_array['where'] = "account_billing.subscription_item_id='$query_options->subscription_item_id'" ; 
                }
        
        if (isset($query_array['where'])) {
            $result = $this->DB->Query('DELETE',$query_array);    
            }
                    
        $this->stripe_query_result = $result ;        
        
        return $result ;
        }
    
    
    
    public function Update_Subscription($subscription_input,$additional_parameters = array()) {
        

        $subscription_id = $subscription_input['subscription_id'] ; 
        $subscription_item_id = $subscription_input['subscription_item_id'] ; 

        $query_array = array(
            'table' => "account_billing",
            'values' => array(
                'account_id' => $this->account_id,
                'subscription_id' => $subscription_id,
                'subscription_item_id' => $subscription_item_id
                ),
            'where' => "account_billing.account_id='$this->account_id' AND account_billing.subscription_id='$subscription_id' AND account_billing.subscription_item_id='$subscription_item_id'"
            );
        
        if (isset($subscription_input['plan_id'])) {
            $query_array['values']['plan_id'] = $subscription_input['plan_id'] ; 
            }        
        if (isset($subscription_input['billing_status'])) {
            $query_array['values']['billing_status'] = $subscription_input['billing_status'] ; 
            }
        if (isset($subscription_input['timestamp_cancel_at'])) {
            $query_array['values']['timestamp_cancel_at'] = $subscription_input['timestamp_cancel_at'] ; 
            }        
        if (isset($subscription_input['timestamp_billing_period_end'])) {
            $query_array['values']['timestamp_billing_period_end'] = $subscription_input['timestamp_billing_period_end'] ; 
            }
        if (isset($subscription_input['coupon_id'])) {
            $query_array['values']['coupon_id'] = $subscription_input['coupon_id'] ; 
            }
        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        $this->stripe_query_result = $subscription_item_record ; 
        
        
        // Create History Entry
        $history_input = array(
            'account_id' => $this->account_id,
            'function' => 'update_billing_subscription',
            'notes' => json_encode($query_array['values'])
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;
        
        return $result ; 
        
        }
    
    
    }