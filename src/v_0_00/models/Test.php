<?php

//declare(strict_types=1);

namespace Ragnar\Ironsides ;

class Test extends Account 
{
    /**
     * Create a new Skeleton Instance
     */
    public function __construct()
    {
        // constructor body
    }

    /**
     * Friendly welcome
     *
     * @param string $phrase Phrase to return
     *
     * @return string Returns the phrase passed in
     */
    public function Echo_Test(string $phrase): string
    {
        return $phrase ;  
    }
    
    public function Extend_User_Test ($extend_user_id)
    {
        
        $extend = new User($extend_user_id) ; 
        $user_record = $extend->Get_User() ; 
        
        return $user_record ;  
    }
    
}
