<?php

namespace Ragnar\Ironsides ;

class Email extends Asset {
    
	private $logs = array();
    public $status ; // user or app : user would use the account's own mail service account; app uses the application account
//    public $email_domains_table ; // based on status parameter. Name of database table - 
    
//    public $vendor_id ; // Defines the email delivery service
//    public $vendor_api_key ; // API key for the email delivery service
    
    private $connected_accounts ; // Full array of all accounts that have an active connection to the email delivery service
    public $query_result_events ; 
    public $last_event_queried ; // This is set dynamically every time the event process loops and updates with the microtimestamp of the most recently processed item
    public $email_events ;    
    public $email_event_paging ;    
    
    // All available domains for the current account
    public $domains = array() ;
    
    // Currently active domain for the current account
    public $domain_id ;
    public $domain_last_event_queried ; // Microtimestamp of the last event retrieved from the delivery service
    public $domain_first_event_queried ; // Microtimestamp of the very first event that was queried when domain was created
    public $domain_initial_query ; // Microtimestamp of the last event queried on the initial query run (upon creation, 30 days - 7 days)
    public $sending_domain ;

    // Deliverables
    public $max_delivery_recipients = 500 ; // Max number of recipients that can be sent to in a single API call
    public $recipient_total_count ;
    public $recipient_sets ; 
    
    public $recipient_list ; // This is just for querying a single set of recipients in the Emails table. Used for getting results, not for deliveries
    
    
    public $email_sender ; // The profile that will be delivering the email
    public $email ;
    public $email_id ; // Tied to a specific recipient of an automation event
    public $email_content_id ; // The id of actual delivered content tied back to an asset automation_id. Created post delivery.
    public $email_action ; // The entire array of content and automation and delivery result 
    
    public $email_list ; 
    public $email_events_list ; 
    
    public $personalized_string ; // Used for one-off personalization submissions
    
    public $email_events_process_limit ; 
    
    // Results
    public $delivery_result ;    
    
    // EMAIL SERVICE CONNECTION
    public $_connection ;
    public $_connection_status ;
    public $email_api_event ; // API
    public $email_query_result ; // Database
    
    public $page_increment = 12 ; 
    
    
    // An account ID and user profile is required to send any email, 
    // so a good idea to set both in initalizing the class
    public function __construct($user_id = 'ignore',$account_id = 'ignore') {
        
        global $DB ;  
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            } 
        
        if ('ignore' !== $account_id) {
            $this->Set_Account_By_ID($account_id) ; 
            $this->Set_Sending_Domain($this->account['email_domain']) ; 
            }
	   }
    
  
    // Instantiate email delivery service
    // If you want to use the app's connection to the service (not a user account), set status = app
    // Assuming for now that Mailgun is our delivery service (vendor_id = 2). Can customize in the future.    
    public function Connect($status = 'app',$vendor_id = 2) {
        
        $this->vendor_id = $vendor_id ;
        
        if ('app' === $status) {

            $this->Set_System_Vendor($this->vendor_id) ; // Get system API keys
            $this->status = $status ; 
//            $this->email_domains_table = 'app_email_domains' ; 
            
            } else {
            
                // Gets an account's API key for the delivery service
                $this->Set_Account($account_id,false) ;  
            
                $vendor = new Vendor() ; 
                $vendor->Set_Account_By_ID($account_id)->Set_Vendor($this->vendor_id) ;  // Assuming Mailgun
                $vendor_key = $vendor->Get_Vendor_Key(6) ;  // Assuming Mailgun API Key
                $vendor_api_key = $vendor_key['key_value'] ; 
                
                $this->status = 'user' ; 
//                $this->email_domains_table = 'email_domains' ; 
            
                }     
        
        
        switch ($this->vendor_id) {
            case 2: // Mailgun
                
                try {
                    $this->_connection = new \Mailgun\Mailgun($this->vendor_keys->Mailgun_API_Key);
                    } catch ( Exception $e ) {
                        $error = $e->getMessage() ;  
                        $this->_connection_status = $error ; 
                        return $error ;  
                        } 
                    catch ( Error $e ) {
                        $error = $e->getMessage() ;  
                        $this->_connection_status = $error ; 
                        }                

                $this->Action_Set_Domains() ; // Return a list of all domains associated with this account from the service
                break ;                
            }                
        
        $this->Set_Model_Timings('Email.Connect - Connected to service') ;
        return $this ; 
        }    
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    

    
    public function Set_Email_Sender() {
        
        $continue = 1 ; 
        $sender_array = array() ; 
        
        if ($this->profile['email_verified'] != 1) {
            $continue = 0 ; 
            $sender_array = 'error'  ;
            // Set alert for email not verified
            }
        
        if (!isset($this->account['email_domain']['domain_display_name'])) {
            $this->Set_Account_By_ID($this->profile['account_id']) ; 
            $this->Set_Alert_Response(148) ; // Email has not been verified
            }
        
        // Process and create sender array
        if ($continue == 1) { 
            
            $format_options = array(
                'search_for' => '&',
                'replace_with' => 'and'
                ) ;
            
            $sender_array['sender_name'] = Utilities::Format_Convert_Spaces($this->profile['profile_display_name'],$format_options) ;            
            $sender_array['sender_email_address'] = $this->profile['profile_username'].'@'.$this->account['email_domain']['domain_display_name'] ; 
            $sender_array['from'] = $sender_array['sender_name'].'<'.$sender_array['sender_email_address'].'>' ; // This is the service prepped name/email combo. Email address won't display in print_r because it's wrapped in a < > 
            $sender_array['h:Reply-To'] = $this->profile['email_address'] ; 
            } 
        
        $this->email_sender = $sender_array ; 
        
        return $this ; 
        }
    
    
    
    public function Set_Email_ID($email_id) {
        
        $this->email_id = $email_id ; 
        
        return $this ; 
        }
    
    
    public function Set_Email_By_ID($email_id,$additional_parameters = array()) {
        
        $this->email_id = $email_id ;
        $this->Set_Email($this->email_id,$additional_parameters) ;        
        
        return $this ; 
        } 
    
    
    public function Set_Email($email_id = 'internal',$additional_parameters = array()) {
        
        if ('internal' === $email_id) {
            $email_id = $this->email_id ; 
            } else {
                $this->email_id = $email_id ;      
                } 
        
        $query_options = array() ; 
        
        $query_options['filter_by_email_id'] = $email_id ; 
        
        $result = $this->Retrieve_Email_List($query_options) ;
        
        
        if ($result['result_count'] == 0) {
            
            $this->email = 'error' ; 
            
            } else {
            
                $email_record = $this->Action_Time_Territorialize_Dataset($result['results'][0]) ;
                $this->email = $email_record ; 
            
                }
            
            
        return $this ; 
        }
    
    public function Get_Email_List() {
        
        return $this->email_list ; 
        }
    
    public function Get_Email_Events_List() {
    
        return $this->email_events_list ;
        }
    
    public function Set_Email_List($query_options = array()) {
        
//        $query_options['filter_by_email_id'] = $email_id ; 
        
        $query_options['skip_email_content'] = 'yes' ; 
        
        $result = $this->Retrieve_Email_List($query_options) ;
        
        
        if ($result['result_count'] == 0) {
            
            $this->email_list = 'error' ; 
            
            } else {
            
//                $email_record = $this->Action_Time_Territorialize_Dataset($result['results'][0]) ;
                $this->email_list = $result['results'] ; 
            
                }
            
            
        return $this ; 
        }
    
    public function Set_Email_Events_list($query_options = array()) {
        
        //        $query_options['filter_by_email_id'] = $email_id ; 
        // $query_options['skip_email_content'] = 'yes' ; 
        
        $result = $this->Retrieve_Email_Events_List($query_options) ;
        
        
        if ($result['result_count'] == 0) {
            
            $this->email_events_list = 'error' ; 
            
            } else {
            
//                $email_record = $this->Action_Time_Territorialize_Dataset($result['results'][0]) ;
                $this->email_events_list = $result['results'] ; 
//                $this->email_events_list = $result['query'] ; 
                }
            
            
        return $this ; 
        }
    
    
    public function Retrieve_Email_List($query_options) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        // Base query:
        $query_array = array(
            'table' => 'emails',
            'join_tables' => array(),
            'fields' => "
                emails.*,
                asset_campaigns.campaign_id, asset_campaigns.account_id, asset_campaigns.user_id, 
                user_profiles.profile_id, user_profiles.first_name AS profile_first_name, user_profiles.last_name AS profile_last_name, 
                    user_profiles.profile_display_name, user_profiles.email_address_id,
                user_emails.email_address AS profile_email_address
                ",
            'group_by' => "emails.email_id"
            );
        

        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ; 
            
            if ($query_array['order_by'] == 'relevance') {
                $query_array['order_by'] = 'asset_core.asset_title' ; 
                $query_array['order_by_relevance'] = 'yes' ; 
                }  
            
            if (isset($query_options->order_field)) {
                $query_array['order_field'] = $query_options->order_field ;
                $query_array['order_start'] = $query_options->order_start ;
                $query_array['order_operator'] = $query_options->order_operator ;
                }           
            } 
        
        
        // PROCESS EMAIL IDs
        if (isset($query_options->filter_by_email_id)) { 
            
            $query_options->filter_by_email_id = Utilities::Array_Force($query_options->filter_by_email_id) ; 
            
            $filter_by_email_id_string = '' ; 
            foreach ($query_options->filter_by_email_id as $query_email_id) {
                $filter_by_email_id_string .= "emails.email_id='$query_email_id' OR " ; 
                }
            $filter_by_email_id_string = rtrim($filter_by_email_id_string," OR ") ;
            $query_array['where'] .=  " AND ($filter_by_email_id_string) " ;                  
            }
        
        
        // PROCESS AUTOMATION IDs
        if (isset($query_options->filter_by_automation_id)) { 
            
            $query_options->filter_by_automation_id = Utilities::Array_Force($query_options->filter_by_automation_id) ; 
            
            $filter_by_automation_id_string = '' ; 
            foreach ($query_options->filter_by_automation_id as $query_automation_id) {
                $filter_by_automation_id_string .= "emails.automation_id='$query_automation_id' OR " ; 
                }
            $filter_by_automation_id_string = rtrim($filter_by_automation_id_string," OR ") ;
            $query_array['where'] .=  " AND ($filter_by_automation_id_string) " ;                  
            }
        
        
        
        // Add in additional join tables...
        if ($query_options->skip_email_content != 'yes') {
            
            $query_array['fields'] .= ", email_content.email_subject, email_content.email_html, email_content.email_text" ; 
            
            $query_array['join_tables'][] = array(
                'table' => 'email_content',
                'on' => 'email_content.email_content_id',
                'match' => 'emails.email_content_id',
                'type' => 'left'
                );            
            }
     
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => 'asset_campaign_automation.automation_id',
            'match' => 'emails.automation_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaigns',
            'on' => 'asset_campaigns.campaign_id',
            'match' => 'asset_campaign_automation.campaign_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_profiles',
            'statement' => '(user_profiles.user_id=asset_campaigns.user_id AND user_profiles.account_id=asset_campaigns.account_id)'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_emails',
            'on' => 'user_emails.email_address_id',
            'match' => 'user_profiles.email_address_id'
            );
        
        
        // Delivery Asset Subquery
        $join_table_name = 'delivery_asset' ;  
        $query_array['fields'] .= ", 
            $join_table_name.asset_id AS delivery_asset_id, 
            $join_table_name.asset_title AS delivery_asset_title, 
            $join_table_name.type_id AS delivery_asset_type_id, 
            $join_table_name.type_name AS delivery_asset_type_name" ; 
            
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT assets.asset_id, assets.asset_title, asset_type.*, asset_status.*, system_visibility_levels.* 
                FROM assets 
                JOIN asset_type ON assets.type_id = asset_type.type_id 
                JOIN asset_status ON asset_status.asset_status_id = asset_status.asset_status_id 
                JOIN system_visibility_levels ON assets.visibility_id = system_visibility_levels.visibility_id
                ) AS $join_table_name",
            'statement' => "
                    (asset_campaigns.delivery_asset_id=$join_table_name.asset_id)"
            );        
        
        
        
        if (isset($query_array['where'])) {
            $query_array['where'] = ltrim($query_array['where']," AND ") ;            
            }

        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->email_query_result = $result ;   
               

        $query_options->value_name = 'email_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Email_Paging($result) ;         
        $this->asset_query_result = $result ; 
                

        return $result ; 
        }
    
    
    
    public function Retrieve_Email_Events_List($query_options) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        // Base query:
        $query_array = array(
            'table' => 'email_events',
            'join_tables' => array(),
            'fields' => "
                email_events.*,
                emails.*,
                asset_campaigns.campaign_id, asset_campaigns.account_id, asset_campaigns.user_id
                ",
            'group_by' => "email_events.event_id"
            );
        

        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ; 
            
            if ($query_array['order_by'] == 'relevance') {
                $query_array['order_by'] = 'asset_core.asset_title' ; 
                $query_array['order_by_relevance'] = 'yes' ; 
                }  
            
            if (isset($query_options->order_field)) {
                $query_array['order_field'] = $query_options->order_field ;
                $query_array['order_start'] = $query_options->order_start ;
                $query_array['order_operator'] = $query_options->order_operator ;
                }           
            } 
        
        
        // PROCESS EMAIL IDs
        if (isset($query_options->filter_by_email_id)) { 
            
            $query_options->filter_by_email_id = Utilities::Array_Force($query_options->filter_by_email_id) ; 
            
            $filter_by_email_id_string = '' ; 
            foreach ($query_options->filter_by_email_id as $query_email_id) {
                $filter_by_email_id_string .= "emails.email_id='$query_email_id' OR " ; 
                }
            $filter_by_email_id_string = rtrim($filter_by_email_id_string," OR ") ;
            $query_array['where'] .=  " AND ($filter_by_email_id_string) " ;                  
            }
        
        
        // PROCESS AUTOMATION IDs
        if (isset($query_options->filter_by_automation_id)) { 
            
            $query_options->filter_by_automation_id = Utilities::Array_Force($query_options->filter_by_automation_id) ; 
            
            $filter_by_automation_id_string = '' ; 
            foreach ($query_options->filter_by_automation_id as $query_automation_id) {
                $filter_by_automation_id_string .= "emails.automation_id='$query_automation_id' OR " ; 
                }
            $filter_by_automation_id_string = rtrim($filter_by_automation_id_string," OR ") ;
            $query_array['where'] .=  " AND ($filter_by_automation_id_string) " ;                  
            }
        
        
        
        // Add in additional join tables...
        $query_array['join_tables'][] = array(
            'table' => 'emails',
            'on' => 'emails.email_id',
            'match' => 'email_events.email_id'
            );  
                
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => 'asset_campaign_automation.automation_id',
            'match' => 'emails.automation_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaigns',
            'on' => 'asset_campaigns.campaign_id',
            'match' => 'asset_campaign_automation.campaign_id'
            );
        
        
        
        if (isset($query_array['where'])) {
            $query_array['where'] = ltrim($query_array['where']," AND ") ;            
            }

        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->email_query_result = $result ;   
               

        $query_options->value_name = 'event_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Email_Paging($result) ;         
        $this->asset_query_result = $result ; 
                

        return $result ; 
        }
    
    
    
    
    public function Get_Page_Increment() {
        
        return $this->page_increment ; 
        } 
    
    // Process a set of email results and separate into paging components to use for site navigation
    public function Set_Email_Paging($results_array) {

        if (!isset($this->email_paging)) {
            $this->email_paging = $this->Set_Default_Paging_Object() ; 
            }       
        
        $this->email_paging->page_increment = $this->Get_Page_Increment() ;
        
        $this->email_paging = $this->Action_Process_Paging($this->email_paging,$results_array) ; 
        
        return $this ; 
        }
    
    
    // Set a sending domain by search filters...
    public function Set_Sending_Domain_By_Query($query_options = array()) {
        
        $result = $this->Retrieve_Domain_List($query_options) ; 

        if ((!$result['error']) AND ($result['result_count'] > 0)) {
            $this->domain_id = $result['results'][0]['domain_id'] ; 
            $this->domain_last_event_queried = $result['results'][0]['microtimestamp_last_event_queried'] ;
            $this->domain_first_event_queried = $result['results'][0]['microtimestamp_first_event_queried'] ;
            $this->domain_initial_query = $result['results'][0]['microtimestamp_initial_query'] ;
            
            $result['results'][0] = $this->Action_Time_Territorialize_Dataset($result['results'][0]) ;
            $this->sending_domain = $result['results'][0] ;
            } else {
                $this->sending_domain = 'error' ; 
                }
        
        return $this ; 
        }
    
    
    // Set a sending domain by the domain ID (integer) as listed in the database
    public function Set_Sending_Domain_By_ID($domain_id) {
        
        $query_options = array(
            'filter_by_domain_id' => $domain_id
            ) ; 
        
        $this->Set_Sending_Domain_By_Query($query_options) ; 
        
        
//        if (!$result['error']) {
//            $this->domain_id = $result['results'][0]['domain_id'] ; 
//            $this->domain_last_event_queried = $result['results'][0]['microtimestamp_last_event_queried'] ;
//            $this->domain_first_event_queried = $result['results'][0]['microtimestamp_first_event_queried'] ;
//            $this->domain_initial_query = $result['results'][0]['microtimestamp_initial_query'] ;
//            
//            $result['results'][0] = $this->Action_Time_Territorialize_Dataset($result['results'][0]) ;
//            $this->sending_domain = $result['results'][0] ;
//            }
        
        $this->Set_Model_Timings('Email.Set_Sending_Domain_By_ID - Set domain '.$domain_id) ;
        return $this ; 
        }
    
    
    
    // Set a sending domain by the domain name (e.g. 'email.website.com')
    public function Set_Sending_Domain($sending_domain) {
        $this->sending_domain = $sending_domain ;
        return $this ; 
        }
    
    

    
    // UNIVERSAL METHOD
    // Sets an array of all accounts who have a connected status to the delivery service
    public function Set_Connected_Accounts($input_array = null) {
        
        
        
        if (null === $input_array) {
            $input_array = array() ; 
            } else {
                
                }
        
        $query_array = array(
            'table' => 'vendor_auth',
            'fields' => "vendor_auth.*",
            'where' => "vendor_auth.vendor_id='$this->vendor_id' AND vendor_auth.connection_status='connected'"
            );
        
        
        $result = $this->DB->Query('SELECT',$query_array);

        $this->connected_accounts = $result['results'] ; 
        return $this ; 
        } 
    
    
    
    public function Set_Email_API_Event($event) {
        
        $this->email_api_event = $event ; 
        $this->Set_Email_Event_Paging($event) ; 
        return $this ; 
        }
    
    public function Set_Email_Event_Paging($event_input) {
        
        $this->email_event_paging = $event_input->http_response_body->paging ; 
        return $this ; 
        }     
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    // Get connection status of vendor
    public function Get_Connection_Status() {
        
        $connection_status = array(
            'status' =>  $this->status,
            'connection_status' =>  $this->_connection_status,
            'connection' =>  $this->_connection            
            ) ; 
                
        return $connection_status ; 
        } 
    
    
    public function Get_Email_Paging() {
        return $this->email_paging ; 
        } 
    
    
    // UNIVERSAL METHOD
    // Get and return the global list of connected accounts 
    public function Get_Connected_Accounts() {
        return $this->connected_accounts ; 
        } 
    
    // Get the result of the email being queued to the service provider
    public function Get_Delivery_Result() {
        return $this->delivery_result ; 
        }    
    
    // Get and return the array of domains for the currently active account
    public function Get_Domains() {
        return $this->domains ; 
        }     
    
    public function Get_Recipient_Sets() {
        
        return $this->recipient_sets ; 
        }   
    
    
    public function Get_Email_Action() {
        
        return $this->email_action ; 
        }
    
    public function Get_Sending_Domain() {
        return $this->sending_domain ; 
        }
    
    public function Get_Email_Sender() {
        
        return $this->email_sender ; 
        }
    
    public function Get_Receipient_Sets() {
        
        return $this->recipient_sets ; 
        }    
    
    public function Get_Personalized_String() {
        
        return $this->personalized_string ; 
        }
    
    public function Get_Email_Event_Paging($paging = 'all') {
        
        $paging_array = $this->email_event_paging ; 
        
        switch ($paging) {
            
            case 'first':
                $url = $paging_array->first ; 
                break ;
            case 'previous':
                $url = $paging_array->previous ; 
                break ;
            case 'last':
                $url = $paging_array->last ; 
                break ;
            case 'next':
                $url = $paging_array->next ; 
                break ;
            case 'all':                
            default:
                $url = $paging_array ;                
            }
        
        return $url ;        
        }     

    public function Get_Email() {
        
        return $this->email ;        
        }
    
    public function Get_Email_Events() {
        
        return $this->email_events ;        
        }
    
    public function Get_Last_Event_Queried() {
        
        return $this->last_event_queried ;
        }

    // Retrieve the most recent API event response from the service
    public function Get_Email_API_Event() {
        
        return $this->email_api_event ; 
        } 
    
    public function Get_Email_Recipient_Total_Count() {
        
        return $this->recipient_total_count ; 
        }
    
    public function Get_Email_Recipients() {
        
        return $this->recipient_list ; 
        }
    
    
    //////////////////////
    //                  //
    // API ACTIONS      //
    //                  //
    //////////////////////  
    
    
    // Remove a suppression from vendor
    public function Action_Delete_Suppression($parameters = array()) {
        
        $account_record = $this->Get_Account() ; 

        $bounce_url = $account_record['email_domain']['domain_name'].'/bounces/'.$parameters['email_address'] ; 
        $result = $this->Action_Execute_Email_Query('delete',$bounce_url) ; 
        
        if ((is_array($result)) AND ($result['error'])) {
            
            $unsubscribe_url = $account_record['email_domain']['domain_name'].'/unsubscribes/'.$parameters['email_address'] ; 
            $result = $this->Action_Execute_Email_Query('delete',$unsubscribe_url) ;            
            }
        
        if ((is_array($result)) AND ($result['error'])) {
            
            $complaints_url = $account_record['email_domain']['domain_name'].'/complaints/'.$parameters['email_address'] ; 
            $result = $this->Action_Execute_Email_Query('delete',$complaints_url) ;            
            }
        
        return $result ; 
        }
    
    
    public function Action_Execute_Email_Query($resource,$action,$parameters = array()) {
        
        // Try the Email function... 
        try {
            if (isset($parameters['query_url'])) {  
                $result = $this->_connection->$resource($parameters['query_url']);                 
                } else {
                    $result = $this->_connection->$resource($action, 
                        $parameters
                        );                
                    }
            } catch ( Exception $e ) {
                $result['error'] = $e->getMessage() ;  
                }
            catch ( Error $e ) {
                $result['error'] = $e->getMessage() ;  
                }
        
        $this->Set_Email_API_Event($result) ;         
        return $result ;
        }
    
    
    // API Get email domains via API call to delivery service
    // ++ Add a loop here to get more than 100 domains
    public function API_Set_Domains($input_array) {
        
        // Process and sanitize the input array into a query array that can ping the delivery service
        if (isset($input_array['limit'])) {
            $query_array['limit'] = $input_array['limit'] ; // Max number of results per call
            }
        if (isset($input_array['skip'])) {
            $query_array['skip'] = $input_array['skip'] ; // Provide an offset to loop beyond the limit parameter
            } 

        $result = $this->Action_Execute_Email_Query('get','domains',$query_array) ; 
        
        return $result ; 
        }
    
    
    // API ACTION: Queue an email to the delivery service to be sent to recipient(s)
    public function API_Action_Queue_Email(
        $recipient_to_array = array(), $recipient_cc_array = array(), $recipient_bcc_array = array(), 
        $recipient_variables = array(),
        $email_subject = 'internal', $email = 'internal', $send_time = 'now', 
        $properties_array = array(),
        $options_array = array()
        ) {
    
        
        if (!isset($properties_array['test_mode'])) {
            $properties_array['test_mode'] = 'no' ; 
            }
        if (!isset($properties_array['tracking'])) {
            $properties_array['tracking'] = 'yes' ; 
            }        
        if (!isset($properties_array['tags'])) {
            $properties_array['tags'] = 'none' ; 
            } 
        if (!isset($properties_array['custom_data'])) {
            $properties_array['custom_data'] = 'none' ; 
            }        
        
        
        $continue = 1 ; // Initialize
        $delivery_array = array() ; // This initializes the array that we will send to the delivery service. It will be filled below.
        $delivery_result = array() ; // What we will return 
        

        // Get email_sender array
        $sender_result = $this->Get_Email_Sender() ; 
        
        if (!isset($sender_result['sender_name'])) {
            $continue = 0  ; 
            } 
        
        
        // Check to see if we have more than 0 recipients submitted
        if ($continue == 1) {
            
            if ((count($recipient_to_array) > 0) OR (count($recipient_cc_array) > 0) OR (count($recipient_bcc_array) > 0)) {
                // We have recipients. Continue script
                } else {
                    $continue = 0  ; 
                    $this->Set_Alert_Response(135) ; // Create alert for no recipients 
                    }
            }
        
        
        // Create delivery array for recipients
        if ($continue == 1) {
        
            // Grab the previous set email_sender variables
            $delivery_array['from'] = $this->email_sender['from'] ; 
            $delivery_array['h:Reply-To'] = $this->email_sender['h:Reply-To'] ; 
            
            
            // Process recipients
            // Provide as...
            //      array(
            //          'name' => 'John Smith',
            //          'email_address' => 'john.smith@email.com'
            //          )
            if (count($recipient_to_array) > 0) {

                $temp_array = array() ; 
                $recipient_to_string = '' ; 

                foreach($recipient_to_array as $recipient) {
                    // str_replace removes commas from recipient name
                    $temp_array[] = preg_replace('/[^\w-\s]/', '', $recipient['full_name']).' <'.$recipient['email_address'].'>' ; 
                    }
 
                $delivery_array['to'] = implode(",",$temp_array) ; 
                } 

            if (count($recipient_cc_array) > 0) {

                $temp_array = array() ; 
                $recipient_cc_string = '' ; 

                foreach($recipient_cc_array as $recipient) {
                    
//                    $account_username = preg_replace('/[^\w-]/', '', $recipient['full_name']); // Allow alphanumeric, dash, and underscore
                    
                    $temp_array[] = preg_replace('/[^\w-\s]/', '', $recipient['full_name']).' <'.$recipient['email_address'].'>' ; 
                    }

                $delivery_array['cc'] = implode(",",$temp_array) ;
                }   

            if (count($recipient_bcc_array) > 0) {

                $temp_array = array() ; 
                $recipient_bcc_string = '' ; 

                foreach($recipient_bcc_array as $recipient) {
                    $temp_array[] = preg_replace('/[^\w-\s]/', '', $recipient['full_name']).' <'.$recipient['email_address'].'>' ; 
                    }

                $delivery_array['bcc'] = implode(",",$temp_array) ; 
                } 


            // Process recipient variables (associated to recipient at the delivery provider)
            if (count($recipient_variables) > 0) { 
                $delivery_array['recipient-variables'] = json_encode($recipient_variables);
                } 


            // Process send time
            if ('now' === $send_time) {
                $delivery_array['o:deliverytime'] = TIMESTAMP ;
                } else {
                    $delivery_array['o:deliverytime'] = $send_time ; 
                    }

            // Process additional properties array
            $properties = array() ; 
            if ('no' === $properties_array['test_mode']) {
                $delivery_array['o:testmode'] = 'no' ;
                } else {
                    $delivery_array['o:testmode'] = 'yes' ;
                    }
            if ('yes' === $properties_array['tracking']) {
                $delivery_array['o:tracking'] = 'yes' ; // yes or no
                $delivery_array['o:tracking-clicks'] = 'yes' ; // yes or no
                $delivery_array['o:tracking-opens'] = 'yes' ; // yes or no
                } else {
                    $delivery_array['o:tracking'] = 'no' ; // yes or no
                    $delivery_array['o:tracking-clicks'] = 'no' ; // yes or no
                    $delivery_array['o:tracking-opens'] = 'no' ; // yes or no
                    } 
            if ('none' === $properties_array['tags']) {
                // Skip adding tags to delivery array
                } else { $delivery_array['o:tag'] = $properties_array['tags']; }         
            if ('none' === $properties_array['custom_data']) {
                // Skip adding custom data to delivery rray
                } else { $delivery_array['v:my-custom-data'] = json_encode($properties_array['custom_data']); }        



            // Process message
            if ('internal' === $email_subject) {
                $delivery_array['subject'] = $this->email['subject_line_01'] ;
                } else {
                    $delivery_array['subject'] = $email_subject ;
                    }

            if ('internal' === $email) {
                $delivery_array['html'] = $this->email['email_html'] ;
                $delivery_array['text'] = $this->email['email_text'] ;
                $email_status = $this->email['status'] ; 
                $attachments = array() ; 
                } else {
                    $email_status = $email['status'] ; 
                    $delivery_array['html'] = $email['email_html'] ;
                    if ($email['documents']['txt']) { // WHY WOULD THIS EXIST?
                        $delivery_array['text'] = $email['documents']['txt'] ; 
                        } else {
                            $delivery_array['text'] = $email['email_text'] ;    
                            }

                    if ($email['attachments']) {
                        $attachments = array(
                            'attachment' => $email['attachments']
                            ) ; 
                        } else {
                            $attachments = array() ; 
                            }            
                    } 
        
            }
        
        
        if ((!$this->sending_domain) AND ($continue == 1)) {
            $continue = 0 ; 
            $this->Set_Alert_Response(134) ; // No sending domain
            }
        
        
        if (($options_array['halt_delivery'] == 'yes') AND ($continue == 1)) {
            $continue = 0 ; 
            $this->Set_Alert_Response(143) ; // Email manually halted
            }

        
        
        if ($continue == 1) {               
                
                error_log('Email.API_Action_Queue_Email() to parameters submited to mailgun: '.json_encode($delivery_array['to'])) ;
            
            
                try {
                    # Make the call to the MailGun client.
                    $email_result = $this->_connection->sendMessage($this->sending_domain['domain_name'], $delivery_array, $attachments);

                    if ($email_result) {

                        $delivery_result['response_code'] = $email_result->http_response_code ; // https://documentation.mailgun.com/api-intro.html#errors
                        
                        
                        switch ($delivery_result['response_code']){
                            case 200:
                                // 200 	Everything worked as expected
                                $this->Set_Alert_Response(136) ; // Email successfully queued
                                
                                $delivery_result['asset_status_id'] = 6 ; // Queued
                                
                                $delivery_result['email_sid'] = $email_result->http_response_body->id ; 
                                $delivery_result['response_message'] = $email_result->http_response_body->message ;   
                                
                                $delivery_array['html_base64'] = base64_encode($delivery_array['html']) ; 
                                $delivery_array['text_base64'] = base64_encode($delivery_array['text']) ;                                
                                
                                break;
                            case 400:  // 400 	Bad Request - Often missing a required parameter
                                
                                $this->Set_Alert_Response(137) ; // Email error
                                
                                $delivery_result['asset_status_id'] = 1 ; // Error
                                $delivery_result['response_message'] = $email_result->http_response_body->message.' https://documentation.mailgun.com/api-intro.html#errors' ; // 
                                
                                break;   
                            case 401:  // 401 	Unauthorized - No valid API key provided
                                
                                $this->Set_Alert_Response(138) ; // Email error
                                
                                $delivery_result['asset_status_id'] = 1 ; // Error
                                $delivery_result['response_message'] = $email_result->http_response_body->message.' https://documentation.mailgun.com/api-intro.html#errors' ; //                                

                                break; 
                            case 402:  // 402 Request Failed - Parameters were valid but request failed
                                
                                $this->Set_Alert_Response(139) ; // Email error
                                
                                $delivery_result['asset_status_id'] = 1 ; // Error
                                $delivery_result['response_message'] = $email_result->http_response_body->message.' https://documentation.mailgun.com/api-intro.html#errors' ; //                               
                                
                                break; 
                            case 404:  // 404 Not Found - The requested item doesn’t exist
                                
                                $this->Set_Alert_Response(140) ; // Email error
                                
                                $delivery_result['asset_status_id'] = 1 ; // Error
                                $delivery_result['response_message'] = $email_result->http_response_body->message.' https://documentation.mailgun.com/api-intro.html#errors' ; //                              
                                
                                break;  
                            case 500 :
                            case 502 :
                            case 503 :
                            case 504 :
                                // 500 Server Errors - something is wrong on Mailgun’s end
                                // 502 Server Errors - something is wrong on Mailgun’s end
                                // 503 Server Errors - something is wrong on Mailgun’s end
                                // 504 Server Errors - something is wrong on Mailgun’s end
                                
                                $this->Set_Alert_Response(141) ; // Email error
                                
                                $delivery_result['asset_status_id'] = 1 ; // Error
                                $delivery_result['response_message'] = $email_result->http_response_body->message.' https://documentation.mailgun.com/api-intro.html#errors' ; // 

                                break;                        
                            }
                        }
                    }

                    // Catch Exception 
                    catch(Exception $e) {
                        
                        $this->Set_Alert_Response(142) ; // 999 Internal app error
                        
                        $delivery_result['asset_status_id'] = 1 ; // Error
                        $delivery_result['response_message'] = $e->getMessage() ; // 

                        $email_result = $delivery_result ; 
                        
                        error_log('Email.API_Action_Queue_Email() 999 error: '.json_encode($delivery_result['response_message'])) ;
                                    
                        }

            
            
            $this->Append_Alert_Response('none',array(
                'admin_context' => json_encode($delivery_result),
                'location' => __METHOD__
                )) ;

            $this->Set_Email_API_Event($email_result) ;
            } 
        
        
        $result = $this->Get_Response() ; 
        
        $delivery_result['email_status'] = $result ; 
        $delivery_result['sending_domain'] = $this->sending_domain ; 
        $delivery_result['email_sender'] = $this->Get_Email_Sender() ;
        $delivery_result['delivery_array'] = $delivery_array ; 	
        $delivery_result['response_full'] = $email_result ; 
        
        $this->delivery_result = $delivery_result ; 
        
        return $this ; 
        
        }    
    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    

    
    
    // Standardize all Mailgun Email SIDs to use angle brackets
    // For some reason, sometimes they come through with angles and other times they don't
    // We will ALWAYS use angle brackets in our database.
    // Mailgun will sometimes requre queries to be run w/out angle brackets
    public function Action_Standardize_Email_SID($incoming_email_sid) {
        
        // First strip Mailgun Send ID with angle brackets stripped
        $email_sid = rtrim($incoming_email_sid, ">");
        $email_sid = ltrim($email_sid, "<"); 

        $result = array(
            'email_sid' => '<'.$email_sid.'>',
            'email_sid_nobrackets' => $email_sid
            ) ; 
        
        return $result ; 
        }
    
    
    
    public function Retrieve_Email_Recipients($query_options = array()) {
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $distribution_sublist_email = $query_options->delivery_asset['distribution_sublist_email'] ; // Email sublist asset_id
        

        $query_array = array(
            'table' => "emails",
            'fields' => "emails.*, asset_campaign_automation.*",
            'where' => "emails.email_id>0 "
            );

        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => 'asset_campaign_automation.automation_id',
            'match' => 'emails.automation_id'
            ); 
 
        
        if (!isset($query_options->override_paging)) {
            $query_options->override_paging = 'yes' ;
            }
        
        
        if ($query_options->filter_by_automation_id == 'yes') {
            $query_array['where'] .= " AND emails.automation_id='$this->automation_id'" ; 
            }
                
        // Should be set as an array of status IDs
        if (isset($query_options->filter_by_email_status_id)) {
            $email_status_id_query_string = ' AND (' ; 
            foreach ($query_options->filter_by_email_status_id as $status_id) {
                $email_status_id_query_string .= "emails.email_status_id='$status_id' OR " ; 
                }
            $email_status_id_query_string = rtrim($email_status_id_query_string," OR ") ; 
            $email_status_id_query_string .= ')' ; 
            $query_array['where'] .= $email_status_id_query_string ; 
            }
        

        if (isset($query_options->delivered)) {
            $query_array['where'] .= " AND emails.delivered='$query_options->delivered'" ; 
            }
        if (isset($query_options->opened)) {
            $query_array['where'] .= " AND emails.opened='$query_options->opened'" ; 
            }
        if (isset($query_options->clicked)) {
            $query_array['where'] .= " AND emails.clicked='$query_options->clicked'" ; 
            }        
        
        
        switch ($query_options->delivery_asset['type_id']) {
            case 19: // distribution list - contacts
            case 26: // series distribution list - contacts  
                
                // Add filter by search parameter
                if (isset($query_options->filter_by_search_parameter)) {

                    $search_parameter_token = explode(" ",$query_options->filter_by_search_parameter) ; 

                    $t = 0 ; 
                    foreach ($search_parameter_token as $search_token) {
                        if ($search_token == '') {
                            unset($search_parameter_token[$t]) ; 
                            }
                        $t++ ; 
                        }
                    array_values($search_parameter_token) ; 

                    foreach ($search_parameter_token as $search_token) {
                        $phone_number = Utilities::Validate_Phone_Number($search_token) ;
                        if ($phone_number['valid'] == 'yes') {
                            $search_parameter_token[] = $phone_number['phone_number'] ;    
                            }
                        }

                    $compiled_search_string = '' ; 

                    foreach ($search_parameter_token as $search_token) {

                        $this_search_string = " (contacts.first_name LIKE '%$search_token%' OR 
                            contacts.last_name LIKE '%$search_token%' OR 
                            CONCAT(contacts.first_name,' ',contacts.last_name) LIKE '%$search_token%' OR 
                            contacts.email_address LIKE '%$search_token%' OR 
                            contacts.phone_number LIKE '%$search_token%' OR 
                            contact_details.yl_member_number LIKE '%$search_token%' " ;                            

                        $this_search_string .= ')' ; 
                        $compiled_search_string .= $this_search_string.' OR ' ;                             
                        }

                    $compiled_search_string = rtrim($compiled_search_string," OR ") ;  

                    $query_array['where'] .= " AND ($compiled_search_string) " ;                                    
                    }
                
                
                $query_array['fields'] .= ", contacts.*, 
                        contact_details.yl_member_number, 
                        contact_restrictions_email.do_not_contact AS do_not_contact_email_address, 
                        contact_restrictions_phone.do_not_contact AS do_not_contact_phone_number, 
                        CONCAT(contacts.first_name,' ',contacts.last_name) AS full_name, 
                        asset_members.timestamp_member_created, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code " ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'contacts',
                    'on' => 'contacts.contact_id',
                    'match' => 'emails.member_id_value'
                    ) ; 
                         
                $query_array['join_tables'][] = array(
                    'table' => 'contact_details',
                    'on' => 'contact_details.contact_id',
                    'match' => 'contacts.contact_id',
                    'type' => 'left'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'asset_members',
                    'on' => 'asset_members.member_id',
                    'match' => 'emails.member_id',
                    'type' => 'left'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'system_visibility_levels',
                    'on' => 'system_visibility_levels.visibility_id',
                    'match' => 'contacts.visibility_id'
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'contact_restrictions AS contact_restrictions_email',
                    'statement' => '(contact_restrictions_email.email_address=contacts.email_address AND contact_restrictions_email.account_id=contacts.account_id AND contact_restrictions_email.contact_id=contacts.contact_id)',
                    'type' => 'left'
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'contact_restrictions AS contact_restrictions_phone',
                    'statement' => '(contact_restrictions_phone.phone_number_international=CONCAT(contacts.phone_country_dialing_code,contacts.phone_number) AND contact_restrictions_phone.account_id=contacts.account_id AND contact_restrictions_phone.contact_id=contacts.contact_id)',
                    'type' => 'left'
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'list_countries',
                    'on' => 'list_countries.country_id',
                    'match' => 'contacts.country_id',
                    'type' => 'left'
                    );                
                break ;
            case 20: // distribution list - users
            case 28: // User SERIES list
                
                $query_array['fields'] .= ", users.*, 
                        CONCAT(users.first_name,' ',users.last_name) AS full_name, 
                        user_emails.email_address, 
                        user_restrictions_email.do_not_contact AS do_not_contact_email_address, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code, 
                        asset_members.timestamp_member_created, 
                        user_registration.registration_token " ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'users',
                    'on' => 'users.user_id',
                    'match' => 'emails.member_id_value'
                    ) ; 

                $query_array['join_tables'][] = array(
                    'table' => 'asset_members',
                    'on' => 'asset_members.member_id',
                    'match' => 'emails.member_id',
                    'type' => 'left'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_emails',
                    'on' => 'users.email_address_id',
                    'match' => 'user_emails.email_address_id'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_registration',
                    'on' => 'user_registration.user_id',
                    'match' => 'users.user_id',
                    'type' => 'left'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_restrictions AS user_restrictions_email',
                    'statement' => '(user_restrictions_email.email_address=user_emails.email_address AND user_restrictions_email.user_id=users.user_id)',
                    'type' => 'left'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'list_countries',
                    'on' => 'list_countries.country_id',
                    'match' => 'users.country_id',
                    'type' => 'left'
                    );                
                break ;
            case 23: // distribution list - account user profiles
            case 27: // SERIES distribution list - account user profiles    
                $query_array['fields'] .= ", users.user_id, 
                        user_profiles.*, 
                        accounts.account_display_name, accounts.account_username, 
                        FORMAT(accounts.marketing_credit_balance,0) AS marketing_credit_balance, 
                        CONCAT(user_profiles.first_name,' ',user_profiles.last_name) AS full_name, 
                        user_profile_restrictions_email.do_not_contact AS do_not_contact_email_address, 
                        user_emails.email_address, 
                        asset_members.timestamp_member_created, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code " ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_profiles',
                    'on' => 'user_profiles.profile_id',
                    'match' => 'emails.member_id_value'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'asset_members',
                    'on' => 'asset_members.member_id',
                    'match' => 'emails.member_id',
                    'type' => 'left'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'accounts',
                    'on' => 'accounts.account_id',
                    'match' => 'user_profiles.account_id'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_emails',
                    'on' => 'user_profiles.email_address_id',
                    'match' => 'user_emails.email_address_id'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_profile_restrictions AS user_profile_restrictions_email',
                    'statement' => '(user_profile_restrictions_email.email_address=user_emails.email_address AND user_profile_restrictions_email.account_id=user_profiles.account_id AND user_profile_restrictions_email.profile_id=user_profiles.profile_id)',
                    'type' => 'left'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'users',
                    'on' => 'users.user_id',
                    'match' => 'user_profiles.user_id'
                    ) ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'list_countries',
                    'on' => 'list_countries.country_id',
                    'match' => 'users.country_id',
                    'type' => 'left'
                    );                
                break ;                
            }

//        $prequery_array = $query_array ; 
//        $prequery_array['skip_query'] = 1 ; 
//        
//        $preresult = $this->DB->Query('SELECT_JOIN',$prequery_array,'force') ;
//        print_r($preresult) ; 
        
        
        // Add sort filter
        if (isset($query_options->order_by)) { 
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ;           
            }
        
        
//        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force') ;
//        $this->email_query_result = $result ;        
        
        
        
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $query_options->value_name = 'email_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        
        $this->Set_Email_Paging($result) ;         
        $this->email_query_result = $result ; 
        
        return $result ; 
        }
    
    

    
    // THIS IS PART OF DELIVERY PROCESS
    // This is to collect recipients for a campaign that needs to be delivered
    public function Action_Set_Recipient_Sets($options_array = array()) { 
        
        $continue = 1 ; 
        
        $data['automation_record'] = $this->Get_Automation() ;
        $data['campaign_record'] = $this->Get_Campaign() ;
        
        switch ($data['campaign_record']['delivery_trigger']) { 
            case 'static':
                
                $recipient_query_options = array(
                    'filter_by_automation_id' => 'yes',
                    'filter_by_email_status_id' => array(4) // Scheduled status
                    ) ; 
                
                $data['recipient_result'] = $this->Retrieve_Email_Recipients($recipient_query_options) ; 
                $data['recipients'] = $data['recipient_result']['results'] ; 
 

                $i = 0 ; 
                foreach ($data['recipients'] as $recipient) { 
                    
                    // Used to validate actions based on member id such as unsubscribe from list
                    $member_token_array = array($recipient['member_id'],$recipient['timestamp_member_created']) ; 
                    $recipient['member_token'] = $this->DB->Simple_Hash($member_token_array) ;
                    
                    $recipient_variables = $data['campaign_record']['structured_data_01']['recipients'][$recipient['delivery_type']]['static']['recipient_variables'][$recipient['recipient_email_address']] ;                    
                    $data['recipients'][$i] = array_merge($recipient,$recipient_variables);
                    $i++ ; 
                    }
                
                break ; 
            default:
                
                $recipient_query_options = array(
                    'delivery_asset' => $data['campaign_record']['delivery_asset'],
                    'filter_by_automation_id' => 'yes',
                    'filter_by_email_status_id' => array(4) // Scheduled status
                    ) ; 
                
                if (isset($options_array['override_paging'])) {
                    $recipient_query_options['override_paging'] = $options_array['override_paging'] ; 
                    }
                if (isset($options_array['page_increment'])) {
                    $this->Set_Page_Increment($options_array['page_increment']) ; 
                    }
                
                $data['recipient_result'] = $this->Retrieve_Email_Recipients($recipient_query_options) ; 
                
                
                $i = 0 ; 
                foreach ($data['recipient_result']['results'] as $recipient) {
                    
                    // Used to validate actions based on member id such as unsubscribe from list
                    $member_token_array = array($recipient['member_id'],$recipient['timestamp_member_created']) ; 
                    $data['recipient_result']['results'][$i]['member_token'] = $this->DB->Simple_Hash($member_token_array) ;
                    
                    $i++ ; 
                    }                
                
                $data['recipients'] = $data['recipient_result']['results'] ; 
                
            }
 

        
        
        if (!$data['automation_record']['automation_id']) {
            $continue = 0 ; 
            // Set error for no automation set
            }
        
        if ($continue == 1) {
            
            if (isset($options_array['override_send_time'])) {
                $send_time = $options_array['override_send_time'] ; 
                } else {
                    $send_time = $data['automation_record']['timestamp_next_action'] ; 
                    }

            // Create recipient sets to ensure a single API call is not made with more than $this->max_delivery_recipients 
            $recipient_sets = array() ; 
            $recipient_error_set = array() ; 
            $set = 0 ; // Set ID increment
            $i = 0 ; // # iteration through the do loop
            $total = 0 ; // count the total unique recipients added
            $set_total = 0 ; // count the total unique recipients added to the set

            $set_template = array(
                'to' => array(),
                'cc' => array(),
                'bcc' => array()
                ) ; 
 

            if ($data['recipient_result']['result_count'] > 0) {
                
                $recipient_sets[$set] = $set_template ; 
                $recipient_sets[$set]['send_time'] = $send_time ;                
                
                do {

                    $validate_email_address = Utilities::Validate_Email_Address($data['recipients'][$i]['email_address']) ;
                    $data['recipients'][$i]['email_address_valid'] = $validate_email_address['valid'] ; 
                    
                    if ($data['recipients'][$i]['do_not_contact_email_address'] == 1) {
                        $data['recipients'][$i]['system_alert_id'] = 180  ; // Do not contact error
                        $recipient_error_set[] = $data['recipients'][$i] ; 
                        
                        } elseif ($data['recipients'][$i]['email_address_valid'] == 'no') {
                            $data['recipients'][$i]['system_alert_id'] = 195 ; // Invalid email address
                            $recipient_error_set[] = $data['recipients'][$i] ;                                                  
                        
                        } else {
                            $recipient_sets[$set][$data['recipients'][$i]['delivery_type']][] = $data['recipients'][$i] ; 
                            $set_total++ ; // Increment the set counter
                            $total++ ; // The total recipients added to all sets 
                            }


                    $i++ ; // Total results processed                

                    if ($set_total >= $this->max_delivery_recipients) {
                        $set++ ; // Increment the set when we get to max recipients in a set
                        $set_total = 0 ; // Reset the set total for the next loop around
                        if ($total < $data['recipient_result']['result_count']) {
                            $recipient_sets[$set] = $set_template ; 
                            if ($send_time != 'now') {
                                $recipient_sets[$set]['send_time'] = $send_time + 30 ;  // Add 30 seconds
                                } else {
                                    $recipient_sets[$set]['send_time'] = $send_time ; 
                                    } 
                            }
                        }

                    } while ($i < $data['recipient_result']['result_count']) ;  
                }
            
            // Process do not contact errors and mark them in emails table
            foreach ($recipient_error_set as $error_item) {

                $this->Set_Email_ID($error_item['email_id']) ; 
                $update_error_item = array(
                    'email_status_id' => 1, // Error
                    'recipient_email_address' => $error_item['email_address'],
                    'recipient_name' => $error_item['full_name'],
                    'system_alert_id' => $error_item['system_alert_id'] // Error as provided in processing above
                    ) ; 
                $error_update = $this->Update_Email_Recipient($update_error_item) ; 
                }

            }
        
        $this->recipient_sets = $recipient_sets ;         
        $this->recipient_total_count = $total ;         
        return $this ;                  
        }
    
    
    

    
    // properties_array = the send properties sent to the email service
    // options_array = custom values that override defaults within the class (outside of email service options)
    
    // 1. Determine who the recipients should be
    // 2. Save those recipients to the recipients table
    // 3. Mark the automation as having recipients packaged
    // 4. 
    
    public function Action_Process_Delivery($campaign_id,$automation_id,$asset_id,$properties_array = array(),$options_array = array()) { 
        
        $continue = 1 ;
        
        $recipient_variables = array() ; // Initialize recipient variables array         
    
        
        $this->Set_Campaign_By_ID($campaign_id) ; 
        $data['campaign_record'] = $this->Get_Campaign() ;         

        if (isset($data['campaign_record']['test_mode'])) {
            $properties_array['test_mode'] = $data['campaign_record']['test_mode'] ;
            }
        if (isset($data['campaign_record']['halt_delivery'])) {
            $options_array['halt_delivery'] = $data['campaign_record']['halt_delivery'] ;
            }
        
        
        $this->Set_Automation_By_ID($automation_id) ; 
        $data['automation_record'] = $this->Get_Automation() ; 

        
        if ($this->user['user_id'] != $data['automation_record']['user_id']) { 
            $this->Set_User_By_ID($data['automation_record']['user_id']) ;
            }

        if ($this->account['account_id'] != $data['automation_record']['account_id']) {
            $this->Set_Account_By_ID($data['automation_record']['account_id']) ;
            }

        if (($this->profile['account_id'] != $data['automation_record']['account_id']) OR ($this->profile['user_id'] != $data['automation_record']['user_id'])) {
            $this->Set_Profile_By_User_Account($data['automation_record']['user_id'],$data['automation_record']['account_id']) ;
            }
        
        if ($this->asset['asset_id'] != $asset_id) {
            $asset_options = array(
                'skip_history' => 'yes'
                ) ; 
            $this->Set_Asset_By_ID($asset_id,$asset_options) ;
            }
        
        // Scheduled -- Compile and save recipients to recipients table
        if ($data['automation_record']['automation_status_id'] == 4) {

            $create_recipients_options = array() ; 
            
            if ($properties_array['test_mode']) {
                $create_recipients_options['test_mode'] = $properties_array['test_mode'] ; 
                }
            
            $this->Action_Create_Email_Recipients($create_recipients_options) ; 
            $data['automation_record'] = $this->Get_Automation() ; 
            }
        

        // Packaged -- Create recipient sets from recipients table and attempt delivery
        if (($data['automation_record']['automation_status_id'] == 5) OR ($data['automation_record']['automation_status_id'] == 1)) {  
            
            $options_array['override_paging'] = 'no' ; 
            $options_array['page_increment'] = 4000 ; // Was necessary to add these contraints to ensure server didn't crash by pulling to many recipients at once

            $this->Action_Set_Recipient_Sets($options_array) ;  // Set the receipient sets
            $this->Action_Queue_Email($properties_array,$options_array) ; 
            }        

        return $this ;    
        }
    


    // ***** NOT USED FOR DELIVERIES!!!    
    // Simple query to filter a list of emails from the Emails table.
    public function Set_Email_Recipients($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
            
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        
        
        $data['recipient_list_query'] = $this->Retrieve_Email_Recipients($query_options) ; 
        $data['recipient_count'] = $data['recipient_list_query']['result_count'] ; 
        $data['recipient_list'] = $data['recipient_list_query']['results'] ; 
        
        $this->email_query_result = $data['recipient_list_query'] ;
        $this->recipient_total_count = $data['recipient_count'] ;
        
        if ($data['recipient_count'] == 0) {
            $this->recipient_list = 'error' ; 
            } else {
            
                $recipient_set_final = array() ; 
                
                $i = 0 ; 
                foreach ($data['recipient_list'] as $recipient) {
                    
                    $recipient_set_final[] = $this->Action_Time_Territorialize_Dataset($recipient) ; 
                    $i++ ; 
                    }

                $this->recipient_list = $recipient_set_final ;
                }        
        
        

        return $this ;        
        }
    
    
    
    // Runs through the recipients determined in the Campaign record settings & filters and pulls a list 
    // of eligible asset members to receive the delivery (based on the currently set automation_id)
    // Once array of recipients is set, places the recipients into the emails table and readies for delivery
    public function Action_Create_Email_Recipients($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $recipients_options = array(
            'override_paging' => 'yes'
            ) ; 
        
        $this->Action_Set_Campaign_Recipients_List($recipients_options) ;
        $recipient_delivery_array = $this->Get_Campaign_Recipients_List() ; 

        
        // This creates the full list of recipients in the recipients table
        // Delivery script should pull from that table to parse recipient sets, and merge IDs with contacts, and deliver
        $email_base_input = array(
            'automation_id' => $this->automation_id,
            ) ; 
        
        if (isset($query_options->test_mode)) {
            $email_base_input['test_mode'] = $query_options->test_mode ; 
            }        
        
        $result = $this->Create_Email_Recipients($email_base_input,$recipient_delivery_array) ;
        $this->test_data = $result ; 


        // If recipients were successfully created, then change the status to Packaged
        // Looking for an error on result isn't the best way to do this, since that's just looking 
        // at the last recipient creation attempt
        if (!$result['error']) {
            $query_options = array(
                'automation_status_id' => 5
                ) ; 
            $data['automation_update'] = $this->Update_Automation($query_options) ; 
            $this->Set_Automation_By_ID() ; 
            }

        return $this ;
        }
    

    
    
    // Create entries of recipients that will be receive an email as part of an automation delivery
    public function Create_Email_Recipients($query_options,
        $recipient_delivery_array = array()
        ) { 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => "emails",
            'values_set' => array()               
            );
        
        if (count($recipient_delivery_array) > 0) {
            
            foreach ($recipient_delivery_array as $recipient) {

                 $this_recipient = array(
                    'automation_id' => $this->automation_id,
                    'email_status_id' => $recipient['email_status_id'],
                    'member_id_value' => $recipient['member_id_value'],
                    'member_id' => $recipient['member_id'],
                    'delivery_type' => $recipient['delivery_type']
                    ) ;
                
                if (isset($recipient['recipient_email_address'])) {
                    $this_recipient['recipient_email_address'] = $recipient['recipient_email_address'] ; 
                    }
                if (isset($recipient['recipient_name'])) {
                    $this_recipient['recipient_name'] = $recipient['recipient_name'] ; 
                    }            
                if (isset($recipient['system_alert_id'])) {
                    $this_recipient['system_alert_id'] = $recipient['system_alert_id'] ; 
                    }                
                if (isset($query_options->test_mode)) {
                    $this_recipient['test_mode'] = $query_options->test_mode ; 
                    }                
                $query_array['values_set'][] = $this_recipient ; 
                
                } 

            $result = $this->DB->Query('INSERT',$query_array) ;
            $this->email_query_result = $result ;                    
            } 
        
        
        return $result ; 
        }
    
    
    
    public function Update_Email_Recipient($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $query_array = array(
            'table' => 'emails',
            'values' => $values_array,
            'where' => "emails.email_id='$this->email_id'"
            );            
            
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->email_query_result = $result ; 
        
        return $result ;         
        }
    
    
    public function Update_Email_Domain($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
                
        switch ($this->status) {
            case 'app':
                $table_name = 'app_email_domains' ; 
                break ;
            default: 
                $table_name = 'email_domains' ; 
            }
        
        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $query_array = array(
            'table' => $table_name,
            'values' => $values_array,
            'where' => "$table_name.domain_id='$this->domain_id'"
            );
                    
            
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->email_query_result = $result ; 

        return $result ;         
        }
    
    
    public function Action_Update_Email_Recipients($campaign_id,$automation_id,$asset_id,$properties_array = array(),$options_array = array()) {
        
        $recipient_variables = array() ; // Initialize recipient variables array         
        
        // Set the receipient sets
        $this->Action_Set_Recipient_Sets($options_array) ;         
        $data['recipient_sets'] = $this->Get_Recipient_Sets() ;
        
        
        $this->Action_Create_Email_Recipients($email_base_input,
            $recipient_set['to'], 
            $recipient_set['cc'], 
            $recipient_set['bcc']
            ) ;        
        
        return $this ;    
        }
    
    
    // Compiles all of the recipient level data into an array with email_address as the array key
    public function Action_Set_Recipient_Variables($recipient_sets) {
        
        $recipient_variables = array() ; 
        
        foreach ($recipient_sets['to'] as $recipient) {
            $recipient_variables[$recipient['email_address']] = $recipient ; 
            }
        foreach ($recipient_sets['cc'] as $recipient) {
            $recipient_variables[$recipient['email_address']] = $recipient ;
            }
        foreach ($recipient_sets['bcc'] as $recipient) {
            $recipient_variables[$recipient['email_address']] = $recipient ;
            }        
        
        return $recipient_variables ; 
        }
    
    
    
    // properties_array = the send properties sent to the email service
    // options_array = custom values that override defaults within the class (outside of email service options)
    public function Action_Queue_Email($properties_array = array(),$options_array = array()) {
        
        $continue = 1 ; 
        $delivery_error_array = array() ; 
        $recipient_variables = array() ; // Initialize recipient variables array 
        
        $data['campaign_record'] = $this->Get_Campaign() ;        
        $data['automation_record'] = $this->Get_Automation() ; 

        if ($this->user['user_id'] != $data['automation_record']['user_id']) {
            $this->Set_User_By_ID($data['automation_record']['user_id']) ;
            }

        if ($this->account['account_id'] != $data['automation_record']['account_id']) {
            $this->Set_Account_By_ID($data['automation_record']['account_id']) ;
            }

        $this->Set_Profile_By_User_Account($data['automation_record']['user_id'],$data['automation_record']['account_id']) ;

        $this->Set_Sending_Domain($this->account['email_domain']) ;
        $this->Action_Compile_Email($this->asset['asset_id']) ;                
        
        $this->Set_Email_Sender() ; 
       
        
        $data['recipient_sets'] = $this->Get_Recipient_Sets() ;             
        $data['delivery_result'] = array() ; 

        
        // If there are no recipients to send to, mark the automation item as complete
        if ($this->recipient_total_count == 0) {
            $continue = 0 ; 
            $query_options = array(
                'automation_status_id' => 8, // Complete because there are no recipients to send to
                'error_count' => 0,
                'status_code' => 0,
                'status_result' => ''
                ) ; 
            $data['automation_update'] = $this->Update_Automation($query_options) ; 
            $this->Set_Automation_By_ID() ;
            }
        
        
        // Now process each set of recipients as a separate delivery              
        $set = 0 ;         
        foreach ($data['recipient_sets'] as $recipient_set) {

            // Create recipient variables for the delivery
            $recipient_variables = $this->Action_Set_Recipient_Variables($recipient_set) ;

            $this->Action_Personalize_Recipient_Variables() ; // Replaces *|recipient_x|* variables with %recipient.x% variables
            
            $this->API_Action_Queue_Email(
                $recipient_set['to'], 
                $recipient_set['cc'], 
                $recipient_set['bcc'], 
                $recipient_variables,
                'internal', 'internal', $recipient_set['send_time'],
                $properties_array,
                $options_array
                ) ;  // For scheduled send time use $data['automation_record']['timestamp_next_action']

            
            $data['delivery_result'][] = $this->Get_Delivery_Result() ; 
            $data['delivery_result'][$set]['set_number'] = $set ; 

            $campaign_automation_input = array() ; 
            $campaign_input = array() ; 

            if ($data['delivery_result'][$set]['email_status']['result'] == 'success') {

                $email_content_input = array(
                    'automation_id' => $data['automation_record']['automation_id'],
                    'vendor_id' => $this->vendor_id,
                    'sending_domain_id' => $data['delivery_result'][$set]['sending_domain']['domain_id'],
                    'email_sid' => $data['delivery_result'][$set]['email_sid'], 
                    'email_subject' => $data['delivery_result'][$set]['delivery_array']['subject'],
                    'email_html' => $data['delivery_result'][$set]['delivery_array']['html'],
                    'email_text' => $data['delivery_result'][$set]['delivery_array']['text']
                    ) ; 

                $this->Action_Create_Email_Content($email_content_input) ; 

                $data['delivery_result'][$set]['email_content_id'] = $this->Get_Email_Content_ID() ; 

                $q = 0 ; 
                foreach ($recipient_set['to'] as $recipient) {

                    $this->Set_Email_ID($recipient['email_id']) ; 
                    $update_email_item = array(
                        'email_content_id' => $data['delivery_result'][$set]['email_content_id'],
                        'email_status_id' => 7, // Queued
                        'recipient_email_address' => $recipient['email_address'],
                        'recipient_name' => $recipient['full_name'],
                        'sender_email_address' => $data['delivery_result'][$set]['email_sender']['sender_email_address'],
                        'sender_name' => $data['delivery_result'][$set]['email_sender']['sender_name'],
                        'test_mode' => $data['delivery_result'][$set]['delivery_array']['o:testmode'],
                        'timestamp_scheduled' => TIMESTAMP
                        ) ; 
                    $email_update = $this->Update_Email_Recipient($update_email_item) ; 
                    $q++ ; 
                    }                
                
                
                // Increment delivery count for the email
//                $delivery_asset = $this->Get_Asset() ; 
//                $delivery_count_update = array(
//                    'asset_id' => $delivery_asset['asset_id'],
//                    'deliveries' => $delivery_asset['deliveries'] + 1,
//                    'auth_override' => 'yes'
//                    ) ; 
//                $this->Action_Update_Content($delivery_count_update) ; 
                
                
                
                if ($data['campaign_record']['structured_data_01']['campaign_options']['cost'] != 'free') {

                    $credit_calculation = array(
                        'action' => 'email',
                        'recipient_count' => $q
                        ) ; 

                    $data['account_marketing_credits'] = $this->Action_Calculate_Marketing_Credits($credit_calculation)->Get_Account_Marketing_Credits() ; 
                    $account_update_query = array(
                        'marketing_credit_balance' => $data['account_marketing_credits']['projected_balance']
                        ) ;

                    $marketing_credit_increment = $this->Action_Update_Account($account_update_query)->Set_Account() ;                    
                    }
 
                
                } else {
                
                    // Add the set number for this errored delivery. Will record it below.
                    $delivery_error_array[] = $set ; 
                    
                    }

            $set++ ; 
            }

        
        $this->email_action = $data['delivery_result'] ; // Set the results of the delivery
        
        
        // Update the automation item following the delivery...
        $this->Action_Set_Recipient_Sets($options_array) ;  // Reset the recipient sets to see if there are any additional that need to be sent to
        
        
        // If there are no additional recipients to send to (after delivery attempt), mark the automation item as complete
        if ($this->recipient_total_count == 0) {
            $query_options = array(
                'automation_status_id' => 8, // Complete because there are no recipients to send to
                'error_count' => 0,
                'status_code' => 0,
                'status_result' => ''
                ) ; 
            $data['automation_update'] = $this->Update_Automation($query_options) ; 
            
            
            $asset_record = $this->Get_Asset() ; 
            $update_delivery_count = array(
                'asset_id' => $asset_record['asset_id'],
                'deliveries' => $asset_record['deliveries'] + 1,
                'timestamp_last_delivered' => TIMESTAMP,
                'auth_override' => 'yes',
                'asset_input_info' => array(
                    'rewrite_latest_history' => 'yes'
                    )
                ) ;  
            
            if ($data['campaign_record']['structured_data_01']['campaign_options']['cost'] != 'free') {
                $this->Action_Update_Content($update_delivery_count) ;            
                }            
            }        
                
        
        // If there are delivery errors, create an error message status_result for the automation item
        if (count($delivery_error_array) > 0) {
            
            $automation_item = $this->Get_Automation() ; 
            
            $status_result = array() ; 
            foreach ($delivery_error_array as $error_set_number) {
                $status_code = $data['delivery_result'][$error_set_number]['email_status']['alert_id'] ; // This will only capture the LAST error code in the set of errors
                $status_result[] = $data['delivery_result'][$error_set_number]['email_status'] ;                  
                }
            
            // Update the automation item
            $query_options = array(
                'automation_status_id' => 1, // Error
                'error_count' => $automation_item['error_count'] + 1, 
                'status_code' => $status_code,
                'status_result' => $data['delivery_result'][$error_set_number]['email_status']['alert_admin_notes']
                ) ; 
            
            }

        
        $this->Set_Automation_By_ID() ;       

        return $this ;
        }
    
    
    
    
    
    public function Get_Email_Content_ID() {

        return $this->email_content_id ; 
        }
    
    
    // Sets an array of available domains for the current account from the service and merges them with the database domain_id
    public function Action_Create_Email_Content($email_content_input = array()) {
        
        
        $result = $this->Create_Email_Content($email_content_input) ;
        
        if ($result['insert_id']) {
            
            $this->email_content_id = $result['insert_id'] ; 
            
            } else {
                $this->email_content_id = $result['error'] ; 
                }

        return $this ; 
        }
    
    
    // CREATE entry of a single domain associated with an account
    public function Create_Email_Content($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
         
        
        $query_array = array(
            'table' => "email_content",
            'values' => array(
                'automation_id' => $query_options->automation_id,
                'vendor_id' => $query_options->vendor_id,
                'sending_domain_id' => $query_options->sending_domain_id,
                'email_sid' => $query_options->email_sid,
                'email_subject' => $query_options->email_subject,
                'email_html' => $query_options->email_html,
                'email_text' => $query_options->email_text
                )
            );

        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->email_query_result = $result ; 
        
        return $result ; 
        }    
    
    
    
    
    // Sets an array of available domains for the current account from the service and merges them with the database domain_id
    public function Action_Set_Domains($query_options = array()) {
        
        if (!isset($query_options['limit'])) {
            $query_options['limit'] = 20 ; 
            }
        if (!isset($query_options['skip'])) {
            $query_options['skip'] = 0 ; 
            }
        
        $result = $this->API_Set_Domains($query_options) ; 
        
        if ($result->http_response_code == 200) {
            $this->domains = $result->http_response_body->items ; 
            
            $i = 0 ; 
            foreach ($this->domains as $domain) {
                
                $domain_query_options['filter_by_domain_display_name'] = $domain->name ; 
                $domain_result = $this->Retrieve_Domain_List($domain_query_options) ;
                
                if (!$domain_result['error']) {
                    $this->domains[$i]->domain_id = $domain_result['results'][0]['domain_id'] ; 
                    }  
                
                $i++ ; 
                }
            
            } else {
                $this->domains = array() ; 
                }

        return $this ; 
        }
    
    
    // ACTION Compile an email content based off the parent asset id
    public function Action_Compile_Email($asset_id,$compile_options = array(),$asset_options = array()) { 
        
        if (!isset($asset_options['skip_history'])) { 
            $asset_options['skip_history'] = 'yes' ;
            }        
        
        $this->Set_Asset_By_ID($asset_id,$asset_options)->Action_Compile_Asset() ; 
        $compiled_asset = $this->Get_Asset_Compiled() ; // This is only the email HTML; not subject line       
        
        $this->Set_Model_Timings('email_asset_set_and_compiled') ; 
        
        
        $this->email['subject_line_01'] = $this->asset['subject_line_01'] ;
        $this->email['subject_line_02'] = $this->asset['subject_line_02'] ;
        $this->email['subject_line_03'] = $this->asset['subject_line_03'] ;
        $this->email['email_original'] = $compiled_asset ;

            
        // Process template logic via Twig
        try {
            $templates = array('content' => $this->email['email_original']);
            $env = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
            $this->email['email_original'] = $env->render('content',array(
                    'user_record' => $this->user,
                    'profile_record' => $this->profile,
                    'account_record' => $this->account
                    )
                );  
            } catch (Exception $e) {
//                print_r($e)  ;
                }        
        
        $this->Set_Model_Timings('twig_templates_processed') ; 
        
        
        // Personalize action key IDs
        $data['campaign_record'] = $this->Get_Campaign() ; 
        $data['automation_record'] = $this->Get_Automation() ; 
        if (isset($data['automation_record']['action_keys'])) {
            $this->email['email_original'] = $this->Action_Personalize_Action_Keys($this->email['email_original'],$data['automation_record']['action_keys']) ;
            }

        
        if (isset($data['automation_record']['data_overrides']) OR (isset($compile_options['data_overrides']))) {
            
            if (isset($data['automation_record']['data_overrides'])) {
                $data_overrides = $data['automation_record']['data_overrides'] ; 
                } else {
                $data_overrides = $compile_options['data_overrides'] ; 
                }
            
            $this->Action_Process_Email_Data_Overrides($data['automation_record']['data_overrides']) ;
            }
        
        
        
        $additional_personalization = array() ; // Initialize personalization array for later
        
        // Personalization keys pull in specific records to be processed as part of delivery process
        if (isset($data['campaign_record']['structured_data_01']['personalization_keys']) OR (isset($compile_options['personalization_keys']))) {
            
            if (isset($data['campaign_record']['structured_data_01']['personalization_keys'])) {
                $personalization_keys = $data['campaign_record']['structured_data_01']['personalization_keys'] ; 
                } else {
                    $personalization_keys = $compile_options['personalization_keys'] ; 
                    }
            
            foreach ($personalization_keys as $key => $value) {
                switch ($key) {
                    case 'comment_id':
                        $personalization_item = new Asset() ; 
                        $additional_personalization['comment_record'] = $personalization_item->Set_Comment_By_ID($value)->Get_Comment() ;  
                        break ; 
                    case 'asset_id':
                        $personalization_item = new Asset() ; 
                        $additional_personalization['asset_record'] = $personalization_item->Set_Asset_By_ID($value)->Get_Asset() ; 
                        break ;
                    case 'account_id':
                        $personalization_item = new Account() ; 
                        $additional_personalization['account_record'] = $personalization_item->Set_Account_By_ID($value)->Get_Account() ; 
                        break ; 
                    case 'campaign_id':
                        $personalization_item = new Asset() ; 
                        $campaign_options = array(
                            'condense_campaign' => 'yes',
                            'skip_delivery_asset' => 'yes',
                            'skip_content_asset' => 'yes'
                            ) ;
                        $additional_personalization['campaign_record'] = $personalization_item->Set_Campaign_By_ID($value,$campaign_options)->Get_Campaign() ; 
                        break ;                        
                    }
                }
            }

        
        // personalization_overrides process through after the other standard strings 
        // are added in the System personalization script. 
        if (isset($data['automation_record']['personalization_overrides'])) {
            $additional_personalization['personalization_overrides'] = $data['automation_record']['personalization_overrides'] ;
            } else if (isset($compile_options['personalization_overrides'])) {
                $additional_personalization['personalization_overrides'] = $compile_options['personalization_overrides'] ;    
                } 
        $this->Action_Personalize_Email('internal',$additional_personalization) ;
        $this->Set_Model_Timings('email_personalized') ; 
        

         
        
        
        // Process template logic via Twig
        $templates = array('content' => $this->email['email_original']);
        $env = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
        $this->email['email_original'] = $env->render('content',array(
                'user_record' => $this->user,
                'profile_record' => $this->profile,
                'account_record' => $this->account
                )
            ); 
        $this->Set_Model_Timings('twig_templates_processed_for_email_content') ; 
        
        
        
        $css = new Css() ;
        $css->Action_Inline($this->email['email_original'],$compile_options) ; 
        $this->Set_Model_Timings('css_inlined') ; 
        $css_result = $css->Get_Inlined_Array() ; 

        
        
        $this->email['status'] = $css_result['status'] ; 
        $this->email['errors'] = $css_result['errors'] ; 
        
        if ($css_result['status']['result'] == 'success') {

            $this->email['email_html'] = mb_convert_encoding($css_result['email_html'], 'UTF-8', 'UTF-8');
            $this->email['email_text'] = mb_convert_encoding($css_result['email_text'], 'UTF-8', 'UTF-8');
            $this->email['email_css'] = $css->Get_Css_Array() ;
            
            $this->Set_Model_Timings('email_encoded') ; 
        
            $this->Set_Alert_Response(132) ; // Email compile success
            
            } else {
                $this->email['email_html'] = 'error' ;
                $this->email['email_text'] = 'error' ;
            
                $this->Set_Alert_Response(133) ; // Error during email compiling
            
                $context_array = array(
                    'asset_id' => $asset_id,
                    'css_result' => $css_result['status']
                    ) ; 
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($context_array),
                    'location' => __METHOD__
                    )) ;            
                }
        
        return $this ; 
        }
    
    
    // This action first pulls the email events and sets them to the email_events property
    // Then it creates the events in the database    
    public function Action_Process_Email_Events($events_query_array) { 
        
        $this->Set_Model_Timings('Email.Action_Process_Email_Events - Initialize') ;
        
        $this->Action_Query_Email_Events($events_query_array) ;  
        $result = $this->Action_Create_Email_Events() ; 
        
        $events_full_array = $this->Get_Email_API_Event() ; 

        $this->Set_Model_Timings('Email.Action_Process_Email_Events - Complete') ;
        return $result ; 
        }
    
    
    
    public function Action_Create_Email_Events() {
        
        $this->Set_Model_Timings('Email.Action_Create_Email_Events - Initialize') ;
        
        $events_array = $this->Get_Email_Events() ; 
        
        if (count($events_array) > 0) {        
            $result = $this->Create_Email_Event_Entries($events_array) ;             
            
            $domain_values = array(
                'microtimestamp_last_event_queried' => $this->last_event_queried
                ) ; 
            $update_event_queried = $this->Update_Email_Domain($domain_values) ;            
            }
                        
        return $result ; 
        }
    
    // This is an API action -- needs to be rewritten
    // ACTION Get email events from delivery service via API
    public function Action_Query_Email_Events($input_array) {

        if (isset($input_array['process_limit'])) {
            $this->email_events_process_limit = $input_array['process_limit'] ; 
            }
        
        // Format for Events array...
        //    array(
        //        'begin' => $microtimestamp_lower, 
        //        'end' => $microtimestamp_upper, 
        //        'event_selections' => array('delivered','clicked','complained') // PREFERRED OVER "EVENT"       
        //        'event_selections' => 'all' // OPTIONAL WAY INSTEAD OF LISTING SEPARATELY        
        //        'event' => 'delivered' // Not preferred
        //        )

        
        // Process and sanitize the input array into a query array that can ping the delivery service
        $query_array = array() ; 
           
        if ($input_array['domain']) {
            $event_name = $input_array['domain'].'/events' ; 
            unset($input_array['domain']) ; 
            } else {
                $event_name = 'events' ;
                }
        
        if ($input_array['begin']) {
            $query_array['begin'] = $input_array['begin'] ; 
            }
        if ($input_array['end']) {
            $query_array['end'] = $input_array['end'] ; 
            } 
        if ($input_array['event']) {
            $query_array['event'] = $input_array['event'] ; 
            }
        if ($input_array['limit']) {
            $query_array['limit'] = $input_array['limit'] ; 
            } 
        
//        if ($input_array['event_selections']) {
//            $events_string = '' ; 
//            foreach ($input_array['event_selections'] as $event) {
//                $events_string .= $event.' OR ' ; 
//                }
//            $events_string = rtrim($events_string," OR ") ; 
////            if ($input_array['event_selections'][0] == 'all') {
////                $events_string = 'accepted OR rejected OR delivered OR failed OR opened OR clicked OR unsubscribed OR complained OR stored' ; 
////                }
//            $query_array['event'] = $events_string ; 
//            
//            unset($input_array['event_selections'],unset($query_array['event']))  ;
//            }
        
        // Next, Last, First, Previous provide a single URL to ping the service with a predefined list of parameters (used in loops)
        // Will take precedence over query_array individual parameters 
        if ($input_array['next']) {
            $query_url = $input_array['next'] ;
            } 
        if ($input_array['last']) {
            $query_url = $input_array['last'] ; 
            } 
        if ($input_array['first']) {
            $query_url = $input_array['first'] ; 
            }  
        if ($input_array['previous']) {
            $query_url = $input_array['previous'] ; 
            }       
                    

        if (isset($query_url)) {
            $query_url = str_replace("https://","",$query_url) ;            
            $query_url = str_replace("api.mailgun.net/v2/","",$query_url) ;
            $query_url = str_replace("api.mailgun.net/v3/","",$query_url) ;
            $query_array['query_url'] = $query_url ;     
            }
        

        $this->Action_Execute_Email_Query('get',$event_name,$query_array) ; 
        $events_full_array = $this->Get_Email_API_Event() ; 
        $this->email_events = $events_full_array->http_response_body->items ; 

        $this->Set_Model_Timings('Email.Action_Query_Email_Events - API query complete') ;
        
        return $this ;        
        }

    
    
    
    

    
    
    // ACTION Personalize an email asset using User / Account information to replace wildcards
    // ** Note - Should only be personalizing SENDER user / profile / account level info. 
    // Recipient level info should be personalized at the email vendor, unless you're sending a static notification to a specific recipient
    public function Action_Personalize_Email($submission = 'internal',$additional_personalization = array()) {
        
        $personalization_array['account_record'] = $this->Get_Account() ; 
        $personalization_array['user_record'] = $this->Get_User() ; 
        $personalization_array['profile_record'] = $this->Get_Profile() ; 

        foreach ($additional_personalization as $set_title => $set) {
            if (isset($set)) {
                foreach ($set as $key => $value) {
                    $personalization_array[$set_title][$key] = $value ; 
                    }                
                }
            }
        
        if (isset($additional_personalization['personalization_overrides'])) {
            foreach ($additional_personalization['personalization_overrides'] as $override_set_title => $override_set_values) {
                $personalization_array[$override_set_title] = $override_set_values ; 
                }
            }
        

        $asset_array = array() ; 
        
        if ('internal' === $submission) {
            
            $asset_array['subject_line_01'] = $this->email['subject_line_01'] ; 
            $asset_array['subject_line_02'] = $this->email['subject_line_02'] ; 
            $asset_array['subject_line_03'] = $this->email['subject_line_03'] ; 
            $asset_array['email_original'] = $this->email['email_original'] ;
            $asset_array['email_html'] = $this->email['email_html'] ; 
            $asset_array['email_text'] = $this->email['email_text'] ; 
            
            } else {
        
                $asset_array['personalized_string'] = $submission ; 
            
                }

            
        foreach ($asset_array as $key => $value) {
            
            $asset_array[$key] = $this->Action_Personalize_String($value,$personalization_array) ;
            
            }
        
        // Return the personalized asset
        if ('internal' === $submission) {
            
            $this->email['subject_line_01'] = $asset_array['subject_line_01'] ;
            $this->email['subject_line_02'] = $asset_array['subject_line_02'] ;
            $this->email['subject_line_03'] = $asset_array['subject_line_03'] ;
            $this->email['email_original'] = $asset_array['email_original'] ;
            $this->email['email_html'] = $asset_array['email_html'] ;
            $this->email['email_text'] = $asset_array['email_text'] ;
            
            } else {
        
                $this->personalized_string = $asset_array['personalized_string'] ;
            
                }
        
        return $this ; 
        }
    
    
    // ACTION Personalize an email asset using User / Account information to replace wildcards
    // ** Note - Should only be personalizing user / account level info. Recipient level info should be personalized at the vendor
    public function Action_Process_Email_Data_Overrides($data_overrides = array()) {
                
        $asset_array = array() ; 
        
        $asset_array['subject_line_01'] = $this->email['subject_line_01'] ; 
        $asset_array['subject_line_02'] = $this->email['subject_line_02'] ; 
        $asset_array['subject_line_03'] = $this->email['subject_line_03'] ; 
        $asset_array['email_original'] = $this->email['email_original'] ;
        $asset_array['email_html'] = $this->email['email_html'] ; 
        $asset_array['email_text'] = $this->email['email_text'] ; 
        
        
        foreach ($asset_array as $key => $value) {
            
            if (isset($data_overrides[$key])) {
                $asset_array[$key] = $data_overrides[$key] ;                
                }

            }
        
        // Return the personalized asset
        $this->email['subject_line_01'] = $asset_array['subject_line_01'] ;
        $this->email['subject_line_02'] = $asset_array['subject_line_02'] ;
        $this->email['subject_line_03'] = $asset_array['subject_line_03'] ;
        $this->email['email_original'] = $asset_array['email_original'] ;
        $this->email['email_html'] = $asset_array['email_html'] ;
        $this->email['email_text'] = $asset_array['email_text'] ;
        
        return $this ; 
        
        }
    
    
    // Replace *|recipient_x|* variables with %recipient.x% variables
    public function Action_Personalize_Recipient_Variables($submission = 'internal') {
        
        $asset_array = array() ; 
        
        if ('internal' === $submission) {
            
            $asset_array['subject_line_01'] = $this->email['subject_line_01'] ; 
            $asset_array['subject_line_02'] = $this->email['subject_line_02'] ; 
            $asset_array['subject_line_03'] = $this->email['subject_line_03'] ; 
            $asset_array['email_original'] = $this->email['email_original'] ;
            $asset_array['email_html'] = $this->email['email_html'] ; 
            $asset_array['email_text'] = $this->email['email_text'] ; 
            
            } else {
        
                $asset_array['personalized_string'] = $submission ; 
            
                }
        
        // Process each value 
        foreach ($asset_array as $key => $value) {

            
            // NORMAL STRINGS
            $found_string = 1 ; 
            do {
                
                $sub_string = Utilities::Extract_Text_Block($value, '*|recipient_', '|*'); // Gathers the replaceable string 
                $original_string = '*|recipient_'.$sub_string['extraction'].'|*' ;
                $replacement_string = '%recipient.'.$sub_string['extraction'].'%' ;
                $value = str_replace($original_string,$replacement_string,$value) ;  // Removes the file string            
                
//                if ($key == 'email_html') {
//                    print_r($sub_string) ;     
//                    }

                if (strlen($sub_string['extraction']) > 0) {
                    $found_string = 1 ;
                    } else {
                        $found_string = 0 ;
                        }
                
                } while ($found_string == 1) ; 
            
            
            
            // LINKS
            $start_position = 0 ; 
            do {
                
                $link_string = Utilities::Extract_Text_Block($value, '<a ', '>',$start_position) ; // Gathers the replaceable string 
                
                $original_string = $link_string['extraction'] ;
                
//                if ($key == 'email_html') {
//                    print_r('<br>link string<br>') ;
//                    print_r($link_string) ; 
//                    print_r('<br><br>') ; 
//                    }                
                
                
                if (strlen($link_string['extraction']) > 0) {
                    $i = 0 ; 
                    $found_variable_string = 1 ;
                    $link_start_position = 0 ; 
                    do {


                        $link_variable_string = Utilities::Extract_Text_Block($original_string, '*%7Crecipient_', '%7C*',$link_start_position) ; // Extracts a replaceable variable from the link

                        $original_variable_string = '*%7Crecipient_'.$link_variable_string['extraction'].'%7C*' ;
                        $replacement_variable_string = '%recipient.'.$link_variable_string['extraction'].'%' ;

                        $link_string['extraction'] = str_replace($original_variable_string,$replacement_variable_string,$link_string['extraction']) ;  // Removes the file string            


//                        if ($key == 'email_html') {
//                            print_r('<br>link variable<br>') ; 
//                            print_r($link_variable_string) ; 
//                            }


                        if (strlen($link_variable_string['extraction']) > 0) {
                            $found_variable_string = 1 ;
                            } else {
                                $found_variable_string = 0 ;
                                }                    

                        $i++; 
                        $link_start_position = $link_variable_string['next_position'] ; 
    //                } while ($i < 10) ;
                        } while ($found_variable_string == 1) ;   
                    
                    
                    
                    
//                    if ($key == 'email_html') {
//                        print_r('replacement string '.$link_string['extraction']) ; 
//                        }
                    
                    $replacement_string = $link_string['extraction'] ;
                    $value = str_replace($original_string,$replacement_string,$value) ;  // Removes the file string            
                    $found_string = 1 ;                    
                    
                    } else {
                        $found_string = 0 ;
                        }

                $start_position = $link_string['next_position'] ; 
                } while ($found_string == 1) ;    
            

            $asset_array[$key] = $value ; 
            }

        
        // Return the personalized asset
        if ('internal' === $submission) {
            
            $this->email['subject_line_01'] = $asset_array['subject_line_01'] ;
            $this->email['subject_line_02'] = $asset_array['subject_line_02'] ;
            $this->email['subject_line_03'] = $asset_array['subject_line_03'] ;
            $this->email['email_original'] = $asset_array['email_original'] ;
            $this->email['email_html'] = $asset_array['email_html'] ;
            $this->email['email_text'] = $asset_array['email_text'] ;
            
            } else {
        
                $this->personalized_string = $asset_array['personalized_string'] ;
            
                }
        
        return $this ; 
        
        }
    
    // Reverts %recipient.x variables back to *|recipient_|* variables.
    public function Action_Revert_Recipient_Vendor_Variables($submission = 'internal') {
        
        $asset_array = array() ; 
        $placeholder_string = '=======placeholderstring=======' ;
        
        if ('internal' === $submission) {
            
            $asset_array['subject_line_01'] = $this->email['subject_line_01'] ; 
            $asset_array['subject_line_02'] = $this->email['subject_line_02'] ; 
            $asset_array['subject_line_03'] = $this->email['subject_line_03'] ; 
            $asset_array['email_original'] = $this->email['email_original'] ;
            $asset_array['email_html'] = $this->email['email_html'] ; 
            $asset_array['email_text'] = $this->email['email_text'] ; 
            
            } else {
        
                $asset_array['personalized_string'] = $submission ; 
            
                }
        
        // Process each value 
        foreach ($asset_array as $key => $value) {

            $value = $placeholder_string.$value ; 
            
            // NORMAL STRINGS
            $found_string = 1 ; 
            do {
                
                $sub_string = Utilities::Extract_Text_Block($value, '%recipient.', '%'); // Gathers the replaceable string 
                $original_string = '%recipient.'.$sub_string['extraction'].'%' ;
                $replacement_string = '*|'.$sub_string['extraction'].'|*' ;
                $value = str_replace($original_string,$replacement_string,$value) ;  // Removes the file string            
                
                if (strlen($sub_string['extraction']) > 0) {
                    $found_string = 1 ;
                    } else {
                        $found_string = 0 ;
                        }
                
                } while ($found_string == 1) ; 
            
            
            $value = ltrim($value,$placeholder_string) ;
            

            $asset_array[$key] = $value ; 
            }

        
        // Return the personalized asset
        if ('internal' === $submission) {
            
            $this->email['subject_line_01'] = $asset_array['subject_line_01'] ;
            $this->email['subject_line_02'] = $asset_array['subject_line_02'] ;
            $this->email['subject_line_03'] = $asset_array['subject_line_03'] ;
            $this->email['email_original'] = $asset_array['email_original'] ;
            $this->email['email_html'] = $asset_array['email_html'] ;
            $this->email['email_text'] = $asset_array['email_text'] ;
            
            } else {
        
                $this->personalized_string = $asset_array['personalized_string'] ;
            
                }
        
        return $this ; 
        
        }
    
    
    
    
    // Replace *|recipient_x variables with *|x|* variables. Also strips out recipient_ prefix
    public function Action_Revert_Recipient_Variables($submission = 'internal') {
        
        $asset_array = array() ; 
        $placeholder_string = '==============' ;
        
        if ('internal' === $submission) {
            
            $asset_array['subject_line_01'] = $this->email['subject_line_01'] ; 
            $asset_array['subject_line_02'] = $this->email['subject_line_02'] ; 
            $asset_array['subject_line_03'] = $this->email['subject_line_03'] ; 
            $asset_array['email_original'] = $this->email['email_original'] ;
            $asset_array['email_html'] = $this->email['email_html'] ; 
            $asset_array['email_text'] = $this->email['email_text'] ; 
            
            } else {
        
                $asset_array['personalized_string'] = $submission ; 
            
                }
        
        // Process each value 
        foreach ($asset_array as $key => $value) {

            $value = $placeholder_string.$value ; 
            
            // NORMAL STRINGS
            $found_string = 1 ; 
            do {
                
                $sub_string = Utilities::Extract_Text_Block($value, '*|recipient_', '|*'); // Gathers the replaceable string 
                $original_string = '*|recipient_'.$sub_string['extraction'].'|*' ;
                $replacement_string = '*|'.$sub_string['extraction'].'|*' ;
                $value = str_replace($original_string,$replacement_string,$value) ;  // Removes the file string            
                
                if (strlen($sub_string['extraction']) > 0) {
                    $found_string = 1 ;
                    } else {
                        $found_string = 0 ;
                        }
                
                } while ($found_string == 1) ; 
            
            $value = ltrim($value,$placeholder_string) ;
            
            // LINKS
//            $start_position = 0 ; 
//            do {
//                
//                $link_string = Utilities::Extract_Text_Block($value, '<a ', '>',$start_position) ; // Gathers the replaceable string 
//                
//                $original_string = $link_string['extraction'] ;
//                
////                if ($key == 'email_html') {
////                    print_r('<br>link string<br>') ;
////                    print_r($link_string) ; 
////                    print_r('<br><br>') ; 
////                    }                
//                
//                
//                if (strlen($link_string['extraction']) > 0) {
//                    $i = 0 ; 
//                    $found_variable_string = 1 ;
//                    $link_start_position = 0 ; 
//                    do {
//
//
//                        $link_variable_string = Utilities::Extract_Text_Block($original_string, '*%7Crecipient_', '%7C*',$link_start_position) ; // Extracts a replaceable variable from the link
//
//                        $original_variable_string = '*%7Crecipient_'.$link_variable_string['extraction'].'%7C*' ;
//                        $replacement_variable_string = '%recipient.'.$link_variable_string['extraction'].'%' ;
//
//                        $link_string['extraction'] = str_replace($original_variable_string,$replacement_variable_string,$link_string['extraction']) ;  // Removes the file string            
//
//
////                        if ($key == 'email_html') {
////                            print_r('<br>link variable<br>') ; 
////                            print_r($link_variable_string) ; 
////                            }
//
//
//                        if (strlen($link_variable_string['extraction']) > 0) {
//                            $found_variable_string = 1 ;
//                            } else {
//                                $found_variable_string = 0 ;
//                                }                    
//
//                        $i++; 
//                        $link_start_position = $link_variable_string['next_position'] ; 
//    //                } while ($i < 10) ;
//                        } while ($found_variable_string == 1) ;   
//                    
//                    
//                    
//                    
////                    if ($key == 'email_html') {
////                        print_r('replacement string '.$link_string['extraction']) ; 
////                        }
//                    
//                    $replacement_string = $link_string['extraction'] ;
//                    $value = str_replace($original_string,$replacement_string,$value) ;  // Removes the file string            
//                    $found_string = 1 ;                    
//                    
//                    } else {
//                        $found_string = 0 ;
//                        }
//
//                $start_position = $link_string['next_position'] ; 
//                } while ($found_string == 1) ;    
            

            $asset_array[$key] = $value ; 
            }

        
        // Return the personalized asset
        if ('internal' === $submission) {
            
            $this->email['subject_line_01'] = $asset_array['subject_line_01'] ;
            $this->email['subject_line_02'] = $asset_array['subject_line_02'] ;
            $this->email['subject_line_03'] = $asset_array['subject_line_03'] ;
            $this->email['email_original'] = $asset_array['email_original'] ;
            $this->email['email_html'] = $asset_array['email_html'] ;
            $this->email['email_text'] = $asset_array['email_text'] ;
            
            } else {
        
                $this->personalized_string = $asset_array['personalized_string'] ;
            
                }
        
        return $this ; 
        
        }    
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    //////////////////////    
    
    
    // RETRIEVE domain by domain id (integer)
    public function Retrieve_Domain_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
         
        
        $query_array = array(
            'table' => "app_email_domains",
            'fields' => "app_email_domains.*",
            'where' => "app_email_domains.domain_id>0 "
            );

        
        if ($query_options->filter_by_app_default_domain == 'yes') {
            $query_array['where'] .= " AND app_email_domains.app_default_domain='1'" ; 
            }
        
        if ($query_options->filter_by_account_default_domain == 'yes') {
            $query_array['where'] .= " AND app_email_domains.account_default_domain='1'" ; 
            }
        
        if (isset($query_options->filter_by_domain_id)) {
            $query_array['where'] .= " AND app_email_domains.domain_id='$query_options->filter_by_domain_id'" ; 
            }
        
        if (isset($query_options->filter_by_domain_name)) {
            $query_array['where'] .= " AND app_email_domains.domain_name='$query_options->filter_by_domain_name'" ; 
            }
        
        if (isset($query_options->filter_by_domain_display_name)) {
            $query_array['where'] .= " AND app_email_domains.domain_display_name='$query_options->filter_by_domain_display_name'" ; 
            }    
        
        $result = $this->DB->Query('SELECT',$query_array,'force') ;
        
        $this->email_query_result = $result ; 
        
        return $result ;        
        }
    
    
    
    
    // CREATE entry of a single domain associated with an account
    public function Create_Domain_Entry($domain_name, $account_id = 'current') {
        
        if ('current' === $account_id) {
            $account_id = $this->account_id ; 
            } 
        
        if ($this->status == 'internal') {
            $table_name = 'app_email_domains' ; 
            $account_id = 0 ; 
            } else {
                $table_name = 'email_domains' ;
                }      
        
        $domain_first_event_queried = Utilities::System_Time_Manipulate('7 d','-')['time_manipulated'] ;
        $domain_initial_query = Utilities::System_Time_Manipulate('40 d','-')['time_manipulated'] ;
            
         

        $query_array = array(
            'table' => $table_name,
            'values' => array(
                'account_id' => $account_id,
                'domain_name' => $domain_name,
                'microtimestamp_last_event_queried' => $domain_first_event_queried,
                'microtimestamp_first_event_queried' => $domain_first_event_queried,
                'microtimestamp_initial_query' => $domain_initial_query
                ),
            'where' => "account_id='$account_id' AND domain_name='$domain_name'"
            );
        
        $domain_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
        if ($domain_record['insert_id']) {
            $this->domain_id = $domain_record['insert_id'] ; 
            $this->domain_last_event_queried = $domain_first_event_queried ; 
            $this->domain_first_event_queried = $domain_first_event_queried ; 
            $this->domain_initial_query = $domain_initial_query ; 
            } else {
                $this->domain_id = $domain_record['results']['domain_id'] ; 
                $this->domain_last_event_queried = $domain_record['results']['microtimestamp_last_event_queried'] ;
                $this->domain_first_event_queried = $domain_record['results']['microtimestamp_first_event_queried'] ;
                $this->domain_initial_query = $domain_record['results']['microtimestamp_initial_query'] ;
                }
        
        return $this ;
        
        }
    
    
    // CREATE entries of event results retrieved from the delivery service in the database
    public function Create_Email_Event_Entries($events_array) { 

        $this->Set_Model_Timings('Email.Create_Email_Event_Entries - Initialize') ;
        
        $c = 0 ;  
        foreach ($events_array as $event) {

            $this->Set_Model_Timings('Email.Create_Email_Event_Entries - Begin process email '.$c) ;
            
            $event->message->headers->{'message-id'} = $this->Action_Standardize_Email_SID($event->message->headers->{'message-id'})['email_sid'] ; 
            
            $this_message_sid = $event->message->headers->{'message-id'} ; // Message ID defined by delivery service
            $this_recipient_email = $event->recipient ; // Recipient email address
            $this_event_microtimestamp = $event->{'timestamp'} ; // Microtimestamp of the event

 
            // Get the email_id for this email so we can create an event
            switch ($event->event) {
            
                case 'accepted': // Message has been queued to delivery service but awaiting delivery to receipient
                case 'delivered': // Delivery has been attemped to receipient
                case 'opened':
                case 'clicked':
                case 'failed':
                case 'complained':    
                    
                    $query_array = array(
                    'table' => "emails",
                    'fields' => "emails.email_id, emails.automation_id, member_id, member_id_value",
                    'join_tables' => array(),
                    'where' => "emails.recipient_email_address='$this_recipient_email' AND email_content.email_sid='$this_message_sid'"
                    );

                    $query_array['join_tables'][] = array(
                        'table' => 'email_content',
                        'on' => 'emails.email_content_id',
                        'match' => 'email_content.email_content_id'
                        );                     

                    $retrieve_email = $this->DB->Query('SELECT_JOIN',$query_array) ; 
                    
                    $email_id = $retrieve_email['results']['email_id'] ;   
                    $automation_id = $retrieve_email['results']['automation_id'] ;   
                    $email_record = $retrieve_email['results'] ; 
                    
//                    print_r($retrieve_email) ; 
//                    print_r('Email record') ;
//                    print_r($email_record) ; 
                    
                    $this->Set_Model_Timings('Email.Create_Email_Event_Entries - Retrieved email id '.$c) ;
                    break ; 
                }
            
//            print_r('Email event:') ; 
//            print_r($event) ; 
            
            switch ($event->event) {
                case 'accepted': // Message has been queued to delivery service but awaiting delivery to receipient
                case 'delivered': // Delivery has been attemped to receipient

                    // Update the email & email_content with the event info...
                    $query_array = array(
                    'table' => "emails",
                    'join_tables' => array(),
                    'values' => array(
                        'emails.recipient_domain' => $event->{'recipient-domain'},
                        'emails.sending_ip' => $event->envelope->{'sending-ip'},
                        'emails.email_size' => $event->message->{'size'},
                        'emails.storage_url' => $event->storage->{'url'},
                        'emails.storage_key' => $event->storage->{'key'}                        
                        ),
                    'where' => "emails.recipient_email_address='$this_recipient_email' AND email_content.email_sid='$this_message_sid'"
                    );

                    $query_array['join_tables'][] = array(
                        'table' => 'email_content',
                        'on' => 'emails.email_content_id',
                        'match' => 'email_content.email_content_id'
                        );                    
                    
                    if ($event->event == 'delivered') {
                        $query_array['values']['emails.email_status_id'] = 8 ;  // Complete
                        $query_array['values']['emails.microtimestamp_delivered'] = $event->{'timestamp'} ; 
                        $query_array['values']['emails.delivered'] = 1 ; 
                        }                    
                    
                    $record_event = $this->DB->Query('UPDATE',$query_array) ; 
 

                    // Record event detail data
                    $query_array = array(
                        'table' => "email_events",
                        'values' => array(
                            'email_id' => $email_id,
                            'event' => $event->event,
                            'microtimestamp_event' => $this_event_microtimestamp,
                            'log_level' => $event->{'log-level'}
                            ),
                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
                        );

                    if ($email_id > 0) {
                        $record_event_details = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
                        }

                    break ;
                case 'opened':

 
                    // Update the email & email_content with the event info...
                    $query_array = array(
                    'table' => "emails",
                    'join_tables' => array(),
                    'values' => array(
                        'emails.email_status_id' => 8, // Complete
                        'emails.opened' => 1,
                        'emails.microtimestamp_opened' => array(
                            'value' => $this_event_microtimestamp,
                            'statement' => "CASE WHEN emails.microtimestamp_opened = 0 THEN 'VALUE' ELSE emails.microtimestamp_opened END"
                            ),
                        ),
                    'where' => "emails.recipient_email_address='$this_recipient_email' AND email_content.email_sid='$this_message_sid'"
                    );
                                     
                    $query_array['join_tables'][] = array(
                        'table' => 'email_content',
                        'on' => 'emails.email_content_id',
                        'match' => 'email_content.email_content_id'
                        ) ;                    
                    
                    $record_event = $this->DB->Query('UPDATE',$query_array) ;
                    
                    
                    
                    // Record event detail data
                    $query_array = array(
                        'table' => "email_events",
                        'values' => array(
                            'email_id' => $email_id,
                            'event' => $event->event,
                            'microtimestamp_event' => $this_event_microtimestamp,
                            'log_level' => $event->{'log-level'},
                            'recipient_ip' => $event->ip,
                            'recipient_country' => $event->geolocation->country,
                            'recipient_region' => $event->geolocation->region,
                            'recipient_city' => $event->geolocation->city,
                            'recipient_client_os' => $event->{'client-info'}->{'client-os'},
                            'recipient_client_name' => $event->{'client-info'}->{'client-name'},
                            'recipient_client_type' => $event->{'client-info'}->{'client-type'},
                            'recipient_device_type' => $event->{'client-info'}->{'device-type'},
                            'recipient_user_agent ' => $event->{'client-info'}->{'user-agent'},
                            ),
                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
                        );

                    if ($email_id > 0) {
                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
                        }

                    break ;                        
                case 'clicked':

                    // Update the email & email_content with the event info...
                    // Since this is a click, it must also have been opened, so mark the email as opened if it isn't already                    
                    $query_array = array(
                    'table' => "emails",
                    'join_tables' => array(),
                    'values' => array(
                        'emails.email_status_id' => 8, // Complete
                        'emails.opened' => 1,
                        'emails.clicked' => 1,
                        'emails.microtimestamp_opened' => array(
                            'value' => $this_event_microtimestamp,
                            'statement' => "CASE WHEN emails.microtimestamp_opened = 0 THEN 'VALUE' ELSE emails.microtimestamp_opened END"
                            ),
                        ),
                    'where' => "emails.recipient_email_address='$this_recipient_email' AND email_content.email_sid='$this_message_sid'"
                    );
                                     
                    $query_array['join_tables'][] = array(
                        'table' => 'email_content',
                        'on' => 'emails.email_content_id',
                        'match' => 'email_content.email_content_id'
                        ) ;                    
                    
                    $record_event = $this->DB->Query('UPDATE',$query_array) ;
                    

                    // Record event detail data
                    $query_array = array(
                        'table' => "email_events",
                        'values' => array(
                            'email_id' => $email_id,
                            'event' => $event->event,
                            'microtimestamp_event' => $this_event_microtimestamp,
                            'log_level' => $event->{'log-level'},
                            'click_url' => $event->url,
                            'recipient_ip' => $event->ip,
                            'recipient_country' => $event->geolocation->country,
                            'recipient_region' => $event->geolocation->region,
                            'recipient_city' => $event->geolocation->city,
                            'recipient_client_os' => $event->{'client-info'}->{'client-os'},
                            'recipient_client_name' => $event->{'client-info'}->{'client-name'},
                            'recipient_client_type' => $event->{'client-info'}->{'client-type'},
                            'recipient_device_type' => $event->{'client-info'}->{'device-type'},
                            'recipient_user_agent ' => $event->{'client-info'}->{'user-agent'},
                            ),
                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
                        );

                    if ($email_id > 0) {
                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
                        }                        

                    break ;
                case 'failed':

                    $event_message = '' ; 
                    if (isset($event->{'delivery-status'}->{'description'})) {
                        $event_message .= $event->{'delivery-status'}->{'description'} ; 
                        }
                    if ((isset($event->{'delivery-status'}->{'message'})) AND ($event->{'delivery-status'}->{'message'} != $event->{'delivery-status'}->{'description'})) {
                        $event_message .= $event->{'delivery-status'}->{'message'} ; 
                        }
                    if (isset($event->{'delivery-status'}->{'attempt-no'})) {
                        $event_message .= ' [Attempt #: '.$event->{'delivery-status'}->{'attempt-no'}.']' ; 
                        }
                    
                    // Record event level data
                    $query_array = array(
                        'table' => "email_events",
                        'values' => array(
                            'email_id' => $email_id,
                            'event' => $event->event,
                            'microtimestamp_event' => $this_event_microtimestamp,
                            'log_level' => $event->{'log-level'},
                            'event_message' => $event_message,
                            'fail_severity' => $event->{'severity'}
                            ),
                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
                        );

                    if (isset($event->{'delivery-status'}->{'code'})) {
                        $query_array['values']['event_code'] = $event->{'delivery-status'}->{'code'} ; 
                        }
                    
                    if ($email_id > 0) {
                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
                        
                        $this->Set_Model_Timings('Email.Create_Email_Event_Entries - Recorded failed event '.$c) ;
                        
                        if ($event->{'severity'} == 'permanent') {
                            // This is a rudimentary update of a metadata id to an email sublist
                            // In the future, would be nice to wrap this in it's own model and add a history update
                            
//                            print_r('automation query') ; 
//                            print_r($query_options) ; 
                            
                            $email = new Email() ; 
                            $email->Set_Automation_By_ID($automation_id) ; 
                            $automation = $email->Get_Automation() ; 

                            $delivery_asset_options = array('skip_history' => 'yes') ; 
                            $email->Set_Asset_By_ID($automation['delivery_asset_id'],$delivery_asset_options) ; 
                            $data['remove_asset'] = $email->Get_Asset() ; 

                            $member_list_options['filter_by_member_id'] = $email_record['member_id'] ; 
                            $member_list_options['filter_by_user_id'] = 'no' ;
                            $member_list_options['filter_by_account_id'] = 'no' ;
                            $email->Set_Asset_Members_List($member_list_options) ;

                            $member_list = $email->Get_Asset_Members_List() ;       
                            $data['member_record'] = $member_list[0] ; 
                            $data['subscribe_asset'] = $email->Get_Asset() ; 

                            $this->Set_Model_Timings('Email.Create_Email_Event_Entries - Pulled asset member list for email '.$c) ;
//                            print_r($member_list) ; 
                            
                            $options_array['asset_id'] = $data['remove_asset']['asset_id'] ; 
                            $options_array['email_metadata_id'] = 49 ; // Cleaned
                            $email->Action_Asset_Member_Update($data['member_record']['member_id'],$options_array) ;
                            }
                        }

                    break ;
                case 'complained': // Marked as SPAM by recipient in email client. Need to mark as Do Not Contact, or at least unsubscribe from the list...

                    // Record event level data
                    $query_array = array(
                        'table' => "email_events",
                        'values' => array(
                            'email_id' => $email_id,
                            'event' => $event->event,
                            'microtimestamp_event' => $this_event_microtimestamp
                            ),
                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
                        );

                    if ($email_id > 0) {
                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
                                                
                        // Since the recipient complained, unsubscribe them from the email sublist for the original delivery asset...

                        // This is a rudimentary update of a metadata id to an email sublist
                        // In the future, would be nice to wrap this in it's own model and add a history update
                        $email = new Email() ; 
                        $email->Set_Automation_By_ID($automation_id) ; 
                        $automation = $email->Get_Automation() ;

                        $delivery_asset_options = array('skip_history' => 'yes') ; 
                        $email->Set_Asset_By_ID($automation['delivery_asset_id'],$delivery_asset_options) ; 
                        $data['remove_asset'] = $email->Get_Asset() ; 

                        $member_list_options['filter_by_member_id'] = $email_record['member_id'] ; 
                        $member_list_options['filter_by_user_id'] = 'no' ;
                        $member_list_options['filter_by_account_id'] = 'no' ;
                        $email->Set_Asset_Members_List($member_list_options) ;

                        $member_list = $email->Get_Asset_Members_List() ;       
                        $data['member_record'] = $member_list[0] ; 
                        $data['subscribe_asset'] = $email->Get_Asset() ; 

                        $options_array['asset_id'] = $data['remove_asset']['asset_id'] ; 
                        $options_array['email_metadata_id'] = 47 ; // Unsubscribe
                        $email->Action_Asset_Member_Update($data['member_record']['member_id'],$options_array) ; 
                        }

                    break ;                        
                case 'unsubscribed': // We won't be using this for Essenty

//                    // Retrieve the email id
//                    $query_array = array(
//                        'table' => "emails",
//                        'fields' => "emails.email_id",
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $result = $this->DB->Query('SELECT',$query_array) ;
//                    $email_id = $result['results']['email_id'] ;
//
//                    // Record event level data
//                    $query_array = array(
//                        'table' => "email_events",
//                        'values' => array(
//                            'email_id' => $email_id,
//                            'event' => $event->event,
//                            'microtimestamp_event' => $this_event_microtimestamp
//                            ),
//                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
//                        );
//
//                    if ($email_id > 0) {
//                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
//                        }

                    break ; 
                case 'stored': // Incoming message to Mailgun

//                    $this_recipient_email = Utilities::Extract_Text_Block($event->message->headers->{'to'}, '<', '>') ; 
//                    if (!$this_recipient_email) {
//                        $this_recipient_email = $event->message->headers->{'to'} ; 
//                        }
//                    $this_sender_email = Utilities::Extract_Text_Block($event->message->headers->{'from'}, '<', '>') ; 
//                    if (!$this_sender_email) {
//                        $this_sender_email = $event->message->headers->{'from'} ; 
//                        }
//
//                    // Record email level data
//                    $query_array = array(
//                        'table' => "emails_incoming",
//                        'values' => array(
//                            'account_id' => $this->account_id,
//                            'vendor_id' => $this->vendor_id,
//                            'message_id' => $this_message_sid,
//                            'recipient_domain' => $this->sending_domain,
//                            'recipient_email_address' => $this_recipient_email,
//                            'recipient_name' => $event->message->headers->{'to'},
//                            'sender_email_address' => $this_sender_email,
//                            'sender_name' => $event->message->headers->{'from'},
//                            'message_subject' => $event->message->headers->{'subject'},
//                            'microtimestamp_received' => $event->{'timestamp'},
//                            'email_size' => $event->message->{'size'},
//                            'storage_url' => $event->storage->{'url'},
//                            'storage_key' => $event->storage->{'key'}
//                            ),
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $record = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;                        

                    break ;                        
                case 'rejected': // This is incoming email that was blocked by Mailgun's spam filters.
                    break ;
                }

            unset($email_id) ;
            
            $this->last_event_queried = $event->{'timestamp'} ; 
            $c++ ; 
            if ((isset($this->email_events_process_limit)) AND ($c == $this->email_events_process_limit)) {
                break ; 
                }
            }
 
        
        $this->Set_Model_Timings('Email.Create_Email_Event_Entries - Complete') ;
        
        return $this ;
        
        }

    }












// ARCHIVE FOR LATER USE 
// There's some good processing logic in here that might be good for an eventual Mailgun event processing app
// But not needed for essenty


//    // CREATE entries of event results retrieved from the delivery service in the database
//    public function ARCHIVE_Create_Event_Entries($events_array) {
//
//         
//        foreach ($events_array as $event) {
//
//            $this_message_sid = $event->message->headers->{'message-id'} ; // Message ID defined by delivery service
//            $this_recipient_email = $event->recipient ; // Recipient email address
//            $this_event_microtimestamp = $event->{'timestamp'} ; // Microtimestamp of the event
//
//            switch ($event->event) {
//                case 'accepted': // Message has been queued to delivery service but awaiting delivery to receipient
//                case 'delivered': // Delivery has been attemped to receipient
//
//                    $recipient_name = str_replace(' <'.$this_recipient_email.'>','',$event->message->headers->{'to'}) ; 
//                    $sender_name = str_replace(' <'.$event->envelope->{'sender'}.'>','',$event->message->headers->{'from'}) ; 
//
//                    // Record top level email data
//                    $query_array = array(
//                        'table' => "emails",
//                        'values' => array(
//                            'account_id' => $this->account_id,
//                            'message_id' => $this_message_sid,
//                            'sending_domain' => $this->sending_domain,
//                            'vendor_id' => $this->vendor_id,
//                            'recipient_email_address' => $this_recipient_email,
//                            'recipient_domain' => $event->{'recipient-domain'},
//                            'recipient_name' => $recipient_name,
//                            'sender_email_address' => $event->envelope->{'sender'},
//                            'sender_name' => $sender_name,
//                            'message_subject' => $event->message->headers->{'subject'},
//                            'timestamp_scheduled' => $event->message->{'scheduled-for'},
//                            'sending_ip' => $event->envelope->{'sending-ip'},
//                            'email_size' => $event->message->{'size'},
//                            'storage_url' => $event->storage->{'url'},
//                            'storage_key' => $event->storage->{'key'}
//                            ),
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    if ($event->event == 'delivered') {
//                        $query_array['values']['microtimestamp_delivered'] = $event->{'timestamp'} ; 
//                        $query_array['values']['delivered'] = 1 ; 
//                        }
//
//                    $record = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ; // Update Else Insert ensures we don't duplicate entries
//
//                    // Get the unique email id from within the database for this message
//                    if ($record['insert_id']) {
//                        $email_id = $record['insert_id'] ;
//                        } else {
//                            $email_id = $record['original_results']['email_id'] ;
//                            }
//
//                    // If this is a NEW entry into the database, process the remainder of the lower level data
//                    // into the database (tied to the email id). Includes tags, user defined variables.
//                    // ++ Need to add processing capability for Campaign Identifiers array
//                    if ($record['insert_id']) {    
//
//                        // Create tag relationships
//                        foreach ($event->tags as $tag) {
//
//                            if ($tag != '') {
//
//                                // Creates an instance of the tag name in the database (associated with account_id)
//                                $query_array = array(
//                                    'table' => "email_tags",
//                                    'values' => array(
//                                        'account_id' => $this->account_id,
//                                        'tag_name' => $tag
//                                        ),
//                                    'where' => "account_id='$this->account_id' AND tag_name='$tag'"
//                                    );
//
//                                $tag_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
//                                if ($tag_record['insert_id']) {
//                                    $tag_id = $tag_record['insert_id'] ; 
//                                    } else {
//                                        $tag_id = $tag_record['results']['tag_id'] ; 
//                                        }
//
//
//                                // Associates the tag id with the email id
//                                $query_array = array(
//                                    'table' => "email_tag_relationships",
//                                    'values' => array(
//                                        'email_id' => $email_id,
//                                        'tag_id' => $tag_id
//                                        ),
//                                    'where' => "email_id='$email_id' AND tag_id='$tag_id'"
//                                    );
//
//                                $tag_relationship_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
//                                }
//                            }
//
//                        // Create custom variable relationships
//                        $custom_data = json_decode($event->{'user-variables'}->{'my-custom-data'},true) ; 
//
//                        if ($custom_data) {
//                            foreach ($custom_data as $data => $value) {
//
//                                // Creates an instance of the variable name in the database (associated with account_id)
//                                $query_array = array(
//                                    'table' => "email_custom_data",
//                                    'values' => array(
//                                        'account_id' => $this->account_id,
//                                        'data_name' => $data
//                                        ),
//                                    'where' => "account_id='$this->account_id' AND data_name='$data'"
//                                    );
//
//                                $data_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
//                                if ($data_record['insert_id']) {
//                                    $data_id = $data_record['insert_id'] ; 
//                                    } else {
//                                        $data_id = $data_record['results']['data_id'] ; 
//                                        }
//
//                                // Associates the data id with the email id
//                                $query_array = array(
//                                    'table' => "email_custom_data_relationships",
//                                    'values' => array(
//                                        'email_id' => $email_id,
//                                        'data_id' => $data_id,
//                                        'data_value' => $value
//                                        ),
//                                    'where' => "email_id='$email_id' AND data_id='$data_id' AND data_value='$value'"
//                                    );
//
//                                $data_relationship_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;                    
//
//
//                                }
//                            }
//                        }
//
//                    // Record event level data
//                    $query_array = array(
//                        'table' => "email_events",
//                        'values' => array(
//                            'email_id' => $email_id,
//                            'event' => $event->event,
//                            'microtimestamp_event' => $this_event_microtimestamp                                    
//                            ),
//                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
//                        );
//
//                    if ($email_id > 0) {
//                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
//                        }
//
//                    break ;
//                case 'opened':
//
//                    // Update top level email data
//                    // Ensures the open microtimestamp is only recorded if the current open time is 0
//                    $query_array = array(
//                        'table' => "emails",
//                        'fields' => "emails.email_id, emails.microtimestamp_opened",
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $result = $this->DB->Query('SELECT',$query_array) ;
//                    $email_id = $result['results']['email_id'] ;
//
//                    if ($result['results']['microtimestamp_opened'] == 0) {
//                        $microtimestamp_opened = $this_event_microtimestamp ; 
//                        } else {
//                            $microtimestamp_opened = $result['results']['microtimestamp_opened'] ;
//                            }                  
//
//                    $query_array = array(
//                        'table' => "emails",
//                        'values' => array(
//                            'microtimestamp_opened' => $microtimestamp_opened,
//                            'opened' => 1
//                            ),
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $record_open = $this->DB->Query('UPDATE',$query_array) ;
//
//
//                    // Record event level data
//                    $query_array = array(
//                        'table' => "email_events",
//                        'values' => array(
//                            'email_id' => $email_id,
//                            'event' => $event->event,
//                            'microtimestamp_event' => $this_event_microtimestamp,
//                            'recipient_ip' => $event->ip,
//                            'recipient_country' => $event->geolocation->country,
//                            'recipient_region' => $event->geolocation->region,
//                            'recipient_city' => $event->geolocation->city,
//                            'recipient_client_os' => $event->{'client-info'}->{'client-os'},
//                            'recipient_client_name' => $event->{'client-info'}->{'client-name'},
//                            'recipient_client_type' => $event->{'client-info'}->{'client-type'},
//                            'recipient_device_type' => $event->{'client-info'}->{'device-type'},
//                            'recipient_user_agent ' => $event->{'client-info'}->{'user-agent'},
//                            ),
//                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
//                        );
//
//                    if ($email_id > 0) {
//                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
//                        }
//
//                    break ;                        
//                case 'clicked':
//
//                    // Update top level email data
//                    $query_array = array(
//                        'table' => "emails",
//                        'fields' => "emails.email_id, emails.opened",
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $result = $this->DB->Query('SELECT',$query_array) ;
//                    $email_id = $result['results']['email_id'] ;
//
//
//                    // Since this is a click, it must have been opened
//                    // So mark the email as opened if it isn't already
//                    if ($result['results']['opened'] == 0) {
//
//                        $query_array = array(
//                            'table' => "emails",
//                            'values' => array(
//                                'microtimestamp_opened' => $microtimestamp_opened,
//                                'opened' => 1,
//                                'clicked' => 1
//                                ),
//                            'where' => "email_id='$email_id'"
//                            );
//
//                        $record_click = $this->DB->Query('UPDATE',$query_array) ;
//
//                        } else {
//
//                            $query_array = array(
//                                'table' => "emails",
//                                'values' => array(
//                                    'clicked' => 1
//                                    ),
//                                'where' => "email_id='$email_id'"
//                                );
//
//                            $record_click = $this->DB->Query('UPDATE',$query_array) ;
//
//                            }
//
//                    // Record event level data
//                    $query_array = array(
//                        'table' => "email_events",
//                        'values' => array(
//                            'email_id' => $email_id,
//                            'event' => $event->event,
//                            'microtimestamp_event' => $this_event_microtimestamp,
//                            'click_url' => $event->url,
//                            'recipient_ip' => $event->ip,
//                            'recipient_country' => $event->geolocation->country,
//                            'recipient_region' => $event->geolocation->region,
//                            'recipient_city' => $event->geolocation->city,
//                            'recipient_client_os' => $event->{'client-info'}->{'client-os'},
//                            'recipient_client_name' => $event->{'client-info'}->{'client-name'},
//                            'recipient_client_type' => $event->{'client-info'}->{'client-type'},
//                            'recipient_device_type' => $event->{'client-info'}->{'device-type'},
//                            'recipient_user_agent ' => $event->{'client-info'}->{'user-agent'},
//                            ),
//                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
//                        );
//
//                    if ($email_id > 0) {
//                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
//                        }                        
//
//                    break ;
//                case 'failed':
//
//                    // Retrieve the email id
//                    $query_array = array(
//                        'table' => "emails",
//                        'fields' => "emails.email_id, emails.opened",
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $result = $this->DB->Query('SELECT',$query_array) ;
//                    $email_id = $result['results']['email_id'] ;
//
//                    // Record event level data
//                    $query_array = array(
//                        'table' => "email_events",
//                        'values' => array(
//                            'email_id' => $email_id,
//                            'event' => $event->event,
//                            'microtimestamp_event' => $this_event_microtimestamp,
//                            'event_message' => $event->{'delivery-status'}->{'description'}.' '.$event->{'delivery-status'}->{'message'},
//                            'fail_severity' => $event->{'severity'}
//                            ),
//                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
//                        );
//
//                    if ($email_id > 0) {
//                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
//                        }
//
//                    break ;
//                case 'complained':
//
//                    // Retrieve the email id
//                    $query_array = array(
//                        'table' => "emails",
//                        'fields' => "emails.email_id",
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $result = $this->DB->Query('SELECT',$query_array) ;
//                    $email_id = $result['results']['email_id'] ;
//
//                    // Record event level data
//                    $query_array = array(
//                        'table' => "email_events",
//                        'values' => array(
//                            'email_id' => $email_id,
//                            'event' => $event->event,
//                            'microtimestamp_event' => $this_event_microtimestamp
//                            ),
//                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
//                        );
//
//                    if ($email_id > 0) {
//                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
//                        }
//
//                    break ;                        
//                case 'unsubscribed':
//
//                    // Retrieve the email id
//                    $query_array = array(
//                        'table' => "emails",
//                        'fields' => "emails.email_id",
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $result = $this->DB->Query('SELECT',$query_array) ;
//                    $email_id = $result['results']['email_id'] ;
//
//                    // Record event level data
//                    $query_array = array(
//                        'table' => "email_events",
//                        'values' => array(
//                            'email_id' => $email_id,
//                            'event' => $event->event,
//                            'microtimestamp_event' => $this_event_microtimestamp
//                            ),
//                        'where' => "email_id='$email_id' AND event='$event->event' AND microtimestamp_event='$this_event_microtimestamp'"
//                        );
//
//                    if ($email_id > 0) {
//                        $record_event = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
//                        }
//
//                    break ; 
//                case 'stored':
//
//                    $this_recipient_email = Utilities::Extract_Text_Block($event->message->headers->{'to'}, '<', '>') ; 
//                    if (!$this_recipient_email) {
//                        $this_recipient_email = $event->message->headers->{'to'} ; 
//                        }
//                    $this_sender_email = Utilities::Extract_Text_Block($event->message->headers->{'from'}, '<', '>') ; 
//                    if (!$this_sender_email) {
//                        $this_sender_email = $event->message->headers->{'from'} ; 
//                        }
//
//                    // Record email level data
//                    $query_array = array(
//                        'table' => "emails_incoming",
//                        'values' => array(
//                            'account_id' => $this->account_id,
//                            'vendor_id' => $this->vendor_id,
//                            'message_id' => $this_message_sid,
//                            'recipient_domain' => $this->sending_domain,
//                            'recipient_email_address' => $this_recipient_email,
//                            'recipient_name' => $event->message->headers->{'to'},
//                            'sender_email_address' => $this_sender_email,
//                            'sender_name' => $event->message->headers->{'from'},
//                            'message_subject' => $event->message->headers->{'subject'},
//                            'microtimestamp_received' => $event->{'timestamp'},
//                            'email_size' => $event->message->{'size'},
//                            'storage_url' => $event->storage->{'url'},
//                            'storage_key' => $event->storage->{'key'}
//                            ),
//                        'where' => "account_id='$this->account_id' AND message_id='$this_message_sid' AND recipient_email_address='$this_recipient_email'"
//                        );
//
//                    $record = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;                        
//
//                    break ;                        
//                case 'rejected':
//                    // This is incoming email that was blocked by Mailgun's spam filters.
//                    break ;
//                }
//
//            $this->last_event_queried = $event->{'timestamp'} ; 
//            }
//
//        return $this ;
//        
//        }