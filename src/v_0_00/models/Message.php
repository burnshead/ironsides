<?php

namespace Ragnar\Ironsides ;

use Instasent\SMSCounter\SMSCounter;

class Message extends Asset {
        
    // MESSAGING SERVICE CONNECTION
    public $_client ;
    public $_connection;
    public $_connection_status ;
    
    public $_config ; 
    public $_messaging_client ; 
    public $_messaging_client_status ;
    
    
    public $message_api_event ; // API    
    public $message_query_result ; // Database
    
    public $message_paging ; 
    
    public $delivery_result ; // Full array delivery result 
    
	private static $_instance; //The single instance
    private $logs = array();
    public $status ; // user or app : user would use the account's own mail service account; app uses the application account
    
    
    
    public $message_sender ;
    
    public $recipients ; 
    public $message_content_id ; 
    public $message_id ; 
    public $message ; 
    
    public $message_list ; 
    public $message_event_list ; 
    
    public $service ; // Twilio Messaging service sid
    public $notify_service ; // Twilio Notify Service
    
    public $application_id ; // Bandwidth Application (Site) ID
    public $sub_account_id ; // Bandwidth Sub Account ID
    
    
    public $messaging_phone_numbers_list ; 
    public $messaging_phone_number ; 
    
    // Deliverables
    public $max_delivery_recipients = 999 ; // Max number of recipients that can be sent to in a single API call
    public $recipient_total_count ;
    public $recipient_sets ;    
    public $message_action ;    

    
    
    
    
    public function __construct($user_id = 'ignore',$account_id = 'ignore') {
        
        global $DB ;  
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            } 
        
        if ('ignore' !== $account_id) {
            $this->Set_Account_By_ID($account_id) ; 
            
            // Could set the from number here.
            
            }
	   }    
    
    
    //////////////////////
    //                  //
    // NOT BEING USED   //
    //                  //
    //////////////////////
    
    
    // Instantiate messaging delivery service
    // If you want to use the app's connection to the service (not a user account), set status = app
    // Assuming for now that Twilio is our delivery service (vendor_id = 3). Can customize in the future.    
    // 9 = Bandwidth
    public function Connect($status = 'app',$vendor_id = 9,$mode = 'live') {
        
        $this->vendor_id = $vendor_id ;
        
        
        
        if ('app' === $status) {

            $this->Set_System_Vendor($this->vendor_id) ; // Get system API keys
            $this->status = $status ; 

            
                switch ($vendor_id) {
                    case 3:
                        switch ($mode) {
                            case 'live':    
                                $service_account_sid = TWILIO_ACCOUNT_SID ;
                                $service_auth_token = TWILIO_AUTH_TOKEN ;
                                break ;
                            case 'test':
                                // TEST ACCOUNT CREDENTIALS
                                $service_account_sid = 'ACccab86ad07d3bca010e7df63e756edbc' ;
                                $service_auth_token = '76f75e6704ddcbffdeacb2a48465dae1' ; 
                                break ;                
                            }
                        break ; 
                    case 9:
                        
                        
                        
                        break ; 
                }

            
            } else {
            
                // Gets an account's API key for the delivery service
                $this->Set_Account($account_id,false) ;  
            
                $vendor = new Vendor() ; 
                $vendor->Set_Account_By_ID($account_id)->Set_Vendor($this->vendor_id) ;  // Assuming Twilio
                $vendor_key = $vendor->Get_Vendor_Key(5) ;  // Assuming Twilio API Key
                $vendor_api_key = $vendor_key['key_value'] ; 
                
                $this->status = 'user' ; 
                } 
        

        switch ($this->vendor_id) {
            case 3: // Twilio
                
                // The response parameter Twilio-Request-Id appears to be the same within each connection
                try {
                    $this->_connection = new Twilio\Rest\Client($service_account_sid, $service_auth_token);
                    } catch ( Exception $e ) {
                        $error = $e->getMessage().'could not connect' ;  
                        $this->_connection_status = $error ; 
                        $this->message_api_event = $error ; 
                        }
              

                break ; 
            case 9: // Bandwidth
                
                $this->application_id = BANDWIDTH_APPLICATION_ID ; 
                $this->sub_account_id = BANDWIDTH_SUB_ACCOUNT_ID ; 
                
                
                // Connect to Account
                try {
                    
                    $this->_client = new \Iris\Client(BANDWIDTH_API_USERNAME, BANDWIDTH_API_PASSWORD, ['url' => 'https://dashboard.bandwidth.com/api/']);
                    $this->_connection = new \Iris\Account(BANDWIDTH_ACCOUNT_ID, $this->_client);
                    
                    
                    
                    // Connect to messaging and voice service
                    try {

                        $this->_config = new BandwidthLib\Configuration(
                            array(
                                'messagingBasicAuthUserName' => BANDWIDTH_API_TOKEN,
                                'messagingBasicAuthPassword' => BANDWIDTH_API_SECRET
                            )
                        );
                        
                        $this->_messaging_client = new BandwidthLib\BandwidthClient($this->_config);


                        } catch ( Exception $e ) {
                            $error = $e->getMessage().'could not connect' ;  
                            $this->_messaging_client_status = $error ; 
                            $this->message_api_event = $error ; 
                            }                    
                    
                    
                    
                    
                    
                    } catch ( Exception $e ) {
                        $error = $e->getMessage().'could not connect' ;  
                        $this->_connection_status = $error ; 
                        $this->message_api_event = $error ; 
                        }
              

                break ;                
            } 
        
        
        return $this ; 
        }

    

	/*
	Get an instance of the Messaging service
	@return Instance
	*/
	public static function getInstance($account_id = 'internal', $auth_token = 'internal') {
        
        // This code was suggested here to create default variables:
        // https://stackoverflow.com/questions/9166914/using-default-arguments-in-a-function
        if ('internal' === $account_id) {
            $service_account_sid = TWILIO_ACCOUNT_SID;
            }
        if ('internal' === $auth_token) {
            $service_auth_token = TWILIO_AUTH_TOKEN;
            }
        
		if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self($service_account_sid,$service_auth_token);
		}
		if(self::$_instance == null) { // If no instance then make one
            self::$_instance = new self($service_account_sid,$service_auth_token);
		}        
        return self::$_instance;
	}
    
    

    
	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }
	
    // Get the Messaging Service connection
	public function getConnection() {
        return $this->_connection;
	   }    
    
    // Destroy the Messaging Service connection
    // This is used to force a new connection with different API credentials
	public static function destroy() {
        self::$from_phone_number = null ;
        self::$_instance = null ; 
        return self::$_instance ;
	   }     
    

    
    // Validate an incoming webhook from Twilio
    // https://www.twilio.com/docs/usage/security
    // Return $valid_webhook=1 if the X-Twilio-Signature is verified
	public function Validate_Webhook($post_incoming_array) {
        
        global $server_config; 
        
        $valid_webhook = 0 ;
        
        switch ($this->vendor_id) {
            case 3: // Twilio    
                
                $validator = new Twilio\Security\RequestValidator(TWILIO_AUTH_TOKEN);

                $headers = array()  ; 
                foreach (getallheaders() as $name => $value) {
                    $headers[$name] = $value ; 
                    }

                if ($validator->validate($headers['X-Twilio-Signature'], $server_config['page_url_array']['url_full'], $post_incoming_array)) {
                    $valid_webhook = 1 ;
                    } else {
                        $valid_webhook = 0 ;
                        }                
                
                break ; 
            case 9: // Bandwidth
                
                // Callback username: essenty
                // Callback password: 8y6Lm3E6l5QucLlnHdIRXiqSvQ59rL
                // Formed as essenty:8y6Lm3E6l5QucLlnHdIRXiqSvQ59rL
                // https://old.dev.bandwidth.com/callbackSecurity.html
                // Base64 encode: ZXNzZW50eTo4eTZMbTNFNmw1UXVjTGxuSGRJUlhpcVN2UTU5ckw=
                
                $username = 'essenty' ; // Callback username: essenty
                $password = '8y6Lm3E6l5QucLlnHdIRXiqSvQ59rL' ; // Callback password: 8y6Lm3E6l5QucLlnHdIRXiqSvQ59rL

                
                $headers = getallheaders() ; 
                
//                error_log('bandwidth header '.json_encode($headers['Authorization'])) ;
                
                
                // Ideal validation method
//                if ($headers['Authorization'] == ('Basic '.base64_encode($username.':'.$password))) {
//                    $valid_webhook = 1 ;
//                    } else {
//                        $valid_webhook = 0 ;
//                        }
                
                
                // Backup validation method
                $application_id = '4f30ac80-9840-4a0d-b78b-e3467eb5aefd' ;
                $keys_to_hash = array($post_incoming_array['time'],$application_id) ; 
                $key_hash = DAL::Simple_Hash($keys_to_hash) ; 
                                
//                error_log(json_encode($keys_to_hash)) ; 
                
                if ($key_hash == DAL::Simple_Hash(array($post_incoming_array['time'],$post_incoming_array['message']['applicationId']))) {
                    $valid_webhook = 1 ;
                    } else {
                        $valid_webhook = 0 ;
                        }                
                
                
                break ; 
            }
        

        return $valid_webhook ;
        }
    
    
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    
    
    // https://docs.google.com/presentation/d/1mmrwpzDKvmw0E_XbS5aqSfwcFic_p54Ve39RK26FNwc/edit#slide=id.p
    public function Action_Process_Reply($incoming_reply) {

        $continue = 1 ; 
        
        global $server_config ; 
        
        
        $incoming_reply_array = $incoming_reply ; 
        $incoming_reply_object = (object) $incoming_reply ; // Cast the options array as an object to make queries easier to write
    
        
        switch ($this->vendor_id) {
            case 3: // Twilio    
                
                // Normalize the incoming object
                $incoming_reply_object->message_sid = $incoming_reply_object->MessageSid ; 
                
                $incoming_reply_object->to = $incoming_reply_object->To ;
                $incoming_reply_object->from = $incoming_reply_object->From ;
                $incoming_reply_object->from_country = $incoming_reply_object->FromCountry ;
                
                $incoming_reply_object->message_content = $incoming_reply_object->Body ;
                
                
                // Identify the recipient (a User Profile)
                $recipient_query_parameters = array(
                    'filter_by_phone_number_messaging' => $incoming_reply_object->to
                    ) ; 

                $this->Set_Messaging_Phone_Number($recipient_query_parameters) ; 
                $data['recipient_messaging_number'] = $this->Get_Messaging_Phone_Number() ; 
                $this->Set_Profile_By_ID($data['recipient_messaging_number']['profile_id']) ;
                $data['recipient_profile_record'] = $this->Get_Profile() ;

                $this->Set_User_By_ID($data['recipient_profile_record']['user_id']) ;
                $data['recipient_user_record'] = $this->Get_User() ;

                $sender_query_parameters = array(
                    'phone_number' => $incoming_reply_object->from,
                    'country_id' => $incoming_reply_object->from_country
                    ) ; 
                
                
                break ;
            case 9: // Bandwidth    
                
                // Normalize the incoming object
                $incoming_reply_object->message_sid = $incoming_reply_array['message']['id'] ; 
                    
                $incoming_reply_object->to = $incoming_reply_array['to'] ;
                $incoming_reply_object->from = $incoming_reply_array['message']['from'] ;
                $incoming_reply_object->from_country = 'US' ;
                
                $incoming_reply_object->message_content = $incoming_reply_array['message']['text'] ;
                $incoming_reply_object->media = $incoming_reply_array['message']['media'] ;
                $incoming_reply_object->segment_count = $incoming_reply_array['message']['segmentCount'] ;
                
                
                // Check to see if a message record exists already, and skip rest of script if it does
                $additional_parameters = array(
                    'set_by' => 'message_sid'
                    ) ; 
                $existing_message = $this->Set_Message_By_ID($incoming_reply_object->message_sid,$additional_parameters)->Get_Message() ; 

                if ($existing_message != 'error') {
                    $continue = 0 ; 
                    }
                
                
                if ($continue == 1) {
                    
                    // Identify the recipient (a User Profile)
                    $recipient_query_parameters = array(
                        'filter_by_phone_number_messaging' => $incoming_reply_object->to
                        ) ; 

                    $this->Set_Messaging_Phone_Number($recipient_query_parameters) ; 
                    $data['recipient_messaging_number'] = $this->Get_Messaging_Phone_Number() ; 
                    $this->Set_Profile_By_ID($data['recipient_messaging_number']['profile_id']) ;
                    $data['recipient_profile_record'] = $this->Get_Profile() ;

                    $this->Set_User_By_ID($data['recipient_profile_record']['user_id']) ;
                    $data['recipient_user_record'] = $this->Get_User() ;

                    $sender_query_parameters = array(
                        'phone_number' => $incoming_reply_object->from,
                        'country_id' => $incoming_reply_object->from_country
                        ) ; 


                    $media_set = array() ; 

                    // Upload files to Uploadcare
                    if ($incoming_reply_object->media) {

                        $file = new File() ; 
                        $file->Connect() ;                    

                        foreach ($incoming_reply_object->media as $media_url) {

                            $media_url_parsed = System_Config::Config_Parse_URL($media_url) ; 
                            $media_url_build = $media_url_parsed['scheme'].'://'.BANDWIDTH_API_TOKEN.':'.BANDWIDTH_API_SECRET.'@'.$media_url_parsed['url_no_scheme'] ;    



                            switch ($media_url_parsed['extension']) {
                                case 'smil':

                                    break ; 
                                default:   

                                    $file->API_Upload_File_From_URL($media_url_build) ;

                                    $new_url = $file->Get_File_Location() ; 

//                                    error_log($new_url) ; 

                                    $file_info = $file->Get_File_Info() ; 

//                                    error_log(json_encode($file_info)) ; 

                                    $file_upload_result = array(
                                        'url_full' => $new_url,
                                        'extension' => $media_url_parsed['extension']
                                        ) ;  

                                    switch ($media_url_parsed['extension']) {
                                        case 'jpg':
                                        case 'png':
                                            $file_upload_result['type'] = 'image' ; 
                                            break ; 
                                        }

                                    $media_set[] = $file_upload_result ; 
                                }

                            }               
                        }

                        $incoming_reply_object->media_set = json_encode($media_set) ;
                    
                    }
                
                break ; 
            }
        
             
        if ($continue == 1) {
            
            $sender_account = new Account() ; 
            $sender_account->Set_User_By_Phone_Number($sender_query_parameters) ; 
            $data['sender_user_record'] = $sender_account->Get_User() ;

            // Prepare Contact class...
            $contact = new Contact($data['recipient_profile_record']['user_id']) ; 
            $contact->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ; 
            $contact->Set_Contact_By_Phone_Number($sender_query_parameters) ; 
            $data['contact_record'] = $contact->Get_Contact() ;        

            $data['sender_phone_number'] = $contact->Action_Validate_Phone_Number($sender_query_parameters['phone_number'],$sender_query_parameters)->Get_Phone_Number_Validation() ;



            // Find most recent message delivered to this phone number...
            $recipient_campaign_query_options['asset_type'] = 18 ; // SMS message
            $recipient_campaign_query_options['phone_number_international_recipient'] = $sender_query_parameters['phone_number'] ; 
            $recipient_campaign_query_options['profile_id'] = $data['recipient_profile_record']['profile_id'] ; 
            $recipient_campaign_query_options['delivery_type'] = array('to','cc','bcc') ; 
            $campaigns = $this->Retrieve_Recipient_Campaign_List($recipient_campaign_query_options) ;  
            $data['campaign_list'] = $campaigns['results'] ;

            if ($campaigns['result_count'] > 0) {
                $data['delivery_latest'] = $campaigns['results'][0] ;
                $data['delivery_latest']['structured_data_campaigns_01'] = json_decode($data['delivery_latest']['structured_data_campaigns_01'],1) ;             
                $delivery_asset = new Asset() ; 
                $delivery_asset->Set_Asset_By_ID($data['delivery_latest']['delivery_asset_id']) ; 
                $data['delivery_latest']['delivery_asset'] = $delivery_asset->Get_Asset() ;            
                } else {
                    $data['delivery_latest'] = 'new' ;
                    }



            // Does phone_number_sender belong to a user?
            switch ($data['sender_user_record']) {
                case 'error':  // %L1% Does sender phone_number belong to a user? NO    
                    $data['logic_result'] = '%L1%' ;

                    // Is phone_number_sender a contact belonging to recipient profile_id?
                    switch ($data['contact_record']) {
                        case 'error': // %L3% <- %L1% Is phone_number_sender a contact belonging to recipient profile_id? NO
                            $data['logic_result'] = '%L3% <- %L1%' ;


                            // Add new contact to CRM
                            $data['new_contact_input'] = array(
                                'update_duplicates' => 'true',
                                'first_name' => 'Unknown',
                                'phone_number' => $data['sender_phone_number']['local'],
                                'phone_country_dialing_code' => $data['sender_phone_number']['phone_country_dialing_code'],
                                'country_id' => $sender_query_parameters['country_id']
                                ) ;

                            $contact->Action_Create_Contact($data['new_contact_input']) ;
                            $data['new_contact'] = $contact->Get_Response() ; 


                            if ($data['new_contact']['result'] == 'success') {

                                $data['record_reply'] = 'yes' ; 

                                $data['contact_record'] = $contact->Get_Contact() ; 
                                $note_input = array(
                                    'note_title' => 'New Contact from Incoming Text',
                                    'note_content' => 'This contact was added to your records automatically when they sent a text message to your account: <br><hr><i>'.$incoming_reply_object->message_content.'</i>'
                                    ) ; 
                                $contact->Action_Update_Contact_Note($note_input) ;
                                $data['note_result'] = $contact->Get_Response() ; 


                                $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                $add_to_list->Set_Asset_By_ID($data['recipient_profile_record']['profile_contact_delivery_asset_id']) ;

                                $member_input['contact_id'] = $data['contact_record']['contact_id'] ; 
                                $add_to_list->Action_Asset_Member_Process($member_input) ;

                                $new_member = $add_to_list->Get_Asset_Member() ; 
                                $new_member_id = $new_member['member']['parent']['insert_id'] ; // Bug ID: 680820379873647/1112697644594663  This array name is probably incorrect

                                $campaign_options = array(
                                    'asset_id' => 0,
                                    'delivery_asset_id' => $data['recipient_profile_record']['profile_contact_delivery_asset_id'],
                                    'campaign_status_id' => 8, // Complete
                                    'automation_status_id' => 8 // Complete
                                    ) ; 


                                // Bug ID: 680820379873647/1112697644594663  This needs to be fixed to member_id
                                $campaign_options['recipients'] = array(
                                    'incoming' =>  array(
                                        'delivery_list_custom' => array(
                                            'member_id' => array($new_member_id)
                                            )
                                        )
                                    ) ;                            

                                $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                $message_content_input = array(
                                    'vendor_id' => $this->vendor_id,
                                    'message_sid' => $incoming_reply_object->message_sid, 
                                    'message_content' => $incoming_reply_object->message_content,
                                    'message_media' => $incoming_reply_object->media_set,
                                    'segment_count' => $incoming_reply_object->segment_count
                                    ) ; 


                                // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                $update_message_item = array(
                                    'message_status_id' => 8, // Complete
                                    'member_id' => $new_member_id,
                                    'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                    'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                    'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                    'sender_name' => $data['contact_record']['full_name'],
                                    'timestamp_scheduled' => TIMESTAMP,
                                    'microtimestamp_delivered' => MICRO_TIMESTAMP
                                    ) ; 

                                }


                            break ;
                        default: // %L4% <- %L1% Is phone_number_sender a contact belonging to recipient profile_id? YES
                            $data['logic_result'] = '%L4% <- %L1%' ;
                            $data['record_reply'] = 'yes' ; 

                            $campaign_options = array(
                                'asset_id' => 0,
                                'campaign_status_id' => 8, // Complete
                                'automation_status_id' => 8 // Complete
                                ) ;

                            $campaign_options['recipients'] = array(
                                'incoming' =>  array(
                                    'recipient_id' => array($data['contact_record']['contact_id'])
                                    )
                                ) ;
                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                            // Was most recent communication to a contact distro list?
                            switch ($data['delivery_latest']) {
                                case 'new': // %L14% <- %L4% <- %L1%  Was most recent communication to a contact distro list? NO

                                    $data['logic_result'] = '%L14% <- %L4% <- %L1%' ;

                                    $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                    $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                    $add_to_list->Set_Asset_By_ID($data['recipient_profile_record']['profile_contact_delivery_asset_id']) ;

                                    $member_input['contact_id'] = $data['contact_record']['contact_id'] ; 
                                    $add_to_list->Action_Asset_Member_Process($member_input) ;

                                    $new_member = $add_to_list->Get_Asset_Member() ; 
                                    $member_id = $new_member['member']['parent']['insert_id'] ; 

                                    $campaign_options['recipients']['incoming']['delivery_list_custom']['member_id'] = array($member_id) ; 

                                    $campaign_options['delivery_asset_id'] = $data['recipient_profile_record']['profile_contact_delivery_asset_id'] ; 

                                    break ;
                                default:  // %L15% <- %L4% <- %L1%  Was most recent communication to a contact distro list? YES
                                    $data['logic_result'] = '%L15% <- %L4% <- %L1%' ;

                                    $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 
                                    $member_id = $data['delivery_latest']['member_id'] ; 
                                    $campaign_options['recipients']['incoming']['delivery_list_custom']['member_id'] = array($member_id) ; 
                                }



                            $message_content_input = array(
                                'automation_id' => $data['automation_record']['automation_id'],
                                'vendor_id' => $this->vendor_id,
                                'message_sid' => $incoming_reply_object->message_sid, 
                                'message_content' => $incoming_reply_object->message_content,
                                'message_media' => $incoming_reply_object->media_set,
                                'segment_count' => $incoming_reply_object->segment_count
                                ) ; 


                            // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                            $update_message_item = array(
                                'message_status_id' => 8, // Complete
                                'member_id' => $member_id,
                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                'sender_name' => $data['contact_record']['full_name'],
                                'timestamp_scheduled' => TIMESTAMP,
                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                ) ; 

                            if (isset($data['delivery_latest']['message_id'])) {
                                $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                }



                        }                

                    break ; 
                default:  // %L2% Does sender phone_number belong to a user? YES    
                    $data['logic_result'] = '%L2%' ;


                    $sender_account->Set_Profile_By_User_Account($data['sender_user_record']['user_id'],$data['recipient_profile_record']['account_id']) ; 
                    $data['sender_profile_record'] = $sender_account->Get_Profile() ; 


                    // Does sender’s user_id have a profile with same account_id as recipient?
                    switch ($data['sender_profile_record']) {
                        case 'error': // %L5% <- %L2% Does sender’s user_id have a profile with same account_id as recipient?  NO
                            $data['logic_result'] = '%L5% <- %L2%' ; 


                            switch ($data['delivery_latest']['type_id']) {
                                case 20: // %L11% <- %L5% <- %L2%  Was most recent communication to a user_distro_list? YES
                                case 28: // User SERIES list

                                    $data['delivery_type'] = 'user' ;
                                    $data['logic_result'] = '%L11% <- %L5% <- %L2%' ;
                                    $data['record_reply'] = 'yes' ; 

                                    $campaign_options = array(
                                        'asset_id' => 0,
                                        'campaign_status_id' => 8, // Complete
                                        'automation_status_id' => 8 // Complete
                                        ) ;


                                    $campaign_options['recipients'] = array(
                                        'incoming' =>  array(
                                            'delivery_list_custom' => array(
                                                'member_id' => array($data['delivery_latest']['member_id'])
                                                )
                                            )
                                        ) ;
                                    $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                    // Grab most recent communication delivery_asset_id
                                    $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 


                                    // Set base message content input
                                    $message_content_input = array(
                                        'vendor_id' => $this->vendor_id,
                                        'message_sid' => $incoming_reply_object->message_sid, 
                                        'message_content' => $incoming_reply_object->message_content,
                                        'message_media' => $incoming_reply_object->media_set,
                                        'segment_count' => $incoming_reply_object->segment_count
                                        ) ; 


                                    $update_message_item = array(
                                        'message_status_id' => 8, // Complete,
                                        'member_id' => $data['delivery_latest']['member_id'],
                                        'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                        'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                        'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                        'sender_name' => $data['sender_user_record']['full_name'],
                                        'timestamp_scheduled' => TIMESTAMP,
                                        'microtimestamp_delivered' => MICRO_TIMESTAMP
                                        ) ; 

                                    if (isset($data['delivery_latest']['message_id'])) {
                                        $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                        }


                                    break ; 
                                default: // %L18% <- %L5% <- %L2%   Was most recent communication to a user_distro_list? NO

                                    $data['logic_result'] = '%L18% <- %L5% <- %L2%' ;


                                    switch ($data['recipient_user_record']['auth_level']) {
                                        case 7:
                                        case 8:
                                        case 9:    
                                        case 10: // %L12% <- %L18% <- %L5% <- %L2% Is the recipient’s user auth_level admin level? YES

                                            $data['logic_result'] = '%L12% <- %L18% <- %L5% <- %L2%' ;
                                            $data['record_reply'] = 'yes' ;

                                            // Ensure user is on default all_user user_distro_list
                                            $data['default_user_distro_list'] = $this->Set_System_Default_Value('distro_list_active_users')->Get_System_Default_Value() ; 

                                            $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                            $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                            $add_to_list->Set_Asset_By_ID($data['default_user_distro_list']) ;

                                            $member_input['user_id'] = $data['sender_user_record']['user_id'] ; 
                                            $add_to_list->Action_Asset_Member_Process($member_input) ;

                                            $new_member = $add_to_list->Get_Asset_Member() ; 
                                            $member_id = $new_member['member']['parent']['insert_id'] ; 

                                            $campaign_options = array(
                                                'asset_id' => 0,
                                                'campaign_status_id' => 8, // Complete
                                                'automation_status_id' => 8 // Complete
                                                ) ;

                                            $campaign_options['recipients'] = array(
                                                'incoming' =>  array(
                                                    'member_id' => array($member_id)
                                                    )
                                                ) ;
                                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 



                                            // Grab most recent communication delivery_asset_id
                                            $campaign_options['delivery_asset_id'] = $data['default_user_distro_list'] ; 


                                            $message_content_input = array(
                                                'vendor_id' => $this->vendor_id,
                                                'message_sid' => $incoming_reply_object->message_sid, 
                                                'message_content' => $incoming_reply_object->message_content,
                                                'message_media' => $incoming_reply_object->media_set,
                                                'segment_count' => $incoming_reply_object->segment_count
                                                ) ; 


                                            // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                            $update_message_item = array(
                                                'message_status_id' => 8, // Complete,
                                                'member_id' => $member_id,
                                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                'sender_name' => $data['sender_user_record']['full_name'],
                                                'timestamp_scheduled' => TIMESTAMP,
                                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                ) ; 

                                            if (isset($data['delivery_latest']['message_id'])) {
                                                $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                }


                                            break ; 
                                        default: // %L13% <- %L12% <- %L18% <- %L5% <- %L2%  Is the recipient’s user auth_level admin level? NO
                                            $data['logic_result'] = '%L13% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2%' ;

                                            // Is phone_number_sender a contact belonging to recipient profile_id?
                                            if ($data['contact_record'] == 'error') { // %L3% <- %L13% Is phone_number_sender a contact belonging to recipient profile_id?  NO

                                                $data['logic_result'] = '%L3% <- %L13% <- %L12% <- %L18% <- %L5% <- %L2%' ;


                                                // Add new contact to CRM
                                                $data['new_contact_input'] = array(
                                                    'update_duplicates' => 'true',
                                                    'first_name' => 'Unknown',
                                                    'phone_number' => $data['sender_phone_number']['local'],
                                                    'phone_country_dialing_code' => $data['sender_phone_number']['phone_country_dialing_code'],
                                                    'country_id' => $sender_query_parameters['country_id']
                                                    ) ;

                                                $contact->Action_Create_Contact($data['new_contact_input']) ;
                                                $data['new_contact'] = $contact->Get_Response() ; 


                                                if ($data['new_contact']['result'] == 'success') {

                                                    $data['record_reply'] = 'yes' ; 

                                                    $data['contact_record'] = $contact->Get_Contact() ; 
                                                    $note_input = array(
                                                        'note_title' => 'New Contact from Incoming Text',
                                                        'note_content' => 'This contact was added to your records automatically when they sent a text message to your account: <br><hr><i>'.$incoming_reply_object->message_content.'</i>'
                                                        ) ; 
                                                    $contact->Action_Update_Contact_Note($note_input) ;
                                                    $data['note_result'] = $contact->Get_Response() ; 


                                                    $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                                    $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                                    $add_to_list->Set_Asset_By_ID($data['recipient_profile_record']['profile_contact_delivery_asset_id']) ;

                                                    $member_input['contact_id'] = $data['contact_record']['contact_id'] ; 
                                                    $add_to_list->Action_Asset_Member_Process($member_input) ;

                                                    $new_member = $add_to_list->Get_Asset_Member() ; 
                                                    $member_id = $new_member['member']['parent']['insert_id'] ; 

                                                    $campaign_options = array(
                                                        'asset_id' => 0,
                                                        'delivery_asset_id' => $data['recipient_profile_record']['profile_contact_delivery_asset_id'],
                                                        'campaign_status_id' => 8, // Complete
                                                        'automation_status_id' => 8 // Complete
                                                        ) ; 

                                                    $campaign_options['recipients'] = array(
                                                        'incoming' =>  array(
                                                            'member_id' => array($member_id)
                                                            )
                                                        ) ;                            
                                                    $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 



                                                    $message_content_input = array(
                                                        'vendor_id' => $this->vendor_id,
                                                        'message_sid' => $incoming_reply_object->message_sid, 
                                                        'message_content' => $incoming_reply_object->message_content,
                                                        'message_media' => $incoming_reply_object->media_set,
                                                        'segment_count' => $incoming_reply_object->segment_count
                                                        ) ; 


                                                    // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                                    $update_message_item = array(
                                                        'message_status_id' => 8, // Complete,
                                                        'member_id' => $member_id,
                                                        'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                        'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                        'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                        'sender_name' => $data['contact_record']['full_name'],
                                                        'timestamp_scheduled' => TIMESTAMP,
                                                        'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                        ) ; 

                                                    }



                                                } else {  // %L4% <- %L13% Is phone_number_sender a contact belonging to recipient profile_id?  NO

                                                    $data['logic_result'] = '%L4% <- %L13% <- %L12% <- %L18% <- %L5% <- %L2%' ;
                                                    $data['record_reply'] = 'yes' ; 

                                                    $campaign_options = array(
                                                        'asset_id' => 0,
                                                        'campaign_status_id' => 8, // Complete
                                                        'automation_status_id' => 8 // Complete
                                                        ) ;

                                                    $campaign_options['recipients'] = array(
                                                        'incoming' =>  array(
                                                            'recipient_id' => array($data['contact_record']['contact_id'])
                                                            )
                                                        ) ;
                                                    $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                                    // Was most recent communication to a contact distro list?
                                                    switch ($data['delivery_latest']) {
                                                        case 'new': // %L14% <- %L4% <- %L13%  Was most recent communication to a contact distro list? NO

                                                            $data['logic_result'] = '%L14% <- %L4% <- %L13% <- %L12% <- %L18% <- %L5% <- %L2%' ;

                                                            $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                                            $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                                            $add_to_list->Set_Asset_By_ID($data['recipient_profile_record']['profile_contact_delivery_asset_id']) ;

                                                            // Adds to subscription list list (or updates existing)
                                                            $member_input['contact_id'] = $data['contact_record']['contact_id'] ; 
                                                            $add_to_list->Action_Asset_Member_Process($member_input) ;

                                                            // Gets member from the subscription list
                                                            $asset_member_query['filter_by_member_id_value'] = $member_input['contact_id'] ; 
                                                            $add_to_list->Set_Asset_Members_List($asset_member_query) ; 
                                                            $member_list = $add_to_list->Get_Asset_Members_List() ;

                                                            $campaign_options['recipients']['incoming']['delivery_list_custom']['member_id'] = array($member_list[0]['member_id']) ;
                                                            $campaign_options['delivery_asset_id'] = $data['recipient_profile_record']['profile_contact_delivery_asset_id'] ; 

                                                            break ;
                                                        default:  // %L15% <- %L4% <- %L1%  Was most recent communication to a contact distro list? YES
                                                            $data['logic_result'] = '%L15% <- %L4% <- %L13% <- %L12% <- %L18% <- %L5% <- %L2%' ;

                                                            $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 
                                                            $member_id = $data['delivery_latest']['member_id'] ;
                                                            $campaign_options['recipients']['incoming']['delivery_list_custom']['member_id'] = array($member_id) ; 
                                                        }



                                                    $message_content_input = array(
                                                        'automation_id' => $data['automation_record']['automation_id'],
                                                        'vendor_id' => $this->vendor_id,
                                                        'message_sid' => $incoming_reply_object->message_sid, 
                                                        'message_content' => $incoming_reply_object->message_content,
                                                        'message_media' => $incoming_reply_object->media_set,
                                                        'segment_count' => $incoming_reply_object->segment_count
                                                        ) ; 


                                                    // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                                    $update_message_item = array(
                                                        'message_status_id' => 8, // Complete,
                                                        'member_id' => $member_id,
                                                        'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                        'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                        'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                        'sender_name' => $data['contact_record']['full_name'],
                                                        'timestamp_scheduled' => TIMESTAMP,
                                                        'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                        ) ; 

                                                    if (isset($data['delivery_latest']['message_id'])) {
                                                        $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                        }                                                    




                                                    }
                                                }
                                        }







                            break ; 
                        default: // %L6% <- %L2% Does sender’s user_id have a profile with same account_id as recipient?  YES
                            $data['logic_result'] = '%L6% <- %L2%' ; 

                            $contact->Set_Contact_By_Phone_Number($sender_query_parameters) ; 
                            $data['contact_record'] = $contact->Get_Contact() ; 


                            // Is phone_number_sender a contact belonging to recipient profile_id?
                            switch ($data['contact_record']) {
                                case 'error': // %L7% <- %L6% <- %L2% Is phone_number_sender a contact belonging to recipient profile_id? NO
                                    $data['logic_result'] = '%L7% <- %L6% <- %L2%' ;


                                    switch ($data['delivery_latest']['type_id']) {
                                        case 23: // %L10% <- %L7% <- %L6% <- %L2%  Was most recent communication to a profile_distro_list?  YES  
                                        case 27: // series distribution list - profiles  

                                            $data['delivery_type'] = 'profile' ;
                                            $data['logic_result'] = '%L10% <- %L7% <- %L6% <- %L2%' ;
                                            $data['record_reply'] = 'yes' ; 

                                            $campaign_options = array(
                                                'asset_id' => 0,
                                                'campaign_status_id' => 8, // Complete
                                                'automation_status_id' => 8 // Complete
                                                ) ;

                                            $campaign_options['recipients'] = array(
                                                'incoming' =>  array(
                                                    'delivery_list_custom' => array(
                                                        'member_id' => array($data['delivery_latest']['member_id'])
                                                        )
                                                    )
                                                ) ;
                                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                            // Grab most recent communication delivery_asset_id
                                            $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 



                                            $message_content_input = array(
                                                'vendor_id' => $this->vendor_id,
                                                'message_sid' => $incoming_reply_object->message_sid, 
                                                'message_content' => $incoming_reply_object->message_content,
                                                'message_media' => $incoming_reply_object->media_set,
                                                'segment_count' => $incoming_reply_object->segment_count
                                                ) ; 


                                            // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                            $update_message_item = array(
                                                'message_status_id' => 8, // Complete,
                                                'member_id' => $data['delivery_latest']['member_id'],
                                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                'sender_name' => $data['sender_profile_record']['profile_display_name'],
                                                'timestamp_scheduled' => TIMESTAMP,
                                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                ) ; 

                                            if (isset($data['delivery_latest']['message_id'])) {
                                                $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                }



                                            break ;
                                        case 20: // %L11% <- %L17% <- %L7% <- %L6% <- %L2%  Was most recent communication to a user_distro_list? YES
                                        case 28: // User SERIES list

                                            $data['delivery_type'] = 'user' ;
                                            $data['logic_result'] = '%L11% <- %L17% <- %L7% <- %L6% <- %L2%' ;
                                            $data['record_reply'] = 'yes' ; 

                                            $campaign_options = array(
                                                'asset_id' => 0,
                                                'campaign_status_id' => 8, // Complete
                                                'automation_status_id' => 8 // Complete
                                                ) ;


                                            $campaign_options['recipients'] = array(
                                                'incoming' =>  array(
                                                    'delivery_list_custom' => array(
                                                        'member_id' => array($data['delivery_latest']['member_id'])
                                                        )
                                                    )
                                                ) ;                                        
                                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                            // Grab most recent communication delivery_asset_id
                                            $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 


                                            // Set base message content input
                                            $message_content_input = array(
                                                'vendor_id' => $this->vendor_id,
                                                'message_sid' => $incoming_reply_object->message_sid, 
                                                'message_content' => $incoming_reply_object->message_content,
                                                'message_media' => $incoming_reply_object->media_set,
                                                'segment_count' => $incoming_reply_object->segment_count
                                                ) ; 


                                            $update_message_item = array(
                                                'message_status_id' => 8, // Complete,
                                                'member_id' => $data['delivery_latest']['member_id'],
                                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                'sender_name' => $data['sender_user_record']['full_name'],
                                                'timestamp_scheduled' => TIMESTAMP,
                                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                ) ; 

                                            if (isset($data['delivery_latest']['message_id'])) {
                                                $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                }



                                            break ; 
                                        default: // %L18% <- %L17% <- %L7% <- %L6% <- %L2%   Was most recent communication to a user_distro_list? NO

                                            $data['logic_result'] = '%L18% <- %L17% <- %L7% <- %L6% <- %L2%' ;


                                            switch ($data['recipient_user_record']['auth_level']) {
                                                case 7:
                                                case 8:
                                                case 9:    
                                                case 10: // %L12% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2% Is the recipient’s user auth_level admin level? YES

                                                    $data['logic_result'] = '%L12% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2%' ;
                                                    $data['record_reply'] = 'yes' ;

                                                    // Ensure user is on default all_user user_distro_list
                                                    $data['default_user_distro_list'] = $this->Set_System_Default_Value('distro_list_active_users')->Get_System_Default_Value() ; 

                                                    $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                                    $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                                    $add_to_list->Set_Asset_By_ID($data['default_user_distro_list']) ;

                                                    $member_input['user_id'] = $data['sender_user_record']['user_id'] ; 
                                                    $add_to_list->Action_Asset_Member_Process($member_input) ;


                                                    $campaign_options = array(
                                                        'asset_id' => 0,
                                                        'campaign_status_id' => 8, // Complete
                                                        'automation_status_id' => 8 // Complete
                                                        ) ;

                                                    $campaign_options['recipients'] = array(
                                                        'incoming' =>  array(
                                                            'member_id' => array($data['delivery_latest']['member_id'])
                                                            )
                                                        ) ;
                                                    $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 



                                                    // Grab most recent communication delivery_asset_id
                                                    $campaign_options['delivery_asset_id'] = $data['default_user_distro_list'] ; 


                                                    $message_content_input = array(
                                                        'vendor_id' => $this->vendor_id,
                                                        'message_sid' => $incoming_reply_object->message_sid, 
                                                        'message_content' => $incoming_reply_object->message_content,
                                                        'message_media' => $incoming_reply_object->media_set,
                                                        'segment_count' => $incoming_reply_object->segment_count
                                                        ) ; 


                                                    // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                                    $update_message_item = array(
                                                        'message_status_id' => 8, // Complete,
                                                        'member_id' => $data['delivery_latest']['member_id'],
                                                        'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                        'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                        'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                        'sender_name' => $data['sender_user_record']['full_name'],
                                                        'timestamp_scheduled' => TIMESTAMP,
                                                        'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                        ) ; 

                                                    if (isset($data['delivery_latest']['message_id'])) {
                                                        $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                        }


                                                    break ; 
                                                default: // %L13% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2%  Is the recipient’s user auth_level admin level? NO
                                                    $data['logic_result'] = '%L13% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2%' ;

                                                    // Is phone_number_sender a contact belonging to recipient profile_id?
                                                    if ($data['contact_record'] == 'error') { // %L3% <- %L13% Is phone_number_sender a contact belonging to recipient profile_id?  NO

                                                        $data['logic_result'] = '%L3% <- %L13% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2%' ;


                                                        // Add new contact to CRM
                                                        $data['new_contact_input'] = array(
                                                            'update_duplicates' => 'true',
                                                            'first_name' => 'Unknown',
                                                            'phone_number' => $data['sender_phone_number']['local'],
                                                            'phone_country_dialing_code' => $data['sender_phone_number']['phone_country_dialing_code'],
                                                            'country_id' => $sender_query_parameters['country_id']
                                                            ) ;

                                                        $contact->Action_Create_Contact($data['new_contact_input']) ;
                                                        $data['new_contact'] = $contact->Get_Response() ; 


                                                        if ($data['new_contact']['result'] == 'success') {

                                                            $data['record_reply'] = 'yes' ; 

                                                            $data['contact_record'] = $contact->Get_Contact() ; 
                                                            $note_input = array(
                                                                'note_title' => 'New Contact from Incoming Text',
                                                                'note_content' => 'This contact was added to your records automatically when they sent a text message to your account: <br><hr><i>'.$incoming_reply_object->message_content.'</i>'
                                                                ) ; 
                                                            $contact->Action_Update_Contact_Note($note_input) ;
                                                            $data['note_result'] = $contact->Get_Response() ; 


                                                            $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                                            $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                                            $add_to_list->Set_Asset_By_ID($data['recipient_profile_record']['profile_contact_delivery_asset_id']) ;

                                                            $member_input['contact_id'] = $data['contact_record']['contact_id'] ; 
                                                            $add_to_list->Action_Asset_Member_Process($member_input) ;

                                                            $new_member = $add_to_list->Get_Asset_Member() ; 
                                                            $member_id = $new_member['member']['parent']['insert_id'] ; 

                                                            $campaign_options = array(
                                                                'asset_id' => 0,
                                                                'delivery_asset_id' => $data['recipient_profile_record']['profile_contact_delivery_asset_id'],
                                                                'campaign_status_id' => 8, // Complete
                                                                'automation_status_id' => 8 // Complete
                                                                ) ; 

                                                            $campaign_options['recipients'] = array(
                                                                'incoming' =>  array(
                                                                    'member_id' => array($member_id)
                                                                    )
                                                                ) ;                            
                                                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 



                                                            $message_content_input = array(
                                                                'vendor_id' => $this->vendor_id,
                                                                'message_sid' => $incoming_reply_object->message_sid, 
                                                                'message_content' => $incoming_reply_object->message_content,
                                                                'message_media' => $incoming_reply_object->media_set,
                                                                'segment_count' => $incoming_reply_object->segment_count
                                                                ) ; 


                                                            // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                                            $update_message_item = array(
                                                                'message_status_id' => 8, // Complete,
                                                                'member_id' => $member_id,
                                                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                                'sender_name' => $data['contact_record']['full_name'],
                                                                'timestamp_scheduled' => TIMESTAMP,
                                                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                                ) ; 

                                                            }



                                                        } else {  // %L4% <- %L13% Is phone_number_sender a contact belonging to recipient profile_id?  NO

                                                            $data['logic_result'] = '%L4% <- %L13% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2%' ;
                                                            $data['record_reply'] = 'yes' ; 

                                                            $campaign_options = array(
                                                                'asset_id' => 0,
                                                                'campaign_status_id' => 8, // Complete
                                                                'automation_status_id' => 8 // Complete
                                                                ) ;

                                                           $campaign_options['recipients'] = array(
                                                                'incoming' =>  array(
                                                                    'recipient_id' => array($data['contact_record']['contact_id'])
                                                                    )
                                                                ) ;
                                                            $campaign_options['recipients'] = array(
                                                                'incoming' =>  array(
                                                                    'recipient_id' => array($data['contact_record']['contact_id'])
                                                                    )
                                                                ) ;
                                                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                                            // Was most recent communication to a contact distro list?
                                                            switch ($data['delivery_latest']) {
                                                                case 'new': // %L14% <- %L4% <- %L13%  Was most recent communication to a contact distro list? NO

                                                                    $data['logic_result'] = '%L14% <- %L4% <- %L13% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2%' ;

                                                                    $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                                                    $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                                                    $add_to_list->Set_Asset_By_ID($data['recipient_profile_record']['profile_contact_delivery_asset_id']) ;

                                                                    $member_input['contact_id'] = $data['contact_record']['contact_id'] ; 
                                                                    $add_to_list->Action_Asset_Member_Process($member_input) ;

                                                                    $new_member = $add_to_list->Get_Asset_Member() ; 
                                                                    $member_id = $new_member['member']['parent']['insert_id'] ; 

                                                                    $campaign_options['delivery_asset_id'] = $data['recipient_profile_record']['profile_contact_delivery_asset_id'] ; 
                                                                    $campaign_options['recipients']['incoming']['delivery_list_custom']['member_id'] = array($member_id) ; 

                                                                    break ;
                                                                default:  // %L15% <- %L4% <- %L1%  Was most recent communication to a contact distro list? YES
                                                                    $data['logic_result'] = '%L15% <- %L4% <- %L13% <- %L18% <- %L17% <- %L7% <- %L6% <- %L2%' ;

                                                                    $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 

                                                                    $member_id = $data['delivery_latest']['member_id'] ; 
                                                                    $campaign_options['recipients']['incoming']['delivery_list_custom']['member_id'] = array($member_id) ;
                                                                }



                                                            $message_content_input = array(
                                                                'automation_id' => $data['automation_record']['automation_id'],
                                                                'vendor_id' => $this->vendor_id,
                                                                'message_sid' => $incoming_reply_object->message_sid, 
                                                                'message_content' => $incoming_reply_object->message_content,
                                                                'message_media' => $incoming_reply_object->media_set,
                                                                'segment_count' => $incoming_reply_object->segment_count
                                                                ) ; 


                                                            // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                                            $update_message_item = array(
                                                                'message_status_id' => 8, // Complete,
                                                                'member_id' => $member_id,
                                                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                                'sender_name' => $data['contact_record']['full_name'],
                                                                'timestamp_scheduled' => TIMESTAMP,
                                                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                                ) ; 

                                                            if (isset($data['delivery_latest']['message_id'])) {
                                                                $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                                }                                                    

                                                            }
                                                }
                                        }                                






                                    break ;
                                default: // %L8% <- %L6% <- %L2% Is phone_number_sender a contact belonging to recipient profile_id? YES
                                    $data['logic_result'] = '%L8% <- %L6% <- %L2%' ;

                                    switch ($data['delivery_latest']['type_id']) {
                                        case 19: // %L9% <- %L8% <- %L6% <- %L2% Contact distro List YES
                                        case 26: // Contact SERIES distribution list

                                            $data['delivery_type'] = 'contact' ;
                                            $data['logic_result'] = '%L9% <- %L8% <- %L6% <- %L2%' ;
                                            $data['record_reply'] = 'yes' ; 


                                            $campaign_options = array(
                                                'asset_id' => 0,
                                                'campaign_status_id' => 8, // Complete
                                                'automation_status_id' => 8 // Complete
                                                ) ;

                                            $member_id = $data['delivery_latest']['member_id'] ; 

                                            $campaign_options['recipients'] = array(
                                                'incoming' =>  array(
                                                    'recipient_id' => array($data['contact_record']['contact_id']),
                                                    'delivery_list_custom' => array(
                                                        'member_id' => array($member_id)
                                                        )
                                                    )
                                                ) ;
                                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                            // Grab most recent communication delivery_asset_id
                                            $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 

                                            $message_content_input = array(
                                                'automation_id' => $data['automation_record']['automation_id'],
                                                'vendor_id' => $this->vendor_id,
                                                'message_sid' => $incoming_reply_object->message_sid, 
                                                'message_content' => $incoming_reply_object->message_content,
                                                'message_media' => $incoming_reply_object->media_set,
                                                'segment_count' => $incoming_reply_object->segment_count
                                                ) ; 


                                            // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                            $update_message_item = array(
                                                'message_status_id' => 8, // Complete
                                                'member_id' => $member_id,
                                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                'sender_name' => $data['contact_record']['full_name'],
                                                'timestamp_scheduled' => TIMESTAMP,
                                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                ) ; 

                                            if (isset($data['delivery_latest']['message_id'])) {
                                                $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                }                                        



                                            break ; 
                                        case 23: // %L10% <- %L16% <- %L8% <- %L6% <- %L2%    Profile distro List YES  
                                        case 27: // series distribution list - profiles  

                                            $data['delivery_type'] = 'profile' ;
                                            $data['logic_result'] = '%L10% <- %L16% <- %L8% <- %L6% <- %L2%' ;
                                            $data['record_reply'] = 'yes' ; 

                                            $campaign_options = array(
                                                'asset_id' => 0,
                                                'campaign_status_id' => 8, // Complete
                                                'automation_status_id' => 8 // Complete
                                                ) ;

    //                                        $campaign_options['recipients'] = array(
    //                                            'incoming' =>  array(
    //                                                'member_id' => array($data['delivery_latest']['member_id'])
    //                                                )
    //                                            ) ;
                                            $campaign_options['recipients'] = array(
                                                'incoming' =>  array(
                                                    'recipient_id' => array($data['sender_profile_record']['user_id']),
                                                    'delivery_list_custom' => array(
                                                        'member_id' => array($data['delivery_latest']['member_id'])
                                                        )
                                                    )
                                                ) ;                                        
                                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                            // Grab most recent communication delivery_asset_id
                                            $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 



                                            $message_content_input = array(
                                                'vendor_id' => $this->vendor_id,
                                                'message_sid' => $incoming_reply_object->message_sid, 
                                                'message_content' => $incoming_reply_object->message_content,
                                                'message_media' => $incoming_reply_object->media_set,
                                                'segment_count' => $incoming_reply_object->segment_count
                                                ) ; 


                                            // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                            $update_message_item = array(
                                                'message_status_id' => 8, // Complete,
                                                'member_id' => $data['delivery_latest']['member_id'],
                                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                'sender_name' => $data['sender_profile_record']['profile_display_name'],
                                                'timestamp_scheduled' => TIMESTAMP,
                                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                ) ; 

                                            if (isset($data['delivery_latest']['message_id'])) {
                                                $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                }



                                            break ;
                                        case 20: // %L11% <- %L17% <- %L16% <- %L8% <- %L6% <- %L2%  User distro List YES
                                        case 28: // User SERIES list

                                            $data['delivery_type'] = 'user' ;
                                            $data['logic_result'] = '%L11% <- %L17% <- %L16% <- %L8% <- %L6% <- %L2%' ;
                                            $data['record_reply'] = 'yes' ; 

                                            $campaign_options = array(
                                                'asset_id' => 0,
                                                'campaign_status_id' => 8, // Complete
                                                'automation_status_id' => 8 // Complete
                                                ) ;

    //                                        $campaign_options['recipients'] = array(
    //                                            'incoming' =>  array(
    //                                                'member_id' => array($data['delivery_latest']['member_id'])
    //                                                )
    //                                            ) ;
                                            $campaign_options['recipients'] = array(
                                                'incoming' =>  array(
                                                    'recipient_id' => array($data['sender_profile_record']['user_id']),
                                                    'delivery_list_custom' => array(
                                                        'member_id' => array($data['delivery_latest']['member_id'])
                                                        )
                                                    )
                                                ) ;
                                            $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 


                                            // Grab most recent communication delivery_asset_id
                                            $campaign_options['delivery_asset_id'] = $data['delivery_latest']['delivery_asset_id'] ; 


                                            // Set base message content input
                                            $message_content_input = array(
                                                'vendor_id' => $this->vendor_id,
                                                'message_sid' => $incoming_reply_object->message_sid, 
                                                'message_content' => $incoming_reply_object->message_content,
                                                'message_media' => $incoming_reply_object->media_set,
                                                'segment_count' => $incoming_reply_object->segment_count
                                                ) ; 


                                            $update_message_item = array(
                                                'message_status_id' => 8, // Complete,
                                                'member_id' => $data['delivery_latest']['member_id'],
                                                'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                'sender_name' => $data['sender_user_record']['full_name'],
                                                'timestamp_scheduled' => TIMESTAMP,
                                                'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                ) ; 

                                            if (isset($data['delivery_latest']['message_id'])) {
                                                $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                }



                                            break ; 
                                        default: // %L18% <- %L17% <- %L16% <- %L8% <- %L6% <- %L2%   Was most recent communication to a user_distro_list? NO

                                            $data['logic_result'] = '%L18% <- %L17% <- %L16% <- %L8% <- %L6% <- %L2%' ;
                                            $data['record_reply'] = 'yes' ; 

                                            switch ($data['recipient_user_record']['auth_level']) {
                                                case 7:
                                                case 8:
                                                case 9:    
                                                case 10: // %L12% <- %L18% <- %L17% <- %L16% <- %L8% <- %L6% <- %L2% Is the recipient’s user auth_level admin level? YES

                                                    $data['logic_result'] = '%L12% <- %L18% <- %L17% <- %L16% <- %L8% <- %L6% <- %L2%' ;

                                                    // Ensure user is on default all_user user_distro_list
                                                    $data['default_user_distro_list'] = $this->Set_System_Default_Value('distro_list_active_users')->Get_System_Default_Value() ; 

                                                    $add_to_list = new Asset($data['recipient_profile_record']['user_id']) ; 
                                                    $add_to_list->Set_Account_By_ID($data['recipient_profile_record']['account_id']) ;
                                                    $add_to_list->Set_Asset_By_ID($data['default_user_distro_list']) ;

                                                    $member_input['user_id'] = $data['sender_user_record']['user_id'] ; 
                                                    $add_to_list->Action_Asset_Member_Process($member_input) ;

                                                    $new_member = $add_to_list->Get_Asset_Member() ; 
                                                    $member_id = $new_member['member']['parent']['insert_id'] ; 

                                                    $campaign_options = array(
                                                        'asset_id' => 0,
                                                        'campaign_status_id' => 8, // Complete
                                                        'automation_status_id' => 8 // Complete
                                                        ) ;

    //                                                $campaign_options['recipients'] = array(
    //                                                    'incoming' =>  array(
    //                                                        'member_id' => array($member_id)
    //                                                        )
    //                                                    ) ;
                                                    $campaign_options['recipients'] = array(
                                                        'incoming' =>  array(
                                                            'recipient_id' => array($data['recipient_user_record']['user_id']),
                                                            'delivery_list_custom' => array(
                                                                'member_id' => array($member_id)
                                                                )
                                                            )
                                                        ) ;
                                                    $campaign_options['messaging_number_id'] = $data['recipient_messaging_number']['messaging_number_id'] ; 



                                                    // Grab most recent communication delivery_asset_id
                                                    $campaign_options['delivery_asset_id'] = $data['default_user_distro_list'] ; 


                                                    $message_content_input = array(
                                                        'vendor_id' => $this->vendor_id,
                                                        'message_sid' => $incoming_reply_object->message_sid, 
                                                        'message_content' => $incoming_reply_object->message_content,
                                                        'message_media' => $incoming_reply_object->media_set,
                                                        'segment_count' => $incoming_reply_object->segment_count
                                                        ) ; 


                                                    // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                                                    $update_message_item = array(
                                                        'message_status_id' => 8, // Complete,
                                                        'member_id' => $member_id,
                                                        'phone_number_international_recipient' => $data['recipient_messaging_number']['phone_number_messaging'],
                                                        'recipient_name' => $data['recipient_profile_record']['profile_display_name'],
                                                        'phone_number_international_sender' => $data['sender_phone_number']['international'],
                                                        'sender_name' => $data['sender_user_record']['full_name'],
                                                        'timestamp_scheduled' => TIMESTAMP,
                                                        'microtimestamp_delivered' => MICRO_TIMESTAMP
                                                        ) ; 

                                                    if (isset($data['delivery_latest']['message_id'])) {
                                                        $update_message_item['conversation_id'] = $data['delivery_latest']['message_id'] ; 
                                                        }


                                                    break ; 
                                                default: // %L13% Is the recipient’s user auth_level admin level? NO
                                                    $data['logic_result'] = '%L13%' ;

                                                    // Is phone_number_sender a contact belonging to recipient profile_id?
                                                    if ($data['contact_record'] == 'error') { // %L3% <- %L13% Is phone_number_sender a contact belonging to recipient profile_id?  NO
                                                        $data['logic_result'] = '%L14% <- %L3% <- %L13%' ;

                                                        } else {  // %L4% <- %L13% Is phone_number_sender a contact belonging to recipient profile_id?  NO

                                                            $data['logic_result'] = '%L14% <- %L4% <- %L13%' ;

                                                            }
                                                }
                                        }

                                }                        

                        }

                }

//            error_log('logic result') ; 
//            error_log($data['logic_result']) ;

            // Create a campaign / automation / message for the reply in the database...    
            // This is where we are
            if ($data['record_reply'] == 'yes') {


    //            $campaign_options['asset_id'] = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'sms_placeholder_incoming')['value'] ; 
                $campaign_options['delivery_trigger'] = 'delivery_list_custom' ;

    //            print_r($campaign_options) ; 

                // Create the reply campaign
                $this->Action_Create_Campaign($campaign_options) ; 
                $this->Action_Create_Message_Recipients() ; 

                $data['create_recipients'] = $this->Get_Test_Data() ;
                $data['campaign_record'] = $this->Get_Campaign() ;
                $data['automation_record'] = $this->Get_Automation() ;

                // Get automation_id associated w/ the new reply campaign and add it to the message_content_input array
                $message_content_input['automation_id'] = $data['automation_record']['automation_id'] ; 

                $data['message_id'] = $this->Get_Message_ID() ; 
                $this->Set_Message_By_ID() ; 


                // EMERGENCY STOP
                $data['message_event'] = $this->Action_Process_Keyword_Reply($message_content_input,$data) ; 


                // Create reply message content
                $this->Action_Create_Message_Content($message_content_input) ;                

                // Get the new message_content_id
                // We need a better way to set the Message_ID here.  For now, I set it as the final thing in Action_Create_Message_Recipients()
                $data['message_content_id'] = $this->Get_Message_Content_ID() ; 
                $update_message_item['message_content_id'] = $data['message_content_id'] ;
                $update_message_item['message_sid'] = $incoming_reply_object->message_sid ;

                // Update the message recipient with the new message_content_id and sender / recipient info
                $data['message_update'] = $this->Update_Message_Recipient($update_message_item) ; 


                // Mark the automation item Complete for the incoming message
                $automation_update_query_options = array(
                    'automation_status_id' => 8, // Complete 
                    ) ; 
                $data['automation_update'] = $this->Update_Automation($automation_update_query_options) ; 
                $this->Set_Automation_By_ID() ;
                $data['automation_record'] = $this->Get_Automation() ;
                }        


            $this->test_data = $data ; 

            } // End of CONTINUE block after testing for existing message
        
        return $this ; 
        }

    
    // Process one word keyword replies like STOP, REMOVE, SUBSCRIBE, etc
    // Give this function the $data array compiled in the Action_Process_Reply model
    // https://support.bandwidth.com/hc/en-us/articles/360005440954
    public function Action_Process_Keyword_Reply($message_content_array,$data = array()) {
        
        $data['message_event'] = $message_content_array ; 
        $data['profile_record'] = $this->Get_Profile() ; 
        
        $message_content_array['message_content'] = trim(strtoupper($message_content_array['message_content'])) ; // Convert the entire message to uppercase so the switch is guaranteed to work
        
        switch ($message_content_array['message_content']) {

            // https://support.twilio.com/hc/en-us/articles/223134027-Twilio-support-for-opt-out-keywords-SMS-STOP-filtering-
            case 'STOP':
            case 'STOPALL':
            case 'CANCEL':
            case 'END':
            case 'QUIT':
                
                // These should all be treated as global opt-outs for any communication 
                // This could impact messages sent to contacts, team, and users
                
                if ($data['delivery_latest'] != 'new') {
                    
                    // Update member's metadata on the delivery list
                    $delivery_asset = new Asset() ; 
                    $delivery_asset->Set_Asset_By_ID($data['delivery_latest']['delivery_asset_id']) ; 
                    $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                    
                    $member_list_options['filter_by_member_id'] = $data['delivery_latest']['member_id'] ; 
                    $member_list_options['filter_by_user_id'] = 'no' ;
                    $member_list_options['filter_by_account_id'] = 'no' ;
                    $delivery_asset->Set_Asset_Members_List($member_list_options) ;
                    
                    $member_list = $delivery_asset->Get_Asset_Members_List() ;       
                    $data['member_record'] = $member_list[0] ; 
                    $data['subscribe_asset'] = $delivery_asset->Get_Asset() ; 
                    
                    
                    $options_array['asset_id'] = $data['delivery_asset']['distribution_sublist_sms']['asset_id'] ; 
                    $options_array['metadata_id'] = 47 ; // Unsubscribed
                    $delivery_asset->Action_Asset_Member_Update($data['member_record']['member_id_distribution_sublist_sms'],$options_array) ; 
                    // END: Update member metadata
                    
                    

                    switch ($data['delivery_latest']['type_id']) {
                        case 19: // Contact distribution list
                        case 26: // Contact SERIES distribution list
                            
                            $contact = new Contact() ; 
                            $contact->Set_User_ID($data['contact_record']['user_id'])->Set_Account_ID($data['contact_record']['account_id']) ; 
                            $contact->Set_Contact_ID($data['contact_record']['contact_id']) ; 

                            $contact_input = array(
                                'phone_number_international' => $data['contact_record']['phone_number_compiled']['international']
                                ) ; 

                            $contact->Action_Update_Contact_Restriction($contact_input) ;                            
                            
                            $message_event_query_options = array(
                                'message_id' => $data['message_id'],
                                'event' => 'recipient_restriction',
                                'event_message' => 'Recipient has blocked '.$data['contact_record']['phone_number_compiled']['international_pretty'].' from 
                                    messages sent by '.$data['recipient_messaging_number']['phone_number_messaging_compiled']['international_pretty']
                                ) ; 

                            $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;
                            break ; 
                            
                        case 20: // Users
                        case 28: // User SERIES list
                            
                            $find_list_asset_options['filter_by_type_id'] = 'no' ; 
                            $find_list_asset_options['filter_by_asset_id'] = 'no' ; 
                            $find_list_asset_options['filter_by_member_id'] = array($data['delivery_latest']['member_id']) ;
                            $find_list_asset_options['skip_asset_mapping'] = 'yes' ; 

                            $member = new Asset() ; 
                            $member->Set_Asset_Members_List($find_list_asset_options) ;
                            $member_list = $member->Get_Asset_Members_List() ;                                

                            $user = new User() ; 
                            $user->Set_User_By_ID($member_list[0]['member_id_value']) ; 
                            $user_record = $user->Get_User() ; 
                            
                            $restriction_input = array(
                                'phone_number_international' => $data['sender_phone_number']['international']
                                ) ; 

                            $user->Action_Update_User_Restriction($restriction_input) ;                            
                            
                            $message_event_query_options = array(
                                'message_id' => $data['message_id'],
                                'event' => 'recipient_restriction',
                                'event_message' => 'Recipient has blocked '.$data['sender_phone_number']['international_pretty'].' from 
                                    messages sent by '.$data['recipient_messaging_number']['phone_number_messaging_compiled']['international_pretty']
                                ) ; 

                            $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;
                            break ; 
                            
                        case 23: // User Profiles
                        case 27: // series distribution list - profiles  
                            
                            $find_list_asset_options['filter_by_type_id'] = 'no' ; 
                            $find_list_asset_options['filter_by_asset_id'] = 'no' ; 
                            $find_list_asset_options['filter_by_member_id'] = array($data['delivery_latest']['member_id']) ;
                            $find_list_asset_options['skip_asset_mapping'] = 'yes' ; 

                            $member = new Asset() ; 
                            $member->Set_Asset_Members_List($find_list_asset_options) ;
                            $member_list = $member->Get_Asset_Members_List() ;                                                                 
                            
                            $profile = new Account() ; 
                            $profile->Set_Profile_By_ID($member_list[0]['member_id_value']) ; 
                            $profile->Set_User_By_Profile_ID() ;  

                            $restriction_input = array(
                                'phone_number_international' => $data['sender_phone_number']['international']
                                ) ; 

                            $profile->Action_Update_Profile_Restriction($restriction_input) ;                            
                            
                            $message_event_query_options = array(
                                'message_id' => $data['message_id'],
                                'event' => 'recipient_restriction',
                                'event_message' => 'Recipient has blocked '.$data['sender_phone_number']['international_pretty'].' from 
                                    messages sent by '.$data['recipient_messaging_number']['phone_number_messaging_compiled']['international_pretty']
                                ) ; 

                            $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;
                            break ;                            
                        } 
                         
                    
                    // Determine the parent delivery asset if there was one for personalization of messages
                    if ($data['delivery_asset']['parent_asset_id']) {
                        $delivery_asset->Set_Asset_By_ID($data['delivery_asset']['parent_asset_id']) ; 
                        $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                        }
                    
                    
                    // Add message event
                    $data['message_id'] = $this->Get_Message_ID() ;
                                        
                    $message_event_query_options = array(
                        'message_id' => $data['message_id'],
                        'event' => 'unsubscribed',
                        'event_message' => 'Recipient unsubscribed from '.$data['delivery_asset']['asset_title']
                        ) ; 
                    
                    $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;                    
                    }

                break ; 
            case 'START':
//            case 'YES':    
            case 'UNSTOP':
                
                if ($data['delivery_latest'] != 'new') {
                    
                    $delivery_asset = new Asset() ; 
                    $delivery_asset->Set_Asset_By_ID($data['delivery_latest']['delivery_asset_id']) ; 
                    $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                    
                    $member_list_options['filter_by_member_id'] = $data['delivery_latest']['member_id'] ; 
                    $member_list_options['filter_by_user_id'] = 'no' ;
                    $member_list_options['filter_by_account_id'] = 'no' ;
                    $delivery_asset->Set_Asset_Members_List($member_list_options) ; 
                    
                    $member_list = $delivery_asset->Get_Asset_Members_List() ;       
                    $data['member_record'] = $member_list[0] ; 
                    $data['subscribe_asset'] = $delivery_asset->Get_Asset() ; 

                    $options_array['asset_id'] = $data['subscribe_asset']['distribution_sublist_sms']['asset_id'] ; 
                    $options_array['metadata_id'] = 46 ; // SUBSCRIBED
                    $delivery_asset->Action_Asset_Member_Update($data['member_record']['member_id_distribution_sublist_sms'],$options_array) ; 
                    
                    // Repull the subscriber list again to check for updates...
                    $member_list_options['filter_by_user_id'] = 'no' ;
                    $member_list_options['filter_by_account_id'] = 'no' ; 
                    $member_list_options['filter_by_member_id'] = $data['delivery_latest']['member_id'] ; 
                    $delivery_asset->Set_Asset_Members_List($member_list_options) ; 
                    

                    // REMOVE FROM DO NOT CONTACT LIST...
                    switch ($data['delivery_latest']['type_id']) {
                        case 19: // Contact distribution list
                        case 26: // Contact SERIES distribution list
                            
                            $contact = new Contact() ; 
                            $contact->Set_User_ID($data['contact_record']['user_id'])->Set_Account_ID($data['contact_record']['account_id']) ; 
                            $contact->Set_Contact_ID($data['contact_record']['contact_id']) ; 

                            $contact_input = array(
                                'phone_number_international' => $data['contact_record']['phone_number_compiled']['international']
                                ) ; 

                            $contact->Action_Update_Contact_Restriction($contact_input,0) ;                            
                            
                            $message_event_query_options = array(
                                'message_id' => $data['message_id'],
                                'event' => 'recipient_restriction',
                                'event_message' => 'Recipient unblocked '.$data['contact_record']['phone_number_compiled']['international_pretty'].' from 
                                    messages sent by '.$data['recipient_messaging_number']['phone_number_messaging_compiled']['international_pretty']
                                ) ; 

                            $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;                            
                            break ; 
                            
                        case 20: // Users
                        case 28: // User SERIES list
                            
                            $find_list_asset_options['filter_by_type_id'] = 'no' ; 
                            $find_list_asset_options['filter_by_asset_id'] = 'no' ; 
                            $find_list_asset_options['filter_by_member_id'] = array($data['delivery_latest']['member_id']) ;
                            $find_list_asset_options['skip_asset_mapping'] = 'yes' ; 

                            $member = new Asset() ; 
                            $member->Set_Asset_Members_List($find_list_asset_options) ;
                            $member_list = $member->Get_Asset_Members_List() ;                                

                            $user = new User() ; 
                            $user->Set_User_By_ID($member_list[0]['member_id_value']) ; 
                            $user_record = $user->Get_User() ; 
                            
                            $restriction_input = array(
                                'phone_number_international' => $data['sender_phone_number']['international']
                                ) ; 

                            $user->Action_Update_User_Restriction($restriction_input,0) ;                            
                            
                            $message_event_query_options = array(
                                'message_id' => $data['message_id'],
                                'event' => 'recipient_restriction',
                                'event_message' => 'Latest Member: '.$data['delivery_latest']['member_id'].' Recipient unblocked '.$data['sender_phone_number']['international_pretty'].' from 
                                    messages sent by '.$data['recipient_messaging_number']['phone_number_messaging_compiled']['international_pretty']
                                ) ; 

                            $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;
                            break ; 
                            
                        case 23: // User Profiles
                        case 27: // series distribution list - profiles      
                            
                            $find_list_asset_options['filter_by_type_id'] = 'no' ; 
                            $find_list_asset_options['filter_by_asset_id'] = 'no' ; 
                            $find_list_asset_options['filter_by_member_id'] = array($data['delivery_latest']['member_id']) ;
                            $find_list_asset_options['skip_asset_mapping'] = 'yes' ; 

                            $member = new Asset() ; 
                            $member->Set_Asset_Members_List($find_list_asset_options) ;
                            $member_list = $member->Get_Asset_Members_List() ;                                                                 
                            
                            $profile = new Account() ; 
                            $profile->Set_Profile_By_ID($member_list[0]['member_id_value']) ; 
                            $profile->Set_User_By_Profile_ID() ;  

                            $restriction_input = array(
                                'phone_number_international' => $data['sender_phone_number']['international']
                                ) ; 

                            $profile->Action_Update_Profile_Restriction($restriction_input,0) ;                            
                            
                            $message_event_query_options = array(
                                'message_id' => $data['message_id'],
                                'event' => 'recipient_restriction',
                                'event_message' => 'Recipient unblocked '.$data['sender_phone_number']['international_pretty'].' from 
                                    messages sent by '.$data['recipient_messaging_number']['phone_number_messaging_compiled']['international_pretty']
                                ) ; 

                            $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;
                            break ;                             
                        }
                    
                    
                    
                    // Determine the parent delivery asset if there was one for personalization of messages
                    if ($data['delivery_asset']['parent_asset_id']) {
                        $delivery_asset->Set_Asset_By_ID($data['delivery_asset']['parent_asset_id']) ; 
                        $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                        }
                    

                    // Send Recipient Confirmation:
                    $response_asset_id = $this->Set_System_Default_Value('sms_list_confirmation')->Get_System_Default_Value() ;
                    
                    $response_campaign_options = array(
                        'asset_id' => $response_asset_id, // Confirm Unsubscribe
                        'delivery_asset_id' => $data['delivery_latest']['delivery_asset_id'],
                        'campaign_title' => 'START Auto-response to '.$data['delivery_latest']['member_id'],
                        'timestamp_next_action' => TIMESTAMP,
                        'messaging_number_id' => $data['profile_record']['messaging_number_id'],
                        'automation_status_name' => 'scheduled',
                        'delivery_trigger' => 'delivery_list_custom',
                        'recipients' => array(
                            'to' => array(
                                'delivery_list_custom' => array(
                                    'member_id' => array($data['delivery_latest']['member_id']),
                                    'sms_metadata_id' => array(46) // Subscribed
                                    ),
                                ),
                            ), 
                        'personalization_overrides' => array(
                            'asset_record' => array(
                                'asset_title' => $data['delivery_asset']['asset_title']
                                )
                            )
                        ) ; 
                    
                    if ($data['delivery_latest']['structured_data_campaigns_01']['messaging_number_id']) {
                        $response_campaign_options['messaging_number_id'] = $data['delivery_latest']['structured_data_campaigns_01']['messaging_number_id'] ; 
                        } 
                    

                    $this->Action_Create_Campaign($response_campaign_options) ;
                    $response_campaign = $this->Get_Response() ;
                        
                    if ($response_campaign['result'] == 'success') {
                        
                        // Archive new campaign
                        $campaign_record = $this->Get_Campaign() ;
                        $campaign_update_query = array(
                            'campaign_id' => $campaign_record['campaign_id'],
                            'archive_campaign' => 'yes'
                            ) ; 
                        $this->Action_Update_Campaign($campaign_update_query) ;                         
                        
                        $response_campaign['automation'] = $this->Get_Automation() ;     
                        $this->Action_Trigger_Automation_Queue($response_campaign['automation']['automation_id']) ;
                        }
                    // END: Recipient Confirmation                    
                    
                    
                    // Add message event
                    $data['message_id'] = $this->Get_Message_ID() ; 
                    
                    $message_event_query_options = array(
                        'message_id' => $data['message_id'],
                        'event' => 'subscribed',
                        'event_message' => 'Recipient confirmed subscription to '.$data['delivery_asset']['asset_title']
                        ) ; 
                    
                    $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;                      
                    
                    }
                
                break ;
            case '?': // Our replacement for INFO since Twilio INFO doesn't deliver to our server
            case 'HELP':
            case 'INFO':
                
                if ($data['delivery_latest'] != 'new') {  
                    
                    $delivery_asset = new Asset() ; 
                    $delivery_asset->Set_Asset_By_ID($data['delivery_latest']['delivery_asset_id']) ; 
                    $data['delivery_asset'] = $delivery_asset->Get_Asset() ;                     
                    
                    // Determine the parent delivery asset if there was one for personalization of messages
                    if ($data['delivery_asset']['parent_asset_id']) {
                        $delivery_asset->Set_Asset_By_ID($data['delivery_asset']['parent_asset_id']) ; 
                        $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                        }
                    
                    
                    // Send Recipient Confirmation:
                    $response_asset_id = $this->Set_System_Default_Value('sms_list_info')->Get_System_Default_Value() ;
                    
                    $response_campaign_options = array(
                        'asset_id' => $response_asset_id, // Confirm Unsubscribe
                        'delivery_asset_id' => $data['delivery_latest']['delivery_asset_id'],
                        'campaign_title' => 'HELP Auto-response to '.$data['delivery_latest']['member_id'],
                        'timestamp_next_action' => TIMESTAMP,
                        'messaging_number_id' => $data['profile_record']['messaging_number_id'],
                        'automation_status_name' => 'scheduled',
                        'delivery_trigger' => 'delivery_list_custom',
                        'recipients' => array(
                            'to' => array(
                                'delivery_list_custom' => array(
                                    'member_id' => array($data['delivery_latest']['member_id']),
                                    'sms_metadata_id' => array(46,47,48,49,51) // All Statuses
                                    ),
                                ),
                            ),
                        'personalization_overrides' => array(
                            'asset_record' => array(
                                'asset_title' => $data['delivery_asset']['asset_title']
                                )
                            )
                        ) ; 
                    
                    if ($data['delivery_latest']['structured_data_campaigns_01']['messaging_number_id']) {
                        $response_campaign_options['messaging_number_id'] = $data['delivery_latest']['structured_data_campaigns_01']['messaging_number_id'] ; 
                        } 

                    
                    $this->Action_Create_Campaign($response_campaign_options) ;
                    $response_campaign = $this->Get_Response() ;
                        
                    if ($response_campaign['result'] == 'success') {
                        
                        // Archive new campaign
                        $campaign_record = $this->Get_Campaign() ;
                        $campaign_update_query = array(
                            'campaign_id' => $campaign_record['campaign_id'],
                            'archive_campaign' => 'yes'
                            ) ; 
                        $this->Action_Update_Campaign($campaign_update_query) ;                         
                        
                        
                        $response_campaign['automation'] = $this->Get_Automation() ;     
                        $trigger = $this->Action_Trigger_Automation_Queue($response_campaign['automation']['automation_id']) ;
                        }
                    // END: Recipient Confirmation                 
                    }
                
                
                break ; 
            case 'REMOVE': // This is unsubscribing from list
            case 'UNSUBSCRIBE':
                
                // If the message content is REMOVE, then unsubscribe them from the sms subslist
                if ($data['delivery_latest'] != 'new') {
                    
                    
                    // Update member's metadata on the delivery list
                    $delivery_asset = new Asset() ; 
                    $delivery_asset->Set_Asset_By_ID($data['delivery_latest']['delivery_asset_id']) ; 
                    $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                    
                    $member_list_options['filter_by_member_id'] = $data['delivery_latest']['member_id'] ; 
                    $member_list_options['filter_by_user_id'] = 'no' ;
                    $member_list_options['filter_by_account_id'] = 'no' ;
                    $delivery_asset->Set_Asset_Members_List($member_list_options) ;
                    
                    $member_list = $delivery_asset->Get_Asset_Members_List() ;       
                    $data['member_record'] = $member_list[0] ; 
                    $data['subscribe_asset'] = $delivery_asset->Get_Asset() ; 
                    
                    
                    $options_array['asset_id'] = $data['delivery_asset']['distribution_sublist_sms']['asset_id'] ; 
                    $options_array['metadata_id'] = 47 ; // Unsubscribed
                    $delivery_asset->Action_Asset_Member_Update($data['member_record']['member_id_distribution_sublist_sms'],$options_array) ; 
                    // END: Update member metadata
                    
                    
                    
                    // Determine the parent delivery asset if there was one for personalization of messages
                    if ($data['delivery_asset']['parent_asset_id']) {
                        $delivery_asset->Set_Asset_By_ID($data['delivery_asset']['parent_asset_id']) ; 
                        $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                        }
                    
                    
                    // Send Recipient Confirmation:
                    $response_asset_id = $this->Set_System_Default_Value('sms_list_unsubscribe_confirmation')->Get_System_Default_Value() ;
                    
                    $response_campaign_options = array(
                        'asset_id' => $response_asset_id, // Confirm Unsubscribe
                        'delivery_asset_id' => $data['delivery_latest']['delivery_asset_id'],
                        'campaign_title' => 'Unsubscribe Auto-response to '.$data['delivery_latest']['member_id'],
                        'timestamp_next_action' => TIMESTAMP,
                        'messaging_number_id' => $data['profile_record']['messaging_number_id'],
                        'automation_status_name' => 'scheduled',
                        'delivery_trigger' => 'delivery_list_custom',
                        'recipients' => array(
                            'to' => array(
                                'delivery_list_custom' => array(
                                    'member_id' => array($data['delivery_latest']['member_id']),
                                    'sms_metadata_id' => array(47) // Unsubscribed
                                    ),
                                ),
                            ),                         
                        'personalization_overrides' => array(
                            'asset_record' => array(
                                'asset_title' => $data['delivery_asset']['asset_title']
                                )
                            )
                        ) ; 
                    
                    if ($data['delivery_latest']['structured_data_campaigns_01']['messaging_number_id']) {
                        $response_campaign_options['messaging_number_id'] = $data['delivery_latest']['structured_data_campaigns_01']['messaging_number_id'] ; 
                        } 

                    
                    $this->Action_Create_Campaign($response_campaign_options) ;
                    $response_campaign = $this->Get_Response() ;
                        
                    
                    if ($response_campaign['result'] == 'success') {
                        
                        // Archive new campaign
                        $campaign_record = $this->Get_Campaign() ;
                        $campaign_update_query = array(
                            'campaign_id' => $campaign_record['campaign_id'],
                            'archive_campaign' => 'yes'
                            ) ; 
                        $this->Action_Update_Campaign($campaign_update_query) ;                         
                        
                        $response_campaign['automation'] = $this->Get_Automation() ;     
                        $this->Action_Trigger_Automation_Queue($response_campaign['automation']['automation_id']) ;
                        }
                    // END: Recipient Confirmation
                        
                                            
                    // Add message event
                    $data['message_id'] = $this->Get_Message_ID() ;
                                        
                    $message_event_query_options = array(
                        'message_id' => $data['message_id'],
                        'event' => 'unsubscribed',
                        'event_message' => 'Recipient unsubscribed from '.$data['delivery_asset']['asset_title']
                        ) ; 
                    
                    $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ; 
                    }                
                
                break ; 
            case 'SUBSCRIBE':    
                
                
                // If the message content is SUBSCRIBE, then subscribe them to the sms subslist
                if ($data['delivery_latest'] != 'new') {
                    
                    $delivery_asset = new Asset() ; 
                    $delivery_asset->Set_Asset_By_ID($data['delivery_latest']['delivery_asset_id']) ; 
                    $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                    
                    $member_list_options['filter_by_member_id'] = $data['delivery_latest']['member_id'] ; 
                    $member_list_options['filter_by_user_id'] = 'no' ;
                    $member_list_options['filter_by_account_id'] = 'no' ;
                    $delivery_asset->Set_Asset_Members_List($member_list_options) ; 
                    
                    $member_list = $delivery_asset->Get_Asset_Members_List() ;       
                    $data['member_record'] = $member_list[0] ; 
                    $data['subscribe_asset'] = $delivery_asset->Get_Asset() ; 

                    $options_array['asset_id'] = $data['subscribe_asset']['distribution_sublist_sms']['asset_id'] ; 
                    $options_array['metadata_id'] = 46 ; // SUBSCRIBED
                    $delivery_asset->Action_Asset_Member_Update($data['member_record']['member_id_distribution_sublist_sms'],$options_array) ; 
                    
                    // Repull the subscriber list again to check for updates...
                    $member_list_options['filter_by_user_id'] = 'no' ;
                    $member_list_options['filter_by_account_id'] = 'no' ; 
                    $member_list_options['filter_by_member_id'] = $data['delivery_latest']['member_id'] ; 
                    $delivery_asset->Set_Asset_Members_List($member_list_options) ; 
                    
                    
                    
                    // LAUNCH NEW SERIES CAMPAIGN IF REQUIRED...
                    $data['clean_member_list'] = $delivery_asset->Get_Asset_Members_List() ;
                    $data['active_member'] = $data['clean_member_list'][0] ;

                    if (isset($data['active_member']['parent_asset'])) {
                        
                        switch ($data['active_member']['parent_asset']['type_id']) {
                            case 17: // Sequence    
                                
                                if (($data['active_member']['campaign_id'] == 0) AND (($data['active_member']['metadata_id_distribution_sublist_email'] == 46) OR ($data['active_member']['metadata_id_distribution_sublist_sms'] == 46))) {
                                    
                                    // Setup the sequence campaign
                                    $campaign_options = array(
                                        'asset_id' => $data['active_member']['parent_asset']['asset_id'],
                                        'delivery_asset_id' => $data['active_member']['asset_id'],
                                        'timestamp_next_action' => TIMESTAMP
                                        ) ;                                

                                    $campaign_options['recipients'] = array(
                                        'to' =>  array(
                                            'member_id' => array($data['active_member']['member_id']),
                                            'metadata_id' => array(46) // subscribed
                                            )
                                        ) ;   

                                    // Create the sequence campaign
                                    $delivery_asset->Set_User_By_ID($data['active_member']['user_id'])->Set_Account_By_ID($data['active_member']['account_id']) ;
                                    $create_campaign = $delivery_asset->Action_Create_Campaign($campaign_options) ;


                                    // Update the member_id with the new campaign_id
                                    $update_attendee_input = array(
                                        'member_id' => $data['active_member']['member_id'],
                                        'campaign_id' => $create_campaign['create_campaign']['insert_id']
                                        ) ; 

                                    $data['update_attendee_record'] = $this->Update_Asset_Member($update_attendee_input) ;                                     
                                    }                                
                                break ; 
                            }                        
                        }                     
                    
                    
                    
                    // Determine the parent delivery asset if there was one for personalization of messages
                    if ($data['delivery_asset']['parent_asset_id']) {
                        $delivery_asset->Set_Asset_By_ID($data['delivery_asset']['parent_asset_id']) ; 
                        $data['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                        }
                    

                    // Send Recipient Confirmation:
                    $response_asset_id = $this->Set_System_Default_Value('sms_list_confirmation')->Get_System_Default_Value() ;
                    
                    $response_campaign_options = array(
                        'asset_id' => $response_asset_id, // Confirm Unsubscribe
                        'delivery_asset_id' => $data['delivery_latest']['delivery_asset_id'],
                        'campaign_title' => 'SUBSCRIBE Auto-response to '.$data['delivery_latest']['member_id'],
                        'timestamp_next_action' => TIMESTAMP,
                        'messaging_number_id' => $data['profile_record']['messaging_number_id'],
                        'automation_status_name' => 'scheduled',
                        'delivery_trigger' => 'delivery_list_custom',
                        'recipients' => array(
                            'to' => array(
                                'delivery_list_custom' => array(
                                    'member_id' => array($data['delivery_latest']['member_id']),
                                    'sms_metadata_id' => array(46) // Subscribed
                                    ),
                                ),
                            ), 
                        'personalization_overrides' => array(
                            'asset_record' => array(
                                'asset_title' => $data['delivery_asset']['asset_title']
                                )
                            )
                        ) ; 
                    
                    if ($data['delivery_latest']['structured_data_campaigns_01']['messaging_number_id']) {
                        $response_campaign_options['messaging_number_id'] = $data['delivery_latest']['structured_data_campaigns_01']['messaging_number_id'] ; 
                        } 
                    

                    $this->Action_Create_Campaign($response_campaign_options) ;
                    $response_campaign = $this->Get_Response() ;
                        
                    if ($response_campaign['result'] == 'success') {
                        
                        // Archive new campaign
                        $campaign_record = $this->Get_Campaign() ;
                        $campaign_update_query = array(
                            'campaign_id' => $campaign_record['campaign_id'],
                            'archive_campaign' => 'yes'
                            ) ; 
                        $this->Action_Update_Campaign($campaign_update_query) ;                        
                        
                        $response_campaign['automation'] = $this->Get_Automation() ;     
                        
                        $this->Action_Trigger_Automation_Queue($response_campaign['automation']['automation_id']) ;
                        }
                    // END: Recipient Confirmation                    
                    
                    
                    // Add message event
                    $data['message_id'] = $this->Get_Message_ID() ; 
                    
                    $message_event_query_options = array(
                        'message_id' => $data['message_id'],
                        'event' => 'subscribed',
                        'event_message' => 'Recipient confirmed subscription to '.$data['delivery_asset']['asset_title']
                        ) ; 
                    
                    $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ;                     
                    }                
                
                break ;
            }
        
                

        return $data['message_event'] ; 
        }
    
    
    
    
    public function Create_Message_Event($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => "message_events",
            'values' => array(
                'message_id' => $query_options->message_id,
                'event' => $query_options->event,
                'microtimestamp_event' => MICRO_TIMESTAMP,
                'event_message' => $query_options->event_message
                )
            );

        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->message_query_result = $result ; 
        
        
        return $result ; 
        } 
    
    
    
    public function Set_Message_ID($message_id) {
        
        $this->message_id = $message_id ; 
        
        return $this ; 
        }
    
    
    public function Set_Message_By_ID($message_id = 'internal',$additional_parameters = array()) {
        
        if (!isset($additional_parameters['set_by'])) {
            $additional_parameters['set_by'] = 'id' ; 
            }        

        switch ($additional_parameters['set_by']) {
            case 'automation_id':
                
                    $query_array = array(
                        'table' => 'messages',
                        'fields' => "messages.automation_id, messages.message_id, messages.message_sid",
                        'where' => "messages.automation_id='$message_id'"
                        );        
                
                    $result = $this->DB->Query('SELECT',$query_array,'force');

                
                    if ($result['results']) {
                        $message_id = $result['results'][0]['message_id'] ;
                        $this->message_id = $message_id ; 
                        } else {
                            $message_id = 0 ; 
                            }
                
                break ;                
            case 'sid':
            case 'message_sid':
                
                    $query_array = array(
                        'table' => 'messages',
                        'fields' => "messages.message_id, messages.message_sid",
                        'where' => "messages.message_sid='$message_id'"
                        );        
                
                    $result = $this->DB->Query('SELECT',$query_array,'force');

                
                    if ($result['results']) {
                        $message_id = $result['results'][0]['message_id'] ;
                        $this->message_id = $message_id ;    
                        
                        } else {
                            $message_id = 0 ; 
                            }
                
                break ;
            case 'id':
            default:    
                                
                if ('internal' === $message_id) {
                    $message_id = $this->message_id ; 
                    } else {
                        $this->message_id = $message_id ; 
                        }
                        
            }
        
        
        if ($message_id != 0) {
            $this->Set_Message($this->message_id) ; 
            } else {
                $this->message = 'error' ; 
                }
        
        
        return $this ; 
        }
    
    
    public function Set_Message($message_id = 'internal') {
        
        if ('internal' === $message_id) {
            $message_id = $this->message_id ; 
            } else {
                $this->message_id = $message_id ; 
                }
        
        $result = $this->Retrieve_Message() ;         
 
        if ($result['result_count'] == 0) {
            $this->message = 'error' ; 
            } else {
            
                $message_record = $this->Action_Time_Territorialize_Dataset($result['results']) ;
                $message_record = $this->Action_Phone_Territorialize_Dataset($message_record) ;

                $message_record['media_set'] = json_decode($message_record['message_media'],1) ; 
            
                $this->message = $message_record ;
                }

        
        return $this ; 
        }
    

    
    
    public function Retrieve_Message($message_id = 'internal',$additional_parameters = array()) {
                
        if ('internal' === $message_id) {
            $message_id = $this->message_id ; 
            } 
        
        
        $query_array = array(
            'table' => 'messages',
            'fields' => "messages.*,
                message_content.vendor_id, message_content.message_content,
                message_content.message_sid AS message_content_message_sid,
                message_content.message_media, 
                asset_campaigns.campaign_id, asset_campaigns.account_id, asset_campaigns.user_id, 
                user_profiles.profile_id, user_profiles.first_name AS profile_first_name, user_profiles.last_name AS profile_last_name, 
                    user_profiles.profile_display_name, user_profiles.messaging_number_id,
                user_messaging_numbers.phone_number_messaging AS phone_number_messaging,
                user_messaging_numbers.messaging_phone_country_id AS country_id",
            'where' => "messages.message_id='$message_id'",
            'group_by' => "messages.message_id"
            );        

        
        $query_array['join_tables'][] = array(
            'table' => 'message_content',
            'on' => 'message_content.message_content_id',
            'match' => 'messages.message_content_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => 'asset_campaign_automation.automation_id',
            'match' => 'messages.automation_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaigns',
            'on' => 'asset_campaigns.campaign_id',
            'match' => 'asset_campaign_automation.campaign_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_profiles',
            'statement' => '(user_profiles.user_id=asset_campaigns.user_id AND user_profiles.account_id=asset_campaigns.account_id)'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_messaging_numbers',
            'on' => 'user_messaging_numbers.messaging_number_id',
            'match' => 'user_profiles.messaging_number_id'
            );
        
        
        // Delivery Asset Subquery
        $join_table_name = 'delivery_asset' ;  
        $query_array['fields'] .= ", 
            $join_table_name.asset_id AS delivery_asset_id, 
            $join_table_name.asset_title AS delivery_asset_title, 
            $join_table_name.type_id AS delivery_asset_type_id, 
            $join_table_name.type_name AS delivery_asset_type_name" ; 
            
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT assets.asset_id, assets.asset_title, asset_type.*, asset_status.*, system_visibility_levels.* 
                FROM assets 
                JOIN asset_type ON assets.type_id = asset_type.type_id 
                JOIN asset_status ON asset_status.asset_status_id = asset_status.asset_status_id 
                JOIN system_visibility_levels ON assets.visibility_id = system_visibility_levels.visibility_id
                ) AS $join_table_name",
            'statement' => "
                    (asset_campaigns.delivery_asset_id=$join_table_name.asset_id)"
            );        
        
        
        
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array);
        
        return $result ;         
        }
    
    
    
    public function Set_Message_List($query_options = array()) {

        $query_options['skip_message_content'] = 'yes' ; 
        $result = $this->Retrieve_Message_List($query_options) ;
        
        
        if ($result['result_count'] == 0) {
            
            $this->message_list = 'error' ; 
            
            } else {
            
//                $email_record = $this->Action_Time_Territorialize_Dataset($result['results'][0]) ;
                $this->message_list = $result['results'] ; 
            
                }
        
        return $this ; 
        }
    
    
    public function Retrieve_Message_List($query_options) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        // Base query:
        $query_array = array(
            'table' => 'messages',
            'join_tables' => array(),
            'fields' => "
                messages.*,
                asset_campaigns.campaign_id, asset_campaigns.account_id, asset_campaigns.user_id, 
                user_profiles.profile_id, user_profiles.first_name AS profile_first_name, user_profiles.last_name AS profile_last_name, 
                    user_profiles.profile_display_name, user_profiles.email_address_id,
                user_emails.email_address AS profile_email_address
                ",
            'group_by' => "messages.message_id"
            );
        

        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ; 
            
            if ($query_array['order_by'] == 'relevance') {
                $query_array['order_by'] = 'asset_core.asset_title' ; 
                $query_array['order_by_relevance'] = 'yes' ; 
                }  
            
            if (isset($query_options->order_field)) {
                $query_array['order_field'] = $query_options->order_field ;
                $query_array['order_start'] = $query_options->order_start ;
                $query_array['order_operator'] = $query_options->order_operator ;
                }           
            } 
        
        
        // PROCESS MESSAGE IDs
        if (isset($query_options->filter_by_message_id)) { 
            
            $query_options->filter_by_message_id = Utilities::Array_Force($query_options->filter_by_message_id) ; 
            
            $filter_by_message_id_string = '' ; 
            foreach ($query_options->filter_by_message_id as $query_message_id) {
                $filter_by_message_id_string .= "messages.message_id='$query_message_id' OR " ; 
                }
            $filter_by_message_id_string = rtrim($filter_by_message_id_string," OR ") ;
            $query_array['where'] .=  " AND ($filter_by_message_id_string) " ;                  
            }
        
        
        // PROCESS AUTOMATION IDs
        if (isset($query_options->filter_by_automation_id)) { 
            
            $query_options->filter_by_automation_id = Utilities::Array_Force($query_options->filter_by_automation_id) ; 
            
            $filter_by_automation_id_string = '' ; 
            foreach ($query_options->filter_by_automation_id as $query_automation_id) {
                $filter_by_automation_id_string .= "messages.automation_id='$query_automation_id' OR " ; 
                }
            $filter_by_automation_id_string = rtrim($filter_by_automation_id_string," OR ") ;
            $query_array['where'] .=  " AND ($filter_by_automation_id_string) " ;                  
            }
        
        
        
        // Add in additional join tables...
        if ($query_options->skip_message_content != 'yes') {
            
            $query_array['fields'] .= ", message_content.message_content, message_content.message_media, message_content.segment_count" ; 
            
            $query_array['join_tables'][] = array(
                'table' => 'message_content',
                'on' => 'message_content.message_content_id',
                'match' => 'messages.message_content_id',
                'type' => 'left'
                );            
            }
     
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => 'asset_campaign_automation.automation_id',
            'match' => 'messages.automation_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaigns',
            'on' => 'asset_campaigns.campaign_id',
            'match' => 'asset_campaign_automation.campaign_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_profiles',
            'statement' => '(user_profiles.user_id=asset_campaigns.user_id AND user_profiles.account_id=asset_campaigns.account_id)'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_emails',
            'on' => 'user_emails.email_address_id',
            'match' => 'user_profiles.email_address_id'
            );
        
        
        // Delivery Asset Subquery
        $join_table_name = 'delivery_asset' ;  
        $query_array['fields'] .= ", 
            $join_table_name.asset_id AS delivery_asset_id, 
            $join_table_name.asset_title AS delivery_asset_title, 
            $join_table_name.type_id AS delivery_asset_type_id, 
            $join_table_name.type_name AS delivery_asset_type_name" ; 
            
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT assets.asset_id, assets.asset_title, asset_type.*, asset_status.*, system_visibility_levels.* 
                FROM assets 
                JOIN asset_type ON assets.type_id = asset_type.type_id 
                JOIN asset_status ON asset_status.asset_status_id = asset_status.asset_status_id 
                JOIN system_visibility_levels ON assets.visibility_id = system_visibility_levels.visibility_id
                ) AS $join_table_name",
            'statement' => "
                    (asset_campaigns.delivery_asset_id=$join_table_name.asset_id)"
            );        
        
        
        
        if (isset($query_array['where'])) {
            $query_array['where'] = ltrim($query_array['where']," AND ") ;            
            }

        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->email_query_result = $result ;   
               

        $query_options->value_name = 'message_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Message_Paging($result) ;         
        $this->message_query_result = $result ; 
                

        return $result ; 
        }
    
    
    
    public function Set_Message_Event_List($query_options = array()) {
        
        
        $result = $this->Retrieve_Message_Event_List($query_options) ;         

        if ($result['result_count'] > 0) {
            
            $i = 0 ; 
            foreach ($result['results'] as $event) {
                
                // Territorialize timestamps...
                $event = $this->Action_Time_Territorialize_Dataset($event) ;
                
                $result['results'][$i] = $event ; 
                $i++ ; 
                }
            
            $this->message_event_list = $result['results'] ; 
            } else {
                $this->message_event_list = 'error' ; 
                }
        
        return $this ; 
        }
    
    
    public function Get_Message_Event_List() {
        
        return $this->message_event_list ; 
        }
    
    
    public function Retrieve_Message_Event_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        

        $query_array = array(
            'table' => "message_events",
            'fields' => "messages.*, 
                message_events.*,
                asset_campaigns.campaign_id, asset_campaigns.account_id, asset_campaigns.user_id ",
            'group_by' => "message_events.event_id"
            );

            
        
        
        if ($query_options->filter_by_message_id == 'yes') {
            $query_array['where'] .= " AND message_events.message_id='$this->message_id'" ; 
            }
                
        if (isset($query_options->message_id)) {
            $query_array['where'] .= " AND message_events.message_id='$query_options->message_id'" ; 
            }
        

        // PROCESS AUTOMATION IDs
        if (isset($query_options->filter_by_automation_id)) { 
            
            $query_options->filter_by_automation_id = Utilities::Array_Force($query_options->filter_by_automation_id) ; 
            
            $filter_by_automation_id_string = '' ; 
            foreach ($query_options->filter_by_automation_id as $query_automation_id) {
                $filter_by_automation_id_string .= "messages.automation_id='$query_automation_id' OR " ; 
                }
            $filter_by_automation_id_string = rtrim($filter_by_automation_id_string," OR ") ;
            $query_array['where'] .=  " AND ($filter_by_automation_id_string) " ;                  
            }        
        
        
        // Add in additional join tables...
        $query_array['join_tables'][] = array(
            'table' => 'messages',
            'on' => 'messages.message_id',
            'match' => 'message_events.message_id'
            ); 

        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => 'asset_campaign_automation.automation_id',
            'match' => 'messages.automation_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaigns',
            'on' => 'asset_campaigns.campaign_id',
            'match' => 'asset_campaign_automation.campaign_id'
            );
        
        if (isset($query_array['where'])) {
            $query_array['where'] = ltrim($query_array['where']," AND ") ;            
            }
        
        
        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $this->message_query_result = $result ; 
        
        
        // Get the total count of items in this query
        if (isset($query_options->total_count)) { 
            
            $result['total_count'] = $query_options->total_count ; 
            
            } else {                
                    
                $query_array['fields'] = "message_events.event_id" ; 
                unset($query_array['limit'],$query_array['offset']) ;
            
                $count = $this->DB->Query('SELECT_JOIN',$query_array,'force');

                $result['total_count'] = $count['result_count'] ;
                $result['uber_result_set'] = $count['results'] ;            
            
            
                }
        
        $result['offset_page'] = $query_options->offset_page ;         
        
        $this->Set_Message_Paging($result) ;
        
        
        return $result ; 
        }
    
    
    
    public function Set_Message_API_Event($event) {
        
        $this->message_api_event = $event ; 
        
        return $this ; 
        }
    
    
    // Set the sender of the message: Currently phone number -- need to add bulk services
    public function Set_Message_Sender($messaging_number_id = 'default') {
        
        $continue = 1 ; 
        $sender_array = array() ; 
        
        if ('default' === $messaging_number_id) {
            
            $sender_array['messaging_number_id'] = $this->profile['messaging_number_id'] ; 
            $sender_array['messaging_phone_vendor_id'] = $this->profile['messaging_phone_vendor_id'] ; 
            $sender_array['phone_number_messaging'] = $this->profile['phone_number_messaging'] ; 
            $sender_array['messaging_notify_service_id'] = $this->profile['messaging_notify_service_id'] ; 
            $sender_array['messaging_phone_active'] = $this->profile['messaging_phone_active'] ;
                    
            } else {
                
                // Deep array find the $messaging_number_id in profile['messaging_numbers']
                $sender_array = Utilities::Deep_Array($this->profile['messaging_numbers_list'],'messaging_number_id',$messaging_number_id) ; 
                
                }
        

        
        if (!$sender_array['messaging_number_id']) {
            $continue = 0 ; 
            $sender_array = 'error'  ;
            $this->Set_Alert_Response(147) ; // Messaging phone number not set
            }

        if ($sender_array['messaging_phone_active'] != 1) {
            $continue = 0 ; 
            $sender_array = 'error'  ;
            $this->Set_Alert_Response(149) ; // Messaging phone number not active
            $this->Append_Alert_Response('none',array(
                'admin_context' => json_encode($this->profile)
                )) ;             
            }
        
        switch ($sender_array['messaging_phone_vendor_id']) {
            case 3: // Twilio
                if (!$sender_array['messaging_notify_service_id']) {
                    $continue = 0 ; 
                    $sender_array = 'error'  ;
                    $this->Set_Alert_Response(182) ; // No messaging notify service set
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($this->profile)
                        )) ;            
                    }                
                break ; 
            }
        
        
        // Process and create sender array
        if ($continue == 1) { 
            $sender_array['sender_name'] = $this->profile['profile_display_name'] ;            
            $this->Set_Alert_Response(203) ; // Message sender compiled
            } 
        
        $this->message_sender = $sender_array ; 
        
        return $this ; 
        }    
    
    
    
    // Process a set of asset results and separate into paging components to use for site navigation
    public function Set_Message_Paging($results_array) {
        
        if (!isset($this->message_paging)) {
            $this->message_paging = $this->Set_Default_Paging_Object() ; 
            }
        
        $this->message_paging->total_count = $results_array['total_count'] ; 
        
        $this->message_paging->current = Utilities::Offset_To_Start_Page($results_array['offset_page']) ; 
        $this->message_paging->last = ceil($this->message_paging->total_count / $this->page_increment) ;
        $this->message_paging->total_pages = $this->message_paging->last ;
        
        if (($this->message_paging->current - 1) < 1) {
            $this->message_paging->previous = 1 ; 
            } else {
                $this->message_paging->previous = $this->message_paging->current - 1 ; 
                }

        if (($this->message_paging->current + 1) > $this->message_paging->last) {
            $this->message_paging->next = $this->message_paging->last ; 
            } else {
                $this->message_paging->next = $this->message_paging->current + 1 ; 
                }
        
        return $this ; 
        }    
    
    
    public function Set_Messaging_Phone_Number_By_ID($messaging_number_id) {
        
        $query_params = array(
            'filter_by_messaging_number_id' => $messaging_number_id
            ) ;
        
        $this->Set_Messaging_Phone_Number($query_params) ; 
        
        
        return $this ;     
        }
    
    
    public function Set_Messaging_Phone_Number($query_parameters) {
        
        $result = $this->Retrieve_Messaging_Phone_Numbers_List($query_parameters) ; 
           
        if ((!$result['error']) AND ($result['result_count'] > 0)) {
            
            $messaging_number = $result['results'][0] ;             
            
            $messaging_number = $this->Action_Phone_Territorialize_Dataset($messaging_number) ;
            $messaging_number = $this->Action_Time_Territorialize_Dataset($messaging_number) ;
            
            $messaging_number['primary_number'] = 0 ; 
            
            if (isset($messaging_number['messaging_number_id']) AND ($messaging_number['messaging_number_id'] == $messaging_number['messaging_number_id_primary'])) {
                $messaging_number['primary_number'] = 1 ; 
                } 
            
            $this->messaging_phone_number = $messaging_number ; 
            } else {
                $this->messaging_phone_number = 'error' ; 
                }

        return $this ; 
        } 
    
    
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_Message_ID() {
        
        return $this->message_id ; 
        }
    
    
    public function Get_Message() {
        
        return $this->message ; 
        }
    
    
    public function Get_Message_List() {
        
        return $this->message_list ; 
        }
    
    // The message sender array
    public function Get_Message_Sender() {
        
        return $this->message_sender ; 
        }
    
    
    public function Get_Recipient_Sets() {
        
        return $this->recipient_sets ; 
        }
    
    // The message_content_id associated with the message that was delivered
    public function Get_Message_Content_ID() {
        
        return $this->message_content_id ;
        }
    
    public function Get_Message_Content() {
        
        return $this->message_content ;
        }
    
    // Get the result of the message that was queued to the service provider
    public function Get_Delivery_Result() {
        
        return $this->delivery_result ; 
        } 
    
    
    // Get the full array of all input and output in the message queue process
    public function Get_Message_Action() {
        
        return $this->message_action ; 
        }
    
    // Available numbers list
    public function Get_Messaging_Phone_Numbers_List() {
        
        return $this->messaging_phone_numbers_list ; 
        }    
    
    public function Get_Messaging_Phone_Number() {
        
        return $this->messaging_phone_number ; 
        }
    
    
    // Retrieve the most recent API event response from the service
    public function Get_Message_API_Event() {
        
        return $this->message_api_event ; 
        }    
    
    
    
    //////////////////////
    //                  //
    // API ACTIONS      //
    //                  //
    ////////////////////// 
    
    
    // API ACTION: Queue SMS Message to a single recipient
    public function API_Action_Queue_SMS($properties_array = array()) {

        try {
            
            $message_result = $this->_connection->messages->create(
                $this->recipients['recipient_single'], // Where to send the SMS message
                array(
                    'from' => $this->message_sender['phone_number_messaging'], // From number; needs to be set via Set_From_Number after initialization
                    'body' => $this->message_content // SMS message body
                    )
                );
            
            $this->Set_Alert_Response(145) ; // Message successfully queued for delivery
            
            $delivery_result['asset_status_id'] = 6 ; // Message queued for delivery
            $delivery_result['message_sid'] = $message_result->sid ; 
            $delivery_result['response_status'] = $message_result->status ;   
            
            } catch ( Exception $e ) {
            
                $this->Set_Alert_Response(146) ; // Message experienced an internal app error

                $delivery_result['asset_status_id'] = 1 ; // Error
                $delivery_result['response_status'] = 'error' ;   
                $delivery_result['response_message'] = $e->getMessage() ; //             
            
                $message_result = $delivery_result ; 
            
                }
        

        $this->Append_Alert_Response('none',array(
            'admin_context' => json_encode($delivery_result),
            'location' => __METHOD__
            )) ;

        $this->Set_Message_API_Event($message_result) ;
        
        
        $result = $this->Get_Response() ; 
        
        $delivery_result['message_status'] = $result ; 
        $delivery_result['message_sender'] = $this->Get_Message_Sender() ;
        $delivery_result['delivery_array'] = array(
            'recipient' => $this->recipients['recipient_single'],
            'from' => $this->message_sender['sender_phone_number'],
            'message_content' => $this->message_content
            ) ; 	
        // $delivery_result['response_full'] = $message_result ; // To get this data in a meaningful way, we'll probably need to create a manual aray of all Twilio responses
        
        $this->delivery_result = $delivery_result ; 

        return $this ;
        }
    


    // API ACTION: Queue Bulk SMS Message
    // https://www.twilio.com/docs/notify/api/notification-resource?code-sample=code-send-a-notification-to-bindings-in-the-request&code-language=PHP&code-sdk-version=5.x
    public function API_Action_Queue_Bulk_SMS(
        $recipient_bindings_array = array(),
        $recipient_variables = array(),
        $message_content = 'internal', $send_time = 'now', 
        $properties_array = array(),
        $options_array = array()
        ) {
        
        $continue = 1 ; 
        
        $data['message_sender'] = $this->Get_Message_Sender() ; 

        if (!isset($data['message_sender']['messaging_notify_service_id'])) {
            $continue = 0  ; 
            $this->Set_Alert_Response(182) ; // No messaging notify service
            $this->Append_Alert_Response('none',array(
                'admin_context' => json_encode($data['message_sender'])
                )) ;            
            }            
            
        // Check to see if we have more than 0 recipients submitted
        if ((count($recipient_bindings_array) == 0) AND ($continue == 1)) {
            $this->Set_Alert_Response(183) ; // Create alert for no recipients 
            $this->Append_Alert_Response('none',array(
                'admin_context' => 'binding count = '.count($recipient_bindings_array).' and continue  = '.$continue
                )) ;
            
            } 

            
        if (($options_array['halt_delivery'] == 'yes') AND ($continue == 1)) {
            $continue = 0 ; 
            $this->Set_Alert_Response(184) ; // Delivery manually halted
            }
            

        
        // Set message for delivery...    
        if ($continue == 1) {
            
            // Process message
            if ('internal' === $message_content) {
                $delivery_array['message_content'] = $this->message_content ;
                } else {
                    $delivery_array['message_content'] = $message_content ;
                    }            
            

            switch ($this->vendor_id) {
                case 3: // Twilio    

                    try {

                        $message_result = $this->_connection->notify->v1->services($data['message_sender']['messaging_notify_service_id'])
                           ->notifications
                           ->create(array(
                                    "toBinding" => $recipient_bindings_array,
                                    'body' => $delivery_array['message_content']
                                    )
                                ) ;            

                        $this->Set_Alert_Response(145) ; // Message successfully queued for delivery

                        $delivery_result['asset_status_id'] = 7 ; // Message queued for delivery
                        $delivery_result['message_sender'] = $data['message_sender'] ; 

                        $delivery_result['delivery_array'] = $delivery_array ; 

                        $delivery_result['message_sid'] = $message_result->sid ; 
                        $delivery_result['service_id'] = $message_result->serviceSid ; 

                        $delivery_result['response_status'] = $message_result ;               
                        $delivery_result['bindings'] = $recipient_bindings_array ; 

                        // Error codes from Twilio
                        // https://www.twilio.com/docs/verify/return-and-error-codes

                        } catch ( Exception $e ) {

                            $this->Set_Alert_Response(146) ; // Message experienced an internal app error

                            $delivery_result['asset_status_id'] = 1 ; // Error
                            $delivery_result['response_message'] = $e->getMessage() ; //             

                            $delivery_result['bindings'] = $recipient_bindings_array ; 

                            $message_result = $delivery_result ;

                            } 



                    break ; 
                case 9: // Bandwidth    

                    $messagingClient = $this->_messaging_client->getMessaging()->getClient();

                    
                    $i = 0 ; 
                    foreach ($recipient_bindings_array as $recipient_binding) {

                        // Decode the binding from json
                        $recipient_binding_decoded = json_decode($recipient_binding,true)  ;
                        $recipient_binding_decoded['message_media'] = array() ;

                        
                        // Personalize content for the individual recipient from recipient variables... 
                        $personalization_array = array(
                            'recipient_record' => $recipient_binding_decoded['recipient_variables']
                            ) ;
                        
                        // Need to revert *|recipient_ variables back to *|x|* variables
                        $email = new Email() ; 
                        $message_content_personalized = $email->Action_Revert_Recipient_Variables($delivery_array['message_content'])->Get_Personalized_String() ; 
                        $message_content_personalized = $email->Action_Personalize_String($message_content_personalized,$personalization_array) ; 

                        $body = new BandwidthLib\Messaging\Models\MessageRequest();
                        $body->from = $data['message_sender']['phone_number_messaging'] ;
                        $body->to = array($recipient_binding_decoded['address']);
                        $body->applicationId = $this->application_id ;
                        $body->text = $message_content_personalized ;

                        $data['sms_counter'] = $this->Action_Count_Characters($delivery_array['message_content']) ; 
                
                        
                        if ($data['sms_counter']->messages > 1) {
                            $body->media = array('https://ucarecdn.com/9f64abe3-3d47-44e0-9a48-e7a8f5babf63/mms.txt') ; 
                            }
                        

                        if ($this->asset['media_asset_id']) {
                            
                            $media_asset_id_array = Utilities::Process_Comma_Separated_String($this->asset['media_asset_id']) ;
                            foreach ($media_asset_id_array as $this_asset_id) {
                                $media = new Asset() ; 
                                
                                $meda_asset_options = array(
                                    'skip_history' => 'yes'
                                    ) ; 
                                $media->Set_Asset_By_ID($this_asset_id,$meda_asset_options) ; 
                                $media_asset = $media->Get_Asset() ; 
                                
                                switch ($media_asset['type_name']) {
                                    case 'image':    
                                        $body->media[] = $media_asset['image_delivery_url'].'/q_auto:good/'.$media_asset['image_folder_filename_extension'] ; 
                                        break ; 
                                    }
                                
                                $recipient_binding_decoded['message_media'][] = array(
                                    'url_full' => $media_asset['image_delivery_url'].'/q_auto:good/'.$media_asset['image_folder_filename_extension'],
                                    'type' => $media_asset['type_name']
                                    ) ;                                
                                
                                } 
                            
                            }
                        
                                                
                        try {
                            
                            $response = $messagingClient->createMessage(BANDWIDTH_ACCOUNT_ID, $body);
                            $message_result = $response->getResult() ; 
                            
                            $this->Set_Alert_Response(145) ; // Message successfully queued for delivery

                            
                            $delivery_result['message_sender'] = $data['message_sender'] ; 

                            $delivery_result['delivery_array'] = $delivery_array ; 

                            $recipient_binding_decoded['asset_status_id'] = 7 ; // Message queued for delivery
                            $recipient_binding_decoded['message_sid'] = $message_result->id ;
                            $recipient_binding_decoded['segment_count'] = $message_result->segmentCount ;
                            
                            $recipient_binding_decoded['response_status'] = $message_result ; 
//                            $recipient_binding_decoded['response_status']['queued_text'] = $message_content_personalized ; 
//                            error_log('queued text message: '.$message_content_personalized) ;
                            
                            $delivery_result['response_status'] = $message_result ;               
                            
                            
                            } catch ( Exception $e ) {

                                $this->Set_Alert_Response(146) ; // Message experienced an internal app error

                                $recipient_binding_decoded['asset_status_id'] = 1 ; // Error
                                $recipient_binding_decoded['response_message'] = $e->getMessage() ; //             

                                $message_result = $recipient_binding_decoded ;
                                }
                        
                        
                        
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($delivery_result),
                            'location' => __METHOD__
                            )) ;

                        $this->Set_Message_API_Event($message_result) ;
                        $result = $this->Get_Response() ; 
                        
                        
                        $recipient_binding_decoded['message_status'] = $result ; 
                        $recipient_bindings_array[$i] = json_encode($recipient_binding_decoded) ;
                        
                        
                        $i++ ; 
                        }
                            

                    $delivery_result['bindings'] = $recipient_bindings_array ;
                    
                    
                    break ; 
                }

      
            }
        
            
        $this->delivery_result = $delivery_result ; 

        return $this ;
        }
    
    
    public function API_Action_Get_Test() {
        
        $test = $this->_connection->sites();
        $event = $test->getList(); // Array(Site1, Site2)

        $this->Set_Message_API_Event($event) ; 
                
        return $this ; 
        }
    
    // API ACTION: Search for available numbers
    // Documentation: https://www.twilio.com/docs/phone-numbers/api/available-phone-numbers
    // Search for local, toll-free and mobile phone numbers that are available for you to purchase
    public function API_Action_Search_Numbers($query_options) {
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $continue = 1 ; 
        
        if (!isset($query_options->country_id)) {
            $continue = 0 ;
            $this->Set_Alert_Response(164) ; // Unable to retrieve numbers because country_id missing
            }
        
        if ($continue == 1) {

            $country_query = array(
                'country_id' => $query_options->country_id
                ) ;
            $country = $this->Set_System_List('list_countries',$country_query)->Get_System_List('list_countries')[0] ; 
            
            
            if (!isset($query_options->voice_enabled)) {
                $query_options->voice_enabled = 'true' ; 
                }
            if (!isset($query_options->sms_enabled)) {
                $query_options->sms_enabled = 'true' ; 
                }            
            if (!isset($query_options->mms_enabled)) {
                $query_options->mms_enabled = 'true' ; 
                }            
            
            
            
            $number_results = array() ; 

            try {

                switch ($this->vendor_id) {
                    case 9: // Bandwidth
                        
                        
                        $query_array = array() ;
                        
                        if (isset($query_options->area_code)) {
                            $query_array['areaCode'] = $query_options->area_code ; 
                            }  
                        
                        if (isset($query_options->quantity)) {
                            $query_array['quantity'] = $query_options->quantity ; 
                            }
                        
                        $numbers_result = $this->_connection->availableNumbers($query_array);
                        $numbers = $numbers_result[0]->TelephoneNumber ; 
                        
                        break ; 
                    case 3: // Twilio   
                        
                        $query_array = array(
                            "voiceEnabled" => true,
                            "smsEnabled" => true,
                            "mmsEnabled" => true
                            ) ; 

                        if (isset($query_options->area_code)) {
                            $query_array['areaCode'] = $query_options->area_code ; 
                            }                        
                        
                        $numbers = $this->_connection->availablePhoneNumbers($query_options->country_id)->local->read($query_array) ;            
                        break ; 
                    }
                
                
                $this->Set_Message_API_Event($numbers) ; 
                
                
                foreach ($numbers as $number) {

                    if (isset($number->phoneNumber)) {
                        
                        $this_number = array(
                            'phone_number_service' => $number->phoneNumber,
                            'city' => $number->locality,
                            'state' => $number->region,
                            'postal_code' => $number->postalCode,
                            'country_id' => $number->isoCountry,
                            'latitude' => $number->latitude,
                            'longitude' => $number->longitude,
                            'voice_enabled' => $number->capabilities['voice'],
                            'sms_enabled' => $number->capabilities['SMS'],
                            'mms_enabled' => $number->capabilities['MMS'],
                            'fax_enabled' => $number->capabilities['fax'],
                            ) ;
                        
                        } else {
                            
                            $this_number = array(
                                'phone_number_service' => $number,
                                'phone_country_dialing_code' => $country['country_dialing_code']
                                ) ;
                            }
                    
                    
 

                    // Process the phone number...
                    $this_number = $this->Action_Phone_Territorialize_Dataset($this_number) ;            

                    $number_results[] = $this_number ; 
                    }

                $this->messaging_phone_numbers_list = $number_results ; 

                
                if (count($number_results) > 0) {
                    $this->Set_Alert_Response(152) ; // Messaging numbers available
                    } else {
                        $this->Set_Alert_Response(165) ; // No messaging numbers matched search
                        }

                } catch ( Exception $e ) {

                    // Twilio error object
                    //        {
                    //          "code": 21211,
                    //          "message": "The 'To' number 5551234567 is not a valid phone number.",
                    //          "more_info": "https://www.twilio.com/docs/errors/21211",
                    //          "status": 400
                    //        }                
                
                    $this->Set_Message_API_Event($e->getMessage()) ; 
                    $this->Set_Alert_Response(153) ; // Unable to retrieve numbers
                    }
            }
                
        return $this ;
        }
    
    

    
    // API ACTION: Provision and purchase an available number
    public function API_Action_Provision_Number($number_to_purchase) {

        switch ($this->vendor_id) {
            case 9: // Bandwidth

                try {
                    
                    $order_request = array(
                        "Name" => 'Number Order for '.$this->profile['profile_display_name'].' [Profile ID: '.$this->profile['profile_id'].']',
                        "SiteId" => $this->sub_account_id,
                        "ExistingTelephoneNumberOrderType" => array(
                            "TelephoneNumberList" => array(
                                "TelephoneNumber" => array($number_to_purchase)
                                )
                            )
                        ) ; 
                    

                    $order = $this->_connection->orders()->create($order_request);
                    
                    $api_result = $this->_connection->orders()->order($order->id, true);                    
                    $this->phone_number['phone_number'] = $number_to_purchase ; 

                    $this->Set_Message_API_Event($api_result) ;             
                    $this->Set_Alert_Response(167) ; // Phone number successfully provisioned
                    
                    
                    } catch ( Exception $e ) {

                        $this->Set_Message_API_Event($e->getMessage()) ; 
                        $this->Set_Alert_Response(166) ; // Error provisioning phone number    
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($e->getMessage()),
                            'location' => __METHOD__
                            )) ;
                        }
                
                

                break ; 
            case 3: // Twilio

                try {

                    $query_array = array(
                        "phoneNumber" => $number_to_purchase
                        ) ;

                    $api_result = $this->_connection->incomingPhoneNumbers->create($query_array) ;            

                    $this->phone_number['sid'] = $api_result->sid ; 
                    $this->phone_number['phone_number'] = $number_to_purchase ; 

                    $this->Set_Message_API_Event($api_result) ;             
                    $this->Set_Alert_Response(167) ; // Phone number successfully provisioned

                    } catch ( Exception $e ) {

                        $this->Set_Message_API_Event($e->getMessage()) ; 
                        $this->Set_Alert_Response(166) ; // Error provisioning phone number    
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($e->getMessage()),
                            'location' => __METHOD__
                            )) ;
                        } 
                
                
                break ; 
            }

        
        return $this ;
        }
    
    
    // API ACTION: Release a previously purchased number from inventory
    // Submit Twilio Phone ID (PNXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX) in query
    // https://www.twilio.com/docs/phone-numbers/api/incoming-phone-numbers#delete-an-incomingphonenumber-resource
    public function API_Action_Release_Number($messaging_phone_number_sid) {

            try {

                $api_result = $this->_connection->incomingPhoneNumbers($messaging_phone_number_sid)->delete();                
                
                $this->Set_Message_API_Event($api_result) ;   
                
                $this->Set_Alert_Response(177) ; // Phone number successfully released
                $this->Append_Alert_Response('none',array(
                    'admin_context' => $messaging_phone_number_sid,
                    'location' => __METHOD__
                    )) ;
                
                } catch ( Exception $e ) {

                    $this->Set_Message_API_Event($e->getMessage()) ; 
                    $this->Set_Alert_Response(176) ; // Error releasing phone number    
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($e->getMessage()),
                        'location' => __METHOD__
                        )) ;
                    }
        
        return $this ;
        }    
    
    
    
    public function API_Action_Create_Messaging_Service($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        try {

            $service = $this->_connection->messaging->v1->services->create($query_options->messaging_service_name);

            $this->service['sid'] = $service->sid ; 
            $this->Set_Message_API_Event($service) ; 
            $this->Set_Alert_Response(185) ; // Messaging service successfully created

            } catch ( Exception $e ) {             

                $this->Set_Message_API_Event($e->getMessage()) ; 
                $this->Set_Alert_Response(186) ; // Unable to create messaging service
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($e->getMessage()),
                    'location' => __METHOD__
                    )) ;            
                }        
        
        
        return $this ; 
        }
    
    
    
    public function API_Action_Update_Messaging_Service($query_options) {
        
        try {

            if (isset($query_options['messaging_phone_number_sid'])) {
                
                $add_phone_number = $this->_connection->messaging->v1->services($this->service['sid'])->phoneNumbers->create($query_options['messaging_phone_number_sid']) ;
                }

            $update_service_array = array() ; 
            if (isset($query_options['messaging_service_name'])) {
                $update_service_array['messaging_service_name'] = $query_options['messaging_service_name'] ;
                }            
            if (isset($query_options['webhook_url'])) {
                $update_service_array['inboundRequestUrl'] = $query_options['webhook_url'] ;
                }  
            if (isset($query_options['status_callback_url'])) {
                $update_service_array['statusCallback'] = $query_options['status_callback_url'] ;
                }              
            
            if (count($update_service_array) > 0) {
                $service_update = $this->_connection->messaging->v1->services($this->service['sid'])->update($update_service_array);
                }
            
            
            $this->Set_Message_API_Event($service_update) ; 
            $this->Set_Alert_Response(187) ; // Messaging service updated

            } catch ( Exception $e ) {             

                $this->Set_Message_API_Event($e->getMessage()) ; 
                $this->Set_Alert_Response(188) ; // Error updating messaging service
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($e->getMessage()),
                    'location' => __METHOD__
                    )) ;
                }        
        
        
        return $this ; 
        }    
    
    
    
    public function API_Action_Create_Notify_Service() {
        
        try {

            $create_notify_service = $this->_connection->notify->v1->services->create();

            $this->notify_service['sid'] = $create_notify_service->sid ;
            
            $this->Set_Message_API_Event($service_update) ; 
            $this->Set_Alert_Response(189) ; // Notify service successfully created

            } catch ( Exception $e ) {             

                $this->Set_Message_API_Event($e->getMessage()) ; 
                $this->Set_Alert_Response(190) ; // Unable to create notify service
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($e->getMessage()),
                    'location' => __METHOD__
                    )) ;            
                }        
                
        return $this ; 
        }
    
    
    public function API_Action_Update_Notify_Service($query_options = array()) {
        
        $notify_service_name = 'Notify Service for Msg # ID '.$this->phone_number['messaging_number_id'] ; 
        
        try {

            $update_notify_service_array = array(
                'friendlyName' => $notify_service_name,
                'messagingServiceSid' => $this->service['sid']
                ) ; 

            // Update a Notify Service
            $update_notify_service = $this->_connection->notify->v1->services($this->notify_service['sid'])->update($update_notify_service_array) ;

            
            $this->Set_Message_API_Event($service_update) ; 
            $this->Set_Alert_Response(192) ; // Phone number successfully provisioned

            } catch ( Exception $e ) {             

                $this->Set_Message_API_Event($e->getMessage()) ; 
                $this->Set_Alert_Response(191) ; // Messaging notify service could not be updated
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($e->getMessage()),
                    'location' => __METHOD__
                    )) ;            
                }        
        
        
        return $this ; 
        }
    
    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    //////////////////////
    
    // Check to see how many SMS segments the message would require
    // https://github.com/instasent/sms-counter-php
    public function Action_Count_Characters($message_content) {
        
        $smsCounter = new SMSCounter();
        $data['sms_counter'] = $smsCounter->count($message_content);
        
        return $data['sms_counter'] ;
        }
        
    
    
    // Update details of a specific message
    public function Action_Update_Message($message_input) {

        $continue = 1 ; 
        
        if (!$this->message_id) {
            $continue = 0 ; 
            $this->Set_Alert_Response(72) ; // Error saving contact note.
            $this->Append_Alert_Response('none',array(
                'admin_context' => 'No message_id provided.',
                'location' => __METHOD__
                )) ;             
            }
        
        if ($continue == 1) {
            
            $result = $this->Update_Message($message_input) ;
            
            
            // Assuming we attempted to update or create the note 
            // Provide a success or error
            if (!$result['error']) {

                $this->Set_Alert_Response(73) ; // Contact note created


                } else {

                    $this->Set_Alert_Response(72) ; // Error saving contact note.
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;            

                    }
            
            }
                
        return $this ; 
        }
    
    
    public function Update_Message($input) {
        
         
        $values_array = array() ; 
        
        if (count($input) > 0) {            
            foreach ($input as $key => $value) {
                $values_array[$key] = $value ; 
                }
            }
        
        unset($values_array['message_id']) ; 
        
        // Updates the contact in the database
        $query_array = array(
            'table' => 'messages',
            'values' => $values_array,
            'where' => "message_id='$this->message_id'"
            );


        
        $result = $this->DB->Query('UPDATE',$query_array);
        $this->message_query_result = $result ; 
        
        
        if ($result['results'] == true) {
            
//            $query_array['values']['note_id'] = $this->contact_note_id ;
//            
//            $history_input = array(
//                'contact_id' => $this->contact_id,
//                'function' => 'update_contact_note',
//                'notes' => json_encode($query_array['values'])
//                ) ; 
//        
//            $user_history = $this->Create_User_History($this->user_id,$history_input) ;             
            }
        
        
                
        return $result ; 
        
        }    


    
    // Compile message content and prep for delivery
    public function Action_Compile_Message($asset_id,$compile_options = array()) {
        
        $data['automation_record'] = $this->Get_Automation() ; 
        
        $asset_options = array(
            'skip_history' => 'yes'
            ) ;
        $this->Set_Asset_By_ID($asset_id,$asset_options) ; 
        $asset = $this->Get_Asset() ; 

        
        $this->message_content = $asset['message_content'] ; 
        
        if (isset($data['automation_record']['action_keys'])) { 
            $this->message_content = $this->Action_Personalize_Action_Keys($this->message_content,$data['automation_record']['action_keys']) ;
            }                
        
        // personalization_overrides process through after the other standard strings 
        // are added in the System personalization script. 
        if (isset($data['automation_record']['personalization_overrides'])) {
            $additional_personalization['personalization_overrides'] = $data['automation_record']['personalization_overrides'] ;
            } else if (isset($compile_options['personalization_overrides'])) {
                $additional_personalization['personalization_overrides'] = $compile_options['personalization_overrides'] ;    
                } else {
                    $additional_personalization = array() ; 
                    }        
        
        
        $this->Action_Personalize_Message('internal',$additional_personalization) ;

        return $this ; 
        }    
    
    
    // Personalize a message asset using User / Account information to replace wildcards
    // ** Note - Should only be personalizing user / account level info. Recipient level info should be personalized immediately before send with Action_Personalize_Message_Recipient
    public function Action_Personalize_Message($submission = 'internal',$additional_personalization = array()) {
        
        $personalization_array['account_record'] = $this->Get_Account() ; 
        $personalization_array['user_record'] = $this->Get_User() ; 
        $personalization_array['profile_record'] = $this->Get_Profile() ;
        $personalization_array['asset_record'] = $this->Get_Asset() ;
        
        
        foreach ($additional_personalization as $key => $value) {
            $personalization_array[$key] = $value ; 
            }

        if (isset($additional_personalization['personalization_overrides'])) {
            foreach ($additional_personalization['personalization_overrides'] as $override_set_title => $override_set_values) {
                $personalization_array[$override_set_title] = $override_set_values ; 
                }
            }
        
        
        
        $asset_array = array() ; 
        
        if ('internal' === $submission) {
            
            $asset_array['message_content'] = $this->message_content ; 
            
            } else {
        
                $asset_array['personalized_string'] = $submission ; 
            
                }
        
        foreach ($asset_array as $key => $value) {
            
            $asset_array[$key] = $this->Action_Personalize_String($value,$personalization_array) ;
            
            }
        
        // Return the personalized asset
        if ('internal' === $submission) {
            
            $this->message_content = $asset_array['message_content'] ;
            
            } else {
        
                $this->personalized_string = $asset_array['personalized_string'] ;
            
                }
        
        return $this ; 
        }

        
    
    
    // Create the array of recipients to send a message to
    // Will provide a single recipient (phone number) for individual delivery
    // Will also create bindings to enable bulk delivery
    // recipient_count parameter counts total recipients
    public function Action_Process_Recipients() {
        
        $continue = 1 ; 
        
        $recipient_to_array = $automation_recipient_array['to']['recipients'] ; 
        $recipient_cc_array = $automation_recipient_array['cc']['recipients'] ; 
        $recipient_bcc_array = $automation_recipient_array['bcc']['recipients'] ; 
        

        if ((count($recipient_to_array) > 0) OR (count($recipient_cc_array) > 0) OR (count($recipient_bcc_array) > 0)) {
            // We have recipients. Continue script
            
            } else {
                
                $continue = 0  ; 
                $this->Set_Alert_Response(150) ; // No recipients set
                }       
        
        if ($continue == 1) {
            
            // Create recipients...
            $recipient_binding = array() ;
            $recipient_single = '' ;
            $recipient_count = 0 ; 
            
            $i = 0 ; 
            foreach ($recipient_to_array as $recipient) {

                $recipient_single = $recipient['phone_number_international'] ; 
                
                $this_recipient = array(
                    'binding_type' => 'sms',
                    'address' => $recipient['phone_number_international']
                    ) ; 

                
                $recipient_to_array[$i]['binding'] = json_encode($this_recipient) ; 
                
                $recipient_binding[] = json_encode($this_recipient) ; 
                $recipient_count++ ; 
                $i++ ; 
                }
            
            $i = 0 ; 
            foreach ($recipient_cc_array as $recipient) {

                $recipient_single = $recipient['phone_number_international'] ; 
                
                $this_recipient = array(
                    'binding_type' => 'sms',
                    'address' => $recipient['phone_number_international']
                    ) ; 

                $recipient_cc_array[$i]['binding'] = json_encode($this_recipient) ; 
                
                $recipient_binding[] = json_encode($this_recipient) ; 
                $recipient_count++ ;
                $i++ ; 
                }
            
            $i = 0 ; 
            foreach ($recipient_bcc_array as $recipient) {

                $recipient_single = $recipient['phone_number_international'] ; 
                
                $this_recipient = array(
                    'binding_type' => 'sms',
                    'address' => $recipient['phone_number_international']
                    ) ; 

                $recipient_bcc_array[$i]['binding'] = json_encode($this_recipient) ; 
                
                $recipient_binding[] = json_encode($this_recipient) ; 
                $recipient_count++ ;
                $i++ ; 
                }
            
            
            $recipient_result = array(
                'recipient_count' => $recipient_count,
                'recipient_single' => $recipient_single,
                'recipient_binding' => $recipient_binding
                ) ; 
            
            $this->recipients = $recipient_result ; 
            
            }
        
        
        return $this ; 
        }
    
    

    // Determine eligibilty for message recipients, and save them to the recipients table
    public function Action_Create_Message_Recipients($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $data['automation_record'] = $this->Get_Automation() ; 

        
        $recipients_options = array(
            'override_paging' => 'yes'
            ) ; 
            
        
        $this->Action_Set_Campaign_Recipients_List($recipients_options) ;
        $recipient_delivery_array = $this->Get_Campaign_Recipients_List() ; 
        
//        error_log('campaign recipients array') ; 
//        error_log(json_encode($recipient_delivery_array)) ;
        
        // This creates the full list of recipients in the message table
        // Delivery script should pull from the message table to parse recipient sets, and merge IDs with contacts, and deliver
        $message_base_input = array(
            'automation_id' => $this->automation_id,
            ) ; 
            
        
        if (isset($query_options->test_mode)) {
            $message_base_input['test_mode'] = $query_options->test_mode ; 
            } 
        
        
        $result = $this->Create_Message_Recipients($message_base_input,$recipient_delivery_array) ;        
        
        $this->test_data = $result ; 

        

        $data['message_id'] = $result['insert_id'] ;
        $this->Set_Message_ID($data['message_id']) ; 
        
        
    

        // If recipients were successfully created, then change the status to Packaged
        // Looking for an error on result isn't the best way to do this, since that's just looking 
        // at the last recipient creation attempt
        if (!$result['error']) {
            $query_options = array(
                'automation_status_id' => 5
                ) ; 
            $data['automation_update'] = $this->Update_Automation($query_options) ; 
            $this->Set_Automation_By_ID() ; 
            $this->Set_Message_ID($data['message_id']) ; 
            }
        
        
//        $this->test_data = $data ;
        
        return $this ;
        }
    

    

    
    public function Action_Set_Recipient_Sets($options_array = array()) {
        
        $continue = 1 ; 
        
        $data['automation_record'] = $this->Get_Automation() ;
        $data['campaign_record'] = $this->Get_Campaign() ;

    
  
        // BANDWIDTH UPDATE REQUIRED
        // STATIC option may need to be addressed to bring it in line w/ what's happening in Email->Action_Set_Recipient_Sets()
        switch ($data['campaign_record']['delivery_trigger']) {
            case 'static':
                
                $recipient_query_options = array(
                    'filter_by_automation_id' => 'yes',
                    'filter_by_message_status_id' => array(4) // Scheduled status
                    ) ; 
                
                $data['recipient_result'] = $this->Retrieve_Message_Recipients($recipient_query_options) ; 
                $data['recipients'] = $data['recipient_result']['results'] ; 
 

                $i = 0 ; 
                foreach ($data['recipients'] as $recipient) {
                    
                    $recipient_variables = $data['campaign_record']['structured_data_01']['recipients'][$recipient['delivery_type']]['static']['recipient_variables'][$recipient['phone_number_international_recipient']] ;                    
                    $data['recipients'][$i] = array_merge($recipient,$recipient_variables);
                    $data['recipients'][$i]['phone_number_compiled']['international'] = $recipient['phone_number_international_recipient'] ; 
                    $i++ ; 
                    }
                
                
                break ; 
            default:
                
                $recipient_query_options = array(
                    'delivery_asset' => $data['campaign_record']['delivery_asset'],
                    'filter_by_automation_id' => 'yes',
                    'filter_by_message_status_id' => array(4) // Scheduled status
                    ) ; 

                $data['recipient_result'] = $this->Retrieve_Message_Recipients($recipient_query_options) ; 
                $data['recipients'] = $data['recipient_result']['results'] ;                                
            }
        
        
        
        
        
        if (!$data['automation_record']['automation_id']) {
            $continue = 0 ; 
            $this->Set_Alert_Response(181) ; // Asset - no automation item set
            }
        
        if ($continue == 1) {
            
            if (isset($options_array['override_send_time'])) {
                $send_time = $options_array['override_send_time'] ; 
                } else {
                    $send_time = $data['automation_record']['timestamp_next_action'] ; 
                    }

            // Create recipient sets to ensure a single API call is not made with more than $this->max_delivery_recipients 
            $recipient_sets = array() ; 
            $recipient_error_set = array() ; 
            $set = 0 ; // Set ID increment
            $i = 0 ; // # iteration through the do loop
            $total = 0 ; // count the total unique recipients added
            $set_total = 0 ; // count the total unique recipients added to the set

            $set_template = array(
                'to' => array(),
                'cc' => array(),
                'bcc' => array(),
                'bindings' => array()
                ) ; 
 
            
            if ($data['recipient_result']['result_count'] > 0) {
                
                $recipient_sets[$set] = $set_template ; 
                $recipient_sets[$set]['send_time'] = $send_time ;                 
                
                do {

                    $data['recipients'][$i] = $this->Action_Phone_Territorialize_Dataset($data['recipients'][$i],$data['recipients'][$i]['phone_country_dialing_code']) ; 
                    
                    if ($data['recipients'][$i]['do_not_contact_phone_number'] == 1) {
                        $data['recipients'][$i]['system_alert_id'] = 180  ; // Do not contact error
                        $recipient_error_set[] = $data['recipients'][$i] ; 
                        
                        } elseif ($data['recipients'][$i]['phone_number_compiled']['valid'] == 'no') {
                        
                            $data['recipients'][$i]['system_alert_id'] = 194 ; // Invalid phone number
                            $recipient_error_set[] = $data['recipients'][$i] ;                         
                    
                        } else {
                        
                        
                            // BANDWIDTH UPDATE REQUIRED
                        
                            $data['recipients'][$i]['binding'] = json_encode(array(
                                'message_id' => $data['recipients'][$i]['message_id'],
                                'binding_type' => 'sms',
                                'address' => $data['recipients'][$i]['phone_number_compiled']['international'],
                                'full_name' => $data['recipients'][$i]['full_name'],
                                'recipient_variables' => $data['recipients'][$i]
                                )) ; 
    
                            $recipient_sets[$set][$data['recipients'][$i]['delivery_type']][] = $data['recipients'][$i] ;
                            $recipient_sets[$set]['bindings'][] = $data['recipients'][$i]['binding'] ;
                            $set_total++ ; // Increment the set counter
                            $total++ ; // The total recipients added to all sets 
                            }


                    $i++ ; // Total results processed                

                    if ($set_total >= $this->max_delivery_recipients) {
                        $set++ ; // Increment the set when we get to max recipients in a set
                        $set_total = 0 ; // Reset the set total for the next loop around
                        if ($total < $data['recipient_result']['result_count']) {
                            $recipient_sets[$set] = $set_template ; 
                            if ($send_time != 'now') {
                                $recipient_sets[$set]['send_time'] = $send_time + 30 ;  // Add 30 seconds
                                } else {
                                    $recipient_sets[$set]['send_time'] = $send_time ; 
                                    } 
                            }
                        }

                    } while ($i < $data['recipient_result']['result_count']) ;  
                }
             
            // Process do not contact errors and mark them in emails table
            foreach ($recipient_error_set as $error_item) {

                $this->Set_Message_ID($error_item['message_id']) ; 
                $update_error_item = array(
                    'message_status_id' => 1, // Error
                    'phone_number_international_recipient' => $error_item['phone_number_compiled']['international'],
                    'recipient_name' => $error_item['full_name'],
                    'system_alert_id' => $error_item['system_alert_id'] // Error as provided in processing above
                    ) ; 
                $error_update = $this->Update_Message_Recipient($update_error_item) ; 
                }

            }
        

        
        $this->recipient_sets = $recipient_sets ;         
        $this->recipient_total_count = $total ;    
        
        return $this ;                  
        }
    
    
    
    // properties_array = the send properties sent to the email service
    // options_array = custom values that override defaults within the class (outside of email service options)
    public function Action_Queue_Message($properties_array = array(),$options_array = array()) {
        
        $continue = 1 ; 
        $delivery_error_array = array() ; 
        $recipient_variables = array() ; // Initialize recipient variables array 
        
        $data['campaign_record'] = $this->Get_Campaign() ;        
        $data['automation_record'] = $this->Get_Automation() ; 

        
        if ($this->user['user_id'] != $data['automation_record']['user_id']) {
            $this->Set_User_By_ID($data['automation_record']['user_id']) ;
            }

        if ($this->account['account_id'] != $data['automation_record']['account_id']) {
            $this->Set_Account_By_ID($data['automation_record']['account_id']) ;
            }

        if (($this->profile['account_id'] != $data['automation_record']['account_id']) OR ($this->profile['user_id'] != $data['automation_record']['user_id'])) {
            $this->Set_Profile_By_User_Account($data['automation_record']['user_id'],$data['automation_record']['account_id']) ;
            }


        
        // Compile the mesage to be sent
        $this->Action_Compile_Message($this->asset['asset_id']) ;                        
        
        // Create and add message_sender array
        if (isset($data['campaign_record']['structured_data_01']['messaging_number_id'])) {
            $messaging_number_id = $data['campaign_record']['structured_data_01']['messaging_number_id'] ; 
            } else {
                $messaging_number_id = $this->profile['messaging_number_id'] ; 
                }
        

        $this->Set_Message_Sender($messaging_number_id) ; 
        $data['sender_result'] = $this->Get_Response() ; 
        $data['message_sender'] = $this->Get_Message_Sender() ; 

        
        $data['recipient_sets'] = $this->Get_Recipient_Sets() ;             
        $data['delivery_result'] = array() ; 

        
        // If there are no recipients to send to, mark the automation item as complete
        if ($this->recipient_total_count == 0) {
            $continue = 0 ; 
            $automation_update_query_options = array(
                'automation_status_id' => 8, // Complete because there are no recipients to send to
                'error_count' => 0,
                'status_code' => 0,
                'status_result' => ''
                ) ; 
            $data['automation_update'] = $this->Update_Automation($automation_update_query_options) ; 
            $this->Set_Automation_By_ID() ;
            }
        

        
        if ($data['sender_result']['result'] != 'success') {
            $continue = 0  ; 
            
            // Update the automation item
            $query_options = array(
                'automation_status_id' => 1, // Error
                'error_count' => $data['automation_record']['error_count'] + 1, 
                'status_code' => $data['sender_result']['alert_id'],
                'status_result' => json_encode($data['sender_result']['alert_message_code'])
                ) ; 

            $data['automation_update'] = $this->Update_Automation($query_options) ;            
            } 


        if (($options_array['halt_delivery'] == 'yes') AND ($continue == 1)) {
            $continue = 0 ; 
            $this->Set_Alert_Response(151) ; // Delivery manually halted
            }
        
        
        
        if ($continue == 1) {
        
            // Now process each set of recipients as a separate delivery              
            $set = 0 ;         
            foreach ($data['recipient_sets'] as $recipient_set) {
            
                $properties_array = array() ; 
                $options_array = array() ; 
                
                $this->API_Action_Queue_Bulk_SMS(
                        $recipient_set['bindings'],
                        array(),
                        'internal', $recipient_set['send_time'], 
                        $properties_array,
                        $options_array
                        ) ;  // For scheduled send time use $data['automation_record']['timestamp_next_action']           
                
                
                $data['delivery_result'][] = $this->Get_Delivery_Result() ; 
                $data['delivery_result'][$set]['set_number'] = $set ; 

                $campaign_automation_input = array() ; 
                $campaign_input = array() ;  


                
                switch ($this->vendor_id) {
                    case 3: // Twilio    

                        if ($data['delivery_result'][$set]['message_status']['result'] == 'success') {

                                $message_content_input = array(
                                    'automation_id' => $data['automation_record']['automation_id'],
                                    'vendor_id' => $this->vendor_id,
                                    'message_sid' => $data['delivery_result'][$set]['message_sid'], 
                                    'message_content' => $data['delivery_result'][$set]['delivery_array']['message_content']
                                    ) ;  

                                $this->Action_Create_Message_Content($message_content_input) ;                


                                $data['delivery_result'][$set]['message_content_id'] = $this->Get_Message_Content_ID() ; 

                                $q = 0 ;
                                foreach ($recipient_set['to'] as $recipient) {

                                    $this->Set_Message_ID($recipient['message_id']) ; 
                                    $update_message_item = array(
                                        'message_content_id' => $data['delivery_result'][$set]['message_content_id'],
                                        'message_status_id' => 7, // Queued
                                        'phone_number_international_recipient' => $recipient['phone_number_compiled']['international'],
                                        'recipient_name' => $recipient['full_name'],
                                        'phone_number_international_sender' => $data['delivery_result'][$set]['message_sender']['phone_number_messaging'],
                                        'sender_name' => $data['delivery_result'][$set]['message_sender']['sender_name'],
                                        'timestamp_scheduled' => TIMESTAMP
                                        ) ; 
                                    $message_update = $this->Update_Message_Recipient($update_message_item) ; 
                                    $q++ ; 
                                    }                

                                } else {

                                    // Add the set number for this errored delivery. Will record it below.
                                    $delivery_error_array[] = $set ; 

                                    }                    

                        break ; 
                    case 9: // Bandwidth

                         
                        
                        $q = 0 ; 
                        foreach ($data['delivery_result'][$set]['bindings'] as $recipient_binding) {
                            
                            // Decode the recipient binding from json
                            $recipient_binding_decoded = json_decode($recipient_binding,true)  ;

                            if ($recipient_binding_decoded['message_status']['result'] == 'success') {


                                    $message_content_input = array(
                                        'automation_id' => $data['automation_record']['automation_id'],
                                        'vendor_id' => $this->vendor_id,
                                        'message_sid' => $recipient_binding_decoded['response_status']['id'], 
                                        'message_content' => $data['delivery_result'][$set]['delivery_array']['message_content'],
                                        'message_media' => json_encode($recipient_binding_decoded['message_media']),
                                        'segment_count' => $recipient_binding_decoded['response_status']['segmentCount']
                                        ) ;  

                                    $this->Action_Create_Message_Content($message_content_input) ;                


                                    $recipient_binding_decoded['message_content_id'] = $this->Get_Message_Content_ID() ; 


                                    $this->Set_Message_ID($recipient_binding_decoded['message_id']) ; 
                                    $update_message_item = array(
                                        'message_content_id' => $recipient_binding_decoded['message_content_id'],
                                        'message_sid' => $recipient_binding_decoded['response_status']['id'],
                                        'message_status_id' => 7, // Queued
                                        'phone_number_international_recipient' => $recipient_binding_decoded['address'],
                                        'recipient_name' => $recipient_binding_decoded['full_name'],
                                        'phone_number_international_sender' => $data['delivery_result'][$set]['message_sender']['phone_number_messaging'],
                                        'sender_name' => $data['delivery_result'][$set]['message_sender']['sender_name'],
                                        'timestamp_scheduled' => TIMESTAMP
                                        ) ; 
                                    $this->Update_Message_Recipient($update_message_item) ;                 
                                    
                                    $q++ ; 
                                    } else {

                                        $this->Set_Message_ID($recipient_binding_decoded['message_id']) ; 
                                
                                        $update_message_item = array(
                                            'phone_number_international_recipient' => $recipient_binding_decoded['address'],
                                            'recipient_name' => $recipient_binding_decoded['full_name'],
                                            'phone_number_international_sender' => $data['delivery_result'][$set]['message_sender']['phone_number_messaging'],
                                            'sender_name' => $data['delivery_result'][$set]['message_sender']['sender_name'],
                                            ) ; 
                                        $message_update = $this->Update_Message_Recipient($update_message_item) ;                                
                                
                                    
                                        $error_item = array(
                                            'message_id' => $recipient_binding_decoded['message_id'],
                                            'response_message' => $recipient_binding_decoded['response_message']
                                            ) ; 
                                
                                        // Add the set number for this errored delivery. Will record it below.
                                        $delivery_error_array[] = $error_item ; 

                                        }                            
                            
                            }
                        
                        break ; 
                    }               
                
                $set++ ; 
                }
            
            

            if ($data['campaign_record']['structured_data_01']['campaign_options']['cost'] != 'free') {

                $credit_calculation = array(
                    'action' => 'sms',
                    'recipient_count' => $q
                    ) ; 

                $data['account_marketing_credits'] = $this->Action_Calculate_Marketing_Credits($credit_calculation)->Get_Account_Marketing_Credits() ; 
                $account_update_query = array(
                    'marketing_credit_balance' => $data['account_marketing_credits']['projected_balance']
                    ) ;

                $marketing_credit_increment = $this->Action_Update_Account($account_update_query)->Set_Account() ;                    
                }              
            
            
            $this->message_action = $data['delivery_result'] ; // Set the results of the delivery


            // Update the automation item following the delivery...
            $this->Action_Set_Recipient_Sets($options_array) ;  // Reset the recipient sets to see if there are any additional that need to be sent to

            
            
            // If there are no additional recipients to send to (after delivery attempt), mark the automation item as complete
            if ($this->recipient_total_count == 0) {
                $query_options = array(
                    'automation_status_id' => 8, // Complete because there are no recipients to send to
                    'error_count' => 0,
                    'status_code' => 0,
                    'status_result' => json_encode($recipient_variables)
                    ) ; 
                $data['automation_update'] = $this->Update_Automation($query_options) ; 
                
                
                $asset_record = $this->Get_Asset() ; 
                $update_delivery_count = array(
                    'asset_id' => $asset_record['asset_id'],
                    'deliveries' => $asset_record['deliveries'] + 1,
                    'timestamp_last_delivered' => TIMESTAMP,
                    'auth_override' => 'yes',
                    'asset_input_info' => array(
                        'rewrite_latest_history' => 'yes'
                        )
                    ) ;  

                if ($data['campaign_record']['structured_data_01']['campaign_options']['cost'] != 'free') {
                    $this->Action_Update_Content($update_delivery_count) ;            
                    }                
                
                }        


            // If there are delivery errors, create an error message status_result for the automation item
            if (count($delivery_error_array) > 0) {

                $automation_item = $this->Get_Automation() ; 

                switch ($this->vendor_id) {
                    case 3: // Twilio                         
                        
                        $status_result = array() ; 
                        foreach ($delivery_error_array as $error_set_number) {
                            $status_code = $data['delivery_result'][$error_set_number]['message_status']['alert_id'] ; // This will only capture the LAST error code in the set of errors
                            $status_result[] = $data['delivery_result'][$error_set_number]['message_status'] ;                  
                            }

                        break ; 
                    case 9: // Bandwidth
                        
                        $status_result = array() ; 
                        foreach ($delivery_error_array as $error_item) {
//                            $status_code = $data['delivery_result'][$error_set_number]['message_status']['alert_id'] ; // This will only capture the LAST error code in the set of errors
                            $status_result[] = $error_item ;                  
                            }
                        
                        break ;                         
                    }
                        
                        
                // Update the automation item
                $query_options = array(
                    'automation_status_id' => 1, // Error
                    'error_count' => $automation_item['error_count'] + 1, 
                    'status_code' => $status_code,
                    'status_result' => json_encode($status_result)
                    ) ; 

                $data['automation_update'] = $this->Update_Automation($query_options) ;            
                }

                $this->Set_Automation_By_ID() ;            


            } else {

                $result = $this->Get_Response() ; 

                $delivery_result['message_status'] = $result ;                 
                $this->delivery_result = $delivery_result ; 
                }
        
            

        return $this ;
        }
    
    
    
    
    
    // properties_array = the send properties sent to the message service
    // options_array = custom values that override defaults within the class (outside of message service options)
    public function Action_Process_Delivery($campaign_id,$automation_id,$asset_id,$properties_array = array(),$options_array = array()) {
        
        $continue = 1 ; 
        
        $recipient_variables = array() ; // Initialize recipient variables array 
            
        
        $this->Set_Campaign_By_ID($campaign_id) ; 
        $data['campaign_record'] = $this->Get_Campaign() ;        

        $this->Set_Automation_By_ID($automation_id) ; 
        $data['automation_record'] = $this->Get_Automation() ;         

        
        if ($this->user['user_id'] != $data['automation_record']['user_id']) {
            $this->Set_User_By_ID($data['automation_record']['user_id']) ;
            }

        if ($this->account['account_id'] != $data['automation_record']['account_id']) {
            $this->Set_Account_By_ID($data['automation_record']['account_id']) ;
            }

        if (($this->profile['account_id'] != $data['automation_record']['account_id']) OR ($this->profile['user_id'] != $data['automation_record']['user_id'])) {
            $this->Set_Profile_By_User_Account($data['automation_record']['user_id'],$data['automation_record']['account_id']) ;
            }
        
        if ($this->asset['asset_id'] != $asset_id) {
            $asset_options = array(
                'skip_history' => 'yes'
                ) ; 
            $this->Set_Asset_By_ID($asset_id,$asset_options) ;
            }
        
        
        
        // Scheduled -- Compile and save recipients to recipients table
        if ($data['automation_record']['automation_status_id'] == 4) {
            $this->Action_Create_Message_Recipients() ; 
            $data['automation_record'] = $this->Get_Automation() ; 
            }
        
        // Packaged -- Create recipient sets from recipients table and attempt delivery
        if (($data['automation_record']['automation_status_id'] == 5) OR ($data['automation_record']['automation_status_id'] == 1)) {
            
            $this->Action_Set_Recipient_Sets($options_array) ;  // Set the recipient sets
            $this->Action_Queue_Message($properties_array,$options_array) ; 
            }        
        

        return $this ;
        }    
    
    
    
    
    public function Action_Create_Message_Content($message_content_input = array()) {
        
        if (isset($message_content_input['message_content'])) {
            $content_options['target'] = "_blank" ; 
            $message_content_input['message_content'] = Utilities::Format_URL_To_Link($message_content_input['message_content'],$content_options) ; 
            }
        
        $result = $this->Create_Message_Content($message_content_input) ;
        
        
        if ($result['insert_id']) {
            
            $this->message_content_id = $result['insert_id'] ; 
            
            } else {
                $this->message_content_id = $result['error'] ; 
                }

        return $this ; 
        }
    

    
    
    // Must have phone number set
    public function Action_Check_Number_Provision_Status($query_options = array()) {
    
        $phone_number = $this->Get_Messaging_Phone_Number() ;

        switch ($this->vendor_id) {
            case 9: // Bandwidth

                $order_response = $this->_connection->orders()->order($phone_number['number_order_id'], true); // tndetail=true
                $order = $order_response->Order; // Object
                        
                $order_result = $order_response->to_array() ; 
                
//                print_r($phone_number['number_order_id']) ; 
//                print_r($order_result) ; 
                
                switch ($order_result['OrderStatus']) {
                    case 'FAILED': // Request failed. Delete entry from messaging numbers table
                    case 'PARTIAL': // 
                    case 'BACKORDERED': //                         
                        
                        $order_result['ErrorList']['Error']['number_order_id'] = $phone_number['number_order_id'] ; // Add Bandwidth order id to the support response array for context

                        $this->Set_Alert_Response(166) ; // Error or temp error provisioning number at Vendor
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($order_result['ErrorList']['Error']),
                            'location' => __METHOD__
                            )) ;

                        $update_phone_number_query_options = array(
                            'messaging_phone_active' => 0,
                            'number_order_status' => $order_result['OrderStatus'],
                            'number_info' => json_encode($order_result['ErrorList']['Error'])
                            ) ; 

                        $data['update_result'] = $this->Update_Messaging_Phone_Number($update_phone_number_query_options) ; 

                        break ; 
                    case 'RECEIVED':
                        
                        $order_result['ErrorList']['Error']['number_order_id'] = $phone_number['number_order_id'] ; // Add Bandwidth order id to the support response array for context

                        $this->Set_Alert_Response(258) ; // Order still processing at vendor
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($order_result['ErrorList']['Error']),
                            'location' => __METHOD__
                            )) ;

                        $update_phone_number_query_options = array(
                            'messaging_phone_active' => 0,
                            'number_order_status' => $order_result['OrderStatus'],
                            'number_info' => json_encode($order_result['ErrorList']['Error'])
                            ) ; 

                        $data['update_result'] = $this->Update_Messaging_Phone_Number($update_phone_number_query_options) ; 

                        
                        break ; 
                    case 'COMPLETE': // Number order successful. Make active.

                        $update_phone_number_query_options = array(
                            'messaging_phone_active' => 1,
                            'number_order_status' => $order_result['OrderStatus'],
                            'number_info' => ''
                            ) ; 

                        $update_phone_number = $this->Update_Messaging_Phone_Number($update_phone_number_query_options) ; 

                        $this->Set_Alert_Response(167) ; // Number provisioned
                        
                        break ;
                    }  
                
                
                // Set the messaging number
                $retrieve_phone_number_query_options = array(
                    'filter_by_messaging_number_id' => $phone_number['messaging_number_id']
                    ) ; 
                $this->Set_Messaging_Phone_Number($retrieve_phone_number_query_options) ;
                
                break ;     
            }

        return $this ; 
        }
    
    
    
    public function Action_Provision_Number($query_options = array()) {
        
        $continue = 1 ;
        
        if (!$query_options['phone_number']) {
            $continue = 0 ;
            $this->Set_Alert_Response(168) ; // Error provisioning phone number - no number selected   
            }
        
        if (!$query_options['country_id']) {
            $continue = 0 ;
            $this->Set_Alert_Response(169) ; // Error provisioning phone number - country_id  
            }
        
        if (!$this->profile_id) {
            $continue = 0 ;
            $this->Set_Alert_Response(170) ; // Error provisioning phone number - profile_id  
            }
        
        if ($continue == 1) {
            
            $this->API_Action_Provision_Number($query_options['phone_number']) ; 
            
            $api_result = $this->Get_Message_API_Event() ; 
            $result = $this->Get_Response() ; 
            
            // If provisioning the number was successful, add it to a Profile and create notifcation service
            if ($result['result'] == 'success') {
                
                $phone_number_raw = $this->Action_Phone_Territorialize_Dataset($query_options) ; 
                
                switch ($this->vendor_id) {
                    case 3: // Twilio
                        
                        
                        $create_phone_number_query_options = array(
                            'messaging_phone_vendor_id' => $this->vendor_id,
                            'messaging_phone_country_id' => $query_options['country_id'],
                            'phone_number_messaging' => $phone_number_raw['phone_number_compiled']['international'],
                            'messaging_phone_number_sid' => $phone_number['sid'],
                            'number_order_status' => 'COMPLETE',
                            'messaging_phone_active' => 1
                            ) ; 

                        $create_phone_number = $this->Create_Messaging_Phone_Number($create_phone_number_query_options) ; 
                        
                        break ; 
                    case 9: // Bandwidth

                        $create_phone_number_query_options = array(
                            'messaging_phone_vendor_id' => $this->vendor_id,
                            'messaging_phone_country_id' => $query_options['country_id'],
                            'phone_number_messaging' => $phone_number_raw['phone_number_compiled']['international'],
                            'number_order_id' => $api_result->Order->id,
                            'number_order_status' => 'RECEIVED',
                            'messaging_phone_active' => 0
                            ) ; 

                        $create_phone_number = $this->Create_Messaging_Phone_Number($create_phone_number_query_options) ; 
                        
                        break ; 
                    }
                
                } 
            
            
            
            // If saving the number was successful and belongs to Bandwidth, query to see if order completed
            if ($create_phone_number['insert_id']) {
                
                $retrieve_phone_number_query_options = array(
                    'filter_by_messaging_number_id' => $create_phone_number['insert_id']
                    ) ; 
                    
                $this->Set_Messaging_Phone_Number($retrieve_phone_number_query_options) ; 
                $phone_number = $this->Get_Messaging_Phone_Number() ;
                
                
                $this->Action_Check_Number_Provision_Status() ; 
                $phone_number = $this->Get_Messaging_Phone_Number() ;
                

                // Need to query Bandwidth at this point to see if the order succeeded
                switch ($phone_number['number_order_status']) {
                    case 'RECEIVED': // Try it again in 2 seconds to see if it is complete now.
                        
                        sleep(2) ; 
                        $this->Action_Check_Number_Provision_Status() ; 
                        $phone_number = $this->Get_Messaging_Phone_Number() ;
                        break ;                         
                    }
                }
            

            // If saving the number was successful
            if ($phone_number['messaging_number_id']) {
                
                $phone_number = $this->Get_Messaging_Phone_Number() ; 
                
                
                switch ($phone_number['number_order_status']) {
                    case 'PARTIAL': // 
                    case 'BACKORDERED': //                         
                        
                        $this->Set_Alert_Response(166) ; // Error or temp error provisioning number at Vendor
                        
                        break ; 
                    case 'FAILED': 
                        
                        $this->Set_Alert_Response(259) ; // Request failed. Number not available error
                        
                        break ; 
                    case 'RECEIVED':
                        
                        $this->Set_Alert_Response(258) ; // Order still processing at vendor
                        
                        break ; 
                    case 'COMPLETE': // Number order successful. Make active.

                        // If current primary messaging number is not set, then set new number as the primary
                        if ($phone_number['messaging_number_id_primary'] == 0) {

                            $profile_update_input = array(
                                'messaging_number_id' => $phone_number['messaging_number_id']
                                ) ; 

                            $this->Action_Update_Profile($profile_update_input) ; 

                            $this->Set_Messaging_Phone_Number($retrieve_phone_number_query_options) ; 
                            $phone_number = $this->Get_Messaging_Phone_Number() ;
                            }
                        
                        $this->Set_Alert_Response(167) ; // Number provisioned
                        
                        break ;
                    }                
                
                
                
                }            
            }
        
         
        
        return $this ;        
        }
    
    
    public function Action_Release_Number($query_options) {
        
        $continue = 1 ;
        
        if (!$this->phone_number['messaging_number_id']) {
            $continue = 0 ;
            $this->Set_Alert_Response(178) ; // No messaging number selected for processing  
            }
        
        if ($continue == 1) {
            
            $data['phone_number'] = $this->Get_Messaging_Phone_Number() ; 
            
            $this->API_Action_Release_Number($data['phone_number']['messaging_phone_number_sid']) ; 
            
            
            $result = $this->Get_Response() ; 

            if ($result['result'] == 'success') {
                
                $personalize['profile_record']['messaging_phone_number'] = $data['phone_number']['phone_number_messaging_compiled']['international_pretty'] ; 
                $this->Append_Alert_Response($personalize) ;                
                
                
                // If releasing the number was successful, mark it as inactive 
                $update_phone_number_query_options = array(
                    'messaging_phone_active' => 0
                    ) ; 
                
                $update_phone_number = $this->Update_Messaging_Phone_Number($update_phone_number_query_options) ; 
                $data['update_result'] = $update_phone_number ; 
                
                 
                
                // If the released number was the primary, it needs to be dropped from the profile
                if ($data['phone_number']['primary_number'] == 1) {
                    
                    $list_query_options['filter_by_messaging_phone_active'] = 1 ; 
                    $data['phone_numbers_list'] = $this->Retrieve_Messaging_Phone_Numbers_List($list_query_options) ; 
                    if ($data['phone_numbers_list']['result_count'] > 0) {
                        
                        // Update the profile w/ the first messaging number as primary
                        $profile_input = array(
                            'messaging_number_id' => $data['phone_numbers_list']['results'][0]['messaging_number_id']
                            ) ; 
                        $this->Action_Update_Profile($profile_input) ; 
                        
                        } else {
                        
                            // Set the profile messaging_number_id as 0
                            $profile_input = array(
                                'messaging_number_id' => 0 
                                ) ; 
                            $this->Action_Update_Profile($profile_input) ;
                            }
                    
                    }
                
                
                // Create History Entry
                $history_input = array(
                    'account_id' => $this->profile['account_id'],
                    'profile_id' => $this->profile_id,
                    'function' => 'message_release_number',
                    'notes' => json_encode($data['phone_number'])
                    ) ; 
                $user_history = $this->Create_User_History($this->user_id,$history_input) ;  
                
                } 
            
            }
        
        $this->test = $data ; 
        
        return $this ;        
        }
    
    

    
    
    public function Action_Create_Messaging_Service() {
        
        global $server_config ; // Need this to grab the default app domain
        
        $continue = 1 ;
        
        if (!$this->profile) {
            $continue = 0 ;
            $this->Set_Alert_Response(170) ; // Error provisioning phone number - profile_id  
            }

        if (!$this->profile_id) {
            $continue = 0 ;
            $this->Set_Alert_Response(170) ; // Error provisioning phone number - profile_id  
            }
        
        if (!$this->phone_number['messaging_number_id']) {
            $continue = 0 ;
            $this->Set_Alert_Response(178) ; // Missing messaging_number_id
            }
        
        if ($this->phone_number['messaging_service_id']) {
            $continue = 0 ;
            $this->Set_Alert_Response(193) ; // Messing messaging_service_id
            }
    
        if ($continue == 1) {
            
            $service_create_options['messaging_service_name'] = 'Service for Profile '.$this->profile_id.' - Messaging Number ID '.$this->phone_number['messaging_number_id'].' - Account '.$this->profile['account_id'] ;
            
            $this->API_Action_Create_Messaging_Service($service_create_options) ; 
            
            $result = $this->Get_Response() ; 
            
            // If provisioning the number was successful, add it to a Profile and create notifcation service
            if ($result['result'] == 'success') {
                
                $update_phone_number_query_options = array(
                    'messaging_service_id' => $this->service['sid']
                    ) ; 
                
                $update_phone_number = $this->Update_Messaging_Phone_Number($update_phone_number_query_options) ; 
                $this->message_query_result = $update_phone_number ; 
                
                
                $service_update_query_options = array(
                    'messaging_phone_number_sid' => $this->phone_number['messaging_phone_number_sid'],
                    'webhook_url' => 'https://'.$server_config['domain']['app_domain']['hostname'].'/events/twilio',
                    'status_callback_url' => 'https://'.$server_config['domain']['app_domain']['hostname'].'/events/twilio'
                    ) ; 
                
                $this->API_Action_Update_Messaging_Service($service_update_query_options) ; 
                $result_service = $this->Get_Response() ;                
                
                } 
            
            // If creating and updating the messaging service was successful, then create a notify service
            if ($result_service['result'] == 'success') {
                
                $this->API_Action_Create_Notify_Service() ;
                $this->API_Action_Update_Notify_Service() ;
                
                $update_phone_number_query_options = array(
                    'messaging_notify_service_id' => $this->notify_service['sid']
                    ) ; 
                
                $update_phone_number = $this->Update_Messaging_Phone_Number($update_phone_number_query_options) ; 
                $this->message_query_result = $update_phone_number ;
                
                }            
            }
        
        return $this ;        
        }    
    
    
    
    public function Update_Messaging_Phone_Number($query_options = array()) {
        

        $messaging_number_id = $this->messaging_phone_number['messaging_number_id'] ; 
        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $values_array['timestamp_number_updated'] = TIMESTAMP ; 
        
        $query_array = array(
            'table' => 'user_messaging_numbers',
            'values' => $values_array,
            'where' => "user_messaging_numbers.messaging_number_id='$messaging_number_id'"
            ) ; 
        
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->message_query_result = $result ; 
        
        return $result ; 
        
        }
    


    public function Delete_Messaging_Phone_Number($messaging_number_id) {
        
        $query_array = array(
            'table' => "user_messaging_numbers",
            'where' => "user_messaging_numbers.messaging_number_id='$messaging_number_id'"
            );
        
        $result = $this->DB->Query('DELETE',$query_array);
        $this->message_query_result = $result ;
        
        return $result ;         
        }    
    
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    ////////////////////// 
    
    
    
    // Create entries of recipients that will be receive an email as part of an automation delivery
    public function Create_Message_Recipients($query_options = array(),
        $recipient_delivery_array = array()
        ) { 
        

        foreach ($recipient_delivery_array as $recipient) {
            
            // Normal message deliveries should be logging:
            // message_status_id
            // member_id_value
            // delivery_type
            
            $query_array = array(
                'table' => "messages",
                'values' => array()                
                );

            
            foreach ($recipient as $key => $value) {
                
                switch ($key) {
                    case 'delivery_type':
                    case 'member_id_value':
                    case 'member_id':
                    case 'message_status_id':
                    case 'automation_id':
                    case 'system_alert_id':  
                    case 'phone_number_international_recipient':  
                    case 'recipient_name':      
                        $query_array['values'][$key] = $value ;         
                        break ; 
                    }                
                }
            
            foreach ($query_options as $query_key => $query_value) {
                $query_array['values'][$query_key] = $query_value ; 
                }
          
            $query_array['values']['timestamp_scheduled'] = TIMESTAMP ; 
            

            $result = $this->DB->Query('INSERT',$query_array) ;
            $this->message_query_result = $result ;            

            
            // There is an error in pre-delivery, so mark the message as error and create an error event
            if (($query_array['values']['system_alert_id']) AND ($recipient['delivery_type'] != 'incoming')) {
                
                $data['campaign_record'] = $this->campaign ; 
                $message_asset = new Asset() ; 
                $message_asset->Set_Asset_By_ID($data['campaign_record']['asset_id']) ; 
                $data['asset_record'] = $message_asset->Get_Asset() ; 
                
                $message_content_input = array(
                    'automation_id' => $this->automation_id,
                    'vendor_id' => $this->vendor_id,
                    'message_content' => $data['asset_record']['message_content']
                    ) ; 
                
                $this->Action_Create_Message_Content($message_content_input) ; 

                // Update the message recipient with the new message_content_id and sender / recipient info
                $this->Set_Message_ID($result['insert_id']) ; 
                $update_message_item['message_content_id'] = $this->Get_Message_Content_ID() ; 
                $data['message_update'] = $this->Update_Message_Recipient($update_message_item) ;

                
                $response = $this->Set_Alert_Response($query_array['values']['system_alert_id'])->Get_Response() ; // 

                $message_event_query_options = array(
                    'message_id' => $result['insert_id'],
                    'event' => 'error',
                    'event_message' => $response['alert_message_clean']
                    ) ; 

                $data['message_event'] = $this->Create_Message_Event($message_event_query_options) ; 
                }
            
            } 
        
        return $result ; 
        }
    


    // CREATE entry of a single domain associated with an account
    public function Create_Message_Content($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
         
        
        $query_array = array(
            'table' => "message_content",
            'values' => array(
                'automation_id' => $query_options->automation_id,
                'vendor_id' => $query_options->vendor_id,
                'message_sid' => $query_options->message_sid,
                'message_content' => $query_options->message_content,
                'message_media' => $query_options->message_media,
                'segment_count' => $query_options->segment_count
                )
            );

        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->message_query_result = $result ; 
        
        return $result ; 
        } 
    
    
    
    // Save a new messaging phone number to the database
    public function Create_Messaging_Phone_Number($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
         
        
        $query_array = array(
            'table' => "user_messaging_numbers",
            'values' => array(
                'profile_id' => $this->profile_id,
                'timestamp_number_created' => TIMESTAMP,
                'timestamp_number_updated' => TIMESTAMP
                )
            );

        foreach ($query_options as $key => $value) {
            $query_array['values'][$key] = $value ; 
            }
        
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->message_query_result = $result ; 
        
        return $result ; 
        } 
    
    
    
    // Retrieve a list of saved phone numbers and their associated user profiles
    public function Retrieve_Messaging_Phone_Numbers_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
                
        
        $query_array = array(
            'table' => 'user_messaging_numbers',
            'fields' => "user_messaging_numbers.*, 
                accounts.*, 
                list_countries.country_dialing_code, 
                user_profiles.messaging_number_id AS messaging_number_id_primary",
            'where' => "user_messaging_numbers.messaging_number_id>0 "
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_profiles',
            'on' => 'user_profiles.profile_id',
            'match' => 'user_messaging_numbers.profile_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'accounts',
            'on' => 'accounts.account_id',
            'match' => 'user_profiles.account_id'
            );        
     
        $query_array['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'user_messaging_numbers.messaging_phone_country_id',
            'match' => 'list_countries.country_id'
            );
        
        
        if(isset($query_options->filter_by_messaging_number_id)) {
            $query_array['where'] .= " AND user_messaging_numbers.messaging_number_id='$query_options->filter_by_messaging_number_id'"  ; 
            } 
        
        if(isset($query_options->filter_by_messaging_phone_active)) {
            $query_array['where'] .= " AND user_messaging_numbers.messaging_phone_active='$query_options->filter_by_messaging_phone_active'"  ; 
            }
        
        if(isset($query_options->filter_by_phone_number_messaging)) {
            $query_array['where'] .= " AND user_messaging_numbers.phone_number_messaging='$query_options->filter_by_phone_number_messaging'"  ; 
            }
        
        if(isset($query_options->filter_by_profile_id)) {
            $query_array['where'] .= " AND user_messaging_numbers.profile_id='$query_options->filter_by_profile_id'"  ; 
            }
        
        if(isset($query_options->filter_by_account_id)) {
            $query_array['where'] .= " AND user_profiles.account_id='$query_options->filter_by_account_id'"  ; 
            }
        
        if(isset($query_options->filter_by_user_id)) {
            $query_array['where'] .= " AND user_profiles.user_id='$query_options->filter_by_user_id'"  ; 
            }        
        
        if(isset($query_options->filter_by_primary_messaging_number_id)) {
            $query_array['where'] .= " AND user_profiles.messaging_number_id='$query_options->filter_by_primary_messaging_number_id'"  ; 
            }        
               
        
        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $this->message_query_result = $result ; 
        
        
        // Get the total count of items in this query
        if (isset($query_options->total_count)) { 
            
            $result['total_count'] = $query_options->total_count ; 
            
            } else {                
                    
                $query_array['fields'] = "user_messaging_numbers.messaging_number_id" ; 
                unset($query_array['limit'],$query_array['offset']) ;
            
                $count = $this->DB->Query('SELECT_JOIN',$query_array,'force');

                $result['total_count'] = $count['result_count'] ;
                $result['uber_result_set'] = $count['results'] ;            
            
            
                }
        
        $result['offset_page'] = $query_options->offset_page ;         
        
        $this->Set_Message_Paging($result) ;        
        
        return $result ; 
        }    
    
    
    
    
    
    
    // Retrieve the eligible recipients of a message
    public function Retrieve_Message_Recipients($query_options = array()) {
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $distribution_sublist_sms = $query_options->delivery_asset['distribution_sublist_sms'] ; // SMS sublist asset_id
        
        

        $query_array = array(
            'table' => "messages",
            'fields' => "messages.*, asset_campaign_automation.*, message_content.*",
            'where' => "messages.message_id>0 "
            );

        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => 'asset_campaign_automation.automation_id',
            'match' => 'messages.automation_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'message_content',
            'on' => 'message_content.message_content_id',
            'match' => 'messages.message_content_id',
            'type' => 'left'
            );    
        
        if ($query_options->filter_by_automation_id == 'yes') {
            $query_array['where'] .= " AND messages.automation_id='$this->automation_id'" ; 
            }
                
        if (isset($query_options->message_id)) {
            $query_array['where'] .= " AND messages.message_id='$query_options->message_id'" ; 
            }
        
        
        // Should be set as an array of status IDs
        if (isset($query_options->filter_by_message_status_id)) {
            $message_status_id_query_string = ' AND (' ; 
            foreach ($query_options->filter_by_message_status_id as $status_id) {
                $message_status_id_query_string .= "messages.message_status_id='$status_id' OR " ; 
                }
            $message_status_id_query_string = rtrim($message_status_id_query_string," OR ") ; 
            $message_status_id_query_string .= ')' ; 
            $query_array['where'] .= $message_status_id_query_string ; 
            }
        

        if (!isset($query_options->delivery_asset)) {
            $pre_query_array = $query_array ; 
            $pre_query_array['fields'] = "messages.automation_id"  ;
            $result = $this->DB->Query('SELECT_JOIN',$pre_query_array) ;
            
            if (!$this->automation_id) {
                $this->Set_Automation_By_ID($result['results']['automation_id']) ;    
                } else {
                    $this->Set_Automation_By_ID($this->automation_id) ;
                    }
            
            $automation = $this->Get_Automation() ; 
            $this->Set_Campaign_By_ID($automation['campaign_id']) ;
            $campaign = $this->Get_Campaign() ; 
            $query_options->delivery_asset = $campaign['delivery_asset'] ; 
            }
        
        switch ($query_options->delivery_asset['type_id']) {
            case 19: // distribution list - contacts
            case 26: // series distribution list - contacts        
                $query_array['fields'] .= ", contacts.*, 
                        contact_restrictions_email.do_not_contact AS do_not_contact_email_address, 
                        contact_restrictions_phone.do_not_contact AS do_not_contact_phone_number, 
                        CONCAT(contacts.first_name,' ',contacts.last_name) AS full_name, 
                        asset_members.timestamp_member_created, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code " ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'contacts',
                    'on' => 'contacts.contact_id',
                    'match' => 'messages.member_id_value'
                    ) ; 
                       
                $query_array['join_tables'][] = array(
                    'table' => 'asset_members',
                    'on' => 'asset_members.member_id',
                    'match' => 'messages.member_id',
                    'type' => 'left'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'system_visibility_levels',
                    'on' => 'system_visibility_levels.visibility_id',
                    'match' => 'contacts.visibility_id'
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'contact_restrictions AS contact_restrictions_email',
                    'statement' => '(contact_restrictions_email.email_address=contacts.email_address AND contact_restrictions_email.account_id=contacts.account_id AND contact_restrictions_email.contact_id=contacts.contact_id)',
                    'type' => 'left'
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'contact_restrictions AS contact_restrictions_phone',
                    'statement' => '(contact_restrictions_phone.phone_number_international=CONCAT(contacts.phone_country_dialing_code,contacts.phone_number) AND contact_restrictions_phone.account_id=contacts.account_id AND contact_restrictions_phone.contact_id=contacts.contact_id)',
                    'type' => 'left'
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'list_countries',
                    'on' => 'list_countries.country_id',
                    'match' => 'contacts.country_id',
                    'type' => 'left'
                    );                
                break ;
            case 20: // distribution list - users
            case 28: // User SERIES list
                
                $query_array['fields'] .= ", users.*, 
                        CONCAT(users.first_name,' ',users.last_name) AS full_name, 
                        user_restrictions_phone.do_not_contact AS do_not_contact_phone_number, 
                        user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                        asset_members.timestamp_member_created, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code " ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'users',
                    'on' => 'users.user_id',
                    'match' => 'messages.member_id_value'
                    ) ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'asset_members',
                    'on' => 'asset_members.member_id',
                    'match' => 'messages.member_id',
                    'type' => 'left'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_phone_numbers',
                    'on' => 'users.phone_number_id',
                    'match' => 'user_phone_numbers.phone_number_id',
                    'type' => 'left'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_restrictions AS user_restrictions_phone',
                    'statement' => '(user_restrictions_phone.phone_number_international=CONCAT(user_phone_numbers.phone_country_dialing_code,user_phone_numbers.phone_number) AND user_restrictions_phone.user_id=users.user_id)',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'list_countries',
                    'on' => 'list_countries.country_id',
                    'match' => 'users.country_id',
                    'type' => 'left'
                    );                
                break ;
            case 23: // distribution list - account user profiles
            case 27: // series distribution list - profiles  
                
                $query_array['fields'] .= ", users.user_id, 
                        user_profiles.*, 
                        accounts.account_display_name, accounts.account_username, 
                        FORMAT(accounts.marketing_credit_balance,0) AS marketing_credit_balance,
                        CONCAT(user_profiles.first_name,' ',user_profiles.last_name) AS full_name, 
                        user_profile_restrictions_phone.do_not_contact AS do_not_contact_phone_number, 
                        user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                        asset_members.timestamp_member_created, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code " ;
                        
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_profiles',
                    'on' => 'user_profiles.profile_id',
                    'match' => 'messages.member_id_value'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'asset_members',
                    'on' => 'asset_members.member_id',
                    'match' => 'messages.member_id',
                    'type' => 'left'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'accounts',
                    'on' => 'accounts.account_id',
                    'match' => 'user_profiles.account_id'
                    ) ;
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_phone_numbers',
                    'on' => 'user_profiles.phone_number_id',
                    'match' => 'user_phone_numbers.phone_number_id',
                    'type' => 'left'
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'user_profile_restrictions AS user_profile_restrictions_phone',
                    'statement' => '(user_profile_restrictions_phone.phone_number_international=CONCAT(user_phone_numbers.phone_country_dialing_code,user_phone_numbers.phone_number) AND user_profile_restrictions_phone.account_id=user_profiles.account_id AND user_profile_restrictions_phone.profile_id=user_profiles.profile_id)',
                    'type' => 'left'
                    ); 
                
                $query_array['join_tables'][] = array(
                    'table' => 'users',
                    'on' => 'users.user_id',
                    'match' => 'user_profiles.user_id'
                    ) ; 
                
                $query_array['join_tables'][] = array(
                    'table' => 'list_countries',
                    'on' => 'list_countries.country_id',
                    'match' => 'users.country_id',
                    'type' => 'left'
                    );                
                break ;                
            }

        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force') ;
        $this->message_query_result = $result ;        
        
        return $result ; 
        }
    
    
    // Update the recipient of a message in the messages table 
    public function Update_Message_Recipient($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        

        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $query_array = array(
            'table' => 'messages',
            'values' => $values_array,
            'where' => "messages.message_id='$this->message_id'"
            );            
            
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->message_query_result = $result ; 

        return $result ;         
        }
    
    
    
    }