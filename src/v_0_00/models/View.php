<?php

namespace Ragnar\Ironsides ;

class View extends View_Parts {

    public $model_data ;
    public $view_data ;
    
    public $list_query_options ; 

    public function __construct($user_id = 'ignore') {

        global $authorization ; 
        
        global $DB ;  
        $this->DB = $DB ;  
        
        if ('ignore' !== $user_id) {            
            $this->Set_Admin_User_By_ID($user_id) ;   
            $this->Set_User_By_ID($user_id) ; 
            $this->Set_Master_User_By_ID($user_id) ; 
            }
        

        }    

    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    

    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_HTML_Clip($clip_name) {
        
        $clip_html = '' ; 
        
        switch ($clip_name) {
            case 'page_load_indicator':
                $clip_html = '<div class="text-center text-secondary py-4 animated fadeIn"><i class="fa fa-circle-o-notch fa-spin fa-2x" aria-hidden="true"></i></div>' ;
                break ; 
            case 'basic_load_indicator':
                $clip_html = '<span class="animated fadeIn"><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></span>' ;
                break ; 
            case 'basic_lg_load_indicator':
                $clip_html = '<span class="animated fadeIn"><i class="fa fa-circle-o-notch fa-spin fa-2x" aria-hidden="true"></i></span>' ;
                break ;                
            }
        
        return $clip_html ; 
        }
    
    public function Get_Final_View_Array() {
        
        return $this->final_view_array;       
        }
    
    public function Get_View_Data() {
        
        return $this->view_data ;       
        }    
    
    public function Get_Model_Data() {
        
        return $this->model_data ;       
        }
    
    public function Get_List_Query_Options() {
        
        return $this->list_query_options ; 
        }
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    

    public function Action_Pluralize_Content($singular_word,$word_count) {
    
        $data['plurals_list'] = $this->Get_System_Plurals_List() ;
        
        if ($word_count === 1) {
            $formatted_word = $singular_word ; 
            } else {
                $formatted_word = $data['plurals_list'][$singular_word][0] ; 
                }

        return $formatted_word ;
        
        } 
    
    
    public function Action_Compile_View($view_name,$data = array()) {
        
        $this->Set_Model_Timings('View.Action_Compile_View_begin') ;        
        
        
        ob_start(); // turn on output buffering
        include VIEWS_PATH.'/'.$view_name.'.php'; 
        $html = ob_get_contents(); // get the contents of the output buffer
        ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering        

//        $html = include VIEWS_PATH.'/'.$view_name.'.php'; 
        
        $this->Set_Model_Timings('View.Action_Compile_View_with_asset_id_'.$data['asset_record']['asset_id']) ;
        $this->Set_Model_Timings('View.Action_Compile_View_end') ;        
        
        
        return $html;        
        }
    

    
    public function Action_Clean_Table_View($data,$view) {
        
        $this->view_data = array() ; 
        $this->model_data = array() ; 
        
        $data['list_fields'] = array() ; 

        
        // If recalling saved view, columns are saved in the database...
        if (isset($data['view_history']['display_columns'])) {
            foreach ($data['view_history']['display_columns'] as $column) {
                $data['list_fields'][] = (array) $column ; 
                }             
            } else {            
                $c = 0 ; 
                foreach ($data['default_list_fields'] as $column) {
                    $data['list_fields'][] = $data['default_list_fields'][$c] ;   
                    $c++ ; 
                    }            
                }
            
            
//                // IF RESET VIEW THESE ARE EXISTING COLUMNS FROM EXISTING VIEW
//                $view['list_column_names'] = json_decode(urldecode($view['list_column_names']),1) ; 
                
//                    print_r($view['existing_list_columns'])  ;
            
        
        // Rename a column
        $c = 0 ; 
        foreach ($data['list_fields'] as $field) {
            if (isset($view['list_column_'.$c])) {
                $data['list_fields'][$c] = Utilities::Deep_Array($data['all_list_fields'], 'field_name', $view['list_column_'.$c]) ;                             
                }
            $c++ ; 
            }
        
        
                
        // RESET TO DEFAULTS
        //$c = 0 ; 
        //foreach ($data['default_list_fields'] as $column) {
        //    $data['list_fields'][] = $data['default_list_fields'][$c] ;   
        //    $c++ ; 
        //    }        

        
        
        // Remove a column
        if (isset($view['remove_list_column'])) {
            $clean_list_fields = array() ; 
            $c = 0 ; 
            $data['original_list_fields'] = $data['list_fields'] ; 
            
            foreach ($data['list_fields'] as $list_field) {
                if ($c != $view['remove_list_column']) {
                    $clean_list_fields[] = $list_field ; 
                    }
                $c++ ; 
                }
            
            $data['list_fields'] = array() ; 
            $data['list_fields'] = $clean_list_fields ; 
            $data['clean_list_fields'] = $clean_list_fields ; 
            }

        
        // Add a column
        if (isset($view['add_list_column'])) {
            $data['list_fields'][] = Utilities::Deep_Array($data['all_list_fields'], 'field_name', $view['add_list_column']) ;
            }
        

        // Create an array of just column names
        $c = 0 ; 
        $data['list_column_names'] = array() ; 
        foreach ($data['list_fields'] as $field) {
            $data['list_column_names']['list_column_'.$c] = $field['field_name'] ; 
            $c++ ; 
            }        
        

        $this->view_data = $view ; 
        $this->model_data = $data ;
            
        return $this ; 
        }

    
    public function Action_Clean_List_Query($view,$list_query_options,$data) {
        
        if ($view['page']) { 
            $list_query_options['start_page'] = $view['page'] ;    
            } else {
                $list_query_options['start_page'] = 1 ;
                }        
        if ($view['page_increment']) { 
            $list_query_options['page_increment'] = $view['page_increment'] ; 
            } else if ($list_query_options['default_page_increment']) {
                $list_query_options['page_increment'] = $list_query_options['default_page_increment'] ;  
                } else {
                $list_query_options['page_increment'] = 12 ; 
                }        
        
        
        switch ($view['view_name']) {
            
            case 'app_viewpart_import_review_skipped_list':
            
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'contact_import_log.import_log_id' ;
                    $list_query_options['order'] = 'ASC' ;  
                    }
                
                break ; 
            case 'app_viewpart_contacts_imports_exports_list':
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'file_tasks.timestamp_task_created' ;
                    $list_query_options['order'] = 'DESC' ;  
                    }
                
                break ;                
            case 'app_viewpart_asset_gallery_image_list':
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'asset_core.timestamp_asset_created' ;
                    $list_query_options['order'] = 'DESC' ;  
                    }
                
                break ;
            case 'app_viewpart_asset_image_gallery_slideout':   
            case 'app_viewpart_asset_image_gallery_slideout_list':               
                
                if ($view['reset_view'] != 'yes') {  
                
                    $list_query_options = $data['view_history']['view_parameters'] ; 

                    } else {

                        // Set default required view options...
                        $list_query_options['filter_by_profile_id'] = 'yes' ; 
                        $list_query_options['filter_by_asset_auth_role_name'] = 'authorized' ; 

                        if (!isset($view['filter_by_visibility_name'])) {
                            $list_query_options['filter_by_visibility_name'] = 'visible' ; 
                            } else {
                                $list_query_options['filter_by_visibility_name'] = $view['filter_by_visibility_name'] ; 
                                }
                    
                        if (!isset($view['allow_global_assets'])) {
                            $list_query_options['allow_global_assets'] = 'only' ; 
                            } else {
                                $list_query_options['allow_global_assets'] = $view['allow_global_assets'] ; 
                                }

                        $list_query_options['filter_by_type_name'] = array('image') ;   

                        if ($view['list_view_layout']) {
                            $list_query_options['list_view_layout'] = $view['list_view_layout'] ;    
                            } 

                        if ($view['search_parameter']) {
                            $list_query_options['filter_by_search_parameter'] = $view['search_parameter'] ;  
                            }


                        $list_query_options['metadata_search_operator'] = 'false' ;


                        if ($view['tag_metadata_id']) {
                            $list_query_options['filter_by_tag_metadata_id'] = $view['tag_metadata_id'] ;
                            $list_query_options['tag_metadata_search_operator'] = $view['tag_metadata_search_operator'] ;
                            }

                        if ($view['category_metadata_id']) {
                            $list_query_options['filter_by_category_metadata_id'] = $view['category_metadata_id'] ;
                            $list_query_options['category_metadata_search_operator'] = $view['category_metadata_search_operator'] ;
                            }                

                        if ($view['visibility_name']) {
                            $list_query_options['filter_by_visibility_name'] = $view['visibility_name'] ;
                            } elseif ($data['view_history']['view_parameters']['filter_by_visibility_name']) {
                                $list_query_options['filter_by_visibility_name'] = $data['view_history']['view_parameters']['filter_by_visibility_name'] ;
                                }

                        }                

                
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'asset_core.timestamp_asset_created' ;
                    $list_query_options['order'] = 'DESC' ;  
                    }

                $list_query_options['list_column_names'] = $data['list_column_names'] ;                
                
                
                break ; 
            case 'app_viewpart_asset_content_gallery_list':
            case 'app_viewpart_asset_content_gallery_all_list':                
            case 'app_viewpart_asset_content_gallery_image_list':
            case 'app_viewpart_asset_content_gallery_text_list': 
            case 'app_viewpart_asset_content_gallery_email_list':
            case 'app_viewpart_asset_content_gallery_sms_list':  
            case 'app_viewpart_asset_content_gallery_series_list':      
                
            case 'app_viewpart_asset_content_gallery_list_filter':
            case 'app_viewpart_asset_content_gallery_list_filter_slideout':
                
            case 'app_viewitem_modal_asset_save_template_modal_content':
            case 'app_viewitem_modal_asset_save_template_existing_list':    
                
                if ($data['view_history']['view_parameters']['list_view_layout']) {
                    $list_query_options['list_view_layout'] = $data['view_history']['view_parameters']['list_view_layout'] ;    
                    } else {
                    
                        if ($view['view_target']) {
                            
                            switch ($view['view_target']) {
                                case 'app_viewpart_asset_content_gallery_image_list':  
                                    $list_query_options['list_view_layout'] = 'cards' ;
                                    break ;
                                default:    
                                    $list_query_options['list_view_layout'] = 'rows' ;
                                }         
                            
                            } else {
                            
                                switch ($view['view_name']) {
                                    case 'app_viewpart_asset_content_gallery_image_list':  
                                        $list_query_options['list_view_layout'] = 'cards' ;
                                        break ;
                                    default:    
                                        $list_query_options['list_view_layout'] = 'rows' ;
                                    }                            
                            
                                }
                        }                
                
                if ($view['reset_view'] != 'yes') {  
                
                    $list_query_options = $data['view_history']['view_parameters'] ; 

                    } else {

                        // Set default required view options...
                        $list_query_options['filter_by_profile_id'] = 'yes' ; 
                        $list_query_options['filter_by_asset_auth_role_name'] = 'authorized' ; 

                        if (!isset($view['filter_by_visibility_name'])) {
                            $list_query_options['filter_by_visibility_name'] = 'visible' ; 
                            } else {
                                $list_query_options['filter_by_visibility_name'] = $view['filter_by_visibility_name'] ; 
                                }
                    
                        if (!isset($view['allow_global_assets'])) {
                            $list_query_options['allow_global_assets'] = 'only' ; 
                            } else {
                                $list_query_options['allow_global_assets'] = $view['allow_global_assets'] ; 
                                }

                        switch ($view['view_name']) {
                            case 'app_viewpart_asset_content_gallery_image_list':
                            case 'app_viewpart_asset_image_gallery_slideout':
                            case 'app_viewpart_asset_image_gallery_slideout_list':    
                                $list_query_options['filter_by_type_name'] = array('image') ;    
                                break ; 
                            case 'app_viewpart_asset_content_gallery_text_list':
                                $list_query_options['filter_by_type_name'] = array('text') ;    
                                break ; 
                            case 'app_viewpart_asset_content_gallery_email_list':
                                $list_query_options['filter_by_type_name'] = array('email') ;    
                                break ; 
                            case 'app_viewpart_asset_content_gallery_sms_list':
                                $list_query_options['filter_by_type_name'] = array('sms') ;    
                                break ;
                            case 'app_viewpart_asset_content_gallery_series_list':
                                $list_query_options['filter_by_type_name'] = array('series') ;    
                                break ;                                
                            case 'app_viewpart_asset_content_gallery_all_list':
                                
                                if ($view['asset_type_name']) {
                                    $list_query_options['filter_by_type_name'] = Utilities::Process_Comma_Separated_String($view['asset_type_name']) ;
                                    } else {
                                        $list_query_options['filter_by_type_name'] = array('image','email','sms','series') ;
                                        }                                
                                
                                break ;                                
                            }

                        if ($view['list_view_layout']) {
                            $list_query_options['list_view_layout'] = $view['list_view_layout'] ;    
                            } 

                        if ($view['search_parameter']) {
                            $list_query_options['filter_by_search_parameter'] = $view['search_parameter'] ;  
                            }


                        $list_query_options['metadata_search_operator'] = 'false' ;


                        if ($view['tag_metadata_id']) {
                            $list_query_options['filter_by_tag_metadata_id'] = $view['tag_metadata_id'] ;
                            $list_query_options['tag_metadata_search_operator'] = $view['tag_metadata_search_operator'] ;
                            }

                        if ($view['category_metadata_id']) {
                            $list_query_options['filter_by_category_metadata_id'] = $view['category_metadata_id'] ;
                            $list_query_options['category_metadata_search_operator'] = $view['category_metadata_search_operator'] ;
                            }                

                        if ($view['visibility_name']) {
                            $list_query_options['filter_by_visibility_name'] = $view['visibility_name'] ;
                            } elseif ($data['view_history']['view_parameters']['filter_by_visibility_name']) {
                                $list_query_options['filter_by_visibility_name'] = $data['view_history']['view_parameters']['filter_by_visibility_name'] ;
                                }

                        }                

                
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'asset_core.timestamp_asset_updated' ;
                    $list_query_options['order'] = 'DESC' ;  
                    }

                $list_query_options['list_column_names'] = $data['list_column_names'] ;                 
                
                
                break ;
            case 'app_viewpart_inbox_texts_list':
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'conversation.most_recent' ;
                    $list_query_options['order'] = 'DESC' ;  
                    }
                
                $list_query_options['list_column_names'] = $data['list_column_names'] ;                 
                
                
                break ;                
            case 'app_viewpart_campaigns_broadcast_list':
            case 'app_viewpart_campaigns_broadcast_list_filter':
            case 'app_viewpart_campaigns_broadcast_list_filter_slideout':
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'asset_campaigns.timestamp_campaign_created' ;
                    $list_query_options['order'] = 'DESC' ;  
                    }
                
                
                if (!$list_query_options['filter_by_type_name']) {
                    $list_query_options['filter_by_type_name'] = 'broadcast_all' ;
                    }
                
                
                $list_query_options['list_column_names'] = $data['list_column_names'] ;    
                
                
                break ; 
            case 'app_viewpart_campaigns_series_list':
            case 'app_viewpart_campaigns_series_list_filter':
            case 'app_viewpart_campaigns_series_list_filter_slideout':
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'asset_campaigns.timestamp_campaign_created' ;
                    $list_query_options['order'] = 'DESC' ;  
                    }
                
                
                if (!$list_query_options['filter_by_type_name']) {
                    $list_query_options['filter_by_type_name'] = 'series' ;
                    }
                
                
                $list_query_options['list_column_names'] = $data['list_column_names'] ;    
                
                
                break ;                
            case 'app_viewpart_contact_lists':
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'asset_core.asset_title' ;
                    $list_query_options['order'] = 'ASC' ;  
                    }
                
                
                
                $list_query_options['list_column_names'] = $data['list_column_names'] ;    
                
                
                break ; 
            case 'app_viewpart_campaign_recipients_series_list':
            case 'app_viewpart_campaign_recipients_series_list_filter':
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'asset_member_temp_table.first_name' ;
                    $list_query_options['order'] = 'ASC' ;  
                    }
                
            
                if ($view['recipients_filter_name']) {
                    $data['recipients_filter_name'] = $view['recipients_filter_name']; 
                    $data['filter_name_parts'] = explode('-',$view['recipients_filter_name']) ; 
                    $data['filter_type'] = $data['filter_name_parts'][0] ;
                    $data['filter_name'] = $data['filter_name_parts'][1] ;
                    }                
                
                switch ($data['filter_type']) {
                    case 'all':
                        
                        
                        break ; 
                    case 'filter':
                        switch ($data['filter_name']) {
                            case 'recipients_progress':
                                $list_query_options['filter_by_member_status_name'] = 'in_progress' ;  
                                break ; 
                            case 'recipients_complete':
                                $list_query_options['filter_by_member_status_name'] = 'complete' ;
                                break ; 
                            }                        
                        break ; 
                    case 'current_step':
                        $list_query_options['filter_by_member_status_name'] = 'in_progress' ;
                        $list_query_options['filter_by_campaign_position'] = $data['filter_name'] ;  
                        break ; 
                    }
                
                      
                
                
                break ; 
            case 'app_viewpart_campaign_recipients_list':  
            case 'app_viewpart_campaign_recipients_list_filtered':
            case 'app_viewpart_campaign_recipients_list_filter':
            case 'app_viewpart_campaign_recipients_list_filter_slideout':
                
                
                // THIS BLOCK IS ONLY RESETTING CAMPAIGN SAVE PARAMETERS -- NOT THE VIEW QUERY PARAMETERS
                
                if (!$view['search_parameter']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_search_parameter']) ; 
                    } 
                if (!$view['search_parameter_refine_contact_notes']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['search_parameter_refine_contact_notes']) ; 
                    } 
                if (!$view['search_parameter_refine_contact_description']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['search_parameter_refine_contact_description']) ; 
                    } 
                
                if (!$view['starred_status']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_starred_status']) ;  
                    } 

                if (!$view['contact_source']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_contact_source']) ;  
                    }

                if (!$view['tag_metadata_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_tag_metadata_id']) ;
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['tag_metadata_search_operator']) ;
                    }

                if (!$view['category_metadata_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_category_metadata_id']) ;
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['category_metadata_search_operator']) ;
                    }

                if (!$view['contact_status_metadata_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_contact_status_metadata_id']) ;
                    }    


                // Rating filter
                if (!$view['rating_filter_status']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['rating_filter_status']) ; 
                    } 
                if (!$view['rating_filter_type']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['rating_filter_type']) ; 
                    } 

                if (!$view['rating_filter_range_between_begin']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['rating_filter_range_between_begin']) ;
                    } 
                if (!$view['rating_filter_range_between_end']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['rating_filter_range_between_end']) ;
                    }
                if (!$view['rating_filter_range_between_begin']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['rating_filter_range_between_begin']) ;
                    } 
                if (!$view['rating_filter_range_between_end']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['rating_filter_range_between_end']) ;
                    }



                // Birthday filter
                if (!$view['birthday_filter_status']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['birthday_filter_status']) ; 
                    } 
                if (!$view['birthday_filter_type']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['birthday_filter_type']) ; 
                    } 

                if (!$view['birthday_filter_range_next_days']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['birthday_filter_range_next_days']) ; 
                    } 
                if (!$view['birthday_filter_range_last_days']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['birthday_filter_range_last_days']) ; 
                    } 

                if (!$view['birthday_filter_range_between_begin']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['birthday_filter_range_between_begin']) ;
                    } 
                if (!$view['birthday_filter_range_between_end']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['birthday_filter_range_between_end']) ;
                    }                 

                // Contact Created filter
                if (!$view['contact_created_filter_status']) {
                    $list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_created_filter_status'] = 'inactive' ;
                    }
                if (!$view['contact_created_filter_type']) {
                    $list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_created_filter_type'] = 'range_last' ;
                    }

                if (!$view['contact_created_filter_range_next_days']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_created_filter_range_next_days']) ; 
                    } 
                if (!$view['contact_created_filter_range_last_days']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_created_filter_range_last_days']) ; 
                    } 

                if (!$view['contact_created_filter_range_between_begin']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_created_filter_range_between_begin']) ;
                    } 
                if (!$view['contact_created_filter_range_between_end']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_created_filter_range_between_end']) ;
                    }                 

                if (!$view['import_file_task_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['import_file_task_id']) ;
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['import_file_task_title']) ; 
                    } 


                // Young Living Filters
                if (!$view['yl_account_status_metadata_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_yl_account_status_metadata_id']) ;
                    }
                if (!$view['yl_current_rank_metadata_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_yl_current_rank_metadata_id']) ;
                    }
                if (!$view['yl_highest_rank_metadata_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_yl_highest_rank_metadata_id']) ;
                    }


                // Contact YL Activation Date filter
                if (!$view['contact_yl_activation_filter_status']) {
                    $list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_yl_activation_filter_status'] = 'inactive' ;
                    }
                if (!$view['contact_yl_activation_filter_type']) {
                    $list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_yl_activation_filter_type'] = 'range_last' ;
                    }

                if (!$view['contact_yl_activation_filter_range_next_days']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_yl_activation_filter_range_next_days']) ; 
                    } 
                if (!$view['contact_yl_activation_filter_range_last_days']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_yl_activation_filter_range_last_days']) ; 
                    } 

                if (!$view['contact_yl_activation_filter_range_between_begin']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_yl_activation_filter_range_between_begin']) ;
                    } 
                if (!$view['contact_yl_activation_filter_range_between_end']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['contact_yl_activation_filter_range_between_end']) ;
                    }                
                
                
                // User Profile filters
                if (!$view['account_auth_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_account_auth_id']) ;
                    }                
                if (!$view['user_auth_id']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_user_auth_id']) ;
                    }
                if (!$view['account_billing_status']) {
                    unset($list_query_options['structured_data_01']['recipients']['to'][$data['list_short_name']]['filter_by_account_billing_status']) ;
                    }                
                
                break ; 
            case 'app_viewpart_contacts_list':
            case 'app_viewpart_contacts_distribution_list':
            case 'app_viewpart_contacts_list_filter':
            case 'app_viewpart_contacts_list_filter_slideout':
                
                
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by']) AND (!$view['view_saved_id'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;
                } else if (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                } else {
                    // Set a default order
                    switch ($view['view_name']) {
                        case 'app_viewpart_contacts_list':
                        case 'app_viewpart_contacts_list_filter':
                        case 'app_viewpart_contacts_list_filter_slideout':
                            $list_query_options['order_by'] = 'contacts.last_name' ;
                            $list_query_options['order'] = 'ASC' ;                            
                            break ; 
                        case 'app_viewpart_contacts_distribution_list':
                            $list_query_options['order_by'] = 'asset_member_temp_table.last_name' ;
                            $list_query_options['order'] = 'ASC' ;
                            break ;                                                        
                        }
                    }
                

                if (!$list_query_options['search_parameter_refine_contact_notes']) {
                    $list_query_options['search_parameter_refine_contact_notes'] = 'no' ; 
                    }
                if (!$list_query_options['search_parameter_refine_contact_description']) {
                    $list_query_options['search_parameter_refine_contact_description'] = 'no' ; 
                    }                
                
                if (!$list_query_options['rating_filter_status']) {
                    $list_query_options['rating_filter_status'] = 'inactive' ; 
                    }                
                if (!$list_query_options['rating_filter_type']) {
                    $list_query_options['rating_filter_type'] = 'range_between' ; 
                    }
                if (!$list_query_options['rating_filter_range_between_begin']) {
                    $list_query_options['rating_filter_range_between_begin'] = 1 ;
                    } 
                if (!$list_query_options['rating_filter_range_between_end']) {
                    $list_query_options['rating_filter_range_between_end'] = 5 ;
                    }                
                
                
                if (!$list_query_options['birthday_filter_status']) {
                    $list_query_options['birthday_filter_status'] = 'inactive' ; 
                    }                
                if (!$list_query_options['birthday_filter_type']) {
                    $list_query_options['birthday_filter_type'] = 'range_next' ; 
                    }
                
                if (!$list_query_options['birthday_filter_range_next_days']) {
                    $list_query_options['birthday_filter_range_next_days'] = 30 ;    
                    }
                if (!$list_query_options['birthday_filter_range_last_days']) {
                    $list_query_options['birthday_filter_range_last_days'] = 14 ;
                    }
                                 
                if (!$list_query_options['birthday_filter_range_between_begin']) {
                    $range_between_begin_manipulated = Utilities::System_Time_Manipulate('14 d','-','now') ; 
                    $range_between_begin = new DateTime() ; 
                    $list_query_options['birthday_filter_range_between_begin'] = $range_between_begin->setTimestamp($range_between_begin_manipulated['time_manipulated'])->format('m/d/y') ;  
                    }                
                if (!$list_query_options['birthday_filter_range_between_end']) {
                    $range_between_end_manipulated = Utilities::System_Time_Manipulate('14 d','+','now') ; 
                    $range_between_end = new DateTime() ; 
                    $list_query_options['birthday_filter_range_between_end'] = $range_between_end->setTimestamp($range_between_end_manipulated['time_manipulated'])->format('m/d/y') ;  
                    }
                

                
                if (!$list_query_options['contact_created_filter_type']) {
                    $list_query_options['contact_created_filter_type'] = 'range_last' ;
                    }
                    
                if (!$list_query_options['contact_created_filter_range_next_days']) {
                    $list_query_options['contact_created_filter_range_next_days'] = 30 ;
                    }                
                if (!$list_query_options['contact_created_filter_range_last_days']) {
                    $list_query_options['contact_created_filter_range_last_days'] = 14 ;
                    }

                if (!$list_query_options['contact_created_filter_range_between_begin']) {
                    $range_between_begin_manipulated = Utilities::System_Time_Manipulate('30 d','-','now') ; 
                    $range_between_begin = new DateTime() ; 
                    $list_query_options['contact_created_filter_range_between_begin'] = $range_between_begin->setTimestamp($range_between_begin_manipulated['time_manipulated'])->format('m/d/y') ; 
                    } 
                if (!$list_query_options['contact_created_filter_range_between_end']) {
                    $range_between_end = new DateTime() ; 
                    $list_query_options['contact_created_filter_range_between_end'] = $range_between_end->setTimestamp(TIMESTAMP)->format('m/d/y') ;  
                    }

                
                if (!$list_query_options['contact_yl_activation_filter_type']) {
                    $list_query_options['contact_yl_activation_filter_type'] = 'range_last' ;
                    }
                    
                if (!$list_query_options['contact_yl_activation_filter_range_next_days']) {
                    $list_query_options['contact_yl_activation_filter_range_next_days'] = 30 ;
                    }                
                if (!$list_query_options['contact_yl_activation_filter_range_last_days']) {
                    $list_query_options['contact_yl_activation_filter_range_last_days'] = 14 ;
                    }

                if (!$list_query_options['contact_yl_activation_filter_range_between_begin']) {
                    $range_between_begin_manipulated = Utilities::System_Time_Manipulate('30 d','-','now') ; 
                    $range_between_begin = new DateTime() ; 
                    $list_query_options['contact_yl_activation_filter_range_between_begin'] = $range_between_begin->setTimestamp($range_between_begin_manipulated['time_manipulated'])->format('m/d/y') ; 
                    } 
                if (!$list_query_options['contact_yl_activation_filter_range_between_end']) {
                    $range_between_end = new DateTime() ; 
                    $list_query_options['contact_yl_activation_filter_range_between_end'] = $range_between_end->setTimestamp(TIMESTAMP)->format('m/d/y') ;  
                    }
                
                
                $list_query_options['list_column_names'] = $data['list_column_names'] ;    
                
                break ; 
                
            case 'app_viewpart_comments_list':
            case 'app_viewpart_comments_list_filter':
            case 'app_viewpart_comments_list_filter_slideout':
            case 'app_viewpart_asset_comments_list':
            case 'app_viewpart_asset_comments_list_filter':
            case 'app_viewpart_asset_comments_list_filter_slideout':                
            
            
                // Order can be set with either reset_view=yes / no
                if (isset($view['order_by'])) {
                    $list_query_options['order_by'] = $view['order_by'] ;
                    $list_query_options['order'] = $view['order'] ;      
                    } elseif (isset($list_query_options['order_by'])) {
                    // Keep saved order settings...
                    // $list_query_options['order_by'] ;
                    // $list_query_options['order'] ;      
                    } else {
                    // Set a default order
                    $list_query_options['order_by'] = 'asset_comments.timestamp_comment_created' ;
                    $list_query_options['order'] = 'DESC' ;  
                    }
                
                if (!$list_query_options['comment_created_filter_status']) {                
                    $list_query_options['comment_created_filter_status'] = 'inactive' ;
                    }
                
                if (!$list_query_options['comment_created_filter_type']) {      
                    $list_query_options['comment_created_filter_type'] = 'range_last' ;
                    }
                
                if (!$list_query_options['comment_created_filter_range_next_days']) {      
                    $list_query_options['comment_created_filter_range_next_days'] = 30 ;
                    }
                if (!$list_query_options['comment_created_filter_range_last_days']) {      
                    $list_query_options['comment_created_filter_range_last_days'] = 14 ;
                    }
                
                if (!$list_query_options['comment_created_filter_range_between_begin']) {      
                    $range_between_begin_manipulated = Utilities::System_Time_Manipulate('30 d','-','now') ; 
                    $range_between_begin = new DateTime() ; 
                    $list_query_options['comment_created_filter_range_between_begin'] = $range_between_begin->setTimestamp($range_between_begin_manipulated['time_manipulated'])->format('m/d/y') ;  
                    }
                
                if (!$list_query_options['comment_created_filter_range_between_end']) {   
                    $range_between_end = new DateTime() ; 
                    $list_query_options['comment_created_filter_range_between_end'] = $range_between_end->setTimestamp(TIMESTAMP)->format('m/d/y') ;  
                    }                
                
                $list_query_options['list_column_names'] = $data['list_column_names'] ;    
                
                break ; 
            }
        
        $this->list_query_options = $list_query_options ; 
        
        return $this ;         
        }
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    ////////////////////// 
    

    
    
    
    
    }


