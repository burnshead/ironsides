<?php

namespace Ragnar\Ironsides ;

//    use Chmiello\ArraySortPackage\ArraySort;

// A GOOD QUERY FOR USING NOT IN ... originally for finding asset members but got replaced

//                        // Create the operator
//                        switch ($query_options->auth_role_search_operator) {
//                            case 'AND':
//                                $auth_role_search_operator = "= $count_auth_role" ;
//                                break ; 
//                            case 'NOT IN':    
//                                $auth_role_search_operator = " IS NULL" ; 
//                                break ; 
//                            case 'OR':
//                            default:
//                                $auth_role_search_operator = ">= 1" ;                                
//                            }            
//
//
//                        foreach ($query_options->filter_by_auth_role_id as $auth_role_id) {
//                            $auth_role_query_string .= "$auth_role_id," ;
//                            }
//
//                        $auth_role_query_string = rtrim($auth_role_query_string,",") ;
//
//                        $join_table_name = 'user_role_query' ;  
//                        $query_array['join_tables'][] = array(
//                            'table' => "
//                                (SELECT system_auth_roles.*, 
//                                users.user_id  
//                                FROM system_auth_roles  
//                                JOIN users ON system_auth_roles.auth_id = users.user_auth_id 
//                                WHERE 
//                                    system_auth_roles.auth_id IN ($auth_role_query_string) 
//                                    GROUP BY users.user_id 
//                                    HAVING COUNT(1) $auth_role_search_operator) 
//                                    AS $join_table_name",
//                            'type' => 'join',
//                            'statement' => "
//                                    (users.user_id=$join_table_name.user_id)"
//                            );



// When setting assets, this indicators mean:
// active history: The asset history id that is being shown in the view (or the script) at present
// current history: The asset history id that would be published on the marketing site (even if others are more recent)
// latest history: The most recently edited asset history version in the database

class Asset extends Account {

    public $automation_result ; 
    
    public $asset_input ; 
    
    public $asset_id;
    public $asset;
    
    public $child_asset_id;
    public $child_asset;
    
    public $structure_item ; 
    
    public $asset_list;
    public $asset_full_list ; // All results within the current "page"
    public $asset_uber_list ; // All results from all pages    
    
    public $asset_history ; 
    
    public $asset_restore_fields = array(
        'asset_title',
        'asset_description'
        ) ; 
    
    public $full_asset_list ; 
    
    public $asset_public_link ; 
    
    public $asset_type ; 
    
    public $asset_paging ; 
    public $page_increment = 12 ; // # of assets to be pulled per page (e.g. )

    public $version_history_increment = 45 ; // # of seconds that need to elapse before a new versioned increment of an asset is created
    
    public $asset_compiled ; // For multi-part assets (like style guides) 
    
    public $comment_id ;
    public $comment ;
    
    
    public $comment_list ;
    public $comment_count ;    
    
    public $asset_member_metadata ; // Metadata values for subscription status
    
    public $asset_members_list ; // This will be an array of attending, not_attending, maybe_attending, invited
    public $asset_members_full_list ; // All results within the current "page"
    public $asset_members_uber_list ; // All results from all pages
    public $asset_member ; // Individual member of asset
    public $asset_members_processing_list ; // An array of clean_invite, clean_update, dirty_invite so manage new/updating members to an asset (list, sequence)   
    
    public $conversation_list ; 
    public $conversation_results ; 
    
    public $action_keys ; 
    
    public $vendor_id ; 
    public $vendor ; 
    public $vendor_keys ; 
    
    public $asset_query_result ;
    public $asset_query_result2 ;
    public $asset_member_insert_result ; 
    public $asset_member_query_result ;
    public $asset_member_query_result2 ;
    public $asset_list_query_result ; 
    public $asset_structure_query_result ; 
    
    public $search_id ; // Created in System model
    public $search ; 
    
    
    // ASSET CAMPAIGNS
    public $campaign_id ; // campaign_id from an individual asset campaign (campaign can have multiple automation_id's associated with it)
    public $campaign ; // An individual campaign
    public $campaign_list ; // The full campaign list array  
    
    public $campaign_report ; 
    
    public $campaign_recipients_list ; // List of recipients defined by the settings & filters in asset_campaigns table
    public $campaign_full_recipients_list ; // List of recipients with all associated member data, as defined by the settings & filters in asset_campaigns table
    public $campaign_recipients_count ; // Paging array of a campaign
    
    public $test_data2 ; 
    
    // AUTOMATION
    public $automation_id ; // automation_id from an individual automation item
    public $automation ; // An individual item from the full automation_list array
    public $automation_list ; // The full array    
    public $automation_update  ; // A place to test the result from automation procesing    
    
    public $scheduled_automation_timestamp = 0 ;
    
    // for testing only...
    public $contact ; 
    
    // Template
    public $campaign_recipients_template_email = array(
                    'to' => array(
                        'delivery_list' => array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'filter_by_email_metadata_id' => array(46)
                            ),
                        'delivery_list_filtered' => array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'filter_by_email_metadata_id' => array(46)
                            ),
                        'delivery_list_custom' =>  array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'filter_by_email_metadata_id' => array(46),
                            'member_id' => array()
                            ),
                        'static' => array(
                            'email_address' => array(),
                            'recipient_variables' => array()
                            )
                        )
                    ) ;
    
    public $campaign_recipients_template_sms = array(
                    'to' => array(
                        'delivery_list' => array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'filter_by_sms_metadata_id' => array(46)
                            ),
                        'delivery_list_filtered' => array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'filter_by_sms_metadata_id' => array(46)
                            ),
                        'delivery_list_custom' =>  array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'filter_by_sms_metadata_id' => array(46),
                            'member_id' => array()
                            ),
                        'static' => array(
                            'email_address' => array(),
                            'recipient_variables' => array()
                            )
                        )
                    ) ;    
    
    public $campaign_recipients_template_series = array(
                    'to' => array(
                        'delivery_list' => array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'filter_by_email_metadata_id' => array(46),
                            'filter_by_sms_metadata_id' => array(46)
                            ),
                        'delivery_list_filtered' => array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'filter_by_email_metadata_id' => array(46),
                            'filter_by_sms_metadata_id' => array(46)
                            ),
                        'delivery_list_custom' =>  array(
                            'filter_by_profile_id' => 'yes',
                            'filter_by_account_id' => 'yes',
                            'email_metadata_id' => array(46),
                            'sms_metadata_id' => array(46),
                            'member_id' => array()
                            ),
                        'static' => array(
                            'email_address' => array(),
                            'recipient_variables' => array()
                            )
                        )
                    ) ;
    
    
    public function __construct($user_id = 'ignore') {

        global $DB ;
        $this->DB = $DB ;        
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            }  
        
        $this->Set_Scheduled_Automation_Timestamp('now')->Set_Scheduled_Automation_Timestamp(62) ; 
        
        }
    

    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    
    
    public function Set_Automation_ID($automation_id) {
        
        $this->automation_id = $automation_id ; 
        
        return $this ; 
        }
    
    public function Set_Automation_By_ID($automation_id = 'internal') {
        
        if ('internal' === $automation_id) {
            $automation_id = $this->automation_id ; 
            } else {
                $this->automation_id = $automation_id ;      
                }
        
        $query_options['filter_by_automation_id'] = $automation_id ; 
        
        $this->Set_Automation($query_options) ; 
        
        return $this ; 
        }
    
    public function Set_Automation_By_Current_Campaign_Position() {
        
        $campaign_record = $this->Get_Campaign() ;
        
        if ($campaign_record['campaign_current_position']) {
            $automation_query = array(
                'filter_by_campaign_id' => $campaign_record['campaign_id'],
                'filter_by_campaign_position' => $campaign_record['campaign_current_position'],
                'timestamp_next_action' => 'all'
                ) ;
            
            $this->Set_Automation_List($automation_query) ; 
            
            $automation_list = $this->Get_Automation_List() ; 
                
            $this->automation = $automation_list[0] ;
            $this->automation_id = $automation_list[0]['automation_id'] ; 
            }
        
        return $this ; 
        }
    
    public function Set_Automation($query_parameters) {
        
        
        $result = $this->Retrieve_Automation_List($query_parameters) ; 
        
        $this->automation_result = $result ; 
        
        if (!$result['error']) {
            
            $automation = $result['results'][0]  ;

            $structured_data_01 = json_decode($automation['structured_data_01'],true) ;

            if (isset($structured_data_01['recipients'])) {
                $automation['recipients'] = $structured_data_01['recipients'] ; 
                }
            
            // Force member_id for series recipient into into the recipient array
            switch ($automation['type_name']) {
                case 'series':
                    $automation['recipients']['to']['delivery_list_custom']['member_id'] = $automation['member_id'] ;
                    break ; 
                }             


            if (isset($structured_data_01['action_keys'])) {
                $automation['action_keys'] = $structured_data_01['action_keys'] ; 
                }  
            
            if (isset($structured_data_01['personalization_overrides'])) {
                $automation['personalization_overrides'] = $structured_data_01['personalization_overrides'] ; 
                }  
            
            if (isset($structured_data_01['data_overrides'])) {
                $automation['data_overrides'] = $structured_data_01['data_overrides'] ; 
                }            
            } 
            
        $automation = $this->Action_Map_Automation($automation) ; 
    
       
        
        
        $this->automation = $automation ; 
        
        return $this ; 
        }    
    
    
    
    public function Set_Automation_List($query_parameters = array()) {
        
        if ($query_parameters['timestamp_next_action'] == 'all') {
            $query_parameters['timestamp_next_action_lower'] = 0 ; 
            $query_parameters['timestamp_next_action_upper'] = Utilities::System_Time_Manipulate("10 y",'+')['time_manipulated'] ;
            }
        
        
        // By default, limit to automations scheduled for up to 70 minutes in the past
        if(!isset($query_parameters['timestamp_next_action_lower'])) {
            $query_parameters['timestamp_next_action_lower'] = Utilities::System_Time_Manipulate("70 m",'-')['time_manipulated'] ; 
            }
        
        // By default, limit to automations scheduled for up to 5 minutes into the future
        if(!isset($query_parameters['timestamp_next_action_upper'])) {
            $query_parameters['timestamp_next_action_upper'] = Utilities::System_Time_Manipulate("5 m",'+')['time_manipulated'] ; 
            }
        
        $result = $this->Retrieve_Automation_List($query_parameters) ; 
        
        if (!$result['error']) {
            
            
            $i = 0 ; 
            foreach ($result['results'] as $queue_item) {
                
                $result['results'][$i] = $this->Action_Time_Territorialize_Dataset($queue_item) ;  
            
                $structured_data_01 = json_decode($queue_item['structured_data_01'],true) ;
                
                if (isset($structured_data_01['recipients'])) {
                    $result['results'][$i]['recipients'] = $structured_data_01['recipients'] ; 
                    }
                
                unset($structured_data_01) ; 
                $i++ ; 
                }            
            }

        $this->automation_list = $result['results'] ; 
        
        return $this ; 
        }
    
    

    
    public function Set_Campaign_ID($campaign_id) {
        
        $this->campaign_id ; 
        
        return $this ; 
        }
    
    public function Set_Campaign_By_ID($campaign_id = 'internal',$query_options = array()) {
        
        $this->Set_Model_Timings('Asset.Set_Campaign_By_ID_initialize_campaign_'.$campaign_id) ; 
        
        if ('internal' === $campaign_id) {
            $campaign_id = $this->campaign_id ; 
            } else {
                $this->campaign_id = $campaign_id ;      
                }
        
        $query_options['filter_by_campaign_id'] = $campaign_id ; 
        $this->Set_Campaign($query_options) ; 
        
        return $this ; 
        }
    
    
    public function Set_Campaign($query_options = array()) {
        
        $this->Set_Model_Timings('Asset.Set_Campaign_initialize_'.$query_options['filter_by_campaign_id']) ; 
        
        $query_options['filter_by_user_id'] = 'no' ; 
        $query_options['filter_by_account_id'] = 'no' ; 
        $query_options['filter_by_asset_status_name'] = array('all') ; 
        $query_options['filter_by_campaign_archived_status'] = 'both' ; 
                
        $this->Set_Campaign_List($query_options) ; 
        $result = $this->Get_Campaign_List() ; 
        
        $this->Set_Model_Timings('Asset.Set_Campaign_campaign_list_retrieved_for_'.$query_options['filter_by_campaign_id']) ; 
        
        
        if (!$result['error']) {
            
            $this->Set_Model_Timings('Asset.Set_Campaign_begin_processing_campaign') ; 
            $campaign = $result[0]  ;

            $structured_data_01 = json_decode($campaign['structured_data_01'],true) ;
            $campaign['structured_data_01'] = $structured_data_01 ; 
            
            if (isset($structured_data_01['recipients'])) {
                $campaign['recipients'] = $structured_data_01['recipients'] ; 
                }            
            if (isset($structured_data_01['test_mode'])) {
                $campaign['test_mode'] = $structured_data_01['test_mode'] ; 
                }
            if (isset($structured_data_01['halt_delivery'])) {
                $campaign['halt_delivery'] = $structured_data_01['halt_delivery'] ; 
                }            
            
            $campaign['delivery_asset'] = array() ; 
            $campaign['asset'] = array() ; 
            
            // Set delivery asset
            if (($query_options['skip_delivery_asset'] != 'yes') AND ($campaign['delivery_asset_id'] != 0)) {
                
                $this->Set_Model_Timings('Asset.Set_Campaign_delivery_asset_set_begin') ;  
                
                $additional_parameters['skip_history'] = 'yes' ; 
                $additional_parameters['skip_asset_mapping'] = 'yes' ; 

                $delivery_asset = new Asset() ; // The list to be delivered to
                $delivery_asset->Set_Replicate_User_Account_Profile($this) ;
                $delivery_asset->Set_Asset_By_ID($campaign['delivery_asset_id'],$additional_parameters) ; 
                $campaign['delivery_asset'] = $delivery_asset->Get_Asset() ; 
                $this->Set_Model_Timings('Asset.Set_Campaign_delivery_asset_set_complete') ;    
                } else {
                
                    $campaign['delivery_asset'] = array(
                        'asset_id' => $campaign['delivery_asset_id'],
                        'asset_title' => $campaign['delivery_asset_title'],
                        'type_id' => $campaign['delivery_asset_type_id'],
                        'type_name' => $campaign['delivery_asset_type_name'],
                        'type_display_name' => $campaign['delivery_asset_type_display_name'],
                        'type_display_name_lc' => $campaign['delivery_asset_type_display_name_lc']
                        ) ;
                    }

            // Set content asset
            if (($query_options['skip_content_asset'] != 'yes') AND ($campaign['asset_id'] != 0)) {
                $this->Set_Model_Timings('Asset.Set_Campaign_content_asset_set_begin') ;  

                $additional_parameters['skip_history'] = 'yes' ; 
                $additional_parameters['skip_asset_mapping'] = 'yes' ; 

                $content_asset = new Asset() ; // The list to be delivered to
                $content_asset->Set_Replicate_User_Account_Profile($this) ;
                $content_asset->Set_Asset_By_ID($campaign['asset_id'],$additional_parameters) ; 
                $campaign['asset'] = $content_asset->Get_Asset() ;             

                $this->Set_Model_Timings('Asset.Set_Campaign_content_asset_set_complete') ;    
                } else {
                
                    $campaign['asset'] = array(
                        'asset_id' => $campaign['asset_id'],
                        'asset_title' => $campaign['content_asset_title'],
                        'type_id' => $campaign['content_asset_type_id'],
                        'type_name' => $campaign['content_asset_type_name'],
                        'type_display_name' => $campaign['content_asset_type_display_name'],
                        'type_display_name_lc' => $campaign['content_asset_type_display_name_lc'],
                        'type_icon' => $campaign['content_asset_type_icon']
                        ) ;
                    }
            
            
            $temp_campaign = $campaign ; 
            $temp_campaign['timestamp_asset_created'] = $temp_campaign['content_asset_timestamp_asset_created'] ;
            $campaign['content_asset_public_link'] = $this->Set_Asset_Public_Link($temp_campaign)->Get_Asset_Public_Link() ; 
                  
            $metadata_query = array(
                'relationship_type' => 'campaign',
                'metadata_type' => 'tag,category'
                ) ; 
                        
            $campaign['tag'] = array() ;
            $campaign['category'] = array() ;
            
            $campaign_metadata = $this->Retrieve_Metadata_Relationship_List($campaign['campaign_id'],$metadata_query) ; 
            if ($campaign_metadata['result_count'] > 0) {
                
                foreach ($campaign_metadata['results'] as $campaign_metadata_result) {
                    switch ($campaign_metadata_result['metadata_type']){
                        case 'tag':
                            $campaign['tag'][] = $campaign_metadata_result ; 
                            break ;
                        case 'category':
                            $campaign['category'][] = $campaign_metadata_result ; 
                            break ;                            
                        }
                    }
                }            
            $this->Set_Model_Timings('Asset.Set_Campaign_metadata_set_complete') ; 
            
            
            $this->Set_Model_Timings('Asset.Set_Campaign_end') ; 
            }  

        
        $this->campaign = $campaign ;         
        return $this ; 
        }    
    
    
    
    public function Set_Replicate_Campaign($source_object) {
        
        $this->Set_Model_Timings('Account.Set_Replicate_Campaign_initialize') ; 
                
        $this->campaign_id = $source_object->campaign['campaign_id'] ; 
        $this->campaign = $source_object->campaign ; 
        
        $this->Set_Model_Timings('Account.Set_Replicate_Campaign_complete') ; 
        
        return $this ; 
        }
    
    // Places a single child of a parent's asset structure into a retrievable array for a new model
    public function Set_Asset_Structure_Item($structure_item) {
        
        $this->structure_item = $structure_item ; 
        
        return $this ; 
        }
    
    public function Set_Asset_Member_Metadata($query_options = array()) {
                
        $result = $this->Retrieve_Asset_Member_Metadata($query_options) ; 
        
        if (!$result['error']) {
            
            $this->asset_member_metadata = $result['results']  ;
          
            } else {
                $this->asset_member_metadata = $result ; 
                }        
        
        return $this ; 
        }
    
    public function Get_Asset_Member_Metadata() {
        
        return $this->asset_member_metadata ; 
        }
    
    
    public function Set_Campaign_List($query_options = array()) {
        
        $this->Set_Model_Timings('Asset.Set_Campaign_List_begin') ; 
        
        
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'yes' ; 
            }
        
        if (!isset($query_options['filter_by_account_id'])) {
            $query_options['filter_by_account_id'] = 'yes' ; 
            }
        
        if (!isset($query_options['filter_by_asset_status_id']) AND (!isset($query_options['filter_by_asset_status_name']))) {
            $query_options['filter_by_asset_status_name'] = array('visible') ; 
            }
           
        if (!$query_options['filter_by_asset_status_name']) {
            $query_options['filter_by_asset_status_name'] = 'visible' ; 
            }
        
        if (!$query_options['filter_by_campaign_archived_status']) {
            $query_options['filter_by_campaign_archived_status'] = 'live' ; 
            }        

        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        
        
        $result = $this->Retrieve_Campaign_List($query_options) ; 
        //        print_r($result) ;
        
        if (!$result['error']) {
            
            $i = 0 ; 
            foreach ($result['results'] as $campaign) {
                
                $result['results'][$i] = $this->Action_Time_Territorialize_Dataset($campaign) ;
                
                $structured_data_01 = json_decode($campaign['structured_data_01'],true) ;
                
                if (isset($structured_data_01['recipients'])) {
                    $result['results'][$i]['recipients'] = $structured_data_01['recipients'] ; 
                    }
                if (isset($structured_data_01['test_mode'])) {
                    $result['results'][$i]['test_mode'] = $structured_data_01['test_mode'] ; 
                    }
                if (isset($structured_data_01['halt_delivery'])) {
                    $result['results'][$i]['halt_delivery'] = $structured_data_01['halt_delivery'] ; 
                    }                
                
                
                unset($structured_data_01) ; 
                $i++ ; 
                } 
                        
            }

        $this->campaign_list = $result['results'] ; 
        
        $this->Set_Model_Timings('Asset.Set_Campaign_List_end') ;
        
        return $this ; 
        }
    
    
    
    // Sets the CURRENTLY eligible recipients for an Automation step in a Campaign based on the filters 
    // and settings within the campaign record
    public function Action_Set_Campaign_Recipients_List($query_options = array()) {
        
        $this->Set_Model_Timings('Asset.Action_Set_Campaign_Recipients_List_initialize') ; 
        

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
                
        $automation_item = $this->Get_Automation() ;
        $campaign_record = $this->Get_Campaign() ;
        
        
        $delivery_trigger = $automation_item['delivery_trigger'] ; 
        
        $recipient_delivery_array = array() ; // Empty array to drop ALL recipients into
        $recipient_full_delivery_array = array() ; // Empty array to drop ALL recipients into WITH full information
        $delivery_type_loop = array('to','cc','bcc','incoming') ; 


        $asset = new Asset($automation_item['user_id']) ; 
        $asset->Set_Account_ID($automation_item['account_id'])->Set_Profile_By_User_Account() ; 

        
        // Set delivery asset
        $additional_parameters['skip_history'] = 'yes' ; 
        $additional_parameters['skip_asset_mapping'] = 'yes' ;         
        $asset->Set_Asset_By_ID($automation_item['delivery_asset_id'],$additional_parameters) ; 
        $this->Set_Model_Timings('Asset.Action_Set_Campaign_Recipients_List_set_delivery_asset_complete') ;        
        
        
        $data['content_asset'] = $this->Get_Asset() ; 
        
        
        $asset_status_options['filter_by_asset_status_name'] = 'scheduled' ;
        $asset_status = $this->Retrieve_Asset_Status_ID_Array($asset_status_options,'single') ; 

        
        $continue = 1 ; 
        

            foreach ($delivery_type_loop as $delivery_type) {

                if (is_array($automation_item['recipients'][$delivery_type][$delivery_trigger])) {
                    
                    
                    $member_list_options = array() ; 

                    if (isset($query_options->page_increment)) {
                        $asset->Set_Page_Increment($query_options->page_increment) ; 
                        }

                    switch ($delivery_trigger) {
                        case 'delivery_list':    
                        case 'delivery_list_filtered':

                            $member_list_options = $automation_item['recipients'][$delivery_type][$delivery_trigger] ; 

                            if (!isset($query_options->override_paging)) {
                                $member_list_options['override_paging'] = 'yes' ; 
                                } else {
                                    $member_list_options['override_paging'] = $query_options->override_paging ; 
                                    }
                            if ($member_list_options['override_paging'] == 'yes') {
                                $member_list_options['minify_list'] = 'yes' ; 
                                }
                            
                            if (isset($query_options->use_temp_table)) {
                                $member_list_options['use_temp_table'] = $query_options->use_temp_table ; 
                                }                            
                            if (isset($query_options->use_permatable)) {
                                $member_list_options['use_permatable'] = $query_options->use_permatable ; 
                                }
                            
//                            error_log('Asset.Action_Set_Campaign_Recipients_List() Pre set members list')  ;
        
                            
                            $asset->Set_Asset_Members_List($member_list_options) ; 
                            $delivery_list = $asset->Get_Asset_Members_List() ; 
                            
                            $this->Add_Model_Timings_Array($asset->Get_Model_Timings()['timings']) ;
                            $this->Set_Model_Timings('Asset.Action_Set_Campaign_Recipients_List_retrieved_asset_members') ;  

                            
                            $this->test_data = $delivery_list ;                            

                            
                            if (count($delivery_list) == 0) {
                                $zero_count['total_count'] = 0 ; 
                                $asset->Set_Asset_Paging($zero_count) ; 
                                }
                
                            $this->campaign_recipients_count = $asset->Get_Asset_Paging() ; 

                            foreach ($delivery_list as $member) {
                                $member_delivery_array = array(
                                    'delivery_type' => $delivery_type,
                                    'member_id_value' => $member['member_id_value'], // The correpsonding contact_id, user_id, profile_id
                                    'member_id' => $member['member_id'] // The member_id in asset_members table
                                    ) ;

                                // Populates the appropriate asset status id based on the type of asset being delivered to the
                                // recipient. This may need to be adjusted for Series type
                                switch ($automation_item['type_name']) {
                                    case 'email':
                                        $member_delivery_array['email_status_id'] = $asset_status['asset_status_id'] ; // Scheduled
                                        break ; 
                                    case 'sms': 
                                        $member_delivery_array['message_status_id'] = $asset_status['asset_status_id'] ; // Scheduled
                                        break ; 
                                    }

                                $recipient_delivery_array[] = $member_delivery_array ; 
                                $member['delivery_array'] = $member_delivery_array ; 
                                $recipient_full_delivery_array[] = $member ; 
                                }                        


                            break ; 
                        case 'delivery_list_custom':
                        
                            $member_list_options['filter_by_member_id'] = $automation_item['recipients'][$delivery_type][$delivery_trigger]['member_id'] ;

                            
                            // Force series to filter by automation id (on the members stable), otherwise, setting asset members will pull 
                            // everyone in the series
                            switch ($automation_item['type_name']) {
                                case 'series':
                                    $member_list_options['filter_by_automation_id'] = array($automation_item['automation_id']) ; 
                                    break ; 
                                }
                            
                            
                            $asset->Set_Asset_Members_List($member_list_options) ; 
                            $delivery_list = $asset->Get_Asset_Members_List() ; 

                            if (count($delivery_list) == 0) {
                                $zero_count['total_count'] = 0 ; 
                                $asset->Set_Asset_Paging($zero_count) ; 
                                }
                
                            $this->campaign_recipients_count = $asset->Get_Asset_Paging() ;
                            
                            foreach ($delivery_list as $member) {
                                
                                $member_delivery_array = array(
                                    'delivery_type' => $delivery_type,
                                    'member_id_value' => $member['member_id_value'], // The correpsonding contact_id, user_id, profile_id
                                    'member_id' => $member['member_id'], // The member_id in asset_members table
                                    ) ;
                                
                                // Populates the appropriate asset status id based on the type of asset being delivered to the
                                // recipient. This may need to be adjusted for Series type
                                switch ($automation_item['type_name']) {
                                    case 'email':
                                        
                                        $member_delivery_array['email_status_id'] = 1 ; // Default as Error (meaning subscriber metadata_id error) unless we find an allowed sublist subscription metadata_id below
                                        $member_delivery_array['system_alert_id'] = 156 ; // Subscription status metadata id error (default unless success replaces it)
                                        
                                        break ; 
                                    case 'sms': 
                                        
                                        $member_delivery_array['message_status_id'] = 1 ; // Default as Error (meaning subscriber metadata_id error) unless we find an allowed sublist subscription metadata_id below
                                        $member_delivery_array['system_alert_id'] = 179 ; // Subscription status metadata id error (default unless success replaces it)
                                        
                                        break ; 
                                    case 'series':     
                                        
                                        switch ($data['content_asset']['type_name']) {
                                            case 'email':

                                                $member_delivery_array['email_status_id'] = 1 ; // Default as Error (meaning subscriber metadata_id error) unless we find an allowed sublist subscription metadata_id below
                                                $member_delivery_array['system_alert_id'] = 156 ; // Subscription status metadata id error (default unless success replaces it)
                                        
                                                break ; 
                                            case 'sms': 

                                                $member_delivery_array['message_status_id'] = 1 ; // Default as Error (meaning subscriber metadata_id error) unless we find an allowed sublist subscription metadata_id below
                                                $member_delivery_array['system_alert_id'] = 179 ; // Subscription status metadata id error (default unless success replaces it)
                                        
                                                break ;                                                 
                                            }
                                        
                                        break ; 
                                    }


                                if (($automation_item['type_name'] == 'email') OR (($automation_item['type_name'] == 'series') AND ($data['content_asset']['type_name'] == 'email'))) {
                                    
                                    if ((isset($automation_item['recipients'][$delivery_type][$delivery_trigger]['email_metadata_id'])) OR (isset($automation_item['recipients'][$delivery_type][$delivery_trigger]['email_metadata_id']))) {

                                        // Search the metadata array assigned to the campaign delivery settings
                                        foreach ($automation_item['recipients'][$delivery_type][$delivery_trigger]['email_metadata_id'] as $distribution_sublist_email_metadata_id) {

                                            // If we find a matching metadata, then override the default array as success
                                            if ($member['email_metadata_id'] == $distribution_sublist_email_metadata_id) {
                                                $member_delivery_array['email_status_id'] = 4 ; // Scheduled
                                                unset($member_delivery_array['system_alert_id']) ; 
                                                } 
                                            }                                        
                                        foreach ($automation_item['recipients'][$delivery_type][$delivery_trigger]['distribution_sublist_email_metadata_id'] as $distribution_sublist_email_metadata_id) {

                                            // If we find a matching metadata, then override the default array as success
                                            if ($member['email_metadata_id'] == $distribution_sublist_email_metadata_id) {
                                                $member_delivery_array['email_status_id'] = 4 ; // Scheduled
                                                unset($member_delivery_array['system_alert_id']) ; 
                                                } 
                                            }                    
                                        }                            
                                    }

                                
                                
                                if (($automation_item['type_name'] == 'sms') OR (($automation_item['type_name'] == 'series') AND ($data['content_asset']['type_name'] == 'sms'))) {
                                
                                    if ((isset($automation_item['recipients'][$delivery_type][$delivery_trigger]['distribution_sublist_sms_metadata_id'])) OR (isset($automation_item['recipients'][$delivery_type][$delivery_trigger]['sms_metadata_id']))) {

                                        // Search the metadata array assigned to the campaign delivery settings
                                        foreach ($automation_item['recipients'][$delivery_type][$delivery_trigger]['distribution_sublist_sms_metadata_id'] as $distribution_sublist_sms_metadata_id) {

                                            // If we find a matching metadata, then override the default array as success
                                            if ($member['sms_metadata_id'] == $distribution_sublist_sms_metadata_id) {
                                                $member_delivery_array['message_status_id'] = 4 ; // Scheduled
                                                unset($member_delivery_array['system_alert_id']) ; 
                                                } 
                                            }                                        
                                        foreach ($automation_item['recipients'][$delivery_type][$delivery_trigger]['sms_metadata_id'] as $distribution_sublist_sms_metadata_id) {

                                            // If we find a matching metadata, then override the default array as success
                                            if ($member['sms_metadata_id'] == $distribution_sublist_sms_metadata_id) {
                                                $member_delivery_array['message_status_id'] = 4 ; // Scheduled
                                                unset($member_delivery_array['system_alert_id']) ; 
                                                } 
                                            }                    
                                        }
                                    }
                                

                                $recipient_delivery_array[] = $member_delivery_array ; 
                                $member['delivery_array'] = $member_delivery_array ; 
                                $recipient_full_delivery_array[] = $member ; 

                                }     
                            break ; 
                        case 'static':
                            
                            $campaign_record = $this->Get_Campaign() ; 
                            switch ($automation_item['type_name']) {
                                case 'email':
                                    $delivery_list = $campaign_record['structured_data_01']['recipients'][$delivery_type]['static']['email_address'] ; 
                                    break ; 
                                case 'sms': 
                                    $delivery_list = $campaign_record['structured_data_01']['recipients'][$delivery_type]['static']['phone_number'] ; 
                                    break ; 
                                }                            
                            
//                            error_log('Static Message') ; 
//                            error_log('Campaign ID: '.$campaign_record['campaign_id']) ;
                            
                            // $static_recipient = email address for emails and phone number for sms
                            foreach ($delivery_list as $static_recipient) { 

                                $this_recipient_variables = $campaign_record['structured_data_01']['recipients'][$delivery_type]['static']['recipient_variables'][$static_recipient] ; 
                                
                                
                                // Populates the appropriate asset status id based on the type of asset being delivered to the
                                // recipient. This may need to be adjusted for Series type
                                switch ($automation_item['type_name']) {
                                    case 'email':
                                        
                                        $member_delivery_array = array(
                                            'delivery_type' => $delivery_type,
                                            'recipient_email_address' => $static_recipient,
                                            'recipient_name' => $this_recipient_variables['first_name'].' '.$this_recipient_variables['last_name']
                                            ) ;                                        
                                        
                                        $member_delivery_array['email_status_id'] = 4 ; // Default as Scheduled since it's static and we're not checking for a subscription to a list.
                                        break ; 
                                    case 'sms': 
                                        
                                        $member_delivery_array = array(
                                            'delivery_type' => $delivery_type,
                                            'phone_number_international_recipient' => $static_recipient,
                                            'recipient_name' => $this_recipient_variables['first_name'].' '.$this_recipient_variables['last_name']
                                            ) ;
                                        
                                        $member_delivery_array['message_status_id'] = 4 ; // Default as Scheduled since it's static and we're not checking for a subscription to a list.
                                        break ; 
                                    }

                                } 
                            
//                            error_log('Delivery: '.json_encode($member_delivery_array)) ;
                            
                            $recipient_delivery_array[] = $member_delivery_array ; 
                            $member['delivery_array'] = $member_delivery_array ; 
                            $recipient_full_delivery_array[] = $member ;                             
                            break ;                            
                        }


                    }            

                }            


        $this->campaign_recipients_list = $recipient_delivery_array ; 
        $this->campaign_full_recipients_list = $recipient_full_delivery_array ; 
        
        unset($recipient_delivery_array,$recipient_full_delivery_array) ;
       
        $this->Set_Model_Timings('Asset.Action_Set_Campaign_Recipients_List_complete') ; 
        
        return $this ; 
        }
        

        
        
        
///// CHECK THIS BUG REPORT BEFORE DELETING ///////        
        // Deliver by list...
        // Bug ID: 680820379873647/1112606122695528
//        if (is_array($automation_item['recipients']['to']['delivery_list'])) {
//
//            foreach ($automation_item['recipients']['to']['delivery_list'] as $asset_item) {
//
//                $continue = 1 ; 
//
//                $asset_id = $asset_item['asset_id'] ; 
//                $compile_options['filter_by_user_id'] = 'no' ; 
//                $compile_options['filter_by_metadata_id'] = $asset_item['metadata_id'] ; 
//                $compile_options['override_paging'] = 'yes' ; 
//                
//                $asset = new Asset() ; 
//                $asset->Set_Account_ID($automation_item['account_id'])->Set_Asset_By_ID($asset_id) ; 
//                
//                $asset->Set_Asset_Members_List($compile_options) ; 
//                $delivery_list = $asset->Get_Asset_Members_List() ; 
//
//                foreach ($delivery_list as $member) {
//                    $recipient_delivery_array[] = array(
//                        'delivery_type' => 'to',
//                        'member_id_value' => $member['member_id_value'],
//                        'member_id' => $member['member_id'],
//                        'email_status_id' => 4 // Scheduled
//                        ) ; 
//                    }                        
//                }                    
//            }                

        
        
///// THIS SHOULD BE SAFE TO DELETE ///////        
//        if (is_array($automation_item['recipients']['cc']['member_id'])) {
//
//            foreach ($automation_item['recipients']['cc']['member_id'] as $member_id) {
//                $member_id_array[] = $member_id ; 
//                } 
//            
//            $asset_id = $automation_item['delivery_asset_id'] ; 
//            $compile_options['filter_by_user_id'] = 'no' ; 
//            $compile_options['filter_by_member_id'] = $member_id_array ; 
//            $compile_options['override_paging'] = 'yes' ; 
//            
//            $asset = new Asset() ; 
//            $asset->Set_Account_ID($automation_item['account_id'])->Set_Asset_By_ID($asset_id) ; 
//
//            $asset->Set_Asset_Members_List($compile_options) ; 
//            $delivery_list = $asset->Get_Asset_Members_List() ; 
// 
//            
//            foreach ($delivery_list as $member) {
//                
//                // The default member delivery array (which will set as an error status)
//                $member_delivery_array = array(
//                    'delivery_type' => 'cc',
//                    'member_id_value' => $member['member_id_value'],
//                    'member_id' => $member['member_id'],
//                    'email_status_id' => 1, // Default as Error (meaning subscriber metadata_id error) unless we find a metadata_id below
//                    'system_alert_id' => 156 // Subscription status metadata id error (default unless success replaces it)
//                    ) ; 
//                
//                
//                if (isset($automation_item['recipients']['cc']['metadata_id'])) {
//
//                    // Search the metadata array assigned to the campaign delivery settings
//                    foreach ($automation_item['recipients']['cc']['metadata_id'] as $search_metadata_id) {
//                        
//                        // If we find a matching metadata, then override the default array as success
//                        if ($member['metadata_id_distribution_sublist_email'] == $search_metadata_id) {
//                            $member_delivery_array['email_status_id'] = 4 ; // Scheduled
//                            unset($member_delivery_array['system_alert_id']) ; 
//                            } 
//                        }                    
//                    }
//
//
//                $recipient_delivery_array[] = $member_delivery_array ; 
//                }
//            }
        

        
///// THIS SHOULD BE SAFE TO DELETE ///////        
//        if (is_array($automation_item['recipients']['bcc']['member_id'])) {
//
//            foreach ($automation_item['recipients']['bcc']['member_id'] as $member_id) {
//                $member_id_array[] = $member_id ; 
//                } 
//            
//            $asset_id = $automation_item['delivery_asset_id'] ; 
//            $compile_options['filter_by_user_id'] = 'no' ; 
//            $compile_options['filter_by_member_id'] = $member_id_array ; 
//            $compile_options['override_paging'] = 'yes' ; 
//            
//            $asset = new Asset() ; 
//            $asset->Set_Account_ID($automation_item['account_id'])->Set_Asset_By_ID($asset_id) ; 
//
//            $asset->Set_Asset_Members_List($compile_options) ; 
//            $delivery_list = $asset->Get_Asset_Members_List() ; 
// 
//
//            foreach ($delivery_list as $member) {
//                
//                // The default member delivery array (which will set as an error status)
//                $member_delivery_array = array(
//                    'delivery_type' => 'bcc',
//                    'member_id_value' => $member['member_id_value'],
//                    'member_id' => $member['member_id'],
//                    'email_status_id' => 1, // Default as Error (meaning subscriber metadata_id error) unless we find a metadata_id below
//                    'system_alert_id' => 156 // Subscription status metadata id error (default unless success replaces it)
//                    ) ; 
//                
//
//                if (isset($automation_item['recipients']['bcc']['metadata_id'])) {
//
//                    // Search the metadata array assigned to the campaign delivery settings
//                    foreach ($automation_item['recipients']['bcc']['metadata_id'] as $search_metadata_id) {
//                        
//                        // If we find a matching metadata, then override the default array as success
//                        if ($member['metadata_id_distribution_sublist_email'] == $search_metadata_id) {
//                            $member_delivery_array['email_status_id'] = 4 ; // Scheduled
//                            unset($member_delivery_array['system_alert_id']) ; 
//                            } 
//                        }                    
//                    }
//
//                
//
//                $recipient_delivery_array[] = $member_delivery_array ; 
//                }
//            }
                

        
    // We will want to replace this with Action_Set_Campaign_Recipients_List ... I think.
    public function Set_Campaign_Recipients_List($query_parameters = array()) {
        
        $campaign_record = $this->Get_Campaign() ; 
        
        if ($campaign_record['campaign_id']) {
            
            $list_short_name = $campaign_record['delivery_trigger'] ; 
            
            if (isset($campaign_record['recipients']['to'][$list_short_name]['filter_by_profile_id'])) {
                $member_list_options['filter_by_profile_id'] = $campaign_record['recipients']['to'][$list_short_name]['filter_by_profile_id'] ;
                }
            if (isset($campaign_record['recipients']['to'][$list_short_name]['filter_by_account_id'])) {
                $member_list_options['filter_by_account_id'] = $campaign_record['recipients']['to'][$list_short_name]['filter_by_account_id'] ;
                } 
            if (isset($campaign_record['recipients']['to'][$list_short_name]['distribution_list_name'])) {
                $member_list_options['distribution_list_name'] = $campaign_record['recipients']['to'][$list_short_name]['distribution_list_name'] ;
                }    
            if (isset($campaign_record['recipients']['to'][$list_short_name]['filter_by_distribution_sublist_email_metadata_id'])) {
                $member_list_options['filter_by_email_metadata_id'] = $campaign_record['recipients']['to'][$list_short_name]['filter_by_distribution_sublist_email_metadata_id'] ;
                }            
            if (isset($campaign_record['recipients']['to'][$list_short_name]['distribution_sublist_email_metadata_id'])) {
                $member_list_options['email_metadata_id'] = $campaign_record['recipients']['to'][$list_short_name]['distribution_sublist_email_metadata_id'] ;
                }
            if (isset($campaign_record['recipients']['to'][$list_short_name]['filter_by_distribution_sublist_sms_metadata_id'])) {
                $member_list_options['filter_by_sms_metadata_id'] = $campaign_record['recipients']['to'][$list_short_name]['filter_by_distribution_sublist_sms_metadata_id'] ;
                }            
            if (isset($campaign_record['recipients']['to'][$list_short_name]['distribution_sublist_sms_metadata_id'])) {
                $member_list_options['sms_metadata_id'] = $campaign_record['recipients']['to'][$list_short_name]['distribution_sublist_sms_metadata_id'] ;
                }            
            if (isset($campaign_record['recipients']['to'][$list_short_name]['tag_id'])) {
                $member_list_options['tag_id'] = $campaign_record['recipients']['to'][$list_short_name]['tag_id'] ;
                $member_list_options['tag_search_operator'] = $campaign_record['recipients']['to'][$list_short_name]['tag_search_operator'] ;
                }            
            
            $member_list_options['start_page'] = 1 ;

            if ($campaign_record['delivery_asset_id'] > 0) {
                $this->Set_Asset_By_ID($campaign_record['delivery_asset_id']) ; 
                $this->Set_Asset_Members_List($member_list_options) ;  
                
                $data['campaign_recipient_count'] = $this->Get_Asset_Paging() ;                 
                $this->campaign_recipients_count = $data['campaign_recipient_count'] ; 
                
                } else {
                    $zero_count['total_count'] = 0 ; 
                    $this->Set_Asset_Paging($zero_count) ;
                    $data['campaign_recipient_count'] = $this->Get_Asset_Paging() ; 
                    $this->campaign_recipients_count = $data['campaign_recipient_count'] ; 
                
                    
                    }
            

            } 
        
        return $this ; 
        }
   
    
    public function Get_Campaign_Recipients_Count() {
        
        return $this->campaign_recipients_count ; 
        }
    
    
    public function Set_Search_ID($search_id) {
        $this->search_id = $search_id ; 
        return $this ; 
        }
    
    
    // I believe this is used to save public site search results. Not sure if it's being used still
    // Need to confirm
    public function Set_Saved_Search($search_id = 'internal',$asset_options = array(
        'override_paging' => 'no'
        )) {
        
         
        
        if ('internal' === $search_id) {
            
            } else {
                $this->Set_Search_ID($search_id) ; 
                } 
        
        
        $system = new System() ; 
        $saved = $system->Action_Search_Recall($this->search_id,SESSION_PUBLIC_ID) ; 
        $new_table_name = $saved['create_table']['new_table_name'] ; 
        
        $this->search = array(
            'search_name' => $saved['results']['search_name'],
            'search_parameter' => $saved['results']['search_parameter']
            ) ;
     

        $asset_options['saved_search_table'] = $new_table_name ; 
        $asset_options['allow_global_assets'] = 'ignore' ;
        $asset_options['order_by'] = "$new_table_name.key_id" ;
        $asset_options['order'] = 'ASC' ;  
        $asset_options['total_count'] = $saved['saved_search_total_count'] ; 

        $this->Set_Asset_List($asset_options) ; 
        
        $saved['query_options'] = $asset_options ; 
        
        $system->Action_Close_Search_Recall($new_table_name) ; 
        
        return $saved ; 
        }
    
    
    
    public function Set_Asset_Type($type_id,$set_by = 'id') {
        
        $type_record = $this->Retrieve_Asset_Type($type_id,$set_by) ; 
        $map_array = json_decode($type_record['results']['map'],true) ; 
        
        $result = array(
            'type' => $type_record['results'],
            'type_map' => $map_array
            ) ; 
        
        $this->asset_type = $result ; 
        return $this ; 
        }
    
    
    
    // Set list of assets assigned to specific account_id 
    // Optional filters include: asset type, user_id, tags / categories, etc.
    public function Set_Asset_List($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }

        if (!isset($query_options['filter_by_visibility_id']) AND (!isset($query_options['filter_by_visibility_name']))) {
            $query_options['filter_by_visibility_name'] = array('all') ; // All user eligible except hidden (which is admin only)
            }
        
        if (!isset($query_options['filter_by_profile_id'])) {
            $query_options['filter_by_profile_id'] = 'yes' ; 
            }
        
        if ($query_options['filter_by_profile_id'] == 'yes') {            
            if (!isset($query_options['filter_by_asset_auth_role_name'])) {
                $query_options['filter_by_asset_auth_role_name'] = array('asset_editor','asset_owner') ; 
                }
            }
            
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        

        // This is a time saving method to avoid having to pull subassets if necessary.
        if (!isset($query_options['skip_asset_mapping'])) {
            $query_options['skip_asset_mapping'] = 'yes' ; 
            }
        

        $asset_list = $this->Retrieve_Asset_List($query_options) ; 
        $this->Set_Model_Timings('Asset.Set_Asset_List_asset_list_retrieved') ; 
        
        
        // This is a failsafe to ensure that if you're on the 2nd+ page of results and you change parameters
        // and it returns results (but not on the current page, you get reset to page 1)
        if (($asset_list['result_count'] > 0) AND (count($asset_list['results']) == 0)) {
            $query_options['start_page'] = 1 ; 
            $asset_list = $this->Retrieve_Asset_List($query_options) ; 
            $this->Set_Model_Timings('Asset.Set_Asset_List_asset_list_retrieved_failsafe') ; 
            }        
        
        
        $asset_save_result = $asset_list ; 
        
        if ($asset_list['result_count'] == 0) {
            $this->asset_list = 'error' ; 
            } else {
            
                $additional_parameters['skip_asset_mapping'] = $query_options['skip_asset_mapping'] ; 
            
                $asset_final = array() ;
                $asset = $this->Action_Map_Asset($asset_list,$additional_parameters) ; 
                $this->Set_Model_Timings('Asset.Set_Asset_List_mapping_complete') ; 

                $i = 0 ; 
                foreach ($asset['results'] as $asset_record) {
                    
                    $asset_record = $this->Action_Time_Territorialize_Dataset($asset_record) ; 
            
                    $asset_record['asset_auth_role_permissions_array'] = json_decode($asset_record['asset_auth_role_permissions'],true) ; 
                    
                    $public_link = $this->Set_Asset_Public_Link($asset_record)->Get_Asset_Public_Link() ; 
                    $asset_record['public_link'] = $public_link['public_link'] ;
                    $asset_record['public_link_url'] = $public_link['public_link_url'] ;
                    
                    if ($query_options['minify'] == 'yes') {
                        
                        $this_asset = array() ; 
                        foreach ($asset_record as $asset_key => $asset_value) {
                            if (in_array($asset_key,$query_options['minify_fields'])) {
                                $this_asset[$asset_key] = $asset_value ; 
                                }
                            }
                        
                        $asset_final[] = $this_asset ; 
                        } else {
                            $asset_final[] = $asset_record ;     
                            }
                    
                    $i++ ; 
                    }

                $this->asset_list = $asset_final ;
                }        
        
//        $this->asset_query_result = $asset_save_result ;

        return $this ; 
        }

    
    
    public function Set_Page_Increment($page_increment) {
        $this->page_increment = $page_increment ; 
        return $this ; 
        }
    
    

    
    
    
    // Set asset_id and the top level asset info
    public function Set_Asset_ID($asset_identifer = 'internal') {
        
        if ('internal' === $asset_identifer) {

            } else {
                $this->asset_id = $asset_identifer ;      
                }
        
        return $this ; 
        }
    
    
    // Set asset_id and the top level asset info
    // Incoming $asset_id can be a slug or link_id
    public function Set_Asset_By_ID($asset_id,$additional_parameters = array()) {
        
        if (!isset($additional_parameters['skip_fields'])) {
            $additional_parameters['skip_fields'] = array() ; 
            }
        if (!isset($additional_parameters['set_by'])) {
            $additional_parameters['set_by'] = 'id' ; 
            }        

        switch ($additional_parameters['set_by']) {
            case 'slug':
                $asset = $this->Retrieve_Asset_ID_By_Slug($asset_id) ; 
                if ($asset['result_count'] == 0) {
                    $this->asset_id = 'error' ; 
                    } else {
                        $this->asset_id = $asset['results']['asset_id'] ;
                        }   
                
                $this->Set_Asset($this->asset_id,$additional_parameters) ; 
                break ;
            case 'public_link': // Requires public link id to be correct AND the secure link
                $asset_link_parts = explode("_",$asset_id) ; // Full public link separated by underscore

                $asset = $this->Retrieve_Asset_ID_By_Public_Link_ID($asset_link_parts[0]) ; 
                
                if ($asset['result_count'] == 0) {
                    $this->asset_id = 'error' ; 
                    } else {
                        
                        $this->asset_id = $asset['results']['asset_id'] ;

                        $this->Set_Asset($this->asset_id,$additional_parameters) ; 
                                        
                        $link_array['full_link'] = $asset_id ; 
                        $link_validation = $this->Action_Validate_Asset_Link($link_array) ; 
                    
                        $this->test_data = $link_validation ; 

                    
                        if ($link_validation['effect'] == 'deny') {
                            unset($this->asset,$this->asset_id) ;
                            $this->asset = 'error' ; 
                            }
                        }                
                
                break ; 
            case 'public_link_id':
                $asset = $this->Retrieve_Asset_ID_By_Public_Link_ID($asset_id) ; 
                if ($asset['result_count'] == 0) {
                    $this->asset_id = 'error' ; 
                    } else {
                        $this->asset_id = $asset['results']['asset_id'] ;
                        }  
                
                $this->Set_Asset($this->asset_id,$additional_parameters) ; 
                break ;                
            case 'id':
            default:
                $this->asset_id = $asset_id ;
                $this->Set_Asset($this->asset_id,$additional_parameters) ; 
            }         
        
        
        return $this ; 
        }    
    
    
    public function Set_Asset_By_History_ID($asset_history_id,$additional_parameters = array()) {
        
        if (!isset($additional_parameters['skip_fields'])) {
            $additional_parameters['skip_fields'] = array() ; 
            }
        
        $query_options['history_id'] = $asset_history_id ; 
        $result = $this->Retrieve_Asset_History_List($this->asset_id,$query_options) ; 
        
        $asset_history = $result['results'][0] ; 
        
        $asset_history['asset_master'] = json_decode($asset_history['asset_master'],1) ; 
        
        $this->asset_history = $asset_history ; 

        
        return $this ; 
        }
    
    
    // WHEN YOU PROCESS THIS
    // You need to weed out certain fields (like slug) which should not be subject to version control
    // Grab version latest, and then overwrite latest with content from the specific history id
    // Weeding out fields that SHOULD NOT be restored (like slug)
    public function Action_Restore_Asset_Version($asset_history_id) {
        
        $asset_record = $this->Get_Asset() ; // Get the CURRENT master status
        $map_array = $asset_record['map_array'] ; 
        
        // Get current asset history
        $additional_parameters['history_id'] = $asset_record['current_history_id'] ;
        $current_asset_history = $this->Set_Asset_History('internal',$additional_parameters)->Get_Asset_History() ;
        
        
        // Get latest asset history
        $additional_parameters['history_id'] = 'latest' ; 
        $latest_asset_history = $this->Set_Asset_History('internal',$additional_parameters)->Get_Asset_History() ;         
        
        // Get restore asset history
        $restore_asset_history = $this->Set_Asset_By_History_ID($asset_history_id)->Get_Asset_History() ;
        
        
        // Replace latest master fields with eligible restore master fields
        $restore_asset_history_original_asset_master = $restore_asset_history['asset_master'] ; 
        $restore_asset_history['asset_master'] = $latest_asset_history['asset_master'] ; 
        
        foreach ($restore_asset_history_original_asset_master as $master_key => $master_value) {
            if (in_array($master_key, $this->asset_restore_fields)) {
                $restore_asset_history['asset_master'][$master_key] = $master_value ; 
                }            
            }
        
        
        // Set the new visibility as draft for the restored version
        $visibility_query_options['filter_by_visibility_name'] = 'visibility_draft' ; 
        $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
        $visibility_id = $visibility_result['visibility_id'] ;
        $visibility_name = $visibility_result['visibility_name'] ;
        
        
        // Remove and add fields
        unset($restore_asset_history['history_id'],$restore_asset_history['asset_master']['visibility_id']) ; 
        $restore_asset_history['microtimestamp_asset_history_updated'] = microtime(true) ;
        $restore_asset_history['asset_master']['visibility_id'] = $visibility_id ; 
        $restore_asset_history['asset_master']['visibility_name'] = $visibility_name ; 
        $restore_asset_history['asset_master'] = json_encode($restore_asset_history['asset_master']) ; 
        
        
        // Insert the new history
        $query_array = array(
            'table' => 'asset_content_history',
            'values' => $restore_asset_history
            );

        $history_result = $this->DB->Query('INSERT',$query_array) ;
        $new_history_id = $history_result['insert_id'] ;

        $this->test_data = $result ; 
        $this->Set_Asset() ; 
        
        if ($history_result['insert_id']) {
            $this->Set_Alert_Response(218) ; // Asset restored.
            
            // Create History Entry
            $history_input = array(
                'account_id' => $this->account_id,
                'asset_id' => $this->asset_id,
                'function' => 'asset_restore',
                'notes' => json_encode(array('history_id' => $asset_history_id))
                ) ; 
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;
            
            } else {
            
                $this->Set_Alert_Response(277) ; // Asset restore error.

                // Create History Entry
                $history_input = array(
                    'account_id' => $this->account_id,
                    'asset_id' => $this->asset_id,
                    'function' => 'asset_restore',
                    'error' => 1,
                    'notes' => json_encode(array('history_id' => $asset_history_id,'sql_error' => $history_result['error']))
                    ) ; 
                $user_history = $this->Create_User_History($this->user_id,$history_input) ;            
            
                }
        
        
        return $this ; 
        }
    
    

    
    
    // Retrieve and set individual asset history entry for an asset based off parameters
    public function Set_Asset_History($asset_id = 'internal',$additional_parameters = array()) {
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            }
                
        // Setting history_id as 'latest' will pull the most recently edited version
        // Otherwise a specific history_id version can be requested.
        if ($additional_parameters['history_id'] == 'latest') {
            $asset_history_params['history_id'] = $additional_parameters['history_id'] ; 
            $this->Set_Page_Increment(1) ; 
            } else if ($additional_parameters['history_id']) {
                $asset_history_params['history_id'] = $additional_parameters['history_id'] ; 
                }
 
        
        // Pull the asset version based on the requested parameters above, and process into the asset array
        $result = $this->Retrieve_Asset_History_List($asset_id,$asset_history_params) ; 
        
        
        $asset_history = $result['results'][0] ; 
        $asset_history_result = $this->Action_Process_Asset_History($asset_history) ; 

        $this->asset_history = $asset_history_result ;         
        return $this ; 
        }
        
    
    // This isn't used for anything useful as of 8/10/21
    public function Set_Asset_History_List($asset_id = 'internal',$additional_parameters = array()) {
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            }
                
        // Setting history_id as 'latest' will pull the most recently edited version
        // Otherwise a specific history_id version can be requested.
        if ($additional_parameters['history_id'] == 'latest') {
            $asset_history_params['history_id'] = $additional_parameters['history_id'] ; 
            } else if ($additional_parameters['history_id']) {
                $asset_history_params['history_id'] = $additional_parameters['history_id'] ; 
                }
 
        
        // Pull the asset version based on the requested parameters above, and process into the asset array
        $result = $this->Retrieve_Asset_History_List($asset_id,$asset_history_params) ; 
 
        $i = 0 ; 
        foreach ($result['results'] as $asset_history) {
            $result['results'][$i] = $this->Action_Process_Asset_History($asset_history) ; 
            $i++  ;
            }
        

        $this->asset_history = $result['results'] ;         
        return $this ; 
        }
    
    
    
    // Get the individual asset history entry for an asset
    public function Get_Asset_History() {
        
        return $this->asset_history ; 
        }
        
    
    // Set asset_id and the top level asset info
    // condense_asset = yes will skip history and skip_asset_mapping
    // skip_asset_mapping means sub assets will not be fully compiled
    public function Set_Asset($asset_id = 'internal',$additional_parameters = array()) {

        $this->Set_Model_Timings('Asset.Set_Asset_begin_'.$asset_id) ; 
        
        
        // IMPORTANT
        // $additional_parameters['skip_fields'] should be used to prevent infinite loops, looking for endlessly 
        // related assets. submit the parameter as an array of field names [e.g. 'skip_fields' => array('author_profile','related_products') ]
        if (!isset($additional_parameters['skip_fields'])) {
            $additional_parameters['skip_fields'] = array() ; 
            }        
        
        if ($additional_parameters['condense_asset'] == 'yes') {
            $additional_parameters['skip_history'] = 'yes' ;
            $additional_parameters['skip_asset_mapping'] = 'yes' ;     
            }
        
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            } else {
                $this->asset_id = $asset_id ;      
                } 
        
        

        $asset = $this->Retrieve_Asset() ;      
        $this->Set_Model_Timings('Asset.Set_Asset_asset_retrieved_'.$asset_id) ; 
                
        
        if ($asset['result_count'] == 0) {
            $this->asset = 'error' ; 
            
            $query_input['asset_id'] = $asset_id ;
            $query_input['additional_paremeters'] = $additional_parameters ;
                
            $this->Set_Alert_Response(205) ; // Slug taken by another asset
            $this->Append_Alert_Response('none',array(
                'admin_context' => json_encode($query_input),
                'location' => __METHOD__
                )) ;            
            
            } else {
            
                $data['account_record'] = $this->Get_Account() ; 
            
//                $asset['results'] = $asset['results'][0] ; // Moved the first result down from the array if using Retrieve_Asset_List ... which we're not because that requires using the account_id
            
                // Map and time territorialize the asset core
                $asset_core = $this->Action_Map_Asset($asset,$additional_parameters) ; 
                $asset['results']['asset_core'] = $this->Action_Time_Territorialize_Dataset($asset_core['results']) ; 
            
                $asset['results']['active_history_id'] = $asset['results']['current_history_id'] ; // Default the active_history_id to the version set in master
            
            
                if ($asset['results']['asset_structure_count'] > 0) { 
                    
                    $asset_structure = $this->Retrieve_Asset_Structure() ; 
//                    $asset['results']['structure_query'] = $asset_structure ; 
                    $this->Set_Model_Timings('Asset.Set_Asset_asset_structure_retrieved_'.$asset_id) ;         
                    

                    // Set structure paging array...
                    $structure_paging = new Asset() ; 
                    $structure_query_options->value_name = 'structure_order' ;
                    $structure_paging_constraints = array() ; 

                    $asset_structure = $structure_paging->Action_Process_Query_Results($asset_structure,$structure_query_options,$structure_paging_constraints) ; 
                    
//                    $asset['results']['structure_paging_intermediate'] = $asset_structure['results'] ;
                    
                    $structure_paging->Set_Asset_Paging($asset_structure) ; 
                    
                    $asset['results']['structure'] = $asset_structure['results'] ;
                    $asset['results']['structure_paging'] = $structure_paging->Get_Asset_Paging() ;
                    
                    $this->Set_Model_Timings('Asset.Set_Asset_asset_structure_processed_'.$asset_id) ;         
                    } else {
                        $asset['results']['asset_structure_count'] = 0 ; 
                        }

                
            
                // If a version of the asset is requested other than the current_history_id set in master,
                // then pull it here and process the results into the asset display.
                if ($additional_parameters['history_id']) {
                      
                    $asset_history = $this->Set_Asset_History('internal',$additional_parameters)->Get_Asset_History() ;

                    // IMPORTANT ***** We need to only process in some items from asset_master
                    foreach ($asset_history['asset_master'] as $master_key => $master_value) {
                        switch ($master_key) {
                            case 'asset_title':
                            case 'asset_description': 
                            case 'asset_slug':
                            case 'global_asset': 
                            case 'comments_allowed': 
                            case 'comments_approval': 
                            case 'timestamp_asset_comments_closed': 
                            case 'timestamp_asset_published':     
                                $asset['results'][$master_key] = $master_value ; 
                                break ; 
                            }                        
                        }
                    
                    $asset['results']['microtimestamp_asset_history_updated'] = $asset_history['asset_info']['microtimestamp_asset_history_updated'] ; // Has already been compiled i think...
                    $asset['results']['structure'] = $asset_history['asset_structure'] ; 
                    
                    
                    unset($asset_history['asset_master'],$asset_history['asset_id']) ; 
                    foreach ($asset_history['asset_content'] as $content_key => $content_value) {
                        $asset['results'][$content_key] = $content_value ; 
                        } 
                    
                    $asset['results']['active_history_id'] = $asset_history['asset_info']['history_id'] ; // Set the active_history_id as the version that is currently being displayed
                    $this->Set_Model_Timings('Asset.Set_Asset_asset_history_set_'.$asset_id.'_historyid_'.$additional_parameters['history_id']) ;
                    }
               
            
                // Compile the delay of the asset structure...
                if (is_array($asset['results']['structure']) AND (count($asset['results']['structure'] > 0))) { 
                    $s = 0 ; 
                    foreach ($asset['results']['structure'] as $structure_item) {
                        $structure_item['delay_compiled'] = Utilities::System_Time_Convert_Seconds($structure_item['delay'],array('ingore_years' => 'yes')) ; 
                        $asset['results']['structure'][$s] = $structure_item ; 
                        $s++ ;
                        } 
                    $this->Set_Model_Timings('Asset.Set_Asset_asset_structure_set_'.$asset_id) ;
                    }
            
            
                
                // This sets a default visibility setting for the asset.            
                if ($asset['results']['account_asset_visibility_id'] == NULL) {
            
                    $this->Set_Model_Timings('Asset.Set_Asset_set_default_visibility_id_begin_'.$asset_id) ;
            
                    if (($asset['results']['global_asset'] == 1) AND ($asset['results']['account_id'] != $this->account_id)) {
                        
                        // Can't quite remember how this is used at the moment but it's important
                        $visibility_query_options['filter_by_visibility_name'] = 'visibility_draft' ; 
                        $visibility_query_options = (object) $visibility_query_options ; 
                        $default_account_visibility = $this->Retrieve_Visibility_ID_Array($visibility_query_options)[0] ;                         
                        
                        $asset['results']['account_asset_visibility_id'] = $default_account_visibility['visibility_id'] ; 
                        $asset['results']['account_asset_visibility_name'] = $default_account_visibility['visibility_name'] ; 
                        $asset['results']['account_asset_visibility_title'] = $default_account_visibility['visibility_title'] ; 
                        $asset['results']['account_asset_visibility_description'] = $default_account_visibility['visibility_description'] ;
                        
                        } else {
                        
                            $asset['results']['account_asset_visibility_id'] = $asset['results']['visibility_id'] ; 
                            $asset['results']['account_asset_visibility_name'] = $asset['results']['visibility_name'] ; 
                            $asset['results']['account_asset_visibility_title'] = $asset['results']['visibility_title'] ; 
                            $asset['results']['account_asset_visibility_description'] = $asset['results']['visibility_description'] ;                        
                            }
                    
                    $this->Set_Model_Timings('Asset.Set_Asset_set_default_visibility_id_complete_'.$asset_id) ;
                    }
        
            
            
                // Pull full asset history list with paging overridden
                if ($additional_parameters['skip_history'] != 'yes') {
                    
                    // Get asset history and add it as an entry to the asset array
                    $history_options['order_by'] = 'asset_content_history.microtimestamp_asset_history_updated' ; 
                    $history_options['order'] = 'DESC' ; 
                    $history_options['override_paging'] = 'yes' ; 
                    $asset_history = $this->Retrieve_Asset_History_List($this->asset_id,$history_options) ; 

                    $asset['results']['history'] = array() ; 

                    foreach ($asset_history['results'] as $history) {
                        $history['history_compiled'] = $this->Action_Process_Asset_History($history) ;    
                        $asset['results']['history'][] = $history ;  
                        }
                    
                    $this->Set_Model_Timings('Asset.Set_Asset_asset_history_retrieved_'.$asset_id) ;  
                    }
            
            
                // Pull asset tags and categories
                if ($additional_parameters['condense_asset'] != 'yes') {
                    
                    $this->Set_Model_Timings('Asset.Set_Asset_compile_metadata_begin_'.$asset_id) ;  
                    
                    // Assign the appropriate tags and categories to an asset
                    $metadata_query_options = array(
                        'filter_by_visibility_name' => 'visibility_published',
                        'asset_id' => $asset_id
                        ) ;
                    $metadata_results = $this->Retrieve_Asset_Metadata_List('tag,category',$metadata_query_options) ;


                    $asset['results']['tag'] = array() ; 
                    $asset['results']['tag_all'] = array() ; 
                    $asset['results']['category'] = array() ; 

                    if ($metadata_results['result_count'] > 0) {
                        foreach ($metadata_results['results'] as $metadata) {
                            $asset['results'][$metadata['metadata_type']][] = $metadata ; 
                            }
                        }

                    $metadata_all_query_options = array(
                        'filter_by_visibility_name' => 'visibility_published',
                        'filter_by_account_id' => 'none',
                        'asset_id' => $asset_id
                        ) ;

                    $metadata_results = $this->Retrieve_Asset_Metadata_List('tag',$metadata_all_query_options) ;            
                    if ($metadata_results['result_count'] > 0) {
                        foreach ($metadata_results['results'] as $metadata) {
                            $asset['results']['tag_all'][] = $metadata ; 
                            }
                        }            


                    $new_members_only = Utilities::Deep_Array($asset['results']['tag_all'], 'metadata_slug', 'new-members-only') ; 

                    if ($new_members_only) {
                        $asset['results']['new_members_only'] = 'yes' ; 
                        } else {
                            $asset['results']['new_members_only'] = 'no' ; 
                            }
                    
                    $this->Set_Model_Timings('Asset.Set_Asset_compile_metadata_end_'.$asset_id) ;  
                    }
            
            
                // Provides some customized HTML manipulation for specific fields
                if ($additional_parameters['format'] == 'yes') {
                    $asset['results'] = $this->Action_Format_Asset($asset['results']) ;  
                    $this->Set_Model_Timings('Asset.Set_Asset_action_format_asset_returned_'.$asset_id) ; 
                    }
 
            
                // Decode the structured data array
                $asset['results']['structured_data_array_01'] = json_decode($asset['results']['structured_data_01'],1) ;
                if (isset($asset['results']['structured_data_array_01'])) {
                    foreach ($asset['results']['structured_data_array_01'] as $key => $value) {
                        $asset['results'][$key] = $value ; 
                        }
                    }
            
                // Decode the structured data array for the parent asset
                $asset['results']['parent_structured_data_array_01'] = json_decode($asset['results']['parent_structured_data_01'],1) ;
                if (isset($asset['results']['parent_structured_data_array_01'])) {
                    foreach ($asset['results']['parent_structured_data_array_01'] as $key => $value) {
                        $asset['results']['parent_structured_data'][$key] = $value ; 
                        }
                    }
            
            
                // Map the asset
                $asset = $this->Action_Map_Asset($asset,$additional_parameters) ; 
                $this->Set_Model_Timings('Asset.Set_Asset_action_map_asset_returned_'.$asset_id) ;
            
            
                // Pull info for related assets
                $related_assets = array() ;
                if ($additional_parameters['condense_asset'] != 'yes') {
                    if ($asset['results']['related_assets']) {
                        $related_asset_query_options['override_paging'] = 'yes' ; 
                        $related_asset_query_options['filter_by_visibility_name'] = 'visible' ; 
                        $related_asset_query_options['allow_global_assets'] = 'all' ; 
                        $related_asset_query_options['filter_by_asset_id'] = Utilities::Process_Comma_Separated_String($asset['results']['related_assets']) ;
                        $related_assets = $this->Set_Asset_List($related_asset_query_options)->Get_Asset_List() ;                     
                        $this->Set_Model_Timings('asset_related_asset_list_set_for_'.$asset_id) ;  
                        }
                    }
                $asset['results']['related_assets'] = $related_assets ;             

            
                // Territorialize dates of the asset
                $asset['results'] = $this->Action_Time_Territorialize_Dataset($asset['results']) ;  

            
                // Toggle comments_open based on whether they are allowed, and whether or not the closed timestamp has passed
                if (($asset['results']['comments_allowed'] == 1) AND (($asset['results']['timestamp_asset_comments_closed'] > TIMESTAMP) OR ($asset['results']['timestamp_asset_comments_closed'] == 0))) {
                    $asset['results']['comments_open'] = 1 ; 
                    
                    // Check global post and global post comment settings
                    if (($asset['results']['global_asset'] == 1) AND ($asset['results']['account_id'] != $data['account_record']['account_id'])) {
                        switch ($asset['results']['type_name']) {
                            case 'blog_post':
                                
                                switch ($data['account_record']['customization_allow_global_blog_posts']) {
                                    case 'no':
                                        $asset['results']['comments_open'] = 0 ; 
                                        break ; 
                                    default: 
                                        switch ($data['account_record']['customization_allow_global_blog_post_comments']) {
                                            case 'no':
                                                $asset['results']['comments_open'] = 0 ; 
                                                break ; 
                                            default:    
                                            }                                         
                                    }
                                
                                break ; 
                            case 'product':
                                
                                switch ($data['account_record']['customization_allow_global_product_posts']) {
                                    case 'no':
                                        $asset['results']['comments_open'] = 0 ; 
                                        break ; 
                                    default: 
                                        switch ($data['account_record']['customization_allow_global_product_post_comments']) {
                                            case 'no':
                                                $asset['results']['comments_open'] = 0 ; 
                                                break ; 
                                            default:    
                                            }                                         
                                    }
                                
                                break ; 
                            }
                        
                        
                        // Disable comments based on Website Settings > Disable Global Post Comments - no comments allowed for any GLOBAl post
                        if ($data['account_record']['customization_disable_global_post_comments'] == 'yes') {
                            $asset['results']['comments_open'] = 0 ; 
                            }                        
                        
                        }
                       
                    
                    // Disable comments based on Website Settings - no comments allowed for any post
                    if ($data['account_record']['customization_disable_asset_comments'] == 'yes') {
                        $asset['results']['comments_open'] = 0 ; 
                        }
                    
                    } else {
                        $asset['results']['comments_open'] = 0 ;
                        }
                                    
                
            
                // Retreive global asset restrictions
                $this->Set_Model_Timings('Asset.Set_Asset_pre_asset_restrictions_retrieved_'.$asset_id) ;  
                $result_restrictions = $this->Retrieve_Global_Asset_Restrictions() ;
                $this->Set_Model_Timings('Asset.Set_Asset_asset_restrictions_retrieved_'.$asset_id) ;  
                    
                if ($result_restrictions['result_count'] > 0) {
                    // Get the profiles for each auth and add them to the asset record
                    $i = 0 ; 
                    foreach ($result_restrictions['results'] as $restriction) {
                        
                        if ($restriction['restriction_type']) {
                            $asset['results']['global_product_restrictions'][] = $restriction ; 
                            }                        
                        $i++ ; 
                        }                    
                    }
            
            
            
                // Retreive asset_auth; pair authorization w/ user & profile info
                $result_auth = $this->Retrieve_Asset_Authorization() ;
                $this->Set_Model_Timings('Asset.Set_Asset_asset_auth_retrieved_'.$asset_id) ;  

                if ($result_auth['result_count'] > 0) {
                    // Get the profiles for each auth and add them to the asset record
                    $i = 0 ; 
                    foreach ($result_auth['results'] as $auth) {

                        $result_auth['results'][$i] = $this->Action_Time_Territorialize_Dataset($result_auth['results'][$i]) ;
                        $result_auth['results'][$i] = $this->Action_Phone_Territorialize_Dataset($result_auth['results'][$i],$result_auth['results'][$i]['phone_country_dialing_code']) ; 

                        if ($auth['asset_auth_role_name'] == 'asset_owner') {
                            $asset['results']['owner_profile'] = $result_auth['results'][$i] ; 
                            }
                        if ($auth['author'] == 1) {
                            $asset['results']['authors'][] = $result_auth['results'][$i] ; 
                            }                        
                        $i++ ; 
                        }                    
                    }

                // Look to see if there is an authorization set for the active profile, and add THAT to the asset array
                $asset['results']['active_profile'] = $this->Get_Profile() ; 
                $active_profile_auth = Utilities::Deep_Array($result_auth['results'],'profile_id',$asset['results']['active_profile']['profile_id']) ; 
                if ($active_profile_auth['profile_id']) {
                    $asset['results']['active_profile'] = $active_profile_auth ; 
                    } 

                $asset['results']['authorization'] = $result_auth['results'] ;

            
            
                $asset['results']['type_display_name_lc'] = strtolower($asset['results']['type_display_name']) ;             
            
                if ($additional_parameters['minify'] == 'yes') {
                    
                    $minified_asset = array() ; 
                    foreach ($asset['results'] as $key => $value) {
                        switch ($key) {
                            case 'asset_id':
                            case 'asset_title':
                            case 'asset_description':
                            case 'type_id':
                            case 'type_name':
                            case 'type_display_name': 
                            case 'type_display_name_lc':     
                            case 'visibility_id':
                            case 'visibility_name':     
                            case 'visibility_title':                                    
                            case 'timestamp_asset_created':  
                            case 'timestamp_asset_updated':      
                                $minified_asset[$key] = $value ;                                 
                                break ; 
                            }
                        }
                    $asset['results'] = $minified_asset ; 
                    }
            

                unset($asset['results']['asset_core']) ; 
                $this->asset = $asset['results'] ;
                
            
                // Get asset member count if Profile or User list
                // Contact list count already provided in initial Retrieve_Asset query
                switch ($asset['results']['type_id']) {
                    case '20':
                    case '23':
                        $this->Set_Asset_Members_Count($asset['results']) ; 
                        break ;
                    }
            
                        
                // Set authorizations for the current active profile / user so actions can be enabled in the view.
                $this->Action_Set_Asset_Auth_Permissions() ;
                $this->Set_Model_Timings('Asset.Set_Asset_asset_auth_permissions_set_'.$asset_id) ;  
                
                // Set public link for asset
                $public_link = $this->Set_Asset_Public_Link()->Get_Asset_Public_Link() ; 
            
        
                $personalization['asset_record'] = $asset['results'] ;             
                $this->Set_Alert_Response(261) ; // Retrieved asset
                $this->Append_Alert_Response($personalization,array(
                    'location' => __METHOD__
                    )) ;
            
                $this->Set_Model_Timings('Asset.Set_Asset_end_set_asset_'.$asset_id) ;  
                }

        $this->asset_query_result = $asset ; 
        return $this ; 
        }


    public function Retrieve_Asset_Authorization($asset_id = 'internal',$query_options = array()) {
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            } else {
                $this->asset_id = $asset_id ;      
                }


        $query_array = array(
            'table' => 'asset_auth',
            'join_tables' => array(),
            'fields' => "
                user_profiles.*, 
                CONCAT(user_profiles.first_name,' ',user_profiles.last_name) AS full_name, 
                accounts.*, 
                system_auth_roles.*,
                asset_auth.*, system_asset_auth_roles.*, 
                user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                user_emails.email_address, user_emails.email_verified, 
                users.user_id, users.city, users.state, users.country_id, list_countries.country_value, 
                user_messaging_numbers.messaging_number_id, user_messaging_numbers.messaging_phone_country_id, 
                user_messaging_numbers.phone_number_messaging, user_messaging_numbers.messaging_notify_service_id, user_messaging_numbers.messaging_phone_active  
                ",
            'where' => "asset_auth.asset_id='$asset_id'"
            );

        $query_array['join_tables'][] = array(
            'table' => 'system_asset_auth_roles',
            'on' => 'system_asset_auth_roles.asset_auth_id',
            'match' => 'asset_auth.asset_auth_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'user_profiles',
            'on' => 'user_profiles.profile_id',
            'match' => 'asset_auth.profile_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'accounts',
            'on' => 'accounts.account_id',
            'match' => 'user_profiles.account_id'
            );         
        
        $query_array['join_tables'][] = array(
            'table' => 'system_auth_roles',
            'on' => 'system_auth_roles.auth_id',
            'match' => 'user_profiles.account_auth_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_emails',
            'on' => 'user_profiles.email_address_id',
            'match' => 'user_emails.email_address_id',
            'type' => 'left'
            );
            
        $query_array['join_tables'][] = array(
            'table' => 'user_phone_numbers',
            'on' => 'user_profiles.phone_number_id',
            'match' => 'user_phone_numbers.phone_number_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'users',
            'on' => 'users.user_id',
            'match' => 'user_profiles.user_id'
            );

        $query_array['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'users.country_id',
            'match' => 'list_countries.country_id'
            );                
                
        $query_array['join_tables'][] = array(
            'table' => 'user_messaging_numbers',
            'on' => 'user_profiles.messaging_number_id',
            'match' => 'user_messaging_numbers.messaging_number_id',
            'type' => 'left'
            );        
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        return $result ; 
        }
    
    
    public function Retrieve_Global_Asset_Restrictions($asset_id = 'internal',$query_options = array()) {
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            } else {
                $this->asset_id = $asset_id ;      
                }


        $query_array = array(
            'table' => 'asset_global_restrictions',
            'join_tables' => array(),
            'fields' => "
                asset_global_restrictions.*, 
                assets.asset_title
                ",
            'where' => "asset_global_restrictions.asset_id='$asset_id'"
            );

        $query_array['join_tables'][] = array(
            'table' => 'assets',
            'on' => 'assets.asset_id',
            'match' => 'asset_global_restrictions.asset_id'
            ); 
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        return $result ; 
        }
    
    
    
    
    // Set the previous and next assets adjacent to the currently set asset_id
    public function Set_Asset_Adjacent($asset_list_options = array()) {
        
        if (!isset($asset_list_options['page_increment'])) {
            $page_increment = 1 ; 
            }
        
        
        $asset_previous_options = array() ; 
        $asset_previous_options['start_page'] = 1 ; 
        $asset_previous_options['order_by'] = "asset_core.timestamp_asset_published" ; 
        $asset_previous_options['order'] = 'DESC' ; 
        
        $asset_previous_options['order_field'] = "asset_core.timestamp_asset_published" ; 
        $asset_previous_options['order_start'] = $this->asset['timestamp_asset_published'] ; 
        $asset_previous_options['order_operator'] = "<" ;
        
        if (isset($asset_list_options['allow_global_assets'])) {
            $asset_previous_options['allow_global_assets'] = $asset_list_options['allow_global_assets'] ; 
            }         
        if (isset($asset_list_options['filter_by_visibility_name'])) {
            $asset_previous_options['filter_by_visibility_name'] = $asset_list_options['filter_by_visibility_name'] ; 
            }        
        if (isset($asset_list_options['filter_by_type_name'])) {
            $asset_previous_options['filter_by_type_name'] = $asset_list_options['filter_by_type_name'] ; 
            }

        $this->Set_Page_Increment($page_increment)->Set_Asset_List($asset_previous_options) ; 
        if ($this->Get_Asset_List() != 'error') {
            $this->asset['previous'] = $this->Get_Asset_List() ;        
            }
        

        $asset_next_options = array() ;         
        $asset_next_options['start_page'] = 1 ; 
        $asset_next_options['order_by'] = "asset_core.timestamp_asset_published" ; 
        $asset_next_options['order'] = 'ASC' ; 
        
        $asset_next_options['order_field'] = "asset_core.timestamp_asset_published" ; 
        $asset_next_options['order_start'] = $this->asset['timestamp_asset_published'] ; 
        $asset_next_options['order_operator'] = ">" ;
        
        if (isset($asset_list_options['allow_global_assets'])) {
            $asset_next_options['allow_global_assets'] = $asset_list_options['allow_global_assets'] ; 
            }        
        if (isset($asset_list_options['filter_by_visibility_name'])) {
            $asset_next_options['filter_by_visibility_name'] = $asset_list_options['filter_by_visibility_name'] ; 
            }        
        if (isset($asset_list_options['filter_by_type_name'])) {
            $asset_next_options['filter_by_type_name'] = $asset_list_options['filter_by_type_name'] ; 
            }

        $this->Set_Page_Increment($page_increment)->Set_Asset_List($asset_next_options) ; 
        if ($this->Get_Asset_List() != 'error') {
            $this->asset['next'] = $this->Get_Asset_List() ;    
            }
        
        
        }
    
    public function Action_Find_All_Strings($haystack, $needle_start, $needle_end) {

        $continue = 1 ; 
        $found_strings = array() ; 
        $next_position = 0 ; 
        
        do {

            $sub_string_result = Utilities::Extract_Text_Block($haystack, $needle_start, $needle_end, $next_position); // Gathers the replaceable string 

            if (strlen($sub_string_result['extraction']) > 0) {

                $found_string = $needle_start.$sub_string_result['extraction'].$needle_end ; 
                $found_strings[] = $found_string ;
                $next_position = $sub_string_result['next_position'] ; 

                } else {
                    $continue = 0 ; 
                    }

            } while ($continue == 1) ; 

        return $found_strings ; 
        }
    
    

    public function Action_Format_Asset($asset_dataset,$additional_parameters = array()) {
        
        foreach ($asset_dataset as $key => $value) {
            
            switch ($key) {
                case 'text_01':
                case 'text_02':
                    
                    $found_strings = $this->Action_Find_All_Strings($value,'<iframe src=','</iframe>') ; 
                    foreach ($found_strings as $string_to_replace) {
                        $value = str_replace($string_to_replace,'<div class="media-embed">'.$string_to_replace.'</div>',$value) ;  // Removes the file string
                        }

                    $asset_dataset[$key] = $value ; 
                    break ; 
                }
            
            }
        
        return $asset_dataset ; 
        }
    
    
    // Set the tag list array
    public function Set_Asset_Tag_List($filter_array = array()) {
        
        if (!isset($filter_array['asset_id'])) {
            $filter_array['asset_id'] = 'none' ; 
            }
        
        if (!isset($filter_array['order_by'])) {        
            $filter_array['order_by'] = "metadata.metadata_name" ; 
            $filter_array['order'] = "ASC" ;
            }
        
        $tag_list = $this->Retrieve_Asset_Metadata_List('tag',$filter_array) ;         
        
        if ($tag_list['result_count'] == 0) {
            $this->tag_list = 'error' ; 
            } else {
                $this->tag_list = $tag_list['results'] ;
                }

        return $this ;         
        }
    
    
    
    // Set the category list array
    public function Set_Asset_Category_List($filter_array = array()) {
        
        if (!isset($filter_array['asset_id'])) {
            $filter_array['asset_id'] = 'none' ; 
            }
        
        if (!isset($filter_array['order_by'])) {        
            $filter_array['order_by'] = "metadata.metadata_name" ; 
            $filter_array['order'] = "ASC" ;
            }
        
        $category_list = $this->Retrieve_Asset_Metadata_List('category',$filter_array) ;         

        if ($category_list['result_count'] == 0) {
            $this->category_list = 'error' ; 
            } else {
                $this->category_list = $category_list['results'] ;
                }

        return $this ;         
        }


    
    public function Set_Comment_By_ID($comment_id = 'internal') {
        
        if ('internal' === $comment_id) {
            $comment_id = $this->comment_id ; 
            } else {
                $this->comment_id = $comment_id ;      
                }
        
        $query_options['comment_id'] = $comment_id ; 
        
        $this->Set_Comment($query_options) ; 
        
        return $this ; 
        }
    
    
    public function Set_Comment($query_options) {

        
        $this->Set_Comment_List($query_options) ; 
        $result = $this->Get_Comment_List() ; 

        
        if (!$result['error']) {
            
            $comment = $result[0]  ;
            $this->comment = $comment ; 
            
            }  

        return $this ; 
        } 
    
    
    // Set a list of comments based on an asset an an array of options
    public function Set_Comment_List($query_options = array()) {  
        
        if ('internal' === $asset_id) {
            
            } else {
                $this->asset_id = $asset_id ;      
                } 
        
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }                   
                        
        if (!isset($query_options['order_by'])) {
            $query_options['order_by'] = "asset_comments.timestamp_comment_created" ; 
            $query_options['order'] = "DESC" ; 
            }
        
        
        if (isset($query_options['filter_by_comment_status'])) {
            
            switch ($query_options['filter_by_comment_status']) {
                case 'all':
                    $query_options['filter_by_comment_status'] = 'comment-spam,comment-deleted' ; 
                    // Intentional fall-through
                case 'visible':
                    $query_options['filter_by_comment_status'] = 'comment-approved,comment-unmoderated' ; 
                    break ;                     
                } 
            } else {
                $query_options['filter_by_comment_status'] = 'comment-approved,comment-unmoderated' ; 
                }         
       
    
        $status_metadata_list_options['filter_by_metadata_slug'] = $query_options['filter_by_comment_status'] ;
        $status_metadata_list_options['allow_global_metadata'] = 'only' ;
        $status_metadata_list_options['filter_by_metadata_type'] = 'comment_status' ; 
        $comment_status_metadata = $this->Set_Metadata_List($status_metadata_list_options)->Get_Metadata_List() ; 


        
        $comment_status_filter = array() ; 
        foreach ($comment_status_metadata as $comment_status) {
            $comment_status_filter[] = $comment_status['metadata_id'] ; 
            } 
       
        $query_options['filter_by_comment_status'] = $comment_status_filter ;   
    
        if (isset($query_options['comment_id'])) {
            unset($query_options['filter_by_comment_status']) ; 
            }
        
        
        
        $result = $this->Retrieve_Comment_List($query_options) ;         

        
        
        // Initializee comment count array
        $comment_count_result = array(
            'all' => 0,
            'visible' => 0, // approved and unmoderated only
            'approved' => 0,
            'unmoderated' => 0,                    
            'spam' => 0,
            'deleted' => 0
            ) ;

        if ($result['result_count'] == 0) {
            $this->comment_list = 'error' ; 
            $this->comment_count = $comment_count_result ;
            } else {
            
                $asset_final = array() ;
                foreach ($result['results'] as $comment) {
                    $asset_final[] = $this->Action_Time_Territorialize_Dataset($comment) ; 
                    } 
            
                $result['results'] = $asset_final ; 
             
                
                    
                $i = 0 ; 
                foreach ($result['results'] as $comment) {

                    switch ($comment['metadata_slug']) {
                        case 'comment-approved':
                            $comment_count_result['all']++ ; 
                            $comment_count_result['visible']++ ; 
                            $comment_count_result['approved']++ ; 
                            break ; 
                        case 'comment-unmoderated':
                            $comment_count_result['all']++ ;  
                            $comment_count_result['visible']++ ; 
                            $comment_count_result['unmoderated']++ ; 
                            break ; 
                        case 'comment-spam':
                            $comment_count_result['all']++ ; 
                            $comment_count_result['spam']++ ; 
                            break ; 
                        case 'comment-deleted':
                            $comment_count_result['all']++ ; 
                            $comment_count_result['deleted']++ ; 
                            break ;                            
                        }
                    
                    $i++ ; 
                    }

                $this->comment_list = $result['results'] ;
                $this->comment_count = $comment_count_result ;
                }

        return $this ; 
        }
    
    
    // Get the count of asset members on a list and add it to the asset record in $this->asset
    // Incoming $query_options should be the entire asset 
    public function Set_Asset_Members_Count($query_options = array()) { 
        
        $asset_id = $query_options['asset_id'] ; 
        $type_id = $query_options['type_id'] ; 
        
        
        $query_array = array(
            'table' => 'assets',
            'join_tables' => array(),
            'fields' => "assets.asset_id",
            'where' => "assets.asset_id='$asset_id'"
            );        
        
        
        
        // Add in asset member (subscriber count) tables...
        $asset_member_table = 'asset_members' ; 
        
        $query_array['fields'] .= ", 
            COUNT(asset_members.member_id_value) as asset_member_count, 
            
            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-subscribed' then 1 else 0 end) distribution_sublist_email_subscribed, 
            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-unsubscribed' then 1 else 0 end) distribution_sublist_email_unsubscribed, 
            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-pending' then 1 else 0 end) distribution_sublist_email_pending, 
            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_email_cleaned, 
            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_email_restricted,
            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-inactive' then 1 else 0 end) distribution_sublist_email_inactive, 
            
            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-subscribed' then 1 else 0 end) distribution_sublist_sms_subscribed, 
            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-unsubscribed' then 1 else 0 end) distribution_sublist_sms_unsubscribed, 
            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-pending' then 1 else 0 end) distribution_sublist_sms_pending, 
            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_sms_cleaned, 
            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_sms_restricted,
            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-inactive' then 1 else 0 end) distribution_sublist_sms_inactive 
            " ; 

        
//            // Prep the member visibility string... 
//            $member_visibility_string = '' ; 
//            $member_visibility_query_options['filter_by_visibility_name'] = 'visible' ;
//            $member_visibility_query_options = (object) $member_visibility_query_options ; 
//            $member_visibility_query_result = $this->Retrieve_Visibility_ID_Array($member_visibility_query_options) ;
//            foreach ($member_visibility_query_result as $member_visibility) { 
//                $this_visibility_id = $member_visibility['visibility_id'] ; 
//                $member_visibility_string .= "system_visibility_levels.visibility_id='$this_visibility_id' OR " ; 
//                }
//            $member_visibility_string = rtrim($member_visibility_string," OR ") ; 
        
    
        
        
        // Contact Count Query
        $query_array['join_tables'][] = array(
            'table' => "(SELECT 
                
                asset_members.asset_id, asset_members.member_id_value, metadata.metadata_name,  
                distribution_sublist_email_metadata_name, distribution_sublist_email_metadata_slug, distribution_sublist_email_asset_title, distribution_sublist_email_type_name, 
                distribution_sublist_sms_metadata_name, distribution_sublist_sms_metadata_slug, distribution_sublist_sms_asset_title, distribution_sublist_sms_type_name
                
                FROM asset_members 
                JOIN assets ON asset_members.asset_id = assets.asset_id 
                JOIN metadata ON metadata.metadata_id = asset_members.metadata_id 
                
                JOIN (

                SELECT 
                asset_members.asset_id AS distribution_sublist_email_asset_id, 
                assets.asset_title as distribution_sublist_email_asset_title, 
                asset_members.parent_member_id AS distribution_sublist_email_parent_member_id,
                asset_members.member_id_value AS distribution_sublist_email_member_id, 
                metadata.metadata_name AS distribution_sublist_email_metadata_name,
                metadata.metadata_slug AS distribution_sublist_email_metadata_slug,
                asset_type.type_name AS distribution_sublist_email_type_name 
                FROM asset_members 
                JOIN assets ON asset_members.asset_id = assets.asset_id 
                JOIN asset_type ON assets.type_id = asset_type.type_id 
                JOIN metadata ON metadata.metadata_id = asset_members.metadata_id 
                WHERE asset_type.type_name='distribution_sublist_email' 

                ) AS distribution_sublist_email ON distribution_sublist_email.distribution_sublist_email_parent_member_id=asset_members.member_id                
                                
                JOIN (

                SELECT 
                asset_members.asset_id AS distribution_sublist_sms_asset_id, 
                assets.asset_title as distribution_sublist_sms_asset_title, 
                asset_members.parent_member_id AS distribution_sublist_sms_parent_member_id,
                asset_members.member_id_value AS distribution_sublist_sms_member_id, 
                metadata.metadata_name AS distribution_sublist_sms_metadata_name,
                metadata.metadata_slug AS distribution_sublist_sms_metadata_slug,
                asset_type.type_name AS distribution_sublist_sms_type_name 
                FROM asset_members 
                JOIN assets ON asset_members.asset_id = assets.asset_id 
                JOIN asset_type ON assets.type_id = asset_type.type_id 
                JOIN metadata ON metadata.metadata_id = asset_members.metadata_id 
                WHERE asset_type.type_name='distribution_sublist_sms' 

                ) AS distribution_sublist_sms ON distribution_sublist_sms.distribution_sublist_sms_parent_member_id=asset_members.member_id  
                
                ) AS $asset_member_table",
            'statement' => "(assets.asset_id=$asset_member_table.asset_id)",
            'type' => 'left'
            ) ; 
        

        
        $result = $this->DB->Query('SELECT_JOIN',$query_array);  

        // Add the member count results to the existing $this->asset entry
        foreach ($result['results'] as $key => $value) {
            $this->asset[$key] = $value ; 
            }
        
        return $this ;
        }
    
    
    
    // Set a list of events assigned to a user / account
    public function Set_Asset_Members_List($query_options = array()) {  
        
        $this->Set_Model_Timings('Asset.Set_Asset_Members_List_initialize') ;
        
        $continue = 1 ; 
                
        if (!isset($query_options['override_paging'])) { 
            $query_options['override_paging'] = 'no' ;
            }
        if (!isset($query_options['filter_by_user_id'])) { 
            $query_options['filter_by_user_id'] = 'yes' ; // 'yes' requires the matching member to also be assigned to the internally set user
            }
        if (!isset($query_options['filter_by_account_id'])) { 
            $query_options['filter_by_account_id'] = 'yes' ; // 'yes' requires the matching member to also be assigned to the internally set account
            }
        
        if (!isset($query_options['filter_by_type_id'])) {
            $query_options['filter_by_type_id'] = 'yes' ; // 
            }
        
        if (!isset($query_options['type_id'])) { 
            if (isset($this->asset['type_id'])) {
                $query_options['type_id'] = $this->asset['type_id'] ;
                } else {
                    $query_options['filter_by_type_id'] = 'no' ; 
                    }
            }
        
        if (!isset($query_options['map_parent_asset'])) {
            $query_options['map_parent_asset'] = 'yes' ; 
            } 
        
        if (!isset($query_options['filter_by_asset_id'])) {
            $query_options['filter_by_asset_id'] = 'yes' ; 
            }
        if (($query_options['filter_by_asset_id'] == 'yes') AND (!$this->asset_id)) { 
            $continue = 0 ;  
            }
        
        if (!isset($query_options['order_by'])) {
            $query_options['order_by'] = 'asset_member_temp_table.last_name' ; 
            $query_options['order'] = 'ASC' ; 
            } 
        
                            
        // Reset old metadata search terms
        if (isset($query_options['filter_by_distribution_sublist_email_metadata_id'])) {
            $query_options['filter_by_email_metadata_id'] = $query_options['filter_by_distribution_sublist_email_metadata_id'] ; 
            }
        if (isset($query_options['distribution_sublist_email_metadata_id'])) {
            $query_options['email_metadata_id'] = $query_options['distribution_sublist_email_metadata_id'] ;
            }                            
        if (isset($query_options['filter_by_distribution_sublist_sms_metadata_id'])) {
            $query_options['filter_by_sms_metadata_id'] = $query_options['filter_by_distribution_sublist_sms_metadata_id'] ;
            }
        if (isset($query_options['distribution_sublist_sms_metadata_id'])) {
            $query_options['sms_metadata_id'] = $query_options['distribution_sublist_sms_metadata_id'] ;
            }                            
        unset($query_options['filter_by_distribution_sublist_email_metadata_id'],$query_options['distribution_sublist_email_metadata_id']) ; 
        unset($query_options['filter_by_distribution_sublist_sms_metadata_id'],$query_options['distribution_sublist_sms_metadata_id']) ; 
                            
                            
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }

        if ($query_options['minify_list'] == 'yes') { 
            $query_options['skip_compile_member'] = 'yes' ; 
            }
        
        if ($continue == 1) {
            
            
// THIS CODE WAS CAUSING A PROBLEM EARLIER -- NOT SURE WHAT WAS GOING ON 8/17/19
// Identify the distribution list being called for parent assets (e.g. a Sequence attempting to get the underlying Contact list)
//            $data['distro_list'] = $this->asset[$query_options['distribution_list_name']] ; 

            
            
            switch ($query_options['type_id']) {    
                case 17: // Series
                    
                    $asset_members = new Asset() ;
                    $asset_members->Set_Replicate_User_Account_Profile($this) ;
                    $asset_members->Set_Asset_By_ID($this->asset[$query_options['distribution_list_name']]['asset_id']) ;
                    
                    switch ($query_options['distribution_list_name']) {
                        case 'distribution_list_contacts':
                            
                            $query_options['type_id'] = 19 ; // Contact distro list
                            $result = $asset_members->Retrieve_Asset_Members($query_options) ;
                            break ;
                        case 'distribution_list_users':
                            
                            $query_options['type_id'] = 20 ; // User distro list
                            $result = $asset_members->Retrieve_Asset_Members($query_options) ;
                            break ;  
                        case 'distribution_list_profiles':
                            
                            $query_options['type_id'] = 23 ; // Account Profile distro list
                            $result = $asset_members->Retrieve_Asset_Members($query_options) ;
                            break ;                            
                        }

//                    $additional_parameters['skip_asset_mapping'] = $query_options['skip_asset_mapping'] ;
//                    $additional_parameters['map_parent_asset'] = $query_options['map_parent_asset'] ;
//                    $result = $asset_members->Action_Map_Asset($result,$additional_parameters) ;  

                    
                    break ; 
                case 19: // Contact Distribution List
                case 26: // Contact Series List    

                    
                    
                    if (!isset($query_options['search_contact_notes'])) {  
                        $query_options['search_contact_notes'] = 'no' ; // 'no' ignores contact_notes in search results; 'yes' will search notes
                        }      

                    $result = $this->Retrieve_Asset_Members($query_options) ;
                    $save_result = $result ;
                    
                    $this->Set_Model_Timings('Asset.Set_Asset_Members_List_results_pulled') ;
                    
                    if ((count($result['result_count']) > 0) AND ($query_options['skip_compile_member'] != 'yes')) {  
                        
                        $contact = new Contact() ;  
                        $contact->Set_Replicate_User_Account_Profile($this) ; 
                        
                        $i = 0 ; 
                        foreach ($result['results'] as $member) {
                            $tag_metadata_query_options['metadata_type_id'] = 1 ; // Tags
                            $tag_list = $contact->Retrieve_Contact_Metadata_List($member['member_id_value'],$tag_metadata_query_options) ; 
                            $result['results'][$i]['tags'] = $tag_list['results'] ;                            
                            $i++ ; 
                            }
                        }


                    $this->Set_Model_Timings('processed_for_tags') ;
                    
                    
//                    $additional_parameters['skip_asset_mapping'] = $query_options['skip_asset_mapping'] ;
//                    $additional_parameters['map_parent_asset'] = $query_options['map_parent_asset'] ;
//                    $result = $this->Action_Map_Asset($result,$additional_parameters) ;
                    
                    break ; 
                case 20: // User Distribution List
                case 28: // User Series List
                    
                    $result = $this->Retrieve_Asset_Members($query_options) ;
                    $save_result = $result ;                
                    
                    $this->Set_Model_Timings('Asset.Set_Asset_Members_List_results_pulled') ;

                    if ((count($result['result_count']) > 0) AND ($query_options['skip_compile_member'] != 'yes')) { 
                        
                        $i = 0 ; 
                        foreach ($result['results'] as $member) {
                            
                            $tag_metadata_query_options['metadata_type_id'] = 1 ; // Tags
                            $tag_metadata_query_options['relationship_type'] = 'user' ; 
                            $tag_list = $this->Retrieve_Metadata_Relationship_List($member['user_id'],$tag_metadata_query_options) ; 
                            $result['results'][$i]['tags'] = $tag_list['results'] ; 
                            $i++ ; 
                            }
                        }


                    
                    $this->Set_Model_Timings('processed_for_tags') ;
                    
                    break ;
                default:
   
                    $result = $this->Retrieve_Asset_Members($query_options) ;
                    $save_result = $result ;
                    
                    $this->Set_Model_Timings('Asset.Set_Asset_Members_List_results_pulled') ;
                }
            

            
            $i = 0 ; 
            $full_result_set_compiled = '' ; 
            $uber_result_set_compiled = '' ; 
            
            if ($result['result_count'] > 0) {

                
                foreach ($result['results'] as $member) {

                    if ($query_options['skip_compile_member'] != 'yes') {
                        $contact = new Contact() ; 
                        $contact->Set_Replicate_User_Account_Profile($this) ;
                        $member = $contact->Action_Compile_Contact($member) ; // This just territorializes timestamps and phone. No queries.
                        }
                    
                    // Process for do not contact restrictions
                    if ($member['do_not_contact_email_address'] == 1) {
                        $member['metadata_id_distribution_sublist_email'] = 52 ; // Restricted
                        $member['metadata_name_distribution_sublist_email'] = 'Restricted' ; // Restricted
                        $member['metadata_slug_distribution_sublist_email'] = 'list-restricted' ; // Restricted
                        }
                    
                    if ($member['do_not_contact_phone_number'] == 1) {
                        $member['metadata_id_distribution_sublist_sms'] = 52 ; // Restricted
                        $member['metadata_name_distribution_sublist_sms'] = 'Restricted' ; // Restricted
                        $member['metadata_slug_distribution_sublist_sms'] = 'list-restricted' ; // Restricted
                        }

                    // Used to validate actions based on member id such as unsubscribe from list
                    $member_token_array = array($member['member_id'],$member['timestamp_member_created']) ; 
                    $member['member_token'] = $this->DB->Simple_Hash($member_token_array) ;

                    $result['results'][$i] = $member ; 
                    $full_result_set_compiled .= $member['member_id'].',' ; 

//                    error_log('Asset.Set_Asset_Members_List() After adding time '.$i)  ;
                
                    $i++ ; 
                    }

                $this->Set_Model_Timings('Asset.Set_Asset_Members_List_territorialized_data') ;
//                error_log('Asset.Set_Asset_Members_List() compiled through the contact model '.$full_result_set_compiled)  ;
                

                $i = 0 ; 
                foreach ($result['uber_result_set'] as $member) {
                    $uber_result_set_compiled .= $member['member_id'].',' ; 
                    }

                $pulled_result_set_compiled = $result['results'] ; 
                $full_result_set_compiled = rtrim($full_result_set_compiled,",") ;
                $uber_result_set_compiled = rtrim($uber_result_set_compiled,",") ;             
            
                $this->Set_Asset_Paging($result) ; 
                unset($result) ; 

                } else {
                
                    $pulled_result_set_compiled = array() ; 
                    $full_result_set_compiled = array() ; 
                    $uber_result_set_compiled = array() ;             
                    }
            
            $this->asset_members_list = $pulled_result_set_compiled ;
            $this->asset_members_full_list = $full_result_set_compiled ; // IDs only
            $this->asset_members_uber_list = $uber_result_set_compiled ; // IDs only   
            }

        unset($pulled_result_set_compiled) ;
        
        $this->asset_query_result2 = $query_options ; 
        $this->Set_Model_Timings('Asset.Set_Asset_Members_List_complete') ;
        
        return ;        
        }
    
    

    // Set the vendor resource for manipulating assets
    public function Set_System_Vendor($vendor_id) {
        
        $this->vendor_id = $vendor_id ;
        
        $vendor = new Vendor() ; 
        $vendor->Set_Vendor_By_ID($this->vendor_id)->Set_System_Vendor()->Set_System_Vendor_Keys() ;
        
        $vendor_record = $vendor->Get_Vendor() ; 
        $vendor_keys = $vendor->Get_Vendor_Keys() ; 

        switch ($this->vendor_id) {               
            case 2: // Mailgun 
            case 3: // Twilio                 
            case 5: // Cloudinary     
            case 9: // Bandwidth         
                $this->vendor_keys = $vendor_keys ; 
                $this->vendor = $vendor->Get_Vendor() ; 
                $this->vendor_keys = $vendor->Get_Vendor_Keys() ; 
                break ;
            default:
                $this->vendor_id = 'error' ; 
                $this->vendor = 'error' ; 
                $this->vendor_keys = 'error' ; 
            }
        
        return $this ;         
        }    
    

    
    public function Set_Scheduled_Automation_Timestamp($seconds_to_increment = 'now') {
        
        if ('now' === $seconds_to_increment) {
            $this->scheduled_automation_timestamp = time() ; 
            } else {
            
                $existing_timestamp = $this->Get_Scheduled_Automation_Timestamp() ; 
                $this->scheduled_automation_timestamp = $existing_timestamp + $seconds_to_increment ; 
                }
        
        return $this ; 
        }
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_Asset_Member() {
        
        return $this->asset_member ;
        
        }
    
    
    public function Get_Asset() {
        
        return $this->asset ;
        } 

    public function Get_Child_Asset_ID() {
        
        return $this->child_asset_id ;
        }
    
    public function Get_Child_Asset() {
        
        return $this->child_asset ;
        }
    
    public function Get_Asset_Structure_Item($structure_order = 'internal') {
        
        if ('internal' === $structure_order) {
            
            $data['structure_record'] = $this->structure_item ; 
                
            } else {
            
                $data['asset_record'] = $this->Get_Asset() ; 
                $data['structure_record'] = Utilities::Deep_Array($data['asset_record']['structure'], 'structure_order', $structure_order) ;
                }
        
        return $data['structure_record'] ;
        }
    
    public function Get_Asset_Paging() {
        
        $this->Set_Model_Timings('Asset.Get_Asset_Paging_requested') ;
        return $this->asset_paging ;        
        }
    
    public function Get_Page_Increment() {
        
        return $this->page_increment ; 
        } 
    
    public function Get_Asset_List() {
        
        return $this->asset_list ;        
        } 
    
   
    public function Get_Asset_Full_List() {
        
        return $this->asset_full_list ;
        }
    
    public function Get_Asset_Uber_List() {
        
        return $this->asset_uber_list ;
        }
    
    
    public function Get_Asset_Compiled() {
        
        return $this->asset_compiled ;
        
        }    
        
    public function Get_Asset_Members_List() {
        
        $this->Set_Model_Timings('Asset.Get_Asset_Members_List_requested') ;
        return $this->asset_members_list ;
        }
    
    public function Get_Asset_Members_Full_List() {
        
        return $this->asset_members_full_list ;
        }
    
    public function Get_Asset_Members_Uber_List() {
        
        return $this->asset_members_uber_list ;
        }
    
    public function Get_Asset_Members_Processing_List() {
        
        return $this->asset_members_processing_list ;
        }
    
    public function Get_Asset_Type() {
        
        return $this->asset_type ; 
        }
    
    public function Get_Comment_Count() {
        
        return $this->comment_count ;
        
        }
    
    public function Get_Comment_List() {
        
        return $this->comment_list ;
        
        }
    
    public function Get_Comment() {
        
        return $this->comment ;
        
        }
    
    public function Get_Search_ID() {
        
        return $this->search_id ;
        
        }
    
    public function Get_Search() { 
        
        return $this->search ;
        
        }    
    
    
    
    public function Get_Campaign_ID() { 
        
        return $this->campaign_id ; 
        }
    
    public function Get_Campaign() {
        
        return $this->campaign ; 
        }
    
    public function Get_Campaign_List() {
        
        return $this->campaign_list ; 
        }
    
    public function Get_Campaign_Report() {
        
        return $this->campaign_report ; 
        } 
    
    
    public function Get_Automation_ID() {
        
        return $this->automation_id ; 
        }
    
    public function Get_Automation() {
        
        return $this->automation ; 
        }
    
    
    public function Get_Automation_List() {

        return $this->automation_list ; 
        }
    
    
    public function Get_Campaign_Recipients_List() {
        
        return $this->campaign_recipients_list ; 
        }
    
    public function Get_Campaign_Full_Recipients_List() {
        
        return $this->campaign_full_recipients_list ; 
        }
    
    public function Get_Asset_Query() {
        
        return $this->asset_query_result ;
                
        }    
    
    // THIS SHOULDN"T BE NEEDED ANYMORE...
    public function Get_Retrieved_Asset() {
        
        return $this->asset ;
                
        }    
    
    
    public function Get_Scheduled_Automation_Timestamp() {
        
        return $this->scheduled_automation_timestamp ; 
        }
    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    
    
    
     
//    public function Action_Validate_Asset_Authorization( 
//        $pass = 1,
//        $master_user = array(),
//        $asset_authorization,
//        $optional_params = array()
//        ) { 
//        
//        // Set defaults...
//        if (!isset($master_user['view_level'])) {
//            $master_user['view_level'] = 7 ;
//            }
//        if (!isset($master_user['edit_level'])) {
//            $master_user['edit_level'] = 7 ;
//            }
//        if (!isset($master_user['owner_level'])) {
//            $master_user['owner_level'] = 7 ;
//            }        
//        
//        $user_test_levels = array(
//            'view' => 1,
//            'edit' => 1, 
//            'owner' => 1
//            ) ; 
//        
//        $test = array(
//            'pass' => $pass,
//            'privilege' => 'none',
//            'fail_reason' => ''
//            ) ; 
//        
//        ///// RUN ACCOUNT LEVEL TESTS /////
//        if ($this->account_id == 0) {
//            $test['pass'] = 0 ; 
//            $test['privilege'] = 'none' ; 
//            $test['fail_reason'] = 'No account_id set' ; 
//            } 
//        
//        
//        ///// IF ACCOUNT LEVEL TESTS PASS, RUN EVENT LEVEL TESTS /////
//        if ($test['pass'] == 1) { 
//
//            $continue = 1 ; 
//            
//            if ($this->account_id != $this->asset['account_id']) {
//                $continue = 0 ;
//                $test['pass'] = 0 ; 
//                $test['privilege'] = 'none' ;
//                $test['fail_reason'] = 'account_id / asset account_id mismatch' ; 
//                }
//     
//                        
//            if ($test['pass'] == 1) {
//                
//                $user_authorization = Utilities::Deep_Array($asset_authorization, 'user_id', $this->user_id) ;
//                
//                if ($user_authorization) {
//
//                    // Then test the user authorization
//                    if ($user_authorization['owner'] == $user_test_levels['owner']) {
//                        $test['pass'] = 1 ; 
//                        $test['privilege'] = 'owner' ; 
//                        }                     
//                    elseif ($user_authorization['edit'] == $user_test_levels['edit']) {
//                        $test['pass'] = 1 ; 
//                        $test['privilege'] = 'edit' ; 
//                        }                    
//                    elseif ($user_authorization['view'] == $user_test_levels['view']) {
//                        $test['pass'] = 1 ; 
//                        $test['privilege'] = 'view' ; 
//                        }  
//                    else {
//                        $test['pass'] = 0 ; 
//                        $test['privilege'] = 'none' ;
//                        $test['fail_reason'] = 'No user privileges passed test.' ; 
//                        }
//                    
//                    } else {
//                    
//                        // FOR ASSET TESTING -- May want to add a visibility test here??
//                        $test['pass'] = 0 ; 
//                        $test['privilege'] = 'none' ; 
//                        $test['fail_reason'] = 'No user privileges passed test.' ; 
//                        }                                
//                
//                
//                // Then test the global user authorization
//                // If the User Asset Auth test fails, then test the global master user to see if rights there override
//                if ($test['pass'] == 0) {
//                    $test = $this->Action_Validate_Master_User_Authorization($test,$master_user) ;  
//                    }                
//                
//                }
//
//            }
//            
//
//        $results = array(
//            'pass' => $test['pass'],
//            'privilege' => $test['privilege'],
//            'master_user_test' => $master_user, // The test levels that need to be passed for master_user to have rights
//            'user_test' => $user_test_levels, // The test levels that need to be passed for user to have rights
//            'master_authorization' => $test['master_authorization'], // Actual rights level of the master user
//            'user_authorization' => $user_authorization, // Actual rights level of the user
//            'fail_reason' => $test['fail_reason']
//            ) ;
//            
//        return $results ; 
//        }
//    
//    
//    
//    public function Action_Test_Asset_Authorization($auth_name,$auth_input = array()) { 
//        
//        $validate_result['pass'] = 1 ; // Default pass
//        $validate_result['privilege'] = 'none' ;         
//        $reroute_url = '/app/events' ; 
//        $rights = 'view' ; 
//        
//        switch ($auth_name) {                
//            case 'asset_edit':
//
//                if ($this->asset['asset_id'] != $auth_input['asset_id']) {
//                    $this->Set_Asset_By_ID($auth_input['asset_id']) ; 
//                    }
//
//                $master_user = array(
//                    'view_level' => 7,
//                    'edit_level' => 7,
//                    'owner_level' => 7
//                    ) ;
//                        
//                $validate_result = $this->Action_Validate_Asset_Authorization(
//                    $validate_result['pass'],
//                    $master_user,
//                    $this->asset['authorization'],
//                    $optional_params) ; 
//                break ;
//            case 'asset_new':
//            case 'event_new': 
//                
//                $this->asset['account_id'] = $this->account_id ; 
//                    
//                $asset_authorization = array(array(
//                        'user_id' => $this->user_id,
//                        'owner' => 1
//                        )
//                    ) ;                     
//                
//                $master_user = array(
//                    'view_level' => 7,
//                    'edit_level' => 7,
//                    'owner_level' => 7
//                    ) ;
//                
//                $validate_result = $this->Action_Validate_Asset_Authorization(
//                    $validate_result['pass'],
//                    $master_user,
//                    $asset_authorization
//                    ) ;                
//                break ;
//            default:
//                $this->Action_Test_Account_Authorization($auth_name) ; 
//                $validate_result = $this->Get_Authorization() ;
//                $reroute_url = $validate_result['reroute_url'] ; 
//                
//            }
//           
//        
//        switch ($validate_result['pass']) {
//            case 1:
//                $this->Set_Alert_Response(34) ; // pass
//                break ; 
//            case 0:
//            default:
//                $this->Set_Alert_Response(35) ; // fail
//            }
//
//        
//        $result = $this->Get_Response() ; 
//        $result['reroute_url'] = $reroute_url ; 
//        $result['privilege'] = $validate_result['privilege'] ; 
//        $result['test'] = $validate_result ; 
//        $result['view'] = $auth_name ; 
//
//        $this->Set_Authorization($result) ; 
//                        
//        return $this ; 
//        } 
    
    
    
    public function Action_Create_Asset_Authorization($auth_input) { 
        
        // Add a switch here to determine if the incoming auth name is "none", in which case we exist any existing matching auth (instead of a block)
        switch ($auth_input['asset_auth_role_name']) {
            case 'asset_none': // Deletes the existing authorization
                $result = $this->Delete_Asset_Authorization($auth_input) ; 
                break ; 
            default: 
                
                if (isset($auth_input['author'])) {
                    switch ($auth_input['author']) {
                        case 'true':
                            $auth_input['author'] = 1; 
                            break ;
                        case 'false':
                            $auth_input['author'] = 0 ; 
                            break ; 
                        }
                    }
                
                $auth_input = $this->Action_Convert_Datetime_To_Timestamp($auth_input) ;
                $result = $this->Create_Asset_Authorization($auth_input) ; 
            }

        if (!$result['error']) { 
            $this->Set_Alert_Response(201) ; // Privileges updated
            } else {
                $this->Set_Alert_Response(202) ; // Error updating privileges
                }        
        
        
        return $this ; 
        }
    
    public function Action_Create_Global_Asset_Restriction($restriction_input) {
        
        $continue = 1 ; 
        
        if (!$this->asset_id) {
            $continue = 0 ; 
            }
        if ((!isset($restriction_input['restriction_id'])) OR (!isset($restriction_input['restriction_type'])) OR (!isset($restriction_input['restriction_active']))) {
            $continue = 0 ; 
            }
            
        
        if ($continue == 1) {
            
            $restriction_input['asset_id'] = $this->asset_id ; 
            
            switch ($restriction_input['restriction_active']) {
                case 'true':    
                    
                    $result = $this->Create_Global_Asset_Restriction($restriction_input) ; 
                    break ; 
                case 'false':
                    
                    // Delete the restrictions
                    $result = $this->Delete_Global_Asset_Restriction($restriction_input) ; 
                    break ; 
                }

            
            if (!$result['error']) {
                $this->Set_Alert_Response(201) ; // Restrictions updated
                } else {
                    $this->Set_Alert_Response(202) ; // Error updating restrictions
                    }              
            }
        
              
        
        
        return $this ; 
        }
    
    
    public function Action_Create_Global_Asset_Relationship($asset_input) {
        
     
        if (!isset($asset_input['account_asset_visibility_id'])) {            
            if (isset($asset_input['account_asset_visibility_name'])) {                
                $visibility_query_options['filter_by_visibility_name'] = $asset_input['account_asset_visibility_name'] ;
                $visibility_query_options = (object) $visibility_query_options ; 
                $visibility_query_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options) ;
                $asset_input['account_asset_visibility_id'] = $visibility_query_result[0]['visibility_id'] ; 
                } 
            }
        
        
        $result = $this->Create_Global_Asset_Relationship($asset_input) ; 

        
        if (!$result['error']) {
            $this->Set_Alert_Response(201) ; // Privileges updated
            } else {
                $this->Set_Alert_Response(202) ; // Error updating privileges
                }        
        
        
        return $this ; 
        }
    
    
    
    
    public function Action_Validate_Asset_Link($asset_link_array = array()) {
        
        
        $continue = 1 ; 
        $asset_link_array['effect'] = 'deny' ; 
        
        if (isset($asset_link_array['full_link'])) {
            
            $asset_link_parts = explode("_",$asset_link_array['full_link']) ; 
            $asset_link_array['public_link_id'] = $asset_link_parts[0] ; 
            $asset_link_array['secret_link_id'] = $asset_link_parts[1] ;            
            }
        
        if (isset($asset_link_array['public_link_id']) AND isset($asset_link_array['secret_link_id'])) {
            
            $public_link = $this->Set_Asset_Public_Link()->Get_Asset_Public_Link() ; 
            
            
            $asset_link_array['queried_link'] = $public_link ; 
            $asset_link_array['asset'] = $this->asset ; 
            
            if (($public_link['public_link_id'] == $asset_link_array['public_link_id']) AND ($public_link['secret_link_id'] == $asset_link_array['secret_link_id'])) {
                $asset_link_array['effect'] = 'allow' ; 
                }             
            }
        
        
        return $asset_link_array ; 
        }
    
    
    
    public function Set_Asset_Public_Link($asset = 'internal') {
        
        global $server_config ; // To create the public_link_url domain name
        
        $continue = 1 ; 
        $link_array = array() ; 
        
        
        if ('internal' === $asset) {
            if (!isset($this->asset['asset_id'])) { 
                $continue = 0 ; 
                } else {
                    $asset_record = $this->Get_Asset() ; 
                    }
            } else {
                $asset_record = $asset ; 
                } 
        
 
        
        if ($continue == 1) {


            $link_array['asset_id'] = $asset_record['asset_id'] ; 
            $link_array['public_link_id'] = \DAL::Simple_Hash(array($asset_record['asset_id'],$asset_record['timestamp_asset_created'])).$asset_record['timestamp_asset_created'] ;
            $link_array['secret_link_id'] = \DAL::Simple_Hash(array($asset_record['asset_id'],$asset_record['timestamp_asset_created'],$link_array['public_link_id'])) ;
            $link_array['public_link'] = $link_array['public_link_id'].'_'.$link_array['secret_link_id'] ; 
            $link_array['public_link_url'] = 'https://'.$server_config['domain']['app_domain']['hostname'].'/view?asset='.$link_array['public_link_id'].'_'.$link_array['secret_link_id'] ; 
            
            if ('internal' === $asset) {
                $this->asset['public_link_id'] = $link_array['public_link_id'] ; 
                $this->asset['public_link'] = $link_array['public_link'] ; 
                $this->asset['public_link_url'] = $link_array['public_link_url'] ; 
                }            
                        
            }
        
        $this->asset_public_link = $link_array ; 
        return $this ; 
        }
    
    
    public function Get_Asset_Public_Link() {
        
        return $this->asset_public_link ; 
        }    
    
    
    // Trigger automation execution for a specific automation ID
    public function Action_Trigger_Automation_Queue($automation_id) { 
        
        if ($automation_id > 0) {
            
            $additional_params['automation_id'] = $automation_id ; 
            $additional_params['avs'] = APP_VERSION['version_name'] ;
            $data['trigger_result'] = System::Cron_Execute('automation',5,$additional_params) ;
            }
        
        return $data['trigger_result'] ;     
        }
    
    
    public function Action_Create_Campaign($options = array()) {  
        
        $this->Set_Model_Timings('Asset.Action_Create_Campaign_begin') ; 
        
        
        $continue = 1 ; 
        
        if (!isset($options['asset_id'])) {  
            $continue = 0 ; 
            } else {
                $campaign_options['asset_id'] = $options['asset_id'] ; 
                $asset_options = array(
                    'condense_asset' => 'yes',
                    'skip_asset_mapping' => 'yes'
                    ) ;
                $data['asset_record'] = $this->Set_Asset_By_ID($campaign_options['asset_id'],$asset_options)->Get_Asset() ; 
                }
        
        $options = $this->Action_Convert_Datetime_To_Timestamp($options) ;

        
        if (!isset($options['user_id'])) {
            $campaign_options['user_id'] = $this->user_id ; 
            } else {
                $campaign_options['user_id'] = $options['user_id'] ; 
                }
        
        if (!isset($options['account_id'])) {
            $campaign_options['account_id'] = $this->account_id ; 
            } else {
                $campaign_options['account_id'] = $options['account_id'] ; 
                }
            
        if (isset($options['delivery_asset_id'])) { 
            $campaign_options['delivery_asset_id'] = $options['delivery_asset_id'] ; 
            } 
        
        if (isset($options['delivery_trigger'])) {
            $campaign_options['delivery_trigger'] = $options['delivery_trigger'] ; 
            }         
      
        if (isset($options['campaign_title'])) { 
            $campaign_options['campaign_title'] = $options['campaign_title'] ; 
            }
        
        if (isset($options['ancestor_campaign_id'])) {
            $campaign_options['ancestor_campaign_id'] = $options['ancestor_campaign_id'] ; 
            }
                
        if (isset($options['timestamp_campaign_scheduled'])) {
            $campaign_options['timestamp_campaign_scheduled'] = $options['timestamp_campaign_scheduled'] ; 
            }
        
        if (isset($options['campaign_status_id'])) {
            $campaign_status_options['filter_by_asset_status_id'] = $options['campaign_status_id'] ; 
            } elseif (isset($options['campaign_status_name'])) {
                $campaign_status_options['filter_by_asset_status_name'] = $options['campaign_status_name'] ; 
            } else {
                $campaign_status_options['filter_by_asset_status_name'] = 'scheduled' ; 
                }

        $campaign_status = $this->Retrieve_Asset_Status_ID_Array($campaign_status_options,'single') ; 
        $campaign_options['campaign_status_id'] = $campaign_status['asset_status_id'] ; 

        
        if (isset($options['automation_status_id'])) {
            $automation_status_options['filter_by_asset_status_id'] = $options['automation_status_id'] ; 
            } elseif (isset($options['automation_status_name'])) {
                $automation_status_options['filter_by_asset_status_name'] = $options['automation_status_name'] ; 
            } else {
                $automation_status_options['filter_by_asset_status_name'] = 'draft' ; 
                }
        
        
        $automation_status = $this->Retrieve_Asset_Status_ID_Array($automation_status_options,'single') ; 
        $automation_options['automation_status_id'] = $automation_status['asset_status_id'] ;
        
        // If we have scheduled the initial automation item, then also schedule the campaign
        if ($automation_status['status_name'] == 'scheduled') {
            $campaign_options['timestamp_campaign_scheduled'] = TIMESTAMP ; 
            }    
        
        $campaign_options['structured_data_01'] = array() ; 
            
        if (isset($options['campaign_options'])) {
            $campaign_options['structured_data_01']['campaign_options'] = $options['campaign_options'] ; 
            }
        
        
        // User Campaign Recipients template...
        switch ($data['asset_record']['type_name']) { 
            case 'email':
                $campaign_options['structured_data_01']['recipients'] = $this->campaign_recipients_template_email ;         
                break ; 
            case 'sms':
                $campaign_options['structured_data_01']['recipients'] = $this->campaign_recipients_template_sms ;         
                break ;
            case 'series':
                $campaign_options['structured_data_01']['recipients'] = $this->campaign_recipients_template_series ;         
                break ;                
            }
        
        
        if (isset($options['recipients'])) {
            $campaign_options['structured_data_01']['recipients'] = $options['recipients'] ; 
            }        
        if (isset($options['action_keys'])) {
            $campaign_options['structured_data_01']['action_keys'] = $options['action_keys'] ; 
            }
        
        if (isset($options['personalization_overrides'])) {
            $campaign_options['structured_data_01']['personalization_overrides'] = $options['personalization_overrides'] ; 
            }        
        
        // Data overrides are above the recipient level... I think
        if (isset($options['data_overrides'])) { 
            $campaign_options['structured_data_01']['data_overrides'] = $options['data_overrides'] ; 
            }
        
        if (isset($options['messaging_number_id'])) {
            $campaign_options['structured_data_01']['messaging_number_id'] = $options['messaging_number_id'] ; 
            }        
        if (isset($options['auto_delete'])) {
            $campaign_options['structured_data_01']['campaign_options']['auto_delete'] = $options['auto_delete'] ; 
            } 
        
        if (!isset($options['timestamp_next_action'])) {
            $automation_options['timestamp_next_action'] = 0 ; 
            } else {
                $automation_options['timestamp_next_action'] = $options['timestamp_next_action'] ; 
                }
        
        
        if ($continue == 1) {
        
            $this->Set_Model_Timings('Asset.Action_Create_Campaign_pre_campaign_create') ; 
        
            $create_campaign = $this->Create_Campaign($campaign_options) ; 
            $this->Set_Model_Timings('Asset.Action_Create_Campaign_campaign_create_complete_'.$create_campaign['insert_id']) ; 
        
            
            if (!$create_campaign['error']) { 
                $automation_options['campaign_id'] = $create_campaign['insert_id'] ; 
                $automation_options['campaign_position'] = '1.00' ; 
                
                $create_automation = $this->Create_Automation($automation_options) ; 
                $this->Set_Model_Timings('Asset.Action_Create_Campaign_create_automation_for_campaign_'.$create_campaign['insert_id']) ; 
                
                if (!$create_automation['error']) { 
                    
                    $this->Set_Model_Timings('Asset.Action_Create_Campaign_trigger_set_campaign_'.$create_campaign['insert_id']) ; 
                    
                    $campaign_options = array(
                        'condense_campaign' => 'yes'
                        ) ; 
                    $this->Set_Campaign_By_ID($create_campaign['insert_id'],$campaign_options) ;
                    $this->Set_Model_Timings('Asset.Action_Create_Campaign_campaign_set_complete_'.$create_campaign['insert_id']) ; 
                    
//                    $data['campaign_query_result'] = $this->asset_query_result ; 
                    $this->Set_Automation_By_ID($create_automation['insert_id']) ;
                    $this->Set_Alert_Response(154) ; // Campaign created successfully
                    $this->Set_Model_Timings('Asset.Action_Create_Campaign_set_automation_id_'.$create_automation['insert_id']) ; 
                
                    }
                }
            
            } else {
                $this->Set_Model_Timings('Asset.Action_Create_Campaign_campaign_create_skipped') ; 
                }
        
        $data['create_campaign'] = $create_campaign ; 
        $data['create_automation'] = $create_automation ; 
        $data['continue'] = $options['asset_id'] ; 
        
//        $this->test_data = $data ; 
        return $data ;         
        }    
    
    
    
    // Can probably use this as a template for Copy campaign at some point
    public function Action_Create_Test_Delivery($test_delivery_input = array()) {
    
        if (isset($test_delivery_input['automation_id'])) { 
            
            $this->Set_Automation_By_ID($test_delivery_input['automation_id']) ; 
            $automation_record = $this->Get_Automation() ; 
            
            $this->Set_Campaign_By_ID($automation_record['campaign_id']) ; 
            $campaign_record = $this->Get_Campaign() ; 
                        
            // Create campaign
            $create_campaign_options = array(
                'asset_id' => $campaign_record['asset_id'],
                'delivery_asset_id' => $campaign_record['delivery_asset_id'],
                'delivery_trigger' => 'delivery_list_custom',
                'campaign_title' => 'TEST - '.$campaign_record['campaign_title'],
                'timestamp_next_action' => TIMESTAMP,
                'timestamp_campaign_scheduled' => TIMESTAMP,
                'campaign_status_name' => 'deleted',
                'automation_status_name' => 'scheduled',
                'data_overrides' => array(),
                'campaign_options' => array(
                    'cost' => 'free'
                    )
                ) ; 
            
            $asset = new Asset() ; // The asset to be sent
            $asset->Set_Asset_By_ID($campaign_record['asset_id']) ; 
            $asset_record = $asset->Get_Asset() ; 
            
            switch ($asset_record['type_name']) {
                case 'email':
                    $create_campaign_options['data_overrides']['subject_line_01'] = 'TEST - '.$asset_record['subject_line_01'] ; 
                    break ;
                case 'sms':
                    
            
//            messaging_number_id
                    
                    break ; 
                }
        

            $this->Action_Create_Campaign($create_campaign_options) ; 

            $new_campaign_record = $this->Get_Campaign() ; 
            $new_automation_record = $this->Get_Automation() ; 
                
            $update_campaign_input = array(
                'campaign_id' => $new_campaign_record['campaign_id'],
                'timestamp_campaign_scheduled' => TIMESTAMP,
                'structured_data_01' => $new_campaign_record['structured_data_01'] 
                ) ;
            $update_campaign_input['structured_data_01']['recipients']['to']['delivery_list_custom']['member_id'] = array($test_delivery_input['member_id']) ; 
            $update_campaign_input['structured_data_01']['recipients']['to']['delivery_list_custom']['email_metadata_id'] = array('46','47','48') ; 
            $this->Action_Update_Campaign($update_campaign_input) ;    

            $this->Action_Trigger_Automation_Queue($new_automation_record['automation_id']) ; // Send the test email
            
            $update_campaign_input = array(
                'campaign_id' => $new_campaign_record['campaign_id'],
                'campaign_status_id' => $new_campaign_record['campaign_status_id'] 
                ) ;
            $this->Action_Update_Campaign($update_campaign_input) ;             
            }

        
        return $this ; 
        }  
    
    
    public function Action_Update_Automation($automation_input = array()) { 
        
        $this->Set_Model_Timings('Asset.Action_Update_Automation_begin_'.$automation_input['automation_id']) ; 
        
        $continue = 1 ; 

        if (!isset($automation_input['automation_id'])) { 
            $continue = 0 ; 
            } 
        

        // If an input exists for timestamp_next_action, remove datetime_next_action so it doesn't conflict
        if ($automation_input['timestamp_next_action']) {
            unset($automation_input['datetime_timestamp_next_action']) ; 
            }

        if ($automation_input['timestamp_next_action'] === 'immediate') {           
            $automation_input['timestamp_next_action'] = time() ; 
            }
        

        if ($continue == 1) {
            $automation_record = $this->Set_Automation_By_ID($automation_input['automation_id'])->Get_Automation() ; 

            $automation_input = $this->Action_Convert_Datetime_To_Timestamp($automation_input) ;

            if (isset($automation_input['automation_status_name'])) {
                $automation_status_options['filter_by_asset_status_name'] = $automation_input['automation_status_name'] ;

                $automation_status = $this->Retrieve_Asset_Status_ID_Array($automation_status_options,'single') ; 
                $automation_input['automation_status_id'] = $automation_status['asset_status_id'] ; 
                
                }
            
            
            switch ($automation_input['automation_status_name']) { 
                case 'scheduled':
                    
                    break ; 
                case 'draft':

                    switch ($automation_record['automation_status_name']) {
                        case 'deleted':                                
                        case 'scheduled':
                        case 'draft':
                            $automation_input['timestamp_next_action'] = 0 ; // Moving to draft so remove a next action timestamp

                            break ;
                        case 'error': // Should this one go here?
                        case 'packaged':
                        case 'in_progress':
                        case 'queued':
                        case 'complete':
                            unset($automation_input['automation_status_id']) ; // Cannot update this automation status so removing from input
                            break ;
                        }                        

                    break ; 
                case 'deleted':    
                    
                    switch ($automation_record['automation_status_name']) {
                        case 'error': // Change to deleted. 
                        case 'draft': // Change to deleted. 
                        case 'scheduled': // Change to deleted. 
                            $automation_input['timestamp_next_action'] = 0 ; // Moving to deleted so remove a next action timestamp

                            break ;
                        case 'packaged': // Cannot be changed - this automation step needs to finish
                        case 'in_progress': // Cannot be changed - this automation step needs to finish
                        case 'queued': // Leave as is. 
                        case 'complete': // Leave as is.  
                        case 'deleted': // Leave as is.  
                            unset($automation_input['automation_status_id']) ; // Cannot update this automation status so removing from input
                            break ;
                        }                    
                    
                    break ; 
                }            
            
            unset($automation_input['automation_status_name']) ; 
            
            $this->Set_Model_Timings('Asset.Action_Update_Automation_update_trigger_'.$automation_input['automation_id']) ; 
            $result = $this->Update_Automation($automation_input) ;
            $this->Set_Model_Timings('Asset.Action_Update_Automation_update_complete_'.$automation_input['automation_id']) ; 
        
            $this->Set_Model_Timings('Asset.Action_Update_Automation_set_automation_by_id_trigger_'.$automation_input['automation_id']) ; 
        
            $this->Set_Automation_By_ID() ;            
            $this->Set_Alert_Response(209) ; // Success - Automation updated
            
            $this->Set_Model_Timings('Asset.Action_Update_Automation_complete_'.$automation_input['automation_id']) ; 
        
            }
        
        return $this ;    
        }
        
        
    public function Action_Update_Campaign($campaign_input = array()) {    
        
        $this->Set_Model_Timings('Asset.Action_Update_Campaign_begin_'.$campaign_input['campaign_id']) ; 
        
        global $server_config ;
             
        $continue = 1 ; 

        if (!isset($campaign_input['campaign_id'])) {    
            $continue = 0 ; 
            }         
        
        if ($continue == 1) { 
            
            $this->Set_Model_Timings('Asset.Action_Update_Campaign_entered_processing') ; 
            
            $skip_post_update_reset = $campaign_input['skip_post_update_reset'] ; 
            unset($campaign_input['skip_post_update_reset']) ;
            
            
            if ($this->campaign['campaign_id'] != $campaign_input['campaign_id']) {  
                
                $campaign_options = array(
                    'condense_campaign' => 'yes',
                    'skip_delivery_asset' => 'yes',
                    'skip_content_asset' => 'yes'
                    ) ;
                $this->Set_Campaign_By_ID($campaign_input['campaign_id'],$campaign_options) ; 
                } 
            
            $campaign_record = $this->Get_Campaign() ; 
            
            $campaign_input = $this->Action_Convert_Datetime_To_Timestamp($campaign_input) ;        
            
            if (!isset($campaign_input['structured_data_01'])) { 
                $campaign_input['structured_data_01'] = $campaign_record['structured_data_01'] ;
                }
            
            $this->Set_Model_Timings('Asset.Action_Update_Campaign_campaign_retrieved') ; 
            
            if (isset($campaign_input['campaign_status_name'])) {   
                
                $campaign_status_options['filter_by_asset_status_name'] = $campaign_input['campaign_status_name'] ;
                
                $campaign_status = $this->Retrieve_Asset_Status_ID_Array($campaign_status_options,'single') ; 
                $campaign_input['campaign_status_id'] = $campaign_status['asset_status_id'] ; 
                
                switch ($campaign_input['campaign_status_name']) {
                    case 'scheduled': // Moving to scheduled                      
                        $campaign_input['timestamp_campaign_scheduled'] = TIMESTAMP ;
                        break ; 
                    case 'in_progress': // Moving series to in progress                          
                        
                        if ($campaign_record['content_asset_type_name'] == 'series') {
                            $campaign_input['timestamp_campaign_scheduled'] = TIMESTAMP ;
                            }
                        
                        break ;
                    case 'draft': // Moving to draft
                        
                        switch ($campaign_record['campaign_status_name']) { 
                            case 'deleted':                                
                            case 'scheduled':
                            case 'draft':
                                $campaign_input['timestamp_campaign_scheduled'] = 0 ;
                                
                                // Since we're moving the campaign to draft, every automation from the 
                                // current position forward should also be moved to draft...
                                $automation_query = array(
                                    'filter_by_campaign_id' => $campaign_input['campaign_id'],
                                    'filter_by_campaign_position' => $campaign_record['campaign_current_position'],
                                    'timestamp_next_action' => 'all'
                                    ) ;

                                $this->Set_Automation_List($automation_query) ; 

                                $automation_list = $this->Get_Automation_List() ; 

                                foreach ($automation_list as $automation_record) {
                                    
                                    $automation_input = array(
                                        'automation_id' => $automation_record['automation_id'],
                                        'automation_status_id' => $campaign_input['campaign_status_id'], // Draft, because we're moving the campaign to draft
                                        'timestamp_next_action' => 0
                                        ) ;
                                    $this->Action_Update_Automation($automation_input) ; 
                                    }
                                
                                break ;
                            case 'error': // Should this one go here?
                            case 'packaged':                            
                            case 'queued':
                                unset($campaign_input['campaign_status_id']) ; // Cannot update this campaign status so removing from input
                                
                                break ; 
                            case 'in_progress':    
                                
                                // Only allow in_progress series to be moved back to Draft
                                if ($campaign_record['content_asset_type_name'] != 'series') {
                                    unset($campaign_input['campaign_status_id']) ; // Cannot update this campaign status so removing from input
                                    }
                                
                                break ; 
                            case 'complete':
                                unset($campaign_input['campaign_status_id']) ; // Cannot update this campaign status so removing from input
                                break ;
                            }                        
                                                
                        break ;
                    case 'deleted': // Moving to deleted
                        
                        // Review current status
                        switch ($campaign_record['campaign_status_name']) { 
                            case 'draft':
                            case 'scheduled':    
                            case 'error':     
                                $campaign_input['timestamp_campaign_scheduled'] = 0 ;
                                
                                // Since we're moving the campaign to deleted, every automation from the 
                                // current position forward should also be moved to deleted...
                                $automation_query = array(
                                    'filter_by_campaign_id' => $campaign_input['campaign_id'],
                                    'filter_by_campaign_position' => $campaign_record['campaign_current_position'],
                                    'timestamp_next_action' => 'all'
                                    ) ;

                                $this->Set_Automation_List($automation_query) ; 

                                $automation_list = $this->Get_Automation_List() ; 

                                foreach ($automation_list as $automation_record) {
                                    
                                    $automation_input = array(
                                        'automation_id' => $automation_record['automation_id'],
                                        'automation_status_id' => $campaign_input['campaign_status_id'], // Draft, because we're moving the campaign to draft
                                        'timestamp_next_action' => 0
                                        ) ;
                                    $this->Action_Update_Automation($automation_input) ; 
                                    }
                                
                                break ;
                            case 'packaged':
                            case 'in_progress':
                            case 'queued':
                            case 'complete':
                                // Will allow campaign to be deleted, but automations that 
                                // have already been queued or delivered must remain delivered.
                                break ;
                            }                        
                                                
                        break ; 
                    }
                
                unset($campaign_input['campaign_status_name']) ; 
                }
            
            
            // Add metadata: Tags, categories, etc...
            if (isset($campaign_input['metadata_id'])) {                 

                $metadata_array = Utilities::Process_Comma_Separated_String($campaign_input['metadata_id']) ;
                unset($campaign_input['metadata_id']) ; 

                // Add metadata relationship...
                if ((isset($metadata_array)) AND (count($metadata_array) > 0)) {
                    foreach ($metadata_array as $metadata_id) {
                        
                        $metadata_relationship = array(
                            'item_id' => $this->campaign_id,
                            'metadata_id' => $metadata_id,
                            'relationship_type' => 'campaign'
                            ) ; 

                        $this->Action_Create_Metadata_Relationship($metadata_relationship) ;                        
                        }
                    }
                
                }
            
            
            // Archive / Unarchive a campaign...
            if (isset($campaign_input['archive_campaign'])) {            
                
                $archived_item_metadata_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'archived_item')['value'] ;  

                switch ($campaign_input['archive_campaign']) {
                    case 'yes':
                        
                        $eligible_archive_array = array('error','deleted','draft','complete','scheduled') ; 
                        
                        // If campaign is currently in an eligible status
                        if (in_array($campaign_record['campaign_status_name'], $eligible_archive_array)) {
                             
                            $archive_relationship = array(
                                'item_id' => $campaign_input['campaign_id'],
                                'metadata_id' => $archived_item_metadata_id,
                                'relationship_type' => 'campaign'
                                ) ; 
                            $this->Action_Create_Metadata_Relationship($archive_relationship) ; 
                            
                            }
                        break ; 
                    case 'no':
                        
                        $unarchive_query = array(
                            'relationship_id' => $campaign_record['archived_item']
                            ) ; 
                        $this->Action_Delete_Metadata_Relationship($unarchive_query) ; 
                        
                        break ; 
                    }
                
                }
            
            
            
            if (isset($campaign_input['delivery_asset_id'])) {
            
                switch ($campaign_record['content_asset_type_name']) {
                    case 'email':
                        $campaign_input['structured_data_01']['recipients'] = $this->campaign_recipients_template_email ; 
                        $campaign_input['delivery_trigger'] = 'delivery_list' ;
                        break ; 
                    case 'sms':
                        $campaign_input['structured_data_01']['recipients'] = $this->campaign_recipients_template_sms ; 
                        $campaign_input['delivery_trigger'] = 'delivery_list' ;
                        break ;
                    case 'series':
                        $campaign_input['structured_data_01']['recipients'] = $this->campaign_recipients_template_series ; 
                        $campaign_input['delivery_trigger'] = 'delivery_list_custom' ;
                        break ;                        
                    }
                
                }
            
//            print_r('<pre>') ;       
//            print_r($campaign_input) ;     
//            print_r('</pre>') ; 
            
            if ($campaign_input['delivery_list_filtered'] == 'reset') {                 
                
                // User Campaign Recipients template...
                switch ($campaign_record['content_asset_type_name']) { 
                    case 'email':
                        $campaign_input['structured_data_01']['recipients']['to']['delivery_list_filtered'] = $this->campaign_recipients_template_email['to']['delivery_list_filtered'] ;         
                        break ; 
                    case 'sms':
                        $campaign_input['structured_data_01']['recipients']['to']['delivery_list_filtered'] = $this->campaign_recipients_template_sms['to']['delivery_list_filtered'] ;         
                        break ;
                    case 'series':
                        $campaign_input['structured_data_01']['recipients']['to']['delivery_list_filtered'] = $this->campaign_recipients_template_series['to']['delivery_list_filtered'] ;         
                        break ;                
                    } 
                unset($campaign_input['delivery_list_filtered']) ; 
                }
            
            
            if (isset($campaign_input['test_mode'])) {
                switch ($campaign_input['test_mode']) {
                    case "true":
                    case "yes":
                        $campaign_input['structured_data_01']['test_mode'] = 'yes' ; 
                        break ; 
                    case "false":
                    case "no":
                        $campaign_input['structured_data_01']['test_mode'] = 'no' ; 
                        break ; 
                    }                
                unset($campaign_input['test_mode']) ; 
                }
            if (isset($campaign_input['halt_delivery'])) {
                switch ($campaign_input['halt_delivery']) {
                    case "true":
                    case "yes":
                        $campaign_input['structured_data_01']['halt_delivery'] = 'yes' ; 
                        break ; 
                    case "false":
                    case "no":
                        $campaign_input['structured_data_01']['halt_delivery'] = 'no' ; 
                        break ; 
                    }
                unset($campaign_input['halt_delivery']) ; 
                }            
            

            if (isset($campaign_input['structured_data_01'])) { 
                $campaign_input['structured_data_01'] = json_encode($campaign_input['structured_data_01']) ; 
                }
            
            
            $result = $this->Update_Campaign($campaign_input) ;  
            error_log(json_encode($result)) ; 
            
            $this->test_data = $result ; 
            
            if ($skip_post_update_reset != 'yes') {
                $campaign_options = array(
                    'condense_campaign' => 'yes'
                    ) ;
                $this->Set_Campaign_By_ID($campaign_input['campaign_id'],$campaign_options) ; 
                }
                
            $this->Set_Alert_Response(209) ; // Success - Campaign updated    
                
            }
        
        $this->Set_Model_Timings('Asset.Action_Update_Campaign_end_'.$campaign_input['campaign_id']) ; 
        
        return $this ; 
        }
    
    public function Action_Process_Automation_Step($automation_result,$campaign_id = 'internal',$automation_id = 'internal') {
        
        
        if ('internal' === $campaign_id) {
            $campaign_id = $this->campaign_id ; 
            } else {
                $this->campaign_id = $campaign_id ;      
                }
        
        if ('internal' === $automation_id) {
            $automation_id = $this->automation_id ; 
            } else {
                $this->automation_id = $automation_id ;  
                }
        
        
        $campaign_input = array() ; // Initialize campaign input array
        
        
        $this->Set_Campaign_By_ID($campaign_id) ; 
        $this->Set_Automation_By_ID($automation_id) ;
        
        $data['campaign_record'] = $this->Get_Campaign() ; 
        $data['automation_record'] = $this->Get_Automation() ; 
        

        switch ($data['campaign_record']['content_asset_type_id']) { 
            case 12: // Email
            case 18: // SMS    
                
                switch ($data['automation_record']['automation_status_id']) {
                    case 8: // Complete
                        
                        $campaign_input = array(
                            'campaign_status_id' => 8 // Complete
                            ) ; 

                        if ($data['campaign_record']['structured_data_01']['campaign_options']['auto_delete'] == 'yes') {
                            $campaign_input['campaign_status_id'] = 2 ;
                            }
                        
                        break ;
                    case 1: // Error 
                        
                        if ($data['automation_record']['error_count'] >= 3) {
                            $campaign_input['campaign_status_id'] = 1 ; // Error
                            }

                        break ;
                    }                 
                
                break ;                
            case 17: // Series    
                
                
                $sequence = new Asset() ; 
                $sequence->Set_Asset_By_ID($data['automation_record']['asset_id']) ; 
                $data['series_record'] = $sequence->Get_Asset() ;

                
                $data['series_current_structure'] = Utilities::Deep_Array($data['series_record']['structure'], 'structure_order', $data['automation_record']['campaign_position']) ; 
                    
                $data['series_current_structure_key'] = Utilities::Deep_Array_Key($data['series_record']['structure'], 'structure_order', $data['automation_record']['campaign_position']) ; 
                if (isset($data['series_record']['structure'][$data['series_current_structure_key'] + 1])) {
                    $data['campaign_next_position'] = $data['automation_record']['campaign_position'] + 1 ; 
                    $data['series_next_structure'] = $data['series_record']['structure'][$data['series_current_structure_key'] + 1] ;                 
                    }
                
                
                switch ($data['automation_record']['automation_status_id']) {
                    case 8: // Complete 
                        
                        if (isset($data['series_record']['structure'][$data['series_current_structure_key'] + 1])) {                

                            $new_automation_input = array(
                                'campaign_id' => $data['automation_record']['campaign_id'],
                                'campaign_position' => $data['series_next_structure']['structure_order'],
                                'automation_status_id' => 4, // Scheduled
                                'timestamp_next_action' => TIMESTAMP + $data['series_next_structure']['delay']
                                ) ; 
                            
                            $data['new_automation'] = $this->Create_Automation($new_automation_input) ;  
                                
                            // Update the past automation record asset_members member_id with new automation id.
                            $member_id_array = array($data['automation_record']['member_id']) ; 
                            $member_update_options = array(
                                'automation_id' => $data['new_automation']['insert_id'],
                                'member_status_id' => 6 // In Progress
                                ) ; 
                            $this->Action_Asset_Member_Update($member_id_array,$member_update_options) ;                            
                            
                            } else {
                            
                                // Asset status marked complete
                                $member_id_array = array($data['automation_record']['member_id']) ; 
                                $member_update_options = array(
                                    'member_status_id' => 8 // Complete
                                    ) ; 
                                $this->Action_Asset_Member_Update($member_id_array,$member_update_options) ; 
                                } 

                        
                        // Update timestamp last member update
                        $update_delivery_asset = array(
                            'auth_override' => 'yes',
                            'asset_input_info' => array(
                                'rewrite_latest_history' => 'yes'
                                ),
                            'asset_id' => $data['automation_record']['delivery_asset_id'],
                            'visibility_id' => $data['automation_record']['delivery_asset_visibility_id'],
                            'timestamp_last_member_update' => time()-1
                            ) ; 
                        $delivery_asset = new Asset() ; 
                        $delivery_asset->Action_Update_Content($update_delivery_asset) ;                        
                        
                        break ;
                    case 1: // Error 
                        
                        // Indicate that the asset subscription is in error
                        $member_id_array = array($data['automation_record']['member_id']) ; 
                        $member_update_options = array(
                            'member_status_id' => 1 // Error
                            ) ; 
                        $this->Action_Asset_Member_Update($member_id_array,$member_update_options) ;  

                        // Update timestamp last member update
                        $update_delivery_asset = array(
                            'auth_override' => 'yes',
                            'asset_input_info' => array(
                                'rewrite_latest_history' => 'yes'
                                ),
                            'asset_id' => $data['automation_record']['delivery_asset_id'],
                            'visibility_id' => $data['automation_record']['delivery_asset_visibility_id'],
                            'timestamp_last_member_update' => time()-1
                            ) ; 
                        $delivery_asset = new Asset() ; 
                        $delivery_asset->Action_Update_Content($update_delivery_asset) ;
                        
                        break ;
                    }                

                break ;                
            }
        
        
        $data['automation_result'] = $automation_result ; 
        
        // UPDATE THE STATUS OF THE AUTOMATION -- Already updating this in email; need to do the same in SMS
//        $data['automation_update'] = $this->Update_Automation($campaign_automation_input) ;

        // UPDATE THE STATUS OF THE CAMPAIGN       
        $data['campaign_update'] = $this->Update_Campaign($campaign_input) ; 
        
        $this->automation_update = $data ; 
        
        return $this ; 
        }
    
    
    public function Action_Map_Automation($automation_item) {
        
        if (is_array($automation_item)) {
        
            if (isset($automation_item['recipients'])) {
        
                $contact_to_array = array() ; 
                $contact_cc_array = array() ; 
                $contact_bcc_array = array() ; 
                $errors_array = array() ; 
                
                
                
                $automation_item['recipients']['to']['count'] = 0 ; 
                $automation_item['recipients']['to']['recipients'] = array() ; 
                
                $automation_item['recipients']['cc']['count'] = 0 ; 
                $automation_item['recipients']['cc']['recipients'] = array() ; 
                
                $automation_item['recipients']['bcc']['count'] = 0 ;
                $automation_item['recipients']['bcc']['recipients'] = array() ; 
                
                $automation_item['recipients']['total_count'] = 0 ; 
                
                $automation_item['recipient_errors'] = $errors_array ;
                
                } 

            }
            
        return $automation_item ; 
        }
    
    
    public function Action_Compile_Report() {
        
        $continue = 1 ; 
        
        if (!isset($this->campaign['campaign_id'])) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            
            $campaign_record = $this->Get_Campaign() ; 
            $automation_record = $this->Get_Automation() ; 
            
            $campaign_report = array(
                'campaign_id' => $campaign_record['campaign_id'],
                'campaign_title' => $campaign_record['campaign_title'],
                'recipient_count' => 0,
                'queued' => 0,
                'delivered' => 0,
                'complained' => 0,
                'failed' => 0,
                'opened' => 0,
                'clicked' => 0,
                'total_opens' => 0,
                'total_clicks' => 0,
                'click_log' => array()
                ) ;
            
            
            switch ($campaign_record['type_name']) {
                case 'email':
                    
                    $email = new Email() ; 
                    $email->Set_Replicate_User_Account_Profile($this) ;
                    
                    $email_query = array(
                        'filter_by_automation_id' => $automation_record['automation_id'],
                        'override_paging' => 'yes'
                        ) ; 
                    $email->Set_Email_List($email_query) ; 
                    $campaign_report['email_list'] = $email->Get_Email_List() ; 
                    
                    $email->Set_Email_Events_List($email_query) ; 
//                    $campaign_report['email_events_list'] = $email->Get_Email_Events_List() ; 
                    $email_events_list = $email->Get_Email_Events_List() ; 
                    
                    foreach ($campaign_report['email_list'] as $email_recieved) {
                        
                        $campaign_report['recipient_count']++ ; 
                        
                        if ($email_recieved['delivered'] == 1) {
                            $campaign_report['delivered']++ ; 
                            } 
                        
                        if ($email_recieved['opened'] == 1) {
                            $campaign_report['opened']++ ; 
                            }   
                        if ($email_recieved['clicked'] == 1) {
                            $campaign_report['clicked']++ ; 
                            }                        
                        }
                    
                    unset($campaign_report['email_list']) ; 
                    
                    $c = 0 ; 
                    $d = 0 ; 
                    do {
                        
                        if ($email_events_list[$c]['event'] == 'accepted') {
                            $campaign_report['queued']++ ;
                            }
                                                
                        if ($email_events_list[$c]['event'] == 'failed') {
                            if ($email_events_list[$c]['fail_severity'] == 'permanent') {
                                $campaign_report['failed']++ ;    
                                $campaign_report['failure_log'][] = $email_events_list[$c] ;                                 
                                }
                            }
                        
                        if ($email_events_list[$c]['event'] == 'complained') {
                            $campaign_report['complained']++ ;
                            }
                        
                        if ($email_events_list[$c]['event'] == 'opened') {
                            $campaign_report['total_opens']++ ;
                            }
                        
                        
                        if ($email_events_list[$c]['event'] == 'clicked') {
                            
                            $campaign_report['total_clicks']++ ;
                            
                            $found_url_key = Utilities::Deep_Array_Key($campaign_report['click_log'],'click_url',$email_events_list[$c]['click_url']) ;
                            
                            if (is_numeric($found_url_key)) {
                                
                                $campaign_report['click_log'][$found_url_key]['total_clicks'] = $campaign_report['click_log'][$found_url_key]['total_clicks'] + 1 ;
                                
                                } else {
                                
                                    $campaign_report['click_log'][] = array(
                                        'click_url' => $email_events_list[$c]['click_url'],
                                        'total_clicks' => 1
                                        ) ;
                                
                                    }
                            unset($found_url) ;                             
                            }
                        
                        
                        if ($email_events_list[$c]['event'] == 'complained') {
                            $campaign_report['total_opens']++ ;
                            }
                        
                        
                        
                        if (($email_events_list[$c]['event'] == 'clicked') AND ($d < 3)) {
                            $campaign_report['events'][] = $email_events_list[$c] ;  
                            $d++ ; 
                            }
                        
                        $c++ ;
                        } while (isset($email_events_list[$c]['event_id'])) ;
                    
                    
                    // Sort the links array by total_clicks DESC
                    $columns = array_column($campaign_report['click_log'], 'total_clicks');
                    array_multisort($columns, SORT_DESC, $campaign_report['click_log']);
                    
                    
                    break ; 
                case 'sms':    
                    
                    $message = new Message() ; 
                    $message->Set_Replicate_User_Account_Profile($this) ;
                    
                    $message_query = array(
                        'filter_by_automation_id' => $automation_record['automation_id'],
                        'override_paging' => 'yes'
                        ) ; 
                    $message->Set_Message_List($message_query) ; 
                    $campaign_report['message_list'] = $message->Get_Message_List() ; 
                    
                    $message->Set_Message_Event_List($message_query) ; 
                    $campaign_report['message_events_list'] = $message->Get_Message_Event_List() ; 
                    $message_events_list = $message->Get_Message_Event_List() ;

                    $campaign_report['query'] = $message->message_query_result ;
                    
                    foreach ($campaign_report['message_list'] as $message_item) {
                        
                        $campaign_report['recipient_count']++ ; 
                        
                        if ($message_item['delivered'] == 1) {
                            $campaign_report['delivered']++ ; 
                            } 
                        
//                        if ($message_item['opened'] == 1) {
//                            $campaign_report['opened']++ ; 
//                            }   
//                        if ($message_item['clicked'] == 1) {
//                            $campaign_report['clicked']++ ; 
//                            }                        
                        }
                    
                    unset($campaign_report['message_list']) ;
                    
                    
                    $c = 0 ; 
                    $d = 0 ; 
                    do {
                        
//                        if ($message_events_list[$c]['event'] == 'accepted') {
//                            $campaign_report['queued']++ ;
//                            }
                                                
                        if ($message_events_list[$c]['event'] == 'error') {
                            $campaign_report['failed']++ ;    
                            }
                        
//                        if ($email_events_list[$c]['event'] == 'complained') {
//                            $campaign_report['complained']++ ;
//                            }
//                        
//                        if ($email_events_list[$c]['event'] == 'opened') {
//                            $campaign_report['total_opens']++ ;
//                            }
                        

                        
                        if (($message_events_list[$c]['event'] == 'error') AND ($d < 3)) {
                            $campaign_report['events'][] = $message_events_list[$c] ;  
                            $d++ ; 
                            }
                        
                        $c++ ;
                        } while (isset($message_events_list[$c]['event_id'])) ;                    
                    break ; 
                }
            
            $this->campaign_report = $campaign_report ; 
            }
     
                
        return ; 
        }     
    
    
    
    
    public function Action_Create_Asset_Metadata_Relationship($query_params = array()) {
        
        $continue = 1 ; 
        
        if ((!isset($query_params['asset_id'])) AND (!isset($this->asset_id))) {
            
            $continue = 0 ; 
            // Add an error for no asset ID submitted
            }
        
        if (!isset($query_params['asset_id'])) {
            
            $query_params['asset_id'] = $this->asset_id ; 
            }        
        
        if (!isset($query_params['metadata_id'])) {
            
            $continue = 0 ; 
            // Add an error for no metadata ID submitted
            }
        
        
        if ($continue == 1) {

            $result = $this->Create_Asset_Metadata_Relationship($query_params) ;         

            if (($result['insert_id']) OR ($result['result_count'] > 0)) {

                // Success creating metadata relationship
                } else {

                    // Error creating metadata relationship
                    }            
            }

        
        
//        $this->Set_Asset() ; 
        return $this ; 
        }
    
    
    public function Action_Delete_Asset_Metadata_Relationship($query_params = array()) {
        
        if ($query_params['relationship_id']) {
            $result = $this->Delete_Asset_Metadata_Relationship($query_params) ;     
            } elseif ($query_params['asset_id'] AND $query_params['metadata_id']) {
            
                $result = $this->Delete_Asset_Metadata_Relationship($query_params) ;            
                }
        
        
        // Add a test here for a valid metadata_id
        
            
//        $this->Set_Asset() ; 
        
        return $this ; 
        
        }

    // Compile asset list loops through each asset id and actually pulls the ENTIRE asset and all subassets for the entire list
    // This would be very process intensive. We should limit this
    public function Action_Compile_Asset_List($compile_list_options = array()) {
        
        $final_asset_set = array() ; 
        
        if (!isset($compile_list_options['skip_history'])) {
            $compile_list_options['skip_history'] = 'yes' ; 
            }

        
        // Process a post array from above, retrieve the full asset and set the appropriate tag list
        if (isset($this->asset_list) AND count($this->asset_list > 0) AND ($this->asset_list != 'error')) {         
            foreach ($this->asset_list as $asset) {
                
                $this->asset = $asset ;                 
                $this->Set_Asset_ID($asset['asset_id'])->Action_Compile_Asset() ; 
                
                $asset_compiled = $this->Get_Asset_Compiled() ; 
                $final_asset = $asset ; 
                $final_asset['asset_compiled'] = $asset_compiled ; 
                
                if ($final_asset != 'error') {
                    
                    if (is_array($compile_list_options['remove_fields'])) {
                        foreach ($compile_list_options['remove_fields'] as $remove_field_name) {
                            unset($final_asset[$remove_field_name],$final_asset['asset_core'][$remove_field_name]) ; 
                            }                        
                        }
                    
                    $final_asset_set[] = $final_asset ; 
                    }
                unset($final_asset) ;
                } 
            
            $this->asset_list = $final_asset_set ; 
            } 
        
        return $this ; 
        }
    
    
    // input_array should be submitted as a comma separated string of member_id's or an array of member_id's
    // This script doesn't do any validity checks on whether ot not a member is ELIGIBLE to be updated -- use carefully.
    public function Action_Asset_Member_Update($input_array,$options_array = array()) { 
        
        $data['asset_record'] = $this->Get_Asset() ;
        
        if (isset($options_array['metadata_slug'])) {
            
            $metadata = Utilities::Deep_Array($data['asset_record']['map_array']['additional_data']['metadata'],'metadata_slug',$options_array['metadata_slug']) ; 
            $options_array['metadata_id'] = $metadata['metadata_id'] ; 
            }
        
        
        // Turn the input into an array of member_id's (not member_id_value)
        $member_id_array = Utilities::Process_Comma_Separated_String($input_array) ; 

        foreach ($member_id_array as $member) { 
            
            if (isset($member['member_id'])) {
                $member_id = $member['member_id'] ; 
                } else {
                    $member_id = $member ; 
                    }
            
            $query_options = array(
                'member_id' => $member_id
                ) ; 
            
            if (isset($options_array['asset_id'])) { 
                $query_options['asset_id'] = $options_array['asset_id'] ; 
                }            
            if (isset($options_array['metadata_id'])) {
                $query_options['metadata_id'] = $options_array['metadata_id'] ; 
                }
            if (isset($options_array['campaign_id'])) {
                $query_options['campaign_id'] = $options_array['campaign_id'] ; 
                }
            if (isset($options_array['automation_id'])) {
                $query_options['automation_id'] = $options_array['automation_id'] ; 
                }
            if (isset($options_array['member_status_id'])) {
                $query_options['member_status_id'] = $options_array['member_status_id'] ; 
                } 
            if (isset($options_array['email_metadata_id'])) {
                $query_options['email_metadata_id'] = $options_array['email_metadata_id'] ; 
                }
            if (isset($options_array['sms_metadata_id'])) {
                $query_options['sms_metadata_id'] = $options_array['sms_metadata_id'] ; 
                }
            
            $result = $this->Update_Asset_Member($query_options) ; 
            
            $result['input_options'] = $options_array ; 
            $result['query_values'] = $query_options ; 
//            $this->asset_query_result = $result ; 
//            $result['input_array'] = $input_array ; 
//            $result['post_comma_array'] = $member_id_array ; 
//            $result['member'] = $member ; 
//            $result['member_id'] = $member_id ; 
//            $this->test_data = $result ; 
            }
        
        
        return $this ;     
        }
    
    
    
    // input_array should be submitted as a string of member_id's or an array of member_id's
    public function Action_Asset_Member_Process($input_array,$options_array = array()) { 

        $processed_count = 0 ; 
        
        $this->asset_members_processing_list = array(
            'clean_invite_list' => array(),
            'clean_update_list' => array(),
            'dirty_invite_list' => array(),
            'invite_count' => 0,
            'update_count' => 0,
            'automation_update_count' => 0
            ) ;
        
        $data['asset_record'] = $this->Get_Asset() ; 
         
        if (isset($options_array['campaign_id'])) {
            $data['campaign_record'] = $this->Set_Campaign_By_ID($options_array['campaign_id'])->Get_Campaign() ; 
            }
        
        // Opt-in options: double, single_welcome, single
        // Default is set in the asset, but can be over-ridden with options input
        if (!isset($options_array['require_optin'])) { 
            $options_array['require_optin'] = $this->asset['require_optin'] ; 
            }
        
        if (!isset($options_array['allow_duplicates'])) { 
            $options_array['allow_duplicates'] = $this->asset['allow_duplicates'] ; 
            }        
        
        if (!isset($options_array['invitation_asset'])) {
            $options_array['invitation_asset'] = $this->asset ; 
            }
        
//        if ($data['asset_record']['asset_id'] == 8232) {
//            error_log('Started processing Action_Asset_Member_Process') ; 
//            }
        
        switch ($data['asset_record']['type_id']) {   
            case 19: // Contact List
            case 26: // Contact Series List    
                
                // Turn the input into an array of contact_id's
                $contact_id_array = Utilities::Process_Comma_Separated_String($input_array['contact_id']) ; 
                $contact_query_options['contact_id'] = $contact_id_array ; 
                
                // Determine if the recipient input is contact_id's or existing asset member_id's
                // Place the recipients into a consolidated array that can be processed.
                if (count($contact_id_array) > 0) { 
                    
                    $processed_count = count($contact_id_array) ; 
                    
                    $contact_query_options['filter_by_account_id'] = 'yes' ; 
                    $contact_query_options['filter_by_contact_id'] = 'list' ;  
                    $contact_query_options['override_paging'] = 'yes' ; 
                    $contact_query_options['skip_compile_contact'] = 'yes' ; 
                    
                    $contact = new Contact() ;
                    $contact->Set_Replicate_User_Account_Profile($this) ; 
                    $contact->Set_Contact_List($contact_query_options) ;                  

                    // The set of submitted contacts
                    $data['contact_list'] = $contact->Get_Contact_List() ;

                    // Get members of the target asset that match the member_id_value
                    $target_member_list_query_options['override_paging'] = 'yes' ; 
                    $target_member_list_query_options['skip_asset_mapping'] = 'yes' ;
                    $target_member_list_query_options['minify_list'] = 'yes' ;
                    $target_member_list_query_options['filter_by_member_id_value'] = $contact_id_array ;
                    
                    $this->Set_Asset_Members_List($target_member_list_query_options) ; 
                    $data['target_asset_members_list'] = $this->Get_Asset_Members_List() ;                

                    
                    // Check to see if recipient already a member of the asset
                    foreach ($data['contact_list'] as $submitted_member) {  

                        $found_member = Utilities::Deep_Array($data['target_asset_members_list'], 'member_id_value', $submitted_member['contact_id']) ; 

                        // Is recipient already a member of the asset?
                        if ($found_member) { // YES                                                          

                            switch ($options_array['allow_duplicates']) {
                                case 'true': // Asset allows duplicates? Meaning, do we allow multiple active subscriptions?
                                    $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                    break ;
                                default:

                                    // Is parent asset member status Inactive?
                                    switch ($found_member['metadata_id']) {
                                        case 51: // YES - Inactive Member  
                                            $submitted_member['member']['original'] = $found_member ; 
                                            $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // We're only going to update the member and reset their subscription if they're currently inactive and duplicates are *not* allowed on the asset
                                            break ;
                                        case 50:  // NO - Member Currently Active
                                            $submitted_member['member']['original'] = $found_member ; 
                                            $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                            break ; 
                                        default: // NO
                                            $submitted_member['member']['original'] = $found_member ; 
                                            $this->asset_members_processing_list['dirty_invite_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                        }                                
                                }

                            } else { // NO
                                $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                }


                        unset($found_member) ; 
                        }

                    } else {                    
                        
                    
                        // If user_id's weren't submitted, then member_id's should have been submitted
                        // Turn the input into an array of member_id's
                        $member_id_array = Utilities::Process_Comma_Separated_String($input_array['member_id']) ; 

                        if (count($member_id_array) > 0) { 
                            
                            $processed_count = count($member_id_array) ; 
                            
                            // Get the submitted member records of the source asset 
                            // ** IMPORTANT: could be a different asset than the target
                            $source_member_query_options['filter_by_member_id'] = $member_id_array ;
                            $source_member_query_options['filter_by_user_id'] = 'no' ; // Maybe don't keep this
                            $source_member_query_options['override_paging'] = 'yes' ;
                            $source_member_query_options['filter_by_asset_id'] = 'no' ; 
                            $this->Set_Asset_Members_List($source_member_query_options) ; 
                            $data['source_member_list'] = $this->Get_Asset_Members_List() ; 

                            // Create an array of member_id_values to search against the target asset
                            $member_id_value_array = array() ; 
                            foreach ($data['source_member_list'] as $target_member) {
                                $member_id_value_array[] = $target_member['contact_id'] ; 
                                }
                                                        
                            // Get members of the target asset that match the member_id_value
                            $target_member_list_query_options['override_paging'] = 'yes' ; 
                            $target_member_list_query_options['skip_asset_mapping'] = 'yes' ;
                            $target_member_list_query_options['minify_list'] = 'yes' ;
                            $target_member_list_query_options['filter_by_member_id_value'] = $member_id_value_array ; 
                            
                            $this->Set_Asset_Members_List($target_member_list_query_options) ; 
                            $data['target_asset_members_list'] = $this->Get_Asset_Members_List() ;
                            
                            
                            // Parse through the submitted members and determine if they are current members of the target asset
                            foreach ($data['source_member_list'] as $submitted_member) {

                                $found_member = Utilities::Deep_Array($data['target_asset_members_list'], 'member_id_value', $submitted_member['member_id_value']) ; 

                                // Is recipient already a member of the asset?
                                if ($found_member) { // YES                                                          

                                    switch ($options_array['allow_duplicates']) { 
                                        case 'true': // Asset allows duplicates? Meaning, do we allow multiple active subscriptions?
                                            $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                            break ;
                                        default:

                                            // Is parent asset member status Inactive?
                                            switch ($submitted_member['metadata_id']) {
                                                case 51: // YES - Inactive Member  
                                                    $submitted_member['member']['original'] = $found_member ; 
                                                    $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // We're only going to update the member and reset their subscription if they're currently inactive and duplicates are *not* allowed on the asset
                                                    break ;
                                                case 50:
                                                    $submitted_member['member']['original'] = $found_member ; 
                                                    $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                                    break ; 
                                                default: // NO
                                                    $submitted_member['member']['original'] = $found_member ; 
                                                    $this->asset_members_processing_list['dirty_invite_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                                }                                
                                        }

                                    } else { // NO
                                        $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                        }

                                unset($found_member) ;                   
                                }
                            }
                        }
                 
        
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['clean_invite_list'] as $invite) {
                    $this->asset_members_processing_list['clean_invite_list'][$i]['member_id_value'] = $invite['contact_id'] ; 
                    $i++ ; 
                    }
                
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['clean_update_list'] as $invite) { 
                    if (!isset($invite['member_id'])) {
                        $this->asset_members_processing_list['clean_update_list'][$i] = $invite['member']['original'] ; 
                        }                   
                    $this->asset_members_processing_list['clean_update_list'][$i]['member_id_value'] = $invite['contact_id'] ; 
                    $i++ ; 
                    }
                
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['dirty_invite_list'] as $invite) {
                    $this->asset_members_processing_list['dirty_invite_list'][$i]['member_id_value'] = $invite['contact_id'] ; 
                    $i++ ; 
                    }                
                

                $this->Action_New_Asset_Members_Process($options_array) ; 
                $this->Action_Updated_Asset_Members_Process($options_array) ; 
                
                $asset_members_processing_list = $this->Get_Asset_Members_Processing_List() ; 
                $data['clean_invite_list'] = $asset_members_processing_list['clean_invite_list'] ; 
                $data['clean_update_list'] = $asset_members_processing_list['clean_update_list'] ; 
                $data['dirty_invite_list'] = $asset_members_processing_list['dirty_invite_list'] ; 
                $data['invite_count'] = $asset_members_processing_list['invite_count'] ; 
                $data['update_count'] = $asset_members_processing_list['update_count'] ; 
                $data['automation_update_count'] = $asset_members_processing_list['automation_update_count'] ; 
                $data['options_inputs'] = $options_array ; 
                
                
                // Below this point needs work for Contacts
                // Would be nice to provide feedback details here on the ENTIRE array that was submitted
                if (!$new_invite['error']) {
                    
                    if ($processed_count == 1) { // 1 member processed
                        
                        if (isset($data['clean_invite_list'][0]['member_id_value'])) {
                            
                            // 1 member added
                            $this->Set_Alert_Response(281) ; // Successfully updated to list  
                            
                            } else if (isset($data['clean_update_list'][0]['member_id_value'])) {
                            
                                // 1 member updated 
                                $this->Set_Alert_Response(162) ; // Successfully updated to list   
                            
                            } else { 
                            
                                // 1 member generic response
                                $this->Set_Alert_Response(162) ; // Successfully updated to list
                                }
                        
                        
                        
                        } else if ($processed_count > 1) { // Multiple members processed
                            $this->Set_Alert_Response(162) ; // Successfully added to list
                            
                            } else { // No members processed
                            $this->Set_Alert_Response(163) ; // Error adding to the list
                            }
                    
                    } else {
                        $this->Set_Alert_Response(163) ; // Error adding to the list
                        }
                
                
//                $this->test_data = $data ; 
                $this->asset_member = $data['clean_invite_list'][0] ;

                
                break ; 
            case 20: // Admin User List
            case 28: // User SERIES list
                
                // Turn the input into an array of user_id's
                $user_id_array = Utilities::Process_Comma_Separated_String($input_array['user_id']) ; 
                $user_query_options['user_id'] = $user_id_array ; 

                // Determine if the recipient input is contact_id's or existing asset member_id's
                // Place the recipients into a consolidated array that can be processed.
                if (count($user_id_array) > 0) { 
                    
                    $user_query_options['filter_by_account_id'] = 'no' ; 
                    $user_query_options['filter_by_user_id'] = 'list' ;  
                    $user_query_options['override_paging'] = 'yes' ; 

                    $account = new Account($this->user_id) ;
                    $account->Set_Account_By_ID($this->account_id)->Set_Account_User_List($user_query_options) ;                  

                    $data['user_list'] = $account->Get_Account_User_List() ;

                    $all_member_list_query_options['override_paging'] = 'yes' ; 
                    $all_member_list_query_options['minify_list'] = 'yes' ;
                    $all_member_list_query_options['skip_asset_mapping'] = 'yes' ; 
                    $this->Set_Asset_Members_List($all_member_list_query_options) ; 
                    $data['asset_members_list'] = $this->Get_Asset_Members_List() ;               

                    
                    // Check to see if recipient already a member of the asset
                    foreach ($data['user_list'] as $submitted_member) {

                        $found_member = Utilities::Deep_Array($data['asset_members_list'], 'member_id_value', $submitted_member['user_id']) ; 
                                            
                        // Is recipient already a member of the asset?
                        if ($found_member) { // YES                                                          

                            switch ($options_array['allow_duplicates']) {
                                case 'true': // Asset allows duplicates? Meaning, do we allow multiple active subscriptions?
                                    $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                    break ;
                                default:

                                    // Is parent asset member status Inactive?
                                    switch ($found_member['metadata_id']) {
                                        case 51: // YES - Inactive Member  
                                            $submitted_member['found_member'] = $found_member ; 
                                            $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // We're only going to update the member and reset their subscription if they're currently inactive and duplicates are *not* allowed on the asset
                                            break ;
                                        case 50:  // NO - Member Currently Active
                                            $submitted_member['found_member'] = $found_member ; 
                                           $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                            break ; 
                                        default: // NO
                                            $submitted_member['found_member'] = $found_member ; 
                                            $this->asset_members_processing_list['dirty_invite_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                        }                                
                                }

                            } else { // NO
                                $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                }


                        unset($found_member) ; 
                        }

                    } else {
                    
                        // If user_id's weren't submitted, then member_id's should have been submitted
                        // Turn the input into an array of member_id's
                        $member_id_array = Utilities::Process_Comma_Separated_String($input_array['member_id']) ; 
                        
                        $member_query_options['filter_by_member_id'] = $member_id_array ;
                        $member_query_options['filter_by_user_id'] = 'no' ; // Maybe don't keep this
                        $member_query_options['filter_by_account_id'] = 'no' ; // Maybe don't keep this
                        $member_query_options['override_paging'] = 'yes' ; 
                        $member_query_options['filter_by_asset_id'] = 'no' ; 
                        
                        // Get the submitted member records of the source asset (could be a different asset than the target)
                        $this->Set_Asset_Members_List($member_query_options) ; 
                        $data['member_list'] = $this->Get_Asset_Members_List() ; 
                    
                        // Now get all the current members of the target asset
                        $all_member_list_query_options['override_paging'] = 'yes' ; 
                        $all_member_list_query_options['minify_list'] = 'yes' ;
                        $all_member_list_query_options['skip_asset_mapping'] = 'yes' ; 
                        $this->Set_Asset_Members_List($all_member_list_query_options) ; 
                        $data['asset_members_list'] = $this->Get_Asset_Members_List() ;
                    
                    
                        // Parse through the submitted members and determine if they are current members of the target asset
                        foreach ($data['member_list'] as $submitted_member) {

                            $found_member = Utilities::Deep_Array($data['asset_members_list'], 'member_id', $submitted_member['member_id']) ; 

                            // Is recipient already a member of the asset?
                            if ($found_member) { // YES                                                          
                            
                                switch ($options_array['allow_duplicates']) {
                                    case 'true': // Asset allows duplicates? Meaning, do we allow multiple active subscriptions?
                                        $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                        break ;
                                    default:

                                        // Is parent asset member status Inactive?
                                        switch ($submitted_member['metadata_id']) {
                                            case 51: // YES - Inactive Member  
                                                $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // We're only going to update the member and reset their subscription if they're currently inactive and duplicates are *not* allowed on the asset
                                                break ;
                                            case 50:
                                                $submitted_member['found_member'] = $found_member ; 
                                                $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                                break ; 
                                            default: // NO
                                                $submitted_member['found_member'] = $found_member ; 
                                                $this->asset_members_processing_list['dirty_invite_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                            }                                
                                    }

                                } else { // NO
                                    $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                    }

                            unset($found_member) ;                   
                            }
                        }
                 
        
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['clean_invite_list'] as $invite) {
                    $this->asset_members_processing_list['clean_invite_list'][$i]['member_id_value'] = $invite['user_id'] ; 
                    $i++ ; 
                    }
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['clean_update_list'] as $invite) {
                    if (!isset($invite['member_id'])) {
                        $this->asset_members_processing_list['clean_update_list'][$i] = $invite['found_member'] ; 
                        }                   
                    $this->asset_members_processing_list['clean_update_list'][$i]['member_id_value'] = $invite['user_id'] ; 
                    $i++ ; 
                    }
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['dirty_invite_list'] as $invite) {
                    $this->asset_members_processing_list['dirty_invite_list'][$i]['member_id_value'] = $invite['user_id'] ; 
                    $i++ ; 
                    }                
                

                
                $this->Action_New_Asset_Members_Process($options_array) ; 
                $this->Action_Updated_Asset_Members_Process($options_array) ; 

                
                
                // Below this point needs work for Contacts
                // Would be nice to provide feedback details here on the ENTIRE array that was submitted
                if (!$new_invite['error']) {
                    $this->Set_Alert_Response(162) ; // Successfully added to list
                    } else {
                        $this->Set_Alert_Response(163) ; // Error adding to the list
                        }
                
                
                $asset_members_processing_list = $this->Get_Asset_Members_Processing_List() ; 
                $data['clean_invite_list'] = $asset_members_processing_list['clean_invite_list'] ; 
                $data['clean_update_list'] = $asset_members_processing_list['clean_update_list'] ; 
                $data['dirty_invite_list'] = $asset_members_processing_list['dirty_invite_list'] ;       
                $data['invite_count'] = $asset_members_processing_list['invite_count'] ; 
                $data['update_count'] = $asset_members_processing_list['update_count'] ; 
                $data['automation_update_count'] = $asset_members_processing_list['automation_update_count'] ; 
//                $data['options_inputs'] = $options_array ; 
//                $data['user_query_inputs'] = $user_query_options ; 
//                $data['member_query_inputs'] = $member_query_options ; 
                
                
                
                $this->test_data = $data ; 
                
                $this->asset_member = $data['clean_invite_list'][0] ; 
                break ;
            case 23: // User Profile List
            case 27: // Profile SERIES list
                
                // Turn the input into an array of profile_id's
                $profile_id_array = Utilities::Process_Comma_Separated_String($input_array['profile_id']) ; 
                $profile_query_options['profile_id'] = $profile_id_array ;
                
                
                // Determine if the recipient input is profile_id's or existing asset member_id's
                // Place the recipients into a consolidated array that can be processed.
                if (count($profile_id_array) > 0) {
                    
                    // FOR TEAMS SOMEDAY  
                    // Would limit query to only profiles within the account
                    // These settings if you want to pull a list of ALL profiles on the submitting account (I think)
                    //$profile_query_options['filter_by_account_id'] = 'yes' ; 
                    //$profile_query_options['filter_by_profile_id'] = 'list' ;
                    
                    // FOR ADMIN ONLY
                    // Pulls the submitted profiles that exist
                    $profile_query_options['filter_by_account_id'] = 'profile' ; 
                    $profile_query_options['filter_by_profile_id'] = 'list' ;   
                    $profile_query_options['profile_id'] = $profile_id_array ;   
                    $profile_query_options['override_paging'] = 'yes' ;

                    // Pull all existing profiles
                    $account = new Account($this->user_id) ;
                    $account->Set_Account_By_ID($this->account_id)->Set_Account_User_List($profile_query_options) ;              

                    $data['user_list'] = $account->Get_Account_User_List() ;
                
                    $all_member_list_query_options['override_paging'] = 'yes' ; 
                    $all_member_list_query_options['minify_list'] = 'yes' ;
                    $all_member_list_query_options['skip_asset_mapping'] = 'yes' ;
                    $this->Set_Asset_Members_List($all_member_list_query_options) ; 
                    $data['asset_members_list'] = $this->Get_Asset_Members_List() ;                   

                    
                    // Check to see if recipient already a member of the asset
                    foreach ($data['user_list'] as $submitted_member) {

                        $found_member = Utilities::Deep_Array($data['asset_members_list'], 'member_id_value', $submitted_member['profile_id']) ; 

                        // Is recipient already a member of the asset?
                        if ($found_member) { // YES                                                          

                            switch ($options_array['allow_duplicates']) {
                                case 'true': // Asset allows duplicates? Meaning, do we allow multiple active subscriptions?
                                    $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                    break ;
                                default:

                                    // Is parent asset member status Inactive?
                                    switch ($found_member['metadata_id']) {
                                        case 51: // YES - Inactive Member  
                                            $submitted_member['found_member'] = $found_member ; 
                                            $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // We're only going to update the member and reset their subscription if they're currently inactive and duplicates are *not* allowed on the asset
                                            break ;
                                        case 50:  // NO - Member Currently Active
                                            $submitted_member['found_member'] = $found_member ; 
                                            $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                            break ; 
                                        default: // NO
                                            $submitted_member['found_member'] = $found_member ; 
                                            $this->asset_members_processing_list['dirty_invite_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                        }                                
                                }

                            } else { // NO
                                $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                }


                        unset($found_member) ; 
                        }

                    } else {
                    
                        // If profile_id's weren't submitted, then member_id's should have been submitted
                        // Turn the input into an array of member_id's
                        $member_id_array = Utilities::Process_Comma_Separated_String($input_array['member_id']) ; 
                        
                        $member_query_options['filter_by_member_id'] = $member_id_array ;
                        $member_query_options['filter_by_user_id'] = 'no' ; // Maybe don't keep this
                        $member_query_options['filter_by_account_id'] = 'no' ; // Maybe don't keep this
                        $member_query_options['override_paging'] = 'yes' ; 
                        $member_query_options['filter_by_asset_id'] = 'no' ; 
                        
                        // Get the submitted member records of the source asset (could be a different asset than the target)
                        $this->Set_Asset_Members_List($member_query_options) ; 
                        $data['member_list'] = $this->Get_Asset_Members_List() ; 
                    
                        // Now get all the current members of the target asset
                        $all_member_list_query_options['override_paging'] = 'yes' ; 
                        $all_member_list_query_options['minify_list'] = 'yes' ;
                        $all_member_list_query_options['skip_asset_mapping'] = 'yes' ; 
                        $this->Set_Asset_Members_List($all_member_list_query_options) ; 
                        $data['asset_members_list'] = $this->Get_Asset_Members_List() ;
                    
                    
                        // Parse through the submitted members and determine if they are current members of the target asset
                        foreach ($data['member_list'] as $submitted_member) {

                            $found_member = Utilities::Deep_Array($data['asset_members_list'], 'member_id', $submitted_member['member_id']) ; 

                            // Is recipient already a member of the asset?
                            if ($found_member) { // YES                                                          
                            
                                switch ($options_array['allow_duplicates']) {
                                    case 'true': // Asset allows duplicates? Meaning, do we allow multiple active subscriptions?
                                        $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                        break ;
                                    default:

                                        // Is parent asset member status Inactive?
                                        switch ($submitted_member['metadata_id']) {
                                            case 51: // YES - Inactive Member  
                                                $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // We're only going to update the member and reset their subscription if they're currently inactive and duplicates are *not* allowed on the asset
                                                break ;
                                            case 50:
                                                $submitted_member['found_member'] = $found_member ; 
                                                $this->asset_members_processing_list['clean_update_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                                break ; 
                                            default: // NO
                                                $submitted_member['found_member'] = $found_member ; 
                                                $this->asset_members_processing_list['dirty_invite_list'][] = $submitted_member ; // If the parent asset status is not Inactive, we'll attempt to update the sublist subscriptions if eligible
                                            }                                
                                    }

                                } else { // NO
                                    $this->asset_members_processing_list['clean_invite_list'][] = $submitted_member ; 
                                    }

                            unset($found_member) ;                   
                            }
                        }
                 
        
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['clean_invite_list'] as $invite) {
                    $this->asset_members_processing_list['clean_invite_list'][$i]['member_id_value'] = $invite['profile_id'] ; 
                    $i++ ; 
                    }
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['clean_update_list'] as $invite) {
                    if (!isset($invite['member_id'])) {
                        $this->asset_members_processing_list['clean_update_list'][$i] = $invite['found_member'] ; 
                        }                   
                    $this->asset_members_processing_list['clean_update_list'][$i]['member_id_value'] = $invite['profile_id'] ; 
                    $i++ ; 
                    }
                $i = 0 ; 
                foreach ($this->asset_members_processing_list['dirty_invite_list'] as $invite) {
                    $this->asset_members_processing_list['dirty_invite_list'][$i]['member_id_value'] = $invite['profile_id'] ; 
                    $i++ ; 
                    }                
                

                $this->Action_New_Asset_Members_Process($options_array) ; 
                $this->Action_Updated_Asset_Members_Process($options_array) ; 

                
                
                // Below this point needs work for Contacts
                // Would be nice to provide feedback details here on the ENTIRE array that was submitted
                if (!$new_invite['error']) { 
                    $this->Set_Alert_Response(162) ; // Successfully added/updated to list    
                    } else {
                        $this->Set_Alert_Response(163) ; // Error adding to the list
                        }
                
                
                $asset_members_processing_list = $this->Get_Asset_Members_Processing_List() ; 
                $data['clean_invite_list'] = $asset_members_processing_list['clean_invite_list'] ; 
                $data['clean_update_list'] = $asset_members_processing_list['clean_update_list'] ; 
                $data['dirty_invite_list'] = $asset_members_processing_list['dirty_invite_list'] ;  
                $data['invite_count'] = $asset_members_processing_list['invite_count'] ; 
                $data['update_count'] = $asset_members_processing_list['update_count'] ; 
                $data['automation_update_count'] = $asset_members_processing_list['automation_update_count'] ; 
                $data['options_inputs'] = $options_array ; 
                
                
                $this->test_data = $data ; 
                $this->asset_member = $data['clean_invite_list'][0] ;                 
                break ; 
            }
        
        
        
        // If member updates were made then update the asset timestamp_last_member_update 
        if ((count($data['invite_count']) > 0) OR (count($data['update_count']) > 0) OR (count($data['automation_update_count']) > 0)) {
            $update_target_asset = array(
                'auth_override' => 'yes',
                'asset_input_info' => array(
                    'rewrite_latest_history' => 'yes'
                    ),
                'asset_id' => $data['asset_record']['asset_id'],
                'visibility_id' => $data['asset_record']['visibility_id'],
                'timestamp_last_member_update' => time()-1
                ) ; 

            $this->Action_Update_Content($update_target_asset) ;
            }
        
        unset($data['asset'],$data['asset_record']) ; 
        unset($data['member_list']) ;
        unset($data['asset_members_list']) ;
//        $this->test_data = $data ;       

        return $this ; 
        }    
    
    
    // Process an array of potential new asset members and add them to the asset
    // Array of members comes from $this->asset_members_processing_list['clean_invite_list'] and must have at least one member to process
    // Submit an options array via 
    public function Action_New_Asset_Members_Process($options_array) {   
        
        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_init') ; 
        
        $continue = 0 ; 
        
        $data['asset_record'] = $this->Get_Asset() ; 
        
        if (isset($options_array['campaign_id'])) {
            $data['campaign_record'] = $this->Get_Campaign() ; 
            if ($data['campaign_record']['campaign_id'] != $options_array['campaign_id']) {
                $data['campaign_record'] = $this->Set_Campaign_By_ID($options_array['campaign_id'])->Get_Campaign() ; 
                }
            
            $content_asset_options = array(
                'skip_history' => 'yes'
                ) ; 

            $content_asset = new Asset() ; 
            $content_asset->Set_Replicate_User_Account_Profile($this) ;
            $content_asset->Set_Asset_By_ID($data['campaign_record']['asset_id'],$content_asset_options) ; 
            $data['content_asset'] = $content_asset->Get_Asset() ; 
            
            }
        
        $data['clean_invite_list'] = $this->asset_members_processing_list['clean_invite_list'] ;        
        $data['invite_count'] = $this->asset_members_processing_list['invite_count'] ; 
        $data['automation_update_count'] = $this->asset_members_processing_list['automation_update_count'] ; 
        
        
        if (count($data['clean_invite_list']) > 0) {
            $continue = 1 ; 
            }
        
        // If there are members to process, add them...
        if ($continue == 1) {
            
            // PROCESS CLEAN INVITE ARRAY
            $i = 0 ; 
            foreach ($data['clean_invite_list'] as $invite) {

                $attendee_input = array() ; 
                $attendee_input['asset_id'] = $data['asset_record']['asset_id'] ; 
                $attendee_input['member_id_value'] = $invite['member_id_value'] ; 
                $invite['member_id_value'] = $invite['member_id_value'] ; 
                $attendee_input['metadata_id'] = 50 ; // Parent List Member

                $akid_options['asset'] = $data['asset_record'] ; // Needed for AKIDS
                
                if (isset($options_array['override_list_status'])) {
                    $attendee_input['metadata_id'] = $options_array['override_list_status'] ; 
                    }

                if ($options_array['allow_duplicates'] == 'true') {
                    $attendee_input['force_create'] = 'yes' ; 
                    }

                
                
//                $data['clean_invite_list'][$i]['member']['parent'] = $data['new_member'] ;
                
//                if ($data['new_member']['insert_id']) { // Should need this anymore.
//                    $attendee_input['parent_member_id'] = $data['new_member']['insert_id'] ; 
//                    $invite['member_id'] = $attendee_input['parent_member_id'] ; // Member ID of the parent distro asset
//                    } else {
//                        $attendee_input['parent_member_id'] = $data['new_member']['original_results']['results']['member_id'] ; 
//                        $invite['member_id'] = $attendee_input['parent_member_id'] ; // Member ID of the parent distro asset
//                        }
                
                
                // *** EMAIL ***
                if ($data['asset_record']['distribution_sublist_email']) {                          

                    // Set asset member email sublist status (if eligible)
                    if ($options_array['subscribe_type_email'] == 'true') {
                        switch ($options_array['require_optin']) {
                            case 'true':
                            case 'double':                                 

                                switch ($invite['do_not_contact_email_address']) {
                                    case 1: // Email address cannot be contacted. Set as unsubscribed.
                                        $attendee_input['email_metadata_id'] = 47 ; // Sublist Subscription Unsubscribed
                                        break ; 
                                    default: // Default as Pending
                                        $attendee_input['email_metadata_id'] = 48 ; // Sublist Subscription Pending
                                    }

                                break ;
                            case 'single_welcome': 
                            case 'single': 
                            case 'false':    
                            default:

                                switch ($invite['do_not_contact_email_address']) {
                                    case 1: // Email address cannot be contacted. Set as unsubscribed.
                                        $attendee_input['email_metadata_id'] = 47 ; // Sublist Subscription Unsubscribed
                                        break ; 
                                    default: // Default as Subscribed
                                        $attendee_input['email_metadata_id'] = 46 ; // Sublist Subscription Subscribed
                                    }

                                break ; 
                            }

                        // Override distribution list status with a manually provided metadata_id
                        if (isset($options_array['override_sublist_status'])) {
                            $attendee_input['email_metadata_id'] = $options_array['override_sublist_status'] ; 
                            }

                        } else {
                            $attendee_input['email_metadata_id'] = 51 ; // Sublist Subscription Inactive
                            }


                    // If no email address exists, then override the sublist subscription as inactive.
                    if (!$invite['email_address']) {
                        $attendee_input['email_metadata_id'] = 51 ; // Sublist Subscription Inactive
                        }


                    // Determine if AKIDS should be created and invite sent...
                    $create_email_invite = 1 ; 
                    switch ($options_array['create_invite']) {
                        case 'no':
                            $create_email_invite = 0 ; 
                            break ; 
                        default: 
                            
                            // If subscription type does not warrant an invite
                            switch ($attendee_input['email_metadata_id']) {
                                case 51: // Inactive
                                case 47: // Unsubscribed   
                                    $create_email_invite = 0 ; 
                                    break ; 
                                default:
                                    
                                    break ; 
                                }
                            
                            break ; 
                        }
                    
                    $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_email_settings_processed') ; 
                    } // End of distribution_sublist_email processing 

                
                // *** SMS ***
                if ($data['asset_record']['distribution_sublist_sms']) {

                    // Set asset member sms sublist status to Pending (if eligible)
                    if ($options_array['subscribe_type_message'] == 'true') { 
                        
                        switch ($options_array['require_optin']) {
                            case 'true':
                            case 'double':                                 

                                switch ($invite['do_not_contact_phone_number']) {
                                    case 1: // Phone number cannot be contacted. Set as unsubscribed.
                                        $attendee_input['sms_metadata_id'] = 47 ; // List Subscription Unsubscribed
                                        break ; 
                                    default: // Default as Pending
                                        $attendee_input['sms_metadata_id'] = 48 ; // List Subscription Pending
                                    }

                                break ;
                            case 'false': 
                            case 'single':                               
                            case 'single_welcome': 
                            default:

                                switch ($invite['do_not_contact_phone_number']) {
                                    case 1: // Phone number cannot be contacted. Set as unsubscribed.
                                        $attendee_input['sms_metadata_id'] = 47 ; // List Subscription Unsubscribed
                                        break ; 
                                    default: // Default as Subscribed
                                        $attendee_input['sms_metadata_id'] = 46 ; // List Subscription Subscribed
                                    }

                                break ; 
                            }

                        // Override distribution list status with a manually provided metadata_id
                        if (isset($options_array['override_sublist_status'])) {
                            $attendee_input['sms_metadata_id'] = $options_array['override_sublist_status'] ; 
                            }

                        } else {
                            $attendee_input['sms_metadata_id'] = 51 ; // Sublist Subscription Inactive                            
                            }


                    // If no phone number exists, then override the sublist subscription as inactive.
                    if (!$invite['phone_number']) {
                        $attendee_input['sms_metadata_id'] = 51 ; // Sublist Subscription Inactive
                        }


                    // Determine if AKIDS should be created and invite sent...
                    $create_sms_invite = 1 ; 
                    switch ($options_array['create_invite']) {
                        case 'no':
                            $create_sms_invite = 0 ; 
                            break ; 
                        default: 
                            
                            // If subscription type does not warrant an invite
                            switch ($attendee_input['sms_metadata_id']) {
                                case 51: // Inactive
                                case 47: // Unsubscribed   
                                    $create_sms_invite = 0 ; 
                                    break ; 
                                default:
                                    
                                    break ; 
                                }
                            
                            break ; 
                        }
                    
                    $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_sms_settings_processed') ; 
                    }  // End of distribution_sublist_sms processing


                    // Process the email sublist subscription
                    // ** MOVE THIS
//                    $attendee_input['asset_id'] = $data['asset_record']['distribution_sublist_email']['asset_id'] ; 
//                    $attendee_input['member_id_value'] = $invite['member_id_value'] ;
                    $new_invite = $this->Create_Asset_Member($attendee_input) ;
                    $data['clean_invite_list'][$i]['member']['invite'] = $new_invite ;
                    $member_invited = 0 ;
                    $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_member_created') ; 
                    
                    if (isset($new_invite['insert_id'])) {
                        
                        $data['clean_invite_list'][$i]['member_id'] = $new_invite['insert_id'] ; 
                        $invite['member_id'] = $new_invite['insert_id'] ; 
                        
                        switch ($attendee_input['email_metadata_id']) {
                            case 46:
                            case 48:
                                $member_invited = 1 ;
                                $data['clean_invite_list'][$i]['member']['email']['invited'] = 1 ; 
                                break ; 
                            default:
                                $data['clean_invite_list'][$i]['member']['email']['invited'] = 0 ; 
                            }
                        switch ($attendee_input['sms_metadata_id']) {
                            case 46:
                            case 48:
                                $member_invited = 1 ;
                                $data['clean_invite_list'][$i]['member']['sms']['invited'] = 1 ; 
                                break ; 
                            default:
                                $data['clean_invite_list'][$i]['member']['sms']['invited'] = 0 ; 
                            }  
                        if ($member_invited == 1) {
                            $data['invite_count']++ ;
                            }
                        }
                                    
                
                    // Begin email Invitations
                    if (($options_array['subscribe_type_email'] == 'true') AND ($create_email_invite == 1)) {                         
                        // Create asset subscribe AKIDs for email address if eligible.
                        switch ($options_array['require_optin']) {
                            case 'false':  // No opt-in confirmation or welcome needed.
                            case 'single':

                                // SKIP
                                
                                break ;                                    
                            case 'true': // Double opt-in required
                            case 'double': 

                                switch ($invite['do_not_contact_email_address']) {
                                    case 1: // Email address cannot be contacted.
                                        // SKIP
                                        break ; 
                                    default: // 

                                        // Create action keys...
                                        $this->Action_Create_Member_Action_Keys(46,$invite,$akid_options) ;  // Subscribed as the primary
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_email_akids_created') ; 
                    
                                        $key_set = array() ; 
                                        foreach ($this->action_keys as $action_key) {
                                            $key_set[] = array(
                                                'action_key_id' => $action_key['action_key_id'],
                                                'action_id' => $action_key['action_id'],
                                                'key_id' => $action_key['key_id']
                                                ) ; 
                                            }
                                        $data['clean_invite_list'][$i]['member']['email']['action_keys'] = $key_set ; 


                                        // Create campaign to invite the member...
                                        $campaign_options = array(
                                            'asset_id' => $data['asset_record']['distribution_sublist_email']['list_email_invitation']['asset_id'],
                                            'delivery_asset_id' => $data['asset_record']['asset_id'],
                                            'campaign_title' => $data['asset_record']['asset_title'].' Invitation Email for '.$invite['email_address'].' ['.$invite['first_name'].' '.$invite['last_name'].']',
                                            'delivery_trigger' => 'delivery_list_custom',
                                            'recipients' => array(
                                                'to' => array(
                                                    'delivery_list_custom' =>  array(
                                                        'member_id' => array($invite['member_id']),
                                                        'email_metadata_id' => array(48), // Pending 
                                                        ),
                                                    ),
                                                ),                                            
                                            'action_keys' => $key_set,
                                            'campaign_status_name' => 'scheduled',
                                            'automation_status_name' => 'scheduled',
                                            'timestamp_next_action' => TIMESTAMP
                                            ) ; 

                                        if ($data['asset_record']['parent_asset_title']) { 
                                            
                                            switch ($data['asset_record']['type_name']) {
                                                case 'series_list_contacts': // Series campaigns use the list name as as the asset title
                                                case 'series_list_profiles': // Series campaigns use the list name as as the asset title
                                                    $campaign_options['personalization_overrides'] = array(
                                                        'asset_record' => array(
                                                            'asset_title' => $data['asset_record']['asset_title']
                                                            )
                                                        ) ;
                                                    break ;
                                                default:    
                                                    $campaign_options['personalization_overrides'] = array(
                                                        'asset_record' => array(
                                                            'asset_title' => $data['asset_record']['parent_asset_title']
                                                            )
                                                        ) ;                                                    
                                                }
 
                                            } else {
                                                $campaign_options['personalization_overrides'] = array(
                                                    'asset_record' => array(
                                                        'asset_title' => $data['asset_record']['asset_title']
                                                        )
                                                    ) ;                                                        
                                                }

                                        $create_campaign = $this->Action_Create_Campaign($campaign_options) ; 
                                        $data['clean_invite_list'][$i]['member']['email']['campaign']['campaign_id'] = $this->Get_Campaign()['campaign_id'] ; 
                                        $data['clean_invite_list'][$i]['member']['email']['automation']['automation_id'] = $this->Get_Automation()['automation_id'] ;
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_email_double_opt_campaign_created') ; 
                                        
                                        // Archive new campaign
                                        $campaign_update_query = array(
                                            'campaign_id' => $data['clean_invite_list'][$i]['member']['email']['campaign']['campaign_id'],
                                            'archive_campaign' => 'yes'
                                            ) ; 
                                        $this->Action_Update_Campaign($campaign_update_query) ;
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_email_double_opt_campaign_updated') ; 
                                    }                                



                                break ;
                            case 'single_welcome': 
                            default:    

                                switch ($invite['do_not_contact_email_address']) {
                                    case 1: // Email address cannot be contacted.
                                        // SKIP
                                        break ; 
                                    default: // 

                                        // Create action keys...
                                        $this->Action_Create_Member_Action_Keys(47,$invite,$akid_options) ;  // Unsubscribed as the primary
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_email_akids_created') ; 
                    
                                        $key_set = array() ; 
                                        foreach ($this->action_keys as $action_key) {
                                            $key_set[] = array(
                                                'action_key_id' => $action_key['action_key_id'],
                                                'action_id' => $action_key['action_id'],
                                                'key_id' => $action_key['key_id']
                                                ) ; 
                                            }
                                        $data['clean_invite_list'][$i]['member']['email']['action_keys'] = $key_set ; 


                                        // Create campaign to invite the member...
                                        $campaign_options = array(
                                            'asset_id' => $data['asset_record']['distribution_sublist_email']['list_email_welcome']['asset_id'],
                                            'delivery_asset_id' => $data['asset_record']['asset_id'],
                                            'campaign_title' => $data['asset_record']['asset_title'].' Welcome Email for '.$invite['email_address'].' ['.$invite['first_name'].' '.$invite['last_name'].']',
                                            'delivery_trigger' => 'delivery_list_custom',
                                            'recipients' => array(
                                                'to' => array(
                                                    'delivery_list_custom' =>  array(
                                                        'member_id' => array($invite['member_id']),
                                                        'email_metadata_id' => array(46), // Subscribed 
                                                        ),
                                                    ),
                                                ), 
                                            'action_keys' => $key_set,
                                            'campaign_status_name' => 'scheduled',
                                            'automation_status_name' => 'scheduled',
                                            'timestamp_next_action' => TIMESTAMP
                                            ) ; 

                                        if ($data['asset_record']['parent_asset_title']) {
                                            switch ($data['asset_record']['type_name']) {
                                                case 'series_list_contacts': // Series campaigns use the list name as as the asset title
                                                case 'series_list_profiles': // Series campaigns use the list name as as the asset title
                                                    $campaign_options['personalization_overrides'] = array(
                                                        'asset_record' => array(
                                                            'asset_title' => $data['asset_record']['asset_title']
                                                            )
                                                        ) ;
                                                    break ;
                                                default:    
                                                    $campaign_options['personalization_overrides'] = array(
                                                        'asset_record' => array(
                                                            'asset_title' => $data['asset_record']['parent_asset_title']
                                                            )
                                                        ) ;                                                    
                                                }
                                            } else {
                                                $campaign_options['personalization_overrides'] = array(
                                                    'asset_record' => array(
                                                        'asset_title' => $data['asset_record']['asset_title']
                                                        )
                                                    ) ;                                                        
                                                }

                                        $this->Action_Create_Campaign($campaign_options) ; 
                                        $data['clean_invite_list'][$i]['member']['email']['campaign']['campaign_id'] = $this->Get_Campaign()['campaign_id'] ; 
                                        $data['clean_invite_list'][$i]['member']['email']['automation']['automation_id'] = $this->Get_Automation()['automation_id'] ;
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_email_welcome_campaign_created') ; 
                                        
                                        // Archive new campaign
                                        $campaign_update_query = array(
                                            'campaign_id' => $data['clean_invite_list'][$i]['member']['email']['campaign']['campaign_id'],
                                            'archive_campaign' => 'yes'
                                            ) ; 
                                        
                                        $this->Action_Update_Campaign($campaign_update_query) ;
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_email_welcome_campaign_updated') ;                                         
                                    }
                            }
                        } // End of email invitation                
                
                
                    // Begin SMS invites
                    // Create asset subscribe AKIDs for phone number if eligible.
                    if (($options_array['subscribe_type_message'] == 'true') AND ($create_sms_invite == 1)) {                      
                        switch ($options_array['require_optin']) {
                            case 'false':  // No opt-in confirmation or welcome needed.
                            case 'single':

                                // SKIP

                                break ;                                    
                            case 'true': // Double opt-in required
                            case 'double': 

                                switch ($invite['do_not_contact_phone_number']) {
                                    case 1: // Phone number cannot be contacted.
                                        // SKIP
                                        break ; 
                                    default: // 

                                        // Create action keys...
                                        $this->Action_Create_Member_Action_Keys(46,$invite,$akid_options) ;  // Subscribed as the primary
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_sms_akids_created') ;
                                        
                                        $key_set = array() ; 
                                        foreach ($this->action_keys as $action_key) {
                                            $key_set[] = array(
                                                'action_key_id' => $action_key['action_key_id'],
                                                'action_id' => $action_key['action_id'],
                                                'key_id' => $action_key['key_id']
                                                ) ; 
                                            }
                                        $data['clean_invite_list'][$i]['member']['sms']['action_keys'] = $key_set ; 


                                        // Create campaign to invite the member...
                                        $campaign_options = array(
                                            'asset_id' => $data['asset_record']['distribution_sublist_sms']['list_sms_invitation']['asset_id'],
                                            'delivery_asset_id' => $data['asset_record']['asset_id'],
                                            'campaign_title' => $data['asset_record']['asset_title'].' Invitation Text for '.$invite['phone_number'].' ['.$invite['first_name'].' '.$invite['last_name'].']',
                                            'delivery_trigger' => 'delivery_list_custom',
                                            'recipients' => array(
                                                'to' =>  array(
                                                    'delivery_list_custom' => array(
                                                        'member_id' => array($invite['member_id']),
                                                        'sms_metadata_id' => array(48), // Pending
                                                        ),                                                    
                                                    ),                                              
                                                ),
                                            'action_keys' => $key_set,
                                            'campaign_status_name' => 'scheduled',
                                            'automation_status_name' => 'scheduled',
                                            'timestamp_next_action' => TIMESTAMP
                                            ) ; 

                                        if ($data['asset_record']['parent_asset_title']) {
                                            switch ($data['asset_record']['type_name']) {
                                                case 'series_list_contacts': // Series campaigns use the list name as as the asset title
                                                case 'series_list_profiles': // Series campaigns use the list name as as the asset title
                                                    $campaign_options['personalization_overrides'] = array(
                                                        'asset_record' => array(
                                                            'asset_title' => $data['asset_record']['asset_title']
                                                            )
                                                        ) ;
                                                    break ;
                                                default:    
                                                    $campaign_options['personalization_overrides'] = array(
                                                        'asset_record' => array(
                                                            'asset_title' => $data['asset_record']['parent_asset_title']
                                                            )
                                                        ) ;                                                    
                                                }
                                            } else {
                                                $campaign_options['personalization_overrides'] = array(
                                                    'asset_record' => array(
                                                        'asset_title' => $data['asset_record']['asset_title']
                                                        )
                                                    ) ;                                                        
                                                }

                                        $this->Action_Create_Campaign($campaign_options) ; 
                                        $data['clean_invite_list'][$i]['member']['sms']['campaign']['campaign_id'] = $this->Get_Campaign()['campaign_id'] ; 
                                        $data['clean_invite_list'][$i]['member']['sms']['automation']['automation_id'] = $this->Get_Automation()['automation_id'] ;
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_sms_double_opt_campaign_created') ;
                                        
                                        // Archive new campaign
                                        $campaign_update_query = array(
                                            'campaign_id' => $data['clean_invite_list'][$i]['member']['sms']['campaign']['campaign_id'],
                                            'archive_campaign' => 'yes'
                                            ) ; 
                                        $this->Action_Update_Campaign($campaign_update_query) ;                                        
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_sms_double_opt_campaign_updated') ;
                                    }                                



                                break ;
                            case 'single_welcome': 
                            default:    

                                switch ($invite['do_not_contact_phone_number']) {
                                    case 1: // Phone number cannot be contacted.
                                        // SKIP
                                        break ; 
                                    default: // 

                                        // Create action keys...
                                        $this->Action_Create_Member_Action_Keys(47,$invite,$akid_options) ;  // Unsubscribed as the primary
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_sms_akids_created') ;
                                        
                                        $key_set = array() ; 
                                        foreach ($this->action_keys as $action_key) {
                                            $key_set[] = array(
                                                'action_key_id' => $action_key['action_key_id'],
                                                'action_id' => $action_key['action_id'],
                                                'key_id' => $action_key['key_id']
                                                ) ; 
                                            }
                                        $data['clean_invite_list'][$i]['member']['sms']['action_keys'] = $key_set ; 


                                        // Create campaign to invite the member...
                                        $campaign_options = array(
                                            'asset_id' => $data['asset_record']['distribution_sublist_sms']['list_sms_welcome']['asset_id'],
                                            'delivery_asset_id' => $data['asset_record']['asset_id'],
                                            'campaign_title' => $data['asset_record']['asset_title'].' Welcome Text for '.$invite['phone_number'].' ['.$invite['first_name'].' '.$invite['last_name'].']',
                                            'delivery_trigger' => 'delivery_list_custom',
                                            'recipients' => array(
                                                'to' =>  array(
                                                    'delivery_list_custom' => array(
                                                        'member_id' => array($invite['member_id']),
                                                        'sms_metadata_id' => array(46), // Subscribed
                                                        ),                                                    
                                                    ),                                              
                                                ),
                                            'action_keys' => $key_set,
                                            'campaign_status_name' => 'scheduled',
                                            'automation_status_name' => 'scheduled',
                                            'timestamp_next_action' => TIMESTAMP
                                            ) ; 

                                        if ($data['asset_record']['parent_asset_title']) {
                                            switch ($data['asset_record']['type_name']) {
                                                case 'series_list_contacts': // Series campaigns use the list name as as the asset title
                                                case 'series_list_profiles': // Series campaigns use the list name as as the asset title
                                                    $campaign_options['personalization_overrides'] = array(
                                                        'asset_record' => array(
                                                            'asset_title' => $data['asset_record']['asset_title']
                                                            )
                                                        ) ;
                                                    break ;
                                                default:    
                                                    $campaign_options['personalization_overrides'] = array(
                                                        'asset_record' => array(
                                                            'asset_title' => $data['asset_record']['parent_asset_title']
                                                            )
                                                        ) ;                                                    
                                                }
                                            } else {
                                                $campaign_options['personalization_overrides'] = array(
                                                    'asset_record' => array(
                                                        'asset_title' => $data['asset_record']['asset_title']
                                                        )
                                                    ) ;                                                        
                                                } 

                                        $this->Action_Create_Campaign($campaign_options) ; 
                                        $data['clean_invite_list'][$i]['member']['sms']['campaign']['campaign_id'] = $this->Get_Campaign()['campaign_id'] ; 
                                        $data['clean_invite_list'][$i]['member']['sms']['automation']['automation_id'] = $this->Get_Automation()['automation_id'] ;
                                        
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_sms_welcome_campaign_created') ;
                                        
                                        // Archive new campaign
                                        $campaign_update_query = array(
                                            'campaign_id' => $data['clean_invite_list'][$i]['member']['sms']['campaign']['campaign_id'],
                                            'archive_campaign' => 'yes'
                                            ) ; 
                                        $this->Action_Update_Campaign($campaign_update_query) ; 
                                        $this->Set_Model_Timings('Asset.Action_New_Asset_Members_Process_sms_welcome_campaign_updated') ;
                                    }
                            }                        
                        } // End SMS invites                
                

                // Create an automation step if the content asset is a sequence
                // Can only create this step if there is no opt in required
                // Otherwise, automation step should be created when recipient first opts in
                if (isset($options_array['campaign_id']) AND (($options_array['subscribe_type_message'] == 'true') OR ($options_array['subscribe_type_email'] == 'true'))) {

                    switch ($data['campaign_record']['content_asset_type_name']) { 
                        case 'series':                    
                    
                            // Create a campaign if there is a parent_asset and it's a sequence and either subscription status is subscribed
                            $asset_member_query['filter_by_user_id'] = 'no'  ;
                            $asset_member_query['filter_by_account_id'] = 'no'  ;
                            $asset_member_query['filter_by_member_id'] = $invite['member_id'] ; 
                            $this->Set_Asset_Members_List($asset_member_query) ; 
                            $data['active_member_list'] = $this->Get_Asset_Members_List() ;
                            $data['active_member'] = $data['active_member_list'][0] ;

                            switch ($options_array['require_optin']) {
                                case 'true':
                                case 'double': 
                                    
                                    // If an campaign ID doesn't exist, create one now
                                    // Do not add automation id until member opts in
                                    if (($data['active_member']['campaign_id'] == 0) AND ($data['content_asset']['asset_structure_count'] > 0)) {
                                    
                                        // Update the member_id with the new campaign_id
                                        $update_series_member = array(
                                            'member_id' => $invite['member_id'],
                                            'campaign_id' => $data['campaign_record']['campaign_id'],
                                            'member_status_id' => '3' // Draft
                                            ) ; 

                                        $data['update_series_member'] = $this->Update_Asset_Member($update_series_member) ; 
                                        $data['automation_update_count']++ ; 
                                        }
                                    
                                    break ; 
                                case 'false':  // No opt-in confirmation or welcome needed.
                                case 'single':
                                case 'single_welcome': 

                                    // If an automation ID doesn't exist, create one now
                                    if (($data['active_member']['automation_id'] == 0) AND ($data['content_asset']['asset_structure_count'] > 0)) {
                                    
                                        $series_automation = new Asset() ; 
                                        $series_automation->Set_Replicate_User_Account_Profile($this) ;            

                                        $automation_options['campaign_id'] = $data['campaign_record']['campaign_id'] ; 
                                        $automation_options['campaign_position'] = '1.00' ; 
                                        $automation_options['automation_status_id'] = '4' ; 
                                        $automation_options['timestamp_next_action'] = $data['content_asset']['structure'][0]['delay'] + $this->Get_Scheduled_Automation_Timestamp() ; 

                                        // Build in a 2 minute delay for the first automation step IF a welcome is being sent (so they don't overlap)
                                        if ($options_array['require_optin'] == 'single_welcome') {
                                            $automation_options['timestamp_next_action'] = $automation_options['timestamp_next_action'] + 120 ; 
                                            }
                                        
                                        $create_series_automation = $series_automation->Create_Automation($automation_options) ;


                                        // Update the member_id with the new automation_id and campaign_id
                                        $update_series_member = array(
                                            'member_id' => $invite['member_id'],
                                            'campaign_id' => $data['campaign_record']['campaign_id'],
                                            'automation_id' => $create_series_automation['insert_id'],
                                            'member_status_id' => '6' // In Progess
                                            ) ; 

                                        $data['update_series_member'] = $this->Update_Asset_Member($update_series_member) ; 
                                        $data['automation_update_count']++ ; 
                                        $this->Set_Scheduled_Automation_Timestamp(2) ;
                                        }
                                    
                                } // End opt in switch 
                            
                            break ; 
                        } // End content type switch                            
                    }
                

                
                $i++ ; 
                unset($new_invite,$attendee_input) ; 
                } // END: PROCESS CLEAN INVITE ARRAY            
            
            
            
            $this->asset_members_processing_list['clean_invite_list']  = $data['clean_invite_list'] ;  
            $this->asset_members_processing_list['invite_count']  = $data['invite_count'] ;
            $this->asset_members_processing_list['automation_update_count']  = $data['automation_update_count'] ;
//            $this->test_data = $data ; 

            }
        
        return $this ; 
        }
    
    
    // This is intended to add new members... 
    public function Action_Updated_Asset_Members_Process($options_array) {
        
        $continue = 0 ; 
        
        $data['asset_record'] = $this->Get_Asset() ; 
       
        if (isset($options_array['campaign_id'])) {
            $data['campaign_record'] = $this->Get_Campaign() ; 
            if ($data['campaign_record']['campaign_id'] != $options_array['campaign_id']) {
                $data['campaign_record'] = $this->Set_Campaign_By_ID($options_array['campaign_id'])->Get_Campaign() ; 
                }
            
            $content_asset_options = array(
                'skip_history' => 'yes'
                ) ; 

            $content_asset = new Asset() ; 
            $content_asset->Set_Replicate_User_Account_Profile($this) ;
            $content_asset->Set_Asset_By_ID($data['campaign_record']['asset_id'],$content_asset_options) ; 
            $data['content_asset'] = $content_asset->Get_Asset() ;             
            }
        
        $data['clean_update_list'] = $this->asset_members_processing_list['clean_update_list'] ;
        $data['update_count'] = $this->asset_members_processing_list['update_count'] ;
        $data['automation_update_count'] = $this->asset_members_processing_list['automation_update_count'] ;
        
            
        if ($data['clean_update_list'] > 0) {
            $continue = 1 ; 
            }
        

        // If there are members to process, add them...
        if ($continue == 1) {
        
            // PROCESS CLEAN UPDATE ARRAY
            $i = 0 ; 
            foreach ($data['clean_update_list'] as $invite) { 
                
                $member_updated = 0 ; 
                $akid_options['asset'] = $data['asset_record'] ; 

                switch ($invite['metadata_id']) {
                    case 51: // Currently Inactive
                        // Reset the metadata_id of parent member to Active Member and the campaign_id to 0 so a new sequence can be added
                        $attendee_parent_input['metadata_id'] = 50 ; // Parent List Member 
                        $attendee_parent_input['campaign_id'] = 0 ; // Reset to 0 as we're adding a new campaign
                        $this->Action_Asset_Member_Update($invite['member_id'],$attendee_parent_input) ; 
                        $data['clean_update_list'][$i]['member']['parent'] = $this->asset_query_result ; 
                        break ;
                    case 50: // Currently active but may not have a campaign started yet (probably pending status on the communication modes)

                        break ; 
                    }

                
                // *** EMAIL ***
                if ($data['asset_record']['distribution_sublist_email']) { 

                    // Provide an admin override that temporarily sets the current subscription status to an eligible critera
                    switch ($invite['email_metadata_id']) {
                        case 47: // Email Sublist Current Status - Unsubscribed
                            if ($this->Action_Test_Authorization_Control('admin_contact_edit') == 'allow') {
                                $invite['email_metadata_id'] = 51 ; // Temporary inactive
                                }                        
                            break ;                            
                        }
                    

                    // Only process sublist updates if the current status matches certain criteria
                    switch ($invite['email_metadata_id']) { 
                        case 51: // Email Sublist Current Status - Inactive
                        case 48: // Email Sublist Current Status - Pending    

                            // Reset asset member email sublist status (if eligible)
                            if ($options_array['subscribe_type_email'] == 'true') {
                                switch ($options_array['require_optin']) { 
                                    case 'true':
                                    case 'double':                                 

                                        switch ($invite['do_not_contact_email_address']) {
                                            case 1: // Email address cannot be contacted. Set as unsubscribed.
                                                $attendee_input['email_metadata_id'] = 47 ; // Sublist Subscription Unsubscribed
                                                break ; 
                                            default: // Default as Pending
                                                $attendee_input['email_metadata_id'] = 48 ; // Sublist Subscription Pending
                                            }

                                        break ;
                                    case 'single_welcome': 
                                    case 'single': 
                                    case 'false':    
                                    default:

                                        switch ($invite['do_not_contact_email_address']) {
                                            case 1: // Email address cannot be contacted. Set as unsubscribed.
                                                $attendee_input['email_metadata_id'] = 47 ; // Sublist Subscription Unsubscribed
                                                break ; 
                                            default: // Default as Subscribed
                                                $attendee_input['email_metadata_id'] = 46 ; // Sublist Subscription Subscribed 
                                            }

                                        break ; 
                                    }

                                // Override distribution list status with a manually provided metadata_id
                                if (isset($options_array['override_sublist_status'])) {
                                    $attendee_input['email_metadata_id'] = $options_array['override_sublist_status'] ; 
                                    }


                                // If no email address exists, then override the sublist subscription as inactive.
                                if (!$invite['email_address']) {
                                    $attendee_input['email_metadata_id'] = 51 ; // Sublist Subscription Inactive
                                    }

                                // Process the email sublist subscription
                                // Metadata_id will come from above
                                if ($invite['email_metadata_id'] != $attendee_input['email_metadata_id']) {
                                    $this->Action_Asset_Member_Update($invite['member_id'],$attendee_input) ; 
                                    $data['clean_update_list'][$i]['member']['email']['result'] = $this->asset_query_result ;                                      
                                    $data['clean_update_list'][$i]['member']['email']['updated'] = 1 ; 
                                    $member_updated = 1 ; 
                                    } else {
                                        $data['clean_update_list'][$i]['member']['email']['result'] = 'No update' ;
                                        $data['clean_update_list'][$i]['member']['email']['updated'] = 0 ; 
                                        }



                                // Determine if AKIDS should be created and invite sent...
                                $create_email_invite = 1 ; 
                                switch ($options_array['create_invite']) {
                                    case 'no':
                                        $create_email_invite = 0 ; 
                                        break ; 
                                    default: 

                                        // If subscription type does not warrant an invite
                                        switch ($attendee_input['email_metadata_id']) {
                                            case 51: // Inactive
                                            case 47: // Unsubscribed   
                                                $create_email_invite = 0 ; 
                                                break ; 
                                            default:

                                                break ; 
                                            }

                                        break ; 
                                    }

                                // Only re-create AKIDs if the last update was made more than xxx days ago...
                                // Need to provide a dedicated field for messaging type update timestamps
//                                if ($invite['timestamp_asset_updated_distribution_sublist_email_compiled']['relative_days'] > -1) {
//                                    $create_email_invite = 0 ; 
//                                    }

                                // Create asset subscribe AKIDs for email address if eligible.
                                if ($create_email_invite == 1) {

                                    switch ($options_array['require_optin']) {
                                        case 'false':  // No opt-in confirmation or welcome needed.
                                        case 'single':

                                            // SKIP

                                            break ;                                                 
                                        case 'true': // Double opt-in required
                                        case 'double': 

                                            switch ($invite['do_not_contact_email_address']) {
                                                case 1: // Email address cannot be contacted.
                                                    // SKIP
                                                    break ; 
                                                default: // 

                                                    // Create action keys...
                                                    $this->Action_Create_Member_Action_Keys(46,$invite,$akid_options) ;  // Subscribed as the primary

                                                    $key_set = array() ; 
                                                    foreach ($this->action_keys as $action_key) {
                                                        $key_set[] = array(
                                                            'action_key_id' => $action_key['action_key_id'],
                                                            'action_id' => $action_key['action_id'],
                                                            'key_id' => $action_key['key_id']
                                                            ) ; 
                                                        }
                                                    $data['clean_update_list'][$i]['member']['email']['action_keys'] = $key_set ; 


                                                    // Create campaign to invite the member...
                                                    $campaign_options = array(
                                                        'asset_id' => $data['asset_record']['distribution_sublist_email']['list_email_invitation']['asset_id'],
                                                        'delivery_asset_id' => $data['asset_record']['asset_id'],
                                                        'campaign_title' => $data['asset_record']['asset_title'].' Invitation Email for '.$invite['email_address'].' ['.$invite['first_name'].' '.$invite['last_name'].']',
                                                        'delivery_trigger' => 'delivery_list_custom',
                                                        'recipients' => array(
                                                            'to' => array(
                                                                'delivery_list_custom' =>  array(
                                                                    'member_id' => array($invite['member_id']),
                                                                    'email_metadata_id' => array(48,51), // Pending, Inactive 
                                                                    ),
                                                                ),
                                                            ), 
                                                        'action_keys' => $key_set,
                                                        'campaign_status_name' => 'scheduled',
                                                        'automation_status_name' => 'scheduled',
                                                        'timestamp_next_action' => TIMESTAMP
                                                        ) ; 

                                                    if ($data['asset_record']['parent_asset_title']) {
                                                        switch ($data['asset_record']['type_name']) {
                                                            case 'series_list_contacts': // Series campaigns use the list name as as the asset title
                                                            case 'series_list_profiles': // Series campaigns use the list name as as the asset title
                                                                $campaign_options['personalization_overrides'] = array(
                                                                    'asset_record' => array(
                                                                        'asset_title' => $data['asset_record']['asset_title']
                                                                        )
                                                                    ) ;
                                                                break ;
                                                            default:    
                                                                $campaign_options['personalization_overrides'] = array(
                                                                    'asset_record' => array(
                                                                        'asset_title' => $data['asset_record']['parent_asset_title']
                                                                        )
                                                                    ) ;                                                    
                                                            }
                                                        } else {
                                                            $campaign_options['personalization_overrides'] = array(
                                                                'asset_record' => array(
                                                                    'asset_title' => $data['asset_record']['asset_title']
                                                                    )
                                                                ) ;                                                        
                                                            }

                                                    $this->Action_Create_Campaign($campaign_options) ; 

                                                    $data['clean_update_list'][$i]['member']['email']['campaign']['campaign_id'] = $this->Get_Campaign()['campaign_id'] ; 
                                                    $data['clean_update_list'][$i]['member']['email']['automation']['automation_id'] = $this->Get_Automation()['automation_id'] ;
                                                    
                                                    // Archive new campaign
                                                    $campaign_update_query = array(
                                                        'campaign_id' => $data['clean_update_list'][$i]['member']['email']['campaign']['campaign_id'],
                                                        'archive_campaign' => 'yes'
                                                        ) ; 
                                                    $this->Action_Update_Campaign($campaign_update_query) ; 
                                                    
                                                }                                



                                            break ;
                                        case 'single_welcome': 
                                        default:    

                                            switch ($invite['do_not_contact_email_address']) {
                                                case 1: // Email address cannot be contacted.
                                                    // SKIP
                                                    break ; 
                                                default: // 

                                                    // Create action keys...
                                                    $this->Action_Create_Member_Action_Keys(47,$invite,$akid_options) ;  // Unsubscribed as the primary

                                                    $key_set = array() ; 
                                                    foreach ($this->action_keys as $action_key) {
                                                        $key_set[] = array(
                                                            'action_key_id' => $action_key['action_key_id'],
                                                            'action_id' => $action_key['action_id'],
                                                            'key_id' => $action_key['key_id']
                                                            ) ; 
                                                        }
                                                    $data['clean_update_list'][$i]['member']['email']['action_keys'] = $key_set ; 

                                                    
                                                    
                                                    // Create campaign to invite the member...
                                                    $campaign_options = array(
                                                        'asset_id' => $data['asset_record']['distribution_sublist_email']['list_email_welcome']['asset_id'],
                                                        'delivery_asset_id' => $data['asset_record']['asset_id'],
                                                        'campaign_title' => $data['asset_record']['asset_title'].' Welcome Email for '.$invite['email_address'].' ['.$invite['first_name'].' '.$invite['last_name'].']',
                                                        'delivery_trigger' => 'delivery_list_custom',
                                                        'recipients' => array(
                                                            'to' => array(
                                                                'delivery_list_custom' =>  array(
                                                                    'member_id' => array($invite['member_id']),
                                                                    'email_metadata_id' => array(46), // Subscribed 
                                                                    ),
                                                                ),
                                                            ), 
                                                        'action_keys' => $key_set,
                                                        'campaign_status_name' => 'scheduled',
                                                        'automation_status_name' => 'scheduled',
                                                        'timestamp_next_action' => TIMESTAMP
                                                        ) ; 

                                                    if ($data['asset_record']['parent_asset_title']) {
                                                        switch ($data['asset_record']['type_name']) {
                                                            case 'series_list_contacts': // Series campaigns use the list name as as the asset title
                                                            case 'series_list_profiles': // Series campaigns use the list name as as the asset title
                                                                $campaign_options['personalization_overrides'] = array(
                                                                    'asset_record' => array(
                                                                        'asset_title' => $data['asset_record']['asset_title']
                                                                        )
                                                                    ) ;
                                                                break ;
                                                            default:    
                                                                $campaign_options['personalization_overrides'] = array(
                                                                    'asset_record' => array(
                                                                        'asset_title' => $data['asset_record']['parent_asset_title']
                                                                        )
                                                                    ) ;                                                    
                                                            }
                                                        } else {
                                                            $campaign_options['personalization_overrides'] = array(
                                                                'asset_record' => array(
                                                                    'asset_title' => $data['asset_record']['asset_title']
                                                                    )
                                                                ) ;                                                        
                                                            }
                                                    

                                                    $this->Action_Create_Campaign($campaign_options) ; 

                                                    $data['clean_update_list'][$i]['member']['email']['campaign']['campaign_id'] = $this->Get_Campaign()['campaign_id'] ; 
                                                    $data['clean_update_list'][$i]['member']['email']['automation']['automation_id'] = $this->Get_Automation()['automation_id'] ; 
                                                    
                                                    
                                                    // Archive new campaign
                                                    $campaign_update_query = array(
                                                        'campaign_id' => $data['clean_update_list'][$i]['member']['email']['campaign']['campaign_id'],
                                                        'archive_campaign' => 'yes'
                                                        ) ; 
                                                    $this->Action_Update_Campaign($campaign_update_query) ;                                                    
                                                }
                                        }                                    
                                    } 
                                }
                            break ; 
                        case 46: // Email Sublist Current Status - Subscribed
                        case 47: // Email Sublist Current Status - Unsubscribed
                            
                            // Reset sublist status if eligible
                            if ($options_array['subscribe_type_email'] == 'true') { 
                                
                                // Override distribution list status with a manually provided metadata_id
                                // This would be used when an VERIFIED recipient changes their subscription status manually
                                switch ($options_array['override_sublist_status']) {
                                    case 46: // Update to subscribed
                                    case 47: // Update to unsubscribed
                                    case 51: // Update to inactive
                                        $attendee_input['email_metadata_id'] = $options_array['override_sublist_status'] ; 
                                        $this->Action_Asset_Member_Update($invite['member_id'],$attendee_input) ; 
                                        $data['clean_update_list'][$i]['member']['email']['result'] = $this->asset_query_result ; 
                                        $data['clean_update_list'][$i]['member']['email']['updated'] = 1 ; 
                                        $member_updated = 1 ; 
                                        break ; 
                                    default:
                                        $data['clean_update_list'][$i]['member']['email']['result'] = 'No update' ;
                                        $data['clean_update_list'][$i]['member']['email']['updated'] = 0 ; 
                                    }                                
                                }
                            
                            break ; 
                        case 49: // Email Sublist Current Status - Cleaned
                            
                            // Reset sublist status if eligible
                            if ($options_array['subscribe_type_email'] == 'true') { 
                                
                                // Override distribution list status with a manually provided metadata_id
                                // This would be used when an VERIFIED recipient changes their subscription status manually
                                switch ($options_array['override_sublist_status']) {
                                    case 46: // Update to subscribed
                                        
                                        $attendee_input['email_metadata_id'] = $options_array['override_sublist_status'] ; 
                                        $this->Action_Asset_Member_Update($invite['member_id'],$attendee_input) ; 
                                        $data['clean_update_list'][$i]['member']['email']['result'] = $this->asset_query_result ; 
                                        $data['clean_update_list'][$i]['member']['email']['updated'] = 1 ; 
                                        $member_updated = 1 ; 
                                        
                                        $clean_params = array(
                                            'email_address' => $invite['email_address']
                                            ) ;  
                                        
                                        $email = new Email() ;
                                        $email->Set_Replicate_User_Account_Profile($this)->Connect() ; 
                                        $email->Action_Delete_Suppression($clean_params) ; 
                                        
                                        break ; 
                                    case 47: // Update to unsubscribed
                                    case 51: // Update to inactive
                                        $attendee_input['email_metadata_id'] = $options_array['override_sublist_status'] ; 
                                        $this->Action_Asset_Member_Update($invite['member_id'],$attendee_input) ; 
                                        $data['clean_update_list'][$i]['member']['email']['result'] = $this->asset_query_result ; 
                                        $data['clean_update_list'][$i]['member']['email']['updated'] = 1 ; 
                                        $member_updated = 1 ; 
                                        break ; 
                                    default:
                                        $data['clean_update_list'][$i]['member']['email']['result'] = 'No update' ;
                                        $data['clean_update_list'][$i]['member']['email']['updated'] = 0 ; 
                                    }                                
                                }
                            
                            break ; 
                        } 

                    } // END: Processing Email Sublist


                
                // *** SMS ***
                if ($data['asset_record']['distribution_sublist_sms']) { 

                    
                    // Provide an admin override that temporarily sets the current subscription status to an eligible critera
                    switch ($invite['sms_metadata_id']) {
                        case 47: // SMS Sublist Current Status - Unsubscribed
                            if ($this->Action_Test_Authorization_Control('admin_contact_edit') == 'allow') {
                                $invite['sms_metadata_id'] = 51 ; // Temporary inactive
                                }                        
                            break ;     
                        }
                    
                    // Only process sublist updates if the current status matches certain criteria
                    switch ($invite['sms_metadata_id']) {
                        case 51: // SMS Sublist Current Status - Inactive
                        case 48: // SMS Sublist Current Status - Pending    

                            // Rest asset member sms sublist status (if eligible)
                            if ($options_array['subscribe_type_message'] == 'true') {
                                switch ($options_array['require_optin']) {
                                    case 'true':
                                    case 'double':                                 

                                        switch ($invite['do_not_contact_phone_number']) {
                                            case 1: // Phone number cannot be contacted. Set as unsubscribed.
                                                $attendee_input['sms_metadata_id'] = 47 ; // Sublist Subscription Unsubscribed
                                                break ; 
                                            default: // Default as Pending
                                                $attendee_input['sms_metadata_id'] = 48 ; // Sublist Subscription Pending
                                            }

                                        break ;
                                    case 'single_welcome': 
                                    case 'single': 
                                    case 'false':    
                                    default:

                                        switch ($invite['do_not_contact_phone_number']) {
                                            case 1: // Phone number cannot be contacted. Set as unsubscribed.
                                                $attendee_input['sms_metadata_id'] = 47 ; // Sublist Subscription Unsubscribed
                                                break ; 
                                            default: // Default as Subscribed
                                                $attendee_input['sms_metadata_id'] = 46 ; // Sublist Subscription Subscribed
                                            }

                                        break ; 
                                    }

                                // Override distribution list status with a manually provided metadata_id
                                if (isset($options_array['override_sublist_status'])) {
                                    $attendee_input['sms_metadata_id'] = $options_array['override_sublist_status'] ; 
                                    }


                                // If no phone number exists, then override the sublist subscription as inactive.
                                if (!$invite['phone_number']) {
                                    $attendee_input['sms_metadata_id'] = 51 ; // Sublist Subscription Inactive
                                    }

                                // Process the sms sublist subscription
                                // Metadata_id supplied above
                                if ($invite['sms_metadata_id'] != $attendee_input['sms_metadata_id']) {
                                    $this->Action_Asset_Member_Update($invite['member_id'],$attendee_input) ; 
                                    $data['clean_update_list'][$i]['member']['sms']['result'] = $this->asset_query_result ;                                      
                                    $data['clean_update_list'][$i]['member']['sms']['updated'] = 1 ; 
                                    $member_updated = 1 ; 
                                    } else {
                                        $data['clean_update_list'][$i]['member']['sms']['result'] = 'No update' ;
                                        $data['clean_update_list'][$i]['member']['sms']['updated'] = 0 ; 
                                        }

                                                               
                                
                                // Determine if AKIDS should be created and invite sent...
                                $create_invite = 1 ; 
                                switch ($options_array['create_invite']) {
                                    case 'no':
                                        $create_invite = 0 ; 
                                        break ; 
                                    default: 

                                        // If subscription type does not warrant an invite
                                        switch ($attendee_input['sms_metadata_id']) {
                                            case 51: // Inactive
                                            case 47: // Unsubscribed   
                                                $create_invite = 0 ; 
                                                break ; 
                                            default:

                                                break ; 
                                            }

                                        break ; 
                                    }


                                // Only re-create AKIDs if the last update was made more than xxx days ago...
                                // This was turned off because it was causing confusion as to why invites weren't getting sent out.
//                                if ($invite['timestamp_asset_updated_distribution_sublist_sms_compiled']['relative_days'] > -1) {
//                                    $create_invite = 0 ;
//                                    }

                                // Create asset subscribe AKIDs for phone number if eligible.
                                if ($create_invite == 1) {

                                    switch ($options_array['require_optin']) {
                                        case 'false':  // No opt-in confirmation or welcome needed.
                                        case 'single':

                                            // SKIP

                                            break ;                                                 
                                        case 'true': // Double opt-in required
                                        case 'double': 

                                            switch ($invite['do_not_contact_phone_number']) {
                                                case 1: // Phone number cannot be contacted.
                                                    // SKIP
                                                    break ; 
                                                default: // 

                                                    // Create action keys...
                                                    $this->Action_Create_Member_Action_Keys(46,$invite,$akid_options) ;  // Subscribed as the primary

                                                    $key_set = array() ; 
                                                    foreach ($this->action_keys as $action_key) {
                                                        $key_set[] = array(
                                                            'action_key_id' => $action_key['action_key_id'],
                                                            'action_id' => $action_key['action_id'],
                                                            'key_id' => $action_key['key_id']
                                                            ) ; 
                                                        }
                                                    $data['clean_update_list'][$i]['member']['sms']['action_keys'] = $key_set ; 


                                                    // Create campaign to invite the member...
                                                    $campaign_options = array(
                                                        'asset_id' => $data['asset_record']['distribution_sublist_sms']['list_sms_invitation']['asset_id'],
                                                        'delivery_asset_id' => $data['asset_record']['asset_id'],
                                                        'campaign_title' => $data['asset_record']['asset_title'].' Invitation Text for '.$invite['phone_number'].' ['.$invite['first_name'].' '.$invite['last_name'].']',
                                                        'delivery_trigger' => 'delivery_list_custom',
                                                        'recipients' => array(
                                                            'to' =>  array(
                                                                'delivery_list_custom' => array(
                                                                    'member_id' => array($invite['member_id']),
                                                                    'sms_metadata_id' => array(48,51), // Pending, Inactive 
                                                                    ),                                                    
                                                                ),                                              
                                                            ),
                                                        'action_keys' => $key_set,
                                                        'campaign_status_name' => 'scheduled',
                                                        'automation_status_name' => 'scheduled',
                                                        'timestamp_next_action' => TIMESTAMP
                                                        ) ; 

                                                    if ($data['asset_record']['parent_asset_title']) {
                                                        switch ($data['asset_record']['type_name']) {
                                                            case 'series_list_contacts': // Series campaigns use the list name as as the asset title
                                                            case 'series_list_profiles': // Series campaigns use the list name as as the asset title    
                                                                $campaign_options['personalization_overrides'] = array(
                                                                    'asset_record' => array(
                                                                        'asset_title' => $data['asset_record']['asset_title']
                                                                        )
                                                                    ) ;
                                                                break ;
                                                            default:    
                                                                $campaign_options['personalization_overrides'] = array(
                                                                    'asset_record' => array(
                                                                        'asset_title' => $data['asset_record']['parent_asset_title']
                                                                        )
                                                                    ) ;                                                    
                                                            } 
                                                        } else {
                                                            $campaign_options['personalization_overrides'] = array(
                                                                'asset_record' => array(
                                                                    'asset_title' => $data['asset_record']['asset_title']
                                                                    )
                                                                ) ;                                                        
                                                            }

                                                    $this->Action_Create_Campaign($campaign_options) ; 
                                                    
                                                    $data['clean_update_list'][$i]['member']['sms']['campaign']['campaign_id'] = $this->Get_Campaign()['campaign_id'] ; 
                                                    $data['clean_update_list'][$i]['member']['sms']['automation']['automation_id'] = $this->Get_Automation()['automation_id'] ;
                                                }                                



                                            break ;
                                        case 'single_welcome': 
                                        default:    

                                            switch ($invite['do_not_contact_phone_number']) {
                                                case 1: // Phone number cannot be contacted.
                                                    // SKIP
                                                    break ; 
                                                default: // 

                                                    // Create action keys...
                                                    $this->Action_Create_Member_Action_Keys(47,$invite,$akid_options) ;  // Unsubscribed as the primary

                                                    $key_set = array() ; 
                                                    foreach ($this->action_keys as $action_key) {
                                                        $key_set[] = array(
                                                            'action_key_id' => $action_key['action_key_id'],
                                                            'action_id' => $action_key['action_id'],
                                                            'key_id' => $action_key['key_id']
                                                            ) ; 
                                                        }
                                                    $data['clean_update_list'][$i]['member']['sms']['action_keys'] = $key_set ; 


                                                    // Create campaign to invite the member...
                                                    $campaign_options = array(
                                                        'asset_id' => $data['asset_record']['distribution_sublist_sms']['list_sms_welcome']['asset_id'],
                                                        'delivery_asset_id' => $data['asset_record']['asset_id'],
                                                        'campaign_title' => $data['asset_record']['asset_title'].' Welcome Text for '.$invite['phone_number'].' ['.$invite['first_name'].' '.$invite['last_name'].']',
                                                        'delivery_trigger' => 'delivery_list_custom',
                                                        'recipients' => array(
                                                            'to' =>  array(
                                                                'delivery_list_custom' => array(
                                                                    'member_id' => array($invite['member_id']),
                                                                    'sms_metadata_id' => array(46), // Subscribed
                                                                    ),                                                    
                                                                ),                                              
                                                            ),
                                                        'action_keys' => $key_set,
                                                        'campaign_status_name' => 'scheduled',
                                                        'automation_status_name' => 'scheduled',
                                                        'timestamp_next_action' => TIMESTAMP                                                        
                                                        ) ; 

                                                    if ($data['asset_record']['parent_asset_title']) {
                                                        switch ($data['asset_record']['type_name']) {
                                                            case 'series_list_contacts': // Series campaigns use the list name as as the asset title
                                                            case 'series_list_profiles': // Series campaigns use the list name as as the asset title
                                                                $campaign_options['personalization_overrides'] = array(
                                                                    'asset_record' => array(
                                                                        'asset_title' => $data['asset_record']['asset_title']
                                                                        )
                                                                    ) ;
                                                                break ;
                                                            default:    
                                                                $campaign_options['personalization_overrides'] = array(
                                                                    'asset_record' => array(
                                                                        'asset_title' => $data['asset_record']['parent_asset_title']
                                                                        )
                                                                    ) ;                                                    
                                                            } 
                                                        } else {
                                                            $campaign_options['personalization_overrides'] = array(
                                                                'asset_record' => array(
                                                                    'asset_title' => $data['asset_record']['asset_title']
                                                                    )
                                                                ) ;                                                        
                                                            }

                                                    $this->Action_Create_Campaign($campaign_options) ; 
                                                    $data['clean_update_list'][$i]['member']['sms']['campaign']['campaign_id'] = $this->Get_Campaign()['campaign_id'] ; 
                                                    $data['clean_update_list'][$i]['member']['sms']['automation']['automation_id'] = $this->Get_Automation()['automation_id'] ;
                                                    
                                                }
                                        }                                    
                                    }
                                }
                            break ; 
                        case 46: // SMS Sublist Current Status - Subscribed
                        case 47: // SMS Sublist Current Status - Unsubscribed    
                        

                            // Reset sublist status if eligible
                            if ($options_array['subscribe_type_message'] == 'true') {
                                
                                // Override distribution list status with a manually provided metadata_id
                                // This would be used when an VERIFIED recipient changes their subscription status manually
                                switch ($options_array['override_sublist_status']) {
                                    case 46: // Update to subscribed
                                    case 47: // Update to unsubscribed
                                    case 51: // Update to inactive
                                        $attendee_input['sms_metadata_id'] = $options_array['override_sublist_status'] ; 

                                        $this->Action_Asset_Member_Update($invite['member_id'],$attendee_input) ; 
                                        $data['clean_update_list'][$i]['member']['sms']['result'] = $this->asset_query_result ; 
                                        $data['clean_update_list'][$i]['member']['sms']['updated'] = 1 ; 
                                        $member_updated = 1 ; 
                                        break ; 
                                    default:
                                        $data['clean_update_list'][$i]['member']['sms']['result'] = 'No update' ;
                                        $data['clean_update_list'][$i]['member']['sms']['updated'] = 0 ; 
                                    }
                                }
                            
                            break ;  
                        case 49: // SMS Sublist Current Status - Cleaned
                            
                            // Reset sublist status if eligible
                            if ($options_array['subscribe_type_message'] == 'true') { 
                                
                                // Override distribution list status with a manually provided metadata_id
                                // This would be used when an VERIFIED recipient changes their subscription status manually
                                switch ($options_array['override_sublist_status']) {
                                    case 46: // Update to subscribed
                                        
                                        $attendee_input['sms_metadata_id'] = $options_array['override_sublist_status'] ; 

                                        $this->Action_Asset_Member_Update($invite['member_id'],$attendee_input) ; 
                                        $data['clean_update_list'][$i]['member']['sms']['result'] = $this->asset_query_result ; 
                                        $data['clean_update_list'][$i]['member']['sms']['updated'] = 1 ; 
                                        $member_updated = 1 ; 
                                        
                                        break ; 
                                    case 47: // Update to unsubscribed
                                    case 51: // Update to inactive
                                        $attendee_input['sms_metadata_id'] = $options_array['override_sublist_status'] ; 

                                        $this->Action_Asset_Member_Update($invite['member_id'],$attendee_input) ; 
                                        $data['clean_update_list'][$i]['member']['sms']['result'] = $this->asset_query_result ; 
                                        $data['clean_update_list'][$i]['member']['sms']['updated'] = 1 ; 
                                        $member_updated = 1 ; 
                                        
                                        break ; 
                                    default:
                                        $data['clean_update_list'][$i]['member']['sms']['result'] = 'No update' ;
                                        $data['clean_update_list'][$i]['member']['sms']['updated'] = 0 ; 
                                    }                                
                                }                            
                        }

                    } // END: Processing SMS Sublist                    


                
                
                
                // Create an automation step if the content asset is a series
                
                $create_automation_step = 0 ; 
                
                switch ($data['campaign_record']['content_asset_type_name']) {
                    case 'series':

                        // Get the active member if we're looking at a series.
                        $asset_member_query['filter_by_user_id'] = 'no'  ;
                        $asset_member_query['filter_by_account_id'] = 'no'  ;
                        $asset_member_query['filter_by_member_id'] = $invite['member_id'] ; 
                        $this->Set_Asset_Members_List($asset_member_query) ; 
                        $data['active_member_list'] = $this->Get_Asset_Members_List() ;
                        $data['active_member'] = $data['active_member_list'][0] ;

                        break ; 
                    } // End content type switch
                
                
                // This creates an automation step if one does not exist already.
                // Can only create this step if there is no opt in required
                // Otherwise, automation step should be created when recipient first opts in
                if (isset($options_array['campaign_id']) AND (($options_array['subscribe_type_message'] == 'true') OR ($options_array['subscribe_type_email'] == 'true'))) { 

                    switch ($options_array['require_optin']) { 
                        case 'false':  // No opt-in confirmation or welcome needed.
                        case 'single':
                        case 'single_welcome':

                            switch ($data['campaign_record']['content_asset_type_name']) {
                                case 'series':

                                    // If an automation ID doesn't exist, create one now
                                    if (($data['active_member']['automation_id'] == 0) AND ($data['content_asset']['asset_structure_count'] > 0)) {
                                                                        
                                        $create_automation_step = 1 ; 
                                        
                                        } 
                                    
                                    break ; 
                                } // End content type switch
                        } // End opt in switch 
                    } // End add automation logic   
                
                
                                
                // Restart automation for an existing member...
                // We should probably put a flag her to only do this if either email or sms is subscribed?
                if (isset($options_array['campaign_id']) AND ($data['active_member']['automation_id'] > 0) AND ($data['content_asset']['asset_structure_count'] > 0) AND ($options_array['campaign_reset'] == 'yes')) {

                    // If an automationid does exist, make sure it is cancelled in campaigns_automation if it hasn't sent yet
                    // then reset with a new automation
                    $existing_automation = new Asset() ; 
                    $existing_automation->Set_Replicate_User_Account_Profile($this) ; 

                    $update_automation_input = array(
                        'automation_id' => $data['active_member']['automation_id'], 
                        'automation_status_name' => 'deleted'
                        ) ; 

                    $existing_automation->Action_Update_Automation($update_automation_input) ; 
                    $automation_record = $existing_automation->Get_Automation() ;

                    switch ($automation_record['automation_status_name']) {
                        case 'packaged': 
                        case 'in_progress': 
                            // Skip the reset. This automation step needs to finish
                            break ;
                        default: // Ok to reset automation   
                            $create_automation_step = 1 ;    
                            
                            // Update this asset timestamp
                            
                            break ; 
                        }                                                                             
                    }                
                
                
                
                // Add automation process
                if ($create_automation_step == 1) {

                    $series_automation = new Asset() ; 
                    $series_automation->Set_Replicate_User_Account_Profile($this) ;            

                    $automation_options['campaign_id'] = $data['campaign_record']['campaign_id'] ; 
                    if (!isset($options_array['campaign_position'])) {
                        $automation_options['campaign_position'] = '1.00' ; 
                        } else {
                            $automation_options['campaign_position'] = $options_array['campaign_position'] ;     
                            }
                    $automation_options['automation_status_id'] = '4' ; 
                    $automation_options['timestamp_next_action'] = $data['content_asset']['structure'][0]['delay'] + $this->Get_Scheduled_Automation_Timestamp() ;  

                    $create_series_automation = $series_automation->Create_Automation($automation_options) ;


                    // Update the member_id with the new campaign_id
                    $update_series_member = array(
                        'member_id' => $invite['member_id'],
                        'campaign_id' => $data['campaign_record']['campaign_id'],
                        'automation_id' => $create_series_automation['insert_id'],
                        'member_status_id' => '6' // In Progess
                        ) ; 

                    $data['update_series_member'] = $this->Update_Asset_Member($update_series_member) ; 
                    $data['automation_update_count']++ ; 
                    $this->Set_Scheduled_Automation_Timestamp(2) ;
                    }                    
                

                if ($member_updated == 1) {
                    $data['update_count']++ ; 
                    }
                $i++ ; // Increment member processing count
                
                } // END: PROCESS CLEAN UPDATE ARRAY            
            
            
            $this->asset_members_processing_list['clean_update_list']  = $data['clean_update_list'] ; 
            $this->asset_members_processing_list['update_count']  = $data['update_count'] ; 
            $this->asset_members_processing_list['automation_update_count']  = $data['automation_update_count'] ; 
//            $this->test_data = $data ;   

            }
        
        return $this ; 
        }
    
    
    // Create an RSVP action key id for an attendee to an asset (e.g. an Event)
    public function Action_Create_Member_Action_Keys($metadata_id,$member_array,$options_array = array()) {
        
        $continue = 1 ; 
        
        
        
        if (isset($options_array['asset'])) {
            $data['asset'] = $options_array['asset'] ; 
            } else {
                $data['asset'] = $this->asset ; 
                }
        

        $data['member_record'] = $member_array ; 

        
        // Identify existing recipient if possible
        if (isset($member_array['member_id'])) {
            
            
            } elseif (isset($member_array['member_id_value'])) {
            
//                $contact->Set_Contact_By_ID($contact_array['contact_id']) ;
            
            } elseif (isset($member_array['email_address'])) {
            
                if ($member_array['email_address']) {

                    $validate_email_address = Utilities::Validate_Email_Address($member_array['email_address']) ; 
                    if ($validate_email_address['valid'] == 'no') {
                        $continue = 0 ; 
                        $this->Set_Alert_Response(96) ; // New contact invalid email
                        } else {
                            $member_array['email_address'] = $validate_email_address['email_address']  ;
                            $query_options['filter_by_account_id'] = 'yes' ;
                            $query_options['filter_by_user_id'] = 'no' ;

                            $contact = new Contact();
                            $contact->Set_Account_By_ID($this->account_id) ;                        
                            $contact->Set_Contact_By_Email_Address($member_array['email_address'],$query_options) ;
                            $data['member_record'] = $contact->Get_Contact() ; 
                            }                
                    }
            
            } else {
                $continue = 0 ; 
                $this->Set_Alert_Response(109) ; // Error - no identifiable member info
                }
        

        if ($continue == 1) {
            
            
            $action_input = array(
                'asset_id' => $data['asset']['asset_id'],
                'account_id' => $this->account_id,
                'structured_data_01' => array(
                    'member' => array(
                        'member_id' => $data['member_record']['member_id'], // Member ID in the asset member table (not member_id_value)
                        'full_name' => $data['member_record']['full_name'],
                        'email_address' => $data['member_record']['email_address']
                        )
                    )
                ) ;             
            
            if ($data['member_record'] == 'error') {  // No contact found. Create temp contact and create RSVP action.
            
                error_log('member = error'); 
                
                $contact->Action_Check_Do_Not_Contact($member_array['email_address'],$query_options) ; 
                $data['do_not_contact_record'] = $contact->Get_Do_Not_Contact() ; 
                
                if ((isset($data['do_not_contact_record']['do_not_contact'])) AND ($data['do_not_contact_record']['do_not_contact'] == 1)) {
                    
                    error_log('was on do not contact list '.$data['do_not_contact_record']['do_not_contact'].' | '.$data['do_not_contact_record']['do_not_contact']); 
                    
                    $continue = 0 ; 
                    $this->Set_Alert_Response(110) ; // Error - Contact is on do not contact list
                    }
                
                } else {  // Contact found. Create RSVP action
                
                    $action_input['structured_data_01']['member']['member_id_value'] = $data['member_record']['member_id_value'] ; 
                        
                    error_log('member DOES NOT = error'); 
                
                    if ($data['member_record']['do_not_contact'] == 1) {
                        
                        error_log('contact = do not contact '.$data['member_record']['do_not_contact']); 
                        
                        $continue = 0 ; 
                        $this->Set_Alert_Response(110) ; // Error - Contact is on do not contact list
                        }
                
                    }            
            }
        
        
        if ($continue == 1) {
            
            error_log('final continue = 1') ;
            error_log('asset type '.$data['asset']['type_id']) ;
            
            switch ($data['asset']['type_id']) {
                case 16: // Event  
            
                    $rsvp_metadata_array = array(15,16,17) ; // Attending, Not Attending, Maybe Attending options
                    $action_keys = array() ; 

                    // Create keys for each of the rsvp options (attending, not attending, maybe attending) and designating
                    // the submitted one as primary
                    foreach ($rsvp_metadata_array as $rsvp) {

                        if ($rsvp == $metadata_id) {
                            $action_input['structured_data_01']['rsvp_primary'] = 1 ;
                            }

                        $action_input['structured_data_01']['rsvp_metadata_id'] = $rsvp ; 
                        $this->Action_Create_System_Action(2,$action_input) ; 
                        $action_key = $this->Get_System_Action() ; 

                        if ($action_input['structured_data_01']['rsvp_primary'] == 1) {
                            $action_key['primary'] = 1 ; 
                            } else {
                                $action_key['primary'] = 0 ; 
                                }

                        $action_keys[] = $action_key ; 
                        unset($action_input['structured_data_01']['rsvp_primary']) ; 
                        }

                    // Need to trigger an RSVP email here

                    $this->Set_Alert_Response(111) ; // Success - RSVP saved and email triggered
                    break ;                    
                case 17: // Sequence
                    
                    
                    
                    break ; 
                case 19: // Contact List
                case 20: // User List
                case 23: // User Profile List  
                    
                case 26: // Contact SERIES list    
                case 27: // Profile SERIES list        
                case 28: // User SERIES list
                    
                    $metadata_array = array(46,47) ; // Subscribed, Unsubscribed
                    $action_keys = array() ; 

//                    error_log('attempting to create AKIDs') ;
                    
                    // Create keys for each of the metadata options and designating
                    // the submitted one as primary
                    foreach ($metadata_array as $metadata_item) {

                        $action_input['metadata_id'] = $metadata_item ; // This is the key_id
                        $action_input['member_id'] = $data['member_record']['member_id'] ;  // This needs to be the member_id of the parent distro list; not the member_id_value
                        
                        if ($metadata_item == $metadata_id) {
                            $action_input['structured_data_01']['subscription_primary'] = 1 ;
                            }

                        $action_input['structured_data_01']['subscription_metadata_id'] = $metadata_item ; 
                        $this->Action_Create_System_Action(3,$action_input) ; // Email version
                        $action_key = $this->Get_System_Action() ; 

                        if ($action_input['structured_data_01']['subscription_primary'] == 1) {
                            $action_key['primary'] = 1 ; 
                            } else {
                                $action_key['primary'] = 0 ; 
                                }

                        $action_keys[] = $action_key ; 
                        
                        $this->Action_Create_System_Action(4,$action_input) ; // SMS version
                        $action_key = $this->Get_System_Action() ; 

                        if ($action_input['structured_data_01']['subscription_primary'] == 1) {
                            $action_key['primary'] = 1 ; 
                            } else {
                                $action_key['primary'] = 0 ; 
                                }

                        $action_keys[] = $action_key ;                        
                        unset($action_input['structured_data_01']['subscription_primary']) ; 
                        }

                    // Need to trigger an invitation email here

                    $this->Set_Alert_Response(111) ; // Success - RSVP saved and email triggered
                    break ;                    
                }
            }
        
        
        $data['member'] = $data['member_record'] ;
        $data['action_keys'] = $action_keys ;
        $this->action_keys = $data['action_keys'] ; 
        
//        error_log('these are akids: '.json_encode($action_keys)) ;
        
        return $this ; 
        }
    
    
    
    // Process an RSVP for an attendee to an asset (e.g. an Event) - using an action key id that has been verified
    // $rsvp_state should be a metadata id
    public function Action_Process_Member_Action_Key($metadata_id,$action_key_array) {
        
        $continue = 1 ; 
        
        $data['metadata_record'] = $this->Set_Metadata($metadata_id)->Get_Metadata() ; 
        $data['asset_record'] = $this->Get_Asset() ; 
        $personalization['asset_record'] = $data['asset_record'] ;
        
        if (isset($action_key_array['member_id'])) { 
          
            $asset_member_query['filter_by_user_id'] = 'no'  ;
            $asset_member_query['filter_by_account_id'] = 'no'  ;
            $asset_member_query['filter_by_member_id'] = $action_key_array['member_id'] ; 
            $this->Set_Asset_Members_List($asset_member_query) ; 
            
            $data['asset_member_list'] = $this->Get_Asset_Members_List() ;
            
            if (count($data['asset_member_list']) < 1) {
                $continue = 0 ; 
                } else {
                    $data['asset_member_record'] = $data['asset_member_list'][0] ; 
                    }
            
            } elseif (isset($action_key_array['structured_data_01']['contact']['contact_id'])) { 
            
                $contact = new Contact();
                $contact->Set_Account_By_ID($this->account_id) ;
            
                $query_options['filter_by_account_id'] = 'yes' ;
                $query_options['filter_by_user_id'] = 'no' ;
                $contact->Set_Contact($action_key_array['structured_data_01']['contact']['contact_id'],$query_options) ;

                $data['contact_record'] = $contact->Get_Contact() ; 
            
            } elseif (isset($action_key_array['structured_data_01']['contact']['email_address'])) {

                $contact = new Contact();
                $contact->Set_Account_By_ID($this->account_id) ;
            
                $validate_email_address = Utilities::Validate_Email_Address($action_key_array['structured_data_01']['contact']['email_address']) ; 
                if ($validate_email_address['valid'] == 'no') {
                    $continue = 0 ; 
                    $this->Set_Alert_Response(96) ; // Contact invalid email
                    } else {
                    
                        $contact_array['email_address'] = $validate_email_address['email_address']  ;
                        $query_options['filter_by_account_id'] = 'yes' ;
                        $query_options['filter_by_user_id'] = 'no' ;

                        $contact->Set_Contact_By_Email_Address($contact_array['email_address'],$query_options) ;
                        $data['contact_record'] = $contact->Get_Contact() ; 
                        }
                
            
                if (($continue == 1) AND ($data['contact_record'] == 'error')) {

                    // No contact found by email address.
                    // Create a new contact with the submitted information.
                    $contact_input = array(
                        'full_name' => $action_key_array['structured_data_01']['contact']['full_name'],
                        'first_name' => $action_key_array['structured_data_01']['contact']['first_name'],
                        'last_name' => $action_key_array['structured_data_01']['contact']['last_name'],
                        'email_address' => $action_key_array['structured_data_01']['contact']['email_address'],
                        'phone_number' => $action_key_array['structured_data_01']['contact']['phone_number']
                        ) ; 
                    
                    $contact->Action_Create_Contact($contact_input) ; 
                    $data['contact_create_set'] = $contact->Get_Contact_Create_Set() ; 
                    
                    if ($data['contact_create_set']['success_count'] > 0) {
                        $contact->Set_Contact_By_ID($data['contact_create_set']['success'][0]['contact_id']) ; 
                        $data['contact_record'] = $contact->Get_Contact() ; 
                        } else {
                            $this->Set_Alert_Response(94) ; // Error - Could not save contact
                            $continue = 0 ; 
                            }
                    }
            
            } else {
                $continue = 0 ; 
                $this->Set_Alert_Response(109) ; // Error - no identifiable contact info
                }

        
        if ($continue == 1) {
            
//            $attendee_input = array(
//                'asset_id' => $this->asset_id,
//                'contact_id' => $data['contact_record']['contact_id'],
//                'metadata_id' => $rsvp_state
//                ) ; 
//            
//            $data['attendee_record'] = $this->Create_Asset_Member($attendee_input) ; 
            
//            // Mark the action key as used
//            if (!isset($data['attendee_record']['error'])) {
//                $this->Action_Complete_System_Action_Key() ; 
//                }
            
            switch ($action_key_array['action_id']) {
                case 3: // Process Email Sublist Subscription
                    
                    $member_input['member_id'] = $data['asset_member_record']['member_id'] ; 
                    $options_input['asset_id'] = $data['asset_record']['asset_id'] ;
                    $options_input['subscribe_type_email'] = 'true' ;
                    $options_input['override_sublist_status'] = $metadata_id ;
                    $this->Action_Asset_Member_Process($member_input,$options_input) ;

                    $asset_members_processing_list = $this->Get_Asset_Members_Processing_List() ;
//                    print_r($asset_members_processing_list) ; 
                    
//                    if ($result['update_count'] == 1) {
                        switch ($metadata_id) {
                            case 46: // Subscribed
                                $this->Set_Alert_Response(211) ; // Asset subscribed
                                $this->Append_Alert_Response($personalization,array(
                                    'location' => __METHOD__
                                    )) ;                                 
                                
                                break ; 
                            case 47: // Unsubscribed
                                $this->Set_Alert_Response(212) ; // Asset unsubscribed
                                $this->Append_Alert_Response($personalization,array(
                                    'location' => __METHOD__
                                    )) ;                                
                                break ; 
                            }
//                        }
                    
                    $response = $this->Get_Response() ; 
                    
                    break ; 
                case 4: // Process SMS Sublist Subscription
                    
                    $member_input['member_id'] = $data['asset_member_record']['member_id'] ; 
                    $options_input['asset_id'] = $data['asset_record']['asset_id'] ;
                    $options_input['subscribe_type_sms'] = 'true' ;
                    $options_input['override_sublist_status'] = $metadata_id ;
                    $this->Action_Asset_Member_Process($member_input,$options_input) ;

                    $asset_members_processing_list = $this->Get_Asset_Members_Processing_List() ;
                    
//                    if ($result['results'] == 1) {
                        switch ($metadata_id) {
                            case 46: // Subscribed
                                $this->Set_Alert_Response(211) ; // Asset subscribed
                                $this->Append_Alert_Response($personalization,array(
                                    'location' => __METHOD__
                                    )) ;                                 
                                
                                break ; 
                            case 47: // Unsubscribed
                                $this->Set_Alert_Response(212) ; // Asset unsubscribed
                                $this->Append_Alert_Response($personalization,array(
                                    'location' => __METHOD__
                                    )) ;                                
                                break ; 
                            }
//                        }
                    
                    
                    break ;                     
                }
            
            // Rerun asset member list query
            $asset_member_query['filter_by_user_id'] = 'no'  ;
            $asset_member_query['filter_by_account_id'] = 'no'  ;
            $asset_member_query['filter_by_member_id'] = $action_key_array['member_id'] ; 
            $this->Set_Asset_Members_List($asset_member_query) ;
            
            }
    

        
        return $this ; 
        }
    
    

    
    
    
    // Save a search query to the DB
    public function Action_Save_Search($search_parameters) {
        
        // Create the search array to save
        $search_array = array() ; 
        foreach ($this->full_asset_list as $asset) {
            $search_array[] = $asset['asset_id'] ;
            }
        
        // Encode search result as json
        $search_result_encoded = json_encode($search_array) ; 

        $search_input = array(
            'session_public_id' => SESSION_PUBLIC_ID,
            'search_name' => $search_parameters['search_name'],
            'search_parameter' => $search_parameters['search_parameter'],
            'search_result' => $search_result_encoded
            ) ; 

        $system_search = new System() ; 
        $system_search->Action_Search_Save($search_input) ; 
        $this->search_id = $system_search->Get_Search_ID() ; 

        return $this ; 
        }
    
    
    
    
    // Decode the json asset map provide in the asset_type table
    public function Action_Decode_Map($map) { 
        
        $map_array = json_decode($map,true) ; 
        return $map_array ; 
        }
    
    
    // Processes and maps linked assets, metadata, and relational metadata (tags / categories) for a parent asset
    // The input here should be a DAL result (w/ result_count, results) to put directly into this function as asset_input
    public function Action_Map_Asset($asset_input,$additional_parameters) {  

        // skip_asset_mapping = yes is designed to decrease database load
        
        $original_asset_input = $asset_input ; // Needed when returning the result at the bottom
        
        // This should create an array of fields to skip during mapping to decrease database load
        if (!isset($additional_parameters['skip_fields'])) { 
            $additional_parameters['skip_fields'] = array() ; 
            }
        
    

        // This handles the various versions of result arrays that might come in - single result[0], multidimensional[0], single result forced multidimensional
        $asset_array = array() ; 
        if ($asset_input['result_count'] == 1) {
            if (isset($asset_input['results'][0])) {
                $asset_array = $asset_input['results'] ; 
                } else {
                    $asset_array[] = $asset_input['results'] ; 
                    }
            } elseif ((isset($asset_input['result_count'])) AND ($asset_input['result_count'] > 1)) {
                $asset_array = $asset_input['results'] ; 
                } else {
                $asset_array[] = $asset_input ; 
                }
        unset($asset_input['results']) ; 
        
        
        foreach ($asset_array as $asset) {         
        
            $asset_id = $asset['asset_id'] ;        
            $asset['map_array'] = $this->Action_Decode_Map($asset['map']) ;     
            $this->Set_Model_Timings('Asset.Action_Map_Asset_action_decode_map_returned_'.$asset_id) ;
                                    
            
            // Map parent asset: Convert asset type keys into mapped keys of the asset with corresponding values
            if (count($asset['map_array']['parent']) > 0){
                foreach ($asset['map_array']['parent'] as $key => $value) {
                    switch($key) {
                        default:
                            $asset[$value] = $asset[$key] ;     
                        }                    
                    }
                }


            // Map asset content: Convert asset type keys into mapped keys of the asset with corresponding values
            if (count($asset['map_array']['content']) > 0) {
                foreach ($asset['map_array']['content'] as $key => $value) {
                    switch($key) {
                        case 'asset_id_01':
                        case 'asset_id_02':
                        case 'asset_id_03':
                        case 'asset_id_04':

                            $this_sub_asset_num = str_replace("asset_id_", "", $key) ; 
                            
                            if (($asset[$key] == 0) AND ($asset['map_array']['additional_data']['default_'.$value.'_asset_id'])) {
                                $asset[$key] = $asset['map_array']['additional_data']['default_'.$value.'_asset_id'] ;                                     
                                }

                            if ($asset[$key] > 0) {
                                if ($additional_parameters['skip_asset_mapping'] != 'yes') { 
                                    
                                    $this->Set_Model_Timings('Asset.Action_Map_Asset_sub_asset_set_begin_'.$this_sub_asset_num.'_'.$asset_id) ;
                                    
                                    $retrieve_asset = new Asset() ; 
                                    $retrieve_asset->Set_Asset_By_ID($asset[$key], array(
                                            'skip_history' => 'yes'
                                            )
                                        ) ;
                                    $asset[$value] = $retrieve_asset->Get_Asset() ;
                                    $this->Set_Model_Timings('Asset.Action_Map_Asset_sub_asset_set_complete_'.$this_sub_asset_num.'_'.$asset_id) ;
                                    
                                    } else {

                                        $sub_asset_data = array() ;
                                        $sub_asset_data['asset_id'] = $asset[$key] ; 
                                        
                                        foreach ($asset as $search_key => $search_value) {
                                            if (substr($search_key, 0, strlen('asset_'.$this_sub_asset_num.'_')) === 'asset_'.$this_sub_asset_num.'_') {
                                                
                                                $this_key_name = str_replace("asset_".$this_sub_asset_num.'_', "", $search_key) ; 
                                                $sub_asset_data[$this_key_name] = $search_value ; 
                                                }
                                            }
                                                 
                                        $sub_asset_additional_parameters['skip_asset_mapping'] = 'yes' ;
                                        $sub_asset_additional_parameters['skip_history'] = 'yes' ;
                                        $asset[$asset['map_array']['content'][$key]] = $this->Action_Map_Asset($sub_asset_data,$sub_asset_additional_parameters)['results'][0] ; 
                                        }
                                }
                            
                            break ;
                        case 'text_01':
                        case 'text_02':    
                        case 'metadata_01':
                        case 'metadata_02':
                        case 'metadata_03':
                        case 'metadata_04':
                            if (!in_array($value,$additional_parameters['skip_fields'])) {
                                switch ($value) {
//                                    case 'retail_price':
//                                    case 'wholesale_price':
//                                        setlocale(LC_MONETARY,'en_US') ; 
//                                        $asset[$value] = money_format('%.2n',$asset[$key]) ; 
//                                        break ;
//                                    case 'related_products':
//                                        $related_product_array = Utilities::Process_Comma_Separated_String($asset[$key]) ; 
//                                        $final_related_product_array = array() ; 
//                                        $product = new Asset() ;
//                                        foreach($related_product_array as $product_asset_id) {
//                                            $product->Set_Asset($product_asset_id,array('skip_fields' => array($value))) ; 
//                                            $final_related_product_array[] = $product->Get_Asset() ; 
//                                            }
//                                        $asset[$value] = $final_related_product_array ; 
//                                        break ;
                                    default:
                                        $asset[$value] = $asset[$key] ; 
                                    } 
                                }
                            break ;
                        }                    
                    }
                }
                

            if ($additional_parameters['skip_asset_mapping'] != 'yes') {
                
                                    
                if (isset($asset['map_array']['additional_data']['metadata_type'])) {

                    $this->Set_Model_Timings('Asset.Action_Map_Asset_set_metadata_begin_'.$asset_id) ;
                
                    foreach ($asset['map_array']['additional_data']['metadata_type'] as $type_id) {

                        $query_options['filter_by_metadata_type'] = 'subscription' ; 
                        $query_options['allow_global_assets'] = 'only' ; 
                        $query_options['override_paging'] = 'yes' ; 

                        $metadata = new Asset() ; 
                        $metadata->Set_Metadata_List($query_options) ; 
                        $metadata_type_array = $metadata->Get_Metadata_List() ; 

                        foreach ($metadata_type_array as $type) {
                            $asset['map_array']['additional_data']['metadata'][] = $type ; 
                            }
                        }

                    $asset['additional_data'] = $asset['map_array']['additional_data'] ; 
                    
                    $this->Set_Model_Timings('Asset.Action_Map_Asset_set_metadata_complete_'.$asset_id) ;
                    }
                               
                }

            
            // Set visibility_title overrides if needed:
            if (is_array($asset['map_array']['additional_data']['visibility_title_overrides'])) {
                
                $this->Set_Model_Timings('Asset.Action_Map_Asset_set_visibility_title_overrides_'.$asset_id) ;
                
                foreach ($asset['map_array']['additional_data']['visibility_title_overrides'] as $override_name => $override_title) {
                    if ($override_name == $asset['visibility_name']) {
                        $asset['visibility_title'] = $override_title ; 
                        }
                    }
                }            
            
            
            
            // Map structured data
            if (count($asset['map_array']['structured_data']) > 0) { 
                foreach ($asset['map_array']['structured_data'] as $structured_key => $structured_value) {                     
                    
                    
                    $asset[$structured_key] = $this->Action_Decode_Map($asset[$structured_key]) ; 

 
//                    print_r('test array ') ; 
//                    print_r($structured_key) ;
//                    print_r($structured_value) ;
                    
                    if (count($asset[$structured_key]) > 0) {                
                        foreach ($asset[$structured_key] as $key => $value) {
                            $asset[$key] = $value ;                              
                            }
                        }
                    
                    
                    // Look for missing default values and add if possible:
                    foreach ($structured_value as $search_value) {
                        if (!$asset[$structured_key][$search_value]) {
                            
                            if (isset($asset['map_array']['additional_data']['default_'.$search_value])) {
                                $asset[$structured_key][$search_value] = $asset['map_array']['additional_data']['default_'.$search_value] ; 
                                $asset[$search_value] = $asset['map_array']['additional_data']['default_'.$search_value] ; 
                                }
                            }
                        }
                    
                    
                    }
                }
            
            

            
            
            // Map type specific data
            switch ($asset['type_id']) { 
                case 3: // image
                    $asset['image_delivery_url'] = CLOUDINARY_SECURE_DELIVERY_URL ;
                    $asset['image_folder'] = CLOUDINARY_DELIVERY_FOLDER ; 
                    $asset['image_folder_filename'] = CLOUDINARY_DELIVERY_FOLDER.'/'.$asset['filename'] ; 
                    $asset['image_folder_filename_extension'] = CLOUDINARY_DELIVERY_FOLDER.'/'.$asset['filename'].'.'.$asset['filename_extension'] ; 
                    break ;
                case 14: // Blog post
                    
                    if (!isset($asset['navigation_title'])) {
                        $asset['navigation_title'] = $asset['asset_title'] ; 
                        }
                    
                    break ; 
                }
            
            
            
        // Remove subasset data...    
        $i = 1;
        do {
            foreach ($asset as $search_key => $search_value) {
                if (substr($search_key, 0, strlen('asset_0'.$i.'_')) === 'asset_0'.$i.'_') {
                    unset($asset[$search_key]) ; 
                    }
                }            
            $i++ ; 
            } while ($i < 5); 
            
            
        if ($asset_input['result_count'] == 1) {
            if (isset($original_asset_input['results'][0])) {
                $asset_input['results'][] = $asset ; 
                } else {
                    $asset_input['results'] = $asset ; 
                    }
            } else {
                $asset_input['results'][] = $asset ; 
                }            
            
        }

        return $asset_input ; 
        }
    
    
    public function Get_Child_Asset_Compiled($child_asset_id) {
        $asset_compiled = '' ;
        
        $data['asset_record'] = $this->Get_Asset() ; 
        
        foreach ($data['asset_record']['structure'] as $child) {
            
            if ($child['structure']['child_asset_id'] == $child_asset_id) {
                $asset_compiled = $child['asset_compiled'] ; 
                }
            
            }
        
        return $asset_compiled ; 
        } 
    
    public function Action_Isolate_Child_Asset($child_asset_id) {
        
        $data['asset_record'] = $this->Get_Asset() ; 
        
        $data['child_asset'] = Utilities::Deep_Array($data['asset_record']['structure'],'child_asset_id',$child_asset_id) ; 
        if ($data['child_asset']['child_asset_id']) {
            $data['asset_record']['structure'] = array() ; 
            $data['asset_record']['structure'][] = $data['child_asset'] ; 
            $this->asset['structure'] = $data['asset_record']['structure'] ; 
            }

        return $this ; 
        }
    
    // IMPORTANT: When pulling results, force a multi-dimensional array or it will break
    public function Action_Compile_Asset($compile_options = array()) {        
        
        $this->Set_Model_Timings('Asset.Action_Compile_Asset_begin_'.$this->asset['asset_id']) ; 
                
        $compiled_asset = '' ; // The compiled asset that we will return
        
        switch ($this->asset['type_id']) {  
            // If the asset type is a collection, then multiple assets need to be compiled 
            case 1:
            case 4:
            case 7:
                
                $query_array = array(
                    'table' => 'asset_structure',
                    'join_tables' => array(),
                    'fields' => "assets.asset_title, 
                        asset_content.*, 
                        asset_type.type_id, 
                        asset_structure.structure_order, asset_structure.delay",
                    'where' => "asset_structure.asset_id='$this->asset_id'",
                    'order_by' => "asset_structure.structure_order",
                    'order' => "ASC"
                    );

                $query_array['join_tables'][] = array(
                    'table' => 'asset_content',
                    'on' => 'asset_content.asset_id',
                    'match' => 'asset_structure.child_asset_id'
                    );  
                
                $query_array['join_tables'][] = array(
                    'table' => 'assets',
                    'on' => 'assets.asset_id',
                    'match' => 'asset_content.asset_id'
                    );
                
                $query_array['join_tables'][] = array(
                    'table' => 'asset_type',
                    'on' => 'asset_type.type_id',
                    'match' => 'assets.type_id'
                    );                

                $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');

                $compiled_asset = $result ;                     
                break ;
            case 17: // Series
                
                $asset_structure = array() ; 
                
                $structure = $this->asset['structure'] ;                 
                
                // Pulling from asset content history
                if ((count($structure) > 0) AND (isset($structure[0]['temp_structure_id']))) { 

                    $temp_structure_table = str_replace(".","",'temp_asset_structure_table_'.microtime(true)) ; 

                    $fields_array[] = 'temp_structure_id INT NOT NULL PRIMARY KEY' ;
                    $fields_array[] = 'asset_id INT(10) NOT NULL' ;
                    $fields_array[] = 'child_asset_id INT(10) NOT NULL' ;
                    $fields_array[] = 'structure_order DECIMAL(5,2) NOT NULL' ;
                    $fields_array[] = 'delay INT(20) NOT NULL' ;

                    $import_fields_array = array() ;
                    foreach ($fields_array as $field_name) {
                        $import_fields_array[] = ltrim(explode(" ",$field_name)[0],"(") ;
                        }

                    $create_table_array = array(
                        'new_table' => $temp_structure_table,
                        'fields_array' => $fields_array
                        ) ; 

                    $create_table = $this->DB->Query('CREATE_TABLE',$create_table_array) ;

                    $add_primary_statement = 'ALTER TABLE '.$temp_structure_table.' MODIFY temp_structure_id INT NOT NULL AUTO_INCREMENT' ;
                    $add_primary = $this->DB->_connection->query($add_primary_statement) ;

                    // CREATE THE TEMP TABLE...      
                    $import_values_array = array() ;         
                    $i = 0 ; 
                    foreach ($structure as $structure_result) {

                        $import_row = array() ; 
                        foreach ($import_fields_array as $field_key) {
                            $import_row[$field_key] = $structure_result[$field_key] ; 
                            }
                        if ((!isset($import_row['asset_id'])) OR ($import_row['asset_id'] == 0)) {
                            $import_row['asset_id'] = $this->asset_id ; 
                            }
                        
                        $import_values_array[] = $import_row ;             
                        $i++ ; 
                        }

                    $import_query_array = array(
                        'table' => $temp_structure_table,
                        'fields' => $import_fields_array,
                        'values' => $import_values_array
                        ) ; 

                    $result_temp_table = $this->DB->Query('BULK_INSERT',$import_query_array); 

                    $structure_options['allow_global_assets'] = 'all' ; 
                    $structure_options['override_paging'] = 'yes' ; 
                    $structure_options['structure_table'] = $temp_structure_table ;
                    $structure_options['filter_by_structure_asset_id'] = $this->asset_id ;
                                    
                    $asset_structure = $this->Set_Asset_List($structure_options)->Get_Asset_List() ; 
                    
                    // Drop the temp table
                    $drop_table_query_array = array(
                        'table' => $temp_structure_table
                        );   
                    
                    $drop_table_result = $this->DB->Query('DROP_TABLE',$drop_table_query_array) ;
                        
                    
                    } else if ((count($structure) > 0) AND (!isset($structure[0]['temp_structure_id']))) { 
                    
                        // Pulling from structure table
                        $asset_structure = $structure ; 
                    
                        }

                // Compile the delay of the asset structure...
                $s = 0 ; 
                foreach ($asset_structure as $structure_item) {
                    $structure_item['delay_compiled'] = Utilities::System_Time_Convert_Seconds($structure_item['delay'],array('ingore_years' => 'yes')) ; 
                    $asset_structure[$s] = $structure_item ; 
                    $s++ ;
                    }
                
                
                $compiled_asset = $asset_structure ; 
                
                break ;
            case 12: // Email 
                
                $email_content = '' ; 
                $structure = $this->asset['structure'] ;    

                if ($compile_options['inline_layouts'] == 'yes') {
                    $css = new Css() ;
                    $css->Action_Inline($this->asset['email_background']['email_background_template']) ;
                    $css_array = $css->Get_Css_Array() ;
                    $compile_options['css_source'] = $css_array ;      
                    $this->Set_Model_Timings('Asset.Action_Compile_Asset_email_background_inlined_'.$this->asset['asset_id']) ; 
                    }

                        
                
                // Compile each individual content block
                $s = 0 ; 
                foreach ($structure as $content_block) {
                    
                    $content = new Asset() ;

                    $content->Replace_Model_Timings_Array($this->Get_Model_Timings()) ; 
                    
                    if ($this->campaign['campaign_id']) {
                        $content->Set_Replicate_Campaign($this) ;
                        }
                    $content->Set_Asset_Structure_Item($content_block) ;
                    
                    $content_asset_params = array(
                        'history_id' => 'latest'
                        ) ;                     
                    $content->Set_Asset_By_ID($content_block['child_asset_id'],$content_asset_params) ;
                    $content->Action_Compile_Asset($compile_options) ; 
                    $email_content .= $content->Get_Asset_Compiled() ;
                    
                    $this_content['structure'] = $content_block ; 
                    $this_content['asset_record'] = $content->Get_Asset() ; 
                    $this_content['asset_compiled'] = $content->Get_Asset_Compiled() ;
                    
                    $this->asset['structure'][$s] = $this_content ;

                    $this->Replace_Model_Timings_Array($content->Get_Model_Timings()) ; 
                    $this->Set_Model_Timings('email_content_block_'.$s.'_compiled') ; 
        
                    $s++ ; 
                    }
                
                
                
                $this->asset['asset_images'] = array() ;
                foreach ($this->asset['structure'] as $structure_item) {
                    
                    if ($structure_item['asset_record']['image_01_asset_id']) {
                        $this->asset['asset_images'][] = $structure_item['asset_record']['image_01'] ; 
                        }
                    if ($structure_item['asset_record']['image_02_asset_id']) {
                        $this->asset['asset_images'][] = $structure_item['asset_record']['image_02'] ; 
                        }
                    if ($structure_item['asset_record']['image_03_asset_id']) {
                        $this->asset['asset_images'][] = $structure_item['asset_record']['image_03'] ; 
                        }
                    if ($structure_item['asset_record']['image_04_asset_id']) {
                        $this->asset['asset_images'][] = $structure_item['asset_record']['image_04'] ; 
                        }                    
                    }
                
                
                try {
                    $templates = array('content' => $this->asset['email_background']['email_background_template']);
                    $env = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
                    $this->asset['email_background']['email_background_template'] = $env->render('content', array(
                        'user_record' => $this->user,
                        'account_record' => $this->account,
                        'profile_record' => $this->profile,
                        'campaign_record' => $this->campaign,
                        ));                    
                    } catch (Exception $e) {
//                        print_r($e)  ;
                        }
                
                // Place the compiled email into the background
                $email_content = str_replace('*|MESSAGE|*',$email_content,$this->asset['email_background']['email_background_template']) ;

                // Place preview text into the email
                $compiled_asset = str_replace('*|PREVIEW_TEXT|*',$this->asset['preview_text'],$email_content) ;     
                
                break ;               
            case 13: // Email Content - Puts content into it's corresponding layout
                

                $content_asset_params = array(
                    'history_id' => 'latest'
                    ) ;
                
                if (isset($this->asset['image_01_asset_id'])) {
                    $image = new Asset() ; 
                    $this->asset['image_01'] = $image->Set_Asset_By_ID($this->asset['image_01_asset_id'],$content_asset_params)->Get_Asset() ; 
                    }
                if (isset($this->asset['image_02_asset_id'])) {
                    $image = new Asset() ; 
                    $this->asset['image_02'] = $image->Set_Asset_By_ID($this->asset['image_02_asset_id'],$content_asset_params)->Get_Asset() ; 
                    }
                if (isset($this->asset['image_03_asset_id'])) {
                    $image = new Asset() ; 
                    $this->asset['image_03'] = $image->Set_Asset_By_ID($this->asset['image_03_asset_id'],$content_asset_params)->Get_Asset() ; 
                    }
                
                $personalization_array = array(
                    'asset_record' => $this->asset
                    ) ; 
                
                $compiled_asset = '' ; 
                
                switch ($compile_options['compile_as']) {
                    case 'editor':
                                
//                        if (isset($this->asset['email_layout']['editor_content_layout'])) {
                         try {    
                            $templates = array('content' => $this->asset['email_layout']['editor_content_layout']);
                            $env = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
                            $rendered = $env->render('content', array(
                                'asset_record' => $this->asset,
                                'campaign_record' => $this->campaign,
                                'structure_item' => $this->Get_Asset_Structure_Item()
                                ));    
                            } catch (Exception $e) {
                                $rendered = $this->asset['email_layout']['email_content_layout'] ; 
                                }
                        
//                        $compiled_asset = $this->Action_Personalize_String($rendered,$personalization_array) ; 
                        
                        break ; 
                    default:

                        try {
                            $templates = array('content' => $this->asset['email_layout']['email_content_layout']);
                            $env = new \Twig\Environment(new \Twig\Loader\ArrayLoader($templates));
                            $rendered = $env->render('content', array(
                                'asset_record' => $this->asset,
                                'campaign_record' => $this->campaign,
                                'structure_item' => $this->Get_Asset_Structure_Item()
                                ));
                            } catch (Exception $e) {
//                                print_r($e)  ;
                                }                        

                    }
                
                
                if ($compile_options['inline_layouts'] == 'yes') {
                    if (isset($compile_options['css_source'])) {
                        
                        $css = new Css() ;
                        $css->Set_Css_Array($compile_options['css_source']) ; 
                        $inlined_content_array = $css->Action_Inline($rendered,array('css_source' => 'internal'))->Get_Inlined_Array() ;                    
                        $rendered = $inlined_content_array['email_html']  ; 
                        }                    
                    }
                
                $compiled_asset = str_replace('*|text_01|*',$personalization_array['asset_record']['text_01'],$rendered) ; 
                $compiled_asset = str_replace('*|text_02|*',$personalization_array['asset_record']['text_02'],$compiled_asset) ;
                $compiled_asset = str_replace('*|asset_id|*',$personalization_array['asset_record']['asset_id'],$compiled_asset) ;
                
//                $compiled_asset = $rendered ; 
//                print_r($rendered) ; 
                    
                break ; 
            case 18: // SMS
                
                $content_asset_params = array(
                    'history_id' => 'latest',
                    'skip_history' => 'yes'
                    ) ;
                    
                $compiled_asset = $this->asset['text_01'] ; 
                
                $personalization_array = array(
                    'account_record' => $this->Get_Account()
                    ) ; 
                $compiled_asset = $this->Action_Personalize_String($compiled_asset,$personalization_array) ;
                
                $media_set = array() ; 
                if ($this->asset['media_asset_id']) {
                    
                    $media_asset_id_array = Utilities::Process_Comma_Separated_String($this->asset['media_asset_id']) ; 
                    
                    $media = new Asset() ; 
                    foreach ($media_asset_id_array as $media_id) {                        
                        $media_set[] = $media->Set_Asset_By_ID($media_id,$content_asset_params)->Get_Asset() ;                         
                        }                    
                    }                
                
                $this->asset['media_set'] = $media_set ; 
                    
                break ;                
            case 14: // Blog post
                $compiled_asset = $this->asset['text_01'] ; 
                
                $personalization_array = array(
                    'account_record' => $this->Get_Account()
                    ) ; 
                $compiled_asset = $this->Action_Personalize_String($compiled_asset,$personalization_array) ;
                
                break ;
            case 15: // Product
                
                $asset_content = '' ; 
                $structure = $this->asset['structure'] ;    

                $personalization_array = array(
                    'account_record' => $this->Get_Account()
                    ) ; 
                
                // Compile each individual content block
                if ((is_array($structure)) AND (count($structure) > 0)) {
                    $s = 0 ; 
                    foreach ($structure as $content_block) {

                        $content = new Asset() ;

                        $content_asset_params = array(
                            'history_id' => 'latest',
                            'skip_history' => 'yes'
                            ) ;                
                        $content->Set_Asset_By_ID($content_block['child_asset_id'],$content_asset_params) ;
                        $content->Action_Compile_Asset() ; 
                        $asset_content .= $content->Get_Asset_Compiled() ;

                        $this_content['structure'] = $content_block ; 
                        $this_content['asset_record'] = $content->Get_Asset() ; 
                        $this_content['asset_compiled'] = $content->Get_Asset_Compiled() ;
                
                        $this_content['asset_record']['text_01'] = $this->Action_Personalize_String($this_content['asset_record']['text_01'],$personalization_array) ;
                        $this_content['asset_record']['text_02'] = $this->Action_Personalize_String($this_content['asset_record']['text_02'],$personalization_array) ;

                        
                        // Not sure if this is needed... 
//                        $this_content['asset_compiled'] = $this->Action_Personalize_String($this_content['asset_compiled'],$personalization_array) ;
                            
                        
                        $this->asset['structure'][$s] = $this_content ;
                        $s++ ; 
                        }
                    }

                $compiled_asset = $asset_content ;
                
//                $personalization_array = array(
//                    'account_record' => $this->Get_Account()
//                    ) ; 
//                

                $this->asset['text_01'] = $this->Action_Personalize_String($this->asset['text_01'],$personalization_array) ;
                $this->asset['text_02'] = $this->Action_Personalize_String($this->asset['text_02'],$personalization_array) ;
                
                
                $compiled_asset = $this->Action_Personalize_String($compiled_asset,$personalization_array) ;
                
                break ; 
            case 19: // Distribution List - Contacts
            case 26: // Series List - Contacts
                
                $this->Set_Asset_Members_List($compile_options) ; 
                $compiled_asset = $this->asset_members_list ; 
                
                break ; 
            case 24: // Post Content - Puts content into it's corresponding layout
                
                $compiled_asset = '' ; 
                $compiled_asset = str_replace('*|text_01|*',$this->asset['text_01'],$this->asset['post_layout']['post_content_layout']) ; 
                $compiled_asset = str_replace('*|text_02|*',$this->asset['text_02'],$compiled_asset) ; 
                
//                $personalization_array = array(
//                    'account_record' => $this->Get_Account()
//                    ) ; 
//                
//                foreach ($this->asset as $key => $value) {
//                    $this->asset[$key] = $this->Action_Personalize_String($value,$personalization_array) ;
//                    }
                
                
                break ;                 
            default: 

                  $compiled_asset = 'Asset -> Action_Compile_Asset needs to be customized for this asset type: '.$this->asset['type_id'] ; 
            }
        
        $this->Set_Model_Timings('Asset.Action_Compile_Asset_complete_'.$this->asset['asset_id']) ; 
                
        $this->asset_compiled = $compiled_asset ; 
        

        // Set default images for certain asset types
        $primary_asset_record = $this->Get_Asset() ; 
                  
        $asset_image_update = new Asset() ; 
        $asset_image_update->Set_Replicate_User_Account_Profile($this) ;
        
        switch ($primary_asset_record['type_name']) {
            case 'email':

                $email_placeholder_featured_image_asset_id = $this->Set_System_Default_Value('asset_email_featured_image_placeholder')->Get_System_Default_Value() ;

                if ((isset($primary_asset_record['asset_images'][0]['asset_id'])) AND ($primary_asset_record['asset_id_04'] != $primary_asset_record['asset_images'][0]['asset_id'])) {

                    $this->Set_Model_Timings('Asset.Action_Compile_Asset_found_email_asset_images_1_'.$primary_asset_record['asset_images'][0]['asset_id']) ; 
        
                    $update_email_image = array(
                        'asset_id' => $primary_asset_record['asset_id'],
                        'asset_id_04' => $primary_asset_record['asset_images'][0]['asset_id']
                        ) ; 

                    if (isset($asset_input['auth_override'])) {
                        $update_email_image['auth_override'] = $asset_input['auth_override'] ; 
                        }

                    $asset_image_update->Action_Update_Content($update_email_image) ; 
                    
                    } elseif ((!isset($primary_asset_record['asset_images'][0]['asset_id'])) AND ($primary_asset_record['asset_id_04'] != $email_placeholder_featured_image_asset_id)) {

                        // Update it with the placeholder featured image for this asset type
                        $update_email_image = array(
                            'asset_id' => $primary_asset_record['asset_id'],
                            'asset_id_04' => $email_placeholder_featured_image_asset_id
                            ) ; 

                        if (isset($asset_input['auth_override'])) {
                            $update_email_image['auth_override'] = $asset_input['auth_override'] ; 
                            }

                        $asset_image_update->Action_Update_Content($update_email_image) ; 
                        }

                break ;
            case 'sms':

                $sms_placeholder_featured_image_asset_id = $this->Set_System_Default_Value('asset_sms_featured_image_placeholder')->Get_System_Default_Value() ;

                if ((isset($primary_asset_record['media_set'][0]['asset_id'])) AND ($primary_asset_record['asset_id_04'] != $primary_asset_record['media_set'][0]['asset_id'])) {

                    $update_sms_image = array(
                        'asset_id' => $primary_asset_record['asset_id'],
                        'asset_id_04' => $primary_asset_record['media_set'][0]['asset_id']
                        ) ; 

                    if (isset($asset_input['auth_override'])) {
                        $update_sms_image['auth_override'] = $asset_input['auth_override'] ; 
                        }

                    $asset_image_update->Action_Update_Content($update_sms_image) ; 
                    
                    } elseif ((!isset($primary_asset_record['media_set'][0]['asset_id']))  AND ($primary_asset_record['asset_id_04'] != $sms_placeholder_featured_image_asset_id)) {

                        // Update it with the placeholder featured image for this asset type
                        $update_sms_image = array(
                            'asset_id' => $primary_asset_record['asset_id'],
                            'asset_id_04' => $sms_placeholder_featured_image_asset_id
                            ) ; 

                        if (isset($asset_input['auth_override'])) {
                            $update_sms_image['auth_override'] = $asset_input['auth_override'] ; 
                            }

                        $asset_image_update->Action_Update_Content($update_sms_image) ; 
                        }                                
                break ;                                
            }
        

        return $this ;                 
        }
    

    
    
    public function Action_Process_Asset_Member_List() {
        
        $member_list = $this->asset_members_list['member_list'] ; 
        return $this ; 
        }
    
    
    // Insert a new child asset into the parent asset's structure
    // create_copy = yes/no.  Default is yes
    // $insert_child_params = array(
//        'new_child_asset_id' => '',
//        'delay' => xx
//        'insert_target' => '',
//        'insert_placement' => '',
//        'create_copy' => ''
//        ) ;
    public function Action_Insert_Child_Asset($insert_child_params) {   
        
        $this->Set_Model_Timings('Asset.Action_Insert_Child_Asset_to_'.$insert_child_params['asset_id'].'_begin') ; 
        
        // $insert_child_params['insert_target']   The current existing child structure_order that we will place the new child adjacent to
        // $insert_child_params['insert_placement']   Options: before or after. Determines whether new child will go before or after the insert_target
        
        
        // Set the parent asset
        $parent_asset = new Asset() ; 
        $parent_asset->Set_Replicate_User_Account_Profile($this) ; 
        $parent_additional_parameters['skip_history'] = 'yes' ; 
        $parent_asset->Set_Asset_By_ID($insert_child_params['asset_id'],$parent_additional_parameters) ; // Set the parent asset
        $data['parent_asset'] = $parent_asset->Get_Asset() ; 
        $this->Set_Model_Timings('Asset.Action_Insert_Child_Asset_parent_asset_set_for_'.$insert_child_params['asset_id']) ;
        
        
        // Parent asset latest history
        $additional_parameters['history_id'] = 'latest' ; 
        $latest_history = $parent_asset->Set_Asset_History('internal',$additional_parameters)->Get_Asset_History() ;
        $this->Set_Model_Timings('Asset.Action_Insert_Child_Asset_parent_asset_history_latest_set_for_'.$insert_child_params['asset_id']) ;
        
        
        switch ($insert_child_params['insert_target']) {
            case 'first':
                $insert_child_params['insert_target'] = '1.00' ; 
                break ;
            case 'last':
                $last_item = end($latest_history['asset_structure']) ; 
                if ($last_item['structure_order']) {
                    $insert_child_params['insert_target'] = $last_item['structure_order'] ; 
                    } else {
                        $insert_child_params['insert_target'] = '1.00' ; 
                        }
                break ; 
            default:    
            }
        
        
        // Determine what type of asset the child is...
        // If it's an email_layout, then a new asset based on that template needs to be created
        $child_additional_parameters['skip_history'] = 'yes' ;
        $this->Set_Asset_By_ID($insert_child_params['new_child_asset_id'],$child_additional_parameters) ;  // The submitted source child
        $data['new_child_asset_record'] = $this->Get_Asset() ; 
        $this->Set_Model_Timings('Asset.Action_Insert_Child_Asset_child_asset_set_complete_for_'.$insert_child_params['asset_id']) ;
        
        
        switch ($data['new_child_asset_record']['type_name']) {
            case 'email_layout': // Create new email content with submitted child as the layout                        

                $insert_child_params['new_child_asset_id'] = $data['new_child_asset_record']['email_content_template'] ; 
                $insert_child_params['create_copy'] = 'yes' ;

                $this->Set_Asset_By_ID($insert_child_params['new_child_asset_id'],$child_additional_parameters) ;  // Reset the submitted child based on content template

                break ;
            case 'post_layout': // Create new post content with submitted child as the layout                        

//                $insert_child_params['new_child_asset_id'] = $data['new_child_asset_record']['email_content_template'] ; 
                $insert_child_params['create_copy'] = 'yes' ;
                $this->Set_Asset_By_ID($insert_child_params['new_child_asset_id'],$child_additional_parameters) ;  // Reset the submitted child based on content template

                break ;                
            default:
                // Blank
            }

        
        // Create a copy of the new child if necessary
        switch ($insert_child_params['create_copy']) {
            case 'no':
                // Insert the source child as is
                // Do we need an asset id labeled $insert_child_params['new_child_asset_id']?
                
                $this->child_asset_id = $data['new_child_asset_record']['asset_id'] ; 
                $this->child_asset = $data['new_child_asset_record'] ; 
                
                break ; 
            case 'yes':
            default:

                // Overrides default draft visibility
                if (isset($insert_child_params['visibility_name'])) { 
                    $duplicate_parameters['visibility_name'] = $insert_child_params['visibility_name'] ; 
                    }
                if (isset($insert_child_params['visibility_id'])) {
                    $duplicate_parameters['visibility_id'] = $insert_child_params['visibility_id'] ; 
                    }
                if (isset($insert_child_params['child_asset_title'])) {
                    $duplicate_parameters['asset_title'] = $insert_child_params['child_asset_title'] ; 
                    }
                if (isset($insert_child_params['child_asset_subject_line_01'])) {
                    $duplicate_parameters['subject_line_01'] = $insert_child_params['child_asset_subject_line_01'] ; 
                    }                
                if (isset($insert_child_params['parent_asset_id'])) {
                    $duplicate_parameters['parent_asset_id'] = $insert_child_params['parent_asset_id'] ; 
                    }

                switch ($data['parent_asset']['type_name']) {
                    case 'series':
                        $duplicate_parameters['parent_asset_id'] = $data['parent_asset']['asset_id'] ; 
                        break ; 
                    }
                
                
                $this->test_data2['dup_params'] = $duplicate_parameters ;

                $this->Action_Duplicate_Asset($duplicate_parameters) ;
                $data['new_child_asset'] = $this->Get_Asset() ; 
                $insert_child_params['new_child_asset_id'] = $data['new_child_asset']['asset_id'] ; 
                
                $this->child_asset_id = $data['new_child_asset']['asset_id'] ; 
                $this->child_asset = $data['new_child_asset'] ; 
            }                

        

    
        // QUESTION: how are we gonna handle children?

        // Re-get Parent asset latest history
        $reget_parent_history_parameters['history_id'] = 'latest' ; 
        $latest_history = $parent_asset->Set_Asset_History($data['parent_asset']['asset_id'],$reget_parent_history_parameters)->Get_Asset_History() ;
        $this->Set_Model_Timings('Asset.Action_Insert_Child_Asset_reget_parent_asset_history_for_'.$latest_history['asset_master']['asset_id']) ;

        
        $new_structure = array() ; 
        $o = number_format(1,2) ; 

        // A structure exists. Cycle through it and insert new child before/after the target
        if (count($latest_history['asset_structure']) > 0) {
            
            $this->Set_Model_Timings('Asset.Action_Insert_Child_Asset_preprocessed_structure_update_for_'.$latest_history['asset_master']['asset_id']) ;

            foreach ($latest_history['asset_structure'] as $current_structure) {

                // Target identified
                if ($insert_child_params['insert_target'] == $o) {

                    switch ($insert_child_params['insert_placement']) {
                        case 'before': // New child goes before target

                            // This is the new child
                            $add_structure_item = array(
                                'asset_id' => $insert_child_params['asset_id'],
                                'child_asset_id' => $insert_child_params['new_child_asset_id'],
                                'structure_order' => $o
                                ) ;                                 

                            if ($insert_child_params['delay']) {
                                $add_structure_item['delay'] = $insert_child_params['delay'] ; 
                                }
                            
                            $new_structure[] = $add_structure_item ;
                            
                            $o = number_format($o + 1,2) ; 

                            // This is the existing structure item with the targeted structure_order
                            $new_structure[] = array(
                                'asset_id' => $insert_child_params['asset_id'],
                                'child_asset_id' => $current_structure['child_asset_id'],
                                'structure_order' => $o,
                                'delay' => $current_structure['delay']
                                ) ;  
                            
                            $o = number_format($o + 1,2) ; 

                            break ;
                        case 'after':

                            // This is the existing structure item with the targeted structure_order
                            $new_structure[] = array(
                                'asset_id' => $insert_child_params['asset_id'],
                                'child_asset_id' => $current_structure['child_asset_id'],
                                'structure_order' => $o,
                                'delay' => $current_structure['delay']
                                ) ; 

                            $o = number_format($o + 1,2) ; 

                            // This is the new child
                            $add_structure_item = array(
                                'asset_id' => $insert_child_params['asset_id'],
                                'child_asset_id' => $insert_child_params['new_child_asset_id'],
                                'structure_order' => $o
                                ) ;                                 

                            if ($insert_child_params['delay']) {
                                $add_structure_item['delay'] = $insert_child_params['delay'] ; 
                                }
                            
                            $new_structure[] = $add_structure_item ; 
                            
                            $o = number_format($o + 1,2) ; 


                            break ;                                 
                        case 'exact': 
                            
                            // Because the submitted child was submitted as 'exact'
                            // Then we just re-add the current structure with the current structure order, with no override changes.
                            $new_structure[] = array(
                                'asset_id' => $insert_child_params['asset_id'],
                                'child_asset_id' => $current_structure['child_asset_id'],
                                'structure_order' => $current_structure['structure_order'],
                                'delay' => $current_structure['delay']
                                ) ; 

                            $o = number_format($o + 1,2) ;
                            
                            break ; 
                        }



                    } else { // Not the target -- continue adding existing structure
                    
                        $new_structure[] = array(
                            'asset_id' => $insert_child_params['asset_id'],
                            'child_asset_id' => $current_structure['child_asset_id'],
                            'structure_order' => $o,
                            'delay' => $current_structure['delay']
                            ) ; 

                        $o = number_format($o + 1,2) ;                     

                        }
                
                    }  // End existing structure loop
            
                // If we are submitting a child with an exact insert_placement, then it won't
                // get hit in the loop of existing structure loop above
                if ($insert_child_params['insert_placement'] == 'exact') {                
                    
                    // This is the new child
                    $add_structure_item = array(
                        'asset_id' => $insert_child_params['asset_id'],
                        'child_asset_id' => $insert_child_params['new_child_asset_id'],
                        'structure_order' => $insert_child_params['insert_target']
                        ) ;                                 

                    if ($insert_child_params['delay']) {
                        $add_structure_item['delay'] = $insert_child_params['delay'] ; 
                        }

                    $new_structure[] = $add_structure_item ;                     
                    }
            
            } else {
            
                // Adding the first structure item
                $add_structure_item = array(
                    'asset_id' => $insert_child_params['asset_id'],
                    'child_asset_id' => $insert_child_params['new_child_asset_id'],
                    'structure_order' => $o
                    ) ;  

                if ($insert_child_params['structure_order']) {
                    $add_structure_item['structure_order'] = $insert_child_params['structure_order'] ; 
                    } 
            
                if ($insert_child_params['delay']) {
                    $add_structure_item['delay'] = $insert_child_params['delay'] ; 
                    } 

                if ($insert_child_params['insert_placement'] == 'exact') { // This overrides the initial structure order of 1.00
                    $add_structure_item['structure_order'] = $insert_child_params['insert_target'] ; 
                    }
            
                $new_structure[] = $add_structure_item ; 
                }

        
        $content_input['asset_id'] = $insert_child_params['asset_id'] ;
        $content_input['asset_structure'] = $new_structure ;
        $content_input['force_history_insert'] = 'yes' ; 
        

        if (isset($insert_child_params['auth_override'])) {
            $content_input['auth_override'] = $insert_child_params['auth_override'] ; 
            }
        
        $this->Set_Model_Timings('Asset.Action_Insert_Child_Asset_trigger_structure_update_for_'.$latest_history['asset_master']['asset_id']) ;
        $reorder = $this->Action_Update_Content($content_input) ;  // Trigger re-ordering of the parent
        $this->Set_Model_Timings('Asset.Action_Insert_Child_Asset_structure_update_complete_for_'.$latest_history['asset_master']['asset_id']) ;

        return $this ; 
        }
    

    
    public function Action_Remove_Child_Asset($remove_child_params) {
        
        // remove_target = the structure order number that is to be removed (x.xx)

        $this->Set_Asset_By_ID($remove_child_params['asset_id']) ; // Set the parent

        $additional_parameters['history_id'] = 'latest' ; 
        $latest_history = $this->Set_Asset_History('internal',$additional_parameters)->Get_Asset_History() ;


        $remove_target_array = Utilities::Process_Comma_Separated_String($remove_child_params['remove_target'])  ;

        
        $new_structure = array() ; 

        // how are we gonna handle children?

        $c = number_format(1,2) ; // 1.00 - Count of passes through the loop. Increments every time.
        $o = number_format(1,2) ; // 1.00 - Count of structure items. Only increments when an item is added

        foreach ($latest_history['asset_structure'] as $current_structure) {

            if (in_array($c,$remove_target_array)) {

                // Just don't add it to the array ... see what happens.                
//                $o = number_format($o + 1,2) ; 
                
                } else {

                    $new_structure[] = array(
                        'asset_id' => $remove_child_params['asset_id'],
                        'child_asset_id' => $current_structure['child_asset_id'],
                        'structure_order' => $o,
                        'delay' => $current_structure['delay']
                        ) ; 

                    $o = number_format($o + 1,2) ; 
                    }

                $c++ ; 
                }  


        $content_input['asset_id'] = $remove_child_params['asset_id'] ;
        $content_input['asset_structure'] = $new_structure ;

        if (isset($remove_child_params['auth_override'])) {
            $content_input['auth_override'] = $remove_child_params['auth_override'] ; 
            }
        
        $reorder = $this->Action_Update_Content($content_input) ;
                

        return $this ; 
        }
    
    
    
    public function Action_Duplicate_Campaign($duplicate_parameters = array()) {
        
        error_log(json_encode($duplicate_parameters)) ; 
        
        $ancestor_campaign_record = $this->Get_Campaign() ; 

        $campaign_options = array(
            'user_id' => $this->user_id,
            'account_id' => $this->account_id
            ) ; 
        
        if (isset($duplicate_parameters['campaign_status_name'])) {
            $campaign_options['campaign_status_name'] = $duplicate_parameters['campaign_status_name'] ; 
            } else {
                $campaign_options['campaign_status_name'] = 'draft' ; 
                }
    
        if (isset($duplicate_parameters['campaign_status_name'])) {
            $campaign_options['campaign_status_name'] = $duplicate_parameters['campaign_status_name'] ; 
            } else {
                $campaign_options['campaign_status_name'] = 'draft' ; 
                }
        
        
        if ($duplicate_parameters['duplicate_asset'] == 'skip') { 
            // Don't duplicate asset in this instance. Trigger a cron after campaign created.
            $campaign_options['asset_id'] = $ancestor_campaign_record['asset_id'] ; 
            }
                
        if ($duplicate_parameters['duplicate_asset'] == 'true') { 
            
            $asset = new Asset() ;
            $asset->Set_Replicate_User_Account_Profile($this) ;
            
            $duplicate_asset_params = array(
                'visibility_name' => 'visibility_deleted'
                ) ;
            
            $asset->Set_Asset_By_ID($ancestor_campaign_record['asset_id'])->Action_Duplicate_Asset($duplicate_asset_params) ; 
            $data['asset_record'] = $asset->Get_Asset() ;
            $campaign_options['asset_id'] = $data['asset_record']['asset_id'] ;                
            }

        
        if (isset($duplicate_parameters['campaign_title'])) {
            $campaign_options['campaign_title'] = $duplicate_parameters['campaign_title'] ; 
            } else {
                $formatted_title_array = Utilities::Remove_String_Right($ancestor_campaign_record['campaign_title'],' (Copy)') ;
                $campaign_options['campaign_title'] = $formatted_title_array['final_string'].' (Copy)' ;
                } 
        
        $create_campaign = $this->Action_Create_Campaign($campaign_options) ; 
                
        $new_campaign_record = $this->Get_Campaign() ; 
        $new_automation_record = $this->Get_Automation() ;    
        
        
        if ($duplicate_parameters['include_recipients'] == 'true') { 
            $campaign_update_query1['campaign_id'] = $new_campaign_record['campaign_id']  ;
            $campaign_update_query1['delivery_asset_id'] = $ancestor_campaign_record['delivery_asset_id'] ; 
            $this->Action_Update_Campaign($campaign_update_query1) ;
            
            $campaign_update_query2['campaign_id'] = $new_campaign_record['campaign_id']  ;
            $campaign_update_query2['delivery_trigger'] = $ancestor_campaign_record['delivery_trigger'] ; 
            $campaign_update_query2['structured_data_01'] = array(
                'recipients' => $ancestor_campaign_record['structured_data_01']['recipients'],
                'test_mode' => $ancestor_campaign_record['structured_data_01']['test_mode'],
                'halt_delivery' => $ancestor_campaign_record['structured_data_01']['halt_delivery'],
                ) ; 
            $this->Action_Update_Campaign($campaign_update_query2) ;
            }
        
        
        if ($duplicate_parameters['include_metadata'] == 'true') {
            $metadata_string = '' ;
            foreach ($ancestor_campaign_record['tag'] as $tag) {
                $metadata_string .= $tag['metadata_id'].',' ; 
                }
            foreach ($ancestor_campaign_record['category'] as $category) {
                $metadata_string .= $category['metadata_id'].',' ; 
                }            
            
            $campaign_metadata_update['campaign_id'] = $new_campaign_record['campaign_id']  ;
            $campaign_metadata_update['metadata_id'] = $metadata_string  ;
            $this->Action_Update_Campaign($campaign_metadata_update) ;
            }
        
        return $this ; 
        }
    
    
    
    
    // Copy an existing asset
    public function Action_Duplicate_Asset($duplicate_parameters = array()) { 
        
        $ancestor_asset_record = $this->Get_Asset() ; 
        
        $create_asset = array(
            'type_id' => $ancestor_asset_record['type_id'],
            'global_asset' => 0,
            'account_id' => $this->account_id
            ) ;        
        
        $create_asset_additional = array() ;
        
        if ((isset($ancestor_asset_record['structure'])) AND (count($ancestor_asset_record['structure']) > 0)) {  
            $create_asset_additional['skip_placeholder_child'] = 'yes' ;
            }

        
        $this->Action_Create_Asset($create_asset,$create_asset_additional) ; // At the end of action_create_asset, the new asset will be set in the model
        $new_asset_record = $this->Get_Asset() ; 
        

        // Take the ancestor slug and increment it so it is similar but unique
//        $ancestor_asset_record['asset_slug'] = 'aid-'.$new_asset_record['asset_id'].'-'.$ancestor_asset_record['asset_slug'] ;
        $slug_result = $this->Action_Validate_Asset_Slug($ancestor_asset_record) ; 
        
        $update_asset = array(
            'asset_id' => $new_asset_record['asset_id'],
            'organization_id' => $ancestor_asset_record['organization_id'],
            'asset_title' => $ancestor_asset_record['asset_title'],
            'asset_description' => $ancestor_asset_record['asset_description'],
            'asset_slug' => $slug_result['asset_slug_validated'],
            'ancestor_asset_id' => $ancestor_asset_record['asset_id'],
            'text_01' => $ancestor_asset_record['text_01'],
            'text_02' => $ancestor_asset_record['text_02'],
            'asset_id_01' => $ancestor_asset_record['asset_id_01'],
            'asset_id_02' => $ancestor_asset_record['asset_id_02'],
            'asset_id_03' => $ancestor_asset_record['asset_id_03'],
            'asset_id_04' => $ancestor_asset_record['asset_id_04'],
            'metadata_01' => $ancestor_asset_record['metadata_01'],
            'metadata_02' => $ancestor_asset_record['metadata_02'],
            'metadata_03' => $ancestor_asset_record['metadata_03'],
            'metadata_04' => $ancestor_asset_record['metadata_04'],
            'metadata_04' => $ancestor_asset_record['metadata_04'],
            'structured_data_01' => $ancestor_asset_record['structured_data_01']
            ) ; 
        
        if (isset($duplicate_parameters['parent_asset_id'])) {
            $update_asset['parent_asset_id'] = $duplicate_parameters['parent_asset_id'] ; 
            }
        
        // Could add a foreach loop here on duplicate_parameters to auto-override items
        if (isset($duplicate_parameters['visibility_name'])) { 
            $update_asset['visibility_name'] = $duplicate_parameters['visibility_name'] ; 
            }
        if (isset($duplicate_parameters['visibility_id'])) {
            $update_asset['visibility_id'] = $duplicate_parameters['visibility_id'] ; 
            }
        
        if (isset($duplicate_parameters['asset_title'])) {
            $update_asset['asset_title'] = $duplicate_parameters['asset_title'] ; 
            } else {
                $formatted_title_array = Utilities::Remove_String_Right($ancestor_asset_record['asset_title'],' (Copy)') ;
                $update_asset['asset_title'] = $formatted_title_array['final_string'].' (Copy)' ;
                }       
        
        if (isset($duplicate_parameters['subject_line_01'])) {
            $update_asset['subject_line_01'] = $duplicate_parameters['subject_line_01'] ; 
            }
        
        if ($duplicate_parameters['campaign_id']) {
            $update_asset['structured_data_01']['campaign_id'] = $duplicate_parameters['campaign_id'] ; 
            }
        
        // Clear past delivery information for duplicated asset...
        switch ($new_asset_record['type_name']) {
            case 'email':
                $update_asset['structured_data_01']['deliveries'] = 0 ; 
                $update_asset['structured_data_01']['timestamp_last_delivered'] = 0 ; 
                $update_asset['structured_data_01']['clicks'] = 0 ; 
                $update_asset['structured_data_01']['opens'] = 0 ; 
                break ;                 
            case 'sms':
                $update_asset['structured_data_01']['deliveries'] = 0 ; 
                $update_asset['structured_data_01']['timestamp_last_delivered'] = 0 ; 
                break ; 
            }
        
        $new_asset_record = $this->Action_Update_Content($update_asset)->Set_Asset_By_ID($new_asset_record['asset_id'])->Get_Asset() ; 

        
        // If the ancestor asset has a structure, duplicate it as well, creating copies for each child asset
        if ((isset($ancestor_asset_record['structure'])) AND (count($ancestor_asset_record['structure']) > 0)) {   
            
            $o = number_format(1,2) ;
            
            foreach ($ancestor_asset_record['structure'] as $structure) {

                $insert_child_params = array(
                    'asset_id' => $new_asset_record['asset_id'], // Parent asset id
                    'new_child_asset_id' => $structure['child_asset_id'],
                    'delay' => $structure['delay'],
                    'insert_target' => $structure['structure_order'],
                    'insert_placement' => 'exact', // before / after
                    'create_copy' => 'yes'
                    ) ; 
                
                if (isset($duplicate_parameters['visibility_name'])) { 
                    $insert_child_params['visibility_name'] = $duplicate_parameters['visibility_name'] ; 
                    }
                if (isset($duplicate_parameters['visibility_id'])) {
                    $insert_child_params['visibility_id'] = $duplicate_parameters['visibility_id'] ; 
                    }
                if (isset($duplicate_parameters['child_visibility_name'])) { 
                    $insert_child_params['visibility_name'] = $duplicate_parameters['child_visibility_name'] ; 
                    }
                if (isset($duplicate_parameters['child_visibility_id'])) {
                    $insert_child_params['visibility_id'] = $duplicate_parameters['child_visibility_id'] ; 
                    }
                
                // New series should have child assets assigned a parent id 
                if ($insert_child_params['create_copy'] == 'yes') {
                    switch ($new_asset_record['type_name']) {
                        case 'series':
                            $insert_child_params['parent_asset_id'] = $new_asset_record['asset_id'] ;                         
                            break ; 
                        }
                    }
                
                // Determines if child assets should be created via cron task, or immediately
                // series child assets are created via cron
                if ($duplicate_parameters['child_schedule_asset_create'] == 'yes') {  
                    
                    $insert_child_params['child_schedule_asset_create_parent_task_automation_id'] = $duplicate_parameters['child_schedule_asset_create_parent_task_automation_id'] ; 
                    
                    if ($duplicate_parameters['campaign_id']) {
                        $insert_child_params['campaign_id'] = $duplicate_parameters['campaign_id'] ; 
                        }
                    if ($duplicate_parameters['campaign_task_automation_id']) {
                        $insert_child_params['campaign_task_automation_id'] = $duplicate_parameters['campaign_task_automation_id'] ; 
                        }
                    
                    $automation_values = array(
                        'task_action' => 'child_asset_create',
                        'timestamp_action' => TIMESTAMP - 3,
                        'structured_data' => $insert_child_params                                                                    
                        ) ;
                    $automation_values['structured_data']['user_id'] = $this->user_id ;
                    $automation_values['structured_data']['account_id'] = $this->account_id ;    

                    $this->Action_Create_Cron_Task_Automation($automation_values) ;                                        
                    $result_child = $this->Get_Response() ;                 

                    $data['task_automation_id'] = $this->Get_Task_Automation_ID() ; 
                    $this->Action_Trigger_Cron_Task_Automation($data['task_automation_id']) ;

                    
                    } else {
                        $this->Action_Insert_Child_Asset($insert_child_params) ;                
                        }
                
                $o = $structure['structure_order'] ; 
                }            
            }
        
        
        
        $this->Set_Alert_Response(230) ; // Asset succesfully copied
        $personalization = array(
            'asset_record' => array(
                'type_display_name' => $new_asset_record['type_display_name']
                )
            ) ;
        $this->Append_Alert_Response($personalization,array(
            'location' => __METHOD__
            )) ;        
        
        
        $data = array(
            'create' => $create_asset,
            'update' => $update_asset,
            'new_asset' => $new_asset_record,
            'ancestor' => $ancestor_asset_record
            ) ; 
        
        return $data ; 
        }
    
    
    // Copy content from one asset to another 
    // Currently not targeting structure ... will need to address for email templates
    public function Action_Save_Asset_As($save_as_parameters = array()) {
        
        // Set the source asset and get the current data...
        $source_params['skip_history'] = 'yes' ;
        $source_asset_record = $this->Set_Asset_By_ID($save_as_parameters['source_asset_id'],$source_params)->Get_Asset() ; 
        
        // Set the asset that the source content will be saved to...
        if ($save_as_parameters['save_as_asset_id'] == 'new') {
            
            $create_asset = array(
                'type_id' => $source_asset_record['type_id'],
                'account_id' => $this->account_id
                ) ;        

            $this->Action_Create_Asset($create_asset) ; // At the end of action_create_asset, the new asset will be set in the model
            $save_as_asset_record = $this->Get_Asset() ;             
            
            $save_as_parameters['save_as_asset_id'] = $save_as_asset_record['asset_id'] ; 
            
            } else {
                $save_as_asset_record = $this->Set_Asset_By_ID($save_as_parameters['save_as_asset_id'])->Get_Asset() ;             
                }
        
        
        
        $update_asset = array(
            'asset_id' => $save_as_parameters['save_as_asset_id'],
            'organization_id' => $source_asset_record['organization_id'],
            'asset_title' => $source_asset_record['asset_title'],
            'asset_description' => $source_asset_record['asset_description'],
            'text_01' => $source_asset_record['text_01'],
            'text_02' => $source_asset_record['text_02'],
            'asset_id_01' => $source_asset_record['asset_id_01'],
            'asset_id_02' => $source_asset_record['asset_id_02'],
            'asset_id_03' => $source_asset_record['asset_id_03'],
            'asset_id_04' => $source_asset_record['asset_id_04'],
            'metadata_01' => $source_asset_record['metadata_01'],
            'metadata_02' => $source_asset_record['metadata_02'],
            'metadata_03' => $source_asset_record['metadata_03'],
            'metadata_04' => $source_asset_record['metadata_04'],
            'metadata_04' => $source_asset_record['metadata_04'],
            'structured_data_01' => $source_asset_record['structured_data_01']
            ) ; 
        
        
        
        
        foreach ($save_as_parameters as $original_source_key => $original_source_value) {
            switch ($original_source_key) {
                case 'save_as_asset_id':
                case 'source_asset_id':   
                case 'asset_id': 
                    break ;
                default: // Use all remaining defaults to override the source asset fields
                    $update_asset[$original_source_key] = $original_source_value ; 
                }
            }

        // In the event the target for saving is currently hidden or deleted, override it to draft
        switch ($save_as_asset_record['visibility_name']) {
            case 'visibility_hidden':
            case 'visibility_deleted':
                $update_asset['visibility_name'] = 'visibility_draft' ; 
                break ; 
            }
        
        
        // Clear past delivery information for saved asset...
        switch ($save_as_asset_record['type_name']) {
            case 'email':
                $update_asset['structured_data_01']['deliveries'] = 0 ; 
                $update_asset['structured_data_01']['timestamp_last_delivered'] = 0 ; 
                $update_asset['structured_data_01']['clicks'] = 0 ; 
                $update_asset['structured_data_01']['opens'] = 0 ; 
                break ;                 
            case 'sms':
                $update_asset['structured_data_01']['deliveries'] = 0 ; 
                $update_asset['structured_data_01']['timestamp_last_delivered'] = 0 ; 
                break ; 
            }
        
        
        $update_asset['asset_structure'] = array() ; // This clears the structure so duplicates aren't created
            
        
        
         
        
        // If child asset creation is going to be scheduled via cron, then save the core components now
        if ($save_as_parameters['child_schedule_asset_create'] == 'yes') {      
            $target_asset_record = $this->Action_Update_Content($update_asset)->Get_Asset() ; 
            }
        
 
        if (($source_asset_record['structure']) AND (count($source_asset_record['structure']) > 0)) {
            
            // Create the new structure
            $update_asset['asset_structure'] = array() ;
            
            $structure_asset = new Asset() ; 
            $structure_asset->Set_Replicate_User_Account_Profile($this) ; 
            
            foreach ($source_asset_record['structure'] as $structure_item) {                
                
                switch ($source_asset_record['type_name']) {
                    case 'series':
                        $child_dup_params['parent_asset_id'] = $save_as_parameters['save_as_asset_id'] ; 
                        break ; 
                    }
                
                
                if ($save_as_parameters['child_schedule_asset_create'] == 'yes') { 
                    
                    $child_dup_params['child_schedule_asset_create_parent_task_automation_id'] = $save_as_parameters['child_schedule_asset_create_parent_task_automation_id'] ; 
                    
                    $child_dup_params['asset_id'] = $save_as_parameters['save_as_asset_id'] ;
                    $child_dup_params['new_child_asset_id'] = $structure_item['child_asset_id'] ;   
                    $child_dup_params['delay'] = $structure_item['delay'] ;   
                    $child_dup_params['insert_target'] = $structure_item['structure_order'] ;   
                    $child_dup_params['insert_placement'] = 'exact' ;  
                    $child_dup_params['create_copy'] = 'yes' ;  
                    
                    if ($save_as_parameters['child_visibility_name']) {
                        $child_dup_params['child_visibility_name'] = $save_as_parameters['child_visibility_name'] ;
                        }
                    
                    $automation_values = array(
                        'task_action' => 'child_asset_create',
                        'timestamp_action' => TIMESTAMP - 3,
                        'structured_data' => $child_dup_params                                                                    
                        ) ;
                    $automation_values['structured_data']['user_id'] = $this->user_id ;
                    $automation_values['structured_data']['account_id'] = $this->account_id ;    

                    $this->Action_Create_Cron_Task_Automation($automation_values) ;                                        
                    $result_child = $this->Get_Response() ;                 

                    $data['task_automation_id'] = $this->Get_Task_Automation_ID() ; 
                    $this->Action_Trigger_Cron_Task_Automation($data['task_automation_id']) ;

                    
                    } else {
                        
                        $child_asset_source = $structure_asset->Set_Asset_By_ID($structure_item['child_asset_id'])->Get_Asset() ; 
                
                        $child_dup_params = array(
                            'visibility_id' => $child_asset_source['visibility_id']
                            ) ;
                    
                        $duplicate_result = $structure_asset->Action_Duplicate_Asset($child_dup_params) ;
                        $child_asset_record = $duplicate_result['new_asset'] ; 

                        $update_asset['asset_structure'][] = array(
                            'asset_id' => $save_as_parameters['save_as_asset_id'],
                            'child_asset_id' => $child_asset_record['asset_id'],
                            'structure_order' => $structure_item['structure_order'],
                            'delay' => $structure_item['delay']
                            ) ;                    
                    
                        }                
                
                }
            }
        
        
        // If child asset creation was not scheduled, then update asset after new structure is compiled
        if ($save_as_parameters['child_schedule_asset_create'] != 'yes') {                 
            $target_asset_record = $this->Action_Update_Content($update_asset)->Get_Asset() ; 
            }
        
        
        return $this ; 
        }
    
    
    
    
    // Create a new asset
    // default visibility = visibility_draft
    public function Action_Create_Asset($asset_input,$additional_asset_input = array()) {  
        
        $this->Set_Model_Timings('Asset.Action_Create_Asset_begin') ; 
        
        
        $continue = 1 ; 

        $account_record = $this->Get_Account()  ;
        
        if (isset($asset_input['type_name'])) {
            $this->Set_Asset_Type($asset_input['type_name'],'name') ;
            $asset_type = $this->Get_Asset_Type() ; 
            $asset_input['type_id'] = $asset_type['type']['type_id'] ; 
            } 
        
        // Set the type of asset that will be created (which will be used to map various subparts of the asset)
        if (isset($asset_input['type_id'])) {
            $this->Set_Asset_Type($asset_input['type_id']) ;
            $asset_type = $this->Get_Asset_Type() ; 
            } else {
                $continue = 0 ; 
                $this->Set_Alert_Response(105) ; // Error creating asset
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($asset_input),
                    'location' => __METHOD__
                    )) ; 
                }
        
        if ($asset_input['visibility_published'] == 'true') {
            $asset_input['visibility_name'] = 'visibility_published' ;
            } 
        if ($asset_input['visibility_published'] == 'false') {
            $asset_input['visibility_name'] = 'visibility_draft' ;
            }
        
        if ((!isset($asset_input['visibility_published'])) AND (!isset($asset_input['visibility_id'])) AND (!isset($asset_input['visibility_name']))) {
            $asset_input['visibility_name'] = 'visibility_draft' ; // Default visibility to draft.
            }
        
        
        
        if ($continue == 1) {
            
            // Set default inputs for various asset types that require them (e.g. background for an email)
            switch ($asset_input['type_id']) {
                case 5: // File
                    if (isset($asset_input['file_id'])) {
                        
                        $file = new File() ; 
                        $data['connection'] = $file->Connect()->API_Get_Connection() ; 
                        $data['file'] = $file->API_Set_File($asset_input['file_id'])->API_Get_File() ; 

                        $asset_input['location'] = $data['file']->getUrl() ; // Sets the https location of the file
                        }                                        
                    break ;                    
                case 12: // Email
                    if (!isset($asset_input['asset_title'])) {
                        $asset_input['asset_title'] = $asset_input['subject_line_01'] ; // Sets the title of the email using the subject line
                        }                    
                    if (!isset($asset_input['email_background'])) {
                        $asset_input['email_background'] = $this->Set_System_Default_Value('email_background_plain_text')->Get_System_Default_Value() ; // Plain text email background
                        }                    
                    break ;
                case 13: // Email Content Block
                    if (!isset($asset_input['email_layout'])) {
                        $asset_input['email_layout'] = $this->Set_System_Default_Value('email_layout_plain_text')->Get_System_Default_Value() ; // Plain text only layout block
                        }                    
                    break ;
                case 14: // blog post
                case 15: // product
                    if (!isset($asset_input['asset_title'])) {
//                        $asset_input['asset_title'] = 'Add a title to your blog post' ; // Sets a placeholder title of the post
                        }                    
                    if (!isset($asset_input['comments_approval'])) {
                        $asset_input['comments_approval'] = $asset_type['type_map']['additional_data']['default_comments_approval'] ; // Sets default comments approval preference
                        }
                    if (!isset($asset_input['comments_allowed'])) {
                        $asset_input['comments_allowed'] = $asset_type['type_map']['additional_data']['default_comments_allowed'] ; // Sets default comments allowed preference
//                        $default_comments_closed_increment = $asset_type['type_map']['additional_data']['default_comments_closed_increment'] ; // The increment in days / weeks to close comments after post created
//                        $asset_input['timestamp_asset_comments_closed'] = Utilities::System_Time_Manipulate($default_comments_closed_increment,'+')['time_manipulated'] ; // Close comments by default xx weeks from creation date
                        }     
                    
                    switch ($account_record['customization_disable_asset_comments']) {
                        case 'yes':
                            $asset_input['comments_allowed'] = 0 ; 
                            break ; 
                        }
                        
                    break ;                    
                case 17: // Series
                    if (isset($asset_type['type_map']['additional_data'])) {
                        if (!isset($asset_input['default_invitation_email_asset_id'])) {
                            $asset_input['default_invitation_email_asset_id'] = $this->Set_System_Default_Value('email_series_invitation')->Get_System_Default_Value() ; // Default invitation
                            }
                        if (!isset($asset_input['default_invitation_sms_asset_id'])) {
                            $asset_input['default_invitation_sms_asset_id'] = $this->Set_System_Default_Value('sms_series_invitation')->Get_System_Default_Value() ; // Default invitation
                            }
                        }
                    
                    break ;                    
                case 19: // Contact list
                case 20: // User List
                case 23: // Profiles list                    
                case 26: // Contact SERIES list    
                case 27: // Profile SERIES list 
                case 28: // User SERIES list
                    
                    if (isset($asset_type['type_map']['additional_data'])) {
                        if (!isset($asset_input['require_optin'])) {
                            $asset_input['require_optin'] = $asset_type['type_map']['additional_data']['default_require_optin'] ; // Default invitation
                            }
                        if (!isset($asset_input['allow_duplicates'])) {
                            $asset_input['allow_duplicates'] = $asset_type['type_map']['additional_data']['default_allow_duplicates'] ; // Default invitation
                            }
                        }
                    
                    break ; 
                case 21: // Email Subscriber Sublist
                case 22: // SMS Subscriber Sublist                    
                    if (isset($asset_type['type_map']['additional_data'])) {
                        if (!isset($asset_input['list_email_invitation'])) {
                            $asset_input['list_email_invitation'] = $this->Set_System_Default_Value('email_list_invitation')->Get_System_Default_Value() ; // Default invitation
                            }
                        if (!isset($asset_input['list_sms_invitation'])) {
                            $asset_input['list_sms_invitation'] = $this->Set_System_Default_Value('sms_list_invitation')->Get_System_Default_Value() ; // Default invitation
                            }
                        if (!isset($asset_input['list_email_welcome'])) {
                            $asset_input['list_email_welcome'] = $this->Set_System_Default_Value('email_list_welcome')->Get_System_Default_Value() ; // Default invitation
                            }
                        if (!isset($asset_input['list_sms_welcome'])) {
                            $asset_input['list_sms_welcome'] = $this->Set_System_Default_Value('sms_list_welcome')->Get_System_Default_Value() ; // Default invitation
                            }                        
                        $asset_input['list_description'] = $asset_type['type_map']['additional_data']['default_list_description'] ; // Default invitation 
                        }                    
                    break ;                    
                }
            }
        
        
        if ($continue == 1) {
            
            $asset_input = $this->Action_Convert_Datetime_To_Timestamp($asset_input) ;        

            $input_result = $this->Action_Process_Inputs($asset_input,$asset_type['type_map']) ;
        
            $this->Set_Model_Timings('Asset.Action_Create_Asset_inputs_processed') ; 
        
            
            $create_asset = $this->Create_Asset($input_result['asset_master_input'],$input_result['asset_content_input'],$additional_asset_input) ;
            $this->Set_Model_Timings('Asset.Action_Create_Asset_base_asset_created_'.$create_asset['parent']['insert_id']) ; 
        
            
            if ($create_asset['parent']['insert_id']) {                 
                
                $data['new_asset']['asset_id'] = $create_asset['parent']['insert_id'] ;                     
                $this->Set_Asset_By_ID($data['new_asset']['asset_id'],array('condense_asset' => 'yes')) ;             
                $data['new_asset'] = $this->Get_Asset() ; 
                $this->Set_Model_Timings('Asset.Action_Create_Asset_set_new_base_asset_'.$data['new_asset']['asset_id']) ; 
                
                $new_history = $this->Create_Asset_History() ;
                $this->Set_Model_Timings('Asset.Action_Create_Asset_asset_history_created_'.$data['new_asset']['asset_id']) ; 
                
                // Add metadata...
                if (isset($asset_input['metadata_id'])) {

                    $metadata_array = Utilities::Process_Comma_Separated_String($asset_input['metadata_id']) ; 
                    unset($asset_input['metadata_id']) ; 

                    // Add general metadata relationship...
                    if ((isset($metadata_array)) AND (count($metadata_array) > 0)) {
                        foreach ($metadata_array as $metadata_id) {
                            
                            $metadata_query = array(
                                'asset_id' => $data['new_asset']['asset_id'],
                                'metadata_id' => $metadata_id
                                ) ; 
                            $metadata_update = $this->Action_Create_Asset_Metadata_Relationship($metadata_query) ;
                            }
                        }                
                    }                
                

                
                $auth_array = array(
                    'asset_auth_role_name' => array('asset_owner'), // Owner
                    'author' => 1
                    ) ;        
                $create_authorization = $this->Create_Asset_Authorization($auth_array) ; 
                $this->Set_Model_Timings('Asset.Action_Create_Asset_asset_owner_created_'.$data['new_asset']['asset_id']) ;
                
                
                // Create public_link_id and set link auth status to blocked
                $public_link_array = $this->Set_Asset_Public_Link()->Get_Asset_Public_Link() ; 
                $this->Set_Model_Timings('Asset.Action_Create_Asset_public_link_set_'.$data['new_asset']['asset_id']) ;
                

                $link_input_array = array(
                    'asset_id' => $data['new_asset']['asset_id'],
                    'public_link_id' => $public_link_array['public_link_id'],
                    'asset_auth_role_name_public_link' => 'asset_blocked'
                    ) ; 
                $update_options = array(
                    'skip_reset' => 'yes'
                    ) ;
                $this->Action_Update_Content($link_input_array,$update_options) ;
                $this->Set_Model_Timings('Asset.Action_Create_Asset_public_link_updated_'.$data['new_asset']['asset_id']) ;
                
                
                switch ($asset_input['type_id']) {
                    case 3: // image    
                        $this->Set_Alert_Response(102) ; // New image created
                        break ;
                    case 12: // email

                        if ($additional_asset_input['skip_placeholder_child'] != 'yes') {
                        
                            // Add a content block to the new email
                            $insert_child_params = array(
                                'asset_id' => $data['new_asset']['asset_id'], // Parent asset id
                                'new_child_asset_id' => $this->Set_System_Default_Value('email_layout_plain_text')->Get_System_Default_Value(), // Plain text only layout block,
                                'insert_target' => 1.00,
                                'insert_placement' => 'before',
                                'create_copy' => 'yes',
                                'visibility_name' => 'visibility_deleted'
                                ) ;

                            if (isset($additional_asset_input['child_visibility_name'])) {
                                $insert_child_params['visibility_name'] = $additional_asset_input['child_visibility_name'] ;
                                }
                            
                            $this->Action_Insert_Child_Asset($insert_child_params) ; 
                            
                            if ($asset_input['block_01_text_01']) {
                                $new_asset_record = $this->Set_Asset_By_ID($data['new_asset']['asset_id'])->Get_Asset() ; 
                                $first_child_asset_id = $new_asset_record['structure'][0]['child_asset_id'] ; 

                                $update_child_asset_array = array(
                                    'asset_id' => $first_child_asset_id,
                                    'visibility_id' => $data['new_asset']['visibility_id'],
                                    'text_01' => $asset_input['block_01_text_01']
                                    ) ;
                                
                                $this->Action_Update_Content($update_child_asset_array) ;
                                $this->Set_Asset_By_ID($data['new_asset']['asset_id']) ; // Reset the parent asset
                                }
                            
                            }

                        break ; 
                    case 16: // event
                        $this->Set_Alert_Response(100) ; // New event created
                        break ;
                    case 17: // series
                        
                        
                        // BLANK                            
                        // THIS WAS DELETED BECAUSE SERIES LISTS WILL ONLY BE CREATED WHEN CAMPAIGN IS CREATED
        
                        
                        break ;                        
                    case 19: // Contact list
                    case 20: // User list
                    case 23: // User Profile list     
                        
                        // Create default sub-lists
                        
                        $sub_list_asset = new Asset() ; 
                        $sub_list_asset->Set_Replicate_User_Account_Profile($this) ; 
                    
                        
                        $email_sub_list_input = array(
                            'type_id' => 21, // Email Subscriber List
                            'visibility_id' => 1,
                            'list_title' => 'Broadcast Asset '.$data['new_asset']['asset_id'].' Email Sublist',
                            'list_email_invitation' => $this->Set_System_Default_Value('email_list_invitation')->Get_System_Default_Value(),
                            'list_email_welcome' => $this->Set_System_Default_Value('email_list_welcome')->Get_System_Default_Value()
                            ) ; 
                        
                        $sub_list_asset->Action_Create_Asset($email_sub_list_input) ; 
                        $data['email_sublist_asset'] = $sub_list_asset->Get_Asset() ; 
                        
                        $sms_sub_list_input = array(
                            'type_id' => 22, // SMS Subscriber List
                            'visibility_id' => 1,
                            'list_title' => 'Broadcast Asset '.$data['new_asset']['asset_id'].' SMS Sublist',
                            'list_sms_invitation' => $this->Set_System_Default_Value('sms_list_invitation')->Get_System_Default_Value(),
                            'list_sms_welcome' => $this->Set_System_Default_Value('sms_list_welcome')->Get_System_Default_Value()
                            ) ; 
                        
                        $sub_list_asset->Action_Create_Asset($sms_sub_list_input) ;
                        $data['sms_sublist_asset'] = $sub_list_asset->Get_Asset() ; 
                        
                        $update_asset_input = array(
                            'asset_id' => $data['new_asset']['asset_id'],
                            'visibility_id' => $data['new_asset']['visibility_id'],
                            'distribution_sublist_email' => $data['email_sublist_asset']['asset_id'],
                            'distribution_sublist_sms' => $data['sms_sublist_asset']['asset_id']
                            ) ; 
                        $this->Action_Update_Content($update_asset_input) ; 
                        
                        $this->Set_Alert_Response(104) ; // New asset created
                        
                        break ;
                    case 26: // Contact SERIES list     
                    case 27: // Profile SERIES list
                    case 28: // User SERIES list
                        
                        // Create default sub-lists
                        
                        $sub_list_asset = new Asset() ; 
                        $sub_list_asset->Set_Replicate_User_Account_Profile($this) ; 
                    
                        
                        $email_sub_list_input = array(
                            'type_id' => 21, // Email Subscriber List
                            'visibility_id' => 1,
                            'list_title' => 'Series Asset '.$data['new_asset']['asset_id'].' Email Sublist',
                            'list_email_invitation' => $this->Set_System_Default_Value('email_series_invitation')->Get_System_Default_Value(),
                            'list_email_welcome' => $this->Set_System_Default_Value('email_series_welcome')->Get_System_Default_Value()
                            ) ; 
                        
                        $sub_list_asset->Action_Create_Asset($email_sub_list_input) ; 
                        $data['email_sublist_asset'] = $sub_list_asset->Get_Asset() ; 
                        
                        $sms_sub_list_input = array(
                            'type_id' => 22, // SMS Subscriber List
                            'visibility_id' => 1,
                            'list_title' => 'Series Asset '.$data['new_asset']['asset_id'].' SMS Sublist',
                            'list_sms_invitation' => $this->Set_System_Default_Value('sms_series_invitation')->Get_System_Default_Value(),
                            'list_sms_welcome' => $this->Set_System_Default_Value('sms_series_welcome')->Get_System_Default_Value()
                            ) ; 
                        
                        $sub_list_asset->Action_Create_Asset($sms_sub_list_input) ;
                        $data['sms_sublist_asset'] = $sub_list_asset->Get_Asset() ; 
                        
                        $update_asset_input = array(
                            'asset_id' => $data['new_asset']['asset_id'],
                            'visibility_id' => $data['new_asset']['visibility_id'],
                            'distribution_sublist_email' => $data['email_sublist_asset']['asset_id'],
                            'distribution_sublist_sms' => $data['sms_sublist_asset']['asset_id']
                            ) ; 
                        $this->Action_Update_Content($update_asset_input) ; 
                        
                        $this->Set_Alert_Response(104) ; // New asset created
                        
                        break ;                        
                    default:
                        $this->Set_Alert_Response(104) ; // New asset created
                    }

                $this->Set_Model_Timings('Asset.Action_Create_Asset_sub_assets_created_'.$data['new_asset']['asset_id']) ;
                
                
                } else {

                    switch ($asset_input['type_id']) {
                        case 3: // image    
                            $this->Set_Alert_Response(103) ; // Error creating image
                            break ;                        
                        case 16: // event
                            $this->Set_Alert_Response(101) ; // Error creating event
                            break ;
                        default:
                            $this->Set_Alert_Response(105) ; // Error creating asset
                        }            

                    }
            }
        
        $this->Set_Model_Timings('Asset.Action_Create_Asset_complete_'.$data['new_asset']['asset_id']) ;
                
        return $this ; 
        }
    
    
    
    // Takes raw asset inputs and splits them between parent and content inputs
    // Visibility ID will take precednece over Visibility Name
    public function Action_Process_Inputs($asset_input,$map_array = array()) {   
        
        if (isset($asset_input['asset_master']) AND (count($asset_input['asset_master']) > 0)) {
            foreach ($asset_input['asset_master'] as $master_key => $master_value) {
                switch ($master_key) {
                    case 'current_history_id':
                        // Skip these fields cuz the raw input is needed....
                        break ; 
                    default:
                        $asset_input[$master_key] = $master_value ; 
                    }
                
                }
            unset($asset_input['asset_master']) ; 
            }
        
        
        // Pull the latest version of the asset
        $additional_parameters['history_id'] = 'latest' ; 
        $latest_history = $this->Set_Asset_History('internal',$additional_parameters)->Get_Asset_History() ; 
        
        
        $asset_master_input = array() ; 
        $asset_content_input = array() ; 
        $asset_structure_input = array() ; 
        $asset_input_info = array() ; 
        
    
        // Process parent inputs
        if (isset($asset_input['account_id'])) {
            $asset_master_input['account_id'] = $asset_input['account_id'] ; 
            }
        if (isset($asset_input['asset_title'])) {
            $asset_master_input['asset_title'] = $asset_input['asset_title'] ; 
            }
        if (isset($asset_input['asset_slug'])) {
            $asset_master_input['asset_slug'] = $asset_input['asset_slug'] ; 
            }    
        if (isset($asset_input['asset_description'])) {
            $asset_master_input['asset_description'] = $asset_input['asset_description'] ; 
            }
        if (isset($asset_input['type_id'])) {
            $asset_master_input['type_id'] = $asset_input['type_id'] ; 
            } 
        if (isset($asset_input['ancestor_asset_id'])) {
            $asset_master_input['ancestor_asset_id'] = $asset_input['ancestor_asset_id'] ; 
            }  
        if (isset($asset_input['parent_asset_id'])) {
            $asset_master_input['parent_asset_id'] = $asset_input['parent_asset_id'] ; 
            } 
        
        if ($asset_input['visibility_published'] == 'true') {
            $asset_input['visibility_name'] = 'visibility_published' ;
            }

        if (isset($asset_input['visibility_id'])) {
            
            $visibility_query_options['filter_by_visibility_id'] = $asset_input['visibility_id'] ; 
            $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
            $visibility_id = $visibility_result['visibility_id'] ;
            $visibility_name = $visibility_result['visibility_name'] ;
            $asset_master_input['visibility_id'] = $visibility_id ; 
            $asset_input_info['visibility_name'] = $visibility_name ; 
            } 
        if ((isset($asset_input['visibility_name'])) AND (!isset($asset_input['visibility_id']))) {
            
            $visibility_query_options['filter_by_visibility_name'] = $asset_input['visibility_name'] ; 
            $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
            $visibility_id = $visibility_result['visibility_id'] ;
            $visibility_name = $visibility_result['visibility_name'] ;             
            $asset_master_input['visibility_id'] = $visibility_id ; 
            $asset_input_info['visibility_name'] = $visibility_name ; 
            }        
        if (isset($asset_input['current_history_id'])) {
            $asset_master_input['current_history_id'] = $asset_input['current_history_id'] ; 
            }
        
        if (isset($asset_input['global_asset'])) {
            switch ($asset_input['global_asset']) {
                case true:
                case 1:
                    $asset_master_input['global_asset'] = 1 ; 
                    break ;
                case false:
                case 0:
                    $asset_master_input['global_asset'] = 0 ; 
                    break ;                    
                }            
            }
        

        if (isset($asset_input['comments_allowed'])) { 
            switch ($asset_input['comments_allowed']) {
                case true:
                case 1:
                    $asset_master_input['comments_allowed'] = 1 ; 
                    break ;
                case false:
                case 0:
                    $asset_master_input['comments_allowed'] = 0 ; 
                    break ;                    
                }
            
            }        
        
        // Comments disabled true form submit must be accompanied by a timestamp
        if (isset($asset_input['comments_disabled']))  { 
            switch ($asset_input['comments_disabled']) {
                case 'true': 
                    $asset_input['timestamp_asset_comments_closed'] = $asset_input['timestamp_asset_comments_closed'] ;
                    break ;
                case 'false':                    
                    $asset_input['timestamp_asset_comments_closed'] = 0 ; 
                    break ; 
                }
            }        
        
        if (isset($asset_input['timestamp_asset_comments_closed'])) {
            $asset_master_input['timestamp_asset_comments_closed'] = $asset_input['timestamp_asset_comments_closed'] ; 
            }    

        if (isset($asset_input['timestamp_asset_published'])) {
            $asset_master_input['timestamp_asset_published'] = $asset_input['timestamp_asset_published'] ; 
            }
        
        if (isset($asset_input['asset_auth_role_name_public_link'])) {
            $asset_auth_role_name_public_link = $asset_input['asset_auth_role_name_public_link'] ; 
            
            $query_visibility_array = array(
                'table' => 'system_asset_auth_roles',
                'fields' => "*",
                'where' => "system_asset_auth_roles.asset_auth_role_name='$asset_auth_role_name_public_link'"
                ) ;
            $link_auth_result = $this->DB->Query('SELECT',$query_visibility_array)['results'] ; 
            $asset_master_input['public_link_asset_auth_id'] = $link_auth_result['asset_auth_id'] ;             
            }        
        if (isset($asset_input['public_link_asset_auth_id'])) {
            $asset_master_input['public_link_asset_auth_id'] = $asset_input['public_link_asset_auth_id'] ; 
            }
        
        if (isset($asset_input['public_link_id'])) {
            $asset_master_input['public_link_id'] = $asset_input['public_link_id'] ; 
            } 
        
        
        if (isset($asset_input['timestamp_asset_published'])) {
            switch ($visibility_result['visibility_name']) {
                case 'visibility_shared':
                case 'visibility_published':
                    $asset_master_input['timestamp_asset_published'] = $asset_input['timestamp_asset_published'] ; 
                    break ; 
                }            
            }        
        
        
        // Process structure input
        if (isset($asset_input['asset_structure'])) { 
            $asset_structure_input = $asset_input['asset_structure'] ; 
            } else {
                $asset_structure_input = $latest_history['asset_structure'] ; 
                }
        

        // Process content inputs
        if (isset($asset_input['text_01'])) {
            $asset_content_input['text_01'] = $asset_input['text_01'] ; 
            }
        if (isset($asset_input['text_02'])) {
            $asset_content_input['text_02'] = $asset_input['text_02'] ; 
            }        
        if (isset($asset_input['asset_id_01'])) {
            $asset_content_input['asset_id_01'] = $asset_input['asset_id_01'] ; 
            }
        if (isset($asset_input['asset_id_02'])) {
            $asset_content_input['asset_id_02'] = $asset_input['asset_id_02'] ; 
            }       
        if (isset($asset_input['asset_id_03'])) {
            $asset_content_input['asset_id_03'] = $asset_input['asset_id_03'] ; 
            }
        if (isset($asset_input['asset_id_04'])) {
            $asset_content_input['asset_id_04'] = $asset_input['asset_id_04'] ; 
            }        
        if (isset($asset_input['metadata_01'])) {
            $asset_content_input['metadata_01'] = $asset_input['metadata_01'] ; 
            }        
        if (isset($asset_input['metadata_02'])) {
            $asset_content_input['metadata_02'] = $asset_input['metadata_02'] ; 
            } 
        if (isset($asset_input['metadata_03'])) {
            $asset_content_input['metadata_03'] = $asset_input['metadata_03'] ; 
            } 
        if (isset($asset_input['metadata_04'])) {
            $asset_content_input['metadata_04'] = $asset_input['metadata_04'] ; 
            } 
              
        
        
        if (count($map_array['parent']) > 0) { 
            foreach ($map_array['parent'] as $key => $value) {
                if (isset($asset_input[$value])) {
                    $asset_master_input[$key] = $asset_input[$value] ; 
                    }
                }
            }
        
        if (count($map_array['content']) > 0) { 
            foreach ($map_array['content'] as $key => $value) {
                if (isset($asset_input[$value])) {
                    $asset_content_input[$key] = $asset_input[$value] ; 
                    }
                }
            }         
                
            
        
        // Map structured data
        // Grab content that was input as a full array under structured data
        if (count($asset_input['structured_data_01']) > 0){  
            foreach ($asset_input['structured_data_01'] as $key => $value) {
                $asset_input[$key] = $value ; 
                }
            }
        
        if (count($map_array['structured_data']) > 0){  
            foreach ($map_array['structured_data'] as $structured_key => $structured_value) {
                
                if (count($map_array['structured_data'][$structured_key]) > 0) { 
        
                    $structured_input_array = json_decode($latest_history['asset_content'][$structured_key],1) ;           
                     
//                    $structured_input_array = $this->asset[$structured_key] ; // Retrieve the existing array of data so that values which are not set aren't lost
                    
                    foreach ($map_array['structured_data'][$structured_key] as $key => $value) {                        
                        if (isset($asset_input[$value])) {
                            $structured_input_array[$value] = $asset_input[$value] ; 
                            }                        
                        }                    
                    $asset_content_input[$structured_key] = json_encode($structured_input_array) ;
                    }
                 
                }

            }
        

        if (isset($asset_input['force_history_insert'])) {
            $asset_input_info['force_history_insert'] = $asset_input['force_history_insert'] ; 
            }       
        if (isset($asset_input['asset_input_info']['rewrite_latest_history'])) {
            $asset_input_info['rewrite_latest_history'] = $asset_input['asset_input_info']['rewrite_latest_history'] ; 
            } 
    
            
        $result = array(
            'asset_master_input' => $asset_master_input,
            'asset_content_input' => $asset_content_input,
            'asset_structure_input' => $asset_structure_input,
            'asset_input_info' => $asset_input_info
            ) ;
        
        return $result ; 
        }
    

    public function Action_Delete_Related_Asset_Relationship($related_asset_input) {
        
        $asset_record = $this->Get_Asset() ; 
        
        $related_assets_to_remove_array = Utilities::Process_Comma_Separated_String($related_asset_input['related_asset_id']) ; 
        
        $related_input_string = '' ; 
        
        foreach ($asset_record['related_assets'] as $existing_related) {
            if (!in_array($existing_related['asset_id'],$related_assets_to_remove_array)) {
                $related_input_string .= $existing_related['asset_id'].',' ;
                }           
            }
        
        $asset_input = array(
            'asset_id' => $this->asset_id,
            'related_assets' => $related_input_string
            ) ; 
        
        $this->test_data = $asset_input ; 
        $this->Action_Update_Content($asset_input) ;
        
        return $this ; 
        }
    
    
    public function Action_Create_Related_Assets($related_asset_input) {
        
        $asset_record = $this->Get_Asset() ; 
        
        $new_related_id_array = Utilities::Process_Comma_Separated_String($related_asset_input['related_asset_id']) ; 
        
        $eligible_add_id_array = array() ; 
        
        foreach ($new_related_id_array as $new_related_id) {            
            $test_existing_related = Utilities::Deep_Array($asset_record['related_assets'], 'asset_id', $new_related_id) ; 
            if (!isset($test_existing_related_id['asset_id'])) {
                $eligible_add_id_array[] = $new_related_id ; 
                }            
            }
        
        
        $related_input_string = '' ; 
        
        foreach ($asset_record['related_assets'] as $existing_related) {            
            $related_input_string .= $existing_related['asset_id'].',' ;
            }
        foreach ($eligible_add_id_array as $eligible_add) {            
            $related_input_string .= $eligible_add.',' ;
            }

        
        $asset_input = array(
            'asset_id' => $this->asset_id,
            'related_assets' => $related_input_string
            ) ; 
        
//        $this->test_data = $asset_input ; 
        $this->Action_Update_Content($asset_input) ;
        
        
        return $this ;
        }
    
    
    public function Action_Update_Content($asset_input,$update_options = array()) {     

        $continue = 1 ; 
        
        if (!isset($asset_input['asset_id'])) {   
            $continue = 0 ; 
            $this->Set_Alert_Response(107) ; // Error updating asset; Missing asset_id in submission
            } else {            
                $asset_id_array = Utilities::Process_Comma_Separated_String($asset_input['asset_id']) ;
                $asset_id_count = count($asset_id_array) ; 
                }
        
        // Don't allow the account_id for an asset to be updated through this method
        if (isset($asset_input['account_id'])) { 
            unset($asset_input['account_id']) ;
            }
        
        
        if ($asset_input['visibility_published'] == 'true') {
            $asset_input['visibility_name'] = 'visibility_published' ;
            } 
        if ($asset_input['visibility_published'] == 'false') {
            $asset_input['visibility_name'] = 'visibility_draft' ;
            }        
        
        if ($asset_input['visibility_name'] == 'visibility_draft') { 
            
            unset($asset_input['datetime_timestamp_asset_published']) ; 
            }
        
        if ($asset_input['visibility_name'] == 'visibility_published_immediate') { 
            
            $asset_input['visibility_name'] = 'visibility_published' ;
            unset($asset_input['datetime_timestamp_asset_published']) ; 
            }
        
        if ($asset_input['visibility_name'] == 'visibility_scheduled') {  
            $asset_input['visibility_name'] = 'visibility_published' ;
            
            if (!$asset_input['datetime_timestamp_asset_published']) {
                $asset_input['timestamp_asset_published'] = TIMESTAMP  ;
                }            
            }
        
        $asset_input_raw = $asset_input ;  
        
        if ($continue == 1) { 
            
            foreach ($asset_id_array as $this_asset) { 
                
                $asset_input = $asset_input_raw ;                 
                $asset_input['asset_id'] = $this_asset ; // Reset the input array to the actual asset ID (as opposed to array)
                
//                $additional_parameters['condense_asset'] = 'yes' ; // This is needed so we don't max out the database query.
                $asset_record = $this->Set_Asset_By_ID($asset_input['asset_id'],$additional_parameters)->Get_Asset() ; // Get the CURRENT master status
                $this->Set_Model_Timings('###_Complete_Trigger_First_Set_Asset') ;
                
                $authorization['auth_result'] = $this->Action_Test_Asset_Authorization_Control('edit_content')  ;

                if (($authorization['auth_result'] == 'deny') AND ($asset_input['auth_override'] != 'yes')) { 
                    $continue = 0 ; // Exit out of update if auth not allowed.
                    }                
                
                $asset_input = $this->Action_Convert_Datetime_To_Timestamp($asset_input) ;        

                if (isset($asset_input['timestamp_marketing_autodelivery_create'])) {
                    if ($asset_input['timestamp_marketing_autodelivery_create'] < (Utilities::System_Time_Manipulate('3 h','+')['time_manipulated'])) {
                        $continue = 0 ;
                        $alert_id = 282 ; 
                        }
                    }
                
                 
                if ($continue == 1) {
                    
                    if ($asset_input['force_core_rewrite'] == 'yes') {  
                        $asset_input['visibility_id'] = $asset_record['visibility_id'] ;
                        }
                                        
                    
                    switch ($asset_record['type_name']) { 
                        case 'sms': 
                            
                            if ($asset_input['text_01']) {
                                $asset_input['message_content'] = $asset_input['text_01'] ; 
                                }
                            
                            if ($asset_input['message_content'])  { 
                                
                                $remove_breaks = array(
                                    'search_for' => '<br>',
                                    'replace_with' => ''
                                    ) ; 
                                
                                $asset_input['message_content'] = Utilities::Format_Convert_Spaces($asset_input['message_content'],$remove_breaks) ; 
                                $asset_input['message_content'] = Utilities::Format_Convert_Spaces(strip_tags($asset_input['message_content'])) ; 
                                
                                $temp_replace_amper = array(
                                    'search_for' => '&amp;',
                                    'replace_with' => '&'
                                    ) ;
                                $asset_input['message_content'] = Utilities::Format_Convert_Spaces($asset_input['message_content'],$temp_replace_amper) ; 
                                                                
                                }
                            
                            break ; 
                        }
                    
                    
                    
                    if (isset($asset_input['asset_publish_selector'])) {
                        switch ($asset_input['asset_publish_selector']) {
                            case 'schedule':

                                break ;                        
                            case 'immediate': 
                            default:  
                                $asset_input['timestamp_asset_published'] = 0 ; 
                            }            
                        }


                    if (isset($asset_input['asset_slug'])) {

                        $slug_result = $this->Action_Validate_Asset_Slug($asset_input) ; 

                        $asset_input['asset_slug'] = $slug_result['asset_slug_validated'] ; 
                        $personalization['asset_record'] = $slug_result ;             

                        // Prevent duplicate slugs for select asset types
                        if ($slug_result['slug_edited'] == 'yes')  {
                            switch ($asset_record['type_name']) {
                                case 'blog_post': 
                                case 'product':     
                                case 'text':     
                                    $continue = 0 ;
                                    $this->Set_Alert_Response(205) ; // Slug taken by another asset
                                    $this->Append_Alert_Response($personalization,array(
                                        'admin_context' => json_encode($asset_input),
                                        'location' => __METHOD__
                                        )) ;
                                    break ; 
                                }
                            }
                        }


                    $prior_timestamp_updated = $asset_record['timestamp_asset_updated']  ;         
                    $map_array = $asset_record['map_array'] ;


                    if ($continue == 1) {

                        $update_core = 0 ; // Trigger to determine if we update assets and asset_content tables (Versus just history)

                        if ((isset($asset_input['visibility_id'])) OR (isset($asset_input['visibility_name']))) {
                            $update_core = 1 ; 
                            }

                        if (isset($asset_input['metadata_id'])) { 

                            $metadata_array = Utilities::Process_Comma_Separated_String($asset_input['metadata_id']) ; 
                            unset($asset_input['metadata_id']) ; 

                            // Add general metadata relationship...
                            if ((isset($metadata_array)) AND (count($metadata_array) > 0)) {
                                foreach ($metadata_array as $metadata_id) {
                                    $metadata_query = array(
                                        'asset_id' => $asset_input['asset_id'],
                                        'metadata_id' => $metadata_id
                                        ) ; 
                                    $metadata_update = $this->Action_Create_Asset_Metadata_Relationship($metadata_query) ; 
                                    }
                                }                
                            }             


                        // Create or update the asset history
                        $asset_history_update = $this->Update_Asset_History($this->asset_id,$asset_input,$prior_timestamp_updated) ;        
                        $this->Set_Model_Timings('Asset.Action_Update_Content_update_asset_history_complete_'.$this->asset_id) ;
//                        $this->test_data2 = $asset_history_update ;

                        // Determine if should update core based on whether the visibility of the update (post history processing)
                        // is the same as the current core.
                        // e.g. Asset never published, in Draft. Draft visibility wasn't submitted
                        // with input, but that was the default when version history processed
                        if ($asset_record['visibility_id'] == $asset_history_update['history_visibility_id']){
                            $update_core = 1 ;
                            }

                        if ($update_core == 1) { // Save over existing core                


                            $continue = 1 ;

                            // Pull the latest version of the asset
                            $additional_parameters['history_id'] = $asset_history_update['history_id'] ; 
                            $revised_history = $this->Set_Asset_History('internal',$additional_parameters)->Get_Asset_History() ;                

                            $revised_history_input = $revised_history['asset_content'] ; 
                            $revised_history_input['current_history_id'] = $asset_history_update['history_id'] ; 
                            $revised_history_input['asset_master'] = $revised_history['asset_master'] ; 
                            $revised_history_input['asset_structure'] = $revised_history['asset_structure'] ; 
                            $revised_history_input = $this->Action_Process_Inputs($revised_history_input,$map_array) ; 

                            if (isset($revised_history_input['asset_master_input']['asset_slug'])) {

                                $slug_result = $this->Action_Validate_Asset_Slug($revised_history_input['asset_master_input']) ; 

                                $asset_input['asset_slug'] = $slug_result['asset_slug_validated'] ; 
                                $personalization['asset_record'] = $slug_result ;             

                                // Prevent duplicate slugs for select asset types
                                if ($slug_result['slug_edited'] == 'yes')  {
                                    switch ($asset_record['type_name']) {
                                        case 'blog_post': 
                                            $continue = 0 ;
                                            $this->Set_Alert_Response(205) ; // Slug taken by another asset
                                            $this->Append_Alert_Response($personalization,array(
                                                'admin_context' => json_encode($asset_input),
                                                'location' => __METHOD__
                                                )) ;
                                            break ; 
                                        }
                                    }
                                }                



                            if ($continue == 1) {

                                $result = $this->Update_Asset($revised_history_input['asset_master_input'],$revised_history_input['asset_content_input']) ;                                                

                                if (isset($revised_history_input['asset_structure_input'])) {
                                    $result_structure = $this->Update_Asset_Structure($revised_history_input['asset_structure_input']) ; 
                                    $this->Set_Model_Timings('Asset.Action_Update_Content_structure_updated_for_'.$revised_history_input['asset_master']['asset_id'].'_HELLLLLOOOOOO') ;
                                    }

                                if (!isset($result['asset_master_update']['error']) AND !isset($result['asset_master_update']['error'])) {
                                    $this->Set_Alert_Response(106) ; // Asset updated
                                    } else {
                                        $this->Set_Alert_Response(107) ; // Generic asset update error
                                        $this->Append_Alert_Response('none',array(
                                            'admin_context' => json_encode($result['asset_master_update']),
                                            'location' => __METHOD__
                                            )) ;            
                                        }                    
                                }

                            } else {
                                // Force an update of timestamp_asset_updated
                                // even though history was the only thing updated (not core)
                                $input_result['asset_master_input'] = array() ; 
                                $input_result['asset_content_input'] = array() ; 
                                $input_result['asset_master_input']['timestamp_asset_updated'] = TIMESTAMP ;
                                $result = $this->Update_Asset($input_result['asset_master_input'],$input_result['asset_content_input']) ;                                
                                }



                        }
                    } // End auth control loop
                }
            
            
            // Compile email / sms asset and set a primary featured image...
            if ($asset_id_count == 1) {

                if ($update_options['skip_reset'] != 'yes') {
                    $this->Set_Asset_By_ID($asset_id_array[0],array('condense_asset' => 'yes')) ;     
                    }                
                $primary_asset_record = $this->Get_Asset() ; 
                
                switch ($primary_asset_record['type_name']) {
                    case 'email':
                    case 'sms':        
                        
                        // you are working here 
                        // NEED A FASTER WAY TO SET A PRIMARY FEATURED IMAGE
                        
//                        $this->Action_Compile_Asset() ; 
//                        $primary_asset_record = $this->Get_Asset() ; 
                        
                        switch ($primary_asset_record['type_name']) {
                            case 'email':
                                
//                                $email_placeholder_featured_image_asset_id = $this->Set_System_Default_Value('asset_email_featured_image_placeholder')->Get_System_Default_Value() ;
//                                            
//                                if ((isset($primary_asset_record['asset_images'][0]['asset_id'])) AND ($primary_asset_record['asset_id_04'] != $primary_asset_record['asset_images'][0]['asset_id'])) {
//
//                                    $update_email_image = array(
//                                        'asset_id' => $asset_id_array[0],
//                                        'asset_id_04' => $primary_asset_record['asset_images'][0]['asset_id']
//                                        ) ; 
//
//                                    if (isset($asset_input['auth_override'])) {
//                                        $update_email_image['auth_override'] = $asset_input['auth_override'] ; 
//                                        }
//
//                                    $this->Action_Update_Content($update_email_image) ; 
//                                    } elseif ((!isset($primary_asset_record['asset_images'][0]['asset_id']))  AND ($primary_asset_record['asset_id_04'] != $email_placeholder_featured_image_asset_id)) {
//                                        
//                                        // Update it with the placeholder featured image for this asset type
//                                        $update_email_image = array(
//                                            'asset_id' => $asset_id_array[0],
//                                            'asset_id_04' => $email_placeholder_featured_image_asset_id
//                                            ) ; 
//
//                                        if (isset($asset_input['auth_override'])) {
//                                            $update_email_image['auth_override'] = $asset_input['auth_override'] ; 
//                                            }
//
//                                        $this->Action_Update_Content($update_email_image) ; 
//                                        }
                                
                                break ;
                            case 'sms':
                                
//                                $sms_placeholder_featured_image_asset_id = $this->Set_System_Default_Value('asset_sms_featured_image_placeholder')->Get_System_Default_Value() ;
//                
//                                if ((isset($primary_asset_record['media_set'][0]['asset_id'])) AND ($primary_asset_record['asset_id_04'] != $primary_asset_record['media_set'][0]['asset_id'])) {
//
//                                    $update_sms_image = array(
//                                        'asset_id' => $asset_id_array[0],
//                                        'asset_id_04' => $primary_asset_record['media_set'][0]['asset_id']
//                                        ) ; 
//
//                                    if (isset($asset_input['auth_override'])) {
//                                        $update_sms_image['auth_override'] = $asset_input['auth_override'] ; 
//                                        }
//
//                                    $this->Action_Update_Content($update_sms_image) ; 
//                                    } elseif ((!isset($primary_asset_record['media_set'][0]['asset_id']))  AND ($primary_asset_record['asset_id_04'] != $sms_placeholder_featured_image_asset_id)) {
//                                        
//                                        // Update it with the placeholder featured image for this asset type
//                                        $update_sms_image = array(
//                                            'asset_id' => $asset_id_array[0],
//                                            'asset_id_04' => $sms_placeholder_featured_image_asset_id
//                                            ) ; 
//
//                                        if (isset($asset_input['auth_override'])) {
//                                            $update_sms_image['auth_override'] = $asset_input['auth_override'] ; 
//                                            }
//
//                                        $this->Action_Update_Content($update_sms_image) ; 
//                                        }                                
                                break ;                                
                            }

                        
                        
                        break ; 
                    }                                
                }
            
            }
        
        
        if ($alert_id) {
            $this->Set_Alert_Response($alert_id) ; //             
            }
        
        return $this ; 
        }
    


    
    public function Action_Update_Comment($comment_input = array()) { 
        
        global $server_config ;
             
        $continue = 1 ; 

        if (!isset($comment_input['comment_id'])) {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {
            
            $comment_id_array = Utilities::Process_Comma_Separated_String($comment_input['comment_id']) ;
            unset($comment_input['comment_id']) ; 
            
            foreach ($comment_id_array as $comment_id) {
                
                $comment = $this->Set_Comment_By_ID($comment_id)->Get_Comment() ; 
                
                // Change status of comment
                if (isset($comment_input['comment_status'])) {                

                    if ($comment['relationship_id']) {
                        
                        $comment_status_relationship = array(
                            'relationship_id' => $comment['relationship_id'],
                            'item_id' => $comment_id,
                            'metadata_id' => $comment_input['comment_status'],
                            'relationship_type' => 'comment'
                            ) ; 

                        $this->Action_Create_Metadata_Relationship($comment_status_relationship) ;
                        
                        } else {
                        
                            $comment_status_relationship = array(
                                'item_id' => $comment_id,
                                'metadata_id' => $comment_input['comment_status'],
                                'relationship_type' => 'comment'
                                ) ; 

                            $this->Action_Create_Metadata_Relationship($comment_status_relationship) ;                        
                            }

                    }                
                                
                }

            $this->Set_Alert_Response(239) ; // Success - Comment updated
            }
        
        return $this ; 
        }
    
    
    
    public function Action_Notify_Comment($input) {
        
        $continue = 1 ; 
        
        
        $data['account_record'] = $this->Get_Account() ;
        $data['comment_record'] = $this->Set_Comment_By_ID($input['comment_id'])->Get_Comment() ;
        
        $user = new User($data['account_record']['account_owner']['user_id']) ; 
        $data['user_record'] = $user->Get_User() ; 
        
        switch ($input['notification_type']) {
            case 'moderate':
                $comment_notification_asset_id = $this->Set_System_Default_Value('user_comment_notification_moderate')->Get_System_Default_Value() ;
                break ; 
            default:  
                $comment_notification_asset_id = $this->Set_System_Default_Value('user_comment_notification')->Get_System_Default_Value() ;        
            }
                
        
        $app_account_support_profile_id = $this->Set_System_Default_Value('app_account_support')->Get_System_Default_Value() ;
        
//        $user_input = array(
//            'user_id' => $data['account_record']['account_owner']['user_id'],
//            'email_verified' => 1,
//            'email_address_id' => $data['account_record']['account_owner']['email_address_id']
//            ) ;
        

        // Get app default email domain...
        $domain_query_options = array(
            'app_default_domain' => 1
            ) ;
        
        $this->Set_System_List('app_email_domains',$domain_query_options) ; 
        $email_domain = $this->Get_System_List('app_email_domains')[0] ; // Email app domain
        
        $email = new Email() ;
        $email->Set_Sending_Domain($email_domain['domain_display_name'])->Set_User_By_Profile_ID($app_account_support_profile_id) ; 
        $email->Action_Compile_Email($comment_notification_asset_id)->Action_Personalize_Email() ;

        $campaign_options = array(
            'asset_id' => $comment_notification_asset_id,
            'campaign_status_name' => 'deleted',
            'automation_status_name' => 'scheduled',
            'timestamp_campaign_scheduled' => TIMESTAMP,
            'campaign_title' => 'Comment Notification for '.$data['account_record']['account_display_name'].' (ID: '.$data['account_record']['account_id'].')',
            'timestamp_next_action' => TIMESTAMP,
            'delivery_trigger' => 'static'
            ) ; 

        $email->Action_Create_Campaign($campaign_options) ;
        $email_campaign = $email->Get_Campaign() ;
        
        // Update the campaign with this specific static recipient...
        $recipient_variables = $data['user_record'] ;
        
        
        $campaign_update_options['campaign_id'] = $email_campaign['campaign_id'] ; 
        $campaign_update_options['structured_data_01'] = $email_campaign['structured_data_01'] ; 
        $campaign_update_options['structured_data_01']['recipients']['to']['static']['email_address'][] = $data['user_record']['email_address'] ;
        $campaign_update_options['structured_data_01']['recipients']['to']['static']['recipient_variables'][$data['user_record']['email_address']] = $recipient_variables ;
        $campaign_update_options['structured_data_01']['personalization_keys'] = array(
            'comment_id' => $data['comment_record']['comment_id'],
            'asset_id' => $data['comment_record']['asset_id'],
            'account_id' => $data['account_record']['account_id']
            )  ;
        
        $email->Action_Update_Campaign($campaign_update_options) ; 
        
        $automation_record = $email->Get_Automation() ; 
        $email->Action_Trigger_Automation_Queue($automation_record['automation_id']) ;        
        
        
        return ; 
        }
    
    
    public function Action_Create_Comment($input) {

        $continue = 1 ; 

        // Honeypot -- if filled out, do not add comment.
        if ($input['form_email_address']) {
            $this->Set_Alert_Response(243) ; // Comment submitted and submitted for moderation
            $continue = 0 ; 
            }
        
        if ($input['email_address']) {
            $validate_email_address = Utilities::Validate_Email_Address($input['email_address']) ; 
            if ($validate_email_address['valid'] == 'no') {
                $continue = 0 ; 
                $this->Set_Alert_Response(96) ; // Invalid email
                } else {
                    $input['email_address'] = $validate_email_address['email_address'] ; 
                    }         
            }
        
        
        if ($continue == 1) {
            // Get comment status options...
            $status_metadata_list_options['filter_by_metadata_type'] = 'comment_status' ; 
            $status_metadata_list_options['allow_global_metadata'] = 'only' ;
            $status_metadata_list_options['order_by'] = 'metadata.metadata_name' ; 
            $status_metadata_list_options['order'] = 'ASC' ; 

            $this->Set_Metadata_List($status_metadata_list_options) ;          
            $data['comment_status_metadata_list'] = $this->Get_Metadata_List() ;        


            $data['asset_record'] = $this->Get_Asset() ; 
            switch ($data['asset_record']['comments_approval']) {
                case 'manual':
                    $comment_status = 'comment-unmoderated' ;
                    break ;                 
                default:    
                    $comment_status = 'comment-approved' ;
                }

            $comment_status_metadata = Utilities::Deep_Array($data['comment_status_metadata_list'],'metadata_slug',$comment_status) ;


            $contact = new Contact() ; 
            
            // Only create a contact (or attempt to associate with existing contact) if an 
            // email address or phone number was provided
            if (($input['email_address']) OR ($input['phone_number'])) {
                // Create contact array
                $contact_input = array() ; 

                foreach ($input as $key => $value) {
                    switch ($key) {
                        case 'full_name':
                        case 'first_name':
                        case 'last_name':
                        case 'email_address':
                        case 'phone_number':
                        case 'created_source':    
                            $contact_input[$key] = $value ; 
                            break ; 
                        }            
                    }
                
                
                $contact->Set_User_By_Profile_ID($this->profile_id)->Action_Create_Contact($contact_input) ; 
                $create_contact = $contact->Get_Contact_Create_Set() ;                
                }
        

            if ($create_contact['success_set'][0]['contact_id']) {
                $input['commenter_id'] = $create_contact['success_set'][0]['contact_id'] ;  // Add the contact ID to the input array  
                $input['commenter_type'] = 'contact' ; 
                } else if ($create_contact['alert_set'][0]['contact_id']) {
                $input['commenter_id'] = $create_contact['alert_set'][0]['contact_id'] ; // Add the contact ID to the input array    
                $input['commenter_type'] = 'contact' ;     
                } else {
                    $input['commenter_id'] = 0 ;     
                    $input['commenter_type'] = 'anonymous' ;   
                    }

            if ($input['full_name']) {
                $split_names = $contact->Action_Split_Names($input['full_name']) ;  
                $input['first_name'] = $split_names['first_name'] ;
                $input['last_name'] = $split_names['last_name'] ;
                }
            
            $result = $this->Create_Comment($input) ;

            if (!isset($result['error'])) {

                $comment_status_relationship = array(
                    'item_id' => $result['insert_id'],
                    'metadata_id' => $comment_status_metadata['metadata_id'],
                    'relationship_type' => 'comment'
                    ) ; 

                $this->Action_Create_Metadata_Relationship($comment_status_relationship) ;        


                $this->Set_Comment_By_ID($create_comment['insert_id']) ;            
                $data['comment_record'] = $this->Get_Comment() ; 
                $data['account_record'] = $this->Get_Account() ; 
                switch ($data['comment_record']['metadata_slug']) {
                    case 'comment-approved':

                        switch ($data['account_record']['notification_website_comments']) {
                            case 'moderation_only':
                            case 'none':
                                // Do not notify for these
                                break ; 
                            case 'all':
                            default:    
                                $comment_notify = array(
                                    'comment_id' => $data['comment_record']['comment_id'],
                                    'notification_type' => 'auto'
                                    ) ; 

                                $this->Action_Notify_Comment($comment_notify) ;                                
                            }
                        
                        $this->Set_Alert_Response(240) ; // Comment submitted and approved
                        
                        break ; 
                    case 'comment-unmoderated':
                    default: 
                        
                        switch ($data['account_record']['notification_website_comments']) {
                            case 'none':
                                // Do not notify for these
                                break ; 
                            case 'moderation_only':
                            case 'all':
                            default:    
                                $comment_notify = array(
                                    'comment_id' => $data['comment_record']['comment_id'],
                                    'notification_type' => 'moderate'
                                    ) ; 

                                $this->Action_Notify_Comment($comment_notify) ;                                
                            }                        
                        
                        $this->Set_Alert_Response(241) ; // Comment submitted and submitted for moderation
                        break ;

                    }

                
                    // Asset comments
                    $list_query = new Asset($data['account_record']['account_owner']['user_id']) ; 
                    $list_query->Set_Account_ID($data['account_record']['account_id']) ;             

                    $master_list_query_options = array(
                        'filter_by_account_id' => 'yes',
                        'override_paging' => 'yes'
                        ) ; 
                    $list_query->Set_Comment_List($master_list_query_options) ;           
                    $data['asset_comment_count'] = $list_query->Get_Comment_Count() ;                 
                
                
                
                    $pusher = new Alert() ; 
                    $pusher->Connect() ; 
                                                
                    $pusher_count_message['channel_name'] = 'channel-account-'.$data['account_record']['account_id'] ;
                    $pusher_count_message['event_name'] = 'asset-comment-count' ;    
                    $pusher_count_message['message'] = array(
                        'asset_comment_count' => $data['asset_comment_count']
                        ) ;
                
                    $pusher->API_Action_Trigger($pusher_count_message) ; 
                                    
                    $pusher_comment_message['channel_name'] = 'channel-account-'.$data['account_record']['account_id'] ;
                    $pusher_comment_message['event_name'] = 'asset-comment' ;    
                    $pusher_comment_message['message'] = array(
                        'comment_record' => $data['comment_record']
                        ) ;
                
                    $pusher->API_Action_Trigger($pusher_comment_message) ; 
                
                
                
                } else {
                    $this->Set_Alert_Response(242) ; // Error saving comment.
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;            
                    }
            }
        
        return $this ; 
        }    
    
    
    public function Action_Validate_Asset_Slug($asset_array) {
        
        $slug_edited = 'no' ; 
        $i_original = '' ; 
        $query_loops = 0 ; 
        $response['microtime']['slug_initial'] = microtime(true) ;
            
        
        // Create an asset slug
        $asset_array['asset_slug'] = $this->Action_Format_Slug($asset_array['asset_title'],$asset_array['asset_slug']) ; 
               
        // Get the type identifier for the end of the slug to prevent "good slugs" from being used by non-public assets
        if ($asset_array['type_id']) {
            $asset_type_record = $this->Retrieve_Asset_Type($asset_array['type_id'])['results'] ; 
            } 
        
        $response['asset_slug_original'] = $asset_array['asset_slug'] ; // Reserved for the output array
        $asset_slug_original = $response['asset_slug_original'] ; // Can be manipulated throughout script 
        
        $diff = 0 ; // Determines whether or not the slug is continuing to be edited by the cleanup script
        do {
            $response['microtime']['slug_cleanup_'.$query_loops] = microtime(true) ;
            
            // Inititalize the alternative slug title counter
            $slug_segments = Utilities::Process_Comma_Separated_String($asset_slug_original,'-') ; 
            $asset_slug_control = $asset_slug_original ; // Compares edits made in this do loop 

            // Strips the submitted slug of the numeric incrementor
            if (is_numeric(end($slug_segments)) == 'true') {

                if ('-'.prev($slug_segments) == $asset_type_record['type_abbreviation']) {
                    $asset_slug_original = rtrim($asset_slug_original,$asset_type_record['type_abbreviation'].'-'.end($slug_segments)) ; 
                    } else {
                        $asset_slug_original = rtrim($asset_slug_original,'-'.end($slug_segments)) ; 
                        }

                $i_original = '-'.end($slug_segments) ; 
                $i = time() + 1; 

                } else {  

                    // Strips the submitted slug of the type abbreviation
                    if ('-'.end($slug_segments) == $asset_type_record['type_abbreviation']) {
                        $asset_slug_original = rtrim($asset_slug_original,$asset_type_record['type_abbreviation']) ; 
                        }

                    $i = time() ;

                    }

                if ($asset_slug_control != $asset_slug_original) {
                    $diff = 1 ;
                    } else {
                        $diff = 0 ; 
                        }
            
            } while ($diff == 1) ; 
        
        
        
        $asset_array['asset_slug'] = $asset_slug_original.$asset_type_record['type_abbreviation'].$i_original ;
        
        $response['asset_slug_raw'] = $asset_slug_original ;
        $response['asset_slug_raw_constructed'] = $asset_array['asset_slug'] ;
        
        $c = 0 ; // Loop count
        do {
            
            $response['microtime']['slug_query_'.$c] = microtime(true) ;
            
            $asset_slug = $asset_array['asset_slug'] ;

            $query_array = array(
                'table' => 'assets',
                'values' => $asset_array,
                'where' => "assets.asset_slug='$asset_slug' AND assets.asset_id!='$this->asset_id'"
                );

            $result = $this->DB->Query('SELECT',$query_array) ;
            $this->asset_query_result = $result ;            
            $query_loops++ ; 
            
            // Check protected values
            $protected_values_query = array(
                'value_name' => $asset_slug,
                'value_type' => 'asset_slug'
                ) ;
            
            $system_lists = new System() ;
            $system_lists->Set_System_List('list_protected_values',$protected_values_query) ; 
            $data['list_protected_values'] = $system_lists->Get_System_List('list_protected_values') ;              
            
            
            if (($result['result_count'] > 0) OR (count($data['list_protected_values']) > 0)) {
                $asset_array['asset_slug'] = $asset_slug_original.$asset_type_record['type_abbreviation'].'-'.$i ; // Add an integer to the slug if there is a duplicate
                $i++ ;  
                $slug_edited = 'yes' ;
                } else {
                    $i = 0 ; 
                    }  
            $c++ ; 
             
            } while ($i > 0) ;        
        

        $response['microtime']['slug_final'] = microtime(true) ;
        $response['microtime']['cumulative'] = $response['microtime']['slug_final'] - $response['microtime']['slug_initial'] ; 
            
        $response['asset_slug_validated'] = $asset_array['asset_slug'] ; // A working slug, could be original or alternate
        $response['asset_slug_alternate'] = $asset_array['asset_slug'] ; // The final alternate slug if different
        $response['slug_edited'] = $slug_edited ; // yes/no Indicates whether the slug was altered by the script or not
        $response['query_loops'] = $query_loops ; 
        
        
        return $response ;          
        }
    
    

    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    //////////////////////  
    
    

    
    
    public function Create_Asset_Member($query_options) {
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        $query_array = array(
            'table' => "asset_members",
            'values' => array(
                'asset_id' => $query_options->asset_id,
                'member_id_value' => $query_options->member_id_value,
                'metadata_id' => $query_options->metadata_id
                ),
            'where' => "asset_members.asset_id='$query_options->asset_id' AND asset_members.member_id_value='$query_options->member_id_value'"
            );

        // Additional WHERE statement to ensure we're updating the correct member of an existing asset if required
        if (isset($query_options->member_id)) {
            $query_array['where'] .= " AND asset_members.member_id='$query_options->member_id' " ; 
            }
        
        
        if (isset($query_options->parent_member_id)) {
            $query_array['values']['parent_member_id'] = $query_options->parent_member_id ; 
            }        
        
        if (isset($query_options->campaign_id)) {
            $query_array['values']['campaign_id'] = $query_options->campaign_id ; 
            }
        if (isset($query_options->email_metadata_id)) {
            $query_array['values']['email_metadata_id'] = $query_options->email_metadata_id ; 
            }
        if (isset($query_options->sms_metadata_id)) {
            $query_array['values']['sms_metadata_id'] = $query_options->sms_metadata_id ; 
            }
        
        $query_array['values']['timestamp_member_updated'] = TIMESTAMP ;
        $query_array['values']['timestamp_member_created'] = array(
            'value' => TIMESTAMP,
            'statement' => "CASE WHEN timestamp_member_created = 0 THEN 'VALUE' ELSE timestamp_member_created END"
            ) ;
        
                        
        
        if ($query_options->force_create == 'yes') {
            $result = $this->DB->Query('INSERT',$query_array) ;
            } else {
                $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
                }
        
        $this->asset_query_result = $result ; 

        return $result ; 
        
        }
    
    
    public function Update_Asset_Member($query_options) {

        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $values_array['timestamp_member_updated'] = TIMESTAMP ; 
        
        $query_array = array(
            'table' => 'asset_members',
            'values' => $values_array
            );            
            
        if (isset($query_options->member_id)) {
            $query_array['where'] = "asset_members.member_id='$query_options->member_id'" ; 
            } else if (isset($query_options->parent_member_id) AND isset($query_options->asset_id)) {
                $query_array['where'] = "asset_members.parent_member_id='$query_options->parent_member_id' AND asset_members.asset_id='$query_options->asset_id'" ; 
                }
                
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->asset_query_result = $result ; 
        
        if (!isset($result['error'])) {
            
            // Create History Entry
//            $history_input = array(
//                'account_id' => $this->account_id,
//                'contact_id' => $query_options->member_id_value,
//                'asset_id' => $query_options->asset_id,
//                'function' => 'create_asset_member',
//                'notes' => json_encode($query_array['values'])
//                ) ; 
//            $user_history = $this->Create_User_History($this->user_id,$history_input) ;
            }
        
        return $result ;         
        }
    

    
    public function Create_Asset($asset_master_input,$asset_content_input = array(),$additional_asset_input = array()) {
                 

        $asset_type_record = $this->Retrieve_Asset_Type($asset_master_input['type_id'])['results'] ; 
        
        $asset_array = array() ;    
        $content_array = array() ; 
 
        
        foreach ($asset_master_input as $key => $value) {  
            switch ($key) {
                case 'asset_title':
                case 'asset_slug':                    
                case 'asset_description':
                case 'type_id':
                case 'ancestor_asset_id':
                case 'parent_asset_id':    
                case 'comments_allowed':
                case 'timestamp_asset_published':
                    $asset_array[$key] = $value ;
                    break ;
                }    
            }

        if ($this->account_id) {
            $asset_array['account_id'] = $this->account_id ;
            } else {
                $asset_array['account_id'] = 0 ;
                }
    
        // Inititalize the timestamps for the new asset (created and last changed)
        $asset_array['timestamp_asset_created'] = TIMESTAMP - 1 ; 
        $asset_array['timestamp_asset_updated'] = TIMESTAMP - 1 ;       
        
        
        // Set asset visibility.
        // Defaults to 3 (Draft) if not set
        if (isset($asset_master_input['visibility_id'])) {
            $asset_array['visibility_id'] = $asset_master_input['visibility_id'] ; 
            } else {
                $asset_array['visibility_id'] = 3 ; // Draft
                }
        
                
        $time_data = System::Action_Time_Territorialize_Dataset($asset_array) ; 
        
        

        
        
        // Create an asset slug
        if (!$asset_array['asset_slug']) {
            if (!$asset_array['asset_title']) {
                $asset_array['asset_slug'] = $time_data['timestamp_asset_created_compiled']['mon_d_yyyy'].' '.$time_data['timestamp_asset_created_compiled']['h_mm_a']  ;
                }            
            $asset_array['asset_slug'] = $this->Action_Format_Slug($asset_array['asset_title'],$asset_array['asset_slug']) ; 
            }
        
        
        // Validate the asset slug
        $asset_slug_result = $this->Action_Validate_Asset_Slug($asset_array) ; 
        $asset_array['asset_slug'] = $asset_slug_result['asset_slug_validated'] ; 
        
        
        // Create the master asset record
        $query_array = array(
            'table' => 'assets',
            'values' => $asset_array
            );

        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->asset_query_result = $result ;
        // End Asset Parent Processing
        
        
        
        // BEGIN PROCESSING ASSET CONTENT ARRAY
        if (isset($result['insert_id']) AND isset($asset_array['account_id'])) {
            
            $asset_id = $result['insert_id'] ; 
            $this->Set_Asset_ID($asset_id) ; 
            
            foreach ($asset_content_input as $key => $value) {  
                switch ($key) {
                    case 'text_01':
                    case 'text_02':
                    case 'asset_id_01':
                    case 'asset_id_02':
                    case 'asset_id_03':
                    case 'asset_id_04':
                    case 'metadata_01':
                    case 'metadata_02':
                    case 'metadata_03':
                    case 'metadata_04':
                    case 'structured_data_01':    
                        $content_array[$key] = $value ;
                        break ;
                    }    
                }            
            
            
            
//            // Parse and map structured data to the appropriate fields...
//            $asset_type_map_array = $this->Action_Decode_Map($asset_type_record['results']['map']) ;
//            $asset_content_map_array = $asset_type_map_array ;
//            unset($asset_content_map_array['structured_data_01'],$asset_content_map_array['structured_data_02']) ; 
//            
//            foreach ($asset_content_map_array as $content_key => $content_value) { 
//
//                if (isset($additional_asset_input[$content_value])) {
//                    $content_array[$content_key] = $additional_asset_input[$content_value] ; 
//                    }
//
//                }            
//            
//            
//            $structured_data_01 = array() ; 
//            if ($asset_type_map_array['structured_data_01']) {
//                foreach ($asset_type_map_array['structured_data_01'] as $structured_key => $structured_value) {
//                    
//                    if (isset($additional_asset_input[$structured_key])) {
//                        $structured_data_01[$structured_key] = $additional_asset_input[$structured_key] ; 
//                        }
//                    
//                    }
//                
//                $content_array['structured_data_01'] = json_encode($structured_data_01) ;
//                }
            
            $content_array['asset_id'] = $asset_id ;  
            
            $query_array = array(
                'table' => 'asset_content',
                'values' => $content_array
                );

            $result_content = $this->DB->Query('INSERT',$query_array) ;
            $this->asset_query_result = $result ;
            
            
            if (isset($asset_input['metadata'])) {
                foreach ($asset_input['metadata'] as $metadata_id) {
                    $metadata_query = array(
                        'asset_id' => $asset_id,
                        'metadata_id' => $metadata_id
                        ) ; 
                    $metadata_update = $this->Action_Create_Asset_Metadata_Relationship($metadata_query) ; 
                    }
                } 
            
            } else {
                $this->Set_Asset_ID('error') ; 
                }

        $result_array = array(
            'parent' => $result,
            'content' => $result_content
            ) ; 
        
        return $result_array ;         
        }
    

    
    public function Create_Structure_Item($asset_id,$child_asset_id,$additional_properties = array()) {

        $structure_array = array(
            'asset_id' => $asset_id,
            'child_asset_id' => $child_asset_id
            ) ; 
        
        if (isset($additional_properties['structure_order'])) {
            $structure_array['structure_order'] = $additional_properties['structure_order'] ;             
            } else {
                $structure_array['structure_order'] = 1.00 ; 
                }
        
        if (isset($additional_properties['delay'])) {
            $structure_array['delay'] = $additional_properties['delay'] ;             
            }        
        

        $i = 1 ; 
        do {
            
            $structure_order = $structure_array['structure_order'] ; 
            
            $query_array = array(
                'table' => 'asset_structure',
                'values' => $structure_array,
                'where' => "asset_structure.asset_id='$asset_id' AND asset_structure.structure_order='$structure_order'"
                );

            $result = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
            $this->asset_query_result = $result ;            
            
            if ($result['result_count'] > 0) {
                $structure_array['structure_order'] = $structure_array['structure_order'] + 1 ; // Increment the order by 1
                } else {
                    $i = 0 ; 
                    } 
            
            } while ($i > 0);         
        
        
        return $result ;   
        
        }
    
    
    
    // THIS IS ONLY USED WITHIN Action_Create_Asset()
    // Action_Update_Content( triggers Update_Asset_History( , which goes through a update/insert switch to determine if history needs to be created or updated
    // But assumes an ORIGINAL asset history has already been created
    public function Create_Asset_History($asset_id = 'internal') {
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            }
        
        $this->Set_Model_Timings('Asset.Create_Asset_History_initialize_'.$asset_id) ;
        
        $asset_record = $this->Get_Asset() ; 
        $content_history_array = array() ; 
        
        if ($asset_id == $asset_record['asset_id']) {
            
            $content_history_array['text_01'] = $asset_record['text_01'] ; 
            $content_history_array['text_02'] = $asset_record['text_02'] ; 
            $content_history_array['asset_id_01'] = $asset_record['asset_id_01'] ; 
            $content_history_array['asset_id_02'] = $asset_record['asset_id_02'] ; 
            $content_history_array['asset_id_03'] = $asset_record['asset_id_03'] ; 
            $content_history_array['asset_id_04'] = $asset_record['asset_id_04'] ; 
            $content_history_array['metadata_01'] = $asset_record['metadata_01'] ; 
            $content_history_array['metadata_02'] = $asset_record['metadata_02'] ; 
            $content_history_array['metadata_03'] = $asset_record['metadata_03'] ; 
            $content_history_array['metadata_04'] = $asset_record['metadata_04'] ; 
            $content_history_array['structured_data_01'] = $asset_record['structured_data_01'] ; 
            
            } else {
                $asset_content = $this->Set_Asset()->Retrieve_Asset_Content() ;   
                unset($asset_content['results']['content_id']) ; 
                }

        $content_history_array['asset_id'] = $asset_id ; 
        $content_history_array['microtimestamp_asset_history_updated'] = microtime(true) ;
        $content_history_array['asset_master'] = json_encode(array('asset_id' => $asset_id)) ; 
            
        foreach ($asset_content['results'] as $key => $value) {
            $content_history_array[$key] = $value ;
            }

        $this->Set_Model_Timings('Asset.Create_Asset_History_processed_history_inputs_'.$asset_id) ;
        
        $query_array = array(
            'table' => 'asset_content_history',
            'values' => $content_history_array
            );

        $result = $this->DB->Query('INSERT',$query_array) ; 
        $history_id = $result['insert_id'] ; 
        $this->Set_Model_Timings('Asset.Create_Asset_History_create_query_run_'.$asset_id) ;
        
        $asset_master_array = array(
            'current_history_id' => $history_id
            ) ; 
        
        // Updates the asset master
        $query_master_array = array(
            'table' => 'assets',
            'values' => $asset_master_array,
            'where' => "asset_id='$asset_id'"
            );

        $master_update = $this->DB->Query('UPDATE',$query_master_array);
        $this->Set_Model_Timings('Asset.Create_Asset_History_updated_master_asset_'.$asset_id) ;
        
        return $result ;         
        }
    
    
    public function Create_Global_Asset_Restriction($input) {
 
        $asset_id = $input['asset_id'] ;
        $restriction_type = $input['restriction_type'] ;
        $restriction_id = $input['restriction_id'] ;
        
        $restriction_input = array() ; 
        foreach ($input as $key => $value) {
            switch ($key) {
                case 'asset_id':
                case 'restriction_type':
                case 'restriction_id':                   
                    $restriction_input[$key] = $value ;
                    break ; 
                }            
            }        

        $query_array = array(
            'table' => 'asset_global_restrictions',
            'values' => $restriction_input,
            'where' => "asset_global_restrictions.asset_id='$asset_id' AND asset_global_restrictions.restriction_type='$restriction_type' AND asset_global_restrictions.restriction_id='$restriction_id'"
            );

        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        $this->asset_query_result = $result ; 
        
        return $result ;
        }
    
    
    public function Delete_Global_Asset_Restriction($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $query_array = array(
            'table' => "asset_global_restrictions"
            );

        $query_array['where'] = "asset_global_restrictions.asset_id='$query_options->asset_id' AND asset_global_restrictions.restriction_type='$query_options->restriction_type' AND asset_global_restrictions.restriction_id='$query_options->restriction_id'" ;

        $result = $this->DB->Query('DELETE',$query_array);
        $this->asset_query_result = $result ;
            
        return $result ; 
        }
    
    
    
    public function Create_Global_Asset_Relationship($input) {
 
            
        $relationship_input = array() ; 
        foreach ($input as $key => $value) {
            switch ($key) {
                case 'asset_id':
                case 'account_id':
                case 'account_asset_visibility_id':
                case 'timestamp_account_asset_published':                    
                    $relationship_input[$key] = $value ;
                    break ; 
                }            
            }        
        
        if (!isset($relationship_input['account_id'])) {
            $relationship_input['account_id'] = $this->account_id ; 
            }
        if (!isset($relationship_input['asset_id'])) {
            $relationship_input['asset_id'] = $this->asset_id ; 
            }
         
        
        $account_id = $relationship_input['profile_id'] ; 
        $asset_id = $relationship_input['asset_id'] ; 
        
        $query_array = array(
            'table' => 'asset_global_relationships',
            'values' => $relationship_input,
            'where' => "asset_global_relationships.asset_id='$asset_id' AND asset_global_relationships.account_id='$account_id'"
            );

        $query_array['values']['timestamp_account_asset_created'] = array(
            'value' => TIMESTAMP,
            'statement' => "CASE WHEN timestamp_account_asset_created = 0 THEN 'VALUE' ELSE timestamp_account_asset_created END"
            ) ;        
        $query_array['values']['timestamp_account_asset_updated'] = TIMESTAMP ;        
        
        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        $this->asset_query_result = $result ; 
        
        return $result ;
        }
    
    
    
    public function Create_Asset_Authorization($input) {

        if (!isset($input['asset_auth_id'])) {
            
            if (isset($input['asset_auth_role_name'])) {
                $asset_auth_query['filter_by_asset_auth_role_name'] = $input['asset_auth_role_name'] ;
                $asset_role_result = $this->Retrieve_Asset_Auth_Role_List($asset_auth_query) ;
                $input['asset_auth_id'] = $asset_role_result['results'][0]['asset_auth_id'] ;                
                }              
            }
        
            
        $authorization_input = array() ; 
        foreach ($input as $key => $value) {
            switch ($key) {
                case 'profile_id':
                case 'asset_id':
                case 'asset_auth_id':
                case 'author':
                case 'timestamp_auth_expiration':                    
                    $authorization_input[$key] = $value ;
                    break ; 
                }            
            }        
        
        if (!isset($authorization_input['profile_id'])) {
            $authorization_input['profile_id'] = $this->profile_id ; 
            }
        if (!isset($authorization_input['asset_id'])) {
            $authorization_input['asset_id'] = $this->asset_id ; 
            }
         
        
        $profile_id = $authorization_input['profile_id'] ; 
        $asset_id = $authorization_input['asset_id'] ; 
        
        $query_array = array(
            'table' => 'asset_auth',
            'values' => $authorization_input,
            'where' => "asset_auth.asset_id='$asset_id' AND asset_auth.profile_id='$profile_id'"
            );

        $query_array['values']['timestamp_auth_created'] = array(
            'value' => TIMESTAMP,
            'statement' => "CASE WHEN timestamp_auth_created = 0 THEN 'VALUE' ELSE timestamp_auth_created END"
            ) ;        
        $query_array['values']['timestamp_auth_updated'] = TIMESTAMP ;        
        
        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        $this->asset_query_result = $result ; 
        
        return $result ;
        }
    
    
    
    
    public function Delete_Asset_Authorization($auth_input = array()) {
        
        $continue = 1 ;
        
        if (!isset($this->asset_id) OR !isset($auth_input['profile_id'])) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            
            $profile_id = $auth_input['profile_id'] ; 
            
            $query_array = array(
                'table' => "asset_auth",
                'where' => "asset_auth.asset_id='$this->asset_id' AND asset_auth.profile_id='$profile_id'"
                );

            $result = $this->DB->Query('DELETE',$query_array);
            $this->asset_query_result = $result ;            
            } else {
                $result['error'] = 'Asset ID or Profile ID missing.' ; 
                }
            
        return $result ;       
        }
    

    
    public function Create_Asset_Metadata_Relationship($query_options = array()) {

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        
        $query_array = array(
            'table' => 'asset_metadata_relationships',
            'values' => array(
                'asset_id' => $query_options->asset_id,
                'metadata_id' => $query_options->metadata_id
                ),
            'where' => "asset_metadata_relationships.asset_id='$query_options->asset_id' AND 
                asset_metadata_relationships.metadata_id='$query_options->metadata_id'"
            );

        $result = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;        
        return $result ; 
        
        }
    
    
    
    
    public function Create_Campaign($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $query_array = array(
            'table' => "asset_campaigns",
            'values' => array(
                'user_id' => $query_options->user_id,
                'account_id' => $query_options->account_id,
                'asset_id' => $query_options->asset_id,
                'delivery_asset_id' => $query_options->delivery_asset_id,
                'delivery_trigger' => $query_options->delivery_trigger,
                'campaign_title' => $query_options->campaign_title,
                'structured_data_01' => json_encode($query_options->structured_data_01),
                'campaign_status_id' => $query_options->campaign_status_id,
                'campaign_current_position' => '1.00',
                'timestamp_campaign_created' => TIMESTAMP,
                'timestamp_campaign_updated' => TIMESTAMP,
                'timestamp_campaign_scheduled' => $query_options->timestamp_campaign_scheduled
                )
            );

        if ($query_options->ancestor_campaign_id) {
            $query_array['values']['ancestor_campaign_id'] = $query_options->ancestor_campaign_id ; 
            }

        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->asset_query_result = $result ; 
 
        return $result ; 
        
        }
    
    
    public function Create_Automation($query_options = array()) {

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write         
        
        $query_array = array(
            'table' => "asset_campaign_automation",
            'values' => array(
                'campaign_id' => $query_options->campaign_id,
                'campaign_position' => $query_options->campaign_position,
                'automation_status_id' => $query_options->automation_status_id,
                'timestamp_next_action' => $query_options->timestamp_next_action
                )
            );
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->asset_query_result = $result ; 
        
        return $result ; 
        
        }
    
    
    public function Create_Comment($input) {
        
        $comment_input = array() ; 
        foreach ($input as $key => $value) {
            switch ($key) {
                case 'commenter_id':
                case 'commenter_type':
                case 'comment_content':
                case 'first_name':
                case 'last_name':    
                    $comment_input[$key] = $value ; 
                    break ; 
                }            
            }
        
        $comment_input['asset_id'] = $this->asset_id ; 
        $comment_input['account_id'] = $this->account_id ; 
        $comment_input['visibility_id'] = 4 ; // Shared to internal
        $comment_input['timestamp_comment_updated'] = TIMESTAMP ;
        $comment_input['timestamp_comment_created'] = TIMESTAMP ;        
        
        $query_array = array(
            'table' => "asset_comments",
            'values' => $comment_input
            );

        $create_comment = $this->DB->Query('INSERT',$query_array) ;        
        return $create_comment ; 
        
        }

    

    
    
    
    // Retrieve the ordered list of structured assets under a multi-component parent asset (e.g. sequence or newsletter)        
    public function Retrieve_Asset_Structure($options = array()) { 
        
        if (!isset($options['structure_table'])) {
            $structure_table_name = 'asset_structure' ;
            } else {
                $structure_table_name = $options['structure_table'] ;
                }
        
        $query_array = array(
            'table' => $structure_table_name,
            'fields' => "$structure_table_name.*, 
                assets.asset_title, assets.asset_description, 
                asset_type.*,
                system_visibility_levels.*",
            'where' => "$structure_table_name.asset_id='$this->asset_id'",
            'order_by' => "$structure_table_name.structure_order",
            'order' => "ASC"
            );                

        $query_array['join_tables'][] = array(
            'table' => 'assets',
            'on' => 'assets.asset_id',
            'match' => "$structure_table_name.child_asset_id"
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_type',
            'on' => 'asset_type.type_id',
            'match' => 'assets.type_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'assets.visibility_id'
            );
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');        

        return $result ; 
        }

    
    
    
    
    public function Update_Asset_Structure($asset_structure_input) { 
        

        $result = array() ; 
        
        $current_structure_result = $this->Retrieve_Asset_Structure() ; 
        $current_structure = $current_structure_result['results'] ; 


        if (count($asset_structure_input) > 0) {
            $continue = 1 ; 
            } else {
                $continue = 0 ; 
                }
        
        if ($continue == 1) {
            
            
            $i = 0 ; 
            foreach ($asset_structure_input as $structure_input) {

                $values_array = array() ;

                $values_array['child_asset_id'] = $structure_input['child_asset_id'] ; 
                $values_array['structure_order'] = $structure_input['structure_order'] ; 
                $values_array['delay'] = $structure_input['delay'] ;

                if(isset($current_structure[$i])) {

                    $current_structure_id = $current_structure[$i]['structure_id'] ; // Structure ID from existing structure table

                    // Updates the asset content in the database
                    $query_array = array(
                        'table' => 'asset_structure',
                        'values' => $values_array,
                        'where' => "asset_structure.structure_id='$current_structure_id'"
                        ) ; 

                    $result[] = $this->DB->Query('UPDATE',$query_array);

                    } else {

                        $values_array['asset_id'] = $this->asset_id ; 

                        $query_array = array(
                            'table' => "asset_structure",
                            'values' => $values_array               
                            );

                        $result[] = $this->DB->Query('INSERT',$query_array) ;                                
                        } 

                $i++ ; 
                }

            
            // There are extra core structure items that need to be removed
            if (isset($current_structure[$i]['structure_id'])) {

                $continue = 1 ; 

                do {

                    $structure_id = $current_structure[$i]['structure_id'] ; 

                    // Delete an extra structure entry
                    $query_array = array(
                        'table' => "asset_structure",
                        'where' => "asset_structure.structure_id='$structure_id'",
                        );

                    $delete_result = $this->DB->Query('DELETE',$query_array);

                    $i++ ; 
                    if (!isset($current_structure[$i]['structure_id'])) {
                        $continue = 0 ; 
                        }
                    } while ($continue == 1) ; 


                }
            } else {
            
                // Delete an extra structure entry
                $query_array = array(
                    'table' => "asset_structure",
                    'where' => "asset_structure.asset_id='$this->asset_id'"
                    );

                $delete_result = $this->DB->Query('DELETE',$query_array);            
                }
        
        
        return $result ; 
        }

    
    
    
    
    
    
  
    

    
    public function Action_Update_Asset_Order($ordered_child_asset_array) {  
        
        $this->Set_Model_Timings('begin_asset_order') ;
        
        $new_structure = array() ;         
        
        $i = 1.00 ; 
        $ordered_child_clean = array() ;
        foreach ($ordered_child_asset_array as $asset) {

            if (isset($asset['sortid'])) {
                
                $this_child = array(
                    'asset_id' => $asset['sortid'], 
                    'structure_order' => $i
                    );                 
                if (isset($asset['delay'])) {
                    $this_child['delay'] = $asset['delay'] ; 
                    }
                
                $ordered_child_clean[] = $this_child ; 
                
                } else {
                
                    $asset_id = str_replace("sortid_", "", $asset);
            
                    $ordered_child_clean[] = array(
                        'asset_id' => $asset_id, 
                        'structure_order' => $i
                        );                
                
                    }
            

            if ($asset['children']) {
                $j = $i + .01 ; 
                foreach ($asset['children'] as $child) {

                    $ordered_child_clean[] = array(
                        'asset_id' => $child['asset_id'], 
                        'structure_order' => $j
                        );

                    $j = $j + .01 ; 
                    }

                }
            $i++ ;
            }        
        
        $this->Set_Model_Timings('reordered_incoming') ;
        
        // Get latest asset history
        $additional_parameters['history_id'] = 'latest' ; 
        $latest_asset_history = $this->Set_Asset_History('internal',$additional_parameters)->Get_Asset_History() ;
        $latest_order = $latest_asset_history['asset_structure'] ; 
        
//        print_r('<pre>') ;
//        print_r($latest_order) ;
        
        
        $this->Set_Model_Timings('pulled_latest_history') ;
        
        // Create the new total structure array
        $i = 0 ; 
        foreach ($ordered_child_clean as $child) {

            $values_array['asset_id'] = $this->asset_id ;
            $values_array['child_asset_id'] = $child['asset_id'] ; 
            $values_array['structure_order'] = number_format($child['structure_order'],2) ; 
            if (isset($child['delay'])) {
                $values_array['delay'] = $child['delay'] ;
                
                } else if (isset($latest_order[$i]['delay'])) {
                $values_array['delay'] = $latest_order[$i]['delay'] ; 
                
                } else {
                    $values_array['delay'] = 0 ; 
                    }
            
            $new_structure[] = $values_array ; 
            $i++  ;
            }       
        
        
        $content_input['asset_id'] = $this->asset_id ;
        $content_input['asset_structure'] = $new_structure ;
        
//        print_r($content_input) ; 
//        print_r('</pre>') ;
        
        $reorder = $this->Action_Update_Content($content_input) ; 
        
        
        $this->Set_Model_Timings('pushed_update') ;
        
        
        $result = array(
            'reorder_query' => $reorder_query,
            'ordered_assets' => $ordered_child_clean,
            'latest_order' => $latest_order,
            'new_structure' => $new_structure
            ) ; 
        
        $this->test_data = $result ; 
        
        return $this ;         
        }
    
    

    
    
    
    public function Retrieve_Comment_List($query_options) {  
        
        global $server_config ; 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        if ($this->user['timezone']) {
            $timezone = $this->user['timezone'] ; 
            } else {
                $timezone = $this->system_timezone ; 
                }  
        
        $query_array = array(
            'table' => 'asset_comments',
            'fields' => "asset_comments.comment_id, asset_comments.asset_id, asset_comments.account_id, 
                    asset_comments.commenter_id, asset_comments.commenter_type, asset_comments.visibility_id, 
                    asset_comments.comment_content, 
                    asset_comments.timestamp_comment_created, asset_comments.timestamp_comment_updated,
                asset_type.*, 
                assets.asset_title, 
                asset_commenters.*,
                metadata.*,
                nonasset_metadata_relationships.relationship_id",                
            'where' => "asset_comments.comment_id>0 "
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'assets',
            'on' => 'assets.asset_id',
            'match' => 'asset_comments.asset_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_type',
            'on' => 'asset_type.type_id',
            'match' => 'assets.type_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'nonasset_metadata_relationships',
            'statement' => 'nonasset_metadata_relationships.item_id=asset_comments.comment_id AND nonasset_metadata_relationships.relationship_type="comment"'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata',
            'on' => 'metadata.metadata_id',
            'match' => 'nonasset_metadata_relationships.metadata_id'
            );
        
        
        
        // Add comment_id filter
        if (isset($query_options->comment_id)) {
            $query_array['where'] .= "AND asset_comments.comment_id='$query_options->comment_id' " ;  
            }
        
        // Add asset_id filter
        if (isset($query_options->asset_id)) {
            $query_array['where'] .= "AND asset_comments.asset_id='$query_options->asset_id' " ;  
            }        
        
        // Add account_id filter
        if (isset($query_options->filter_by_account_id)) {
            switch ($query_options->filter_by_account_id) {
                case 'yes':
                    $query_array['where'] .= "AND asset_comments.account_id='$this->account_id' " ;                    
                    break ;                    
                }            
            }
        
        
        
            // COMMENTER SUBQUERIES        
            // SUB QUERY 1: Commenters who are contacts
            $subquery_commenter_contacts_input = array(
                'skip_query' => 1,
                'table' => 'asset_comments',
                'join_tables' => array(),
                'fields' => "
                    asset_comments.comment_id AS commenter_comment_id, 
                    contacts.first_name, contacts.last_name, 
                    CONCAT(contacts.first_name, ' ', contacts.last_name) AS full_name,
                    contacts.email_address
                    ",
                'where' => "
                    (asset_comments.commenter_type='contact') 
                    "
                );
            $subquery_commenter_contacts_input['join_tables'][] = array(
                'table' => 'contacts',
                'on' => 'contacts.contact_id',
                'match' => 'asset_comments.commenter_id'
                );         
            $subquery_commenter_contacts_result = $this->DB->Query('SELECT_JOIN',$subquery_commenter_contacts_input);        
            $subquery_commenter_contacts = $subquery_commenter_contacts_result['query'] ;      


        
            // SUB QUERY 2: Commenters who are user profiles
            $subquery_commenter_profiles_input = array(
                'skip_query' => 1,
                'table' => 'asset_comments',
                'join_tables' => array(),
                'fields' => "
                    asset_comments.comment_id AS commenter_comment_id, 
                    user_profiles.first_name, user_profiles.last_name, 
                    CONCAT(user_profiles.first_name, ' ', user_profiles.last_name) AS full_name,
                    '' AS email_address
                    ",
                'where' => "
                    (asset_comments.commenter_type='profile') 
                    "
                );
            $subquery_commenter_profiles_input['join_tables'][] = array(
                'table' => 'user_profiles',
                'on' => 'user_profiles.profile_id',
                'match' => 'asset_comments.commenter_id'
                );         
            $subquery_commenter_profiles_result = $this->DB->Query('SELECT_JOIN',$subquery_commenter_profiles_input);        
            $subquery_commenter_profiles = $subquery_commenter_profiles_result['query'] ;      
       
    
        
            // SUB QUERY 3: Commenters who are not associated with contact or profile
            $subquery_commenter_anonymous_input = array(
                'skip_query' => 1,
                'table' => 'asset_comments',
                'fields' => "
                    asset_comments.comment_id AS commenter_comment_id, 
                    asset_comments.first_name, asset_comments.last_name, 
                    CONCAT(asset_comments.first_name, ' ', asset_comments.last_name) AS full_name,
                    '' AS email_address
                    ",
                'where' => "
                    (asset_comments.commenter_type='anonymous') 
                    "
                );        
            $subquery_commenter_anonymous_result = $this->DB->Query('SELECT',$subquery_commenter_anonymous_input);        
            $subquery_commenter_anonymous = $subquery_commenter_anonymous_result['query'] ;  
        
        
        // WRITE THE MASTER SUB QUERY JOIN...
        $master_join_statement = "(" ;        
        $master_join_statement .= $subquery_commenter_contacts ;
        $master_join_statement .= ' UNION DISTINCT ' ; 
        $master_join_statement .= $subquery_commenter_profiles ;
        $master_join_statement .= ' UNION DISTINCT ' ; 
        $master_join_statement .= $subquery_commenter_anonymous ;        
        $master_join_statement .= ") AS asset_commenters " ;       
        
        $this->test_data = $master_join_statement ; 
        
        
        
        $query_array['join_tables'][] = array(
            'table' => $master_join_statement,
            'on' => 'asset_commenters.commenter_comment_id',
            'match' => 'asset_comments.comment_id',
            'type' => 'left'
            );        
        
        // END COMMENTER SUB  QUERY
        
        
        
        // Add visibility filter
        if (isset($query_options->filter_by_visibility_id)) {

            // Convert visiblity input to array
            $visibility_id_array = Utilities::Array_Force($query_options->filter_by_visibility_id) ; 
            
            if (count($visibility_id_array) > 0) {
                $filter_by_visibility_id_string = '' ; 
                foreach ($visibility_id_array as $visibility_id) {
                    
                    switch ($visibility_id) {
                        case 'logged_in':
                            $filter_by_visibility_id_string .= "asset_comments.visibility_id>='2' OR " ;        
                            break ; 
                        default:
                            $filter_by_visibility_id_string .= "asset_comments.visibility_id='$visibility_id' OR " ;                     
                        }
                    
                    }
                $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_visibility_id_string) " ;                
                }                 
            }        
        

        // Add filter by search parameter
        if (isset($query_options->filter_by_comment_status)) {

            $comment_status_id_array = Utilities::Array_Force($query_options->filter_by_comment_status) ; 
            
            $compiled_status_string = '' ; 
            
            foreach ($comment_status_id_array as $status_id) {
                $compiled_status_string .= "metadata.metadata_id='$status_id' OR " ; 
                }
            
            $compiled_status_string = rtrim($compiled_status_string," OR ") ;     
            $query_array['where'] .= " AND ($compiled_status_string) " ;                                    
            }
        
        
        
        // PROCESS COMMENT CREATED FILTER
        if ($query_options->comment_created_filter_status == 'active') {
            
            $today_midnight = new DateTime() ; 
            $today_midnight->setTimezone(new DateTimeZone($timezone)) ;  
            $today_midnight_timestamp = $today_midnight->setTimestamp(TIMESTAMP)->setTime( 23, 59, 59 )->getTimestamp() ;

            
            switch ($query_options->comment_created_filter_type) {
                case 'range_next':
                    
                    $range_next = Utilities::System_Time_Manipulate($query_options->comment_created_filter_range_next_days.' d','+',$today_midnight_timestamp) ; 
                    $range_next_timestamp = $range_next['time_manipulated'] ;
                    $query_array['where'] .= "AND (asset_comments.timestamp_comment_created>$today_midnight_timestamp AND asset_comments.timestamp_comment_created<$range_next_timestamp) " ; 
            
                    break ; 
                case 'range_last':
                    
                    $range_last = Utilities::System_Time_Manipulate($query_options->comment_created_filter_range_last_days.' d','-',$today_midnight_timestamp) ; 
                    $range_last_timestamp = $range_last['time_manipulated'] ;
                    $query_array['where'] .= "AND (asset_comments.timestamp_comment_created>$range_last_timestamp AND asset_comments.timestamp_comment_created<$today_midnight_timestamp) " ; 
            
                    break ;                    
                case 'range_between':
                    
                    $range_between_begin = new DateTime($query_options->comment_created_filter_range_between_begin) ; 
                    $range_between_begin->setTimezone(new DateTimeZone($timezone)) ;  
                    $range_between_begin_timestamp = $range_between_begin->getTimestamp() ;
                    
                    $range_between_end = new DateTime($query_options->comment_created_filter_range_between_end) ; 
                    $range_between_end->setTimezone(new DateTimeZone($timezone)) ;  
                    $range_between_end_timestamp = $range_between_end->getTimestamp() ;

                    $query_array['where'] .= "AND (asset_comments.timestamp_comment_created>$range_between_begin_timestamp AND asset_comments.timestamp_comment_created<$range_between_end_timestamp)" ; 
                    break ;                                        
                }             
            } 
        

        
        // Add sort filter 
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ;           
            }
        
        
        
        // Add filter by search parameter
        if (isset($query_options->filter_by_search_parameter)) {

            $search_parameter_token = explode(" ",$query_options->filter_by_search_parameter) ; 
            
            $compiled_search_string = '' ; 
            
            foreach ($search_parameter_token as $search_token) {
                $this_search_string = " (asset_comments.comment_content LIKE '%$search_token%' OR 
                    asset_commenters.first_name LIKE '%$search_token%' OR 
                    asset_commenters.last_name LIKE '%$search_token%' OR
                    asset_commenters.full_name LIKE '%$search_token%'

                    ) " ; 
                
                $compiled_search_string .= $this_search_string.' OR ' ; 
                }
            
            $compiled_search_string = rtrim($compiled_search_string," OR ") ;     
            $query_array['where'] .= " AND ($compiled_search_string) " ;                                    
            }
        
        // Add sort filter
        if (isset($query_options->order_by)) { 
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ;           
            }
        

        // NEW PAGING PROCESS 
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force'); 

        if (($query_array['order_by_relevance'] == 'yes') AND (isset($query_options->filter_by_search_parameter))) {
            
            $relevance_options = array(
                'fields' => array('comment_content') 
                ) ; 
            $result = $this->Sort_Search_Relevance($result,$query_array,$query_options,$relevance_options) ; 
            } 
                
        $query_options->value_name = 'comment_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Asset_Paging($result) ;         
        $this->asset_query_result = $result ;                
        
        return $result ;       
        }    
    
    
    public function Retrieve_Asset_Member_Metadata($query_options) { 

        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'metadata',
            'fields' => "metadata.*",
            'where' => "metadata.metadata_type_id='4'"
            ) ; 
        
        
        switch ($query_options->subscription_type) {
            case 'parent':
                
                $subscription_type_query = " 
                    AND (
                    metadata.metadata_slug='list-member' 
                    )" ; 
                
                break ; 
            case 'sublist':
            default:    
                
                $subscription_type_query = " 
                    AND (
                    metadata.metadata_slug='list-subscribed' OR 
                    metadata.metadata_slug='list-unsubscribed' OR 
                    metadata.metadata_slug='list-pending' OR 
                    metadata.metadata_slug='list-cleaned' OR 
                    metadata.metadata_slug='list-inactive' OR 
                    metadata.metadata_slug='list-restricted' 
                    )" ; 
                
                break ; 
            }
        

        $query_array['where'] .= $subscription_type_query ; 
        
        $result = $this->DB->Query('SELECT',$query_array,'force');
        $this->asset_query_result = $result ;

        return $result ;       
        }
    
    
    
    public function Retrieve_Asset_Type($type_id,$set_by = 'id') { 

        if ('name' === $set_by) {
            $query_array = array(
                'table' => 'asset_type',
                'fields' => "asset_type.*",
                'where' => "asset_type.type_name='$type_id'"
                );        
            } else {
                $query_array = array(
                    'table' => 'asset_type',
                    'fields' => "asset_type.*",
                    'where' => "asset_type.type_id='$type_id'"
                    );             
                }
        
        
        $result = $this->DB->Query('SELECT',$query_array);
        $this->asset_query_result = $result ;

        return $result ;       
        }
   
    
    public function Retrieve_Asset_Auth_Role_List($query_options) { 

        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_asset_auth_roles',
            'fields' => "system_asset_auth_roles.*",
            'where' => "system_asset_auth_roles.asset_auth_id>'0'"
            );        
        

        if (isset($query_options->filter_by_asset_auth_id)) {
            
            $asset_auth_id_array = Utilities::Array_Force($query_options->filter_by_asset_auth_id) ; 
            
            if (count($asset_auth_id_array) > 0) {   
                $asset_auth_role_id_query_string = '' ; 
                foreach ($asset_auth_id_array as $asset_auth_id) {
                    $asset_auth_role_id_query_string .= "system_asset_auth_roles.asset_auth_id='$asset_auth_id' OR " ; 
                    }
                $asset_auth_role_id_query_string = rtrim($asset_auth_role_id_query_string," OR ") ;
                $query_array['where'] .= " AND ($asset_auth_role_id_query_string)" ;
                }
            }
        
        if (isset($query_options->filter_by_asset_auth_role_name)) {
            
            $asset_auth_name_array = Utilities::Array_Force($query_options->filter_by_asset_auth_role_name) ; 
            
            if (count($asset_auth_name_array) > 0) {   
                $asset_auth_role_name_query_string = '' ; 
                foreach ($asset_auth_name_array as $asset_auth_name) {
                    $asset_auth_role_name_query_string .= "system_asset_auth_roles.asset_auth_role_name='$asset_auth_name' OR " ; 
                    }
                $asset_auth_role_name_query_string = rtrim($asset_auth_role_name_query_string," OR ") ;
                $query_array['where'] .= " AND ($asset_auth_role_name_query_string)" ;
                }
            }            
            

        $result = $this->DB->Query('SELECT',$query_array,'force');
        $this->asset_query_result = $result ;

        return $result ;       
        }
    
    
    
    public function Retrieve_Asset_ID_By_Slug($asset_slug) {
        
        $query_array = array(
            'table' => 'assets',
            'fields' => "assets.asset_id",
            'where' => "assets.asset_slug='$asset_slug' AND assets.asset_slug!=''"
            );        
        
//        if (isset($this->account_id)) {
//            $query_array['where'] .= " AND (assets.account_id='0' OR assets.account_id='$this->account_id')" ; 
//            } else {
//                $query_array['where'] .= " AND assets.account_id='0'" ; 
//                }
        
        $result = $this->DB->Query('SELECT',$query_array);
        
        $this->asset_query_result = $result ;
        return $result ;       
        }    
    
    public function Retrieve_Asset_ID_By_Public_Link_ID($asset_link_id) {
        
        $query_array = array(
            'table' => 'assets',
            'fields' => "assets.asset_id",
            'where' => "assets.public_link_id='$asset_link_id' AND assets.public_link_id!=''"
            );        
        
        $result = $this->DB->Query('SELECT',$query_array);
        
        $this->asset_query_result = $result ;
        return $result ;       
        }
    
    
    
    
    public function Retrieve_Asset($asset_id = 'internal',$query_array = array()) {  
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            } 

        
        $query_array = array(
            'table' => 'asset_content',
            'join_tables' => array(),
            'fields' => "asset_type.*, asset_content.*, 
                assets.account_id, assets.organization_id, assets.global_asset,  
                assets.asset_title, assets.asset_description, assets.asset_slug, assets.visibility_id, 
                assets.current_history_id, assets.comments_allowed, 
                assets.parent_asset_id, 
                assets.timestamp_asset_created, assets.timestamp_asset_updated, assets.timestamp_asset_comments_closed, assets.timestamp_asset_published, 
                assets.public_link_asset_auth_id, assets.public_link_id, 
                system_visibility_levels.*",
            'where' => "asset_content.asset_id='$asset_id'", 
            'group_by' => "asset_content.asset_id"
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'assets',
            'on' => 'assets.asset_id',
            'match' => 'asset_content.asset_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_type',
            'on' => 'asset_type.type_id',
            'match' => 'assets.type_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'assets.visibility_id'
            );
        
        
        // Add Asset Owner. This is not a filter.
        $asset_owner_table = 'asset_owner' ; 
        
        $query_array['fields'] .= ", $asset_owner_table.asset_auth_role_name AS asset_owner_auth_role_name, 
            $asset_owner_table.asset_auth_role_title AS asset_owner_auth_role_title,
            $asset_owner_table.profile_display_name AS asset_owner_profile_display_name
            " ; 

        $query_array['join_tables'][] = array(
            'table' => "(SELECT 
                assets.asset_id, system_asset_auth_roles.*, user_profiles.* 
                FROM assets 
                JOIN asset_auth ON asset_auth.asset_id = assets.asset_id 
                JOIN system_asset_auth_roles ON system_asset_auth_roles.asset_auth_id = asset_auth.asset_auth_id 
                JOIN user_profiles ON user_profiles.profile_id = asset_auth.profile_id 
                WHERE system_asset_auth_roles.asset_auth_role_name='asset_owner'
                ) AS $asset_owner_table",
            'statement' => "(assets.asset_id=$asset_owner_table.asset_id)",
            'type' => 'left'
            );        
        
        
        // Add Asset Author. This is not a filter.
        $asset_author_table = 'asset_author' ; 
        
        $query_array['fields'] .= ", $asset_author_table.asset_auth_role_name AS asset_author_auth_role_name, 
            $asset_author_table.asset_auth_role_title AS asset_author_auth_role_title,
            $asset_author_table.profile_display_name AS asset_author_profile_display_name
            " ; 

        $query_array['join_tables'][] = array(
            'table' => "(SELECT 
                assets.asset_id, system_asset_auth_roles.*, user_profiles.* 
                FROM assets 
                JOIN asset_auth ON asset_auth.asset_id = assets.asset_id 
                JOIN system_asset_auth_roles ON system_asset_auth_roles.asset_auth_id = asset_auth.asset_auth_id 
                JOIN user_profiles ON user_profiles.profile_id = asset_auth.profile_id 
                WHERE asset_auth.author='1'
                ) AS $asset_author_table",
            'statement' => "(assets.asset_id=$asset_author_table.asset_id)",
            'type' => 'left'
            );        
        
        
        // Add Profile Auth. This is not a filter.
        if (isset($this->profile_id)) {    
            $profile_auth_table = 'active_profile' ; 

            $query_array['fields'] .= ", $profile_auth_table.asset_auth_role_name AS active_profile_auth_role_name, 
                $profile_auth_table.asset_auth_role_title AS active_profile_auth_role_title,
                $profile_auth_table.profile_display_name AS active_profile_profile_display_name
                " ; 

            $query_array['join_tables'][] = array(
                'table' => "(SELECT 
                    assets.asset_id, system_asset_auth_roles.*, user_profiles.* 
                    FROM assets 
                    JOIN asset_auth ON asset_auth.asset_id = assets.asset_id 
                    JOIN system_asset_auth_roles ON system_asset_auth_roles.asset_auth_id = asset_auth.asset_auth_id 
                    JOIN user_profiles ON user_profiles.profile_id = asset_auth.profile_id 
                    WHERE user_profiles.profile_id='$this->profile_id'
                    ) AS $profile_auth_table",
                'statement' => "(assets.asset_id=$profile_auth_table.asset_id)",
                'type' => 'left'
                );
            }
        
        
        
        if (isset($this->account_id)) {
            $query_array['fields'] .= ", asset_global_relationships.global_publish_id, 
                    asset_global_relationships.account_asset_visibility_id, 
                    asset_global_relationships.timestamp_account_asset_created, 
                    asset_global_relationships.timestamp_account_asset_published,
                    account_asset_visibility_levels.visibility_name AS account_asset_visibility_name,
                    account_asset_visibility_levels.visibility_title AS account_asset_visibility_title,                    
                    account_asset_visibility_levels.visibility_description AS account_asset_visibility_description" ; 
            
            $query_array['join_tables'][] = array(
                'table' => 'asset_global_relationships',
                'statement' => "(asset_global_relationships.asset_id=assets.asset_id AND asset_global_relationships.account_id=$this->account_id)",
                'type' => 'left'
                ); 
            
            $query_array['join_tables'][] = array(
                'table' => 'system_visibility_levels AS account_asset_visibility_levels',
                'on' => "asset_global_relationships.account_asset_visibility_id",
                'match' => "account_asset_visibility_levels.visibility_id",
                'type' => 'left'
                );             
            }
        
        
        $link_status_table = 'public_link' ; 
        
        $query_array['fields'] .= ", $link_status_table.asset_auth_role_name AS asset_auth_role_name_$link_status_table, 
            $link_status_table.asset_auth_role_title AS asset_auth_role_title_$link_status_table
            " ; 


        $query_array['join_tables'][] = array(
            'table' => "(SELECT assets.public_link_asset_auth_id, system_asset_auth_roles.* 
                FROM assets 
                JOIN system_asset_auth_roles ON system_asset_auth_roles.asset_auth_id = assets.public_link_asset_auth_id 
                WHERE assets.asset_id=$asset_id) AS $link_status_table",
            'statement' => "(assets.public_link_asset_auth_id=$link_status_table.asset_auth_id)",
            'type' => 'left'
            );         
        
        
        $asset_content_history_table = 'asset_content_history' ; 
        
        $query_array['fields'] .= " 
            , $asset_content_history_table.microtimestamp_asset_history_updated AS timestamp_latest_history_updated, 
            $asset_content_history_table.history_id AS latest_history_id" ; 


        $query_array['join_tables'][] = array(
            'table' => "(SELECT asset_content_history.* 
                FROM asset_content_history 
                JOIN assets ON assets.asset_id = asset_content_history.asset_id 
                WHERE asset_content_history.asset_id=$asset_id ORDER BY asset_content_history.microtimestamp_asset_history_updated DESC LIMIT 1) AS $asset_content_history_table",
            'statement' => "(assets.asset_id=$asset_content_history_table.asset_id)",
            'type' => 'left'
            );
        
        
        
        // Get a count of structure items within the asset_structure table
        $asset_structure_table = 'asset_structure_table' ;
        
        $query_array['fields'] .= " 
            , $asset_structure_table.asset_structure_count" ; 
        
        $query_array['join_tables'][] = array(
            'table' => "(SELECT COUNT(asset_structure.structure_id) AS asset_structure_count, asset_structure.asset_id  
                FROM asset_structure 
                WHERE asset_structure.asset_id=$asset_id) AS $asset_structure_table",
            'statement' => "(asset_content.asset_id=$asset_structure_table.asset_id)",
            'type' => 'left'
            );
               

        // Add parent asset data 
        $parent_asset_table = 'parent_asset_table' ; 

        $query_array['fields'] .= ", $parent_asset_table.asset_title AS parent_asset_title, 
            $parent_asset_table.structured_data_01 AS parent_structured_data_01, 
            $parent_asset_table.type_name AS parent_asset_type_name, 
            $parent_asset_table.type_display_name AS parent_asset_type_display_name 
            " ; 

        $query_array['join_tables'][] = array(
            'table' => "(SELECT 
                assets.*, 
                asset_content.structured_data_01, 
                asset_type.type_name, asset_type.type_display_name 
                FROM assets 
                JOIN asset_type ON asset_type.type_id = assets.type_id 
                JOIN asset_content ON asset_content.asset_id = assets.asset_id
                ) AS $parent_asset_table",
            'statement' => "(assets.parent_asset_id=$parent_asset_table.asset_id)",
            'type' => 'left'
            );
        
        
        
        // Add in asset member (subscriber count) tables...
        // *** CURRENTLY ONLY COUNTING CONTACT DISTRO LISTS ****
        $asset_member_table = 'asset_members' ; 
        
//        $query_array['fields'] .= ", 
//            COUNT(asset_members.member_id_value) as asset_member_count, 
//            
//            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-subscribed' then 1 else 0 end) distribution_sublist_email_subscribed, 
//            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-unsubscribed' then 1 else 0 end) distribution_sublist_email_unsubscribed, 
//            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-pending' then 1 else 0 end) distribution_sublist_email_pending, 
//            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_email_cleaned, 
//            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_email_restricted,
//            SUM(case when $asset_member_table.distribution_sublist_email_metadata_slug = 'list-inactive' then 1 else 0 end) distribution_sublist_email_inactive, 
//            
//            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-subscribed' then 1 else 0 end) distribution_sublist_sms_subscribed, 
//            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-unsubscribed' then 1 else 0 end) distribution_sublist_sms_unsubscribed, 
//            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-pending' then 1 else 0 end) distribution_sublist_sms_pending, 
//            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_sms_cleaned, 
//            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_sms_restricted,
//            SUM(case when $asset_member_table.distribution_sublist_sms_metadata_slug = 'list-inactive' then 1 else 0 end) distribution_sublist_sms_inactive 
//            " ; 

        
            // Prep the member visibility string... 
//            $member_visibility_string = '' ; 
//            $member_visibility_query_options['filter_by_visibility_name'] = 'visible' ;
//            $member_visibility_query_options = (object) $member_visibility_query_options ; 
//            $member_visibility_query_result = $this->Retrieve_Visibility_ID_Array($member_visibility_query_options) ;
//            foreach ($member_visibility_query_result as $member_visibility) {
//                $this_visibility_id = $member_visibility['visibility_id'] ; 
//                $member_visibility_string .= "system_visibility_levels.visibility_id='$this_visibility_id' OR " ; 
//                }
//            $member_visibility_string = rtrim($member_visibility_string," OR ") ; 
        
        
        // Contact Count Query
//        $query_array['join_tables'][] = array(
//            'table' => "(SELECT 
//                
//                asset_members.asset_id, asset_members.member_id_value, metadata.metadata_name,  
//                distribution_sublist_email_metadata_name, distribution_sublist_email_metadata_slug, distribution_sublist_email_asset_title, distribution_sublist_email_type_name, 
//                distribution_sublist_sms_metadata_name, distribution_sublist_sms_metadata_slug, distribution_sublist_sms_asset_title, distribution_sublist_sms_type_name, 
//                contact_match.contact_id 
//                
//                FROM asset_members 
//                JOIN assets ON asset_members.asset_id = assets.asset_id 
//                JOIN metadata ON metadata.metadata_id = asset_members.metadata_id 
//                
//                JOIN (
//
//                SELECT 
//                contacts.contact_id 
//                FROM contacts  
//                JOIN system_visibility_levels ON system_visibility_levels.visibility_id=contacts.visibility_id 
//                WHERE $member_visibility_string  
//
//                ) AS contact_match ON contact_match.contact_id = asset_members.member_id_value 
//
//                JOIN (
//
//                SELECT 
//                asset_members.asset_id AS distribution_sublist_email_asset_id, 
//                assets.asset_title as distribution_sublist_email_asset_title, 
//                asset_members.parent_member_id AS distribution_sublist_email_parent_member_id,
//                asset_members.member_id_value AS distribution_sublist_email_member_id, 
//                metadata.metadata_name AS distribution_sublist_email_metadata_name,
//                metadata.metadata_slug AS distribution_sublist_email_metadata_slug,
//                asset_type.type_name AS distribution_sublist_email_type_name 
//                FROM asset_members 
//                JOIN assets ON asset_members.asset_id = assets.asset_id 
//                JOIN asset_type ON assets.type_id = asset_type.type_id 
//                JOIN metadata ON metadata.metadata_id = asset_members.metadata_id 
//                WHERE asset_type.type_name='distribution_sublist_email' 
//
//                ) AS distribution_sublist_email ON distribution_sublist_email.distribution_sublist_email_parent_member_id=asset_members.member_id                
//                                
//                JOIN (
//
//                SELECT 
//                asset_members.asset_id AS distribution_sublist_sms_asset_id, 
//                assets.asset_title as distribution_sublist_sms_asset_title, 
//                asset_members.parent_member_id AS distribution_sublist_sms_parent_member_id,
//                asset_members.member_id_value AS distribution_sublist_sms_member_id, 
//                metadata.metadata_name AS distribution_sublist_sms_metadata_name,
//                metadata.metadata_slug AS distribution_sublist_sms_metadata_slug,
//                asset_type.type_name AS distribution_sublist_sms_type_name 
//                FROM asset_members 
//                JOIN assets ON asset_members.asset_id = assets.asset_id 
//                JOIN asset_type ON assets.type_id = asset_type.type_id 
//                JOIN metadata ON metadata.metadata_id = asset_members.metadata_id 
//                WHERE asset_type.type_name='distribution_sublist_sms' 
//
//                ) AS distribution_sublist_sms ON distribution_sublist_sms.distribution_sublist_sms_parent_member_id=asset_members.member_id  
//                
//                ) AS $asset_member_table",
//            'statement' => "(assets.asset_id=$asset_member_table.asset_id AND asset_type.type_name='distribution_list_contacts')",
//            'type' => 'left'
//            ) ; 
        

        
        $result = $this->DB->Query('SELECT_JOIN',$query_array);  
        $this->asset_query_result = $result ;
        
        return $result ;       
        }    
    
    
    

    
    
    
    
    public function Retrieve_Asset_List($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        
        //        $query_options = array(
        //            'filter_by_type_id' => 7,
        //            'filter_by_user' => true,
        //            'allow_global_assets' => true  // Allowed options: true, false, only.  Only means only global assets will be pulled.
        //            ) ; 
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $master_subqueries = array() ;
        
        //
        // PROCESSING
        //
                
        // Original fields...
//                asset_type.*, asset_content.*, 
//                assets.account_id, assets.asset_title, assets.asset_slug, assets.visibility_id, assets.global_asset, 
//                assets.current_history_id, assets.timestamp_asset_created, assets.timestamp_asset_updated, assets.timestamp_asset_published, 
//                system_visibility_levels.* 
                    
        // Base query:
        $query_array = array(
            'table' => 'asset_content',
            'join_tables' => array(),
            'fields' => "
                asset_core.*, 
                asset_content.text_01, text_02, 
                asset_content.asset_id_01, asset_content.asset_id_02, asset_content.asset_id_03, asset_content.asset_id_04, 
                asset_content.metadata_01, asset_content.metadata_02, asset_content.metadata_03, asset_content.metadata_04, 
                asset_content.structured_data_01, 
                system_visibility_levels.visibility_name, system_visibility_levels.visibility_title, system_visibility_levels.visibility_description, asset_type.*
                ",
//            'where' => "",
            'group_by' => "asset_content.asset_id"
            );
        

       

        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ; 
            
            if ($query_array['order_by'] == 'relevance') {
                $query_array['order_by'] = 'asset_core.asset_title' ; 
                $query_array['order_by_relevance'] = 'yes' ; 
                }  
            
            if (isset($query_options->order_field)) {
                $query_array['order_field'] = $query_options->order_field ;
                $query_array['order_start'] = $query_options->order_start ;
                $query_array['order_operator'] = $query_options->order_operator ;
                }           
            } 
        
        
//        // Add saved search filter
//        if (isset($query_options->saved_search_table)) { 
//            
//            $query_array['join_tables'][] = array(
//                'table' => $query_options->saved_search_table,
//                'on' => "$query_options->saved_search_table.key_id",
//                'match' => 'assets.asset_id'
//                );            
//            }
        
    
        // PROCESS ASSET IDs
        if (isset($query_options->filter_by_asset_id)) { 
            
            $query_options->filter_by_asset_id = Utilities::Array_Force($query_options->filter_by_asset_id) ; 
            
            $filter_by_asset_id_string = '' ; 
            foreach ($query_options->filter_by_asset_id as $query_asset_id) {
                $filter_by_asset_id_string .= "assets.asset_id='$query_asset_id' OR " ; 
                }
            $filter_by_asset_id_string = rtrim($filter_by_asset_id_string," OR ") ;
            $filter_by_asset_id_string = " AND ($filter_by_asset_id_string) " ;                  
            }
        
        if (isset($query_options->filter_by_asset_id_exclude)) {        
            
            $query_options->filter_by_asset_id_exclude = Utilities::Array_Force($query_options->filter_by_asset_id_exclude) ; 
            
            $filter_by_asset_id_exclude_string = '' ; 
            foreach ($query_options->filter_by_asset_id_exclude as $query_asset_id) {
                $filter_by_asset_id_exclude_string .= "assets.asset_id!='$query_asset_id' OR " ; 
                }
            $filter_by_asset_id_exclude_string = rtrim($filter_by_asset_id_exclude_string," OR ") ;
            $filter_by_asset_id_exclude_string = " AND ($filter_by_asset_id_exclude_string) " ;
            
            $filter_by_asset_id_string .= $filter_by_asset_id_exclude_string ;
            }
    
        
        // PROCESS ASSET VISIBILITY FILTER        
        $visibility_array = $this->Retrieve_Visibility_ID_Array($query_options) ; 

        
        // Add visibility filter if there are visibility_id's to process
        $filter_by_visibility_id_string = '' ; 
        $filter_by_globals_visibility_id_string = '' ; 
        $filter_by_shared_visibility_id_string = '' ; 
        
        if (count($visibility_array) > 0) {

            foreach ($visibility_array as $visibility) {
                
                $this_visibility_id = $visibility['visibility_id'] ; 
                
                $filter_by_visibility_id_string .= "assets.visibility_id='$this_visibility_id' 
                    OR " ;
                $filter_by_globals_visibility_id_string .= "asset_global_relationships.account_asset_visibility_id='$this_visibility_id' 
                    OR " ;
                
                switch ($visibility['visibility_name']) {
                    case 'visibility_shared':
                    case 'visibility_published':
                        $filter_by_shared_visibility_id_string .= "assets.visibility_id='$this_visibility_id' 
                            OR " ;        
                        break ; 
                    }                
                }
            
            $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," 
                OR ") ; 
            $filter_by_visibility_id_string = " AND ($filter_by_visibility_id_string) " ;
            
            $filter_by_globals_visibility_id_string = rtrim($filter_by_globals_visibility_id_string," 
                OR ") ; 
            $filter_by_globals_visibility_id_string = " AND ($filter_by_globals_visibility_id_string) " ;
            
            $filter_by_shared_visibility_id_string = rtrim($filter_by_shared_visibility_id_string," 
                OR ") ; // For items in account only
            $filter_by_shared_visibility_id_string = " AND ($filter_by_shared_visibility_id_string) " ;            
            }
        
        
        
        
        

        // PROCESS ORGANIZATION FILTER
        $organization_id_array = array() ; 
        
        // Submitted as an array of organization_id values.
        if (isset($query_options->filter_by_organization_id)) {
            // If an array wasn't submitted, turn it into an array
            $organization_id_array = Utilities::Array_Force($query_options->filter_by_organization_id) ;             
            }
                
        // Add organization filter if there are organization_id's to process
        if (count($organization_id_array) > 0) {
            $filter_by_organization_id_string = '' ; 
            foreach ($organization_id_array as $organization_id) {
                $filter_by_organization_id_string .= "assets.organization_id='$organization_id' 
                    OR " ; 
                }
            $filter_by_organization_id_string = rtrim($filter_by_organization_id_string," 
                OR ") ;
            $filter_by_organization_id_string = " AND ($filter_by_organization_id_string) " ;                
            } else {
                $filter_by_organization_id_string = '' ; 
                }
        
        

        
        // PROCESS PROFILE FILTER + PROFILE AUTHORIZATION
        // Requires the profile_id to be added as authorized on the asset
        if ($query_options->filter_by_profile_id == 'yes') {  
            
            if (isset($query_options->profile_id)) {
                // If an array wasn't submitted, turn it into an array
                $profile_id_array = Utilities::Array_Force($query_options->filter_by_profile_id) ;   
                
                } elseif (isset($this->profile_id)) {
                    $profile_id_array[] = $this->profile_id ; 
                } else {
                    $profile_id_array = array() ; 
                    }

        
            if (count($profile_id_array) > 0) {

                if (isset($query_options->filter_by_asset_auth_role_name)) {

                    $asset_auth_name_array = Utilities::Array_Force($query_options->filter_by_asset_auth_role_name) ; 

                    if (count($asset_auth_name_array) > 0) {   
                        $asset_auth_role_name_query_string = '' ; 
                        foreach ($asset_auth_name_array as $asset_auth_name) {
                            switch ($asset_auth_name) {
                                case 'authorized':
                                    $asset_auth_role_name_query_string .= "system_asset_auth_roles.asset_auth_role_name='asset_viewer' 
                                        OR " ;
                                    $asset_auth_role_name_query_string .= "system_asset_auth_roles.asset_auth_role_name='asset_commenter' 
                                        OR " ;
                                    $asset_auth_role_name_query_string .= "system_asset_auth_roles.asset_auth_role_name='asset_contributor' 
                                        OR " ;
                                    $asset_auth_role_name_query_string .= "system_asset_auth_roles.asset_auth_role_name='asset_editor' 
                                        OR " ;
                                    $asset_auth_role_name_query_string .= "system_asset_auth_roles.asset_auth_role_name='asset_admin' 
                                        OR " ;
                                    $asset_auth_role_name_query_string .= "system_asset_auth_roles.asset_auth_role_name='asset_owner' 
                                        OR " ;
                                    break ; 
                                default:
                                    $asset_auth_role_name_query_string .= "system_asset_auth_roles.asset_auth_role_name='$asset_auth_name' 
                                        OR " ;     
                                }
                            }
                        $asset_auth_role_name_query_string = rtrim($asset_auth_role_name_query_string," 
                            OR ") ;
                        $asset_auth_role_name_query_string = " AND ($asset_auth_role_name_query_string)" ;
                        }
                    } else {
                        $asset_auth_role_name_query_string = '' ; 
                        }


                $filter_by_profile_id_query_string = '' ; 
                foreach ($profile_id_array as $profile_id) {
                    switch ($profile_id) {
                        case 'yes':
                            $profile_id = $this->profile_id ; 
                        default:
                            $filter_by_profile_id_query_string .= " (asset_auth.profile_id='$profile_id' $asset_auth_role_name_query_string) OR " ; 
                            $master_subqueries['filter_by_profile_id'] = 'yes' ;
                            $master_subqueries['profile_id'] = $profile_id ;
                        } 
                    }
                $filter_by_profile_id_query_string = rtrim($filter_by_profile_id_query_string," OR ") ;
                $filter_by_profile_id_query_string = " AND ($filter_by_profile_id_query_string) " ;              

                }  
            } else {
                $filter_by_profile_id_query_string = '' ; 
                }       
        
        
         
        // PROCESS ASSET TYPE FILTER
        $type_id_array = $this->Retrieve_Asset_Type_ID_Array($query_options) ; 

        
        // Add asset type filter if there are type_id's to process
        if (count($type_id_array) > 0) {
            
            // Type filter to be used if additional types within allow_global_assets were not submitted
            $master_filter_by_asset_type_string = '' ; 
            foreach ($type_id_array as $type_id) {
                $master_filter_by_asset_type_string .= "assets.type_id='$type_id' OR " ; 
                }
            $master_filter_by_asset_type_string = rtrim($master_filter_by_asset_type_string," OR ") ;
            $master_filter_by_asset_type_string = "AND ($master_filter_by_asset_type_string) " ;                
            
            
            // This filter sits outside the global subqueries and serves as an override of the entire query in the main WHERE clause
            // Must say asset_core as table
            $uber_master_filter_by_asset_type_string = '' ; 
            foreach ($type_id_array as $type_id) {
                $uber_master_filter_by_asset_type_string .= "asset_core.type_id='$type_id' OR " ; 
                }
            $uber_master_filter_by_asset_type_string = rtrim($uber_master_filter_by_asset_type_string," OR ") ;
            $uber_master_filter_by_asset_type_string = "AND ($uber_master_filter_by_asset_type_string) " ;             
            $query_array['where'] .= $uber_master_filter_by_asset_type_string  ;
            
            }
        
        
        // If allow_global_assets was submitted as array, it allows us to set different global filters
        // depending on the asset type
        // If simply set as a string, then assume all asset types will get the same global filter
        // Regarless, this section translates allow_global_assets into an array even if submitted as string
        if (is_array($query_options->allow_global_assets)) {
            
            $query_options->allow_global_assets_array = $query_options->allow_global_assets ; 
            $query_options->allow_global_assets = array() ;  
            

            foreach ($query_options->allow_global_assets_array as $global_setting) {
                
                $global_setting = (object) $global_setting ;
                $this_global_type_id_array = $this->Retrieve_Asset_Type_ID_Array($global_setting) ; 

                
                // Add asset type filter if there are type_id's to process
                if (count($this_global_type_id_array) > 0) {
                    $filter_by_asset_type_string = '' ; 
                    foreach ($this_global_type_id_array as $type_id) {
                        $filter_by_asset_type_string .= "assets.type_id='$type_id' OR " ; 
                        }
                    $filter_by_asset_type_string = rtrim($filter_by_asset_type_string," OR ") ;
                    $filter_by_asset_type_string = "AND ($filter_by_asset_type_string) " ;                
                    }
                
                $this_global_array = array(
                    'allow_global_assets' => $global_setting->allow_global_assets,
                    'filter_by_asset_type_string' => $filter_by_asset_type_string
                    ) ;
                $query_options->allow_global_assets[] = $this_global_array ; 
 
                }
            } else {
            
                // With this option, all asset types will be submitted to the same allow_global_assets filter
            
                $allow_global_assets_string = $query_options->allow_global_assets ; 
                $query_options->allow_global_assets = array() ; 
            
                $this_global_array = array(
                    'allow_global_assets' => $allow_global_assets_string,
                    'filter_by_asset_type_string' => $master_filter_by_asset_type_string
                    ) ;
                $query_options->allow_global_assets[] = $this_global_array ; 
                }
        

        
        // PROCESS GLOBAL FILTER
        // Add in Profile + Account + Organization + Visibility filters
        // Uses the new allow_global_assets array created above
        foreach ($query_options->allow_global_assets as $global_asset_set) {
            
            $allow_global_assets = $global_asset_set['allow_global_assets'] ;
            $filter_by_asset_type_string = $global_asset_set['filter_by_asset_type_string'] ;

        
            switch ($allow_global_assets) { 
                case 'unselected_globals': 
                    $master_subqueries['unselected_globals'] = 1 ; 
                    $master_subqueries['selected_globals'] = 0 ; 
                    $master_subqueries['account_owned'] = 0 ;
                    break ; 
                case 'selected_globals': 
                    $master_subqueries['unselected_globals'] = 0 ; 
                    $master_subqueries['selected_globals'] = 1 ; 
                    $master_subqueries['account_owned'] = 0 ;
                    break ;
                case 'selected_nonglobals': 
                    $master_subqueries['unselected_globals'] = 0 ; 
                    $master_subqueries['selected_globals'] = 0 ; 
                    $master_subqueries['account_owned'] = 0 ;
                    $master_subqueries['selected_nonglobals'] = 1 ; 
                    break ;                                    
                case 'globals_only':    
                case 'only':
                    $master_subqueries['unselected_globals'] = 1 ; 
                    $master_subqueries['selected_globals'] = 1 ; 
                    $master_subqueries['account_owned'] = 0 ; 
                    break ;
                case 'ignore': // Ignore adding an global assets to query; only add account assets                
                case 'no':
                case 'account': 
                case 'account_only':                 
                    $master_subqueries['unselected_globals'] = 0 ; 
                    $master_subqueries['selected_globals'] = 0 ; 
                    $master_subqueries['account_owned'] = 1 ; 
                    break ;
                case 'all':    
                    $master_subqueries['unselected_globals'] = 1 ; 
                    $master_subqueries['selected_globals'] = 1 ; 
                    $master_subqueries['account_owned'] = 1 ; 
                    break ;                   
                case 1:
                case 'yes':                
                case 'account+selected_globals':   
                default:    
                    $master_subqueries['unselected_globals'] = 0 ; 
                    $master_subqueries['selected_globals'] = 1 ; 
                    $master_subqueries['account_owned'] = 1 ; 
                    break ;  
                }


                // SUB QUERY 1A: Create a sub query for global items the account has NOT selected
                $subquery_unselected_globals_input = array(
                    'skip_query' => 1,
                    'table' => 'assets',
                    'join_tables' => array(),
                    'fields' => "
                        assets.*, 
                        assets.account_id AS publishing_account_id,  
                        assets.visibility_id AS account_asset_visibility_id,
                        assets.timestamp_asset_created AS timestamp_account_asset_created,
                        assets.timestamp_asset_published AS timestamp_account_asset_published                
                        ",
                    'where' => "
                        (assets.account_id!=$this->account_id AND assets.global_asset=1 $filter_by_organization_id_string) 
                        AND (asset_global_relationships.global_publish_id IS NULL $filter_by_shared_visibility_id_string) 
                        $filter_by_asset_type_string 
                        $filter_by_asset_id_string
                        "
                    );
                $subquery_unselected_globals_input['join_tables'][] = array(
                    'table' => 'asset_global_relationships',
                    'statement' => "(asset_global_relationships.asset_id=assets.asset_id AND asset_global_relationships.account_id=$this->account_id)",
                    'type' => 'left'
                    );     

            
                // For Query 1A: Unselected GLobal Product Restriction Filter
                $join_table_name = 'asset_unselected_global_restrictions_product' ;  

                // Create the operator
                $join_type = 'left' ; 
                $subquery_unselected_globals_input['where'] .= "AND $join_table_name.global_restriction_id IS NULL " ; 

                $subquery_unselected_globals_input['join_tables'][] = array(
                    'table' => "
                        (SELECT asset_global_restrictions.* 
                        FROM asset_global_restrictions 
                        JOIN system_billing_plans ON system_billing_plans.product_id = asset_global_restrictions.restriction_id 
                        JOIN account_billing ON system_billing_plans.plan_id = account_billing.plan_id 
                        WHERE account_billing.account_id=$this->account_id)
                            AS $join_table_name",
                    'type' => $join_type,
                    'statement' => "
                            (assets.asset_id=$join_table_name.asset_id AND $join_table_name.restriction_type = 'product')"
                    );                         

            
            
                $subquery_unselected_globals_result = $this->DB->Query('SELECT_JOIN',$subquery_unselected_globals_input);        
                $subquery_unselected_globals = $subquery_unselected_globals_result['query'] ;      


                // SUB QUERY 1B: Create a sub query for NON-global items the account has HAS selected
                $subquery_selected_nonglobals_input = array(
                    'skip_query' => 1,
                    'table' => 'assets',
                    'join_tables' => array(),
                    'fields' => "
                        assets.*, 
                        assets.account_id AS publishing_account_id,  
                        assets.visibility_id AS account_asset_visibility_id,
                        assets.timestamp_asset_created AS timestamp_account_asset_created,
                        assets.timestamp_asset_published AS timestamp_account_asset_published                
                        ",
                    'where' => "
                        (assets.account_id!=$this->account_id AND assets.global_asset=0 $filter_by_organization_id_string) 
                        $filter_by_asset_type_string 
                        $filter_by_asset_id_string
                        "
                    );
                $subquery_selected_nonglobals_input['join_tables'][] = array(
                    'table' => 'asset_global_relationships',
                    'statement' => "(asset_global_relationships.asset_id=assets.asset_id AND asset_global_relationships.account_id=$this->account_id)",
                    );     

                // For Query 1B: Unselected GLobal Product Restriction Filter
                $join_table_name = 'asset_selected_nonglobal_restrictions_product' ;  

                // Create the operator
                $join_type = 'left' ; 
                $subquery_selected_nonglobals_input['where'] .= "AND $join_table_name.global_restriction_id IS NULL " ; 

                $subquery_selected_nonglobals_input['join_tables'][] = array(
                    'table' => "
                        (SELECT asset_global_restrictions.* 
                        FROM asset_global_restrictions 
                        JOIN system_billing_plans ON system_billing_plans.product_id = asset_global_restrictions.restriction_id 
                        JOIN account_billing ON system_billing_plans.plan_id = account_billing.plan_id 
                        WHERE account_billing.account_id=$this->account_id)
                            AS $join_table_name",
                    'type' => $join_type,
                    'statement' => "
                            (assets.asset_id=$join_table_name.asset_id AND $join_table_name.restriction_type = 'product')"
                    );                         

                $subquery_selected_nonglobals_result = $this->DB->Query('SELECT_JOIN',$subquery_selected_nonglobals_input);        
                $subquery_selected_nonglobals = $subquery_selected_nonglobals_result['query'] ;
            
            
                // SUB QUERY 2: Assets owned by account AND global assets selected by account 
                $subquery_account_assets_input = array(
                    'skip_query' => 1,
                    'table' => 'assets',
                    'join_tables' => array(),
                    'fields' => "
                        assets.*,  
                        assets.account_id AS publishing_account_id,                 
                        assets.visibility_id AS account_asset_visibility_id,
                        assets.timestamp_asset_created AS timestamp_account_asset_created,
                        assets.timestamp_asset_published AS timestamp_account_asset_published
                        ",
                    'where' => ""
                    );



                // Write the account sub queries...
                $master_account_selected_globals_string = " ((assets.global_asset=1 AND asset_global_relationships.account_id=$this->account_id) 
                    $filter_by_shared_visibility_id_string 
                    $filter_by_asset_id_string
                    ) " ;

                $master_account_owned_string = " (assets.account_id=$this->account_id 
                    $filter_by_profile_id_query_string 
                    $filter_by_visibility_id_string 
                    $filter_by_asset_id_string 
                    ) " ;        


                if ($master_subqueries['account_owned'] == 1) {
                    $subquery_account_assets_input['where'] .= "$master_account_owned_string" ;                
                    }
                if (($master_subqueries['account_owned'] == 1) AND ($master_subqueries['selected_globals'] == 1)) {
                    $subquery_account_assets_input['where'] .= " OR " ;
                    }        
                if ($master_subqueries['selected_globals'] == 1) {
                    $subquery_account_assets_input['where'] .= "$master_account_selected_globals_string" ;
                    }

                $subquery_account_assets_input['where'] = "(".$subquery_account_assets_input['where'].") $filter_by_asset_type_string " ; 

                $subquery_account_assets_input['join_tables'][] = array(
                    'table' => 'asset_global_relationships',
                    'statement' => "(asset_global_relationships.asset_id=assets.asset_id AND asset_global_relationships.account_id=$this->account_id)",
                    'type' => 'left'
                    ); 

            
            
                // Selected Global Product Restriction Filter
                $join_table_name = 'asset_selected_global_restrictions_product' ;  

                // Create the operator
                $join_type = 'left' ; 
                $subquery_account_assets_input['where'] .= " AND $join_table_name.global_restriction_id IS NULL " ; 

                $subquery_account_assets_input['join_tables'][] = array(
                    'table' => "
                        (SELECT asset_global_restrictions.* 
                        FROM asset_global_restrictions 
                        JOIN system_billing_plans ON system_billing_plans.product_id = asset_global_restrictions.restriction_id 
                        JOIN account_billing ON system_billing_plans.plan_id = account_billing.plan_id 
                        WHERE account_billing.account_id=$this->account_id)
                            AS $join_table_name",
                    'type' => $join_type,
                    'statement' => "
                            (assets.asset_id=$join_table_name.asset_id AND $join_table_name.restriction_type = 'product')"
                    ); 
            
            
            
                if ($master_subqueries['filter_by_profile_id'] == 'yes') { 

                    $profile_id = $master_subqueries['profile_id'] ; 

                    $subquery_account_assets_input['join_tables'][] = array(
                        'table' => 'asset_auth',
                        'on' => 'asset_auth.asset_id',
                        'match' => 'assets.asset_id',
                        'type' => 'left'
                        );

                    $subquery_account_assets_input['join_tables'][] = array(
                        'table' => 'system_asset_auth_roles',
                        'on' => 'system_asset_auth_roles.asset_auth_id',
                        'match' => 'asset_auth.asset_auth_id',
                        'type' => 'left'
                        ); 

                    $subquery_account_assets_input['join_tables'][] = array(
                        'table' => 'user_profiles',
                        'on' => 'user_profiles.profile_id',
                        'match' => 'asset_auth.profile_id',
                        'type' => 'left'
                        );         
                    }
                $this->Set_Model_Timings('account_sub_query') ;

                $subquery_account_assets_result = $this->DB->Query('SELECT_JOIN',$subquery_account_assets_input);        
                $subquery_account_assets = $subquery_account_assets_result['query'] ;

                $this->Set_Model_Timings('post_account_sub_query') ;

            // WRITE THE MASTER SUB QUERY JOIN...
            $master_join_statement = "" ;

                if ($master_subqueries['unselected_globals'] == 1) {
                    $master_join_statement .= $subquery_unselected_globals ;
                    // The inclusion of unselected globals requires a UNION to be added if we're also adding in 
                    // unselected globals and account owned items to weed out duplicates
                    if (($master_subqueries['selected_globals'] == 1) OR ($master_subqueries['account_owned'] == 1)) {
                        $master_join_statement .= ' UNION ' ; 
                        }
                    }
                    if (($master_subqueries['selected_globals'] == 1) OR ($master_subqueries['account_owned'] == 1)) {
                        $master_join_statement .= $subquery_account_assets ; 
                        }
                if ($master_subqueries['selected_nonglobals'] == 1) {
                    $master_join_statement .= $subquery_selected_nonglobals ;
                    // The inclusion of unselected globals requires a UNION to be added if we're also adding in 
                    // unselected globals and account owned items to weed out duplicates
//                    if (($master_subqueries['selected_globals'] == 1) OR ($master_subqueries['account_owned'] == 1)) {
//                        $master_join_statement .= ' UNION ' ; 
//                        }
                    }   

            // Takes the master_join_statement from this set of asset type subqueries and places them 
            // in an uber_master array
            $uber_master_join_array[] = $master_join_statement ; 
            }        
        
        
        // Splice all of the master_join_statements together, using UNION in order to eliminate duplicates
        $uber_master_join_statement = '' ;
        foreach ($uber_master_join_array as $uber_statement) {
            $uber_master_join_statement .= $uber_statement.' UNION ' ; 
            }
        $uber_master_join_statement = rtrim($uber_master_join_statement," UNION ") ;
        $uber_master_join_statement = "($uber_master_join_statement) AS asset_core " ;
        
        $this->test_data = $uber_master_join_statement ; 
        

        
        // Add in additional join tables...
        $query_array['join_tables'][] = array(
            'table' => $uber_master_join_statement,
            'on' => 'asset_core.asset_id',
            'match' => 'asset_content.asset_id',
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'asset_core.account_asset_visibility_id'
            );          
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_type',
            'on' => 'asset_type.type_id',
            'match' => 'asset_core.type_id'
            );
        
        // If retrieving asset structure, must add in a join to the structure or temp_structure table
        if (isset($query_options->filter_by_structure_asset_id)) {
            
            $query_array['join_tables'][] = array(
                'table' => $query_options->structure_table,
                'on' => 'asset_core.asset_id',
                'match' => "$query_options->structure_table.child_asset_id"
                );
            
            $query_array['fields'] .= ", $query_options->structure_table.child_asset_id, $query_options->structure_table.structure_order, $query_options->structure_table.delay" ;
            $query_array['where'] .= "$query_options->structure_table.asset_id='$query_options->filter_by_structure_asset_id' " ; 
            
            $query_array['order'] = "ASC" ;            
            $query_array['order_by'] = "$query_options->structure_table.structure_order" ;
            }        
        
        
        // Add parent asset data 
        $parent_asset_table = 'parent_asset_table' ; 

        $query_array['fields'] .= ", $parent_asset_table.asset_title AS parent_asset_title, 
            $parent_asset_table.type_name AS parent_asset_type_name, 
            $parent_asset_table.type_display_name AS parent_asset_type_display_name 
            " ; 

        $query_array['join_tables'][] = array(
            'table' => "(SELECT 
                assets.*, 
                asset_type.type_name, asset_type.type_display_name 
                FROM assets 
                JOIN asset_type ON asset_type.type_id = assets.type_id 
                
                ) AS $parent_asset_table",
            'statement' => "(asset_core.parent_asset_id=$parent_asset_table.asset_id)",
            'type' => 'left'
            );

        
        
        // Add in asset member (subscriber count) tables...
        if ($query_options->skip_asset_member_count != 'yes') {
            $asset_member_table = 'asset_members' ; 

            $query_array['fields'] .= ", 
                COUNT(asset_members.member_id_value) as asset_member_count, 

                SUM(case when $asset_member_table.email_metadata_slug = 'list-subscribed' then 1 else 0 end) distribution_sublist_email_subscribed, 
                SUM(case when $asset_member_table.email_metadata_slug = 'list-unsubscribed' then 1 else 0 end) distribution_sublist_email_unsubscribed, 
                SUM(case when $asset_member_table.email_metadata_slug = 'list-pending' then 1 else 0 end) distribution_sublist_email_pending, 
                SUM(case when $asset_member_table.email_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_email_cleaned, 
                SUM(case when $asset_member_table.email_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_email_restricted,
                SUM(case when $asset_member_table.email_metadata_slug = 'list-inactive' then 1 else 0 end) distribution_sublist_email_inactive, 

                SUM(case when $asset_member_table.sms_metadata_slug = 'list-subscribed' then 1 else 0 end) distribution_sublist_sms_subscribed, 
                SUM(case when $asset_member_table.sms_metadata_slug = 'list-unsubscribed' then 1 else 0 end) distribution_sublist_sms_unsubscribed, 
                SUM(case when $asset_member_table.sms_metadata_slug = 'list-pending' then 1 else 0 end) distribution_sublist_sms_pending, 
                SUM(case when $asset_member_table.sms_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_sms_cleaned, 
                SUM(case when $asset_member_table.sms_metadata_slug = 'list-cleaned' then 1 else 0 end) distribution_sublist_sms_restricted,
                SUM(case when $asset_member_table.sms_metadata_slug = 'list-inactive' then 1 else 0 end) distribution_sublist_sms_inactive 
                " ; 

            $query_array['join_tables'][] = array(
                'table' => "(SELECT 

                    asset_members.asset_id, asset_members.member_id_value, asset_members.metadata_id, 
                    asset_members.email_metadata_id, asset_members.sms_metadata_id, 
                    metadata.metadata_name, metadata.metadata_slug, 
                    email_metadata_name, email_metadata_slug, 
                    sms_metadata_name, sms_metadata_slug   
                    
                    FROM asset_members 
                    JOIN assets ON asset_members.asset_id = assets.asset_id 
                    JOIN metadata ON metadata.metadata_id = asset_members.metadata_id 

                    JOIN (

                    SELECT 
                    metadata.metadata_id,
                    metadata.metadata_name AS email_metadata_name,
                    metadata.metadata_slug AS email_metadata_slug 
                    FROM metadata 
                    
                    ) AS distribution_sublist_email ON distribution_sublist_email.metadata_id=asset_members.email_metadata_id                

                    JOIN (

                    SELECT 
                    metadata.metadata_id,
                    metadata.metadata_name AS sms_metadata_name,
                    metadata.metadata_slug AS sms_metadata_slug 
                    FROM metadata 

                    ) AS distribution_sublist_sms ON distribution_sublist_sms.metadata_id=asset_members.sms_metadata_id  

                    ) AS $asset_member_table",
                'statement' => "(asset_core.asset_id=$asset_member_table.asset_id)",
                'type' => 'left'
                );     
            
            } // End asset member count
        
        
        
        // Add Asset Owner. This is not a filter.
        $asset_owner_table = 'asset_owner' ; 
        
        $query_array['fields'] .= ", $asset_owner_table.asset_auth_role_name AS asset_owner_auth_role_name, 
            $asset_owner_table.asset_auth_role_title AS asset_owner_auth_role_title,
            $asset_owner_table.profile_display_name AS asset_owner_profile_display_name
            " ; 

        $query_array['join_tables'][] = array(
            'table' => "(SELECT 
                assets.asset_id, system_asset_auth_roles.*, user_profiles.* 
                FROM assets 
                JOIN asset_auth ON asset_auth.asset_id = assets.asset_id 
                JOIN system_asset_auth_roles ON system_asset_auth_roles.asset_auth_id = asset_auth.asset_auth_id 
                JOIN user_profiles ON user_profiles.profile_id = asset_auth.profile_id 
                WHERE system_asset_auth_roles.asset_auth_role_name='asset_owner'
                ) AS $asset_owner_table",
            'statement' => "(asset_core.asset_id=$asset_owner_table.asset_id)",
            'type' => 'left'
            );
        
        
        
        // Add Asset Author. This is not a filter.
        $asset_author_table = 'asset_author' ; 
        
        $query_array['fields'] .= ", $asset_author_table.asset_auth_role_name AS asset_author_auth_role_name, 
            $asset_author_table.asset_auth_role_title AS asset_author_auth_role_title,
            $asset_author_table.profile_display_name AS asset_author_profile_display_name
            " ; 

        $query_array['join_tables'][] = array(
            'table' => "(SELECT 
                assets.asset_id, system_asset_auth_roles.*, user_profiles.* 
                FROM assets 
                JOIN asset_auth ON asset_auth.asset_id = assets.asset_id 
                JOIN system_asset_auth_roles ON system_asset_auth_roles.asset_auth_id = asset_auth.asset_auth_id 
                JOIN user_profiles ON user_profiles.profile_id = asset_auth.profile_id 
                WHERE asset_auth.author='1'
                ) AS $asset_author_table",
            'statement' => "(asset_core.asset_id=$asset_author_table.asset_id)",
            'type' => 'left'
            );        
        
        
        
        // Add Profile Auth. This is not a filter.
        if (isset($this->profile_id)) {    
            $profile_auth_table = 'active_profile' ; 

            $query_array['fields'] .= ", $profile_auth_table.asset_auth_role_name AS active_profile_auth_role_name, 
                $profile_auth_table.asset_auth_role_title AS active_profile_auth_role_title,
                $profile_auth_table.profile_display_name AS active_profile_profile_display_name
                " ; 

            $query_array['join_tables'][] = array(
                'table' => "(SELECT 
                    assets.asset_id, system_asset_auth_roles.*, user_profiles.* 
                    FROM assets 
                    JOIN asset_auth ON asset_auth.asset_id = assets.asset_id 
                    JOIN system_asset_auth_roles ON system_asset_auth_roles.asset_auth_id = asset_auth.asset_auth_id 
                    JOIN user_profiles ON user_profiles.profile_id = asset_auth.profile_id 
                    WHERE user_profiles.profile_id='$this->profile_id'
                    ) AS $profile_auth_table",
                'statement' => "(asset_core.asset_id=$profile_auth_table.asset_id)",
                'type' => 'left'
                );
            }
        
        
        // Add Profile Filter to the main query
        if ($master_subqueries['filter_by_profile_id'] == 'yes') {
            
            $profile_id = $master_subqueries['profile_id'] ; 
                
            $query_array['fields'] .= ", asset_auth.asset_auth_id, system_asset_auth_roles.*" ;

            $query_array['join_tables'][] = array(
                'table' => 'asset_auth',
                'statement' => "(asset_auth.asset_id=asset_core.asset_id AND asset_auth.profile_id=$profile_id)",
                'type' => 'left'
                );
            
            $query_array['join_tables'][] = array(
                'table' => 'user_profiles',
                'on' => "asset_auth.profile_id",
                'match' => "user_profiles.profile_id",
                'type' => 'left'
                ); 
            
            $query_array['join_tables'][] = array(
                'table' => 'system_asset_auth_roles',
                'on' => 'system_asset_auth_roles.asset_auth_id',
                'match' => 'asset_auth.asset_auth_id',
                'type' => 'left'
                );                
            }
               

        
        $said = 1 ; // subasset id
        do {
            
            $subquery_subassets_input = array(
                'skip_query' => 1,
                'table' => 'assets',
                'join_tables' => array(),
                'fields' => "
                    assets.asset_id AS asset_0".$said."_id, 
                    assets.asset_title AS asset_0".$said."_title,
                    asset_content.text_01 AS asset_0".$said."_text_01,
                    asset_content.text_02 AS asset_0".$said."_text_02,
                    asset_content.asset_id_01 AS asset_0".$said."_asset_id_01,
                    asset_content.asset_id_02 AS asset_0".$said."_asset_id_02,
                    asset_content.asset_id_03 AS asset_0".$said."_asset_id_03,
                    asset_content.asset_id_04 AS asset_0".$said."_asset_id_04,
                    asset_content.metadata_01 AS asset_0".$said."_metadata_01,
                    asset_content.metadata_02 AS asset_0".$said."_metadata_02,
                    asset_content.metadata_03 AS asset_0".$said."_metadata_03,
                    asset_content.metadata_04 AS asset_0".$said."_metadata_04,
                    asset_content.structured_data_01 AS asset_0".$said."_structured_data_01,
                    asset_type.type_id AS asset_0".$said."_type_id,
                    asset_type.type_name AS asset_0".$said."_type_name,
                    asset_type.map AS asset_0".$said."_map
                    "
                );
            $subquery_subassets_input['join_tables'][] = array(
                'table' => 'asset_type',
                'on' => 'asset_type.type_id',
                'match' => 'assets.type_id'
                );        
            $subquery_subassets_input['join_tables'][] = array(
                'table' => 'asset_content',
                'on' => 'asset_content.asset_id',
                'match' => 'assets.asset_id'
                );


            $subquery_subassets_input_result = $this->DB->Query('SELECT_JOIN',$subquery_subassets_input);        
            $subquery_subassets = $subquery_subassets_input_result['query'] ; 

            $sub_assets_statement = "(".$subquery_subassets.") AS asset_0".$said." " ;        


            $query_array['join_tables'][] = array(
                'table' => $sub_assets_statement,
                'on' => 'asset_0'.$said.'.asset_0'.$said.'_id',
                'match' => 'asset_content.asset_id_0'.$said.'',
                'type' => 'left'
                );            
            

            $query_array['fields'] .= ", 
                asset_0".$said.".asset_0".$said."_title,
                asset_0".$said.".asset_0".$said."_type_id,
                asset_0".$said.".asset_0".$said."_type_name,
                asset_0".$said.".asset_0".$said."_map,
                asset_0".$said.".asset_0".$said."_text_01,
                asset_0".$said.".asset_0".$said."_text_02,
                asset_0".$said.".asset_0".$said."_asset_id_01,
                asset_0".$said.".asset_0".$said."_asset_id_02,
                asset_0".$said.".asset_0".$said."_asset_id_03,
                asset_0".$said.".asset_0".$said."_asset_id_04,
                asset_0".$said.".asset_0".$said."_metadata_01,
                asset_0".$said.".asset_0".$said."_metadata_02,
                asset_0".$said.".asset_0".$said."_metadata_03,
                asset_0".$said.".asset_0".$said."_metadata_04,
                asset_0".$said.".asset_0".$said."_structured_data_01" ;
            
            
            $said++ ; 
            } while ($said < 5);
        
        

        // Filter by tags
        if (isset($query_options->filter_by_tag_metadata_id)) {  
            
            $query_options->filter_by_tag_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;
            $count_metadata = count($query_options->filter_by_tag_metadata_id) ; 
            $metadata_comma_query_string = Utilities::Create_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;            
            
            $join_table_name = 'tag_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->tag_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            
            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT asset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM asset_metadata_relationships 
                    JOIN metadata ON asset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        asset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        GROUP BY asset_metadata_relationships.asset_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (asset_content.asset_id=$join_table_name.asset_id)"
                );                         
            }
        
        
        // Filter by categories
        if (isset($query_options->filter_by_category_metadata_id)) {
            
            $query_options->filter_by_category_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
            $count_metadata = count($query_options->filter_by_category_metadata_id) ; 
            $metadata_comma_query_string = Utilities::Create_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
            
            $join_table_name = 'category_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->category_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            


            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT asset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM asset_metadata_relationships 
                    JOIN metadata ON asset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        asset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        GROUP BY asset_metadata_relationships.asset_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (asset_content.asset_id=$join_table_name.asset_id)"
                );                         
            } 
        
        
        // Filter by general metadata_id
        if (isset($query_options->filter_by_metadata_id)) {
            
            $query_options->filter_by_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_metadata_id) ;
            $count_metadata = count($query_options->filter_by_metadata_id) ; 
            $metadata_comma_query_string = Utilities::Create_Comma_Separated_String($query_options->filter_by_metadata_id) ;
            
            $join_table_name = 'general_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    $metadata_having_string = '' ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 
                    $metadata_having_string = '' ; 
                    
                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            


            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT asset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM asset_metadata_relationships 
                    JOIN metadata ON asset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        asset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        GROUP BY asset_metadata_relationships.asset_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (asset_content.asset_id=$join_table_name.asset_id)"
                );                         
            }

        
        // Add timestamp_asset_published filter 
        if (isset($query_options->filter_by_timestamp_asset_published)) {

            $current_timestamp = TIMESTAMP ; 
            
            switch($query_options->filter_by_timestamp_asset_published) {
                case 'past':
                    $query_array['where'] .= " AND (asset_core.timestamp_asset_published>0 AND asset_core.timestamp_asset_published<$current_timestamp)" ; 
                    break ;
                case 'future':
                    $query_array['where'] .= " AND (asset_core.timestamp_asset_published>$current_timestamp)" ;
                    break ;
                }                    
            }
        
        
        
        // Add filter by search parameter
        if (isset($query_options->filter_by_search_parameter)) { 

            if ($query_options->search_parameter_match == 'exact') {
                
                    $search_token = Utilities::Escape($query_options->filter_by_search_parameter) ; 

                    $this_search_string = " (asset_core.asset_title LIKE '%$search_token%' OR 
                        asset_core.asset_description LIKE '%$search_token%' OR 
                        asset_content.text_01 LIKE '%$search_token%' OR 
                        asset_content.text_02 LIKE '%$search_token%' OR 
                        asset_content.metadata_01 LIKE '%$search_token%' OR 
                        asset_content.metadata_02 LIKE '%$search_token%' OR 
                        asset_content.metadata_03 LIKE '%$search_token%' OR 
                        asset_content.metadata_04 LIKE '%$search_token%'
                        ) " ; 

                    $compiled_search_string = $this_search_string ; 

                    $compiled_search_string = rtrim($compiled_search_string," OR ") ;     
                    $query_array['where'] .= " AND ($compiled_search_string) " ;                
                
                
                } else {
                
                    $search_parameter_token = explode(" ",$query_options->filter_by_search_parameter) ; 

                    $compiled_search_string = '' ; 

                    foreach ($search_parameter_token as $search_token) {

                        $search_token = Utilities::Escape($search_token) ; 

                        $this_search_string = " (asset_core.asset_title LIKE '%$search_token%' OR 
                            asset_core.asset_description LIKE '%$search_token%' OR 
                            asset_content.text_01 LIKE '%$search_token%' OR 
                            asset_content.text_02 LIKE '%$search_token%' OR 
                            asset_content.metadata_01 LIKE '%$search_token%' OR 
                            asset_content.metadata_02 LIKE '%$search_token%' OR 
                            asset_content.metadata_03 LIKE '%$search_token%' OR 
                            asset_content.metadata_04 LIKE '%$search_token%'
                            ) " ; 

                        $compiled_search_string .= $this_search_string.' OR ' ; 
                        }

                    $compiled_search_string = rtrim($compiled_search_string," OR ") ;     
                    $query_array['where'] .= " AND ($compiled_search_string) " ;                
                
                    }                     
            }
        
        
        if (isset($query_array['where'])) {
            $query_array['where'] = ltrim($query_array['where']," AND ") ;            
            }



        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->asset_list_query_result = $result ;   
//        $this->asset_structure_query_result = $result ;   
        
        if (($query_array['order_by_relevance'] == 'yes') AND (isset($query_options->filter_by_search_parameter))) {
            
            $relevance_options = array(
                'fields' => array('asset_title','asset_description','text_01','text_02','metadata_01','metadata_02','metadata_03','metadata_04') 
                ) ; 
            $result = $this->Sort_Search_Relevance($result,$query_array,$query_options,$relevance_options) ; 
            } 
                
        $query_options->value_name = 'asset_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Asset_Paging($result) ;         
        $this->asset_query_result = $result ; 
                
        return $result ;       
        }
    
    
    

    
    
    // Process a set of asset results and separate into paging components to use for site navigation
    public function Set_Asset_Paging($results_array) {

        if (!isset($this->asset_paging)) {
            $this->asset_paging = $this->Set_Default_Paging_Object() ; 
            }       
        
        $this->asset_paging->page_increment = $this->Get_Page_Increment() ;
        
        $this->asset_paging = $this->Action_Process_Paging($this->asset_paging,$results_array) ; 
        
        return $this ; 
        }    
    
    
    

    // Only use temp_table for broadcast right now
    public function Retrieve_Asset_Members($query_options = array()) { 
        
        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_initialize') ;
        
        global $server_config ; 
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ; 
            }
        
        if ($this->user['timezone']) {
            $timezone = $this->user['timezone'] ; 
            } else {
                $timezone = $this->system_timezone ; 
                } 
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write        
        
        // Create a permatable for asset members belonging to that asset with single-use metadata included
        if ($query_options->use_permatable == 'yes') { 
            $query_options->use_temp_table = 'no' ;
            }
        
        if ($query_options->use_temp_table == 'no') { 
            $temp_member_table = 'system_operations_asset_members_account_'.$this->account_id.'_asset_'.$this->asset_id ; 
            $use_temp_table = 0 ;
            } else {
                $temp_member_table = str_replace(".","",'temp_asset_member_list_'.microtime(true)) ;
                $use_temp_table = 1 ; 
                }

        $repull_temp_table = 1 ; // Indicates that members will be pulled into a fresh table for filtering. Use of permatable may change this to 0 if the data is fresh enough
        
        // If using permatable, determine whether or not to re-pull
        // Repull if a member was updated more recently than the last permatable pull
        // or if the last permatable pull happened more than an hour ago
        if (($use_temp_table == 0) 
            AND (($this->asset['timestamp_last_member_retrieve'] > $this->asset['timestamp_last_member_update']) AND (isset($this->asset['timestamp_last_member_update'])))
            AND ($this->asset['timestamp_last_member_retrieve'] > Utilities::System_Time_Manipulate('60 m','-')['time_manipulated'])
            ) {
            $repull_temp_table = 0 ;
            }         
        
        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_use_temp_table_'.$use_temp_table) ;
        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_repull_temp_table_'.$repull_temp_table) ;
        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_table_name_'.$temp_member_table) ;
        
        
        $query_array = array(
            'table' => 'assets',
            'fields' => "asset_members.*, 
                asset_type.*, 
                assets.account_id, assets.asset_title, assets.visibility_id, 
                assets.current_history_id, assets.timestamp_asset_created, assets.timestamp_asset_updated, assets.timestamp_asset_published,
                metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description, 
                system_visibility_levels.*, ",
            'where' => "", 
            'group_by' => "asset_members.member_id"
            );        
        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_content',
            'on' => 'asset_content.asset_id',
            'match' => 'assets.asset_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_members',
            'on' => 'asset_members.asset_id',
            'match' => 'assets.asset_id'
            );

        
        // Add email metadata
        $join_table_name = 'email_metadata' ;  
        $query_array['fields'] .= "$join_table_name.metadata_name AS email_metadata_name, 
            $join_table_name.metadata_slug AS email_metadata_slug,
            $join_table_name.metadata_description AS email_metadata_description, " ; 
            
        // Create the operator
        $join_type = 'left' ; 
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT metadata.* 
                FROM metadata WHERE metadata.metadata_type_id='4' 
                ) AS $join_table_name",
            'type' => $join_type,
            'statement' => "
                    (asset_members.email_metadata_id=$join_table_name.metadata_id)"
            ) ;
        
        
        // Add sms metadata
        $join_table_name = 'sms_metadata' ;  
        $query_array['fields'] .= "$join_table_name.metadata_name AS sms_metadata_name, 
            $join_table_name.metadata_slug AS sms_metadata_slug,
            $join_table_name.metadata_description AS sms_metadata_description, " ; 
            
        // Create the operator
        $join_type = 'left' ; 
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT metadata.* 
                FROM metadata WHERE metadata.metadata_type_id='4' 
                ) AS $join_table_name",
            'type' => $join_type,
            'statement' => "
                    (asset_members.sms_metadata_id=$join_table_name.metadata_id)"
            ) ;
        
        
        
        // Merge the attendee list with the status list (defined in metadata)
         $query_array['join_tables'][] = array(
            'table' => 'metadata',
            'on' => 'metadata.metadata_id',
            'match' => 'asset_members.metadata_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            ) ;
 
        $query_array['join_tables'][] = array(
            'table' => 'asset_type',
            'on' => 'asset_type.type_id',
            'match' => 'assets.type_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'assets.visibility_id'
            );
        
                
        // Automation Asset Member Subquery
        $join_table_name = 'campaign_automation' ;  
        $query_array['fields'] .= "$join_table_name.campaign_position, $join_table_name.automation_status_id, 
            $join_table_name.automation_status_id, $join_table_name.timestamp_next_action, 
            $join_table_name.campaign_id, $join_table_name.campaign_title, 
            $join_table_name.status_name AS automation_status_name, $join_table_name.status_title AS automation_status_title, 
            $join_table_name.asset_title AS automation_asset_title, $join_table_name.asset_id AS automation_asset_id, 
            $join_table_name.type_id AS automation_asset_type_id, $join_table_name.type_name AS automation_asset_type_name, 
                $join_table_name.type_display_name AS automation_asset_type_display_name, $join_table_name.type_icon AS automation_asset_type_icon, " ; 
            
            
        // Create the operator
        $join_type = 'left' ; 
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT asset_campaign_automation.*, 
                    asset_campaigns.campaign_title, 
                    asset_status.status_name, 
                    asset_status.status_title,
                    assets.asset_id, assets.asset_title, 
                    asset_type.type_id, asset_type.type_name, asset_type.type_display_name, asset_type.type_icon 
                FROM asset_campaign_automation 
                JOIN asset_campaigns ON asset_campaigns.campaign_id = asset_campaign_automation.campaign_id 
                JOIN asset_status ON asset_status.asset_status_id = asset_campaign_automation.automation_status_id 
                LEFT JOIN asset_structure ON (asset_structure.asset_id = asset_campaigns.asset_id) AND (asset_structure.structure_order = asset_campaign_automation.campaign_position) 
                LEFT JOIN assets ON assets.asset_id = asset_structure.child_asset_id 
                LEFT JOIN asset_type ON assets.type_id = asset_type.type_id
                ) AS $join_table_name",
            'type' => $join_type,
            'statement' => "
                    (asset_members.automation_id=$join_table_name.automation_id)"
            ) ; 
        
        
        // Campaign Status - Asset Member Subquery
        $join_table_name = 'member_asset_status' ;  
        $query_array['fields'] .= "$join_table_name.status_name AS member_status_name, 
            $join_table_name.status_title AS member_status_title, " ; 
            
        // Create the operator
        $join_type = 'left' ; 
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT asset_status.* 
                FROM asset_status 
                ) AS $join_table_name",
            'type' => $join_type,
            'statement' => "
                    (asset_members.member_status_id=$join_table_name.asset_status_id)"
            ) ;        
        

        
        // PROCESS DELIVERY ASSET TYPE VISIBILITY FILTER        
        $visibility_array = $this->Retrieve_Visibility_ID_Array($query_options) ; 
        
        // Add visibility filter if there are visibility_id's to process
        $filter_by_visibility_id_string = '' ; 
        if (count($visibility_array) > 0) {

            foreach ($visibility_array as $visibility) {
                
                $this_visibility_id = $visibility['visibility_id'] ; 
                $filter_by_visibility_id_string .= "assets.visibility_id='$this_visibility_id' OR " ;               
                }
            
            $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," OR ") ;
            $query_array['where'] .= "AND ($filter_by_visibility_id_string) " ;  
            }
        
        
        // PROCESS DELIVERY ASSET TYPE FILTER
        if (isset($query_options->filter_by_type_name)) {
                        
            $asset_type_query_options = new stdClass();
            $asset_type_query_options->filter_by_type_name = $query_options->filter_by_type_name ; 
                
            $asset_type_id_array = $this->Retrieve_Asset_Type_ID_Array($asset_type_query_options) ; 

            // Add asset type filter if there are type_id's to process
            if (count($asset_type_id_array) > 0) {
                $filter_by_asset_type_string = '' ; 
                foreach ($asset_type_id_array as $type_id) {
                    $filter_by_asset_type_string .= "assets.type_id='$type_id' OR " ; 
                    }
                $filter_by_asset_type_string = rtrim($filter_by_asset_type_string," OR ") ;
                $query_array['where'] .= " AND ($filter_by_asset_type_string) " ;                
                }            
            }
        
        
        if (isset($query_options->filter_by_type_id)) {
            switch ($query_options->filter_by_type_id) {
                case 'no':    
                    // Intentionally blank
                    break ;
                case 'yes':  
                default: 
                    $query_array['where'] .= "AND assets.type_id='$query_options->type_id' " ;
                }
            }

        if (isset($query_options->asset_id)) {
            $query_array['where'] .= "AND assets.asset_id='$query_options->asset_id' " ;
            }


        switch($query_options->filter_by_asset_id) {
            case 'no':    
                // Intentionally blank
                break ;
            case 'yes':  // Requires members to be currently part of the internally set asset
            default: 
                $query_array['where'] .= "AND assets.asset_id='$this->asset_id' " ;
            }
        
        
        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_filter_by_type_id_'.$query_options->filter_by_type_id) ;
        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_filter_by_type_id_'.$query_options->type_id) ;
        
        if ($query_options->filter_by_type_id == 'yes') {
            switch ($query_options->type_id) {
//                case 16: // Event - Ties to Contacts
                case 19: // Subscriber List - Ties to Contacts  
                case 26: // Subscriber Series List - Ties to Contacts 
                     
                    $fields_array = array() ; 
                    $fields_array[] = 'temp_id INT NOT NULL PRIMARY KEY' ;
                    
                    $fields_array[] = 'type_id INT(4)' ; 
                    $fields_array[] = 'type_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'type_display_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'type_abbreviation VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'asset_id INT(10)' ; 
                    $fields_array[] = 'asset_title VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'contact_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'account_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'user_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'visibility_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'email_address VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'first_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'last_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'company VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'role VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'contact_description MEDIUMTEXT NOT NULL' ; 
                    $fields_array[] = 'phone_country_dialing_code VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'phone_number VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'phone_type VARCHAR(20) NOT NULL' ;
                    $fields_array[] = 'address_01 VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'address_02 VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'city VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'state VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'postal_code VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'country_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'date_birthday VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'timestamp_birthday_next BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_birthday_last BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_contact_created BIGINT(40)' ; 
                    $fields_array[] = 'created_source VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'import_file_task_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'yl_member_number VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'rank_current INT(10) NOT NULL' ; 
                    $fields_array[] = 'rank_highest INT(10) NOT NULL' ; 
                    $fields_array[] = 'timestamp_yl_join_date BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_yl_activation_date BIGINT(40)' ; 
                    $fields_array[] = 'yl_enroller_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_enroller_member_number VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_sponsor_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_sponsor_member_number VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'birthday_short VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'full_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'starred_favorite_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'country_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'country_value VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'country_timezone VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'country_dialing_code VARCHAR(220) NOT NULL' ; 
                                        
                    $fields_array[] = 'rank_current_metadata_name VARCHAR(50) NOT NULL' ; 
                    $fields_array[] = 'rank_highest_metadata_name VARCHAR(50) NOT NULL' ; 
                    
                    $fields_array[] = 'contact_rating_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'contact_rating_metadata_slug VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'contact_rating_metadata_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'contact_rating_metadata_name VARCHAR(220) NOT NULL' ; 
                    
                    $fields_array[] = 'contact_status_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'contact_status_metadata_slug VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'contact_status_metadata_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'contact_status_metadata_name VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'yl_contact_group_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_contact_group_metadata_slug VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_contact_group_metadata_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_contact_group_metadata_name VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'yl_member_type_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_member_type_metadata_slug VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_member_type_metadata_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_member_type_metadata_name VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'yl_member_status_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_member_status_metadata_slug VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_member_status_metadata_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_member_status_metadata_name VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'yl_team_status_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_team_status_metadata_slug VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_team_status_metadata_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_team_status_metadata_name VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'yl_essential_rewards_status_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_essential_rewards_status_metadata_slug VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_essential_rewards_status_metadata_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_essential_rewards_status_metadata_name VARCHAR(220) NOT NULL' ;
                                        
                    $fields_array[] = 'yl_account_status_relationship_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_account_status_metadata_slug VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_account_status_metadata_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'yl_account_status_metadata_name VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'member_id INT(10)' ; 
//                    $fields_array[] = 'parent_member_id INT(10)' ; 
                    $fields_array[] = 'member_id_value INT(10)' ; 
                    $fields_array[] = 'metadata_id INT(10)' ; 
                    $fields_array[] = 'email_metadata_id INT(10)' ; 
                    $fields_array[] = 'sms_metadata_id INT(10)' ; 
                    $fields_array[] = 'campaign_id INT(10)' ; 
                    $fields_array[] = 'automation_id INT(10)' ; 
                    $fields_array[] = 'member_status_id INT(10)' ; 
                    $fields_array[] = 'timestamp_member_created BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_member_updated BIGINT(40)' ; 
                    
//                    $fields_array[] = 'asset_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'member_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'parent_member_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_name_distribution_sublist_email VARCHAR(220) NOT NULL' ;
                    
//                    $fields_array[] = 'asset_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'member_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'parent_member_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_name_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
                    
                    // ***** THESE ARE THE NEW ONES FOR 7.15
                    $fields_array[] = 'email_metadata_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'email_metadata_slug VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'email_metadata_description VARCHAR(220) NOT NULL' ;                    
                    $fields_array[] = 'sms_metadata_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'sms_metadata_slug VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'sms_metadata_description VARCHAR(220) NOT NULL' ;

                    
                    $fields_array[] = 'campaign_position DECIMAL(5,2)' ;     
                    $fields_array[] = 'automation_status_id INT(10)' ; 
                    $fields_array[] = 'timestamp_next_action BIGINT(40)' ; 
                    
                    $fields_array[] = 'campaign_title TEXT NOT NULL' ;
                    $fields_array[] = 'automation_status_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_status_title TEXT NOT NULL' ;
                    
                    $fields_array[] = 'automation_asset_title TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_id INT(10)' ;
                    $fields_array[] = 'automation_asset_type_id INT(10)' ;
                    $fields_array[] = 'automation_asset_type_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_type_display_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_type_icon TEXT NOT NULL' ;
                                                    
                    $fields_array[] = 'member_status_name TEXT NOT NULL' ;
                    $fields_array[] = 'member_status_title TEXT NOT NULL' ;
                    
                    
                    $import_fields_array = array() ;
                    foreach ($fields_array as $field_name) {
                        $import_fields_array[] = ltrim(explode(" ",$field_name)[0],"(") ;
                        }
                       
                    
                    // Check to see if the permatable / temp table exists
                    $check_table_query = array(
                        'table' => $temp_member_table
                        ) ;
                    $check_table = $this->DB->Query('SHOW_TABLES',$check_table_query) ;
                    
                    
                    // If table does not exist, create it
                    if ($check_table['result_count'] == 0) { 
                        
                        $repull_temp_table = 1 ; 
                        $create_table_array = array(
                            'new_table' => $temp_member_table,
                            'fields_array' => $fields_array
                            ) ; 

                        $create_table = $this->DB->Query('CREATE_TABLE',$create_table_array) ;
    //                    $this->asset_query_result2 = $create_table ; 

                        $add_primary_statement = 'ALTER TABLE '.$temp_member_table.' MODIFY temp_id INT NOT NULL AUTO_INCREMENT' ;
                        $add_primary = $this->DB->_connection->query($add_primary_statement) ;                        
                        }
                    
                    
                    // If the table does already exist, and using permatable, and results need to be repulled, truncate it
                    if (($check_table['result_count'] == 1) AND ($use_temp_table == 0) AND ($repull_temp_table == 1)) {
                        
                        $truncate_query = array(
                            'table' => $temp_member_table
                            ) ; 
                        $truncate_table = $this->DB->Query('TRUNCATE_TABLE',$truncate_query) ;
                        }

                    
                    $query_array['fields'] .= "
                        contacts.*, 
                            contact_details.yl_member_number, 
                            contact_details.rank_current, contact_details.rank_highest, 
                            contact_details.timestamp_yl_join_date, contact_details.timestamp_yl_activation_date, 
                            contact_details.yl_enroller_name, contact_details.yl_enroller_member_number, 
                            contact_details.yl_sponsor_name, contact_details.yl_sponsor_member_number,
                            yl_rank_current_metadata.metadata_name AS rank_current_metadata_name, 
                            yl_rank_highest_metadata.metadata_name AS rank_highest_metadata_name, 

                        CONCAT(contacts.first_name,' ',contacts.last_name) AS full_name, 
                        DATE_FORMAT(contacts.date_birthday, '%m/%d') AS birthday_short,
                        contact_favorites_metadata_relationships.relationship_id AS starred_favorite_relationship_id, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code, 
                        system_visibility_levels.visibility_name, system_visibility_levels.visibility_title, " ;


                    
                    $query_array['join_tables'][] = array(
                        'table' => 'contacts',
                        'on' => 'contacts.contact_id',
                        'match' => 'asset_members.member_id_value' 
                        );

                    $query_array['join_tables'][] = array(
                        'table' => 'contact_details',
                        'on' => 'contact_details.contact_id',
                        'match' => 'contacts.contact_id',
                        'type' => 'left'
                        );
                    
//                    $query_array['join_tables'][] = array(
//                        'table' => 'contact_restrictions AS contact_restrictions_email',
//                        'statement' => '(contact_restrictions_email.email_address=contacts.email_address 
//                            AND contact_restrictions_email.account_id=contacts.account_id 
//                            AND contact_restrictions_email.contact_id=contacts.contact_id)',
//                        'type' => 'left'
//                        );
//
//                    $query_array['join_tables'][] = array(
//                        'table' => 'contact_restrictions AS contact_restrictions_phone',
//                        'statement' => '(contact_restrictions_phone.phone_number_international=CONCAT(contacts.phone_country_dialing_code,contacts.phone_number) 
//                                AND contact_restrictions_phone.account_id=contacts.account_id 
//                                AND contact_restrictions_phone.contact_id=contacts.contact_id)',
//                        'type' => 'left'
//                        );

                    $query_array['join_tables'][] = array(
                        'table' => 'list_countries',
                        'on' => 'list_countries.country_id',
                        'match' => 'contacts.country_id',
                        'type' => 'left'
                        );

                    
                    // Add starred favorites metadata query
                    $starred_favorite_metadata_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'starred_favorite')['value'] ; 

                    $query_array['join_tables'][] = array(
                        'table' => 'nonasset_metadata_relationships AS contact_favorites_metadata_relationships',
                        'statement' => "(contact_favorites_metadata_relationships.item_id=contacts.contact_id) AND (contact_favorites_metadata_relationships.relationship_type='contact') AND (contact_favorites_metadata_relationships.metadata_id='$starred_favorite_metadata_id')",
                        'type' => 'left'
                        );                                         
                    
                    $query_array['join_tables'][] = array(
                        'table' => 'metadata AS yl_rank_current_metadata',
                        'on' => 'yl_rank_current_metadata.metadata_id',
                        'match' => 'contact_details.rank_current',
                        'type' => 'left'
                        ); 
                    
                    $query_array['join_tables'][] = array(
                        'table' => 'metadata AS yl_rank_highest_metadata',
                        'on' => 'yl_rank_highest_metadata.metadata_id',
                        'match' => 'contact_details.rank_highest',
                        'type' => 'left'
                        );
                    
                    // ** CONTACT METADATA SUBQUERIES ** 
                    $metadata_subquery_array = array() ;
                    $metadata_subquery_array[] = array(
                        'subquery_prefix' => 'contact_rating', // Contact Rating Subquery
                        ) ; 
                    $metadata_subquery_array[] = array(
                        'subquery_prefix' => 'contact_status', // Contact Status Subquery
                        ) ; 
                    $metadata_subquery_array[] = array(            
                        'subquery_prefix' => 'yl_contact_group', // YL Contact Group Subquery
                        ) ; 
                    $metadata_subquery_array[] = array(            
                        'subquery_prefix' => 'yl_member_type', // YL Member Type Subquery
                        ) ; 
                    $metadata_subquery_array[] = array(            
                        'subquery_prefix' => 'yl_team_status', // YL Team Status Subquery
                        ) ;
                    $metadata_subquery_array[] = array(            
                        'subquery_prefix' => 'yl_essential_rewards_status', // YL Essential Rewards Subquery
                        ) ;        
                    $metadata_subquery_array[] = array(            
                        'subquery_prefix' => 'yl_account_status', // YL Account Status Subquery
                        ) ;                

                    // Process and build metadata subqueries
                    foreach ($metadata_subquery_array as $metadata_subquery) {

                        // Subquery variables
                        $metdata_subquery_prefix = $metadata_subquery['subquery_prefix'] ; 
                        $metdata_subquery_metadata_type_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', $metdata_subquery_prefix.'_metadata_type')['value'] ; // Identify the metadata_type_id
                        $metdata_subquery_table_name = $metdata_subquery_prefix."_metadata_relationships" ; // Subquery table name

                        // Add in Subquery fields
                        $query_array['fields'] .= " 
                            $metdata_subquery_table_name.relationship_id AS ".$metdata_subquery_prefix."_relationship_id, 
                            $metdata_subquery_table_name.metadata_slug AS ".$metdata_subquery_prefix."_metadata_slug, 
                            $metdata_subquery_table_name.metadata_id AS ".$metdata_subquery_prefix."_metadata_id, 
                            $metdata_subquery_table_name.metadata_name AS ".$metdata_subquery_prefix."_metadata_name, " ;

                        // Subquery Statement
                        $query_array['join_tables'][] = array(
                            'table' => "(SELECT nonasset_metadata_relationships.*, metadata.metadata_name, metadata.metadata_slug, metadata_type.* 
                                FROM nonasset_metadata_relationships 
                                JOIN metadata ON metadata.metadata_id = nonasset_metadata_relationships.metadata_id  
                                JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id  
                                WHERE nonasset_metadata_relationships.relationship_type='contact' AND 
                                    metadata_type.metadata_type_id=$metdata_subquery_metadata_type_id) AS $metdata_subquery_table_name",
                            'statement' => "($metdata_subquery_table_name.item_id=contacts.contact_id)",
                            'type' => 'left'
                            ); 
                        }
                    // ** END: CONTACT METADATA SUBQUERIES **
                    
                

                    // Add filter by contact_id
                    // This needs to be submitted as an array of contact_id values.
                    if (isset($query_options->contact_id)) {
                        // If an array wasn't submitted, turn it into an array
                        $contact_id_array = Utilities::Array_Force($query_options->contact_id) ;   
                        $query_options->filter_by_contact_id = 'list' ; 
                        }


                    switch($query_options->filter_by_contact_id) {                    
                        case 'no':

                            break ;   
                        case 'yes':                 
                            $query_array['where'] .= "AND contacts.contact_id='$this->contact_id' " ;
                            break ; 
                        case 'list':

                            $contact_id_query_string = '' ; 
                            foreach ($contact_id_array as $contact_id) {

                                $contact_id_query_string .= "contacts.contact_id='$contact_id' OR " ; 
                                }
                            $contact_id_query_string = rtrim($contact_id_query_string," OR ") ; 
                            $query_array['where'] .= "AND ($contact_id_query_string) " ;
                            break ;                
                        }
                    
                    
                    switch($query_options->filter_by_user_id) {
                        case 'no':    
                            // Intentionally blank
                            break ;
                        case 'yes':  // Requires contacts to be owned by the internally set user
                        default: 
                            $query_array['where'] .= "AND contacts.user_id='$this->user_id' " ;
                        }

                    switch($query_options->filter_by_account_id) {
                        case 'no':    
                            // Intentionally blank
                            break ;
                        case 'asset': // Requires contacts to be owned by the account that the asset belongs to
                            $asset_account_id = $this->assset['account_id'] ; 
                            $query_array['where'] .= "AND contacts.account_id='$asset_account_id' " ;
                            break ;                        
                        case 'yes':  // Requires contacts to be owned by the internally set account
                        default: 
                            $query_array['where'] .= "AND contacts.account_id='$this->account_id' " ;
                        }                     

                    
                    // Process the RSVP status filter
                    // ---->  Need to add this I think
                    
                    

                    break ; 
                case 20: // User List
                case 28: // User SERIES list
                    
                    $fields_array = array() ; 
                    $fields_array[] = 'temp_id INT NOT NULL PRIMARY KEY' ;

                    $fields_array[] = 'type_id INT(4)' ; 
                    $fields_array[] = 'type_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'type_display_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'type_abbreviation VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'asset_id INT(10)' ; 
                    $fields_array[] = 'asset_title VARCHAR(220) NOT NULL' ; 

                    $fields_array[] = 'user_id INT(10) NOT NULL' ; 
                    
                    $fields_array[] = 'email_address_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'email_verified INT(10) NOT NULL' ;                             
                    $fields_array[] = 'email_address VARCHAR(220) NOT NULL' ; 
                    
                    $fields_array[] = 'phone_number_id INT(10) NOT NULL' ;                                 
                    $fields_array[] = 'phone_country_dialing_code VARCHAR(20) NOT NULL' ; 
                    $fields_array[] = 'phone_number VARCHAR(200) NOT NULL' ; 
                    $fields_array[] = 'phone_verified INT(10) NOT NULL' ;       
                    
                    $fields_array[] = 'first_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'last_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'full_name VARCHAR(220) NOT NULL' ; 
                    
                    $fields_array[] = 'street_address_1 VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'street_address_2 VARCHAR(220) NOT NULL' ;  
                    $fields_array[] = 'city VARCHAR(220) NOT NULL' ;  
                    $fields_array[] = 'state VARCHAR(220) NOT NULL' ;  
                    $fields_array[] = 'postal_code VARCHAR(220) NOT NULL' ;  
                    $fields_array[] = 'country_id VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'timezone VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'country_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'country_value VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'country_dialing_code VARCHAR(20) NOT NULL' ;                    
                    
                    $fields_array[] = 'referring_profile_id INT(10) NOT NULL' ; 
                    
                    $fields_array[] = 'timestamp_created BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_updated BIGINT(40)' ; 
                    
                    $fields_array[] = 'auth_role_title VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'auth_role_description VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'user_auth_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'user_auth_role_name VARCHAR(200) NOT NULL' ;
                    $fields_array[] = 'user_auth_role_title VARCHAR(200) NOT NULL' ;
                    $fields_array[] = 'user_auth_role_description VARCHAR(200) NOT NULL' ;

                    $fields_array[] = 'member_id INT(10)' ; 
//                    $fields_array[] = 'parent_member_id INT(10)' ; 
                    $fields_array[] = 'member_id_value INT(10)' ; 
                    $fields_array[] = 'metadata_id INT(10)' ; 
                    $fields_array[] = 'email_metadata_id INT(10)' ; 
                    $fields_array[] = 'sms_metadata_id INT(10)' ; 
                    $fields_array[] = 'campaign_id INT(10)' ; 
                    $fields_array[] = 'automation_id INT(10)' ; 
                    $fields_array[] = 'member_status_id INT(10)' ; 
                    $fields_array[] = 'timestamp_member_created BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_member_updated BIGINT(40)' ; 
                    
//                    $fields_array[] = 'asset_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'member_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'parent_member_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_name_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    
//                    $fields_array[] = 'asset_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'member_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'parent_member_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_name_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
                    
                    // ***** THESE ARE THE NEW ONES FOR 7.15
                    $fields_array[] = 'email_metadata_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'email_metadata_slug VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'email_metadata_description VARCHAR(220) NOT NULL' ;                    
                    $fields_array[] = 'sms_metadata_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'sms_metadata_slug VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'sms_metadata_description VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'campaign_position DECIMAL(5,2)' ;     
                    $fields_array[] = 'automation_status_id INT(10)' ; 
                    $fields_array[] = 'timestamp_next_action BIGINT(40)' ; 
                    
                    $fields_array[] = 'campaign_title TEXT NOT NULL' ;
                    $fields_array[] = 'automation_status_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_status_title TEXT NOT NULL' ;
                    
                    $fields_array[] = 'automation_asset_title TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_id INT(10)' ;
                    $fields_array[] = 'automation_asset_type_id INT(10)' ;
                    $fields_array[] = 'automation_asset_type_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_type_display_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_type_icon TEXT NOT NULL' ;
                                                    
                    $fields_array[] = 'member_status_name TEXT NOT NULL' ;
                    $fields_array[] = 'member_status_title TEXT NOT NULL' ;
                    
                    
                    $import_fields_array = array() ;
                    foreach ($fields_array as $field_name) {
                        $import_fields_array[] = ltrim(explode(" ",$field_name)[0],"(") ;
                        }

                    
                    // Check to see if the permatable / temp table exists
                    $check_table_query = array(
                        'table' => $temp_member_table
                        ) ;
                    $check_table = $this->DB->Query('SHOW_TABLES',$check_table_query) ;
                    
                    
                    // If table does not exist, create it
                    if ($check_table['result_count'] == 0) {
                        
                        $repull_temp_table = 1; 
                        $create_table_array = array(
                            'new_table' => $temp_member_table,
                            'fields_array' => $fields_array
                            ) ; 

                        $create_table = $this->DB->Query('CREATE_TABLE',$create_table_array) ;
    //                    $this->asset_query_result2 = $create_table ; 

                        $add_primary_statement = 'ALTER TABLE '.$temp_member_table.' MODIFY temp_id INT NOT NULL AUTO_INCREMENT' ;
                        $add_primary = $this->DB->_connection->query($add_primary_statement) ;                        
                        }
                    
                    
                    // If the table does already exist, and using permatable, and results need to be repulled, truncate it
                    if (($check_table['result_count'] == 1) AND ($use_temp_table == 0) AND ($repull_temp_table == 1)) {
                        
                        $truncate_query = array(
                            'table' => $temp_member_table
                            ) ; 
                        $truncate_table = $this->DB->Query('TRUNCATE_TABLE',$truncate_query) ;
                        }                    
                    // END: Check for temp table and create
                    
                    
                    
                    
                    $query_array['fields'] .= "
                        users.*, 
                        CONCAT(users.first_name,' ',users.last_name) AS full_name, 
                        user_emails.email_address, user_emails.email_verified, 
                        user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                        system_auth_roles.auth_role_title, system_auth_roles.auth_role_description, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code, " ;     

                    $query_array['join_tables'][] = array(
                        'table' => 'users',
                        'on' => 'users.user_id',
                        'match' => 'asset_members.member_id_value' 
                        );

                    $query_array['join_tables'][] = array(
                        'table' => 'user_emails',
                        'on' => 'users.email_address_id',
                        'match' => 'user_emails.email_address_id'
                        );

                    $query_array['join_tables'][] = array(
                        'table' => 'user_phone_numbers',
                        'on' => 'users.phone_number_id',
                        'match' => 'user_phone_numbers.phone_number_id',
                        'type' => 'left'
                        );

//                    $query_array['join_tables'][] = array(
//                        'table' => 'user_restrictions AS user_restrictions_email',
//                        'statement' => '(user_restrictions_email.email_address=user_emails.email_address AND user_restrictions_email.user_id=users.user_id)',
//                        'type' => 'left'
//                        );
//
//                    $query_array['join_tables'][] = array(
//                        'table' => 'user_restrictions AS user_restrictions_phone',
//                        'statement' => '(user_restrictions_phone.phone_number_international=CONCAT(user_phone_numbers.phone_country_dialing_code,user_phone_numbers.phone_number) AND user_restrictions_phone.user_id=users.user_id)',
//                        'type' => 'left'
//                        ); 
                    
                    $query_array['join_tables'][] = array(
                        'table' => 'system_auth_roles',
                        'on' => 'system_auth_roles.auth_id',
                        'match' => 'users.user_auth_id'
                        );

                    $query_array['join_tables'][] = array(
                        'table' => 'list_countries',
                        'on' => 'list_countries.country_id',
                        'match' => 'users.country_id',
                        'type' => 'left'
                        );




                    // User Auth Role subquery    
                    $subquery_user_auth_role_input = array(
                        'skip_query' => 1,
                        'table' => 'system_auth_roles',
                        'fields' => "
                            system_auth_roles.auth_id AS user_auth_id, 
                            system_auth_roles.auth_role_name AS user_auth_role_name, 
                            system_auth_roles.auth_role_title AS user_auth_role_title, 
                            system_auth_roles.auth_role_description AS user_auth_role_description,
                            system_auth_roles.auth_role_permissions AS user_auth_role_permissions
                            "
                        );       

                    $subquery_user_auth_role_result = $this->DB->Query('SELECT',$subquery_user_auth_role_input);        
                    $subquery_user_auth_role = $subquery_user_auth_role_result['query'] ; 

                    $sub_user_auth_role_statement = "(".$subquery_user_auth_role.") AS user_auth_role " ;        

                    $query_array['join_tables'][] = array(
                        'table' => $sub_user_auth_role_statement,
                        'on' => 'user_auth_role.user_auth_id',
                        'match' => 'users.user_auth_id'
                        );                

                    $query_array['fields'] .= " 
                        user_auth_role.user_auth_id,
                        user_auth_role.user_auth_role_name,
                        user_auth_role.user_auth_role_title,
                        user_auth_role.user_auth_role_description,
                        user_auth_role.user_auth_role_permissions, " ;

                    // END: User Auth Role subquery  
                    


                    
                    break ;
                case 23: // Account User List - Profiles - Teams
                case 27: // Profile SERIES list
                    
                    $fields_array = array() ; 
                    $fields_array[] = 'temp_id INT NOT NULL PRIMARY KEY' ;

                    $fields_array[] = 'type_id INT(4)' ; 
                    $fields_array[] = 'type_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'type_display_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'type_abbreviation VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'asset_id INT(10)' ; 
                    $fields_array[] = 'asset_title VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'user_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'profile_id INT(10) NOT NULL' ;
                    $fields_array[] = 'account_id INT(10) NOT NULL' ;
                    
                    $fields_array[] = 'email_address_id INT(10) NOT NULL' ; 
                    $fields_array[] = 'email_verified INT(10) NOT NULL' ;                             
                    $fields_array[] = 'email_address VARCHAR(220) NOT NULL' ; 
                    
                    $fields_array[] = 'phone_number_id INT(10) NOT NULL' ;                                 
                    $fields_array[] = 'phone_country_dialing_code VARCHAR(20) NOT NULL' ; 
                    $fields_array[] = 'phone_number VARCHAR(200) NOT NULL' ; 
                    $fields_array[] = 'phone_verified INT(10) NOT NULL' ;       
                    
                    $fields_array[] = 'messaging_number_id INT(10) NOT NULL' ;
                    
                    $fields_array[] = 'first_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'last_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'full_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'profile_display_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'profile_username VARCHAR(220) NOT NULL' ; 
                    
                    $fields_array[] = 'profile_role VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'profile_description MEDIUMTEXT NOT NULL' ; 
                    
                    $fields_array[] = 'facebook VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'instagram VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'pinterest VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'twitter VARCHAR(220) NOT NULL' ; 
                    
                    $fields_array[] = 'account_display_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'billing_id VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'billing_status VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'version VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'marketing_credit_balance INT(10) NOT NULL' ;         
                    
                    $fields_array[] = 'user_auth_id INT(10) NOT NULL' ;
                    $fields_array[] = 'user_auth_role_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'user_auth_role_title VARCHAR(220) NOT NULL' ; 
                    
                    $fields_array[] = 'account_auth_id INT(10) NOT NULL' ;
                    $fields_array[] = 'account_auth_role_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'account_auth_role_title VARCHAR(220) NOT NULL' ; 
                    
                    $fields_array[] = 'street_address_1 VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'street_address_2 VARCHAR(220) NOT NULL' ;  
                    $fields_array[] = 'city VARCHAR(220) NOT NULL' ;  
                    $fields_array[] = 'state VARCHAR(220) NOT NULL' ;  
                    $fields_array[] = 'postal_code VARCHAR(220) NOT NULL' ;  
                    $fields_array[] = 'country_id VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'timezone VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'country_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'country_value VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'country_dialing_code VARCHAR(20) NOT NULL' ;                    
                    
                    $fields_array[] = 'timestamp_profile_created BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_profile_updated BIGINT(40)' ;
                    $fields_array[] = 'timestamp_created BIGINT(40)' ; // Account
                    $fields_array[] = 'timestamp_updated BIGINT(40)' ; // Account
                    
                    $fields_array[] = 'member_id INT(10)' ; 
//                    $fields_array[] = 'parent_member_id INT(10)' ; 
                    $fields_array[] = 'member_id_value INT(10)' ; 
                    $fields_array[] = 'metadata_id INT(10)' ; 
                    $fields_array[] = 'email_metadata_id INT(10)' ; 
                    $fields_array[] = 'sms_metadata_id INT(10)' ; 
                    $fields_array[] = 'campaign_id INT(10)' ; 
                    $fields_array[] = 'automation_id INT(10)' ; 
                    $fields_array[] = 'member_status_id INT(10)' ; 
                    $fields_array[] = 'timestamp_member_created BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_member_updated BIGINT(40)' ; 
                    
//                    $fields_array[] = 'asset_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'member_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'parent_member_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_id_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_name_distribution_sublist_email VARCHAR(220) NOT NULL' ;
//                    
//                    $fields_array[] = 'asset_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'member_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'parent_member_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_id_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
//                    $fields_array[] = 'metadata_name_distribution_sublist_sms VARCHAR(220) NOT NULL' ;
                    
                    // ***** THESE ARE THE NEW ONES FOR 7.15
                    $fields_array[] = 'email_metadata_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'email_metadata_slug VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'email_metadata_description VARCHAR(220) NOT NULL' ;                    
                    $fields_array[] = 'sms_metadata_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'sms_metadata_slug VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'sms_metadata_description VARCHAR(220) NOT NULL' ;
                    
                    $fields_array[] = 'campaign_position DECIMAL(5,2)' ;     
                    $fields_array[] = 'automation_status_id INT(10)' ; 
                    $fields_array[] = 'timestamp_next_action BIGINT(40)' ; 
                    
                    $fields_array[] = 'campaign_title TEXT NOT NULL' ;
                    $fields_array[] = 'automation_status_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_status_title TEXT NOT NULL' ;
                    
                    $fields_array[] = 'automation_asset_title TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_id INT(10)' ;
                    $fields_array[] = 'automation_asset_type_id INT(10)' ;
                    $fields_array[] = 'automation_asset_type_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_type_display_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_type_icon TEXT NOT NULL' ;
                                                    
                    $fields_array[] = 'member_status_name TEXT NOT NULL' ;
                    $fields_array[] = 'member_status_title TEXT NOT NULL' ;
                    
                    
                    $import_fields_array = array() ;
                    foreach ($fields_array as $field_name) {
                        $import_fields_array[] = ltrim(explode(" ",$field_name)[0],"(") ;
                        }
                                        

                    
                    // Check to see if the permatable / temp table exists
                    $check_table_query = array(
                        'table' => $temp_member_table
                        ) ;
                    $check_table = $this->DB->Query('SHOW_TABLES',$check_table_query) ;
                    
                    
                    // If table does not exist, create it
                    if ($check_table['result_count'] == 0) {
                        
                        $repull_temp_table = 1; 
                        $create_table_array = array(
                            'new_table' => $temp_member_table,
                            'fields_array' => $fields_array
                            ) ; 

                        $create_table = $this->DB->Query('CREATE_TABLE',$create_table_array) ;
    //                    $this->asset_query_result2 = $create_table ; 

                        $add_primary_statement = 'ALTER TABLE '.$temp_member_table.' MODIFY temp_id INT NOT NULL AUTO_INCREMENT' ;
                        $add_primary = $this->DB->_connection->query($add_primary_statement) ;                        
                        }
                    
                    
                    // If the table does already exist, and using permatable, and results need to be repulled, truncate it
                    if (($check_table['result_count'] == 1) AND ($use_temp_table == 0) AND ($repull_temp_table == 1)) {
                        
                        $truncate_query = array(
                            'table' => $temp_member_table
                            ) ; 
                        $truncate_table = $this->DB->Query('TRUNCATE_TABLE',$truncate_query) ;
                        }                    
                    // END: Check for temp table and create                  
                    
                    
                    
                    $query_array['group_by'] = "user_profiles.profile_id" ; 
                    
                    $query_array['fields'] .= " 
                        users.*, 
                        user_profiles.*, 
                        CONCAT(user_profiles.first_name,' ',user_profiles.last_name) AS full_name, 
                        accounts.*,
                        account_billing.*,
                        user_emails.email_address, user_emails.email_verified, 
                        user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                        list_countries.country_name, list_countries.country_value, list_countries.country_timezone, list_countries.country_dialing_code, " ;     

                    $query_array['join_tables'][] = array(
                        'table' => 'user_profiles',
                        'on' => 'user_profiles.profile_id',
                        'match' => 'asset_members.member_id_value' 
                        );

                    $query_array['join_tables'][] = array(
                        'table' => 'users',
                        'on' => 'users.user_id',
                        'match' => 'user_profiles.user_id' 
                        );
                    
                    $query_array['join_tables'][] = array(
                        'table' => 'accounts',
                        'on' => 'accounts.account_id',
                        'match' => 'user_profiles.account_id' 
                        );
                    
                    $query_array['join_tables'][] = array(
                        'table' => 'account_billing',
                        'on' => 'account_billing.account_id',
                        'match' => 'accounts.account_id',
                        'type' => 'left'
                        );
                    
                    $query_array['join_tables'][] = array(
                        'table' => 'user_emails',
                        'on' => 'user_profiles.email_address_id',
                        'match' => 'user_emails.email_address_id',
                        'type' => 'left'
                        );

                    $query_array['join_tables'][] = array(
                        'table' => 'user_phone_numbers',
                        'on' => 'user_profiles.phone_number_id',
                        'match' => 'user_phone_numbers.phone_number_id',
                        'type' => 'left'
                        );
                    
                    $query_array['join_tables'][] = array(
                        'table' => 'list_countries',
                        'on' => 'list_countries.country_id',
                        'match' => 'users.country_id',
                        'type' => 'left'
                        );
                    
                    
//                    $query_array['join_tables'][] = array(
//                        'table' => 'user_profile_restrictions AS user_profile_restrictions_email',
//                        'statement' => '(user_profile_restrictions_email.email_address=user_emails.email_address AND user_profile_restrictions_email.account_id=user_profiles.account_id AND user_profile_restrictions_email.profile_id=user_profiles.profile_id)',
//                        'type' => 'left'
//                        );
//
//                    $query_array['join_tables'][] = array(
//                        'table' => 'user_profile_restrictions AS user_profile_restrictions_phone',
//                        'statement' => '(user_profile_restrictions_phone.phone_number_international=CONCAT(user_phone_numbers.phone_country_dialing_code,user_phone_numbers.phone_number) AND user_profile_restrictions_phone.account_id=user_profiles.account_id AND user_profile_restrictions_phone.profile_id=user_profiles.profile_id)',
//                        'type' => 'left'
//                        ); 

                    
                    
                    // User Auth Role subquery    
                    $subquery_user_auth_role_input = array(
                        'skip_query' => 1,
                        'table' => 'system_auth_roles',
                        'fields' => "
                            system_auth_roles.auth_id AS user_auth_id, 
                            system_auth_roles.auth_role_name AS user_auth_role_name, 
                            system_auth_roles.auth_role_title AS user_auth_role_title, 
                            system_auth_roles.auth_role_description AS user_auth_role_description,
                            system_auth_roles.auth_role_permissions AS user_auth_role_permissions
                            "
                        );       

                    $subquery_user_auth_role_result = $this->DB->Query('SELECT',$subquery_user_auth_role_input);        
                    $subquery_user_auth_role = $subquery_user_auth_role_result['query'] ; 

                    $sub_user_auth_role_statement = "(".$subquery_user_auth_role.") AS user_auth_role " ;        

                    $query_array['join_tables'][] = array(
                        'table' => $sub_user_auth_role_statement,
                        'on' => 'user_auth_role.user_auth_id',
                        'match' => 'users.user_auth_id'
                        );                

                    $query_array['fields'] .= " 
                        user_auth_role.user_auth_id,
                        user_auth_role.user_auth_role_name,
                        user_auth_role.user_auth_role_title,
                        user_auth_role.user_auth_role_description,
                        user_auth_role.user_auth_role_permissions, " ;
                    // END: User Auth Role subquery 
                    
                    
                    // Profile Auth Role subquery    
                    $subquery_account_auth_role_input = array(
                        'skip_query' => 1,
                        'table' => 'system_auth_roles',
                        'fields' => "
                            system_auth_roles.auth_id AS account_auth_id, 
                            system_auth_roles.auth_role_name AS account_auth_role_name, 
                            system_auth_roles.auth_role_title AS account_auth_role_title, 
                            system_auth_roles.auth_role_description AS account_auth_role_description,
                            system_auth_roles.auth_role_permissions AS account_auth_role_permissions
                            "
                        );       


                    $subquery_account_auth_role_result = $this->DB->Query('SELECT',$subquery_account_auth_role_input);        
                    $subquery_account_auth_role = $subquery_account_auth_role_result['query'] ; 

                    $sub_account_auth_role_statement = "(".$subquery_account_auth_role.") AS account_auth_role " ;        

                    $query_array['join_tables'][] = array(
                        'table' => $sub_account_auth_role_statement,
                        'on' => 'account_auth_role.account_auth_id',
                        'match' => 'user_profiles.account_auth_id'
                        );                

                    $query_array['fields'] .= " 
                        account_auth_role.account_auth_id,
                        account_auth_role.account_auth_role_name,
                        account_auth_role.account_auth_role_title,
                        account_auth_role.account_auth_role_description,
                        account_auth_role.account_auth_role_permissions, " ;
                    
                    // END: Profile Auth Role subquery    
                

                    break ;
                default:

                    // Something here if we don't find a type id matching?

                } 
            } else {  // If we're not filtering by type_id 
                
                    $use_temp_table = 1 ; 
            
                    $fields_array = array() ; 
                    $fields_array[] = 'temp_id INT NOT NULL PRIMARY KEY' ;
                    
                    $fields_array[] = 'type_id INT(4)' ; 
                    $fields_array[] = 'type_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'type_display_name VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'type_abbreviation VARCHAR(220) NOT NULL' ; 
                    $fields_array[] = 'asset_id INT(10)' ; 
                    $fields_array[] = 'asset_title VARCHAR(220) NOT NULL' ;
            
                    $fields_array[] = 'member_id INT(10)' ; 
//                    $fields_array[] = 'parent_member_id INT(10)' ; 
                    $fields_array[] = 'member_id_value INT(10)' ; 
                    $fields_array[] = 'metadata_id INT(10)' ; 
                    $fields_array[] = 'email_metadata_id INT(10)' ; 
                    $fields_array[] = 'sms_metadata_id INT(10)' ; 
                    $fields_array[] = 'campaign_id INT(10)' ; 
                    $fields_array[] = 'automation_id INT(10)' ; 
                    $fields_array[] = 'member_status_id INT(10)' ; 
                    $fields_array[] = 'timestamp_member_created BIGINT(40)' ; 
                    $fields_array[] = 'timestamp_member_updated BIGINT(40)' ; 

                    // ***** THESE ARE THE NEW ONES FOR 7.15
                    $fields_array[] = 'email_metadata_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'email_metadata_slug VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'email_metadata_description VARCHAR(220) NOT NULL' ;                    
                    $fields_array[] = 'sms_metadata_name VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'sms_metadata_slug VARCHAR(220) NOT NULL' ;
                    $fields_array[] = 'sms_metadata_description VARCHAR(220) NOT NULL' ;
            
            
                    $fields_array[] = 'campaign_position DECIMAL(5,2)' ;     
                    $fields_array[] = 'automation_status_id INT(10)' ; 
                    $fields_array[] = 'timestamp_next_action BIGINT(40)' ; 
                    
                    $fields_array[] = 'campaign_title TEXT NOT NULL' ;
                    $fields_array[] = 'automation_status_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_status_title TEXT NOT NULL' ;
                    
                    $fields_array[] = 'automation_asset_title TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_id INT(10)' ;
                    $fields_array[] = 'automation_asset_type_id INT(10)' ;
                    $fields_array[] = 'automation_asset_type_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_type_display_name TEXT NOT NULL' ;
                    $fields_array[] = 'automation_asset_type_icon TEXT NOT NULL' ;
            
            
            
            
                    $import_fields_array = array() ;
                    foreach ($fields_array as $field_name) {
                        $import_fields_array[] = ltrim(explode(" ",$field_name)[0],"(") ;
                        }
                                        

            
                    // Check for temp table and create
                    $check_table_query = array(
                        'table' => $temp_member_table
                        ) ;
                    $check_table = $this->DB->Query('SHOW_TABLES',$check_table_query) ;
                                        
                    if ($check_table['result_count'] == 0) {
                        
                        $create_table_array = array(
                            'new_table' => $temp_member_table,
                            'fields_array' => $fields_array
                            ) ; 

                        $create_table = $this->DB->Query('CREATE_TABLE',$create_table_array) ;
    //                    $this->asset_query_result2 = $create_table ; 

                        $add_primary_statement = 'ALTER TABLE '.$temp_member_table.' MODIFY temp_id INT NOT NULL AUTO_INCREMENT' ;
                        $add_primary = $this->DB->_connection->query($add_primary_statement) ;                        
                        }
                    
                    if ($check_table['result_count'] == 1) {
                        
                        $truncate_query = array(
                            'table' => $temp_member_table
                            ) ; 
                        $truncate_table = $this->DB->Query('TRUNCATE_TABLE',$truncate_query) ;
                        }                    
                    // END: Check for temp table and create            
            
                    }
        

        
        
//        // ADD AUTOMATION MEMBER PRE-FILTERS
//        // Filter by an array of member_status_name's to identify the status of the member's automation 
//        if (isset($query_options->filter_by_member_status_name)) {
//
//            // Convert input to array
//            $member_status_name_array = Utilities::Array_Force($query_options->filter_by_member_status_name) ; 
//
//            if (count($member_status_name_array) > 0) {
//                $filter_by_member_status_name_string = '' ; 
//                foreach ($member_status_name_array as $member_status_name) {
//
//                    $filter_by_member_status_name_string .= "member_asset_status.status_name='$member_status_name' OR " ;
//
//                    }
//                $filter_by_member_status_name_string = rtrim($filter_by_member_status_name_string," OR ") ;
//                $query_array['where'] .= "AND ($filter_by_member_status_name_string) " ;                
//                }                 
//            }
//        
//        
//        // Filter by an array of member_status_name's to identify the status of the member's automation 
//        if (isset($query_options->filter_by_campaign_position)) {
//
//            // Convert input to array
//            $campaign_position_array = Utilities::Array_Force($query_options->filter_by_campaign_position) ; 
//
//            if (count($campaign_position_array) > 0) {
//                $filter_by_campaign_position_string = '' ; 
//                foreach ($campaign_position_array as $campaign_position) {
//
//                    $filter_by_campaign_position_string .= "campaign_automation.campaign_position='$campaign_position' OR " ;
//
//                    }
//                $filter_by_campaign_position_string = rtrim($filter_by_campaign_position_string," OR ") ;
//                $query_array['where'] .= "AND ($filter_by_campaign_position_string) " ;                
//                }                 
//            } 

        
        // Filter by an array of automation ids ON the asset_members table
        // Needed for filtering Series
        if (isset($query_options->filter_by_automation_id)) {

            // Convert input to array
            $automation_id_array = Utilities::Array_Force($query_options->filter_by_automation_id) ; 

            if (count($automation_id_array) > 0) {
                $filter_by_automation_id_string = '' ; 
                foreach ($automation_id_array as $automation_id) {

                    $filter_by_automation_id_string .= "asset_members.automation_id='$automation_id' OR " ;

                    }
                $filter_by_automation_id_string = rtrim($filter_by_automation_id_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_automation_id_string) " ;                
                }                 
            }
        
        
        // ADD ASSET MEMBER PRE-FILTERS
        // Filter by an array of member_id_values's (which could be contact id's, user id's, or profile id's)
        if (isset($query_options->filter_by_member_id_value)) {

            // Convert input to array
            $member_id_value_array = Utilities::Array_Force($query_options->filter_by_member_id_value) ; 

            if (count($member_id_value_array) > 0) {
                $filter_by_member_id_value_string = '' ; 
                foreach ($member_id_value_array as $member_id_value) {

                    $filter_by_member_id_value_string .= "asset_members.member_id_value='$member_id_value' OR " ;

                    }
                $filter_by_member_id_value_string = rtrim($filter_by_member_id_value_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_member_id_value_string) " ;                
                }                 
            }        

        
        // Filter by an array of member_id's (which are unique to the asset members list)
        if (isset($query_options->filter_by_member_id)) { 

            // Convert input to array
            $member_id_array = Utilities::Process_Comma_Separated_String($query_options->filter_by_member_id) ; 

            if (count($member_id_array) > 0) {
                $filter_by_member_id_string = '' ; 
                foreach ($member_id_array as $member_id) {

                    $filter_by_member_id_string .= "asset_members.member_id='$member_id' OR " ;

                    }
                $filter_by_member_id_string = rtrim($filter_by_member_id_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_member_id_string) " ;                
                }                 
            }         


        


        // FINALIZE QUERY TO GET RESULTS FOR PERMA / TEMP TABLE
        // Remove leading "AND" from master WHERE statement
        $query_array['where'] = ltrim($query_array['where'],"AND ") ;
//        error_log('pre_retrieve_initial_merge_table') ;
        

//        $this->asset['timestamp_last_member_update'] = TIMESTAMP ; 
        // If using permatable, determine whether or not to re-pull
        if (($use_temp_table == 0) AND ($repull_temp_table == 1)) { 
            
            // Update last member retrieve for permatable
            $asset_update = new Asset() ;
            $asset_update->Set_Replicate_User_Account_Profile($this) ; 

            $update_asset_pull = array(
                'auth_override' => 'yes',
                'asset_input_info' => array(
                    'rewrite_latest_history' => 'yes'
                    ),
                'asset_id' => $this->asset_id,
                'timestamp_last_member_retrieve' => time(),
                'timestamp_last_member_update' => time()-1,
                'member_table_name' => $temp_member_table
                ) ; 
            if ((isset($this->asset['visibility_id'])) AND ($this->asset_id == $this->asset['asset_id'])) {
                $update_asset_pull['visibility_id'] = $this->asset['visibility_id'] ; 
                } 

            $permatable_record = $asset_update->Action_Update_Content($update_asset_pull)->Get_Asset() ; 
//            $this->Add_Model_Timings_Array($asset_update->Get_Model_Timings()['timings']) ; 
            $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_updated_last_retrieve_date_for_permatable') ;
            } 
        
        
        
//        $repull_temp_table = 1 ; 
        // PULL DATA INTO TEMP / PERMATABLE TABLE
        if ($repull_temp_table == 1) {
            
            $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_pre_retrieve_temp_table') ;
            
            // CREATE THE TEMP TABLE...        
            $initial_query_array = $query_array ; 
            $initial_query_array['limit'] = 6000 ; 
            $initial_query_array['order_by'] = 'asset_members.member_id' ; 
            $initial_query_array['order'] = 'ASC' ; 
            $last_pulled_member_id = 0 ; 

            $initial_set = 0 ; 
            $initial_pull_halt = 0 ; 
            do {

                $initial_query_array_repull = $initial_query_array ; 
                $initial_query_array_repull['where'] .= "AND (asset_members.member_id>$last_pulled_member_id)" ; 
                $result = $this->DB->Query('SELECT_JOIN',$initial_query_array_repull,'force'); 
                $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_retrieve_initial_merge_table_'.$initial_set) ;
    //            error_log('retrieve_initial_merge_table_'.$initial_set) ;
                $this->asset_member_query_result = $result ;
    //            unset($query_array['skip_query']) ; 


                if ($result['result_count'] == 0) {
                    $initial_pull_halt = 1 ; 
                    }

                if ($result['result_count'] > 0) {

                    // CREATE THE TEMP TABLE...
                    $asset_type_id = $result['results'][0]['type_id'] ;
                    $import_values_array = array() ;         
                    $i = 0 ; 
                    $set = 0 ; 
                    $set_value = 0 ; 
                    $max_bulk_insert_value = 4000 ; 
                    
                    $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_initialize_bulk_insert') ;
    //                error_log('initialize_bulk_insert') ;

                    foreach ($result['results'] as $member_result) {

                        $import_row = array() ; 
                        foreach ($import_fields_array as $field_key) {
                            $import_row[$field_key] = $member_result[$field_key] ; 
                            }

                        $import_values_array[] = $import_row ; 
                        $i++ ; 
                        $set_value++ ; 


                        // If there are more than 1000 members in the pull, then import them into the 
                        // temp table in batches of 1000. Reset temp arrays along the way to reduce server load.
                        if ($set_value == $max_bulk_insert_value) {

                            $import_query_array = array(
                                'table' => $temp_member_table,
                                'fields' => $import_fields_array,
                                'values' => $import_values_array
                                ) ; 

                            $result_temp_table = $this->DB->Query('BULK_INSERT',$import_query_array);
                            $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_insert_set_'.$set) ;
    //                        error_log('insert_set_'.$set) ;

                            $set++ ; 
                            $set_value = 0 ;
                            unset($import_values_array) ; 
                            $import_values_array = array() ; 

                            }

                        $last_pulled_member_id = $member_result['member_id'] ; 
                        }

                    if ($set_value > 0) {
                        // Final catch-all import into temp table
                        $import_query_array = array(
                            'table' => $temp_member_table,
                            'fields' => $import_fields_array,
                            'values' => $import_values_array
                            ) ; 

                        $result_temp_table = $this->DB->Query('BULK_INSERT',$import_query_array); 
                        $this->asset_member_insert_result = $result_temp_table ;     
                        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_catch_all_set_'.$set) ;
        //                error_log('catch_all_set_'.$set) ;
                        unset($import_query_array,$result_temp_table,$result) ;                
                        $query_array['offset'] = $query_array['offset'] + $query_array['limit'] ; 
                        $initial_set++ ;
                        }
                    
                    if ($set_value != $max_bulk_insert_value) {
//                        $initial_pull_halt = 1 ; 
                        }
                    }

                } while ($initial_pull_halt == 0) ;


            unset($query_array['limit'],$query_array['offset']) ;
            $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_end_create_temp_table') ;
    //        error_log('end_create_temp_table') ;
            // END: CREATE TEMP TABLE
        
            }


        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_pre_prep_filtered_temp_table') ;
        // NOW FILTER THE RESULTS OF THE TEMP TABLE....
        $query_array = array(
            'table' => $temp_member_table,
            'fields' => "$temp_member_table.*, ",
            'where' => "$temp_member_table.temp_id>0 ", 
            'group_by' => "$temp_member_table.temp_id"
            );        

        
        
        // ADD AUTOMATION MEMBER PRE-FILTERS
        // Filter by an array of member_status_name's to identify the status of the member's automation 
        if (isset($query_options->filter_by_member_status_name)) {

            // Convert input to array
            $member_status_name_array = Utilities::Array_Force($query_options->filter_by_member_status_name) ; 

            if (count($member_status_name_array) > 0) {
                $filter_by_member_status_name_string = '' ; 
                foreach ($member_status_name_array as $member_status_name) {

                    $filter_by_member_status_name_string .= "$temp_member_table.member_status_name='$member_status_name' OR " ;

                    }
                $filter_by_member_status_name_string = rtrim($filter_by_member_status_name_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_member_status_name_string) " ;                
                }                 
            }
        
        
        // Filter by an array of member_status_name's to identify the status of the member's automation 
        if (isset($query_options->filter_by_campaign_position)) {

            // Convert input to array
            $campaign_position_array = Utilities::Array_Force($query_options->filter_by_campaign_position) ; 

            if (count($campaign_position_array) > 0) {
                $filter_by_campaign_position_string = '' ; 
                foreach ($campaign_position_array as $campaign_position) {

                    $filter_by_campaign_position_string .= "$temp_member_table.campaign_position='$campaign_position' OR " ;

                    }
                $filter_by_campaign_position_string = rtrim($filter_by_campaign_position_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_campaign_position_string) " ;                
                }                 
            } 
        

        
        
        if ($query_options->filter_by_type_id == 'yes') { 
            switch ($query_options->type_id) {
//                case 16: // Event - Ties to Contacts
                case 19: // Subscriber List - Ties to Contacts 
                case 26: // Subscriber Series List - Ties to Contacts                     

                    if ($query_options->minify_list == 'yes') { 
                        $query_array['fields'] = "$temp_member_table.temp_id, 
                            $temp_member_table.member_id,  
                            $temp_member_table.member_id_value, 
                            $temp_member_table.metadata_id, 
                            $temp_member_table.contact_id, 
                            $temp_member_table.visibility_id, 
                            $temp_member_table.email_address, 
                            $temp_member_table.phone_number, 
                            $temp_member_table.full_name, 
                            $temp_member_table.asset_id, 
                            $temp_member_table.email_metadata_id,
                            $temp_member_table.email_metadata_name,
                            $temp_member_table.sms_metadata_id,
                            $temp_member_table.sms_metadata_name, " ; 
                        }
                    
                    
                    // Submitted as an array of visibility_names's which is converted into, and added to the visibility_name array
                    if (isset($query_options->filter_by_visibility_name)) {

                        $visibility_name_array = Utilities::Array_Force($query_options->filter_by_visibility_name) ; 

                        $query_visibility_array = array(
                            'table' => 'system_visibility_levels',
                            'fields' => "*",
                            'where' => ""
                            ) ;

                        foreach ($visibility_name_array as $visibility_name) {
                            switch ($visibility_name) {
                                case 'admin_all': // We should provide special treatment for hiddden since it's admin only...
                                    $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_hidden' OR " ;  
                                case 'all':
                                    $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_deleted' OR " ; 
                                case 'visible':    
                                    $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_draft' OR " ; 
                                    $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_shared' OR " ; 
                                    $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_published' OR " ; 
                                    $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_review' OR " ; 
                                    break ; 
                                default:
                                    $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='$visibility_name' OR " ; 
                                }
                            }
                        $query_visibility_array['where'] = rtrim($query_visibility_array['where']," OR ") ;
                        $visibility_result = $this->DB->Query('SELECT',$query_visibility_array,'force');   

                        foreach ($visibility_result['results'] as $visibility) {
                            $visibility_array[] = $visibility ; 
                            }
                        }



                    if (isset($query_options->filter_by_email_metadata_id)) {
                        
                        $email_sublist_filter_by_string = '' ;
                        if (isset($query_options->filter_by_email_metadata_id)) {
                            $metadata_id_value_array = Utilities::Process_Comma_Separated_String($query_options->filter_by_email_metadata_id) ;
                            } 
                        
                        if (count($metadata_id_value_array) > 0) {
                            $email_sublist_filter_by_string = ' AND (' ; 

                            foreach ($metadata_id_value_array as $metadata_id) {
                                $email_sublist_filter_by_string .= "$temp_member_table.email_metadata_id='$metadata_id' OR " ; 
                                }
                            $email_sublist_filter_by_string = rtrim($email_sublist_filter_by_string," OR ") ; 
                            $email_sublist_filter_by_string .= ")" ; 

                            $query_array['where'] .= "$email_sublist_filter_by_string" ;        
                            }
                        }
                         
                    if (isset($query_options->filter_by_sms_metadata_id)) {

                        $sms_sublist_filter_by_string = '' ;
                        if (isset($query_options->filter_by_sms_metadata_id)) {
                            $metadata_id_value_array = Utilities::Process_Comma_Separated_String($query_options->filter_by_sms_metadata_id) ;
                            } 
                        
                        if (count($metadata_id_value_array) > 0) {
                            $sms_sublist_filter_by_string = ' AND (' ; 

                            foreach ($metadata_id_value_array as $metadata_id) {
                                $sms_sublist_filter_by_string .= "$temp_member_table.sms_metadata_id='$metadata_id' OR " ; 
                                }
                            $sms_sublist_filter_by_string = rtrim($sms_sublist_filter_by_string," OR ") ; 
                            $sms_sublist_filter_by_string .= ")" ; 

                            $query_array['where'] .= "$sms_sublist_filter_by_string" ;        
                            }
                        }
                    
                    
                    
                    // Add visibility filter if there are visibility_id's to process
                    $filter_by_visibility_id_string = '' ; 
                    $filter_by_published_visibility_id_string = '' ; 
                    if (count($visibility_array) > 0) {

                        foreach ($visibility_array as $visibility) {

                            $this_visibility_id = $visibility['visibility_id'] ; 
                            $filter_by_visibility_id_string .= "$temp_member_table.visibility_id='$this_visibility_id' OR " ;

                            switch ($visibility['visibility_name']) {
                                case 'visibility_shared':
                                case 'visibility_published':
                                    $filter_by_published_visibility_id_string .= "$temp_member_table.visibility_id='$this_visibility_id' OR " ;        
                                    break ; 
                                }


                            }
                        $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," OR ") ; 
                        $filter_by_visibility_id_string = " AND ($filter_by_visibility_id_string) " ;

                        $query_array['where'] .= " $filter_by_visibility_id_string " ; 
                        } 
                    

                    // This needs to be submitted as an array of TAG metadata_id values
                    // This query will successfully operate AND / OR clauses based on $query_options->tag_search_operator
                    // Uses mysql query outlined here: https://stackoverflow.com/questions/24519215/mysql-match-all-tags-rather-than-any

                    // Filter by tags
                    if (isset($query_options->filter_by_tag_metadata_id)) {

                        $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_tag_metadata_id,","),",") ;
                        $query_options->filter_by_tag_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;
                        $count_metadata = count($query_options->filter_by_tag_metadata_id) ; 

                        $join_table_name = 'tag_metadata_query' ;  

                        // Create the operator
                        switch ($query_options->tag_metadata_search_operator) {
                            case 'AND': 
                                $join_type = 'join' ; 
                                $metadata_search_operator = "= $count_metadata" ;
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                                break ; 
                            case 'NOT IN':

                                $join_type = 'left' ; 
                                $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                                break ;
                            case 'both':

                                $join_type = 'left' ; 

                                break ;                    
                            case 'OR':
                            default: 
                                $join_type = 'join' ; 
                                $metadata_search_operator = ">= 1" ;      
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            }            


                        $query_array['join_tables'][] = array(
                            'table' => "
                                (SELECT nonasset_metadata_relationships.*, 
                                metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                                FROM nonasset_metadata_relationships 
                                JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                                JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                                WHERE 
                                    nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                                    AND nonasset_metadata_relationships.relationship_type='contact' 
                                    GROUP BY nonasset_metadata_relationships.item_id    
                                    $metadata_having_string)
                                    AS $join_table_name",
                            'type' => $join_type,
                            'statement' => "
                                    ($temp_member_table.contact_id=$join_table_name.item_id)"
                            );                         
                        }                    
                    

                    // Filter by categories
                    if (isset($query_options->filter_by_category_metadata_id)) {

                        $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_category_metadata_id,","),",") ;
                        $query_options->filter_by_category_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
                        $count_metadata = count($query_options->filter_by_category_metadata_id) ; 

                        $join_table_name = 'category_metadata_query' ;  

                        // Create the operator
                        switch ($query_options->category_metadata_search_operator) {
                            case 'AND': 
                                $join_type = 'join' ; 
                                $metadata_search_operator = "= $count_metadata" ;
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                                break ; 
                            case 'NOT IN':

                                $join_type = 'left' ; 
                                $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                                break ;
                            case 'both':

                                $join_type = 'left' ; 

                                break ;                    
                            case 'OR':
                            default: 
                                $join_type = 'join' ; 
                                $metadata_search_operator = ">= 1" ;      
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            }            

                        $query_array['join_tables'][] = array(
                            'table' => "
                                (SELECT nonasset_metadata_relationships.*, 
                                metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                                FROM nonasset_metadata_relationships 
                                JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                                JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                                WHERE 
                                    nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                                    AND nonasset_metadata_relationships.relationship_type='contact' 
                                    GROUP BY nonasset_metadata_relationships.item_id    
                                    $metadata_having_string)
                                    AS $join_table_name",
                            'type' => $join_type,
                            'statement' => "
                                    ($temp_member_table.contact_id=$join_table_name.item_id)"
                            );                         
                        }

                    
                    
                    // Filter by contact status
                    if (isset($query_options->filter_by_contact_status_metadata_id)) {

                        $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_contact_status_metadata_id,","),",") ;
                        $query_options->filter_by_contact_status_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_contact_status_metadata_id) ;
                        $count_metadata = count($query_options->filter_by_contact_status_metadata_id) ; 

                        $join_table_name = 'contact_status_metadata_query' ;

                        // Create the operator
                        switch ($query_options->tag_metadata_search_operator) {
                            case 'AND': 
                                $join_type = 'join' ; 
                                $metadata_search_operator = "= $count_metadata" ;
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                                break ; 
                            case 'NOT IN':

                                $join_type = 'left' ; 
                                $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                                break ;
                            case 'both':

                                $join_type = 'left' ; 

                                break ;                    
                            case 'OR':
                            default: 
                                $join_type = 'join' ; 
                                $metadata_search_operator = ">= 1" ;      
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            }            


                        $query_array['where'] .= "AND $temp_member_table.contact_status_metadata_id IN ($metadata_comma_query_string) " ; 
                        }
                    
                    
                    
                    // PROCESS CONTACT STARRED FILTER
                    if (isset($query_options->filter_by_starred_status)) {

                        $starred_item_metadata_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'starred_favorite')['value'] ; 
                        $count_metadata = 1 ; 


                        $join_table_name = 'starred_item_query' ;  
                        $query_array['fields'] .= "$join_table_name.relationship_id AS starred_item, " ; 

                        // Create the operator
                        switch ($query_options->filter_by_starred_status) {
                            case 'AND': 
                                $join_type = 'join' ; 
                                $metadata_search_operator = "= $count_metadata" ;
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                                break ; 
                            case 'live':
                            case 'unstarred':    
                            case 'NOT IN':

                                $join_type = 'left' ; 
                                $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                                break ;
                            case 'both':

                                $join_type = 'left' ; 

                                break ;                    
                            case 'OR':
                            case 'starred':    
                            default: 
                                $join_type = 'join' ; 
                                $metadata_search_operator = ">= 1" ;      
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            }            


                        
                        $query_array['join_tables'][] = array(
                            'table' => "
                                (SELECT nonasset_metadata_relationships.*, 
                                metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                                FROM nonasset_metadata_relationships 
                                JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                                JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                                WHERE 
                                    nonasset_metadata_relationships.metadata_id IN ($starred_item_metadata_id) 
                                    GROUP BY nonasset_metadata_relationships.item_id    
                                    $metadata_having_string)
                                    AS $join_table_name",
                            'type' => $join_type,
                            'statement' => "
                                    ($temp_member_table.contact_id=$join_table_name.item_id)"
                            ); 

                        }                    
                    
                    

                    // PROCESS CONTACT RATING FILTER
                    if ($query_options->rating_filter_status == 'active') {

                        switch ($query_options->rating_filter_type) {
                            case 'not_rated':
                                $query_array['where'] .= "AND ($temp_member_table.contact_rating_metadata_name IS NULL) " ; 
                                break ;                     
                            case 'range_between':
                                $query_array['where'] .= "AND ($temp_member_table.contact_rating_metadata_name>=$query_options->rating_filter_range_between_begin AND $temp_member_table.contact_rating_metadata_name<=$query_options->rating_filter_range_between_end) " ; 
                                break ; 
                            }            
                        }

                    
                    
                    
                    // PROCESS CONTACT BIRTHDAY FILTER
                    if ($query_options->birthday_filter_status == 'active') {

                        $today_midnight = new DateTime() ; 
                        $today_midnight->setTimezone(new DateTimeZone($timezone)) ;  
                        $today_midnight_timestamp = $today_midnight->setTimestamp(TIMESTAMP)->setTime( 0, 0, 0 )->getTimestamp() ;


                        switch ($query_options->birthday_filter_type) {
                            case 'range_next':

                                $range_next = Utilities::System_Time_Manipulate($query_options->birthday_filter_range_next_days.' d','+',$today_midnight_timestamp) ; 
                                $range_next_timestamp = $range_next['time_manipulated'] ;
                                $query_array['where'] .= "AND ($temp_member_table.timestamp_birthday_next>$today_midnight_timestamp AND $temp_member_table.timestamp_birthday_next<$range_next_timestamp) " ; 

                                break ; 
                            case 'range_last':

                                $range_last = Utilities::System_Time_Manipulate($query_options->birthday_filter_range_last_days.' d','-',$today_midnight_timestamp) ; 
                                $range_last_timestamp = $range_last['time_manipulated'] ;
                                $query_array['where'] .= "AND ($temp_member_table.timestamp_birthday_last>$range_last_timestamp AND $temp_member_table.timestamp_birthday_last<$today_midnight_timestamp) " ; 

                                break ;                    
                            case 'range_between':

                                $range_between_begin = new DateTime($query_options->birthday_filter_range_between_begin) ; 
                                $range_between_begin->setTimezone(new DateTimeZone($timezone)) ;  
                                $range_between_begin_timestamp = $range_between_begin->getTimestamp() ;

                                $range_between_end = new DateTime($query_options->birthday_filter_range_between_end) ; 
                                $range_between_end->setTimezone(new DateTimeZone($timezone)) ;  
                                $range_between_end_timestamp = $range_between_end->getTimestamp() ;

                                $query_array['where'] .= "AND (($temp_member_table.timestamp_birthday_last>$range_between_begin_timestamp AND $temp_member_table.timestamp_birthday_last<$range_between_end_timestamp) OR ($temp_member_table.timestamp_birthday_next>$range_between_begin_timestamp AND $temp_member_table.timestamp_birthday_next<$range_between_end_timestamp)) " ; 

                                break ;                    
                            case 'range_next_expired':

                                $range_next = Utilities::System_Time_Manipulate($query_options->birthday_filter_range_next_days.' d','-',$today_midnight_timestamp) ; 
                                $range_next_timestamp = $range_next['time_manipulated'] ;
                                $query_array['where'] .= "AND ($temp_member_table.timestamp_birthday_next<$today_midnight_timestamp AND $temp_member_table.timestamp_birthday_next>$range_next_timestamp) " ; 

                                break ; 
                            }            
                        }                    
                    
                    
                    
                    
                    // PROCESS CONTACT CREATED FILTER
                    if ($query_options->contact_created_filter_status == 'active') {

                        $today_midnight = new DateTime() ; 
                        $today_midnight->setTimezone(new DateTimeZone($timezone)) ;  
                        $today_midnight_timestamp = $today_midnight->setTimestamp(TIMESTAMP)->setTime( 23, 59, 59 )->getTimestamp() ;


                        switch ($query_options->contact_created_filter_type) {
                            case 'range_next':

                                $range_next = Utilities::System_Time_Manipulate($query_options->contact_created_filter_range_next_days.' d','+',$today_midnight_timestamp) ; 
                                $range_next_timestamp = $range_next['time_manipulated'] ;
                                $query_array['where'] .= "AND ($temp_member_table.timestamp_contact_created>$today_midnight_timestamp AND $temp_member_table.timestamp_contact_created<$range_next_timestamp) " ; 

                                break ; 
                            case 'range_last':

                                $range_last = Utilities::System_Time_Manipulate($query_options->contact_created_filter_range_last_days.' d','-',$today_midnight_timestamp) ; 
                                $range_last_timestamp = $range_last['time_manipulated'] ;
                                $query_array['where'] .= "AND ($temp_member_table.timestamp_contact_created>$range_last_timestamp AND $temp_member_table.timestamp_contact_created<$today_midnight_timestamp) " ; 

                                break ;                    
                            case 'range_between':

                                $range_between_begin = new DateTime($query_options->contact_created_filter_range_between_begin) ; 
                                $range_between_begin->setTimezone(new DateTimeZone($timezone)) ;  
                                $range_between_begin_timestamp = $range_between_begin->getTimestamp() ;

                                $range_between_end = new DateTime($query_options->contact_created_filter_range_between_end) ; 
                                $range_between_end->setTimezone(new DateTimeZone($timezone)) ;  
                                $range_between_end_timestamp = $range_between_end->getTimestamp() ;

                                $query_array['where'] .= "AND ($temp_member_table.timestamp_contact_created>$range_between_begin_timestamp AND $temp_member_table.timestamp_contact_created<$range_between_end_timestamp) " ; 

                                break ;                    

                            }            
                        }



                    // ADD CONTACT SOURCE FILTER
                    if ($query_options->filter_by_contact_source) {            

                        $query_options->filter_by_contact_source = Utilities::Process_Comma_Separated_String($query_options->filter_by_contact_source) ;

                        $contact_source_search_string = '' ; 

                        foreach ($query_options->filter_by_contact_source as $contact_source) { 
                            $contact_source_search_string .= "$temp_member_table.created_source LIKE '$contact_source' OR " ; 
                            }

                        $contact_source_search_string = rtrim($contact_source_search_string," OR ") ;     
                        $query_array['where'] .= " AND ($contact_source_search_string) " ; 
                        }
                    
                    
        
                    // ADD CONTACT STATE FILTER
                    if ($query_options->filter_by_contact_state_id) {              

                        $query_options->filter_by_contact_state_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_contact_state_id) ;

                        $contact_state_search_string = '' ; 

                        foreach ($query_options->filter_by_contact_state_id as $contact_state) {
                            $contact_state_search_string .= "$temp_member_table.state LIKE '$contact_state' OR " ; 
                            }

                        $contact_state_search_string = rtrim($contact_state_search_string," OR ") ;     
                        $query_array['where'] .= " AND ($contact_state_search_string) " ; 
                        }        


                    // ADD CONTACT COUNTRY FILTER
                    if ($query_options->filter_by_contact_country_id) {              

                        $query_options->filter_by_contact_country_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_contact_country_id) ;

                        $contact_country_search_string = '' ; 

                        foreach ($query_options->filter_by_contact_country_id as $contact_country) {
                            $contact_country_search_string .= "$temp_member_table.country_id LIKE '$contact_country' OR " ; 
                            }

                        $contact_country_search_string = rtrim($contact_country_search_string," OR ") ;     
                        $query_array['where'] .= " AND ($contact_country_search_string) " ; 
                        }
                    
                    
                    

                    // ADD YL INFO FILTERS
                    
                    // PROCESS CONTACT YL ACTIVATION DATE FILTER
                    if ($query_options->contact_yl_activation_filter_status == 'active') {

                        $today_midnight = new DateTime() ; 
                        $today_midnight->setTimezone(new DateTimeZone($timezone)) ;  
                        $today_midnight_timestamp = $today_midnight->setTimestamp(TIMESTAMP)->setTime( 23, 59, 59 )->getTimestamp() ;


                        switch ($query_options->contact_yl_activation_filter_type) { 
                            case 'range_next':

                                $range_next = Utilities::System_Time_Manipulate($query_options->contact_yl_activation_filter_range_next_days.' d','+',$today_midnight_timestamp) ; 
                                $range_next_timestamp = $range_next['time_manipulated'] ;
                                $query_array['where'] .= "AND ($temp_member_table.timestamp_yl_activation_date>$today_midnight_timestamp AND $temp_member_table.timestamp_yl_activation_date<$range_next_timestamp) " ; 

                                break ; 
                            case 'range_last':

                                $range_last = Utilities::System_Time_Manipulate($query_options->contact_yl_activation_filter_range_last_days.' d','-',$today_midnight_timestamp) ; 
                                $range_last_timestamp = $range_last['time_manipulated'] ;
                                $query_array['where'] .= "AND ($temp_member_table.timestamp_yl_activation_date>$range_last_timestamp AND $temp_member_table.timestamp_yl_activation_date<$today_midnight_timestamp) " ; 

                                break ;                    
                            case 'range_between':

                                $range_between_begin = new DateTime($query_options->contact_yl_activation_filter_range_between_begin) ; 
                                $range_between_begin->setTimezone(new DateTimeZone($timezone)) ;  
                                $range_between_begin_timestamp = $range_between_begin->getTimestamp() ;

                                $range_between_end = new DateTime($query_options->contact_yl_activation_filter_range_between_end) ; 
                                $range_between_end->setTimezone(new DateTimeZone($timezone)) ;  
                                $range_between_end_timestamp = $range_between_end->getTimestamp() ;

                                $query_array['where'] .= "AND ($temp_member_table.timestamp_yl_activation_date>$range_between_begin_timestamp AND $temp_member_table.timestamp_yl_activation_date<$range_between_end_timestamp) " ; 

                                break ;                    

                            }            
                        }
                    
                    
                    // FILTER BY YL ACCOUNT STATUS
                    if (isset($query_options->filter_by_yl_account_status_metadata_id)) { 

                        $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_account_status_metadata_id,","),",") ;
                        $query_options->filter_by_yl_account_status_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_account_status_metadata_id) ;
                        $count_metadata = count($query_options->filter_by_yl_account_status_metadata_id) ; 

                        $join_table_name = 'yl_account_status_metadata_query' ;  

                        // Create the operator
                        switch ($query_options->tag_metadata_search_operator) { 
                            case 'AND': 
                                $join_type = 'join' ; 
                                $metadata_search_operator = "= $count_metadata" ;
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                                break ; 
                            case 'NOT IN':

                                $join_type = 'left' ; 
                                $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                                break ;
                            case 'both':

                                $join_type = 'left' ; 

                                break ;                    
                            case 'OR':
                            default: 
                                $join_type = 'join' ; 
                                $metadata_search_operator = ">= 1" ;      
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            }            


                        $query_array['where'] .= "AND $temp_member_table.yl_account_status_metadata_id IN ($metadata_comma_query_string) " ;                          
                        }        


                    // FILTER BY YL CURRENT RANK
                    if (isset($query_options->filter_by_yl_current_rank_metadata_id)) {

                        $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_current_rank_metadata_id,","),",") ;
                        $query_options->filter_by_yl_current_rank_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_current_rank_metadata_id) ;
                        $count_metadata = count($query_options->filter_by_yl_current_rank_metadata_id) ; 

                        $filter_by_yl_current_rank_string = '' ; 
                        if (count($query_options->filter_by_yl_current_rank_metadata_id) > 0) {

                            foreach ($query_options->filter_by_yl_current_rank_metadata_id as $rank) {

                                $filter_by_yl_current_rank_string .= "$temp_member_table.rank_current='$rank' OR " ;
                                }
                            $filter_by_yl_current_rank_string = rtrim($filter_by_yl_current_rank_string," OR ") ; 
                            $filter_by_yl_current_rank_string = " AND ($filter_by_yl_current_rank_string) " ;

                            $query_array['where'] .= " $filter_by_yl_current_rank_string " ; 
                            }            
                        }


                    // FILTER BY YL HIGHEST RANK
                    if (isset($query_options->filter_by_yl_highest_rank_metadata_id)) {

                        $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_highest_rank_metadata_id,","),",") ;
                        $query_options->filter_by_yl_highest_rank_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_highest_rank_metadata_id) ;
                        $count_metadata = count($query_options->filter_by_yl_highest_rank_metadata_id) ; 

                        $filter_by_yl_highest_rank_string = '' ; 
                        if (count($query_options->filter_by_yl_highest_rank_metadata_id) > 0) {

                            foreach ($query_options->filter_by_yl_highest_rank_metadata_id as $rank) {

                                $filter_by_yl_highest_rank_string .= "$temp_member_table.rank_highest='$rank' OR " ;
                                }
                            $filter_by_yl_highest_rank_string = rtrim($filter_by_yl_highest_rank_string," OR ") ; 
                            $filter_by_yl_highest_rank_string = " AND ($filter_by_yl_highest_rank_string) " ;

                            $query_array['where'] .= " $filter_by_yl_highest_rank_string " ; 
                            }            
                        }
                    
                    
                    
                    if (isset($query_options->filter_by_yl_enroller_member_number)) {

                        $enroller_number_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_enroller_member_number,","),",") ;
                        $query_options->filter_by_yl_enroller_member_number = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_enroller_member_number) ;
                        $count_metadata = count($query_options->filter_by_yl_enroller_member_number) ; 

                        $filter_by_yl_enroller_member_number_string = '' ; 
                        if (count($query_options->filter_by_yl_enroller_member_number) > 0) {

                            foreach ($query_options->filter_by_yl_enroller_member_number as $enroller) {

                                $filter_by_yl_enroller_member_number_string .= "$temp_member_table.yl_enroller_member_number='$enroller' OR " ;
                                }
                            $filter_by_yl_enroller_member_number_string = rtrim($filter_by_yl_enroller_member_number_string," OR ") ; 
                            $filter_by_yl_enroller_member_number_string = " AND ($filter_by_yl_enroller_member_number_string) " ;

                            $query_array['where'] .= " $filter_by_yl_enroller_member_number_string " ; 
                            }            
                        }

                    if (isset($query_options->filter_by_yl_sponsor_member_number)) {

                        $sponsor_number_comma_query_string = ltrim(rtrim($query_options->filter_by_yl_sponsor_member_number,","),",") ;
                        $query_options->filter_by_yl_sponsor_member_number = Utilities::Process_Comma_Separated_String($query_options->filter_by_yl_sponsor_member_number) ;
                        $count_metadata = count($query_options->filter_by_yl_sponsor_member_number) ; 

                        $filter_by_yl_sponsor_member_number_string = '' ; 
                        if (count($query_options->filter_by_yl_sponsor_member_number) > 0) {

                            foreach ($query_options->filter_by_yl_sponsor_member_number as $sponsor) {

                                $filter_by_yl_sponsor_member_number_string .= "$temp_member_table.yl_sponsor_member_number='$sponsor' OR " ;
                                }
                            $filter_by_yl_sponsor_member_number_string = rtrim($filter_by_yl_sponsor_member_number_string," OR ") ; 
                            $filter_by_yl_sponsor_member_number_string = " AND ($filter_by_yl_sponsor_member_number_string) " ;

                            $query_array['where'] .= " $filter_by_yl_sponsor_member_number_string " ; 
                            }            
                        }                     
                    
                    
                    // *** END: YL INFO FILTERS ***
                    
                

                    // Add filter by search parameter
                    if (isset($query_options->filter_by_search_parameter)) {

                        $search_parameter_token = explode(" ",$query_options->filter_by_search_parameter) ; 

                        $t = 0 ; 
                        foreach ($search_parameter_token as $search_token) {
                            if ($search_token == '') {
                                unset($search_parameter_token[$t]) ; 
                                }
                            $t++ ; 
                            }
                        array_values($search_parameter_token) ; 
                        
                        foreach ($search_parameter_token as $search_token) {
                            $phone_number = Utilities::Validate_Phone_Number($search_token) ;
                            if ($phone_number['valid'] == 'yes') {
                                $search_parameter_token[] = $phone_number['phone_number'] ;    
                                }
                            }

                            switch($query_options->search_parameter_refine_contact_notes) { 
                                case 'yes':
                                    $query_array['join_tables'][] = array(
                                        'table' => 'contact_notes',
                                        'on' => 'contact_notes.contact_id',
                                        'match' => $temp_member_table.'.contact_id',
                                        'type' => 'left'
                                        );

                                    break ;    
                                default:

                                }
                        
                        $compiled_search_string = '' ; 

                        foreach ($search_parameter_token as $search_token) {

                            $this_search_string = " ($temp_member_table.full_name LIKE '%$search_token%' OR 
                                $temp_member_table.email_address LIKE '%$search_token%' OR 
                                $temp_member_table.phone_number LIKE '%$search_token%' " ;

                            switch($query_options->search_parameter_refine_contact_notes) { 
                                case 'yes':
                                    $this_search_string .= " OR contact_notes.note_title LIKE '%$search_token%' OR contact_notes.note_content LIKE '%$search_token%' " ; 
                                    break ;    
                                default:
                                    // blank
                                }

                            switch($query_options->search_parameter_refine_contact_description) { 
                                case 'yes':
                                    $this_search_string .= " OR $temp_member_table.contact_description LIKE '%$search_token%' " ; 
                                    break ;    
                                default:
                                    // blank
                                }                            
                            
                            $this_search_string .= ')' ; 
                            $compiled_search_string .= $this_search_string.' OR ' ;                             
                            }

                        $compiled_search_string = rtrim($compiled_search_string," OR ") ;  
                        $query_array['where'] .= " AND ($compiled_search_string) " ;                                    
                        }
                    
                    
                    
                    // Add filter by search parameter
//                    if ($query_options->filter_by_search_parameter) {            
//
//                        $query_array['where'] .= "AND ($temp_member_table.full_name LIKE '%$query_options->filter_by_search_parameter%' OR 
//                            $temp_member_table.email_address LIKE '%$query_options->filter_by_search_parameter%' OR 
//                            $temp_member_table.phone_number LIKE '%$query_options->filter_by_search_parameter%'" ; 
//
//                        switch($query_options->search_parameter_refine_contact_notes) { 
//                            case 'yes':
//                                $query_array['join_tables'][] = array(
//                                    'table' => 'contact_notes',
//                                    'on' => 'contact_notes.contact_id',
//                                    'match' => $temp_member_table.'.contact_id',
//                                    'type' => 'left'
//                                    );
//
//                                $query_array['where'] .= " OR contact_notes.note_title LIKE '%$query_options->filter_by_search_parameter%' OR contact_notes.note_content LIKE '%$query_options->filter_by_search_parameter%' " ; 
//                                break ;    
//                            default:
//
//                            }
//
//                        switch($query_options->search_parameter_refine_contact_description) { 
//                            case 'yes':
//                                $query_array['where'] .= " OR $temp_member_table.contact_description LIKE '%$query_options->filter_by_search_parameter%' " ; 
//                                break ;    
//                            default:
//
//                            }
//
//                        $query_array['where'] .= ") " ; 
//                        }


                                   
                    

                    // Add sort filter
                    if (isset($query_options->order_by)) {
                        $query_array['order'] = $query_options->order ;            
                        $query_array['order_by'] = $query_options->order_by ; 

                        if ($query_array['order_by'] == 'relevance_score') {
                            $query_array['order_by'] = $temp_member_table.'.last_name' ; 
                            $query_array['order_by_relevance'] = 'yes' ; 
                            }  
                        }                        
                        
                break ;
            case 20: // User Subscription list Filters        
            case 28: // User SERIES list

                if ($query_options->minify_list == 'yes') { 
                    $query_array['fields'] = "$temp_member_table.temp_id, 
                        $temp_member_table.member_id,  
                        $temp_member_table.member_id_value, 
                        $temp_member_table.metadata_id, 
                        $temp_member_table.user_id, 
                        $temp_member_table.email_address, 
                        $temp_member_table.phone_number, 
                        $temp_member_table.full_name, 
                        $temp_member_table.asset_id, 
                        $temp_member_table.email_metadata_id,
                        $temp_member_table.email_metadata_name,
                        $temp_member_table.sms_metadata_id,
                        $temp_member_table.sms_metadata_name, " ; 
                    }
                    
                if ($query_options->filter_by_search_parameter) {            

                    $query_array['where'] .= " AND ($temp_member_table.first_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        $temp_member_table.last_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        $temp_member_table.full_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                        $temp_member_table.phone_number LIKE '%$query_options->filter_by_search_parameter%' OR 
                        $temp_member_table.email_address LIKE '%$query_options->filter_by_search_parameter%'" ;                        

                    $query_array['where'] .= ") " ; 
                    }
                    
                    
                // USER AUTHORIZATION FILTER
                // Add USER authorization level filter: e.g. Only get active user, pending user ... etc.
                // Submit as an array of auth_role_name strings
                if (count($query_options->user_auth_role_name_array) > 0) {   
                    $user_auth_role_name_query_string = '' ; 
                    foreach ($query_options->user_auth_role_name_array as $user_auth_name) {
                        $user_auth_role_name_query_string .= "$temp_member_table.user_auth_role_name='$user_auth_name' OR" ; 
                        }
                    $user_auth_role_name_query_string = rtrim($user_auth_role_name_query_string," OR ") ;
                    $query_array['where'] .= "AND ($user_auth_role_name_query_string)" ;
                    }

                // Add USER authorization level filter: Submit as comma separated auth_role_id string
                if (isset($query_options->filter_by_user_auth_id)) {

                    $query_options->filter_by_user_auth_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_user_auth_id) ;

                    foreach ($query_options->filter_by_user_auth_id as $user_auth_id) {
                        $user_auth_id_query_string .= "$temp_member_table.user_auth_id='$user_auth_id' OR " ; 
                        }

                    $user_auth_id_query_string = rtrim($user_auth_id_query_string," OR ") ;
                    $user_auth_id_query_string = rtrim($user_auth_id_query_string," AND ") ;
                    $query_array['where'] .= 'AND ('.$user_auth_id_query_string.') ' ;            
                    }


                    
            
                    
                // Filter by tags
                if (isset($query_options->filter_by_tag_metadata_id)) {

                    $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_tag_metadata_id,","),",") ;
                    $query_options->filter_by_tag_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;
                    $count_metadata = count($query_options->filter_by_tag_metadata_id) ; 

                    $join_table_name = 'tag_metadata_query' ;  

                    // Create the operator
                    switch ($query_options->tag_metadata_search_operator) {
                        case 'AND': 
                            $join_type = 'join' ; 
                            $metadata_search_operator = "= $count_metadata" ;
                            $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            break ; 
                        case 'NOT IN':

                            $join_type = 'left' ; 
                            $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                            break ;
                        case 'both':

                            $join_type = 'left' ; 

                            break ;                    
                        case 'OR':
                        default: 
                            $join_type = 'join' ; 
                            $metadata_search_operator = ">= 1" ;      
                            $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                        }            



                    $query_array['join_tables'][] = array(
                        'table' => "
                            (SELECT nonasset_metadata_relationships.*, 
                            metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                            FROM nonasset_metadata_relationships 
                            JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                            JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                            WHERE 
                                nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                                AND nonasset_metadata_relationships.relationship_type='user' 
                                GROUP BY nonasset_metadata_relationships.item_id    
                                $metadata_having_string)
                                AS $join_table_name",
                        'type' => $join_type,
                        'statement' => "
                                ($temp_member_table.user_id=$join_table_name.item_id)"
                        );                         
                    }                    


                // Filter by categories
                if (isset($query_options->filter_by_category_metadata_id)) {

                    $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_category_metadata_id,","),",") ;
                    $query_options->filter_by_category_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
                    $count_metadata = count($query_options->filter_by_category_metadata_id) ; 

                    $join_table_name = 'category_metadata_query' ;  

                    // Create the operator
                    switch ($query_options->category_metadata_search_operator) {
                        case 'AND': 
                            $join_type = 'join' ; 
                            $metadata_search_operator = "= $count_metadata" ;
                            $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            break ; 
                        case 'NOT IN':

                            $join_type = 'left' ; 
                            $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                            break ;
                        case 'both':

                            $join_type = 'left' ; 

                            break ;                    
                        case 'OR':
                        default: 
                            $join_type = 'join' ; 
                            $metadata_search_operator = ">= 1" ;      
                            $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                        }            



                    $query_array['join_tables'][] = array(
                        'table' => "
                            (SELECT nonasset_metadata_relationships.*, 
                            metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                            FROM nonasset_metadata_relationships 
                            JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                            JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                            WHERE 
                                nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                                AND nonasset_metadata_relationships.relationship_type='user' 
                                GROUP BY nonasset_metadata_relationships.item_id    
                                $metadata_having_string)
                                AS $join_table_name",
                        'type' => $join_type,
                        'statement' => "
                                ($temp_member_table.user_id=$join_table_name.item_id)"
                        );                         
                    }
                    
                    
                break ;
            case 23: // User Profile Subscription List filters
            case 27: // Profile SERIES list    
                    
                    if ($query_options->minify_list == 'yes') { 
                        $query_array['fields'] = "$temp_member_table.temp_id, 
                            $temp_member_table.member_id,  
                            $temp_member_table.member_id_value, 
                            $temp_member_table.metadata_id, 
                            $temp_member_table.user_id, 
                            $temp_member_table.profile_id,  
                            $temp_member_table.email_address, 
                            $temp_member_table.phone_number, 
                            $temp_member_table.full_name, 
                            $temp_member_table.asset_id, 
                            $temp_member_table.email_metadata_id,
                            $temp_member_table.email_metadata_name,
                            $temp_member_table.sms_metadata_id,
                            $temp_member_table.sms_metadata_name, " ; 
                        }

                    // USER AUTHORIZATION FILTER
                    // Add USER authorization level filter: e.g. Only get active user, pending user ... etc.
                    // Submit as an array of auth_role_name strings
                    if (count($query_options->user_auth_role_name_array) > 0) {   
                        $user_auth_role_name_query_string = '' ; 
                        foreach ($query_options->user_auth_role_name_array as $user_auth_name) {
                            $user_auth_role_name_query_string .= "$temp_member_table.user_auth_role_name='$user_auth_name' OR" ; 
                            }
                        $user_auth_role_name_query_string = rtrim($user_auth_role_name_query_string," OR ") ;
                        $query_array['where'] .= "AND ($user_auth_role_name_query_string)" ;
                        }

                    // Add USER authorization level filter: Submit as comma separated auth_role_id string
                    if (isset($query_options->filter_by_user_auth_id)) {

                        $query_options->filter_by_user_auth_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_user_auth_id) ;

                        foreach ($query_options->filter_by_user_auth_id as $user_auth_id) {
                            $user_auth_id_query_string .= "$temp_member_table.user_auth_id='$user_auth_id' OR " ; 
                            }

                        $user_auth_id_query_string = rtrim($user_auth_id_query_string," OR ") ;
                        $user_auth_id_query_string = rtrim($user_auth_id_query_string," AND ") ;
                        $query_array['where'] .= 'AND ('.$user_auth_id_query_string.') ' ;            
                        }
 

                    //  ACCOUNT AUTHORIZATION FILTER
                    //  Add ACCOUNT authorization level filter: e.g. Only get members, or admins, or owner... etc.
                    // Submit as an array of auth_role_name strings
                    if (count($query_options->account_auth_role_name_array) > 0) {   
                        $account_auth_role_name_query_string = '' ; 
                        foreach ($query_options->account_auth_role_name_array as $account_auth_name) {
                            $account_auth_role_name_query_string .= "$temp_member_table.account_auth_role_name='$account_auth_name' OR" ; 
                            }
                        $account_auth_role_name_query_string = rtrim($account_auth_role_name_query_string," OR ") ;
                        $query_array['where'] .= "AND ($account_auth_role_name_query_string)" ;
                        }

                    //  Add ACCOUNT authorization level filter: Submit as comma separated auth_role_id string
                    if (isset($query_options->filter_by_account_auth_id)) {
                        $query_options->filter_by_account_auth_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_account_auth_id) ;

                        foreach ($query_options->filter_by_account_auth_id as $account_auth_id) {
                            $account_auth_id_query_string .= "$temp_member_table.account_auth_id='$account_auth_id' OR " ; 
                            }

                        $account_auth_id_query_string = rtrim($account_auth_id_query_string," OR ") ;
                        $account_auth_id_query_string = rtrim($account_auth_id_query_string," AND ") ;
                        $query_array['where'] .= 'AND ('.$account_auth_id_query_string.') ' ;            
                        }
                    

                    if ($query_options->filter_by_search_parameter) {            

                        $query_array['where'] .= "AND (
                            $temp_member_table.first_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                            $temp_member_table.last_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                            CONCAT($temp_member_table.first_name,' ',$temp_member_table.last_name) LIKE '%$query_options->filter_by_search_parameter%' OR 
                            $temp_member_table.phone_number LIKE '%$query_options->filter_by_search_parameter%' OR 
                            $temp_member_table.email_address LIKE '%$query_options->filter_by_search_parameter%' OR 
                            $temp_member_table.profile_display_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                            $temp_member_table.profile_username LIKE '%$query_options->filter_by_search_parameter%' OR 
                            $temp_member_table.profile_role LIKE '%$query_options->filter_by_search_parameter%')" ; 

                        }
                
                    
                    // Account billing status filter
                    if (isset($query_options->filter_by_account_billing_status)) {
                        $query_options->filter_by_account_billing_status = Utilities::Process_Comma_Separated_String($query_options->filter_by_account_billing_status) ;

                        foreach ($query_options->filter_by_account_billing_status as $account_billing_status) {
                            $account_billing_status_query_string .= "$temp_member_table.billing_status='$account_billing_status' OR " ; 
                            }

                        $account_billing_status_query_string = rtrim($account_billing_status_query_string," OR ") ;
                        $account_billing_status_query_string = rtrim($account_billing_status_query_string," AND ") ;
                        $query_array['where'] .= ' AND ('.$account_billing_status_query_string.') ' ;            
                        }
                    
                    
                    
                    // Filter by tags
                    if (isset($query_options->filter_by_tag_metadata_id)) {

                        $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_tag_metadata_id,","),",") ;
                        $query_options->filter_by_tag_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;
                        $count_metadata = count($query_options->filter_by_tag_metadata_id) ; 

                        $join_table_name = 'tag_metadata_query' ;  

                        // Create the operator
                        switch ($query_options->tag_metadata_search_operator) {
                            case 'AND': 
                                $join_type = 'join' ; 
                                $metadata_search_operator = "= $count_metadata" ;
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                                break ; 
                            case 'NOT IN':

                                $join_type = 'left' ; 
                                $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                                break ;
                            case 'both':

                                $join_type = 'left' ; 

                                break ;                    
                            case 'OR':
                            default: 
                                $join_type = 'join' ; 
                                $metadata_search_operator = ">= 1" ;      
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            }            



                        $query_array['join_tables'][] = array(
                            'table' => "
                                (SELECT nonasset_metadata_relationships.*, 
                                metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                                FROM nonasset_metadata_relationships 
                                JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                                JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                                WHERE 
                                    nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                                    AND nonasset_metadata_relationships.relationship_type='profile' 
                                    GROUP BY nonasset_metadata_relationships.item_id    
                                    $metadata_having_string)
                                    AS $join_table_name",
                            'type' => $join_type,
                            'statement' => "
                                    ($temp_member_table.profile_id=$join_table_name.item_id)"
                            );                         
                        }                    
                    
                    
                    // Filter by categories
                    if (isset($query_options->filter_by_category_metadata_id)) {

                        $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_category_metadata_id,","),",") ;
                        $query_options->filter_by_category_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
                        $count_metadata = count($query_options->filter_by_category_metadata_id) ; 

                        $join_table_name = 'category_metadata_query' ;  

                        // Create the operator
                        switch ($query_options->tag_metadata_search_operator) {
                            case 'AND': 
                                $join_type = 'join' ; 
                                $metadata_search_operator = "= $count_metadata" ;
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                                break ; 
                            case 'NOT IN':

                                $join_type = 'left' ; 
                                $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 

                                break ;
                            case 'both':

                                $join_type = 'left' ; 

                                break ;                    
                            case 'OR':
                            default: 
                                $join_type = 'join' ; 
                                $metadata_search_operator = ">= 1" ;      
                                $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                            }            



                        $query_array['join_tables'][] = array(
                            'table' => "
                                (SELECT nonasset_metadata_relationships.*, 
                                metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                                FROM nonasset_metadata_relationships 
                                JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                                JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                                WHERE 
                                    nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                                    AND nonasset_metadata_relationships.relationship_type='profile' 
                                    GROUP BY nonasset_metadata_relationships.item_id    
                                    $metadata_having_string)
                                    AS $join_table_name",
                            'type' => $join_type,
                            'statement' => "
                                    ($temp_member_table.profile_id=$join_table_name.item_id)"
                            );                         
                        }
                    
                    
                break ; 
            default:
                $query_options->order_by = "$temp_member_table.temp_id" ;
                $query_options->order = 'ASC' ;                                         
                }
            } else {
                $query_options->order_by = "$temp_member_table.temp_id" ;
                $query_options->order = 'ASC' ;      
                }
        
        

        // Set order parameters
        if (isset($query_options->order_by)) {
            $query_options->order_by = str_replace("asset_member_temp_table.",$temp_member_table.".",$query_options->order_by) ;
            $query_array['order_by'] = "$query_options->order_by" ;  
            $query_array['order'] = $query_options->order ;  
            }        


        // Remove leading "AND" from master WHERE statement
        $query_array['where'] = ltrim($query_array['where'],"AND ") ;

        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
        
        $result = $this->DB->Query('SELECT',$query_array,'force');
        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_pull_filtered_temp_table') ;
        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_result_count_'.$result['result_count']) ;
        
//        $this->asset_member_query_result2 = $result ; 
        
        $query_options->value_name = 'member_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_processed_temp_table_query_results') ;
        $this->Set_Asset_Paging($result) ;         
        
        
        // Drop the temp table
        if ($use_temp_table == 1) {
            
            $drop_table_query_array = array(
                'table' => $temp_member_table
                );  

            $drop_table_result = $this->DB->Query('DROP_TABLE',$drop_table_query_array) ;    
            $this->Set_Model_Timings('Asset.Retrieve_Asset_Members_dropped_temp_table') ;
            }

        return $result ;               
        }
    
    
    

    
    
    // microtimestamp_asset_history_updated
    public function Retrieve_Asset_History_List($asset_id = 'internal',$query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ; 
            }
                
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            } else {
            
                if (!is_array($asset_id)) {
                    $this->asset_id = $asset_id ; 
                    }
                            
                }

        $query_array = array(
            'table' => 'asset_content_history',
            'fields' => "asset_content_history.history_id, 
                asset_content_history.asset_id, 
                asset_content_history.microtimestamp_asset_history_updated, 
                asset_content_history.structured_data_01, 
                asset_content_history.asset_master, 
                asset_content_history.asset_structure"
            );        

        
        if (is_array($asset_id)) {
            
            $asset_id_query_string = '' ; 
            foreach ($asset_id as $this_asset_id) {
                $asset_id_query_string .= "asset_content_history.asset_id='$this_asset_id' OR " ; 
                }
            $asset_id_query_string = rtrim($asset_id_query_string," OR ") ;
            $query_array['where'] = "(".$asset_id_query_string.") " ; 
            
            } else {
                $query_array['where'] = "asset_content_history.asset_id='$asset_id' " ; 
                }
        
        
        // Only pull the full history record IF a specific history_id has been indicated
        if (isset($query_options->history_id)) {
            $query_array['limit'] = 1 ; 
            $query_array['fields'] = 'asset_content_history.*' ;
            if ($query_options->history_id == 'latest') {
                $query_options->order = 'DESC' ; 
                $query_options->order_by = 'microtimestamp_asset_history_updated' ;
                unset($query_options->history_id) ; 
                if (is_array($asset_id)) {
                    unset($query_array['limit']) ; 
                    }
                }
            }
        
        // Add history id filter
        if (isset($query_options->history_id)) {
            $query_array['where'] .= " AND asset_content_history.history_id='$query_options->history_id' " ;  
            } 
        
        
        // Add order filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }        
        
            
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results        
        if (is_array($asset_id)) {
            
            $query_array['skip_query'] = 1 ;
            $result = $this->DB->Query('SELECT_JOIN',$query_array,'force') ;
            
            $bulk_query_array = array(
                'statement' => "SELECT * FROM (".$result['query'].") AS history_list",
                'group_by' => 'history_list.asset_id'
                ) ; 
            
            $result = $this->DB->Query('STATEMENT',$bulk_query_array,'force') ;
            
            } else {
                $result = $this->DB->Query('SELECT_JOIN',$query_array,'force') ;
                }     
        
        
        $query_options->value_name = 'history_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Asset_Paging($result) ;         
        $this->asset_query_result = $result ;

        return $result ;       
        }
    
    
    
    
    public function Retrieve_Asset_Master($asset_id = 'internal') {
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            } 

        $query_array = array(
            'table' => 'assets',
            'fields' => "assets.*",
            'where' => "assets.asset_id='$this->asset_id'"
            );        
        
        $result = $this->DB->Query('SELECT',$query_array);
        
        $this->asset_query_result = $result ;

        return $result ;       
        }
    
    
    public function Retrieve_Asset_Content($asset_id = 'internal') {
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            } 
        
        
         
        
        $query_array = array(
            'table' => 'asset_content',
            'fields' => "asset_content.*",
            'where' => "asset_content.asset_id='$this->asset_id'"
            );        
        
        $result = $this->DB->Query('SELECT',$query_array);
        
        $this->asset_query_result = $result ;

        return $result ;       
        }
    
    

    // Retrieve a list of metadata based on various query options
    // $metadata_type_name can be a single type name or a comma separated string of type names
    public function Retrieve_Asset_Metadata_List($metadata_type_name,$query_options = array()) {       
        
        // QUERY OPTIONS
        // asset_id - If set to an ID, then it will pull metadata associated w/ that asset ID. Defaults to 'none' and is skipped.
        // filter_by_account_id - 
        //      If set to an integer, then limits metadata to that account id. 
        //      internal limits to the class account_id
        //      internal_global limits to class account_id and 0 (for global assets)
        //      Default looks for internal_global
        
         
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
                
        if (!isset($query_options->asset_id)) {
            $query_options->asset_id = 'none' ; 
            } 
        
        if (!isset($query_options->filter_by_account_id)) {
            $query_options->filter_by_account_id = 'internal_global' ; 
            } 
        
        
        
        $query_array = array(
            'table' => 'metadata',
            'join_tables' => array(),
            'fields' => "metadata.*, metadata_type.*, 
                system_visibility_levels.visibility_name, system_visibility_levels.visibility_name, system_visibility_levels.visibility_title",            
            'where' => "metadata.metadata_id>0 "
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'metadata.visibility_id'
            );
        
        if ($query_options->asset_id != 'none') {
            $query_array['fields'] .= ", asset_metadata_relationships.relationship_id, asset_metadata_relationships.asset_id" ; 
            $query_array['where'] .= " AND asset_metadata_relationships.asset_id='$query_options->asset_id'" ; 
            
            $query_array['join_tables'][] = array(
                'table' => 'asset_metadata_relationships',
                'on' => 'asset_metadata_relationships.metadata_id',
                'match' => 'metadata.metadata_id'
                );            
            }
        

        
        // Add account_id filter
        // REQUIRED
        switch ($query_options->filter_by_account_id) {
            case 'none':

                break ;
            case 'internal':
                $query_array['where'] .= " AND metadata.account_id='$this->account_id' " ;   
                break ;
            case 'internal_global':
                $query_array['where'] .= " AND (metadata.account_id='$this->account_id' OR metadata.global_metadata='1') " ;  
                break ; 
            default:
                $query_array['where'] .= " AND metadata.account_id='$query_options->filter_by_account_id' " ;   
            }

    
        // REQUIRED
        // Filter by metadata_name
        // metadata_type.metadata_type_name='$metadata_type_name'         
        $metadata_type_array = Utilities::Process_Comma_Separated_String($metadata_type_name) ; 

        $metadata_type_query_string = '' ; 
        foreach ($metadata_type_array as $metadata_type) {
            $metadata_type_query_string .= "metadata_type.metadata_type='$metadata_type' OR " ; 
            }
        $metadata_type_query_string = rtrim($metadata_type_query_string," OR ") ; 
        $query_array['where'] .= " AND ($metadata_type_query_string) " ;
        
        
        
            
        // PROCESS METADATA VISIBILITY FILTER        
        $visibility_array = $this->Retrieve_Visibility_ID_Array($query_options) ; 
        
        // Add visibility filter if there are visibility_id's to process
        $filter_by_visibility_id_string = '' ; 
        if (count($visibility_array) > 0) {

            foreach ($visibility_array as $visibility) {
                
                $this_visibility_id = $visibility['visibility_id'] ; 
                $filter_by_visibility_id_string .= "metadata.visibility_id='$this_visibility_id' OR " ;               
                }
            
            $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," OR ") ;
            $query_array['where'] .= "AND ($filter_by_visibility_id_string) " ;  
            }
        
        
        
        if (isset($query_options->asset_type)) {
            $asset_type_array = Utilities::Process_Comma_Separated_String($query_options->asset_type) ; 
            
            $asset_type_query_string = '' ; 
            foreach ($asset_type_array as $asset_type) {
                $asset_type_query_string .= "asset_type.type_name='$asset_type' OR " ; 
                }
            $asset_type_query_string = rtrim($asset_type_query_string," OR ") ;             
            
            $query_array['fields'] .= ", asset_metadata_relationships.relationship_id, asset_metadata_relationships.asset_id" ;
            $query_array['where'] .= " AND ($asset_type_query_string)" ; 
            $query_array['group_by'] = "metadata.metadata_id" ;  
            
            $query_array['join_tables'][] = array(
                'table' => 'asset_metadata_relationships',
                'on' => 'asset_metadata_relationships.metadata_id',
                'match' => 'metadata.metadata_id'
                ); 
            
            $query_array['join_tables'][] = array(
                'table' => 'assets',
                'on' => 'assets.asset_id',
                'match' => 'asset_metadata_relationships.asset_id'
                );
            
            $query_array['join_tables'][] = array(
                'table' => 'asset_type',
                'on' => 'asset_type.type_id',
                'match' => 'assets.type_id'
                );            
            }
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->asset_query_result = $result ;
        
        return $result ;               
        }
    


    public function Get_Conversation_List() {
        
        return $this->conversation_list ; 
        } 
    
    public function Get_Conversation_Results() {
        
        return $this->conversation_results ; 
        } 
    

    public function Action_Mark_Conversation_Read() {
    
        $data['conversation_list'] = $this->Get_Conversation_List() ;
        
        foreach ($data['conversation_list'] as $conversation_set) {

            foreach ($conversation_set['conversation'] as $conversation) {
                
                if ($conversation['delivery_type'] == 'incoming') {
                    switch ($conversation['opened']) {
                        case 0:  // Currently unread
                            if (isset($conversation['message_id'])) {// SMS
                                $update_message_item = array(
                                        'opened' => 1
                                        ) ; 

                                $message = new Message() ; 
                                $message->Set_Message_ID($conversation['message_id']) ; 
                                $data['message_update'][] = $message->Update_Message_Recipient($update_message_item) ;
                                
                                }
                           
                            break ;
                        }
                    }                
                }            
            }
        
        return $this ; 
        }
    
    
    
    public function Action_Process_Conversation_List($new_conversation_set,$query_options = array()) {   
         
        $data['profile_record'] = $this->Get_Profile() ; 
        
        if (isset($query_options['existing_conversation_set'])) {  
            $data['conversation_list'] = $query_options['existing_conversation_set'] ; 
            } else {
                $data['conversation_list'] = array() ;
                }
        
        $data['conversation_results'] = array(
            'incoming_unread' => 0,
            'incoming_unread_conversations' => array()
            ) ;
        
        if (count($new_conversation_set) > 0) {
            foreach ($new_conversation_set as $new_conversation) { 

                switch ($query_options['asset_type']) {
                    case 18: // SMS message    

                        $event_query = array(
                            'message_id' => $new_conversation['message_id']
                            ) ; 

                        $message_events = new Message() ; 
                        $message_events->Set_Message_Event_List($event_query) ; 
                        $new_conversation['events'] = $message_events->Get_Message_Event_List() ; 
 
                        
                        if ($new_conversation['delivery_type'] == 'incoming') {
                            $messaging_number = Utilities::Deep_Array($data['profile_record']['messaging_numbers_list'],'phone_number_messaging',$new_conversation['phone_number_international_recipient']) ;
                            $new_conversation['messaging_number'] = $messaging_number ; 
                            } else {
                                $messaging_number = Utilities::Deep_Array($data['profile_record']['messaging_numbers_list'],'phone_number_messaging',$new_conversation['phone_number_international_sender']) ;
                                $new_conversation['messaging_number'] = $messaging_number ; 
                                }                   

                        break ; 
                    }


                
                
                $new_conversation = $this->Action_Time_Territorialize_Dataset($new_conversation) ;
                $new_conversation = $this->Action_Phone_Territorialize_Dataset($new_conversation) ;            

                if ($new_conversation['message_media']) { 
                    
                    $new_conversation['message_media_set'] = array() ; 
                    
                    $new_conversation['message_media'] = json_decode($new_conversation['message_media'],true) ;
                    foreach ($new_conversation['message_media'] as $media_item) {
                        $new_conversation['message_media_set'][] = $media_item ; 
                        }
                    }
                
                
                if ($new_conversation['delivery_type'] == 'incoming') {  
                    if ($new_conversation['opened'] == 0) {
                        $data['conversation_results']['incoming_unread']++ ; 
                        
                        $data['conversation_results']['incoming_unread_conversations'][$new_conversation['member_id']] = array(
                            'member_id_value' => $new_conversation['member_id_value'], 
                            'member_id' => $new_conversation['member_id'], 
                            'type_id' => $new_conversation['type_id'],
                            'type_name' => $new_conversation['type_name'],
                            'unread' =>  $data['conversation_results']['incoming_unread_conversations'][$new_conversation['member_id']]['unread'] + 1
                            ) ; 
                        }
                    }

                $add_to_set = 'create' ; 
                $c = 0 ;
                do {

                    if (count($data['conversation_list']) > 0) {
                        if (($new_conversation['member_id_value'] == $data['conversation_list'][$c]['member_id_value']) AND ($new_conversation['type_id'] == $data['conversation_list'][$c]['type_id'])) {
                            $add_to_set = $c ;                         
                            }
                        }

                    $c++  ;
                    } while ($c < count($data['conversation_list'])) ;    


                if ($add_to_set === 'create') { // This has to be === because of mixed variable types (integer vs string) I think 

                    switch ($new_conversation['type_id']) { 
                        case 19: // Contact
                        case 26: // Contact Series
                            $contact = new Contact() ; 
                            $contact->Set_Account_ID($new_conversation['account_id'])->Set_Contact_By_ID($new_conversation['member_id_value']) ; 
                            $new_conversation['member'] = $contact->Get_Contact() ; 
                            $new_conversation['conversation_type'] = 'Contact' ; 
                            $new_conversation['phone_number_international_recipient_compiled'] = $new_conversation['member']['phone_number_compiled'] ; 
                            
                            $list_query = new Asset() ;
                            $recipient_campaign_query_options['asset_type'] = 18 ; // SMS message
                            $recipient_campaign_query_options['profile_id'] = $this->profile_id ; 
                            $recipient_campaign_query_options['delivery_type'] = array('incoming') ;
                            $recipient_campaign_query_options['override_paging'] = 'yes' ; 
                            
                            $recipient_campaign_query_options['type_name'] = 'distribution_list_contacts' ; 
                            $recipient_campaign_query_options['member_id'] = $new_conversation['member_id'] ; 
                            $recipient_campaign_query_options['opened'] = 0 ; 
                            
                            $recipient_campaign_query_options['order_by'] = 'messages.timestamp_scheduled' ; 
                            $recipient_campaign_query_options['order'] = 'ASC' ; 

                            $data['unread_list'] = $list_query->Retrieve_Recipient_Campaign_List($recipient_campaign_query_options) ;                   

                            break ; 
                        case 23: // Account User Profile
                        case 27: // Account User Profile Series  
                            $profile = new Account() ; 
                            $profile->Set_Profile_By_ID($new_conversation['member_id_value']) ; 
                            $new_conversation['member'] = $profile->Get_Profile() ;
                            
                            if ($new_conversation['member']['account_id'] == $this->account_id) {
                                $new_conversation['conversation_type'] = 'Team Member' ; 
                                } else {
                                $new_conversation['conversation_type'] = 'App User Profile' ; 
                                }
                            
                            $new_conversation['phone_number_international_recipient_compiled'] = $new_conversation['member']['phone_number_compiled'] ; 
                            
                            $list_query = new Asset() ;
                            $recipient_campaign_query_options['asset_type'] = 18 ; // SMS message
                            $recipient_campaign_query_options['profile_id'] = $this->profile_id ; 
                            $recipient_campaign_query_options['delivery_type'] = array('incoming') ;
                            $recipient_campaign_query_options['override_paging'] = 'yes' ; 
                            
                            $recipient_campaign_query_options['type_name'] = 'distribution_list_profiles' ; 
                            $recipient_campaign_query_options['member_id'] = $new_conversation['member_id'] ; 
                            $recipient_campaign_query_options['opened'] = 0 ; 
                            
                            $recipient_campaign_query_options['order_by'] = 'messages.timestamp_scheduled' ; 
                            $recipient_campaign_query_options['order'] = 'ASC' ; 

                            $data['unread_list'] = $list_query->Retrieve_Recipient_Campaign_List($recipient_campaign_query_options) ;  
                            break ;
                        case 20: // Admin User
                        case 28: // Admin User Series   
                            $user = new User() ; 
                            $user->Set_User_By_ID($new_conversation['member_id_value']) ; 
                            $new_conversation['member'] = $user->Get_User() ; 
                            $new_conversation['conversation_type'] = 'App User' ; 
                            $new_conversation['phone_number_international_recipient_compiled'] = $new_conversation['member']['phone_number_compiled'] ; 
                            
                            $list_query = new Asset() ;
                            $recipient_campaign_query_options['asset_type'] = 18 ; // SMS message
                            $recipient_campaign_query_options['profile_id'] = $this->profile_id ; 
                            $recipient_campaign_query_options['delivery_type'] = array('incoming') ;
                            $recipient_campaign_query_options['override_paging'] = 'yes' ; 
                            
                            $recipient_campaign_query_options['type_name'] = 'distribution_list_users' ; 
                            $recipient_campaign_query_options['member_id'] = $new_conversation['member_id'] ; 
                            $recipient_campaign_query_options['opened'] = 0 ; 
                            
                            $recipient_campaign_query_options['order_by'] = 'messages.timestamp_scheduled' ; 
                            $recipient_campaign_query_options['order'] = 'ASC' ; 

                            $data['unread_list'] = $list_query->Retrieve_Recipient_Campaign_List($recipient_campaign_query_options) ;  
                            break ;                        
                        }

                    $create_conversation = $new_conversation ;
                    $create_conversation['conversation'][] = $create_conversation ; 

                    $create_conversation['conversation_results'] = array(
                        'incoming_unread' => $data['unread_list']['total_count']
                        ) ;

                    if ($new_conversation['delivery_type'] == 'incoming') {
                        if ($new_conversation['opened'] == 0) {
                            $create_conversation['conversation_results']['incoming_unread']++ ; 
                            }
                        }

                    $data['conversation_list'][] = $create_conversation ; 


                    } else {
                        $data['conversation_list'][$add_to_set]['conversation'][] = $new_conversation ; 

                        if ($new_conversation['delivery_type'] == 'incoming') {
                            if ($new_conversation['opened'] == 0) {
                                $data['conversation_list'][$add_to_set]['conversation_results']['incoming_unread']++ ; 
                                }
                            }

                        }


                }
            }
        return $data ; 
        }
    
    
    public function Set_Conversation_List($query_options = array()) {
        
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        
        if (!$query_options['order_by']) {
            $query_options['order_by'] = 'messages.timestamp_scheduled' ; 
            }
        
        if (!isset($query_options['order'])) {
            $query_options['order'] = 'DESC' ; 
            } 
        
        $result_conversation = $this->Retrieve_Recipient_Campaign_List($query_options) ; 
 
//        print_r('<pre>') ; 
//        print_r($result_conversation['results']) ; 
        
        
//        $additional_parameters['skip_asset_mapping'] = 'yes' ; 
//        $result_conversation = $this->Action_Map_Asset($result_conversation,$additional_parameters) ;        
        
        $process_query_options['asset_type'] = $query_options['asset_type'] ; 
        
        $data = $this->Action_Process_Conversation_List($result_conversation['results'],$process_query_options) ;                 
        
        $this->asset_query_result = $result_conversation ; 
        $this->conversation_list = $data['conversation_list'] ;
        $this->conversation_results = $data['conversation_results'] ;
        
        return $this ; 
        }
    
    
    
    public function Retrieve_Recipient_Campaign_List($query_options = array()) { 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write


        switch ($query_options->asset_type) {
            case 18: // SMS message
                
                $asset_table = 'messages' ; 
                
                $query_array = array(
                    'table' => 'messages',
                    'fields' => "messages.message_id, messages.conversation_id, messages.automation_id, messages.message_sid, 
                        messages.message_content_id, messages.message_status_id, messages.member_id_value, messages.member_id, messages.delivery_type, 
                        messages.sender_name, messages.timestamp_scheduled, 
                        messages.microtimestamp_delivered, messages.microtimestamp_opened, messages.test_mode, messages.delivered, messages.opened, messages.clicked, messages.system_alert_id, messages.status_result, 
                        message_content.vendor_id, message_content.message_content, message_content.message_media, message_content.segment_count",
                    'order' => 'DESC',
                    'order_by' => 'messages.timestamp_scheduled',
                    'where' => "messages.message_id>0 "
                    );                
                
                $query_array['join_tables'][] = array(
                    'table' => 'message_content',
                    'on' => 'message_content.message_content_id',
                    'match' => 'messages.message_content_id',
                    'type' => 'left'
                    ) ; 
                

                if (isset($query_options->member_id)) {
                    $query_array['where'] .= " AND messages.member_id='$query_options->member_id' " ;
                    }
                
                if (isset($query_options->member_id_value)) {
                    $query_array['where'] .= " AND messages.member_id_value='$query_options->member_id_value' " ;
                    }
                
                if (isset($query_options->phone_number_international_recipient)) {
                    $query_array['where'] .= " AND messages.phone_number_international_recipient='$query_options->phone_number_international_recipient' " ;
                    }
                
                if (isset($query_options->message_id)) {
                    $query_array['where'] .= " AND messages.message_id='$query_options->message_id' " ;
                    }
                
                if (isset($query_options->order)) {
                    $query_array['order'] = $query_options->order ; 
                    }
                
                if (isset($query_options->opened)) {
                    $query_array['where'] .= " AND messages.opened='$query_options->opened' " ;
                    }               
                
                break ;                
            }        
        
        // Standard fields
        $query_array['fields'] .= ", asset_campaign_automation.campaign_position, asset_campaign_automation.automation_status_id, 
                asset_campaign_automation.timestamp_next_action, asset_campaign_automation.error_count, 
                asset_campaigns.campaign_id, asset_campaigns.user_id, asset_campaigns.account_id, 
                asset_campaigns.asset_id AS delivered_asset_id, asset_campaigns.delivery_asset_id, 
                asset_campaigns.structured_data_01 AS structured_data_campaigns_01, 
            assets.asset_title, assets.type_id, asset_type.type_name, 
            asset_content.structured_data_01, 
            user_profiles.profile_id" ;  
        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => "$asset_table.automation_id",
            'match' => 'asset_campaign_automation.automation_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaigns',
            'on' => 'asset_campaign_automation.campaign_id',
            'match' => 'asset_campaigns.campaign_id'
            );
                
        $query_array['join_tables'][] = array(
            'table' => 'asset_status',
            'on' => 'asset_status.asset_status_id',
            'match' => 'asset_campaigns.campaign_status_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'assets',
            'on' => 'assets.asset_id',
            'match' => 'asset_campaigns.delivery_asset_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_type',
            'on' => 'asset_type.type_id',
            'match' => 'assets.type_id'
            ) ; 
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_content',
            'on' => 'asset_content.asset_id',
            'match' => 'assets.asset_id'
            );        

        $query_array['join_tables'][] = array(
            'table' => 'users',
            'on' => 'asset_campaigns.user_id',
            'match' => 'users.user_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'accounts',
            'on' => 'accounts.account_id',
            'match' => 'asset_campaigns.account_id'
            );

        $query_array['join_tables'][] = array(
            'table' => 'user_profiles',
            'statement' => '(user_profiles.user_id=asset_campaigns.user_id AND user_profiles.account_id=asset_campaigns.account_id)'
            );
        
        
        if (isset($query_options->profile_id)) {
            $query_array['where'] .= " AND user_profiles.profile_id='$query_options->profile_id' " ;
            }
        
//        if (isset($query_options->type_id)) {
//            $query_array['where'] .= " AND asset_type.type_id='$query_options->type_id' " ;
//            }
//        
//        if (isset($query_options->type_name)) {
//            $query_array['where'] .= " AND asset_type.type_name='$query_options->type_name' " ;
//            }
        
        
        
        // PROCESS DELIVERY ASSET TYPE FILTER
        $type_id_array = $this->Retrieve_Asset_Type_ID_Array($query_options) ; 

        // Add asset type filter if there are type_id's to process
        if (count($type_id_array) > 0) {
            $filter_by_asset_type_string = '' ; 
            foreach ($type_id_array as $type_id) {
                $filter_by_asset_type_string .= "assets.type_id='$type_id' OR " ; 
                }
            $filter_by_asset_type_string = rtrim($filter_by_asset_type_string," OR ") ;
            $query_array['where'] .= " AND ($filter_by_asset_type_string) " ;                
            }
        
        
        
        if (isset($query_options->delivery_type)) {
            
            // This needs to be submitted as an array of values.
            // If an array wasn't submitted, turn it into an array
            $delivery_type_array = Utilities::Array_Force($query_options->delivery_type) ;             

            if (count($delivery_type_array) > 0) {
                $filter_by_delivery_type_string = '' ; 
                foreach ($delivery_type_array as $delivery_type) {
                    $filter_by_delivery_type_string .= "$asset_table.delivery_type='$delivery_type' OR " ; 
                    }
                $filter_by_delivery_type_string = rtrim($filter_by_delivery_type_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_delivery_type_string) " ;                
                }         
            }
        
        
        switch ($query_options->list_group_type) {
            case 'contacts':
                
                $query_array['join_tables'][] = array(
                    'table' => 'contacts',
                    'on' => 'messages.member_id_value',
                    'match' => 'contacts.contact_id'
                    );                
                
                $query_array['fields'] .= ", CONCAT(contacts.first_name,' ',contacts.last_name) AS recipient_name, 
                    CONCAT(contacts.phone_country_dialing_code,contacts.phone_number) AS phone_number_international_recipient, contacts.timestamp_contact_created " ;
                 
                break ;
            case 'users':
                
                $query_array['join_tables'][] = array(
                    'table' => 'users AS recipient_users',
                    'on' => 'messages.member_id_value',
                    'match' => 'recipient_users.user_id'
                    );                
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_phone_numbers AS recipient_user_phone_numbers',
                    'on' => 'recipient_user_phone_numbers.phone_number_id',
                    'match' => 'recipient_users.phone_number_id'
                    );
                
                $query_array['fields'] .= ", CONCAT(recipient_users.first_name,' ',recipient_users.last_name) AS recipient_name, 
                    CONCAT(recipient_user_phone_numbers.phone_country_dialing_code,recipient_user_phone_numbers.phone_number) AS phone_number_international_recipient, 
                    recipient_users.timestamp_created " ;
                 
                break ; 
            case 'profiles':
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_profiles AS recipient_profiles',
                    'on' => 'messages.member_id_value',
                    'match' => 'recipient_profiles.profile_id'
                    );                
                
                $query_array['join_tables'][] = array(
                    'table' => 'user_phone_numbers AS recipient_user_phone_numbers',
                    'on' => 'recipient_user_phone_numbers.phone_number_id',
                    'match' => 'recipient_profiles.phone_number_id',
                    'type' => 'left'
                    );
                
                $query_array['fields'] .= ", CONCAT(recipient_profiles.first_name,' ',recipient_profiles.last_name) AS recipient_name, 
                    CONCAT(recipient_user_phone_numbers.phone_country_dialing_code,recipient_user_phone_numbers.phone_number) AS phone_number_international_recipient, 
                    recipient_profiles.timestamp_profile_created " ;
                 
                break ;                
            default:  
                
//                $query_array['join_tables'][] = array(
//                    'table' => 'users AS recipient_users',
//                    'statement' => '(messages.member_id_value=recipient_users.user_id AND assets.type_id=20)'
//                    );                

            }


        // Add filter by search parameter
        if (isset($query_options->filter_by_search_parameter)) {

            $search_parameter_token = explode(" ",$query_options->filter_by_search_parameter) ; 

            $t = 0 ; 
            foreach ($search_parameter_token as $search_token) {
                if ($search_token == '') {
                    unset($search_parameter_token[$t]) ; 
                    }
                $t++ ; 
                }
            array_values($search_parameter_token) ; 

            foreach ($search_parameter_token as $search_token) {
                $phone_number = Utilities::Validate_Phone_Number($search_token) ;
                if ($phone_number['valid'] == 'yes') {
                    $search_parameter_token[] = $phone_number['phone_number'] ;    
                    }
                }

            $compiled_search_string = '' ; 

            foreach ($search_parameter_token as $search_token) {
                
                switch ($query_options->list_group_type) {
                    case 'contacts':
                        $this_search_string = " (
                            CONCAT(contacts.first_name,' ',contacts.last_name) LIKE '%$search_token%' OR 
                            CONCAT(contacts.phone_country_dialing_code,contacts.phone_number) LIKE '%$search_token%'
                            ) " ;                
                        break ; 
                    case 'users':
                        $this_search_string = " (
                            CONCAT(recipient_users.first_name,' ',recipient_users.last_name) LIKE '%$search_token%' OR 
                            CONCAT(recipient_user_phone_numbers.phone_country_dialing_code,recipient_user_phone_numbers.phone_number) LIKE '%$search_token%'
                            ) " ;
                        break ; 
                    case 'profiles':
                        $this_search_string = " (
                            CONCAT(recipient_profiles.first_name,' ',recipient_profiles.last_name) LIKE '%$search_token%' OR 
                            CONCAT(recipient_user_phone_numbers.phone_country_dialing_code,recipient_user_phone_numbers.phone_number) LIKE '%$search_token%'
                            ) " ;
                        break ; 
                    }
                                   
                $compiled_search_string .= $this_search_string.' OR ' ;                             
                }

            $compiled_search_string = rtrim($compiled_search_string," OR ") ;  
            
            $query_array['where'] .= " AND ($compiled_search_string) " ;                                    
            }
        
        
        

        if (isset($query_options->group_by)) {
            
            $query_array['skip_query'] = 1 ; 
            $query_array['fields'] .= ", MAX(messages.timestamp_scheduled) AS most_recent" ;
            $query_array['group_by'] = $query_options->group_by ; 
            
            $result = $this->DB->Query('SELECT_JOIN',$query_array,'force') ;
            
            $query_array = array(
                'statement' => 'SELECT conversation.* FROM ('.$result['query'].') AS conversation',
                ) ;

            
            // Add sort filter
            if (isset($query_options->order_by)) {
                $query_array['order_by'] = $query_options->order_by ;  
                $query_array['order'] = $query_options->order ;  
                }         
                
            
            // NEW PAGING PROCESS
            // Add paging constraints
            $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

            // Pull visible results
            $result = $this->DB->Query('STATEMENT',$query_array,'force') ;  
            
            $result['options'] = $query_options ;
            
            } else {
            
            
                // Add sort filter
                if (isset($query_options->order_by)) {
                    $query_array['order_by'] = $query_options->order_by ;  
                    $query_array['order'] = $query_options->order ;  
                    }            
            
            
            
            
                // NEW PAGING PROCESS
                // Add paging constraints
                $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

            
                // Pull visible results
                $result = $this->DB->Query('SELECT_JOIN',$query_array,'force') ;           
                $result['options'] = $query_options ;
                }     
        
        
        
        
        $query_options->value_name = 'campaign_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_Asset_Paging($result) ;         
        $this->asset_query_result = $result ;   
        
                
        return $result ;        
        }
    
    
    
    public function Retrieve_Asset_Status_ID_Array($query_options,$single = 'no') {
        
        if (is_array($query_options)) {
            $query_options = (object) $query_options  ;
            }
        
        // PROCESS ASSET STATUS FILTER
        $asset_status_array = array() ; 
        
        // Submitted as an array of type_id values.
        if (isset($query_options->filter_by_asset_status_id)) {
            
            // If an array wasn't submitted, turn it into an array
            $asset_status_id_array = Utilities::Process_Comma_Separated_String($query_options->filter_by_asset_status_id) ; 
//            $asset_status_id_array = Utilities::Array_Force($query_options->filter_by_asset_status_id) ; 
            
            $query_status_array = array(
                'table' => 'asset_status',
                'fields' => "*",
                'where' => ""
                ) ;

            foreach ($asset_status_id_array as $status_id) {
                $query_visibility_array['where'] .= "asset_status.asset_status_id='$status_id' OR " ;
                }
            
            $query_status_array['where'] = rtrim($query_visibility_array['where']," OR ") ;
            $asset_status_id_result = $this->DB->Query('SELECT',$query_status_array,'force');             
            
            foreach ($asset_status_id_result['results'] as $status) {
                $asset_status_array[] = $status ; 
                }            
            }
        
        
        // Submitted as an array of status_names's which is converted into, and added to the status_name array
        if (isset($query_options->filter_by_asset_status_name)) {
            
            // If an array wasn't submitted, turn it into an array
            $asset_status_name_array = Utilities::Process_Comma_Separated_String($query_options->filter_by_asset_status_name) ; 
//            $asset_status_name_array = Utilities::Process_Comma($query_options->filter_by_asset_status_name) ; 
            
            $query_status_array = array(
                'table' => 'asset_status',
                'fields' => "*",
                'where' => ""
                ) ;

            foreach ($asset_status_name_array as $status_name) {
                switch ($status_name) {
//                    case 'admin_all': // We should provide special treatment for hiddden since it's admin only...
//                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_hidden' OR " ;  
//                    case 'all':
//                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_deleted' OR " ; 
                    case 'all':    
                        $query_status_array['where'] .= "asset_status.status_name='error' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='deleted' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='draft' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='scheduled' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='packaged' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='in_progress' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='queued' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='complete' OR " ; 
                        break ;
                    case 'visible':    
                        $query_status_array['where'] .= "asset_status.status_name='draft' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='scheduled' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='packaged' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='in_progress' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='queued' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='complete' OR " ;
                        break ;                        
                    case 'in_delivery':    
                        $query_status_array['where'] .= "asset_status.status_name='packaged' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='in_progress' OR " ; 
                        $query_status_array['where'] .= "asset_status.status_name='queued' OR " ;
                        break ;                         
                    default:
                        $query_status_array['where'] .= "asset_status.status_name='$status_name' OR " ; 
                    }
                }
            
            
            $query_status_array['where'] = rtrim($query_status_array['where']," OR ") ;
            $asset_status_result = $this->DB->Query('SELECT',$query_status_array,'force');   

            foreach ($asset_status_result['results'] as $status) {
                $asset_status_array[] = $status ; 
                }
            }        
        
        if ('single' === $single) {
            $asset_status_array = $asset_status_array[0] ; 
            }
        
        return $asset_status_array ;
        }    
    
    
    
    public function Retrieve_Campaign_List($query_options = array()) {
        
        $this->Set_Model_Timings('Asset.Retrieve_Campaign_List_begin') ;
        
        global $server_config ; // To pull in list defaults
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
              
        
        $query_array = array(
            'table' => 'asset_campaigns',
            'fields' => "asset_campaigns.*, 
                asset_status.status_name AS campaign_status_name, asset_status.status_title AS campaign_status_title, asset_status.status_description AS campaign_status_description, 
                asset_campaign_automation.timestamp_next_action ",
            'where' => "",
            'group_by' => "asset_campaigns.campaign_id"
            );  
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_status',
            'on' => 'asset_status.asset_status_id',
            'match' => 'asset_campaigns.campaign_status_id'
            ); 
              
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaign_automation',
            'on' => 'asset_campaign_automation.campaign_id',
            'match' => 'asset_campaigns.campaign_id',
            'type' => 'left'
            ); 
        
//        $query_array['join_tables'][] = array(
//            'table' => 'asset_type',
//            'on' => 'asset_type.type_id',
//            'match' => 'assets.type_id',
//            'type' => 'left'
//            ) ; 
        
//        $query_array['join_tables'][] = array(
//            'table' => 'assets as delivery_asset',
//            'on' => 'delivery_asset.asset_id',
//            'match' => 'asset_campaigns.delivery_asset_id',
//            'type' => 'left'
//            ); 
//        
//        $query_array['join_tables'][] = array(
//            'table' => 'asset_type AS delivery_asset_type',
//            'on' => 'delivery_asset_type.type_id',
//            'match' => 'delivery_asset.type_id',
//            'type' => 'left'
//            );
        

        // Content Asset Subquery
        $join_table_name = 'content_asset' ;  
        $query_array['fields'] .= ", $join_table_name.asset_id AS content_asset_id, 
            $join_table_name.asset_title AS content_asset_title, 
            $join_table_name.type_id AS content_asset_type_id, 
            $join_table_name.timestamp_asset_created AS content_asset_timestamp_asset_created,
            $join_table_name.type_name AS content_asset_type_name,
            $join_table_name.type_display_name AS content_asset_type_display_name,
            LOWER($join_table_name.type_display_name) AS content_asset_type_display_name_lc,
            $join_table_name.type_icon AS content_asset_type_icon,
            $join_table_name.type_id AS type_id, 
            $join_table_name.type_name AS type_name,
            $join_table_name.type_display_name AS type_display_name" ;  // Remove these last three lines after all users are on 7.02+, then update the system_fields broadcast list DB entries to reflect new field names
            
        // Create the operator
        $join_type = 'left' ; 
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT assets.asset_id, assets.asset_title, assets.timestamp_asset_created, 
                asset_type.*, asset_status.*, 
                system_visibility_levels.* 
                FROM assets 
                JOIN asset_type ON assets.type_id = asset_type.type_id 
                JOIN asset_status ON asset_status.asset_status_id = asset_status.asset_status_id 
                JOIN system_visibility_levels ON assets.visibility_id = system_visibility_levels.visibility_id 
                ) AS $join_table_name",
            'type' => $join_type,
            'statement' => "
                    (asset_campaigns.asset_id=$join_table_name.asset_id)"
            );
        
        
        
        // Content Asset Structure Count
        if ($query_options->condense_campaign != 'yes') {
            
            $join_table_name = 'content_asset_structure' ;  
            $query_array['fields'] .= ", $join_table_name.asset_structure_count AS asset_structure_count" ; 

            // Create the operator
            $join_type = 'left' ; 

            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT 
                    asset_structure.asset_id, 
                    asset_structure.structure_id, 
                    COUNT(asset_structure.structure_id) AS asset_structure_count 
                    FROM asset_structure 
                    GROUP BY asset_structure.asset_id  
                    ) AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (asset_campaigns.asset_id=$join_table_name.asset_id)"
                );        
            }
        
        
        // Delivery Asset Member Count
        if ($query_options->condense_campaign != 'yes') {
            
            $join_table_name = 'campaign_members' ;  
            $query_array['fields'] .= ", $join_table_name.asset_member_count AS asset_member_count " ; 

            // Create the operator
            $join_type = 'left' ; 

            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT 
                    asset_members.asset_id, 
                    COUNT(asset_members.member_id) AS asset_member_count  

                    FROM asset_members 
                    JOIN assets ON asset_members.asset_id = assets.asset_id 

                    GROUP BY asset_members.asset_id  
                    ) AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (asset_campaigns.delivery_asset_id=$join_table_name.asset_id)"
                );        
            }
               
        
        
        // Delivery Asset Subquery
        $join_table_name = 'delivery_asset' ;  
        $query_array['fields'] .= ", 
            $join_table_name.asset_title AS delivery_asset_title, 
            $join_table_name.type_id AS delivery_asset_type_id, 
            $join_table_name.type_name AS delivery_asset_type_name,
            $join_table_name.type_display_name AS delivery_asset_type_display_name,
            LOWER($join_table_name.type_display_name) AS delivery_asset_type_display_name_lc" ; 
            

        // Create the operator
        $join_type = 'left' ; 
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT assets.asset_id, assets.asset_title, asset_type.*, asset_status.*, system_visibility_levels.* 
                FROM assets 
                JOIN asset_type ON assets.type_id = asset_type.type_id 
                JOIN asset_status ON asset_status.asset_status_id = asset_status.asset_status_id 
                JOIN system_visibility_levels ON assets.visibility_id = system_visibility_levels.visibility_id
                ) AS $join_table_name",
            'type' => $join_type,
            'statement' => "
                    (asset_campaigns.delivery_asset_id=$join_table_name.asset_id)"
            );        
        
        
        
        
        
        // Broadcast Campaign - Email & message deliveries 
        if ($query_options->condense_campaign != 'yes') {
            
            $join_table_name = 'content_deliveries' ;  
            $query_array['fields'] .= ", 
                $join_table_name.automation_id AS current_automation_id, 
                $join_table_name.email_delivery_count, $join_table_name.message_delivery_count  " ; 


            // Create the operator
            $join_type = 'left' ; 

            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT 
                    asset_campaign_automation.automation_id, 
                    COUNT(email_deliveries.email_id) AS email_delivery_count, 
                    COUNT(message_deliveries.message_id) AS message_delivery_count 
                    FROM asset_campaign_automation 

                    LEFT JOIN (
                        SELECT 
                        emails.email_id, emails.automation_id 
                        FROM emails 
                        GROUP BY emails.email_id 
                        ) AS email_deliveries ON asset_campaign_automation.automation_id=email_deliveries.automation_id                

                    LEFT JOIN (
                        SELECT 
                        messages.message_id, messages.automation_id 
                        FROM messages 
                        GROUP BY messages.message_id 
                        ) AS message_deliveries ON asset_campaign_automation.automation_id=message_deliveries.automation_id  

                    GROUP BY asset_campaign_automation.campaign_id    
                    ) AS $join_table_name",
                'type' => $join_type,
                'statement' => "(asset_campaign_automation.automation_id=$join_table_name.automation_id)"
                ) ;
            }
        
        
        
        
        // THIS PART OF THE QUERY IS PROBLEMATIC -- IT WILL CRASH THE DATABASE
        // It is used to send test emails to the user from a campaign
        // Current Automation Item Subquery Statement
//        $query_array['fields'] .= ", current_automation.automation_id AS current_automation_id, 
//                current_automation.timestamp_next_action, current_automation.automation_status_id AS current_automation_status_id, current_automation.status_name AS current_automation_status_name, current_automation.status_title AS current_automation_status_title" ;         
//        
//        $query_array['join_tables'][] = array(
//            'table' => "
//                (SELECT asset_campaign_automation.*, asset_status.* 
//                FROM asset_campaign_automation 
//                JOIN asset_campaigns ON asset_campaigns.campaign_id = asset_campaign_automation.campaign_id 
//                JOIN asset_status ON asset_status.asset_status_id = asset_campaign_automation.automation_status_id 
//                ) AS current_automation",
//            'statement' => "(current_automation.campaign_id=asset_campaigns.campaign_id)",
//            'type' => 'left'
//            );
        
        
        

        if ($query_options->filter_by_user_id == 'yes') {
            if (isset($query_options->user_id)) {
                $query_array['where'] .= " AND asset_campaigns.user_id='$query_options->user_id'"  ; 
                } else {
                    $query_array['where'] .= " AND asset_campaigns.user_id='$this->user_id'"  ; 
                    }            
            }
        
        if ($query_options->filter_by_account_id == 'yes') {
            if (isset($query_options->account_id)) {
                $query_array['where'] .= " AND asset_campaigns.account_id='$query_options->account_id'"  ; 
                } else {
                    $query_array['where'] .= " AND asset_campaigns.account_id='$this->account_id'"  ;
                    }              
            }        
        
        if(isset($query_options->timestamp_scheduled_lower)) {
            $query_array['where'] .= " AND asset_campaigns.timestamp_campaign_scheduled>='$query_options->timestamp_scheduled_lower'"  ; 
            }
        
        if(isset($query_options->timestamp_scheduled_upper)) {
            $query_array['where'] .= " AND asset_campaigns.timestamp_campaign_scheduled<='$query_options->timestamp_scheduled_upper'"  ; 
            }        
        
        if(isset($query_options->filter_by_campaign_id)) { 
            $query_array['where'] .= " AND asset_campaigns.campaign_id='$query_options->filter_by_campaign_id'"  ; 
            }  
    

        
        // PROCESS CONTENT ASSET TYPE FILTER
        $type_id_array = $this->Retrieve_Asset_Type_ID_Array($query_options) ; 

        // Add asset type filter if there are type_id's to process
        if (count($type_id_array) > 0) {
            $filter_by_asset_type_string = '' ; 
            foreach ($type_id_array as $type_id) {
                $filter_by_asset_type_string .= "content_asset.type_id='$type_id' OR " ; 
                }
            $filter_by_asset_type_string = rtrim($filter_by_asset_type_string," OR ") ;
            $query_array['where'] .= " AND ($filter_by_asset_type_string) " ;                
            }
        
        
        // PROCESS DELIVERY ASSET TYPE FILTER
        if (isset($query_options->filter_by_delivery_asset_type_name)) {
            
            $delivery_asset_query_options = new stdClass();
            $delivery_asset_query_options->filter_by_type_name = $query_options->filter_by_delivery_asset_type_name ; 
                
            $delivery_asset_type_id_array = $this->Retrieve_Asset_Type_ID_Array($delivery_asset_query_options) ; 

            // Add asset type filter if there are type_id's to process
            if (count($delivery_asset_type_id_array) > 0) {
                $filter_by_delivery_asset_type_string = '' ; 
                foreach ($delivery_asset_type_id_array as $type_id) {
                    $filter_by_delivery_asset_type_string .= "delivery_asset.type_id='$type_id' OR " ; 
                    }
                $filter_by_delivery_asset_type_string = rtrim($filter_by_delivery_asset_type_string," OR ") ;
                $query_array['where'] .= " AND ($filter_by_delivery_asset_type_string) " ;                
                }            
            }
        
        
        // PROCESS CAMPAIGN STATUS FILTER        
        $status_array = $this->Retrieve_Asset_Status_ID_Array($query_options) ; 
                
        // Add visibility filter if there are visibility_id's to process
        $filter_by_asset_status_id_string = '' ; 
        
        if (count($status_array) > 0) {

            foreach ($status_array as $status) {
                
                $this_status_id = $status['asset_status_id'] ; 
                $filter_by_asset_status_id_string .= "asset_campaigns.campaign_status_id='$this_status_id' OR " ;               
                }
            
            $filter_by_asset_status_id_string = rtrim($filter_by_asset_status_id_string," OR ") ; 
            $query_array['where'] .= " AND ($filter_by_asset_status_id_string) " ;            
            }
        
        
        

        
        // Filter by tags
        if (isset($query_options->filter_by_tag_metadata_id)) { 
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_tag_metadata_id,","),",") ;
            $query_options->filter_by_tag_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;
            $count_metadata = count($query_options->filter_by_tag_metadata_id) ; 
                        
            $join_table_name = 'tag_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->tag_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='campaign' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (asset_campaigns.campaign_id=$join_table_name.item_id)"
                );                         
            }
        
        
        // Filter by categories
        if (isset($query_options->filter_by_category_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_category_metadata_id,","),",") ;
            $query_options->filter_by_category_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
            $count_metadata = count($query_options->filter_by_category_metadata_id) ; 
                        
            $join_table_name = 'category_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->category_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='campaign' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (asset_campaigns.campaign_id=$join_table_name.item_id)"
                );                         
            }        
        
        
        
        // PROCESS CAMPAIGN ARCHIVED FILTER
        if (isset($query_options->filter_by_campaign_archived_status)) {

            $archived_item_metadata_id = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'archived_item')['value'] ; 
            $count_metadata = 1 ; 
            
            
            $join_table_name = 'archived_item_query' ;  
            $query_array['fields'] .= ", $join_table_name.relationship_id AS archived_item, " ; 
            
            // Create the operator
            switch ($query_options->filter_by_campaign_archived_status) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'live':
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                case 'archived':    
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            


            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($archived_item_metadata_id) 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (asset_campaigns.campaign_id=$join_table_name.item_id)"
                ); 

            }
        
            
                            
        
        // Add filter by search parameter
        if (isset($query_options->filter_by_search_parameter)) {

            $query_array['where'] .= " AND 
                 (asset_campaigns.campaign_title LIKE '%$query_options->filter_by_search_parameter%'
                )" ;                       
            }
        
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        // Remove leading AND from where clause
        $query_array['where'] = ltrim($query_array['where']," AND ") ;
        
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
        $this->Set_Model_Timings('Asset.Retrieve_Campaign_List_paging_processed') ;

        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
//        $this->asset_query_result = $result ;         
        $this->asset_query_result2 = $result ; 
        $this->Set_Model_Timings('Asset.Retrieve_Campaign_List_results_pulled') ;
        
        $query_options->value_name = 'campaign_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 
        $this->Set_Model_Timings('Asset.Retrieve_Campaign_List_results_processed') ;
        
        
        $this->Set_Asset_Paging($result) ;                 
        $this->Set_Model_Timings('Asset.Retrieve_Campaign_List_end') ;
        
        return $result ; 
        }
    
    
    
    // Automation items (all should be associated with a parent campaign)
    public function Retrieve_Automation_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        

        $query_array = array(
            'table' => 'asset_campaign_automation',
            'fields' => "asset_campaign_automation.*, 
                asset_campaigns.*,
                asset_type.*,
                accounts.marketing_credit_balance, 
                delivery_asset.type_id AS delivery_asset_type_id, delivery_asset.visibility_id AS delivery_asset_visibility_id, 
                delivery_asset_type.type_name AS delivery_asset_type_name, 
                automation_status.status_name AS automation_status_name, automation_status.status_title AS automation_status_title, automation_status.status_description AS automation_status_description, 
                campaign_status.status_name AS campaign_status_name, campaign_status.status_title AS campaign_status_title, campaign_status.status_description AS campaign_status_description",
            'where' => "asset_campaign_automation.automation_id>0 "
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaigns',
            'on' => 'asset_campaigns.campaign_id',
            'match' => 'asset_campaign_automation.campaign_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'accounts',
            'on' => 'accounts.account_id',
            'match' => 'asset_campaigns.account_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_status AS automation_status',
            'on' => 'automation_status.asset_status_id',
            'match' => 'asset_campaign_automation.automation_status_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_status AS campaign_status',
            'on' => 'campaign_status.asset_status_id',
            'match' => 'asset_campaigns.campaign_status_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'assets',
            'on' => 'assets.asset_id',
            'match' => 'asset_campaigns.asset_id',
            'type' => 'left'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'assets as delivery_asset',
            'on' => 'delivery_asset.asset_id',
            'match' => 'asset_campaigns.delivery_asset_id',
            'type' => 'left'
            ); 
        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_type',
            'on' => 'asset_type.type_id',
            'match' => 'assets.type_id',
            'type' => 'left'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_type AS delivery_asset_type',
            'on' => 'delivery_asset_type.type_id',
            'match' => 'delivery_asset.type_id',
            'type' => 'left'
            ); 
        
        
        
       // Automation Asset Member Subquery
        $join_table_name = 'automation_member' ;  
        $query_array['fields'] .= ", 
            $join_table_name.member_id" ; 
            

        // Create the operator
        $join_type = 'left' ; 
        
        $query_array['join_tables'][] = array(
            'table' => "
                (SELECT asset_members.* 
                FROM asset_members 
                JOIN asset_campaign_automation ON asset_campaign_automation.automation_id = asset_members.automation_id 
                ) AS $join_table_name",
            'type' => $join_type,
            'statement' => "
                    (asset_campaigns.delivery_asset_id=$join_table_name.asset_id AND asset_campaign_automation.automation_id=$join_table_name.automation_id)"
            );        
        
        
        if(isset($query_options->timestamp_next_action_lower)) {
            $query_array['where'] .= " AND asset_campaign_automation.timestamp_next_action>='$query_options->timestamp_next_action_lower' "  ; 
            }
        
        if(isset($query_options->timestamp_next_action_upper)) {
            $query_array['where'] .= " AND asset_campaign_automation.timestamp_next_action<='$query_options->timestamp_next_action_upper' "  ; 
            }        
        
        if(isset($query_options->filter_by_campaign_id)) {
            $query_array['where'] .= " AND asset_campaign_automation.campaign_id='$query_options->filter_by_campaign_id' "  ; 
            }
        
        if(isset($query_options->filter_by_automation_id)) {
            $query_array['where'] .= " AND asset_campaign_automation.automation_id='$query_options->filter_by_automation_id' "  ; 
            }  
    
        if(isset($query_options->filter_by_campaign_position)) {
            $query_array['where'] .= " AND asset_campaign_automation.campaign_position='$query_options->filter_by_campaign_position' "  ; 
            }
        
        if(isset($query_options->limit_error_count)) {
            $query_array['where'] .= " AND asset_campaign_automation.error_count<='$query_options->limit_error_count' "  ; 
            }
        
        // Add automation status filter
        if(isset($query_options->filter_by_automation_status_id)) {  
            
            // This needs to be submitted as an array of type_id values.
            // If an array wasn't submitted, turn it into an array
            $automation_status_id_array = Utilities::Array_Force($query_options->filter_by_automation_status_id) ;             
           
            
            if (count($automation_status_id_array) > 0) {
                $filter_by_automation_status_id_string = '' ; 
                foreach ($automation_status_id_array as $automation_status_id) {
                    $filter_by_automation_status_id_string .= "asset_campaign_automation.automation_status_id='$automation_status_id' OR " ; 
                    }
                $filter_by_automation_status_id_string = rtrim($filter_by_automation_status_id_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_automation_status_id_string) " ;                
                }         
            }
        
        
        if (isset($query_options->filter_by_email_id)) {
            $query_array['fields'] .= ", emails.*"  ;
            $query_array['where'] .= " AND emails.email_id='$query_options->filter_by_email_id'"  ; 
            
            $query_array['join_tables'][] = array(
                'table' => 'emails',
                'on' => 'emails.automation_id',
                'match' => 'asset_campaign_automation.automation_id'
                ) ;             
            }
        
        if (isset($query_options->filter_by_cron_active)) {
            $query_array['where'] .= " AND asset_campaign_automation.cron_active='$query_options->filter_by_cron_active'" ;             
            }
        
        
 
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $query_options->value_name = 'automation_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        
        $this->Set_Asset_Paging($result) ;         
        $this->asset_query_result = $result ;        
        
        return $result ; 
        } 
    
    
    
    public function Update_Asset($parent_input,$content_input = array()) {

        if (isset($content_input['metadata_id'])) {
            $metadata_array = $content_input['metadata_id'] ; 
            unset($content_input['metadata_id']) ; 
            }
        
        
        if (count($content_input) > 0) {
            
            if ($this->asset['asset_id'] != $this->asset_id) {
                $this->Set_Asset('internal',array('condense_asset' => 'yes')) ;     
                }
            $prior_timestamp_updated = $this->asset['timestamp_asset_updated'] ; 

            
            $values_array = array() ; 
            foreach ($content_input as $key => $value) {
                $values_array[$key] = $value ; 
                }

            // Updates the asset content in the database
            $query_array = array(
                'table' => 'asset_content',
                'values' => $values_array,
                'where' => "asset_id='$this->asset_id'"
                );

            
            $asset_content_update = $this->DB->Query('UPDATE',$query_array) ;  
            }
        
        if (count($parent_input) > 0) {        

            // Updates the asset parent content in the database
            $parent_values_array = array() ; 
            foreach ($parent_input as $key => $value) {

                $parent_values_array[$key] = $value ; 
                }

            $parent_values_array['timestamp_asset_updated'] = TIMESTAMP ; 


            // Updates the asset content in the database
            $query_array = array(
                'table' => 'assets',
                'values' => $parent_values_array,
                'where' => "asset_id='$this->asset_id'"
                );

            $asset_master_update = $this->DB->Query('UPDATE',$query_array);
            }
        
        
        
        // Add general metadata relationship...
        if ((isset($metadata_array)) AND (count($metadata_array) > 0)) {
            foreach ($metadata_array as $metadata_id) {
                
                $metadata_query = array(
                    'asset_id' => $this->asset_id,
                    'metadata_id' => $metadata_id
                    ) ; 
                $metadata_update = $this->Action_Create_Asset_Metadata_Relationship($metadata_query) ; 
                
                }
            }
        
        
        
        
        $result = array(
            'asset_master_update' => $asset_master_update,
            'asset_content_update' => $asset_content_update
            ) ; 
        
        $this->Set_Model_Timings('Asset.Update_Asset_complete_'.$this->asset_id) ; 
        
        $this->asset_query_result = $result ;
        return $result ; 
        }
    
    
    
    // asset_master and asset_structure will be returned as arrays
    public function Action_Process_Asset_History($asset_history) {
        

        $asset_history['asset_master'] = json_decode($asset_history['asset_master'],1) ;
        $asset_history['asset_structure'] = json_decode($asset_history['asset_structure'],1) ;
        
        
        $asset_master = array() ; 
        $asset_content = array() ; 
        $asset_structure = array() ; 
        $asset_info = array() ; 
        
        $asset_info['history_id'] = $asset_history['history_id'] ; 
        $asset_info['microtimestamp_asset_history_updated'] = $asset_history['microtimestamp_asset_history_updated'] ; 
        
        if (isset($asset_history['asset_master'])) {
            foreach ($asset_history['asset_master'] as $master_key => $master_value) {
                $asset_master[$master_key] = $master_value ; 
                }
            }
        
        $save_history_structure = $asset_history['asset_structure'] ;
        
        if (isset($asset_history['asset_structure'])) {
            
            if (count($asset_history['asset_structure']) > 0) {
                
                $asset_history['asset_structure'] = (new \Chmiello\ArraySortPackage\ArraySort($asset_history['asset_structure']))->asc('structure_order')->sort()->getItems();
                $asset_structure = $asset_history['asset_structure'] ; 
                
                $temp_structure_table = str_replace(".","",'temp_asset_structure_table_'.microtime(true)) ; 
        
                $fields_array[] = 'temp_structure_id INT NOT NULL PRIMARY KEY' ;
                $fields_array[] = 'asset_id INT(10) NOT NULL' ;
                $fields_array[] = 'child_asset_id INT(10) NOT NULL' ;
                $fields_array[] = 'structure_order DECIMAL(5,2) NOT NULL' ;
                $fields_array[] = 'delay INT(20) NOT NULL' ;

                $import_fields_array = array() ;
                foreach ($fields_array as $field_name) {
                    $import_fields_array[] = ltrim(explode(" ",$field_name)[0],"(") ;
                    }
                
                $create_table_array = array(
                    'new_table' => $temp_structure_table,
                    'fields_array' => $fields_array
                    ) ; 

                $create_table = $this->DB->Query('CREATE_TABLE',$create_table_array) ;

                $add_primary_statement = 'ALTER TABLE '.$temp_structure_table.' MODIFY temp_structure_id INT NOT NULL AUTO_INCREMENT' ;
                $add_primary = $this->DB->_connection->query($add_primary_statement) ;
                    
                // CREATE THE TEMP TABLE...      
                $import_values_array = array() ;         
                $i = 0 ; 
                foreach ($asset_history['asset_structure'] as $structure_result) {

                    $import_row = array() ; 
                    foreach ($import_fields_array as $field_key) {
                        $import_row[$field_key] = $structure_result[$field_key] ; 
                        }

                    if (!$import_row['asset_id']) {
                        $import_row['asset_id'] = $this->asset_id ;
                        }
                    
                    
                    $import_values_array[] = $import_row ;             
                    $i++ ; 
                    }

                $import_query_array = array(
                    'table' => $temp_structure_table,
                    'fields' => $import_fields_array,
                    'values' => $import_values_array
                    ) ; 

                $result_temp_table = $this->DB->Query('BULK_INSERT',$import_query_array); 
                
                $structure_options['structure_table'] = $temp_structure_table ; 
                $asset_structure_results = $this->Retrieve_Asset_Structure($structure_options) ; 
                $asset_structure = $asset_structure_results['results'] ;
                
                // Drop the temp table
                $drop_table_query_array = array(
                    'table' => $temp_structure_table
                    ) ;
                
                $drop_table_result = $this->DB->Query('DROP_TABLE',$drop_table_query_array) ;          
                    
                              
                }
            }

        unset($asset_history['asset_master'],$asset_history['asset_structure'],$asset_history['history_id'],$asset_history['asset_id'],$asset_history['microtimestamp_asset_history_updated']) ; 


        
        foreach ($asset_history as $content_key => $content_value) {
            $asset_content[$content_key] = $content_value ; 
            }
        
        $visibility_query_options['filter_by_visibility_id'] = $asset_master['visibility_id'] ; 
        $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
        $asset_info['visibility_name'] = $visibility_result['visibility_name'] ; // Latest visibility
        
//        $asset_info['structure_query'] = $asset_structure_results ; 
        
//        $asset_master = $this->Action_Time_Territorialize_Dataset($asset_master) ;
//        $asset_content = $this->Action_Time_Territorialize_Dataset($asset_content) ; // This isn't working for some reason
        $asset_info = $this->Action_Time_Territorialize_Dataset($asset_info) ;
        
        $result = array(
            'asset_master' => $asset_master,
            'asset_content' => $asset_content,
            'asset_structure' => $asset_structure,
            'asset_info' => $asset_info
            ) ; 
        
    
        return $result ; 
        }
    
    
    
    
    // Update asset history will copy the LATEST asset history, and then parse in new inputs from $history_input_array
    // Based on the incoming timestamp, relative to the latest history entry, it will either update the latest
    // or create a new entry.  
    // ** Also, publishing an asset should force a new entry
    public function Update_Asset_History($asset_id = 'internal',$history_input_array,$prior_timestamp_updated_input = 'none') {  
 
        $force_history_insert = 0 ; // By default, do not force a new history entry
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            }
            
        if ($this->asset['asset_id'] != $asset_id) {
            $additional_parameters['condense_asset'] = 'yes' ; // This is needed so we don't max out the database query.
            $this->Set_Asset_By_ID($asset_id,$additional_parameters) ; // Current core    
            }
        
        $asset_record = $this->Get_Asset() ; 
        $latest_history_id = $asset_record['latest_history_id'] ; 
        $current_history_id = $asset_record['current_history_id'] ;
        
        if ('none' === $prior_timestamp_updated_input) {
            $prior_timestamp_updated = $asset_record['timestamp_asset_updated'] ; 
            } else {
                $prior_timestamp_updated = $prior_timestamp_updated_input ; 
                }
        
        
        // If no visibility submitted to history, then create a default
        if ((!isset($history_input_array['visibility_id'])) AND (!isset($history_input_array['visibility_name']))) {
            
            switch ($asset_record['visibility_name']) {
                case 'visibility_hidden':  // If latest history visibility is hidden or deleted, then maintain it -- don't upgrade to draft
                case 'visibility_deleted': 
                    
                    $visibility_query_options['filter_by_visibility_name'] = $asset_record['visibility_name'] ; 
                    $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
                    $history_input_array['visibility_id'] = $visibility_result['visibility_id'] ;  
                    
                    break ; 
                default:
                    // Set the new visibility as draft for the restored version
                    $visibility_query_options['filter_by_visibility_name'] = 'visibility_draft' ; 
                    $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
                    $history_input_array['visibility_id'] = $visibility_result['visibility_id'] ;                    
                }
            }
        
        
        $history_input_array = $this->Action_Process_Inputs($history_input_array,$asset_record['map_array']) ;         
        $latest_history = $this->Get_Asset_History() ; // Get the latest version of the asset; pulled in Action_Process_Inputs
         

        // If new history visibility is different than latest history, force history insert
        if ($latest_history['asset_master']['visibility_id'] != $history_input_array['asset_master_input']['visibility_id']) {
            $force_history_insert = 1 ; 
            }
            
        // Increment based insert
        if (($force_history_insert == 0) AND ($history_input_array['asset_input_info']['rewrite_latest_history'] != 'yes') AND ($prior_timestamp_updated < Utilities::System_Time_Manipulate($this->version_history_increment.' s','-')['time_manipulated'])) {
            $force_history_insert = 1 ; 
            }
        
        // Need history insert since one doesn't currently exist
        if (($force_history_insert == 0) AND ($asset_record['current_history_id'] == 0)) {
            $force_history_insert = 1 ; 
            }
        
        if ($history_input_array['asset_input_info']['force_history_insert'] == 'yes') {
            $force_history_insert = 1 ; 
            unset($history_input_array['asset_input_info']['force_history_insert']) ; 
            }        
        
        // Require current timestamp to be set as the published date if Immediate chosen as the publish date
        switch ($history_input_array['asset_input_info']['visibility_name']) {
            case 'visibility_shared':
            case 'visibility_published': 
                if (((!$history_input_array['asset_master_input']['timestamp_asset_published']) OR ($history_input_array['asset_master_input']['timestamp_asset_published'] == 0)) AND ((!$latest_history['asset_master']['timestamp_asset_published']) OR ($latest_history['asset_master']['timestamp_asset_published'] == 0))) {
                    $history_input_array['asset_master_input']['timestamp_asset_published'] = TIMESTAMP ; 
                    }                
                break ;            
            }
 

//        if (isset($history_input_array['asset_input_info']['visibility_name'])) {
//            $history_input_array['asset_master_input']['visibility_name'] = $history_input_array['asset_input_info']['visibility_name'] ;
//            }
        
        // If the last time the asset was updated was more than $this->version_history_increment seconds ago,
        // or if the current_history_id is 0, then create a new asset history entry
        // Otherwise, update the current asset history entry
        if ($force_history_insert == 1) {
            

                unset($asset_content['results']['content_id']) ; 

                // Create the history input
                $content_history_array = array() ; 
                $content_history_array['asset_id'] = $asset_id ;
                $content_history_array['microtimestamp_asset_history_updated'] = microtime(true) ;


                // Capture the previous asset content and overwrite with NEW asset content            
                foreach ($latest_history['asset_content'] as $key => $value) { // Previous asset content
                    $content_history_array[$key] = $value ;
                    }
                foreach ($history_input_array['asset_content_input'] as $key => $value) { // NEW asset content
                    $content_history_array[$key] = $value ;
                    }
            
                // Capture the previous asset master and overwrite with NEW asset master
                foreach ($latest_history['asset_master'] as $key => $value) { // Previous asset master
                    $master_history_array[$key] = $value ;
                    }
                foreach ($history_input_array['asset_master_input'] as $key => $value) { // NEW asset master
                    $master_history_array[$key] = $value ;
                    }            
                $content_history_array['asset_master'] = json_encode($master_history_array) ; 
            

            
                // Capture the previous asset structure and overwrite with NEW asset structure if submitted
                foreach ($latest_history['asset_structure'] as $key => $value) { // Previous asset master
                    $asset_structure_array[$key] = $value ;                    
                    }                            
                if (isset($history_input_array['asset_structure_input'])) {
                    $asset_structure_array = $history_input_array['asset_structure_input'] ;                
                    } 
                
            
                $content_history_array['asset_structure'] = json_encode($asset_structure_array) ; 
            

            
                $query_array = array(
                    'table' => 'asset_content_history',
                    'values' => $content_history_array
                    );

                $result = $this->DB->Query('INSERT',$query_array) ;            
                $history_id = $result['insert_id'] ;
            
                if (!isset($result['error'])) {
                    $this->Set_Alert_Response(206) ; // Asset version history saved
                    } else {
                        $this->Set_Alert_Response(207) ; // Asset version could not be saved
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($result),
                            'location' => __METHOD__
                            )) ;            
                        }            
            
            
            // I believe we deprecated this switch...
//            } elseif ($prior_timestamp_updated == TIMESTAMP) {
        
                // This ensures Create_Asset_History doesn't unnecessarily update the history event after it has just been created
            
            
            } else {  // We are not forcing a history insert

            
                // Create the history input
                $content_history_array = array() ; 

                // Capture the previous asset content and overwrite with NEW asset content            
                foreach ($latest_history['asset_content'] as $key => $value) { // Previous asset content
                    $content_history_array[$key] = $value ;
                    }
                foreach ($history_input_array['asset_content_input'] as $key => $value) { // NEW asset content
                    $content_history_array[$key] = $value ;
                    }
            
                // Capture the previous asset master and overwrite with NEW asset master
                foreach ($latest_history['asset_master'] as $key => $value) { // Previous asset master
                    $master_history_array[$key] = $value ;
                    }
                foreach ($history_input_array['asset_master_input'] as $key => $value) { // NEW asset master
                    $master_history_array[$key] = $value ;
                    }            
                $content_history_array['asset_master'] = json_encode($master_history_array) ; 
            
            
            
                // Capture the previous asset structure and overwrite with NEW asset structure if submitted
                foreach ($latest_history['asset_structure'] as $key => $value) { // Previous asset master
                    $asset_structure_array[$key] = $value ;                    
                    }                            
                if (isset($history_input_array['asset_structure_input'])) {
                    $asset_structure_array = $history_input_array['asset_structure_input'] ;                
                    } 
                $content_history_array['asset_structure'] = json_encode($asset_structure_array) ;            


            
                $content_history_array['asset_id'] = $asset_id ;
                $content_history_array['microtimestamp_asset_history_updated'] = microtime(true) ;
            
                $query_array = array(
                    'table' => 'asset_content_history',
                    'values' => $content_history_array,
                    'where' => "asset_content_history.history_id='$latest_history_id' AND asset_content_history.asset_id='$asset_id'"
                    );

            
            
                $result = $this->DB->Query('UPDATE',$query_array) ;
                if (!isset($result['error'])) {
                    $this->Set_Alert_Response(206) ; // Asset version history saved
                    } else {
                        $this->Set_Alert_Response(207) ; // Asset version could not be saved
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($result),
                            'location' => __METHOD__
                            )) ;            
                        }
            
            
                $history_id = $latest_history_id ;
                } 
        
        
        $result['history_id'] = $history_id ; 
        $result['history_visibility_id'] = $master_history_array['visibility_id'] ; 
        return $result ;         
        }
    
    
    
    
    public function Update_Automation($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $query_array = array(
            'table' => 'asset_campaign_automation',
            'values' => $values_array,
            'where' => "asset_campaign_automation.automation_id='$this->automation_id'"
            );            
            
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->asset_query_result = $result ; 
        
        return $result ;         
        }
    
    
    
    public function Update_Campaign($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
            
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
        
        $values_array['timestamp_campaign_updated'] = TIMESTAMP ; 
        
        $query_array = array(
            'table' => 'asset_campaigns',
            'values' => $values_array,
            'where' => "asset_campaigns.campaign_id='$this->campaign_id'"
            );            
            
        $result = $this->DB->Query('UPDATE',$query_array) ;             
        $this->asset_query_result = $result ; 
        
        return $result ;         
        }
    
    
    
    
    
    public function Delete_Asset($asset_id = 'internal') {
        
        if ('internal' === $asset_id) {
            $asset_id = $this->asset_id ; 
            } 
        
        
         
        
        $query_array = array(
            'table' => "assets",
            'where' => "assets.asset_id='$asset_id'"
            );
        
        $result = $this->DB->Query('DELETE',$query_array);
        $this->asset_query_result = $result ;
        
        $query_array = array(
            'table' => "asset_auth",
            'where' => "asset_auth.asset_id='$asset_id'"
            );
        
        $result = $this->DB->Query('DELETE',$query_array);
        
        $query_array = array(
            'table' => "asset_content",
            'where' => "asset_content.asset_id='$asset_id'"
            );
        
        $result = $this->DB->Query('DELETE',$query_array);
            
        return $result ;       
         
        
        }    

    
    
    public function Delete_Asset_Metadata_Relationship($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        $query_array = array(
            'table' => "asset_metadata_relationships"
            );

        
        if ($query_options->relationship_id) {
            
            $query_array['where'] = "asset_metadata_relationships.relationship_id='$query_options->relationship_id'" ;
            $result = $this->Delete_Asset_Metadata_Relationship($query_array) ;     
            
            } elseif ($query_options->asset_id AND $query_options->metadata_id) {
            
                $query_array['where'] = "asset_metadata_relationships.asset_id='$query_options->asset_id' AND asset_metadata_relationships.metadata_id='$query_options->metadata_id'" ; 
                $result = $this->Delete_Asset_Metadata_Relationship($query_array) ;            
                }        
        

        $result = $this->DB->Query('DELETE',$query_array);
        $this->asset_query_result = $result ;
            
        return $result ;       
         
        
        }    
    
    }


 