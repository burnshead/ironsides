<?php

namespace Ragnar\Ironsides ;

class Vendor extends Account {
    
    public $vendor_id ;
    public $vendor ;
    public $vendor_list ;
    
    public $vendor_keys ; 
    
    public $authorization_id ;
    public $vendor_authorization ;
    
    
    public $vendor_query_result ;
    
    
    public function __construct($user_id = 'ignore') {

        global $DB ;  
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            }         
        }
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    
    
    public function Set_Vendor_ID($vendor_id) {
        
        $this->vendor_id = $vendor_id ; 
        return $this ; 
        }
    
    
    public function Set_Vendor_By_ID($vendor_id = 'internal',$query_options = array()) {
        
        if ('internal' === $vendor_id) {
            $vendor_id = $this->vendor_id ; 
            } else {
                $this->vendor_id = $vendor_id ; 
                }

        
        $this->vendor_id = $vendor_id ; 
        $this->Set_Vendor($query_options) ; 
        return $this ; 
        }
    
    
    // Set the vendor record
    // Default: Setting filter_by_account_id = yes will associate with a vendor authorization id
    // Set filter_by_account_id = no to pull the system vendor
    public function Set_Vendor($query_options = array()) {
        
        if (!isset($query_options['filter_by_account_id'])) {
            $query_options['filter_by_account_id'] = 'yes' ; 
            }
                
        $result = $this->Retrieve_Vendor($query_options) ; 
        
        if ($result['result_count'] > 0) {
            $this->vendor = $result['results'];     
            } else {
                $this->vendor = 'error' ;  
                }
        
        return $this ; 
        }
    
    public function Set_System_Vendor($query_options = array()) {
        
        $query_options['filter_by_account_id'] = 'no' ; 
        
        $this->Set_Vendor($query_options) ; 
        return $this ; 
        }    
    
    public function Set_Vendor_Keys($map_id = 'none') {
        
        $result = $this->Retrieve_Vendor_Keys($map_id) ; 
        $vendor_keys = $this->Action_Process_Vendor_Keys($result['results']) ; 
        $this->vendor_keys = $vendor_keys ;        
        return $this ;
        }  
    

    
    public function Set_System_Vendor_Keys($map_id = 'none') {
        
        $result = $this->Retrieve_System_Vendor_Keys($map_id) ; 
        
        $vendor_keys = $this->Action_Process_Vendor_Keys($result['results']) ; 
        $this->vendor_keys = $vendor_keys ;        
        return $this ;
        
        }    
    
    
    public function Set_Vendor_Authorization() {
        
        $result = $this->Retrieve_Vendor_Authorization() ; 
        
        if ($result['result_count'] > 0) {
            $this->vendor_authorization = $result['results'] ; 
            } else {
                $this->vendor = 'error' ;  
                }
        
        return $this ; 
        }
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
  
    public function Get_Vendor() {
        
        return $this->vendor ;
        
        } 
    
    public function Get_Vendor_Keys() {
        
        return $this->vendor_keys ;
        
        }    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    
    
    
    // 
    public function Action_Toggle_Vendor_Authorization($status, $parameters_array) {
        
        // $status options: connected, disconnected, expired, error
        
        $auth_record = $this->Create_Vendor_Authorization() ;
        if ($auth_record['insert_id']) {
            $this->authorization_id = $auth_record['insert_id'] ;
            } else {
                $this->authorization_id = $auth_record['results']['vendor_auth_id'] ;
                }
        
        $auth_update = $this->Update_Vendor_Authorization($status,$parameters_array) ; 
        
        return $this ; 
        }
    
    
    
    
    // Takes an array of keys from the database and places them in an array with the key as the key_name
    // Allows us to call a key simply by it's name
    // Converts to an object 
    public function Action_Process_Vendor_Keys($key_set) {
        
        $vendor_keys = array() ; 
        foreach ($key_set as $set) {
            $vendor_keys[$set['key_name']] = $set['key_value'] ; 
            }
        
        $vendor_keys = (object) $vendor_keys ; 
        
        return $vendor_keys ; 
        }
    
    
    public function Action_Test_Connection() {
        
        return $this->vendor_keys ;
        
        }    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    ////////////////////// 
    
    
    public function Create_Vendor_Authorization() {
        
         
        
        $query_array = array(
            'table' => "vendor_auth",
            'values' => array(
                'account_id' => $this->account_id,
                'vendor_id' => $this->vendor_id
                ),
            'where' => "vendor_auth.account_id='$this->account_id' AND vendor_auth.vendor_id='$this->vendor_id'"
            );       
        
        $auth_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
        // $this->vendor_query_result = $auth_record ; 
        
        return $auth_record ;         
        
        }
    
    
    public function Update_Vendor_Authorization($connection_status,$parameters_array = array()) {
        
         
        
        $query_array = array(
            'table' => "vendor_auth",
            'values' => array(),
            'where' => "vendor_auth.vendor_auth_id='$this->authorization_id'"
            );

        $query_array['values']['connection_status'] = $connection_status ;
        
        if (isset($parameters_array['token'])) {
            $query_array['values']['token'] = $parameters_array['token'] ; 
            }
        if (isset($parameters_array['refresh_token'])) {
            $query_array['values']['refresh_token'] = $parameters_array['refresh_token'] ; 
            }        
        if (isset($parameters_array['expiration_date'])) {
            $query_array['values']['expiration_date'] = $parameters_array['expiration_date'] ; 
            } 
        
        switch ($connection_status) {
            case 'connected':
                $query_array['values']['connection_date'] = TIMESTAMP ;
                break ;
            case 'disconnected':
                $query_array['values']['connection_date'] = 0 ;
                break ;
            }
        
        
        $auth_record = $this->DB->Query('UPDATE',$query_array) ;
        $this->vendor_query_result = $auth_record ; 
        
        return $auth_record ;         
        
        }
    
    
    
    public function Retrieve_Vendor($query_options = array()) {
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'vendors',
            'join_tables' => array(),
            'fields' => "vendors.*, ",
            'where' => "vendors.vendor_id='$this->vendor_id'"
            );
        


        // Determine whether or not results should be restricted to a specific account_id
        // Default is to restrict to $this->account_id
        switch ($query_options->filter_by_account_id) {
            case 'yes':
                
                $query_array['join_tables'][] = array(
                    'table' => 'vendor_auth',
                    'on' => 'vendors.vendor_id',
                    'match' => 'vendor_auth.vendor_id',
                    'type' => 'left'
                    ); 

                $query_array['join_tables'][] = array(
                    'table' => 'accounts',
                    'on' => 'accounts.account_id',
                    'match' => 'vendor_auth.account_id',
                    'type' => 'left'
                    );
                
                $query_array['fields'] .= "vendor_auth.vendor_auth_id, vendor_auth.connection_status, vendor_auth.token, vendor_auth.refresh_token, vendor_auth.connection_date, vendor_auth.expiration_date, " ;
                $query_array['fields'] .= "accounts.account_id, accounts.account_display_name, accounts.account_username, " ; 
                
                $query_array['where'] .= " AND vendor_auth.account_id='$this->account_id' " ;         
        
            break ;
            }
        
        // Clean up the query
        $query_array['fields'] = rtrim($query_array['fields'],", ") ;
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array);
        $this->vendor_query_result = $result ; 
        
        return $result ; 
        }
    
    
    
    // Retrieves vendor keys associated w/ account
    public function Retrieve_Vendor_Keys($map_id = 'none') {
        
        $query_array = array(
            'table' => 'vendor_keys',
            'join_tables' => array(),
            'fields' => "vendor_keys.*, vendors.vendor_name, vendors.description, vendor_keys_map.state, vendor_keys_map.key_name, vendor_keys_map.key_public_name",
            'where' => "vendor_auth.vendor_id='$this->vendor_id' AND vendor_auth.account_id='$this->account_id' "
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'vendor_auth',
            'on' => 'vendor_keys.vendor_auth_id',
            'match' => 'vendor_auth.vendor_auth_id'
            ); 
        
        $query_array['join_tables'][] = array(
            'table' => 'vendor_keys_map',
            'on' => 'vendor_keys_map.vendor_map_id',
            'match' => 'vendor_keys.map_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'vendors',
            'on' => 'vendors.vendor_id',
            'match' => 'vendor_auth.vendor_id'
            );
        
        if ('none' === $map_id) {
            
            } else {
                $query_array['where'] .=  "AND vendor_keys.map_id='$map_id'" ;
            
                } 
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->vendor_query_result = $result ; 
        
        return $result ;         
        
        }
    
    
    
    public function Retrieve_System_Vendor_Keys($key_name = 'none') {
        
         
        
        $query_array = array(
            'table' => 'system_keys',
            'join_tables' => array(),
            'fields' => "system_keys.*, vendors.vendor_name, vendors.description",
            'where' => "system_keys.vendor_id='$this->vendor_id' "
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'vendors',
            'on' => 'vendors.vendor_id',
            'match' => 'system_keys.vendor_id'
            ); 
        
        if ('none' === $key_name) {
            
            } else {
                $query_array['where'] .=  "AND system_keys.key_name='$key_name'" ;
            
                } 
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->vendor_query_result = $result ; 
        
        return $result ;         
        
        }    
    
    public function Retrieve_Vendor_Authorization($key_name = 'none') {
        
         
        
        $query_array = array(
            'table' => 'system_keys',
            'join_tables' => array(),
            'fields' => "system_keys.*, vendors.vendor_name, vendors.description",
            'where' => "system_keys.vendor_id='$this->vendor_id' "
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'vendors',
            'on' => 'vendors.vendor_id',
            'match' => 'system_keys.vendor_id'
            ); 
        
        if ('none' === $key_name) {
            
            } else {
                $query_array['where'] .=  "AND system_keys.key_name='$key_name'" ;
            
                } 
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->vendor_query_result = $result ; 
        
        return $result ;         
        
        }
    
    
    }


