<?php

namespace Ragnar\Ironsides ;

class Alert extends Vendor {
    
	private $logs = array();
    public $status ; // user or app : user would use the account's own mail service account; app uses the application account

    // ALERT SERVICE CONNECTION
    public $_connection ;
    public $_connection_status ;
    public $alert_api_event ; // API
    public $alert_query_result ; // Database
    
    
    
    public function __construct($user_id = 'ignore',$account_id = 'ignore') {
        
        global $DB ;  
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            } 
        
        if ('ignore' !== $account_id) {
            $this->Set_Account_By_ID($account_id) ; 
            $this->Set_Sending_Domain($this->account['email_domain']) ; 
            }
	   }
    
  
    // Instantiate alert delivery service
    public function Connect($status = 'app',$vendor_id = 7,$options = array()) {
        
        $this->vendor_id = $vendor_id ;
        
        if ('app' === $status) {

            $this->Set_System_Vendor($this->vendor_id)->Set_System_Vendor_Keys() ; // Get system API keys
            $this->status = $status ; 

            } else {
            
                // Gets an account's API key for the delivery service
                $this->Set_Account($account_id,false) ;  
            
                $vendor = new Vendor() ; 
                $vendor->Set_Account_By_ID($account_id)->Set_Vendor($this->vendor_id) ;  // Assuming Mailgun
                $vendor_key = $vendor->Get_Vendor_Key(6) ;  // Assuming Mailgun API Key
                $vendor_api_key = $vendor_key['key_value'] ; 
                
                $this->status = 'user' ; 
//                $this->email_domains_table = 'email_domains' ; 
            
                }     
        
        
        switch ($this->vendor_id) {
            case 7: // Pusher
                
                if (!isset($options['cluster'])) {
                    $options['cluster'] = $this->vendor_keys->Pusher_API_Cluster ; 
                    }
                if (!isset($options['useTLS'])) {
                    $options['useTLS'] = true ; 
                    }
                if (!isset($options['debug'])) {
                    $options['debug'] = true ; 
                    }                
                

                try {
                    $this->_connection = new Pusher\Pusher(
                        $this->vendor_keys->Pusher_API_Key, 
                        $this->vendor_keys->Pusher_API_Secret,
                        $this->vendor_keys->Pusher_App_ID,
                        $options
                      );
                    

                    } catch ( Exception $e ) {
                        $error = $e->getMessage() ;  
                        $this->_connection_status = $error ; 
                        return $error ;  
                        } 
                    catch ( Error $e ) {
                        $error = $e->getMessage() ;  
                        $this->_connection_status = $error ; 
                        }                

                break ;                
            }                
        
        return $this ; 
        }    
    
    
    
    
    public function API_Action_Trigger($data = array()) {
            
            try {
                # Make the call to the MailGun client.

                if (!isset($options['cluster'])) {
                    $options['cluster'] = $this->vendor_keys->Pusher_API_Cluster ; 
                    }
                if (!isset($options['useTLS'])) {
                    $options['useTLS'] = true ; 
                    }
                if (!isset($options['debug'])) {
                    $options['debug'] = true ; 
                    }                
                
                
                $this->alert_api_event = $this->_connection->trigger($data['channel_name'],$data['event_name'],$data['message']);                    
                
            
                }
        
                // Catch Exception 
                catch(Exception $e) {

                    
                    }        
        
        return $this ; 
        }

    
    public function Get_Alert_API_Event() {
        
        return $this->alert_api_event ; 
        }
    
    
    
    }