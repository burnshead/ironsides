<?php

namespace Ragnar\Ironsides ;

class Css extends Email {
    
    public $inlined_array ;
    public $css_array ;
    
    
    
    
    public function __construct($user_id = 'ignore') {
        
        global $DB ;   
        $this->DB = $DB ;

        }
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    
    function Set_Css_Array($css_array) {
        $this->css_array = $css_array ;         
        return ; 
        }
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    // Get the Inlined array
    function Get_Inlined_Array() {
        return $this->inlined_array ;         
        }
    
    function Get_Css_Array() {
        return $this->css_array ;         
        }    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    //////////////////////
    
    
    // 
    function Action_Inline($html_string,$inline_options = array()) {
        // https://github.com/crossjoin/PreMailer
        
        // PROCESS
        // 1. Extracts CSS and Responsive CSS from <head> <link> references
        // 2. Inserts CSS content string (minus responsive queries) into <head> <style>
        // 3. Inlines the HTML doc and removes <head> <style>
        // 4. Inserts CSS responsive queries into the <head> <style> tag
        // 5. Returns HTML and Text versions of the doc
        
        // Codes:
        // 201: Success
        // 901: CSS read error
        
        $this->Set_Alert_Response(129) ; // CSS read / write success
        $errors = array() ;
        
        $original_html = $html_string ; // For QA and to compare to original with the final

        $html = str_replace('&nbsp;','*|HTML_PRESERVE_SPACE|*',$html_string) ; // Protects the $nbsp; HTML character. To be reinserted at end of method.

        // Parse the CSS files of this html string
        // Returns array of CSS content, CSS files, and responsive queries
        // CSS content comes back with responsive queries extracted
        if ($inline_options['css_source'] == 'internal') {
            $css = $this->Get_Css_Array() ; // Use the already parsed CSS stored in the CSS model
            } else {
                $css = $this->Action_Parse_Css($html)->Get_Css_Array() ; // Parse THIS html string and extract the css
                }
        
        
        // Combines all of the extracted CSS contents into a single string
        $final_css = '' ; 
        foreach ($css['file_contents'] as $css_content) {
            $final_css .= $css_content ; 
            }
        

        // Intention is to process CSS external files. This isn't working for exterrnal files right now, so don't use.        
//        foreach ($css['external_files'] as $css_file) {
//            try {
//                $reader = new \Crossjoin\Css\Reader\CssFile($css_file);
//                $writer = new \Crossjoin\Css\Writer\Compact($reader->getStyleSheet());
//                $css_content = $writer->getContent();
//                }
//                catch(Exception $e) {
//                    $status = 901 ; 
//                    $errors[] = ' { CSS Read Error: [901] '.$e->getMessage().' } ' ;
//                }
//            $final_css .= $final_css.$css_content ; 
//            }        


        // Inserts the full CSS contents (minus responsive queries) back into the template within a <style> block
        $css_placeholder = '/* PLACE_CSS_HERE */' ; 
        
        // Check to see if the css placeholder exists in the HTML already. if it does not, add it.
        if(strpos($html, $css_placeholder) !== false){
            $html = str_replace("/* PLACE_CSS_HERE */",$final_css,$html) ;
            } else{
                $html = '<style>'.$css_placeholder.'</style>'.$html ; 
                $html = str_replace("/* PLACE_CSS_HERE */",$final_css,$html) ;
                }        
        
        
        
        // Remove Outlook conditional statements temporarily
        $html = str_replace("<![if !mso]><!-- NOT OUTLOOK -->","<!-- OUTLOOK_CONDITIONAL Precedes Not Outlook -->",$html) ; 
        $html = str_replace("<!--<![endif]-->","<!-- OUTLOOK_CONDITIONAL Follows Not Outlook -->",$html) ; 
        $html = str_replace("<!--[if gte mso 9]>","<!-- OUTLOOK_CONDITIONAL Precedes Outlook -->",$html) ; 
        $html = str_replace("<![endif]-->","<!-- OUTLOOK_CONDITIONAL Follows Outlook -->",$html) ;    


        // Removes the CSS file URLs from the document
        $css_files_string = Utilities::Extract_Text_Block($html, '<link rel="stylesheet" href="begin.css" type="text/css">', '<link rel="stylesheet" href="end.css" type="text/css">'); // Gathers the string of <link> filenames
        $html = str_replace($css_files_string['extraction'],'',$html) ;  // Removes the file string
        $html = str_replace('<link rel="stylesheet" href="begin.css" type="text/css">','',$html) ;  // Removes the beginning of the <link> files set
        $html = str_replace('<link rel="stylesheet" href="end.css" type="text/css">','',$html) ; // Removes the end of the <link> files set

        
        // Begin inlining the HTML string using Premailer
        if (strlen($html) > 0) {
            try {
                libxml_use_internal_errors(true);
                $premailer = new \Crossjoin\PreMailer\HtmlString($html);
                }
                catch(Exception $e) {
                    
                    $status = 902 ; 
                    $errors[] = ' { HTML Get Error: '.$e->getMessage().' } ' ;
                    
                    $this->Set_Alert_Response(130) ; // Error: CSS read / write
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($errors),
                        'location' => __METHOD__
                        )) ;                    
                    }        



            
            // Location of the style tag
            switch ($inline_options['css_style_tag_location']) {
                case 'head':                
                    // Move the style tag to the head of the HTML document
                    $premailer->setOption($premailer::OPTION_STYLE_TAG, $premailer::OPTION_STYLE_TAG_HEAD);
                    break ; 
                case 'body':
                default:
                    // Move the style tag to the body of the HTML document (default)
                    $premailer->setOption($premailer::OPTION_STYLE_TAG, $premailer::OPTION_STYLE_TAG_BODY);           
                }
            
            
            
            // Move the style tag to the head of the HTML document
            switch ($inline_options['css_remove_style_tag']) {
                case 'keep':                
                    // Do not remove style tag
                    
                    break ; 
                case 'remove':
                default:
                    // Keep HTML class attributes (default)
                    $premailer->setOption($premailer::OPTION_STYLE_TAG, $premailer::OPTION_STYLE_TAG_REMOVE);            
                }
            
            
            
            // Keep HTML comments
            $premailer->setOption($premailer::OPTION_HTML_COMMENTS, $premailer::OPTION_HTML_COMMENTS_KEEP);
            
            
            // Keep HTML comments
            switch ($inline_options['css_classes']) {
                case 'remove':
                    // Remove HTML class attributes
                    $premailer->setOption($premailer::OPTION_HTML_CLASSES, $premailer::OPTION_HTML_CLASSES_REMOVE);
                    break ; 
                default:
                    // Keep HTML class attributes (default)
                    $premailer->setOption($premailer::OPTION_HTML_CLASSES, $premailer::OPTION_HTML_CLASSES_KEEP);
                }
            
        
            
            
            // Get the HTML version of the string
            $inlined_html = $premailer->getHtml();

            // Get the text version of the string
            $text = $premailer->getText();
            // FUTURE: Consider adding something here that removes duplicate carriage returns / line spaces from the text version

            // Restore Outlook conditional statements
            $inlined_html = str_replace("<!-- OUTLOOK_CONDITIONAL Precedes Not Outlook -->","<![if !mso]><!-- NOT OUTLOOK -->",$inlined_html) ; 
            $inlined_html = str_replace("<!-- OUTLOOK_CONDITIONAL Follows Not Outlook -->","<!--<![endif]-->",$inlined_html) ; 
            $inlined_html = str_replace("<!-- OUTLOOK_CONDITIONAL Precedes Outlook -->","<!--[if gte mso 9]>",$inlined_html) ; 
            $inlined_html = str_replace("<!-- OUTLOOK_CONDITIONAL Follows Outlook -->","<![endif]-->",$inlined_html) ;   

            } else {
            
                $status = 903 ; 
                $errors[] = ' { HTML Content Error: Blank message supplied. } ' ; // { HTML: '.$html.' } { Original: '.$original_html. ' } ' ;
            
                $this->Set_Alert_Response(131) ; // Error: CSS read / write
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($errors),
                    'location' => __METHOD__
                    )) ;            
            
                }        
        
        
        // Insert Responsive Queries CSS into the template
        if ($css['responsive_queries']) {
            $inlined_html = str_replace('<link rel="stylesheet" href="placeholder_responsive.css" type="text/css">',"<style>".$css['responsive_queries']."</style>",$inlined_html) ; 
            }         

  
        
        // Return &nbsp; characters to the string
        $inlined_html = str_replace('*|HTML_PRESERVE_SPACE|*','&nbsp;',$inlined_html) ;
        $text = str_replace('*|HTML_PRESERVE_SPACE|*','&nbsp;',$text) ;

        $result = $this->Get_Response() ; 


        $result = array(
            'status' => $result, 
//            'css_string' => $final_css,
            'css' => $css,
            'email_html' => $inlined_html, 
            'email_text' => $text,
            'errors' => implode($errors),
            ) ; 

        $this->inlined_array = $result ;
        
        return $this ; 

    }
    
    
    function Action_Parse_Css($html_string) {

        // Takes an HTML string with CSS file references within <head> <link> and extracts
        // as an array of standard CSS content and responsive CSS content
        
         
        
        // Initializes output arrays and variables
        $responsive_queries = '' ; 
        $Phone_Media_Queries = '' ;
        $Tablet_Media_Queries = '' ; 
        
        $css_file_array = array() ;
        $css_file_contents_array = array() ;
        $css_external_file_array = array() ;
        $css_file_name_array = array() ;
        
        // Extract CSS file names
        $css_files = Utilities::Extract_Text_Block($html_string,'<link rel="stylesheet" href="begin.css" type="text/css">', '<link rel="stylesheet" href="end.css" type="text/css">');
        
        // Creats an arry of CSS URLs using the css_files string from above
        $raw_css_file_array = explode('<link rel="stylesheet" href="', str_replace('" type="text/css">','',$css_files['extraction'])); // Clean the css "link" string into an array of just file names ("href")
    
        // Processes and parses each CSS file UR
        // If the URL is formatted as internal/css_file_name.css?asset_id=###, then that signifies CSS content will come
        // from internal database.
        foreach ($raw_css_file_array as $url) {
            if (strlen($url) > 1) {
                $url = trim ( $url , " \t\n\r\0\x0B") ;   // Removes whitespaces from CSS file names.
                $css_file_array[] = $url ; // Includes http(s) location
                
                $parsed_url = System_Config::Config_Parse_URL($url) ; 
                
                if ($parsed_url['host'] == 'internal') {
                    $css_file_name_array[] = $parsed_url['queries']['asset_id'] ; // format internal/css_file_name.css?asset_id=###
                    } else {
                        $css_external_file_array[] = $parsed_url['url_full'] ; 
                        }           
                }
            }


        // Pulls CSS content from within the database
        foreach ($css_file_name_array as $asset_id) {

            $CSS = new Asset() ; 
            $css_asset = $CSS->Retrieve_Asset($asset_id) ;
            
            $css_string = $css_asset['results']['text_01'] ; 
            $css_file_contents_array[] = $css_string ;
            
            // Extracts responsive queries from this CSS block
            $Phone_Media_Queries .= ' '.Utilities::Extract_Text_Block($css_string, 'BEGIN_PHONE_QUERIES', 'END_PHONE_QUERIES')['extraction'];
            $Tablet_Media_Queries .= ' '.Utilities::Extract_Text_Block($css_string, 'BEGIN_TABLET_QUERIES', 'END_TABLET_QUERIES')['extraction'];
            }

        // Place responsives queries into an output string
        $responsive_queries .= $Phone_Media_Queries.' '.$Tablet_Media_Queries.' ' ;

        
        // Using files: This needs to be a server location (not https) 
        $Phone_Media_Queries = '' ;
        $Tablet_Media_Queries = '' ; 

        foreach ($css_external_file_array as $css_file) {
            try {
                $reader = new \Crossjoin\Css\Reader\CssFile($css_file);
                $writer = new \Crossjoin\Css\Writer\Compact($reader->getStyleSheet());
                $css_content = $writer->getContent();
                }
                catch(Exception $e) {
                    $status = 901 ; 
                    $error[] = ' { CSS Read Error: [901] '.$e->getMessage().' } ' ;
                } 

            $Phone_Media_Queries .= ' '.Utilities::Extract_Text_Block($css_content, 'BEGIN_PHONE_QUERIES', 'END_PHONE_QUERIES')['extraction'];
            $Tablet_Media_Queries .= ' '.Utilities::Extract_Text_Block($css_content, 'BEGIN_TABLET_QUERIES', 'END_TABLET_QUERIES')['extraction'];
            }

        $responsive_queries .= $Phone_Media_Queries.' '.$Tablet_Media_Queries ;


        $css = array(
            'files' => $css_file_array, 
            'file_names' => $css_file_name_array,
            'file_contents' => $css_file_contents_array,
            'external_files' => $css_external_file_array,
            'responsive_queries' => $responsive_queries
            ) ; 

        $this->css_array = $css ; 
        return $this ; 
    }    

}