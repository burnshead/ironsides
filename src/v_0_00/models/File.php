<?php

namespace Ragnar\Ironsides ;

class File extends Asset {
    
    public $file_task_status ; 
    
    public $file_info ; 
    
    private $file_location ;
    public $file_task_list ; 
    public $file_task ;
    public $file_task_id ;
    
    public $csv_keys ;
    public $csv_contents ;
    
    public $csv_processed_content ; 
    
    
    public $csv_transformed ;
    
    public $file_query_result ;
    
    public $file_paging ; 
    
    public $file_task_query ; 
    
    // FILE SERVICE CONNECTION
    public $_connection ;
    public $_connection_status ;    
    
    
    
    public function __construct($user_id = 'ignore') {

        global $DB ;
        $this->DB = $DB ;        
        
        if ('ignore' !== $user_id) {
            $this->Set_Admin_User_By_ID($user_id) ;
            $this->Set_User_By_ID($user_id) ;
            $this->Set_Master_User_By_ID($user_id) ; 
            }  
        
        }
    
    public function Connect($vendor_id = 5) {
        
        switch ($vendor_id) {              
            default:
 
                try {
                    $this->_connection = new Uploadcare\Api(UPLOADCARE_PUBLIC_KEY, UPLOADCARE_SECRET_KEY) ;
                    } catch ( Exception $e ) {
                        $error = $e->getMessage() ;  
                        $this->_connection_status = $error ; 
                        return $error ;  
                        } 
                    catch ( Error $e ) {
                        $error = $e->getMessage() ;  
                        $this->_connection_status = $error ; 
                        }                
            }
    

        return $this ; 
        }
    
    
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    // Set the file location to read
    public function Set_File_Location($file_location) {
        $this->file_location = $file_location ; 
        return $this ; 
        } 
    
    
    // Set the file task by file_task_id
    public function Set_File_Task_By_ID($file_task_id = 'internal') {
        
        $continue = 1 ; 
        
        if ('internal' === $file_task_id) {
            $file_task_id = $this->file_task_id ; 
            } else {
                $this->file_task_id = $file_task_id ; 
                }
        
        if ($this->file_task_id == 'new') {
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {
            
            $query_options = array(
                'filter_by_visibility_name' => 'admin',
                'filter_by_user_id' => 'no',
                'filter_by_account_id' => 'no',
                'filter_by_file_task_id' => $this->file_task_id
                ) ;        
            
            $file_task_list = $this->Set_File_Task_List($query_options)->Get_File_Task_List() ;
            }


        if ($file_task_list != 'error') {
            
            $this->file_task = $file_task_list[0] ; 
            
            
            
            // These are metadata entries that determine how imports will be mapped when processed (not a tag for the import itself)
            $metadata_query = array(
                'relationship_type' => 'file_task_map',
                'metadata_type' => 'tag,category',
                'allow_global_metadata' => 'yes',
                'visibility_name' => 'visible'
                ) ;  

            $this->file_task['tag_map'] = array() ;
            $this->file_task['category_map'] = array() ;
            
            $metadata_results = $this->Retrieve_Metadata_Relationship_List($this->file_task_id,$metadata_query) ; 
            if ($metadata_results['result_count'] > 0) {
                
                foreach ($metadata_results['results'] as $metadata_result) {
                    switch ($metadata_result['metadata_type']){
                        case 'tag':
                            $this->file_task['tag_map'][] = $metadata_result ; 
                            break ;
                        case 'category':
                            $this->file_task['category_map'][] = $metadata_result ; 
                            break ;                            
                        }
                    }
                }
            
            
            $data['contact_fields'] = $this->Get_System_List('list_contact_fields') ; 
            if (!isset($data['contact_fields'])) {
                $query_options = array(
                'override_paging' => 'yes',
                'active' => 1,
                'order_by' => 'relative_order',
                'order' => 'ASC'
                ) ; 
                $this->Set_System_List('list_contact_fields',$query_options) ; 
                $data['contact_fields'] = $this->Get_System_List('list_contact_fields') ; 
                }


            $this->file_task['columns'] = $data['contact_fields'] ;
            
            
            
            
            if ($this->file_task['asset_id'] > 0) {
                $this->file_task['asset_record'] = $this->Set_Asset_By_ID($this->file_task['asset_id'])->Get_Asset() ;    
                
                if (!isset($this->file_task['file_sample'])) {

                    $this->file_task['has_header'] = 'yes' ; 
                    $this->file_task['file_sample'] = $this->Action_Create_Csv_File_Sample($this->file_task['asset_record'],4) ;

                    $task_input['has_header'] = $this->file_task['has_header'] ; 
                    $task_input['file_sample'] = $this->file_task['file_sample'] ; 
                    $this->Action_Update_File_Task($task_input) ; 
                    
                    $k = 0 ; 
                    $file_task_map_details = array() ; 
                    
                    foreach ($task_input['file_sample'][0] as $column_match) {
                        
                        $header_submission = array(
                            'column_name' => $column_match,
                            'key_number' => $k
                            ) ; 
                        
                        $match_result = $this->Action_Auto_Map_Field($header_submission) ; 
                        if ($match_result['result'] == 'matched') {
                            $file_task_map_updates[] = $match_result ; 
                            }
                        
                        $k++ ; 
                        }                    
                    }
                
                foreach ($file_task_map_updates as $map_update) {
                    $map_update_model = new File() ; 
                    $map_update_model->Set_File_Task_By_ID($this->file_task_id)->Action_Update_File_Task_Map($map_update) ; 
                    $updated_task_record = $map_update_model->Get_File_Task() ; 
                    $this->file_task['map_selections'] = $updated_task_record['map_selections'] ; 
                    }
                 
                $this->file_task['column_count'] = count($this->file_task['file_sample'][0]) ;

                
                // Set the map field preview...
                $this->file_task['file_map_list'] = array() ; 

                $i = 'A' ; 
                if ($this->file_task['has_header'] == 'yes') {
                    $o = 0 ; 
                    foreach ($this->file_task['file_sample'][0] as $header) {
                        $this->file_task['file_map_list'][$o]['key_number'] = $o ; 
                        $this->file_task['file_map_list'][$o]['column_name'] = 'column_'.$i ; 
                        $this->file_task['file_map_list'][$o]['column_header'] = $header ;
                        $this->file_task['file_map_list'][$o]['match_to'] = '' ;
                        
                        $o++ ;
                        $i++ ; 
                        }
                    } else {   
                        $o = 0 ; 
                        foreach ($this->file_task['file_sample'][0] as $header) {
                            $this->file_task['file_map_list'][$o]['key_number'] = $o ; 
                            $this->file_task['file_map_list'][$o]['column_name'] = 'column_'.$i ;  ; 
                            $this->file_task['file_map_list'][$o]['column_header'] = 'Column '.$i ;
                            $this->file_task['file_map_list'][$o]['match_to'] = '' ;
                            $o++ ;
                            $i++ ; 
                            }                
                        }

                $c = 0 ;            
                foreach ($this->file_task['file_map_list'] as $column) {
                    $i = 0 ; 
                    $k = 0 ; // collected
                    foreach ($this->file_task['file_sample'] as $sample_data) {
                        if ((($this->file_task['has_header'] == 'yes') AND ($i == 0)) OR ($k > 2)) {
                            // SKIP
                            } else {
                                $this->file_task['file_map_list'][$c]['sample_data'] .= $sample_data[$c].'<br> ' ; 
                                $k++ ; 
                                }
                        $i++ ; 
                        }

                    $c++ ;
                    }                                
                }
            
 
            
    
            
            
            
            // Set processing page status...
            $this->file_task['column_match']['matched_columns'] = count($this->file_task['map_selections']) ; 
            $this->file_task['column_match']['column_count'] = count($this->file_task['file_sample'][0]) ;
            $this->file_task['column_match']['unmatched_columns'] = $this->file_task['column_match']['column_count'] - $this->file_task['column_match']['matched_columns'] ; 

            $this->file_task['processing_pages']['settings'] = '' ; 
            $this->file_task['processing_pages']['submit'] = '' ; 
            $this->file_task['processing_pages']['map'] = '' ; 
            
            
            if ($this->file_task['column_match']['unmatched_columns'] > 0) {
                $this->file_task['processing_pages']['settings'] = 'disabled' ;
                $this->file_task['processing_pages']['submit'] = 'disabled' ;
                } 
            
            if ($this->file_task['asset_id'] == 0) {
                $this->file_task['processing_pages']['settings'] = 'disabled' ;
                $this->file_task['processing_pages']['submit'] = 'disabled' ;
                $this->file_task['processing_pages']['map'] = 'disabled' ;
                }
            
            
            // Determine current status...
            $this->file_task['processing_status'] = 'file' ; 
            if ($this->file_task['asset_id'] > 0) {
                $this->file_task['processing_status'] = 'map' ;
                }
            if (($this->file_task['column_match']['unmatched_columns'] == 0) AND ($this->file_task['column_match']['column_count'] > 0)) {
                $this->file_task['processing_status'] = 'settings' ;
                }            
            
            
            } else {
                $this->file_task = $file_task_list ; 
                }
        
        
        
        return $this ; 
        } 
    
    
    // Create a sample of a CSV file with first x rows
    public function Action_Create_Csv_File_Sample($asset_record,$rows_to_include) {
        
        $result_sample = array() ; 
        
        $this->Set_File_Location($asset_record['location'])->Action_Read_Csv() ; 

        $result_sample[] = $this->Get_Csv_Keys() ; 
        $csv_file_content = $this->Get_Csv_Content() ; 

        $i = 0;
        do {
            if ($csv_file_content[$i]) {
                $result_sample[] = $csv_file_content[$i] ;      
                }                
            $i++ ; 
            } while ($i < ($rows_to_include + 1));        
        
        
        
        return $result_sample ; 
        }
    
    
    
    // Set list of files available to process
    public function Set_File_Task_List($query_options = array()) {

        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }

        if (!isset($query_options['filter_by_visibility_id']) AND (!isset($query_options['filter_by_visibility_name']))) {
            $query_options['filter_by_visibility_name'] = array('visible') ; // All user eligible except hidden (which is admin only)
            }
        
        if (!isset($query_options['filter_by_user_id'])) {
            $query_options['filter_by_user_id'] = 'yes' ; // 'yes' uses the internally defined properties of profile_id to pull matching contact list
            }
        if (!isset($query_options['filter_by_account_id'])) {
            $query_options['filter_by_account_id'] = 'yes' ; // 'yes' uses the internally defined properties of account_id to pull matching contact list
            }
            
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        

        $file_task_list = $this->Retrieve_File_Task_List($query_options) ; 
                 
        // This is a failsafe to ensure that if you're on the 2nd+ page of results and you change parameters
        // and it returns results (but not on the current page, you get reset to page 1)
        if (($file_task_list['result_count'] > 0) AND (count($file_task_list['results']) == 0)) {
            $query_options['start_page'] = 1 ; 
            $file_task_list = $this->Retrieve_File_Task_List($query_options) ; 
            }        
        
        
        $file_task_list_save_result = $file_task_list ; 
        
        if ($file_task_list['result_count'] == 0) {
            $this->file_task_list = 'error' ; 
            } else {
            
                $file_task_list_final = array() ; 
            
                $i = 0 ; 
                foreach ($file_task_list['results'] as $file_task_record) {
                    
                    $file_task_list_record = $this->Action_Time_Territorialize_Dataset($file_task_record) ; 
            
                    
                    $file_task_list_record['map_selections'] = json_decode($file_task_list_record['map_selections'],1) ; 

                    $file_task_list_record['structured_data_01'] = json_decode($file_task_list_record['structured_data_01'],1) ; 
                    foreach ($file_task_list_record['structured_data_01'] as $structured_key => $structured_value) {
                        $file_task_list_record[$structured_key] = $structured_value ; 
                        }

                    $file_task_list_record['structured_data_02'] = json_decode($file_task_list_record['structured_data_02'],1) ; 
                    
                    unset($file_task_list_record['structured_data_02']['alert_set'],$file_task_list_record['structured_data_02']['error_set']) ; 
                    
                    foreach ($file_task_list_record['structured_data_02'] as $structured_key => $structured_value) {
                        $file_task_list_record[$structured_key] = $structured_value ; 
                        }                    
                    
                    
                    
                    
                    $list_query = new Contact($this->user_id) ; 
                    $list_query->Set_Account_ID($this->account_id) ; 

                    $list_query_options['filter_by_file_task_id'] = $file_task_list_record['file_task_id'] ; 
                    $list_query_options['override_paging'] = 'yes' ; 
                    $list_query_options['skip_processing'] = 'yes' ; 

                    $list_query->Set_Contact_Import_Log($list_query_options) ; 
                    $data['contact_import_log'] = $list_query->Get_Contact_Import_Log() ;

                    $file_task_list_record['import_log_duplicates_unprocessed'] = 0 ; 
                    $file_task_list_record['import_log_errors_unprocessed'] = 0 ; 

                    if (count($data['contact_import_log']) > 0) {
                        foreach ($data['contact_import_log'] as $log) {
                            switch ($log['import_status']) {
                                case 'error_unprocessed':
                                    $file_task_list_record['import_log_errors_unprocessed']++ ; 
                                    break ; 
                                case 'duplicate_unprocessed':
                                    $file_task_list_record['import_log_duplicates_unprocessed']++ ; 
                                    break ;     
                                }
                            }
                        }
                    
                    $file_task_list_final[] = $file_task_list_record ;     
                    
                    $i++ ; 
                    }

                $this->file_task_list = $file_task_list_final ;
                }        

        
        $this->file_query_result = $file_task_list_save_result ;

        return $this ; 
        } 
    
    
    
    
    // Set data from the temp file associated w/ this file_Task
    public function Set_File_Task_Data_List($query_options = array()) {

        $data['file_task_record'] = $this->Get_File_Task() ; 
        
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
            
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        
        $query_options['table'] = $data['file_task_record']['temp_data_table'] ;
    
        $file_task_data_list = $this->Retrieve_File_Task_Data_List($query_options) ; 

        
        // This is a failsafe to ensure that if you're on the 2nd+ page of results and you change parameters
        // and it returns results (but not on the current page, you get reset to page 1)
//        if (($file_task_list['result_count'] > 0) AND (count($file_task_list['results']) == 0)) {
//            $query_options['start_page'] = 1 ; 
//            $file_task_list = $this->Retrieve_File_Task_List($query_options) ; 
//            }        
        
        

        if ($file_task_data_list['result_count'] == 0) {
            $this->file_task_data_list = array() ; 
            } else {

                $this->file_task_data_list = $file_task_data_list['results'] ;
                }        


        return $this ; 
        } 
    
    public function Get_File_Task_Data_List() {
        
        return $this->file_task_data_list ; 
        }
    
    // Process a set of asset results and separate into paging components to use for site navigation
    public function Set_File_Paging($results_array) {

        if (!isset($this->file_paging)) {
            $this->file_paging = $this->Set_Default_Paging_Object() ; 
            }       
        
        $this->file_paging->page_increment = $this->Get_Page_Increment() ;
        
        $this->file_paging = $this->Action_Process_Paging($this->file_paging,$results_array) ; 
        
        return $this ; 
        }
    
    
    //////////////////////
    //                  //
    // API ACTIONS      //
    //                  //
    //////////////////////
    
    // Get the file location to read
    public function API_Get_Connection() {
        
        return $this->_connection ; 
        }    
    
    
    // Get the file location to read
    public function API_Set_File($vendor_file_id) {
        
        $file_object = $this->_connection->getFile($vendor_file_id);        
        
        $this->file_object = $file_object ; 
        
        return $this ; 
        }
    
    public function API_Get_File() {
        
        return $this->file_object ; 
        }    

    public function API_Upload_File_From_URL($file_url) {
        
        $api_connect = $this->API_Get_Connection() ;

        $file = $api_connect->uploader->fromUrl($file_url);
        $file->store();

        $token = $api_connect->uploader->fromUrl(
            $file_url,
            array('check_status' => false)
            );

        $data = $api_connect->uploader->status($token);

        
        $this->file_info = $data ;
        
        if ($data->status === 'success') {
            $file_id = $data->file_id ; 
            // do smth with a file
            $this->Set_File_Location($file->getUrl()) ; 

            } else {
                $this->Set_File_Location('error') ;
                }
        
        return $this ; 
        }
    
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    

    
    // Get the file location to read
    public function Get_Default_Contact_Fields() {
        
        if ($this->default_contact_fields = '') {
            
            $this->Set_Default_Contact_Fields() ; 
            
            }
        
        return $this->default_contact_fields ; 
        }
    
    
    
    
    // Get the file location to read
    public function Get_File_Location() {
        
        return $this->file_location ; 
        }
    
    // Get the file info as stored at API vendor to read
    public function Get_File_Info() {
        
        return $this->file_info ; 
        }
    
    
    // Get task file list w/ associated assets
    public function Get_File_Task() {
        return $this->file_task ; 
        }
    
    
    // Get task file list w/ associated assets
    public function Get_File_Task_List() {
        return $this->file_task_list ; 
        } 
    
    
    // Get and return the global list of connected accounts 
    public function Get_Csv_Transformed() {
        return $this->csv_transformed ; 
        } 

    
    public function Get_File_Paging() {
        
        return $this->file_paging ;        
        }    
    
    
    public function Get_Csv_Keys() {
        
        return $this->csv_keys ;        
        } 
    
    public function Get_Csv_Content() {
        
        return $this->csv_contents ;        
        } 
    
    public function Get_Csv_Processed_Content() {
        
        return $this->csv_processed_content ;        
        }    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    //////////////////////
    
    
    
    public function Action_Auto_Map_Field($match_input) {
        
//        $header_submission = array(
//            'column_name' => $column_match,
//            'key_number' => $k
//            ) ; 
        
        $match_result = $match_input  ;
        $match_result['result'] = 'none' ;
                
        $data['contact_field_match'] = $this->Get_System_List('list_contact_field_match') ; 
        if (!isset($data['contact_fields'])) {
            $query_options = array(
            'override_paging' => 'yes',
            'active' => 1
            ) ; 
            $this->Set_System_List('list_contact_field_match',$query_options) ; 
            $data['contact_field_match'] = $this->Get_System_List('list_contact_field_match') ;
            }

        
        $lowercase_column_name = strtolower($match_input['column_name']) ;
        
        $i = 0 ; 
        $i_count = count($data['contact_field_match']) ; 
        do {
            
            if ($lowercase_column_name == $data['contact_field_match'][$i]['submitted_key']) {
                $match_result['result'] = 'matched' ;
                $match_result['key'] = $data['contact_field_match'][$i]['field_key'] ;
                } else if (preg_replace('/[^\w-]/', '', $lowercase_column_name) == $data['contact_field_match'][$i]['submitted_key']) {
                    $match_result['result'] = 'matched' ;
                    $match_result['key'] = $data['contact_field_match'][$i]['field_key'] ;
                    }
                        
            $i++ ; 
            } while (($match_result['result'] == 'none') AND ($i < $i_count)) ; 
        
        
        return $match_result ;
        }
    
    
    
    public function Action_Create_File_Task($task_input) {
        
        $continue = 1 ;
        
        $task_input_clean = array() ; 
        
        foreach ($task_input as $file_task_key => $file_task_value) {
            switch ($file_task_key) {                       
                case 'task_title': 
                case 'task_type':    
                case 'task_action': 
                case 'asset_id':
                case 'auto_capitalize_names':
                    $task_input_clean[$file_task_key] = $file_task_value ; 
                    break ; 
                }
            }
        
        if (!isset($task_input_clean['task_title'])) {
            
            $date['timestamp_now'] = TIMESTAMP ; 
            $date = $this->Action_Time_Territorialize_Dataset($date) ;
            $task_input_clean['task_title'] = 'New Import on '.$date['timestamp_now_compiled']['m/d/yy'] ; 
            }
        
        if (!isset($task_input_clean['structured_data_01']['auto_capitalize_names'])) {
            $task_input_clean['structured_data_01']['auto_capitalize_names'] = 'yes' ;
            }
        
        
        $task_input_clean['user_id'] = $this->user_id ; 
        $task_input_clean['account_id'] = $this->account_id ;
        
        if (isset($task_input_clean['asset_id'])) {
            $task_input_clean['task_status'] = 'incomplete' ;     
                        
            $asset_record = $this->Set_Asset_By_ID($task_input_clean['asset_id'])->Get_Asset() ; 

            // File sample, header selection, and auto mapping will be done via Set_File_Task_By_ID() below after initial task record is created.
 
            } else {
                $task_input_clean['task_status'] = 'incomplete' ;
                }
        
        

        
        if (isset($task_input_clean['structured_data_01'])) {
            $task_input_clean['structured_data_01'] = json_encode($task_input_clean['structured_data_01']) ; 
            }
        
        
        $visibility_query_options['filter_by_visibility_name'] = 'visibility_draft' ; 
        $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
        $visibility_name = $visibility_result['visibility_name'] ;
        
        $task_input_clean['visibility_id'] = $visibility_result['visibility_id'] ;
        
        
        
        
        if ($continue == 1) {
            $result = $this->Create_File_Task($task_input_clean) ;

            if ($result['insert_id']) {

                $this->Set_File_Task_By_ID($result['insert_id']) ; 
                
                $task = $this->Get_File_Task() ; 
                
                $this->Set_Alert_Response(18) ; // Account updated

                } else {

                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result)
                        )) ;             

                    }             
            }

        return $this ; 
        }
    
    
    public function Create_File_Task($query_options) {
        
//        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        $query_array = array(
            'table' => "file_tasks",
            'values' => $query_options,
            );
        
        $query_array['values']['timestamp_task_updated'] = TIMESTAMP ;
        $query_array['values']['timestamp_task_created'] = TIMESTAMP ;
        
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        
        $this->file_query_result = $result ; 
        
        return $result ; 
        }
    
    
    
    public function Action_Update_File_Task($task_input) {
        
        $continue = 1 ;
        $task_input_clean = array() ; 
        
        $file_task_record = $this->Get_File_Task() ; 
        $existing_data_01 = $file_task_record['structured_data_01'] ;
        $existing_data_02 = $file_task_record['structured_data_02'] ;
        

        if ($task_input['visibility_name']) {
            $visibility_query_options['filter_by_visibility_name'] = $task_input['visibility_name'] ; 
            $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
            $task_input['visibility_id'] = $visibility_result['visibility_id'] ; 
            }
        
        
        foreach ($task_input as $file_task_key => $file_task_value) {
            switch ($file_task_key) {                       
                case 'visibility_id':
                case 'task_title': 
                case 'asset_id':
                case 'task_status':                    
                    $task_input_clean[$file_task_key] = $file_task_value ; 
                    break ;  
                }
            }
        
        // Update submission duplicates...
        if (isset($task_input['update_duplicates'])) {
            switch ($task_input['update_duplicates']) {
                case 'false':
                case 'no':
                    $task_input_clean['structured_data_01']['update_duplicates'] = 'false' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $task_input_clean['structured_data_01']['update_duplicates'] = 'true' ; 
                    break ; 
                }            
            }
        
        if (isset($task_input['contact_permission'])) {
            switch ($task_input['contact_permission']) {
                case 'false':
                case 'no':
                    $task_input_clean['structured_data_01']['contact_permission'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $task_input_clean['structured_data_01']['contact_permission'] = 'yes' ; 
                    break ; 
                }            
            }        
        
        
        // File Has Header
        if (isset($task_input['has_header'])) {
            switch ($task_input['has_header']) {
                case 'false':
                case 'no':
                    $task_input_clean['structured_data_01']['has_header'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $task_input_clean['structured_data_01']['has_header'] = 'yes' ; 
                    break ; 
                }            
            }
        
        if (isset($task_input['no_header'])) {
            switch ($task_input['no_header']) {
                case 'false':
                case 'no':
                    $task_input_clean['structured_data_01']['has_header'] = 'yes' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $task_input_clean['structured_data_01']['has_header'] = 'no' ; 
                    break ; 
                }            
            }
        
        
        // File Has Header
        if (isset($task_input['auto_capitalize_names'])) {
            switch ($task_input['auto_capitalize_names']) {
                case 'false':
                case 'no':
                    $task_input_clean['structured_data_01']['auto_capitalize_names'] = 'no' ; 
                    break ; 
                case 'true':
                case 'yes':
                    $task_input_clean['structured_data_01']['auto_capitalize_names'] = 'yes' ; 
                    break ; 
                }            
            }
        
        
        // File Sample
        if (isset($task_input['file_sample'])) {
            $task_input_clean['structured_data_01']['file_sample'] = $task_input['file_sample'] ;            
            }
        
        
        $task_input_clean['structured_data_01']['map_override'] = $existing_data_01['map_override'] ; // Get the original overrides before mapping over them
        
        if (isset($task_input['map_override_contact_rating'])) {
            $task_input_clean['structured_data_01']['map_override']['contact_rating'] = $task_input['map_override_contact_rating'] ;            
            if ($task_input['map_override_contact_rating'] == 'remove') { // Removes the override 
                unset($task_input_clean['structured_data_01']['map_override']['contact_rating']) ; 
                }
            }        
        
        if (isset($task_input['map_override_contact_status'])) {
            $task_input_clean['structured_data_01']['map_override']['contact_status'] = $task_input['map_override_contact_status'] ;            
            if ($task_input['map_override_contact_status'] == 'remove') { // Removes the override 
                unset($task_input_clean['structured_data_01']['map_override']['contact_status']) ; 
                }
            }
        
        
        
        
        if ($task_input_clean['structured_data_01']['contact_permission'] == 'no') {
            $continue = 0  ;
            $this->Set_Alert_Response(249) ; // No persmission
            }
        
  
        if (isset($task_input['alert_count'])) {
            $task_input_clean['structured_data_02']['alert_count'] = $task_input['alert_count'] ;
            }
        if (isset($task_input['error_count'])) {
            $task_input_clean['structured_data_02']['error_count'] = $task_input['error_count'] ;
            }        
        if (isset($task_input['created_count'])) {
            $task_input_clean['structured_data_02']['created_count'] = $task_input['created_count'] ;
            }        
        if (isset($task_input['success_count'])) {
            $task_input_clean['structured_data_02']['success_count'] = $task_input['success_count'] ;
            }
        if (isset($task_input['updated_count'])) {
            $task_input_clean['structured_data_02']['updated_count'] = $task_input['updated_count'] ;
            }        
        
        
        
        
        // Encode the structured data...
        if (isset($task_input_clean['structured_data_01'])) {
            
            // Retrieve the existing metadata entries and make sure they don't get overwritten while adding new key => values
            foreach ($task_input_clean['structured_data_01'] as $key => $value) {
                $existing_data_01[$key] = $value ;         
                }
            
            $task_input_clean['structured_data_01'] = json_encode($existing_data_01) ;
            }
        
        if (isset($task_input_clean['structured_data_02'])) {
            
            // Retrieve the existing metadata entries and make sure they don't get overwritten while adding new key => values
            foreach ($task_input_clean['structured_data_02'] as $key => $value) {
                $existing_data_02[$key] = $value ;         
                }
            
            $task_input_clean['structured_data_02'] = json_encode($existing_data_02) ;
            }
        

        if (!isset($this->file_task_id)) {
            $continue = 0  ;
            }
        
        

        if ($continue == 1) {
            $update_file_task = $this->Update_File_Task($task_input_clean) ;

            
            $file_task = $this->Set_File_Task_By_ID()->Get_File_Task() ; 
            
            if ($update_file_task['results'] == true) {

                
                // Wipe existing field map...
                if ((isset($task_input['asset_id'])) AND ($task_input['asset_id'] == 0)) {
                    
                    $wipe_file_map_update['structured_data_01'] = $file_task['structured_data_01'] ; 
                    unset($wipe_file_map_update['structured_data_01']['file_sample']) ;
                    
                    $wipe_file_map_update['structured_data_01'] = json_encode($wipe_file_map_update['structured_data_01']) ;
                    $wipe_file_map_update['map_selections'] = '' ; 
                    $update_file_task = $this->Update_File_Task($wipe_file_map_update) ;
                    }
                

                if ($task_input['visibility_name'] == 'visibility_deleted') {
                    $this->Set_Alert_Response(254) ; // File Task deleted
                    } else if ($file_task['task_status'] == 'processing') {
                    $this->Set_Alert_Response(250) ; // File Task updated and submitted for processing 
                    } else {
                    $this->Set_Alert_Response(247) ; // File Task updated    
                    }
                
                
                } else {

                    $this->Set_Alert_Response(248) ; // File Task Error
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($update_file_task)
                        )) ;             

                    }             
            }

        $this->file_task_query = $update_file_task_initial ;
        return $this ; 
        }
    
    
    
    public function Action_Update_File_Task_Map($task_input) {
        
        $continue = 1 ;
        $task_input_clean = array() ; 
        
        $file_task_record = $this->Get_File_Task() ; 
        $existing_map_selections = $file_task_record['map_selections'] ;
        if (!is_array($existing_map_selections)) {
            $existing_map_selections = array() ; 
            }

        $task_input_clean['map_selections'] = array() ; 
        $task_input_clean['map_selections'][$task_input['key_number']] = $task_input['key'] ; 
            


        if ((in_array($task_input['key'],$existing_map_selections)) AND ($task_input['key'] != 'do_not_map')) {
            
            $continue = 0 ; 
            $this->Set_Alert_Response(255) ; // Duplicate map selection
            
            $personalization['field_name'] = $task_input['key'] ; 
            $this->Append_Alert_Response($personalization,array(
                'admin_context' => json_encode($task_input),
                'location' => __METHOD__
                )) ;            
            }
        
        
        // Encode the map selections data...
        if (isset($task_input_clean['map_selections'])) {
            
            // Retrieve the existing metadata entries and make sure they don't get overwritten while adding new key => values
            foreach ($task_input_clean['map_selections'] as $key => $value) {
                $existing_map_selections[$key] = $value ;         
                }
            
            $task_input_clean['map_selections'] = json_encode($existing_map_selections) ;
            }
        
        
        if (!isset($task_input['key_number']) OR (!$task_input['key'])) {
            $continue = 0  ;
            }
        
        if (!isset($this->file_task_id)) {
            $continue = 0  ;
            }
        
        
        if ($continue == 1) {
            
            $update_file_task = $this->Update_File_Task($task_input_clean) ;
            
            
            $file_task = $this->Set_File_Task_By_ID()->Get_File_Task() ; 
            
            if ($update_file_task['results'] == true) {

                    $this->Set_Alert_Response(247) ; // File Task updated    

                

                } else {

                    $this->Set_Alert_Response(248) ; // File Task Error
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($update_file_task)
                        )) ;             

                    }             
            }

        return $this ; 
        }
    
    
    
    public function Action_Process_File_Task($task_options = array()) {
        
        $continue = 1 ;
        
        $this->file_task_status = 'setup' ; 
        
        // Elapsed time is calculated only internal to this model -- not the entire cron script
        $microtime_start = microtime(true) ;
        
        if (!isset($task_options['microtime_limit'])) {
            $task_options['microtime_limit'] = 30 ; 
            } 
        
        if (!isset($this->file_task_id)) {
            $continue = 0  ;
            }        
        
        if ($this->file_task['file_task_id'] != $this->file_task_id) {
            $continue = 0  ;
            }
        
        
        if ($continue == 1) {
            
            $this->file_task_status = 'entered continue' ; 
            
            $data['file_task_record'] = $this->Get_File_Task() ;

            
            // Set up the temp table... 
            if (!$data['file_task_record']['temp_data_table']) {
                                
                $fields_array = array() ; 
                $fields_array[] = 'item_id INT NOT NULL PRIMARY KEY' ;
                foreach ($data['file_task_record']['map_selections'] as $key_number => $key) {
                    if ($key != 'do_not_map') {
                        $fields_array[] = $key.' LONGTEXT NOT NULL' ;
                        }
                    }
                $fields_array[] = 'update_duplicates VARCHAR(220) NOT NULL' ;
                $fields_array[] = 'auto_capitalize_names VARCHAR(220) NOT NULL' ;
                $fields_array[] = 'processed INT(2)' ;
                
                
                $create_table_array = array(
                    'new_table' => 'temp_file_task_'.$this->file_task['file_task_id'].'_data' ,
                    'fields_array' => $fields_array
                    ) ; 

                $result = $this->DB->Query('CREATE_TABLE',$create_table_array); 
                
                if (!$result['error']) {
                    
                    $add_primary_statement = 'ALTER TABLE '.$create_table_array['new_table'].' MODIFY item_id INT NOT NULL AUTO_INCREMENT' ;
                    $add_primary = $this->DB->_connection->query($add_primary_statement) ; 
                    
                    
                    $set_table_update['structured_data_01'] = $data['file_task_record']['structured_data_01'] ; 
                    $set_table_update['structured_data_01']['temp_data_table'] = $create_table_array['new_table'] ; 

                    $set_table_update['structured_data_01'] = json_encode($set_table_update['structured_data_01']) ;
                    $update_file_task = $this->Update_File_Task($set_table_update) ;                
                    
                    $data['file_task_record'] = $this->Set_File_Task_By_ID()->Get_File_Task() ;
                    } else {
                        $continue = 0 ;
                        $this->file_task_status = 'table error'.json_encode($result['error']) ; 
                        }  
                
                $this->file_query_result = $result ; 
                }
            
            
            // Insert data into temp table...
            if (($continue == 1) AND ($data['file_task_record']['temp_data_table_status'] != 'filled')) {
                
                $this->Set_File_Location($data['file_task_record']['asset_record']['location'])->Action_Read_Csv() ; 
                $data['csv_processed_content'] = array() ; 

                $i = 0 ; 
                if ($data['file_task_record']['has_header'] == 'no') {
                    $csv_first_row = $this->Get_Csv_Keys() ;
                    foreach ($data['file_task_record']['map_selections'] as $key_number => $key) {
                        if ($key != 'do_not_import') {
                            $data['csv_processed_content'][$i][$key] = $csv_first_row[$key_number] ; 
                            }                    
                        }
                    $data['csv_processed_content'][$i]['update_duplicates'] = $data['file_task_record']['update_duplicates'] ;
                    $data['csv_processed_content'][$i]['auto_capitalize_names'] = $data['file_task_record']['auto_capitalize_names'] ;
                    $data['csv_processed_content'][$i]['processed'] = 0 ; 
                    $i++ ; 
                    }

                
                $csv_content = $this->Get_Csv_Content() ;
                foreach ($csv_content as $row) {
                    foreach ($data['file_task_record']['map_selections'] as $key_number => $key) {
                        if ($key != 'do_not_map') {
                            $data['csv_processed_content'][$i][$key] = $row[$key_number] ; 
                            }
                        }
                    $data['csv_processed_content'][$i]['update_duplicates'] = $data['file_task_record']['update_duplicates'] ; 
                    $data['csv_processed_content'][$i]['auto_capitalize_names'] = $data['file_task_record']['auto_capitalize_names'] ;
                    $data['csv_processed_content'][$i]['processed'] = 0 ; 
                    $i++ ; 
                    }

                $this->csv_processed_content = $data['csv_processed_content'] ;                 
                
                
                // Set the table keys...
                $import_keys = array() ; 
                foreach ($data['file_task_record']['map_selections'] as $key_number => $key) {
                    if ($key != 'do_not_map') {
                        $import_keys[] = $key ; 
                        }
                    }
                $import_keys[] = 'update_duplicates' ; 
                $import_keys[] = 'auto_capitalize_names' ; 
                $import_keys[] = 'processed' ; 
                
                
                $query_array = array(
                    'table' => $data['file_task_record']['temp_data_table'],
                    'fields' => $import_keys,
                    'values' => $data['csv_processed_content']
                    ) ; 
                
                $result_temp_table = $this->DB->Query('BULK_INSERT',$query_array);                
                
                
                
                if (!$result_temp_table['error']) {
                    
                    $task_data_query = array(
                        'table' => $data['file_task_record']['temp_data_table'],
                        'fields' => "COUNT(item_id), item_id",
                        'group_by' => 'item_id'
                        ) ;
                    $task_data_result = $this->DB->Query('SELECT',$task_data_query) ;                    
                    
                    
                    $temp_table_update['structured_data_01'] = $data['file_task_record']['structured_data_01'] ; 
                    $temp_table_update['structured_data_01']['temp_data_table_status'] = 'filled' ; 
                    $temp_table_update['structured_data_01'] = json_encode($temp_table_update['structured_data_01']) ;
                    
                    $temp_table_update['structured_data_02'] = $data['file_task_record']['structured_data_02'] ; 
                    $temp_table_update['structured_data_02']['submitted_count'] = $task_data_result['result_count'] ;
                    $temp_table_update['structured_data_02']['processed_count'] = 0 ;
                    $temp_table_update['structured_data_02']['success_count'] = 0 ;
                    $temp_table_update['structured_data_02']['alert_count'] = 0 ;
                    $temp_table_update['structured_data_02']['error_count'] = 0 ;
                    $temp_table_update['structured_data_02']['created_count'] = 0 ;
                    $temp_table_update['structured_data_02']['updated_count'] = 0 ;
                    $temp_table_update['structured_data_02']['alert_set'] = array() ;
                    $temp_table_update['structured_data_02']['error_set'] = array() ;
                    $temp_table_update['structured_data_02'] = json_encode($temp_table_update['structured_data_02']) ;
                    
                    $update_file_task = $this->Update_File_Task($temp_table_update) ;                
                    
                    $data['file_task_record'] = $this->Set_File_Task_By_ID()->Get_File_Task() ;
                    } else {
                        $continue = 0 ;
                        }  
                
                $this->file_query_result = $result_temp_table ; 
                }
            
            
            
            // Task Processing         
            if (($continue == 1) AND ($data['file_task_record']['temp_data_table_status'] == 'filled') AND (isset($data['file_task_record']['temp_data_table']))) {


                // Process Task Do Loop 
                do {
                    
                    $data_list_options = array(
                        'page_increment' => $task_options['process_record_limit'],
                        'filter_by_processed' => 0
                        ) ; 
                    $data['file_task_data_list'] = $this->Set_File_Task_Data_List($data_list_options)->Get_File_Task_Data_List() ;                         

                
                    
                    
                    if (count($data['file_task_data_list']) > 0) {                    
                    
                        // Process in the new contacts
                        switch ($data['file_task_record']['task_type']) {
                            case 'contact':

                                // Add in the overrides, metadata, and list here...
                                // Contact status override
                                if (isset($data['file_task_record']['map_override']['contact_status'])) {
                                    $i = 0 ;
                                    foreach ($data['file_task_data_list'] as $item) {
                                        $data['file_task_data_list'][$i]['contact_status_metadata_id'] = $data['file_task_record']['map_override']['contact_status'] ; 
                                        $i++  ;
                                        }
                                    }

                                // Contact rating override
                                if (isset($data['file_task_record']['map_override']['contact_rating'])) {
                                    $i = 0 ;
                                    foreach ($data['file_task_data_list'] as $item) {
                                        $data['file_task_data_list'][$i]['contact_rating_metadata_id'] = $data['file_task_record']['map_override']['contact_rating'] ; 
                                        $i++  ;
                                        }
                                    }


                                // Add metadata overrides...
                                if (is_array($data['file_task_record']['tag_map'])) {
                                    $i = 0 ;
                                    foreach ($data['file_task_data_list'] as $item) {
                                        
                                        $data['file_task_data_list'][$i]['metadata_id'] = '' ;
                                        
                                        foreach ($data['file_task_record']['tag_map'] as $tag) {
                                            $data['file_task_data_list'][$i]['metadata_id'] .= $tag['metadata_id'].',' ; 
                                            }  
                                        
                                        $i++  ;
                                        }
                                    }
                                

                                // Set the contact source...
                                $i = 0 ;
                                foreach ($data['file_task_data_list'] as $item) {
                                    $data['file_task_data_list'][$i]['created_source'] = 'Contact import' ;
                                    $data['file_task_data_list'][$i]['import_file_task_id'] = $data['file_task_record']['file_task_id'] ; 
                                    $i++  ;
                                    }
                                
                                $contact = new Contact($data['file_task_record']['user_id']) ; 
                                $contact->Set_Account_By_ID($data['file_task_record']['account_id'])->Set_Profile_By_User_Account() ; 
                                $contact->Action_Create_Contact($data['file_task_data_list']) ; 

                                $result = $contact->Get_Response() ; 
                                $data['item_create_set'] = $contact->Get_Contact_Create_Set() ;                            

                                $this->test_data = $data['item_create_set'] ; 
                                break ; 
                            }



                        // Increment file task results and mark these items as processed in the temp table
                        foreach ($data['file_task_data_list'] as $processed_item) {

                            $this_item_id = $processed_item['item_id'] ; 

                            $update_processed_item = array(
                                'table' => $data['file_task_record']['temp_data_table'],
                                'values' => array(
                                    'item_id' => $this_item_id,
                                    'processed' => 1),
                                'where' => "item_id=$this_item_id"
                                ) ; 

                            $update_task_item = $this->Update_File_Task_Item($update_processed_item) ; 
                            }



                        // Update the file task with processing results
                        // Record errors to individual item submission
                        $processing_update['structured_data_02'] = $data['file_task_record']['structured_data_02'] ; 
                        $processing_update['structured_data_02']['processed_count'] = $data['file_task_record']['structured_data_02']['processed_count'] + $data['item_create_set']['submitted_count'] ;
                        $processing_update['structured_data_02']['success_count'] = $data['file_task_record']['structured_data_02']['success_count'] + $data['item_create_set']['success_count'] ;
                        $processing_update['structured_data_02']['alert_count'] = $data['file_task_record']['structured_data_02']['alert_count'] + $data['item_create_set']['alert_count'] ;
                        $processing_update['structured_data_02']['error_count'] = $data['file_task_record']['structured_data_02']['error_count'] + $data['item_create_set']['error_count'] ;
                        $processing_update['structured_data_02']['created_count'] = $data['file_task_record']['structured_data_02']['created_count'] + $data['item_create_set']['created_count'] ;
                        $processing_update['structured_data_02']['updated_count'] = $data['file_task_record']['structured_data_02']['updated_count'] + $data['item_create_set']['updated_count'] ;

                        // Record ERRORS to individual contact submission
                        foreach ($data['item_create_set']['error_set'] as $error_item) {
                            
//                            $processing_update['structured_data_02']['error_set'][] = $error_item ; 
                                                        
                            $content_history_array = array(
                                'file_task_id' => $data['file_task_record']['file_task_id'],
                                'submitted_contact' => $error_item
                                ) ;
                            $content_history_array['submitted_contact'] = json_encode($content_history_array['submitted_contact']) ;
//                            $content_history_array['alert'] = json_encode($error_item['error']) ;
                            
                            $content_history_array['import_status'] = 'error_unprocessed' ;
                            
                            foreach ($error_item as $contact_key => $contact_value) {
                                switch ($contact_key) {
                                    case 'email_address':
                                    case 'yl_member_number':
                                    case 'phone_number':
                                    case 'phone_country_dialing_code':
                                        $content_history_array[$contact_key] = $contact_value ;
                                        break ; 
                                    }
                                }
                                                        
                            $query_array = array(
                                'table' => 'contact_import_log',
                                'values' => $content_history_array
                                );

                            $result = $this->DB->Query('INSERT',$query_array) ;                            
                            
                            
                            }
                        
                        
                        // Record ALERTS to individual contact submission
                        foreach ($data['item_create_set']['alert_set'] as $alert_item) {
//                            $processing_update['structured_data_02']['alert_set'][] = $alert_item ; 
                            
                            $content_history_array = array(
                                'file_task_id' => $data['file_task_record']['file_task_id'],
                                'submitted_contact' => $alert_item['duplicate_check']['submitted_contact']
                                ) ;
                            $content_history_array['submitted_contact'] = json_encode($content_history_array['submitted_contact']) ;
                            
                            $alert_item['alert']['dup_score_phone_number'] = $alert_item['duplicate_check']['dup_score_phone_number'] ; 
                            $alert_item['alert']['dup_score_email_address'] = $alert_item['duplicate_check']['dup_score_email_address'] ; 
                            $alert_item['alert']['dup_score_yl_member_number'] = $alert_item['duplicate_check']['dup_score_yl_member_number'] ; 
                                                        
//                            $content_history_array['alert'] = json_encode($alert_item['alert']) ;
                            
                            switch ($alert_item['alert']['alert_id']) {
                                case 171:
                                    $content_history_array['import_status'] = 'duplicate_unprocessed' ; 
                                    $content_history_array['duplicate_contact_id'] = $alert_item['duplicate_check']['duplicate_contact_id'] ; 
                                    break ; 
                                }
                            
                            foreach ($alert_item['duplicate_check']['submitted_contact'] as $contact_key => $contact_value) {
                                switch ($contact_key) {
                                    case 'email_address':
                                    case 'phone_number':
                                    case 'phone_country_dialing_code':
                                        $content_history_array[$contact_key] = $contact_value ;
                                        break ; 
                                    case 'contact_details':
                                        foreach ($contact_value as $contact_detail_key => $contact_detail_value) {
                                            switch ($contact_detail_key) {
                                                case 'yl_member_number':
                                                    $content_history_array[$contact_detail_key] = $contact_detail_value ;
                                                    break ; 
                                                }
                                            }
                                        break ;
                                    }
                                }
                                                        
                            $query_array = array(
                                'table' => 'contact_import_log',
                                'values' => $content_history_array
                                );

                            $result = $this->DB->Query('INSERT',$query_array) ;
                            }                
                        
                        
                        $processing_update['structured_data_02'] = json_encode($processing_update['structured_data_02']) ;
                        $update_file_task = $this->Update_File_Task($processing_update) ; 

                        $data['file_task_record'] = $this->Set_File_Task_By_ID()->Get_File_Task() ;                    



                        // When all items are marked as processed=1, then mark the file_task as complete and delete the temp table
                        if ($data['file_task_record']['structured_data_02']['processed_count'] == $data['file_task_record']['structured_data_02']['submitted_count']) {
                            $complete_task_update['task_status'] = 'complete' ;
                            $complete_task_update['timestamp_task_completed'] = time() ;
                            $update_file_task = $this->Update_File_Task($complete_task_update) ;
                            $data['file_task_record'] = $this->Set_File_Task_By_ID()->Get_File_Task() ;      


                            $data_list_options = array(
                                'page_increment' => $task_options['process_record_limit'],
                                'filter_by_processed' => 0
                                ) ; 
                            $data['check_file_task_data_list'] = $this->Set_File_Task_Data_List($data_list_options)->Get_File_Task_Data_List() ;                         

                            
                            
                            // Send desktop alert for completed import...
                            $pusher = new Alert() ; 
                            $pusher->Connect() ; 

                            $pusher_message = array() ; 
                            $pusher_message['channel_name'] = 'channel-account-'.$data['file_task_record']['account_id'] ;
                            $pusher_message['event_name'] = 'contact-import-complete' ;    
                            $pusher_message['message'] = array(
                                'file_task_record' => array(
                                    'file_task_id' => $data['file_task_record']['file_task_id'],
                                    'task_title' => $data['file_task_record']['task_title']
                                    ),
                                ) ;

                            $pusher->API_Action_Trigger($pusher_message) ;                            
                            
                            
                            
                            // Drop the temp data table if all items marked as processed...
                            if (count($data['check_file_task_data_list']) == 0) {
                                $query_array = array(
                                    'table' => $data['file_task_record']['temp_data_table']
                                    );

                                $result = $this->DB->Query('DROP_TABLE',$query_array) ;
                                }                                                     
                            }    



                        }
    
                    $microtime_end = microtime(true) ; // Capture current microtime and calculate elapsed from beginning of script
                    $microtime_elapsed = $microtime_end - $microtime_start ;
                    
                    } while ((count($data['file_task_data_list']) > 0) AND ($data['file_task_record']['structured_data_02']['processed_count'] < $data['file_task_record']['structured_data_02']['submitted_count']) AND ($microtime_elapsed < $task_options['microtime_limit'])) ;
                    // END: Process Task Do Loop 

                } // END: Task Processing            
            
                    
            } // END: Final continue=1 check
        
        
        return $this ; 
        }
    

    
    
    
    // ACTION Converts a CSV file contents to an array, and creates a separate array of keys for the CSV file
    // https://www.php.net/manual/en/function.str-getcsv.php
    function Action_Read_Csv() {

        //load the CSV document from a file path
//        $csv = League\Csv\Reader::createFromPath($this->file_location, 'r');
//        $csv->setHeaderOffset(0);
//        $this->csv_keys = $csv->getHeader(); //returns the CSV header record
//        $this->csv_contents = $csv->getRecords(); //returns all the CSV records as an Iterator object        
        
        
        $csv = array_map("str_getcsv", file($this->file_location,FILE_SKIP_EMPTY_LINES));
        $keys = array_shift($csv);

        $this->csv_keys = $keys ;
        $this->csv_contents = $csv ;
        
        return $this ; 
        }
    
    
    
    // ACTION Takes an array of CSV keys and "destination key names", then scrubs the original
    // CSV array into a transformed array of just the selected keys and associated data
    function Action_Transform_Csv($key_transformations) {

        $csv_transformed = array() ; 
        
        foreach ($this->csv_contents as $row) {
            
            $row_transformed = array() ; 
            
            foreach ($key_transformations as $key => $value) {
                $row_transformed[$value] = $row[$key] ; 
                }
            
            $csv_transformed[] = $row_transformed ; 
                
            }
        
        $this->csv_transformed = $csv_transformed ; 
        
        return $this ; 
    }    

    
    
    
    
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    ////////////////////// 
    
    
    // file_task_id must be set to update the record for an account
    public function Update_File_Task($input) {         

        $values_array = array() ; 
        foreach ($input as $key => $value) {            
            $values_array[$key] = $value ;             
            }
        
        
        // Updates the file_task in the database
        $query_array = array(
            'table' => 'file_tasks',
            'values' => $values_array,
            'where' => "file_task_id='$this->file_task_id'"
            );

        $query_array['values']['timestamp_task_updated'] = TIMESTAMP ; 

        $result = $this->DB->Query('UPDATE',$query_array) ;        
        
        if (!$result['error']) {
            // Create History Entry
            $history_input = array(
                'account_id' => $this->account_id,
                'function' => 'update_file_task',
                'notes' => json_encode($query_array['values'])
                ) ; 
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;
            } else {
                $this->Set_Alert_Response(19) ; // Account update failed
                }
        
        $this->file_task_query = $result ; 
        
        return $result ; 
        
        }
    
    
    // Updates file task items within the file task temp table
    // Must submit table name w/ request
    //    $update_processed_item = array(
    //        'table' => '',
    //        'values' => array(
    //            'item_id' => $processed_item['item_id'],
    //            'processed' => 1),
    //        'where' => "item_id=$this_item_id"
    //        ) ;  
    
    
    public function Update_File_Task_Item($input) {         

        $values_array = array() ; 
        foreach ($input['values'] as $key => $value) {            
            $values_array[$key] = $value ;             
            }
        
        
        // Updates the file_task in the database
        $query_array = array(
            'table' => $input['table'],
            'values' => $values_array,
            'where' => $input['where']
            );
        
        $result = $this->DB->Query('UPDATE',$query_array) ;        
        
        return $result ;         
        }
    
    
    
    
    public function Retrieve_File_Task_List($query_options = array()) {
        
        $timezone = $this->system_timezone ; 
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $master_subqueries = array() ;
        
        //
        // PROCESSING
        //
                

        
        // Base query:
        $query_array = array(
            'table' => 'file_tasks',
            'join_tables' => array(),
            'fields' => "
                file_tasks.*, 
                assets.asset_id, assets.asset_title, 

                system_visibility_levels.visibility_title, system_visibility_levels.visibility_description, asset_type.*
                ",
//            'where' => "",
            'group_by' => "file_tasks.file_task_id"
            );
        
//                asset_status.asset_status_id, asset_status.status_name, asset_status.status_title, 
       
        // Add in additional join tables...
        $query_array['join_tables'][] = array(
            'table' => 'assets',
            'on' => 'assets.asset_id',
            'match' => 'file_tasks.asset_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_type',
            'on' => 'asset_type.type_id',
            'match' => 'assets.type_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'file_tasks.visibility_id'
            );
        
//        $query_array['join_tables'][] = array(
//            'table' => 'asset_status',
//            'on' => 'asset_status.asset_status_id',
//            'match' => 'file_tasks.task_status_id'
//            );
        
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ; 
            
            if ($query_array['order_by'] == 'relevance') {
                $query_array['order_by'] = 'file_tasks.task_title' ; 
                $query_array['order_by_relevance'] = 'yes' ; 
                }  
            
            if (isset($query_options->order_field)) {
                $query_array['order_field'] = $query_options->order_field ;
                $query_array['order_start'] = $query_options->order_start ;
                $query_array['order_operator'] = $query_options->order_operator ;
                }           
            } 
        
        
        // PROCESS USER ID FILTER
        switch($query_options->filter_by_user_id) {                    
            case 'no':
                
                break ;
            case 'yes':                
            default: 
                $query_array['where'] .= "AND file_tasks.user_id='$this->user_id' " ;
            }        
        
        // PROCESS ACCOUNT ID FILTER
        switch($query_options->filter_by_account_id) {
            case 'no':    
                
                break ;
            case 'yes': 
            default: 
                $query_array['where'] .= "AND file_tasks.account_id='$this->account_id' " ;
            }
        
        
        if (isset($query_options->filter_by_task_status)) {
            
            $query_options->filter_by_task_status = Utilities::Array_Force($query_options->filter_by_task_status) ; 
            
            $filter_by_file_task_status_string = '' ; 
            foreach ($query_options->filter_by_task_status as $query_file_task_status) {
                $filter_by_file_task_status_string .= "file_tasks.task_status='$query_file_task_status' OR " ; 
                }
            $filter_by_file_task_status_string = rtrim($filter_by_file_task_status_string," OR ") ;
            $filter_by_file_task_status_string = " AND ($filter_by_file_task_status_string) " ; 
            
            $query_array['where'] .= " $filter_by_file_task_status_string " ; 
            }
        
        
        // PROCESS FILE TASK IDs
        if (isset($query_options->filter_by_file_task_id)) {
            
            $query_options->filter_by_file_task_id = Utilities::Array_Force($query_options->filter_by_file_task_id) ; 
            
            $filter_by_file_task_id_string = '' ; 
            foreach ($query_options->filter_by_file_task_id as $query_file_task_id) {
                $filter_by_file_task_id_string .= "file_tasks.file_task_id='$query_file_task_id' OR " ; 
                }
            $filter_by_file_task_id_string = rtrim($filter_by_file_task_id_string," OR ") ;
            $filter_by_file_task_id_string = " AND ($filter_by_file_task_id_string) " ; 
            
            $query_array['where'] .= " $filter_by_file_task_id_string " ; 
            }
        
        
        // PROCESS ASSET IDs
        if (isset($query_options->filter_by_asset_id)) {
            
            $query_options->filter_by_asset_id = Utilities::Array_Force($query_options->filter_by_asset_id) ; 
            
            $filter_by_asset_id_string = '' ; 
            foreach ($query_options->filter_by_asset_id as $query_asset_id) {
                $filter_by_asset_id_string .= "assets.asset_id='$query_asset_id' OR " ; 
                }
            $filter_by_asset_id_string = rtrim($filter_by_asset_id_string," OR ") ;
            $filter_by_asset_id_string = " AND ($filter_by_asset_id_string) " ;    
            
            $query_array['where'] .= " $filter_by_asset_id_string " ; 
            }
        
    
        
        // PROCESS VISIBILITY FILTER        
        // Submitted as an array of visibility_names's which is converted into, and added to the visibility_name array
        if (isset($query_options->filter_by_visibility_name)) {

            $visibility_name_array = Utilities::Array_Force($query_options->filter_by_visibility_name) ; 
            
            $query_visibility_array = array(
                'table' => 'system_visibility_levels',
                'fields' => "*",
                'where' => ""
                ) ;

            foreach ($visibility_name_array as $visibility_name) {
                switch ($visibility_name) {
                    case 'admin_all': // We should provide special treatment for hiddden since it's admin only...
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_hidden' OR " ;  
                    case 'all':
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_deleted' OR " ; 
                    case 'visible':    
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_draft' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_shared' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_published' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_review' OR " ; 
                        break ; 
                    default:
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='$visibility_name' OR " ; 
                    }
                }
            $query_visibility_array['where'] = rtrim($query_visibility_array['where']," OR ") ;
            $visibility_result = $this->DB->Query('SELECT',$query_visibility_array,'force');   

            foreach ($visibility_result['results'] as $visibility) {
                $visibility_array[] = $visibility ; 
                }
            }

        
        
        // Add visibility filter if there are visibility_id's to process
        $filter_by_visibility_id_string = '' ; 
        $filter_by_published_visibility_id_string = '' ; 
        if (count($visibility_array) > 0) {

            foreach ($visibility_array as $visibility) {
                
                $this_visibility_id = $visibility['visibility_id'] ; 
                $filter_by_visibility_id_string .= "file_tasks.visibility_id='$this_visibility_id' OR " ;
                
                switch ($visibility['visibility_name']) {
                    case 'visibility_shared':
                    case 'visibility_published':
                        $filter_by_published_visibility_id_string .= "file_tasks.visibility_id='$this_visibility_id' OR " ;        
                        break ; 
                    }
                
                
                }
            $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," OR ") ; 
            $filter_by_visibility_id_string = " AND ($filter_by_visibility_id_string) " ;
            
            $query_array['where'] .= " $filter_by_visibility_id_string " ; 
            }
        
        
        
        // PROCESS TASK UPDATED FILTER
        if ($query_options->file_task_updated_filter_status == 'active') { 
            
            $now = TIMESTAMP ; 
            
            $today_midnight = new DateTime() ; 
            $today_midnight->setTimezone(new DateTimeZone($timezone)) ;  
            $today_midnight_timestamp = $today_midnight->setTimestamp(TIMESTAMP)->setTime( 23, 59, 59 )->getTimestamp() ;

            
            switch ($query_options->file_task_updated_filter_type) {
                case 'range_next': // in minutes
                    
                    $range_next = Utilities::System_Time_Manipulate($query_options->file_task_updated_filter_range_next_mins.' m','+',TIMESTAMP) ; 
                    $range_next_timestamp = $range_next['time_manipulated'] ;
                    $query_array['where'] .= "AND (file_tasks.timestamp_task_updated>$now AND file_tasks.timestamp_task_updated<$range_next_timestamp) " ; 
            
                    break ; 
                case 'range_last':
                    
                    $range_last = Utilities::System_Time_Manipulate($query_options->file_task_updated_filter_range_last_days.' m','-',TIMESTAMP) ; 
                    $range_last_timestamp = $range_last['time_manipulated'] ;
                    $query_array['where'] .= "AND (file_tasks.timestamp_task_updated>$range_last_timestamp AND file_tasks.timestamp_task_updated<$now) " ; 
            
                    break ;                    
                case 'range_between':
                    
//                    $range_between_begin = new DateTime($query_options->file_task_updated_filter_range_between_begin) ; 
//                    $range_between_begin->setTimezone(new DateTimeZone($timezone)) ;  
//                    $range_between_begin_timestamp = $range_between_begin->getTimestamp() ;
//                    
//                    $range_between_end = new DateTime($query_options->file_task_updated_filter_range_between_end) ; 
//                    $range_between_end->setTimezone(new DateTimeZone($timezone)) ;  
//                    $range_between_end_timestamp = $range_between_end->getTimestamp() ;

                    $query_array['where'] .= "AND (file_tasks.timestamp_task_updated>$query_options->file_task_updated_filter_range_between_begin AND file_tasks.timestamp_task_updated<$query_options->file_task_updated_filter_range_between_end)" ; 
            
                    break ;                    
                    
                }             
            }
        
        
        
        
        // Filter by tags
        if (isset($query_options->filter_by_tag_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_tag_metadata_id,","),",") ;
            $query_options->filter_by_tag_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_tag_metadata_id) ;
            $count_metadata = count($query_options->filter_by_tag_metadata_id) ; 
                        
            $join_table_name = 'tag_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->tag_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            
            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='file_task' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (file_tasks.file_task_id=$join_table_name.item_id)"
                );                         
            }
        
        
        
        // Filter by categories
        if (isset($query_options->filter_by_category_metadata_id)) {
            
            $metadata_comma_query_string = ltrim(rtrim($query_options->filter_by_category_metadata_id,","),",") ;
            $query_options->filter_by_category_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_category_metadata_id) ;
            $count_metadata = count($query_options->filter_by_category_metadata_id) ; 
                        
            $join_table_name = 'category_metadata_query' ;  
            
            // Create the operator
            switch ($query_options->category_metadata_search_operator) {
                case 'AND': 
                    $join_type = 'join' ; 
                    $metadata_search_operator = "= $count_metadata" ;
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                    break ; 
                case 'NOT IN':
                    
                    $join_type = 'left' ; 
                    $query_array['where'] .= "AND $join_table_name.relationship_id IS NULL " ; 
                    
                    break ;
                case 'both':
                    
                    $join_type = 'left' ; 

                    break ;                    
                case 'OR':
                default: 
                    $join_type = 'join' ; 
                    $metadata_search_operator = ">= 1" ;      
                    $metadata_having_string = "HAVING COUNT(1) $metadata_search_operator " ; 
                }            

            $query_array['join_tables'][] = array(
                'table' => "
                    (SELECT nonasset_metadata_relationships.*, 
                    metadata.metadata_name, metadata.metadata_slug, metadata.metadata_description  
                    FROM nonasset_metadata_relationships 
                    JOIN metadata ON nonasset_metadata_relationships.metadata_id = metadata.metadata_id 
                    JOIN metadata_type ON metadata_type.metadata_type_id = metadata.metadata_type_id 
                    WHERE 
                        nonasset_metadata_relationships.metadata_id IN ($metadata_comma_query_string) 
                        AND nonasset_metadata_relationships.relationship_type='file_task' 
                        GROUP BY nonasset_metadata_relationships.item_id    
                        $metadata_having_string)
                        AS $join_table_name",
                'type' => $join_type,
                'statement' => "
                        (file_tasks.file_task_id=$join_table_name.item_id)"
                );                         
            }
        
        
        
        

        
        // Add filter by search parameter
        if (isset($query_options->filter_by_search_parameter)) {

            $search_parameter_token = explode(" ",$query_options->filter_by_search_parameter) ; 
            
            $compiled_search_string = '' ; 
            
            foreach ($search_parameter_token as $search_token) {
                $this_search_string = " (
                    file_tasks.task_title LIKE '%$search_token%' OR 
                    assets.asset_title LIKE '%$search_token%'
                    ) " ; 
                
                $compiled_search_string .= $this_search_string.' OR ' ; 
                }
            
            $compiled_search_string = rtrim($compiled_search_string," OR ") ;     
            $query_array['where'] .= " AND ($compiled_search_string) " ; 
                                   
            }
        
        
        if (isset($query_array['where'])) {
            $query_array['where'] = ltrim($query_array['where']," AND ") ;            
            }


        // Add sort filter 
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ;           
            }
        
        
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        
        if (($query_array['order_by_relevance'] == 'yes') AND (isset($query_options->filter_by_search_parameter))) {
            
            $relevance_options = array(
                'fields' => array('task_title','asset_title') 
                ) ; 
            $result = $this->Sort_Search_Relevance($result,$query_array,$query_options,$relevance_options) ; 
            } 
                
        $query_options->value_name = 'file_task_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_File_Paging($result) ;         
        $this->file_query_result = $result ; 
                
        return $result ;       
        }
    
    
    
    
    
    public function Retrieve_File_Task_Data_List($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        

        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write


        //
        // PROCESSING
        //
                

        
        // Base query:
        $query_array = array(
            'table' => $query_options->table,
            'fields' => "*",
            'where' =>"$query_options->table.item_id > 0",
            'group_by' => "$query_options->table.item_id"
            );

        
        
        // Add sort filter 
        if (isset($query_options->filter_by_processed)) {
            $query_array['where'] .= " AND ($query_options->table.processed=$query_options->filter_by_processed) " ;   
            }
        
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ; 
            
            if ($query_array['order_by'] == 'relevance') {
                $query_array['order_by'] = 'file_tasks.task_title' ; 
                $query_array['order_by_relevance'] = 'yes' ; 
                }  
            
            if (isset($query_options->order_field)) {
                $query_array['order_field'] = $query_options->order_field ;
                $query_array['order_start'] = $query_options->order_start ;
                $query_array['order_operator'] = $query_options->order_operator ;
                }           
            } 
        


        // Add sort filter 
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ;           
            }
        
        
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->file_query_result = $result ; 
                
        $query_options->value_name = 'item_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_File_Paging($result) ;         
        
                
        return $result ;       
        }    
        
}