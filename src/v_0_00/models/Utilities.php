<?php

namespace Ragnar\Ironsides ;

// $User = new User(1);
// print_r($User) ;
//$User->set_email_address('bob@email.com');
//echo $User->get_email_address();
//echo $User->first_name; 

class Utilities {
    
    // MYSQLI escape using global database instance
    static public function Escape($input) {
    
        global $DB ; 
        $escaped_input = $DB->_connection->real_escape_string($input) ; 
        return $escaped_input ; 
        
        }
    
    
    // Checks to ensure the email address is valid and converts it to lowercase
    static public function Validate_Email_Address($email_address) {
    
        $result = array( 
            'email_address_original' => $email_address,
            'email_address' => strtolower(str_replace(" ","",$email_address)),
            'valid' => 'no'
            ) ; 
        
        if (filter_var($result['email_address'], FILTER_VALIDATE_EMAIL)) {
            $result['valid'] = 'yes' ; 
            
            $email_parts = explode('@',$result['email_address']) ;

            $result['email_username'] = $email_parts[0] ; 
            $result['email_domain'] = $email_parts[1] ; 
            
            } 
        
        return $result ; 
        }
    

    
    // Checks to ensure the email address is valid and converts it to lowercase
    static public function Validate_Password($password,$password_confirm = 'none') {
    
        $result = array(
            'password' => $password,
            'password_confirm' => $password_confirm,
            'valid' => 'no'
            ) ; 
        
        if (strlen($result['password']) > 7) {
            $result['valid'] = 'yes' ; // Entry is over 7 digits long
            } else {
                $result['error'] = 'length' ; 
                }
        
        if (($result['valid'] == 'yes') AND ('none' !== $password_confirm)) {
            $new_hash = Password::hashPassword($password);
            $match = Password::checkPassword($password,$new_hash);
            
            switch ($match) {
                case true:
                    $result['valid'] = 'yes' ; 
                    break ;
                case false:
                    $result['valid'] = 'no' ; 
                    $result['error'] = 'mismatch' ; 
                    break ;
                }
            }
        
        return $result ; 
        }
    
    
    // Use this to process incoming requests, and determine if it's an attempted dictionary attack
    static public function Prevent_Dictionary($values_array) {
    
        $timestamp_bounds = 5 ; // seconds
        $max_attempts = 3 ; // Within the time bounds
        $lockout_period = 1 ; // minutes
            
            
        $fail = 'fail' ; 
        $pass = 'pass' ; 
        
        $request_url = $values_array['request_url'] ; 
        $user_ip = $values_array['user_ip'] ; 
        $timestamp_lower = TIMESTAMP - $timestamp_bounds ; 
        
        global $DB ; 
        
        // Log this request
        $query_array = array(
            'table' => 'system_log_requests',
            'values' => array(
                'request_url' => $request_url,
                'user_ip' => $user_ip,
                'timestamp' => TIMESTAMP
                )
            );
        
        $log = $DB->Query('INSERT',$query_array);
        
        $log_id = $log['insert_id'] ; 
        
        // Select past three matching requests
        $query_array = array(
            'table' => 'system_log_requests',
            'fields' => "*",
            'where' => "request_url='$request_url' AND user_ip='$user_ip'",
            'order_by' => 'timestamp',
            'order' => 'DESC'
            );
        
        $log_history = $DB->Query('SELECT',$query_array);
        
        $result = array(
            'request_url' => $request_url,
            'user_ip' => $user_ip,
            'history_count' => $log_history['result_count']
            ) ; 
        
        
        if (($log_history['results'][1]['fail'] == 1) AND ($log_history['results'][1]['timestamp'] > (TIMESTAMP - (60 * $lockout_period)))) {    
                
            $result['status'] = $fail ; 
            $result['message'] = Utilities::System_Alert(10)['alert_message_clean'] ; 

            // Updates the log id with a fail indicator
            $values_array = array(
                'fail' => 1
                ) ;

            $query_array = array(
                'table' => 'system_log_requests',
                'values' => $values_array,
                'where' => "log_id='$log_id'"
                );

            $log_update = $DB->Query('UPDATE',$query_array);
            
            } else {
            
                // Select past three matching requests within time bounds
                $query_array = array(
                    'table' => 'system_log_requests',
                    'fields' => "*",
                    'where' => "request_url='$request_url' AND user_ip='$user_ip' AND timestamp>$timestamp_lower",
                    'order_by' => 'timestamp',
                    'order' => 'DESC'
                    );

                $log_history = $DB->Query('SELECT',$query_array);

                if ($log_history['result_count'] > $max_attempts) {
            
                    $result['status'] = $fail ; 
                    $result['message'] = Utilities::System_Alert(10)['alert_message_clean'] ; 
                    
                     // Updates the log id with a fail indicator
                    $values_array = array(
                        'fail' => 1
                        ) ;

                    $query_array = array(
                        'table' => 'system_log_requests',
                        'values' => $values_array,
                        'where' => "log_id='$log_id'"
                        );

                    $log_update = $DB->Query('UPDATE',$query_array);
                    
                    } else {
            
                        $result['status'] = $pass ; 
            
                        }
            
                }

        
        return $result ; 
        }
    
    
    // Log query name & length
    static public function Log_App_Query($values_array) {
    
        $timestamp_bounds = 5 ; // seconds
        $max_attempts = 3 ; // Within the time bounds
        $lockout_period = 1 ; // minutes
            
            
        $fail = 'fail' ; 
        $pass = 'pass' ; 
        
        $request_url = $values_array['request_url'] ; 
        $timestamp_lower = TIMESTAMP - $timestamp_bounds ; 
        
        $begin_microtimestamp = MICRO_TIMESTAMP ; 
        $end_microtimestamp = microtime(true) ;  
        $query_elapsed = $end_microtimestamp - $begin_microtimestamp ; 

        if (isset($values_array['microtime'])) {
            $query_elapsed = $end_microtimestamp - $values_array['microtime'] ; 
            }
                
        $values_input_array = array(
            'request_url' => $request_url,
            'timestamp' => TIMESTAMP,
            'query_elapsed' => $query_elapsed
            ) ;
        
        if (isset($values_array['request_name'])) {
            $values_input_array['request_name'] = $values_array['request_name'] ; 
            }
        if (isset($values_array['user_id'])) {
            $values_input_array['user_id'] = $values_array['user_id'] ; 
            }
        if (isset($values_array['account_id'])) {
            $values_input_array['account_id'] = $values_array['account_id'] ; 
            }        
        
        
        global $DB ; 
        
        // Log this request
        $query_array = array(
            'table' => 'system_log_query',
            'values' => $values_input_array
            );
        
        $log = $DB->Query('INSERT',$query_array);
        
        $log_id = $log['insert_id'] ; 
        
        return $result ; 
        }
    
    
    static public function System_Alert($alert_id) {
    
        global $DB ; 
        
        $query_array = array(
            'table' => 'system_alerts',
            'fields' => "*",
            'where' => "alert_id='$alert_id'"
            );
        
        $result = $DB->Query('SELECT',$query_array);
        
        $response = array() ; 
        
        $response['alert_id'] = $result['results']['alert_id'] ; 
        $response['result'] = $result['results']['type'] ; 
        $response['alert_message_code'] = $result['results']['alert_message'].' (Ref. Code: '.$result['results']['alert_id'].')'  ;
        $response['alert_message_clean'] = $result['results']['alert_message'] ;
        $response['alert_admin_notes'] = $result['results']['admin_notes'] ;
        
        return $response ; 
        
        }
    
    static public function System_Date($format,$timestamp) {
    
        switch($format) {            
            case 'm/d/yy':
                
                break ;
            case 'm/d/yy @ time':
                $date = date("n/j/y",$timestamp) ;
                $time = date("g:i:s a",$timestamp) ;
                $formatted_time = $date.' @ '.$time ; 
                break ;                
            case 'm/d/yyyy':
                
                break ;                
            case 'm/d/yyyy @ time':
                //$formatted_time = date("",$timestamp) ;
                break ;

            }
        
        return $formatted_time ; 
        
        }
    
    
    // Sum a comma separated string of time measurements into seconds
    // Allows it to easily be maniupated against a Unix timestamp
    // time_string = the manipulation you want to occur in the form of a string
    // operator = the direction manipulation should go (+ or -)
    // time_base = the timestamp starting point from which manipulation should orginate (current timestamp by default)
    static public function System_Time_Manipulate($time_string,$operator = 'none',$time_base = 'now') {
    
        // SAMPLE TIME STRING: '1 y,13 w,40 d,14 h,15 m,35 s'
        // Convert that string to an array and manipulate the time accordingly
        // Individual pieces calculated 60 seconds * 60 mins * 24 hours * xx days * yy years
        $time_array = explode(",",$time_string) ; 
        $time_sum = 0 ; 
        
        if ('now' === $time_base) {
            $time_base = TIMESTAMP ;
            }         
        
        foreach ($time_array as $element) {

            $pieces = explode(" ",$element) ; 

            switch ($pieces[1]) {
                
                case 'y':
                    // Assumes 365 days in a year  
                    $this_time_sum = (60 * 60 * 24 * 365 * $pieces[0]) ; 
                    $time_sum = $time_sum + $this_time_sum ; 
                    break ;
                case 'w':
                    // Assumes 7 days in a week  
                    $this_time_sum = (60 * 60 * 24 * 7 * $pieces[0]) ; 
                    $time_sum = $time_sum + $this_time_sum ;                     
                    break ;
                case 'd':
                    $this_time_sum = (60 * 60 * 24 * $pieces[0]) ; 
                    $time_sum = $time_sum + $this_time_sum ;                    
                    break ;
                case 'h':
                    $this_time_sum = (60 * 60 * $pieces[0]) ; 
                    $time_sum = $time_sum + $this_time_sum ;
                    break ;
                case 'm':
                    $this_time_sum = (60 * $pieces[0]) ; 
                    $time_sum = $time_sum + $this_time_sum ;                    
                    break ;
                case 's':
                    $this_time_sum = $pieces[0] ; 
                    $time_sum = $time_sum + $this_time_sum ;                    
                    break ;                    
                    
                }
            
            }
        
        switch($operator) {            
            case '+':
                $time_manipulated = $time_base + $time_sum ; 
                break ;
            case '-':
                $time_manipulated = $time_base - $time_sum ; 
                break ;                
            default:
                $time_manipulated = null ; 
                $operator = null ; 
                break ;

            }
        
        $dataset = array(
            'timestamp_manipulated' => $time_manipulated
            ) ; 
        $system = new System() ; 
        $timestamp_manipulated = $system->Action_Time_Territorialize_Dataset($dataset) ; 
        
        $result = array(
            'time_sum' => $time_sum, // Sum of seconds manipulated
            'time_manipulated' => $time_manipulated, // Result of seconds manipulated (a timestamp)
            'time_base' => $time_base, // Starting timestamp
            'operator' => $operator, // + or -
            'array' => $time_array, // Manipulation items
            'pieces' => $pieces, // Manipulation items
            'time_manipulated_in_days' => $time_sum/86400, // Estimate of time manipulated in days
            'timestamp_manipulated' => $timestamp_manipulated['timestamp_manipulated'], // Timestamp result, same as time_manipulated
            'timestamp_manipulated_compiled' => $timestamp_manipulated['timestamp_manipulated_compiled'] // Result territorialized in human terms
            ) ;
        
        return $result ; 
        }    

    
    /** 
     * Convert number of seconds into hours, minutes and seconds 
     * and return an array containing those values 
     * 
     * @param integer $inputSeconds Number of seconds to parse 
     * @return array 
     * https://stackoverflow.com/questions/8273804/convert-seconds-into-days-hours-minutes-and-seconds/12118442
     */ 
    static public function System_Time_Convert_Seconds($inputSeconds,$options = array()) {

        $pluralize = new View() ; 
        $pluralize->Action_Pluralize_Content($singular_word,$word_count) ; 
        
        $daysInAYear = 365 ; 
        $secondsInAMinute = 60;
        $secondsInAnHour  = 60 * $secondsInAMinute;
        $secondsInADay    = 24 * $secondsInAnHour;

        
        
        
        if ($options['ingore_years'] == 'yes') {
            $daySeconds = $inputSeconds ;  
            } else {
                // extract years
                $years = floor($inputSeconds / ($secondsInADay * $daysInAYear));
                $daySeconds = $inputSeconds % ($secondsInADay * $daysInAYear) ;
                }
        
        
        // extract days
        $days = floor($daySeconds / $secondsInADay);

        // extract hours
        $hourSeconds = $inputSeconds % $secondsInADay;
        $hours = floor($hourSeconds / $secondsInAnHour);

        // extract minutes
        $minuteSeconds = $hourSeconds % $secondsInAnHour;
        $minutes = floor($minuteSeconds / $secondsInAMinute);

        // extract the remaining seconds
        $remainingSeconds = $minuteSeconds % $secondsInAMinute;
        $seconds = ceil($remainingSeconds);


        // return the final array
        $obj = array(
            'years' => (int) $years,
            'days' => (int) $days,
            'hours' => (int) $hours,
            'minutes' => (int) $minutes,
            'seconds' => (int) $seconds,
            'seconds_total' => $inputSeconds
            );
        
        $text_display = '' ; 
        if ($obj['days'] > 0) {
            $text_display .= $obj['days'].' '.$pluralize->Action_Pluralize_Content('day',$obj['days']) ; 
            } 
        
        $text_hours = $obj['hours'].' '.$pluralize->Action_Pluralize_Content('hour',$obj['hours']) ; 
        if (($obj['days'] > 0) AND ($obj['hours'] > 0)) {
            $text_display .= ' & '.$text_hours ;
            } else {
                if ($obj['hours'] > 0) {
                    $text_display .= $text_hours ; 
                    }
                }
        
        $text_minutes = $obj['minutes'].' '.$pluralize->Action_Pluralize_Content('minute',$obj['minutes']) ;
        if ((($obj['days'] > 0) OR ($obj['hours'] > 0)) AND ($obj['minutes'] > 0)) {
            $text_display = str_replace(' & ',', ',$text_display).' & '.$text_minutes ;
            } else {
                if ($obj['minutes'] > 0) {
                    $text_display .= $text_minutes ;
                    }
                }        
        
        $text_seconds = $obj['seconds'].' '.$pluralize->Action_Pluralize_Content('second',$obj['seconds']) ; 
        if ((($obj['days'] > 0) OR ($obj['hours'] > 0) OR ($obj['minutes'] > 0)) AND ($obj['seconds'] > 0)) {
            $text_display = str_replace(' & ',', ',$text_display).' & '.$text_seconds ;
            } else {
                if ($obj['seconds'] > 0) {
                    $text_display .= $text_seconds ;
                    }
                }        
        
        $obj['text_display'] = $text_display ; 
        
        return $obj;
    }
    
    
    
    
    // Use ReflectionObject to access the protected / private properties of an object
    static public function Reflection($property_name,$object) {
    
        $refObj  = new ReflectionObject($object);
        
        $reference_property = $refObj->getProperty($property_name);
        $reference_property->setAccessible(TRUE);
        return $reference_property->getValue($object);
 
        
        }
    
    
    
    
    
    // NUMBER MANIPULATION
    static public function Start_Page_To_Offset($start_page) {
        $offset_page = $start_page - 1 ; 
        return $offset_page ;
        }
    static public function Offset_To_Start_Page($offset_page) {
        $start_page = $offset_page + 1 ; 
        return $start_page ;
        }
    
    // TEXT MANIPULATION    
    static public function Format_Convert_Lowercase($text) {
        $text = strtolower($text) ; 
        return $text ;
        }
    
    static public function Format_Remove_Whitespace($text) {
        $text = str_replace(" ","",$text) ; // remove whitespace
        return $text ;
        }
    
    static public function Format_Convert_Spaces($text,$options = array()) {
        
        if (!isset($options['search_for'])) {
            $options['search_for'] = '&nbsp;' ;
            }
        if (!isset($options['replace_with'])) {
            $options['replace_with'] = ' ' ;
            }
        
        $text = str_replace($options['search_for'],$options['replace_with'],$text) ; // remove whitespace
        return $text ;
        }
    
    static public function Format_URL_To_Link($text,$options = array()) {
        
        // The Regular Expression filter
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        
        if (isset($options['target'])) {
            switch ($options['target']) {
                case '_blank':
                    $target = 'target="_blank"' ; 
                    break ;
                default:
                    $target = '' ; 
                    
                }
            }
        
        // Check if there is a url in the text
        if(preg_match($reg_exUrl, $text, $url)) {

            // make the urls hyper links
            $text = preg_replace($reg_exUrl, '<a href="'.$url[0].'" target="_blank" '.$target.'>'.$url[0].'</a> ', $text) ;

            } 
        
        return $text ; 
        
        }
    
    
   
    
    // Checks to ensure the phone number is valid 
    static public function Validate_Phone_Number($phone_number,$country_dialing_code = '+1') {
    
        $result = array(
            'phone_number_original' => $phone_number,
            'phone_number' => preg_replace('/[^0-9]*/', '', $phone_number), // Removes spaces, dashes, parenthesis and the plus before international code
            'valid' => 'no',
            'phone_country_dialing_code' => $country_dialing_code
            ) ; 
        
        if (strlen($result['phone_number']) >= 7) {
            $result['valid'] = 'yes' ; // Entry is 7 or more digits long
            } else {
                $result['valid'] = 'no' ; 
                }
        
        return $result ; 
        }
    
    static public function Format_Phone_Number($phone_number,$country_dialing_code = '+1') {
 
//        $phone_number = preg_replace('/[^0-9+]*/','',$phone_number);
 
        $phone_number_submitted = $phone_number ; 
        
        
        // This should remove the country_dialing_code off the front of the phone_number string
        if (substr($phone_number, 0, strlen($country_dialing_code)) == $country_dialing_code) {
            $phone_number = substr($phone_number, strlen($country_dialing_code)) ;
            }
        
        // Extract + sign from country dialing code
        $country_dialing_code_no_plus = preg_replace('/[^0-9+]*/','',$country_dialing_code) ;
        if (substr($phone_number, 0, strlen($country_dialing_code_no_plus)) == $country_dialing_code_no_plus) {
            $phone_number = substr($phone_number, strlen($country_dialing_code_no_plus)) ;
            }        
        

        
        $unformatted_phone_number = $phone_number ; 
            
        switch ($country_dialing_code) {
            case '+1': // US
                if(strlen($unformatted_phone_number) > 10) {
                    $countryCode = substr($unformatted_phone_number, 0, strlen($unformatted_phone_number)-10);
                    $areaCode = substr($unformatted_phone_number, -10, 3);
                    $nextThree = substr($unformatted_phone_number, -7, 3);
                    $lastFour = substr($unformatted_phone_number, -4, 4);

                    $local_phone_number_pretty = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
                    $phone_number = '+'.$countryCode.' '.$local_phone_number_pretty ;

                } else if (strlen($unformatted_phone_number) == 10) {
                    $areaCode = substr($unformatted_phone_number, 0, 3);
                    $nextThree = substr($unformatted_phone_number, 3, 3);
                    $lastFour = substr($unformatted_phone_number, 6, 4);

                    $local_phone_number_pretty = '('.$areaCode.') '.$nextThree.'-'.$lastFour;                    
                    $phone_number = $country_dialing_code.' '.$local_phone_number_pretty;

                } else if (strlen($phone_number) == 7) {
                    $nextThree = substr($phone_number, 0, 3);
                    $lastFour = substr($phone_number, 3, 4);

                    $phone_number = $nextThree.'-'.$lastFour;
                    
                    }       
                break ;  
            case '+64': // New Zealand
                $local_phone_number_pretty = $unformatted_phone_number ; 
                $phone_number = $country_dialing_code.' '.$local_phone_number_pretty ; 
                break ;
            default: 
                $local_phone_number_pretty = $unformatted_phone_number ; 
                $phone_number = $country_dialing_code.' '.$local_phone_number_pretty ; 
                break ;                
            }
        
        $international_phone_number = $country_dialing_code.$unformatted_phone_number ; 
        
        
        $result = array(
            'unformatted' => $unformatted_phone_number, // 6157520898
            'international' => $international_phone_number,  // +16157520898
            'international_pretty' => $phone_number,  // +1 (615) 752-0898
            'local' => ltrim($international_phone_number,$country_dialing_code), // 6157520898
            'local_pretty' => $local_phone_number_pretty, // (615) 752-0898
            'phone_country_dialing_code' => $country_dialing_code, // +1
            'area_code' => $areaCode // 615
            ) ; 

        
        
        return $result;
        }
    
    
    
    static public function Extract_Text_Block($haystack, $needle_start, $needle_end, $start_position = 0) {
        
        $unit = '' ;
        
        $pos = stripos($haystack, $needle_start, $start_position);
        
        if ($pos != FALSE) {
            $str = substr($haystack, $pos);
            $str_two = substr($str, strlen($needle_start));
            $second_pos = stripos($str_two, $needle_end);
            $str_three = substr($str_two, 0, $second_pos);
            $unit = trim($str_three); // remove whitespaces
            }

        $result = array(
            'extraction' => $unit,
            'position' => $pos,
            'next_position' => $pos + 1
            ) ; 
        
        return $result;
        }    
    
    
    // Remove all instances of an extract string (the needle) from the END of the string
    static public function Remove_String_Right($haystack, $needle, $start_position = 0) {
        
        $continue = 1 ; 
        $original_length = strlen($haystack) ; 
        
        do {

            $length = strlen($haystack) ; 
            $pos = strrpos($haystack, $needle, $start_position);

            if (($pos != FALSE) AND ($pos + strlen($needle) == $length)) {

                $remainder = substr($haystack, 0, $pos);
                $haystack = $remainder ;

                } else {
                    $continue = 0 ; 
                    $remainder = $haystack ; 
                    }

            } while ($continue == 1) ; 


        $result = array(
            'final_string' => $remainder,
            'final_string_length' => $length,
            'position' => $pos, // Of current occurence of needle
            'next_position' => $pos + 1,            
            'original_length' => $original_length
            ) ;
        
        return $result;
        } 
    
    
    // Convert a comma separated string into an array
    // $input options:
    //        23 OPTION B
    //
    //        23,98,294  OPTION A
    //
    //        [0] => 23  OPTION C
    //        [1] => 98
    //        [2] => 294
    //
    //        [0] => array('some_id' => 23)  OPTION C
    //        [1] => array('some_id' => 98)
    //        [2] => array('some_id' => 294)            
    static public function Process_Comma_Separated_String($input,$alternate_separator = ',') {

        $output = array() ; 
        
        if ((!is_numeric($input)) AND (!is_array($input))) { // OPTION A: This must be a string (that is not a number) AND it cannot be an array
            if ($input) {
                $input = ltrim($input,$alternate_separator) ; 
                $output = explode($alternate_separator,rtrim($input,$alternate_separator)) ; 
                
                $i = 0 ; 
                $clean_output = array() ; 
                foreach ($output as $item) {
                    $clean_output[$i] = rtrim(ltrim($item)) ;
                    $i++ ; 
                    }
                $output = $clean_output ; 
                } 
            } else { // The input is either an array, or a string that is a single number
                if (is_array($input)) {  // OPTION C: Array or multidimensional array
                    $output = $input ;
                    } else {
                        $output[] = $input ;  // OPTION B: String that is a single number
                        }      
                } 

//        $output['isarray'] = !is_array($input) ; 
//        $output['isnumberic'] = !is_numeric($input) ; 
//        $output['sep'] = $alternate_separator ; 
        return $output ; 

        }
    
   static public function Create_Comma_Separated_String($array_of_id) {

        $new_comma_separated_string = '' ;
        foreach ($array_of_id as $id) {
            $new_comma_separated_string .= ','.$id  ;
            }

        return Utilities::Trim_Comma_Separated_String($new_comma_separated_string)  ;
        }
    
   static public function Trim_Comma_Separated_String($comma_separated_string) {

        $output = ltrim(rtrim($comma_separated_string,","),",") ;
       
        return $output ; 
        }
    
    
    static public function Get_Characters($input_string,$start_position = 0,$count = 10,$options = array()) {
        
        $output_string = '' ;
        
        $output_string = substr($input_string,$start_position,$count) ; 
               
        $remaining_string = substr($input_string,$count) ;
        if ($remaining_string) {
            
            if ($options['add_ellipsis'] == 'yes') {
                $output_string .= '...' ; 
                }
            
            }
        
        return $output_string ;
        }
    
    static public function Get_Words($input_string,$count = 10,$strip_tags = 'strip_tags',$options = array()) {
        
        if ('strip_tags' === $strip_tags) {
            $input_string = strip_tags($input_string) ; 
            } else {
                $input_string = strip_tags($input_string,$strip_tags) ; 
                }
        
        $output_string = implode(' ', array_slice(explode(' ', $input_string), 0, $count)); 

        $remaining_string = implode(' ', array_slice(explode(' ', $input_string), $count));
        if ($remaining_string) {
            
            if ($options['add_ellipsis'] == 'yes') {
                $output_string .= '...' ; 
                }
            
            }
        
        return $output_string ;       
        }
    
    
    static public function Strip_Tags($input_string,$strip_tags = 'strip_tags') {
        
        if ('strip_tags' === $strip_tags) {
            $output_string = strip_tags($input_string) ; 
            } else {
                $output_string = strip_tags($input_string,$strip_tags) ; 
                }        

        return $output_string ; 
        }
    
    static public function Text_Add_Links($input_string,$options = array()) {
        
        if ($options['target_blank'] == 'yes') {
            $output_string = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a target="_blank" href="$1">$1</a>', $input_string);
            } else {
            $output_string = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1">$1</a>', $input_string);
            }

        return $output_string ; 
        }
 
    
    static public function Implode_Keyed_Array($input_array,$alternate_separator = ',') {

        $output = '' ; 
        
        if (is_array($input_array)) {
            foreach ($input_array as $key => $value) {
                $output .= $value.$alternate_separator ; 
                } 
            $output = rtrim($output,$alternate_separator) ; 
            }
        return $output ; 
        }
    
    
    
    // Turn a string into an array
    // string thing
    // Converts into...
    // [0] => string thing    
    static public function Array_Force($array) {

        if (!is_array($array)) {
            $array = (array) $array ; 
            }

        return $array;
        }
    
    
    // Turn a single level array into a multidimensional array
    // [0] => string thing 1,
    // [1] => string thing 2
    // Converts into...
    // [0] => array(
    //    [0] => string thing 1,
    //    [1] => string thing 2
    //    )
    static public function Array_Force_Multidimensional($array) {

        $array = Utilities::Array_Force($array) ; 

        $array_depth = Utilities::Array_Depth($array) ; 

        if ($array_depth == 1) {
            $clean_array = array() ; 
            $clean_array[] = $array ; 
            } else {
                $clean_array = $array ; 
                }     

        return $clean_array;
        }    
    
    static public function Array_Depth(array $array) {
        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = Utilities::Array_Depth($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;
        }
    
    
    
    // Return the deep array result based off the key number
    static public function Deep_Array($array, $deep_key, $search_value) {

        $deep_array_num = Utilities::Deep_Array_Key($array, $deep_key, $search_value) ; 
        
        if (is_array($array)) {
            return $array[$deep_array_num] ;    
            } 
        }
    
    
    // Return only the key number of the deep array
    static public function Deep_Array_Key($array, $deep_key, $search_value) {

        // Use this to determine which array of an array to use based off a unique search criteria
        // For example, of all Consultant Profiles, which profile to use based off of Profile ID.
        $array_num = 0 ;
        
        if (is_array($array)) {
            foreach ($array as $set){
                if($set[$deep_key] == $search_value) {
                    $deep_array_num = $array_num;
                    }
                $array_num++ ; 
                }
            return $deep_array_num;
            } else {
                return 'error' ;
                }
        
        
        }
    
    
    static public function Nested_Children_Array($raw_string) {    
        // Create nested array from string data
        // This function is fairly specific to the serialized data sent from the Nested-Sortable javascript function
        // Clean the serialized data and place into an array, with nested items as children of parent
        //    0	{…}
        //    asset_id	3
        //    1	{…}
        //    asset_id	6
        //    children	[…]
            //    0	{…}
            //    asset_id	5
            //    1	{…}
            //    asset_id	1
            //    2	{…}
            //    asset_id	4
        //    2	{…}
        //    asset_id	7

        $post_ordered_child_assets = explode('&',$raw_string) ;
        
        $ordered_child_assets = array() ; 

        foreach ($post_ordered_child_assets as $asset_string) {

            $child_asset_parts =  explode('=',$asset_string) ; 
            $child_asset_id = Utilities::Extract_Text_Block($child_asset_parts[0], '[', ']')['extraction'] ; 


            if ($child_asset_parts[1] == 'null') {
                $ordered_child_assets[] = array('asset_id' => $child_asset_id) ; 
                } else {
                    // find this $child_asset_parts[1] in deep array
                    $insert_key = Utilities::Deep_Array_Key($ordered_child_assets, 'asset_id', $child_asset_parts[1]) ; 
                    $ordered_child_assets[$insert_key]['children'][] = array('asset_id' => $child_asset_id) ; 
                    }

            }    
    
    
        return $ordered_child_assets;
        
        }    
    
    
    static public function Obfuscate($input_string) {
        
        $alwaysEncode = array('.', ':', '@');

        $result = '';

        // Encode string using oct and hex character codes
        for ($i = 0; $i < strlen($input_string); $i++) {
            // Encode 25% of characters including several that always should be encoded
            if (in_array($input_string[$i], $alwaysEncode) || mt_rand(1, 100) < 45) {
                if (mt_rand(0, 1)) {
                    $result .= '&#' . ord($input_string[$i]) . ';';
                } else {
                    $result .= '&#x' . dechex(ord($input_string[$i])) . ';';
                }
            } else {
                $result .= $input_string[$i];
            }
        }

        return $result;
        }
    
    static public function Timestamp() {

        $timestamp = time();
        return $timestamp;
        
        }
    
    function Time_Format_Timezone_Name($name) {
        
        $result = array() ;
        
        $name = str_replace('/', ', ', $name);
        $name = str_replace('_', ' ', $name);
        $name = str_replace('St ', 'St. ', $name);
        
        $name_array = explode (', ',$name) ; 
        $result['timezone_pretty'] = $name ; 
        $result['timezone_continent'] = $name_array[0] ;
        $result['timezone_city'] = $name_array[1] ;
        
        return $result;
        }

    function Time_Format_GMT_Offset($offset) {
        $hours = intval($offset / 3600);
        $minutes = abs(intval($offset % 3600 / 60));
        return 'GMT' . ($offset ? sprintf('%+03d:%02d', $hours, $minutes) : '');
        }
    
    
    static public function Create_Variables_From_Array($input) {
        foreach($input as $key => $value) {
            $$key = $value;
            }
        echo $first; // '1st'
        }
    
    static public function Cast_Multi_Array($input) {

        $new_array = array() ; 
        
        // Determine if the input array is multidimensional 
        // https://stackoverflow.com/questions/145337/checking-if-array-is-multidimensional-or-not/994599#994599
        if (count($input) == count($input, COUNT_RECURSIVE)) {
            $new_array[0] = $input ;
            } else {
                foreach ($input as $item) {
                    $new_array[] = $item;
                    }
                }
        
        return $new_array;
        
        }    
    
    static public function CurlExecute($url, $fields, $options = array()) {
        // https://davidwalsh.name/curl-post
        //url-ify the data for the POST
    
        $fields_string = '' ; 
        foreach ($fields as $key=>$value) { 
            $fields_string .= $key.'='.$value.'&'; 
            }
        $fields_string = rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        if (isset($options['auth_bearer'])) {
            //Set your auth headers
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
               'Content-Type: application/x-www-form-urlencoded',
               'Authorization: Bearer ' . $options['auth_bearer']
               ));            
            }

        
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);               // THIS IS THE KEY TO MAKING IT RUN ASYNCHRONOUSLY. Number is the # of seconds to keep the connection open
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
            
        //execute post
        $ch_result = curl_exec($ch);
        $ch_error = curl_error($ch);
    
        $ch_httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
        $ch_response_array = array(
            'http_code' => $ch_httpcode,
            'version' => $_SESSION['version']
            );
    
        //close connection
        curl_close($ch);
    
        $ch_final_result = array(
            'curl_url' => $url,
            'curl_result' => $ch_result,
            'curl_error' => $ch_error,
            'curl_response' => $ch_response_array
            ); 
    
        return $ch_final_result ; 
    
    }
    
    
    // Use this to instantiate basic array templates 
    static public function Template_Array_Frameworks($array_name) {    
        
        $base_array = array() ; 
        
        switch ($array_name) {
            case 'verified_options':
                $base_array[] = array(
                    'verified' => 0,
                    'name' => 'Unverified'
                    ) ; 
                $base_array[] = array(
                    'verified' => 1,
                    'name' => 'Verified'
                    ) ;                 
                break ;
            }
        
        return $base_array ; 
        
        }
}