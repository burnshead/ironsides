<?php

namespace Ragnar\Ironsides ;

class View_Items extends Account {

    public $view_item_set ; // Processed in View.php    
    
    public $final_view_array ; // Processed in View.php
    
    public $view_globals = array(
        'page_load_indicator' => '<div class="text-center text-secondary py-4 animated fadeIn"><i class="fa fa-circle-o-notch fa-spin fa-2x" aria-hidden="true"></i></div>'
        ) ; 


    public function __construct($user_id = 'ignore') {

        global $DB ;  
        $this->DB = $DB ; 
           
        }
    

    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    
    public function Set_View_Item_Set($view_item_set) {
        
        $this->view_item_set = $view_item_set ;       
        
        return $this ; 
        }
    
    
    public function Set_Auth_Permissions($incoming_model) {
        
        $this->auth_permissions = $incoming_model->Get_Auth_Permissions() ;
        $this->asset_auth_permissions = $incoming_model->Get_Asset_Auth_Permissions() ;
        
        if (isset($incoming_model->asset['asset_id'])) {
            $this->asset = $incoming_model->asset ; 
            }
        
        return $this ; 
        }    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_View_Auth_Permissions() {

        $permissions = array(
            'auth_permissions' => $this->auth_permissions,
            'asset_auth_permissions' => $this->asset_auth_permissions,
            'asset_id' => $this->asset['asset_id']
            ) ;
        
        return $permissions ; 
        }
    
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
    
    
    public function Action_Compile_Item($view_name,$item = array(),$data = array()) {
        
        ob_start(); // turn on output buffering
        if (is_file(VIEWS_PATH.'/'.$view_name.'.php')) {
            include VIEWS_PATH.'/'.$view_name.'.php'; 
            $html = ob_get_contents(); // get the contents of the output buffer
            }         
        ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering        

        return $html;         
        }
    
    
    public function Action_Add_View_Item_Data($name,$data) {
        
        $view_item_set = array() ; 

        if (is_array($this->view_item_set)) {
            foreach ($this->view_item_set as $set) {

                $set['data'][$name] = $data ; 
                $view_item_set[] = $set ; 
                }    

            $this->view_item_set = $view_item_set ; 
            }
        
        return $this ; 
        }
    
   
    public function Action_Process_View_Item_Set() {
        
        $final_view_array = array() ;
        
        if (is_array($this->view_item_set)) {        
            foreach ($this->view_item_set as $view_item) {

                if (isset($view_item['system_view_field_id'])) {
                    $field_id_array = Utilities::Process_Comma_Separated_String($view_item['system_view_field_id']) ;
                    $field_query = array(
                        'filter_by_system_view_field_id' => $field_id_array
                        ) ;
                     
                    $field_set = $this->Set_System_View_List_Fields($field_query)->Get_System_View_List_Fields() ;
                    foreach ($field_id_array as $system_view_field_id) {
                        $view_item['data']['data']['list_fields'][] = Utilities::Deep_Array($field_set, 'system_view_field_id', $system_view_field_id) ; 
                        }
                    }
                
                if (isset($view_item['order_by'])) {
                    $view_item['data']['data']['order_by'] = $view_item['order_by'] ; 
                    }                
                if (isset($view_item['row_number'])) {
                    $view_item['data']['data']['row_number'] = $view_item['row_number'] ; 
                    }
                if (isset($view_item['table_view_disable_actions'])) {
                    $view_item['data']['data']['table_view_disable_actions'] = $view_item['table_view_disable_actions'] ; 
                    }                

                switch ($view_item['view_item_name']) {

                    case 'app_viewitem_register_billing_stripe_form':
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name']) ;                         
                        break ;
                    case 'app_viewitem_register_create_user':                      
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['user_input'],$view_item['data']['data']) ;                         
                        break ;
                    case 'app_viewitem_admin_users_list_row':
                    case 'app_viewitem_admin_users_distribution_list_row': 
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['user_record'],$view_item['data']['data']) ;                         
                        break ;                         
                    case 'app_viewitem_register_plan_select': 
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['product_set'],$view_item['data']['data']) ;                         
                        break ;                        
                    case 'app_viewitem_account_billing_plans_coupon_edit':
                    case 'app_viewitem_account_billing_plans_confirm_button':                        
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['product_record'],$view_item['data']['data']) ;                         
                        break ;                        
                    case 'app_viewitem_admin_billing_coupons_edit_modal_title':
                    case 'app_viewitem_admin_billing_coupons_list_row' ; 
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['coupon_record']) ; 
                        break ;
                    case 'app_viewitem_admin_billing_plan_list_row':
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['plan_record']) ; 
                        break ;                      
                    case 'app_viewitem_admin_billing_plans_product_name':
                    case 'app_viewitem_admin_billing_plans_product_description':
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['product_record']) ; 
                        break ;
                    case 'app_viewitem_metadata_array':
                    case 'app_viewitem_contact_lists_row':           
                    case 'app_viewitem_modal_asset_create_campaign_modal_content':    
                    case 'app_viewitem_asset_content_gallery_list_row':  
                    case 'app_viewitem_asset_content_gallery_list_card':      
                    case 'app_viewitem_asset_slideout_asset_details_header': 
                    case 'app_viewitem_asset_editor_header':    
                    case 'app_viewitem_asset_editor_series_component_row':
                    case 'app_viewitem_asset_series_slideout_asset_details_header':    
                        
                        if (isset($view_item['structure_order'])) {
                            $view_item['data']['data']['structure_order'] = $view_item['structure_order'] ; 
                            }
                        if (isset($view_item['parent_asset_id'])) {
                            $view_item['data']['data']['parent_asset_id'] = $view_item['parent_asset_id'] ; 
                            }
                        
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['asset_record'],$view_item['data']['data']) ; 
                        break ; 
                    case 'app_viewitem_asset_series_edit_content_components':    
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['structure_record'],$view_item['data']['data']) ; 
                        break ;                         
                    case 'app_viewitem_asset_tag_array':
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['asset_record'],$view_item['data']['data']) ; 
                        break ;
                    case 'app_viewitem_campaigns_broadcast_list_row':
                    case 'app_viewitem_campaigns_series_list_row':
                    case 'app_viewitem_campaign_header':    
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['campaign_record'],$view_item['data']['data']) ; 
                        break ;  
                    case 'app_viewitem_contacts_imports_exports_list_row':
                    case 'app_viewitem_contacts_slideout_imports_exports_details_header':
                    case 'app_viewitem_import_details_header':
                    case 'app_viewitem_import_details_header_actions':    
                    case 'app_viewitem_import_edit_file_upload':    
                    case 'app_viewitem_import_edit_map_footer_nav':
                    case 'app_viewitem_import_edit_file_upload_footer_nav': 
                    case 'app_viewitem_import_edit_settings_contact_type': 
                    case 'app_viewitem_import_edit_settings_contact_rating':
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['file_task_record'],$view_item['data']['data']) ;                         
                        break ; 
                    case 'app_viewitem_import_edit_map_row':   
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['file_task_map_row'],$view_item['data']['data']) ;                         
                        break ;                        
                    case 'app_viewitem_contacts_list_row':
                    case 'app_viewitem_contacts_distribution_list_row':                       
                    case 'app_viewitem_contacts_edit_modal_title': 
                    case 'app_viewitem_contacts_slideout_contact_details_header':
                    case 'app_viewitem_contact_details_labels':    
                    case 'app_viewitem_contact_details_header':
                        
                    case 'app_viewitem_campaign_recipients_series_list_row':       
                        
                        if (isset($view_item['distribution_list_asset_id'])) {
                            $view_item['data']['data']['distribution_list_asset_id'] = $view_item['distribution_list_asset_id'] ; 
                            }                        
                        
                        
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['contact_record'],$view_item['data']['data']) ;                         
//                        print_r($view_item['data']['data']['sort_name']); 
                        break ; 
                    case 'app_viewitem_contact_notes_list_card':
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['note_record'],$view_item['data']['data']) ;                         
                        break ;
                    case 'app_viewitem_import_review_skipped_row':   
                    case 'app_viewitem_import_review_errors_row':       
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['import_log_record'],$view_item['data']['data']) ;                         
                        break ; 
                    case 'app_viewitem_events_list_row':
                    case 'app_viewitem_events_edit_modal_title':                        
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['event_record'],$view_item['data']['data']) ;                         
                        break ;
                    case 'app_viewitem_metadata_list_row':
                    case 'app_viewitem_metadata_tags_list_row':
                    case 'app_viewitem_metadata_categories_list_row':
                    case 'app_viewitem_slideout_metadata_details_header':    
//                    case 'app_viewitem_contacts_edit_modal_title':                        
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['metadata_record'],$view_item['data']['data']) ;                         
                        break ;                        
                    case 'app_viewitem_admin_content_app_page_list_row':
                    case 'app_viewitem_admin_content_app_page_edit_modal_title':                        
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],$view_item['data']['app_page_record'],$view_item['data']['data']) ;                         
                        break ;
                    default:
                        $view_item['html'] = $this->Action_Compile_Item($view_item['view_item_name'],array(),$view_item['data']['data']) ;                                                 
                    } 

                
                unset($view_item['data']) ; // Reduce payload. Toggle off for QA.
                
                $final_view_array[] = $view_item ; 
                }
            }
        
        $this->final_view_array = $final_view_array ; 
        
        return $this ; 
        }
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    ////////////////////// 
    

    
    
    }


