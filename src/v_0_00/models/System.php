<?php

namespace Ragnar\Ironsides ;

    class System {

        protected $DB ;
        
        public $model_timings_array ; 
        public $model_timing_increment = 1 ;
        
        
        public $system_timezone = 'America/Chicago' ;
        
        public $auth_permissions ; // Supplied by User or Profile
        public $asset_auth_permissions ; // Supplied by Profile Authorization record on the asset
        
        public $system_permissions_list ; 
        public $system_operations_list ; 
        
        public $search_id ;
        public $search ;
        
        public $system_paging ;
        public $page_increment = 12 ; // # of assets to be pulled per page (e.g. )
        
        public $app_view_history_id ; 
        public $app_view_history ; 
        public $app_view_history_list ; 
        
        public $system_view_list_fields ; // The fields used for display a contact list result
        
        public $system_billing_status_levels ; // Stripe billing plan status types
        
        public $system_version_list ; 
        public $system_auth_list ;  
        
        public $physical_address_validation ;
        public $email_address_validation ; 
        public $phone_number_validation ; 
        public $password_validation ;
                
        public $list_countries ; 
        public $list_usa_states ;
        public $list_canada_provinces ;
        public $list_timezones ;        
        public $list_domains ;
        public $list_defaults ;        
        public $list_protected_values ;
        public $list_contact_fields ;
        public $list_contact_field_match ;
        public $app_email_domains ; 
        
        public $system_default_value ; // This is supplied from list_defaults in the database
        
        public $action_key_id ;
        public $action_key ; 
        public $action ; 
        
        protected $alert_id ;        
        public $response ; 
        public $system_action ;         
        
        public $task_automation_id ; 
        
        public $system_setting_id ;
        public $system_setting ;
        
        public $system_query_result ; 
        
        public $system_plurals_list = array(
            'is' => array('are'),
            'it' => array('they'),
            'day' => array('days'),
            'hour' => array('hours'),
            'minute' => array('minutes'),
            'second' => array('seconds'),
            'this' => array('these'),
            'step' => array('steps'),
            'item' => array('items'),
            'original' => array('originals'),
            'credit' => array('credits'),
            'recipient' => array('recipients'),
            'member' => array('members'),
            'user' => array('users'),
            'profile' => array('profiles'),
            'account' => array('accounts'),
            'contact' => array('contacts'),
            'list' => array('lists'),
            'campaign' => array('campaigns'),
            'comment' => array('comments'),
            'blog post' => array('blog posts'),
            'post' => array('posts'),
            'email' => array('emails'),
            'image' => array('images'),
            'product' => array('products'),
            'text blocks' => array('text blocks'),
            'text message' => array('text messages'),
            'message' => array('messages'),
            'tag' => array('tags'),
            'category' => array('categories'),
            'result' => array('results'),
            'run' => array('runs')
            ); 
        
        public $metadata_id ;
        public $metadata ;
        public $metadata_type ;        
        public $metadata_list ;    
        public $metadata_relationship_list ; // This is the list items (campaigns, contacts) and the list of metadata it's associated with   
        public $metadata_id_relationship_list ; // This is the list of relationships for a specific metadata_id
        public $category_id;
        public $category;
        public $category_list ; 

        public $tag_id;
        public $tag;
        public $tag_list ;         
        
        public $app_page_id ; 
        public $app_page ; 
        public $app_page_list ; 
        
        public $test_data ;
        
        
        
    public function __construct() {
        
        global $DB ;
        $this->DB = $DB ;
        
        }        
        
        
        
        
    public function Add_Model_Timings_Array($new_timing_array) {
        
//        global $microtime_array ;
        
        $model_timings = $this->model_timings_array ; 
            
        foreach ($new_timing_array as $key => $value) {
            $model_timings[$key] = $value ; 
            }
        
        $this->model_timings_array = $model_timings ; 
            
        return $this ; 
        }
        
    public function Replace_Model_Timings_Array($new_timing_array) {
        
        $this->model_timing_increment = $new_timing_array['model_timing_increment'] ;
        $this->model_timings_array = $new_timing_array['timings'] ; 
            
        return $this ; 
        }
        
    public function Set_Model_Timings($new_timing_name,$time = 'now') {
        
        if ('now' === $time) {
            $this->model_timings_array[$this->model_timing_increment.'_'.$new_timing_name] =  microtime(true) ; 
            } else {
                $this->model_timings_array[$this->model_timing_increment.'_'.$new_timing_name] =  $time ; 
                }
        
        $this->model_timing_increment++ ; 
        
        return $this ; 
        }
        
    public function Get_Model_Timings() {
        
        $model_timings = $this->model_timings_array ; 
        asort($model_timings) ;  
        
        $standard_array = array() ; 
        $microtime_array = array() ; 
        $microtime_difference_array = array() ;
        $microtime_cumulative_array = array() ;
        
        $i = 0 ; 
        foreach ($model_timings as $key => $value) {
            
            $standard_array[] = $value ; 
            $microtime_array[$key] = $value ; 
            
            if ($i == 0) {
                $microtime_difference_array[$key] = 0 ; 
                $microtime_cumulative_array[$key] = 0 ; 
                } else {
                    $diff1 = round($standard_array[$i],8) ; 
                    $diff2 = round($standard_array[$i-1],8) ; 
                    $microtime_difference_array[$key] = round($diff1-$diff2,8) ; 
                
                    $microtime_cumulative_array[$key] = round($microtime_cumulative_array[$last_key] + round($diff1-$diff2,8),8) ; 
                    }
            
            $i++ ; 
            $last_key = $key ; 
            }

        $microtime_result = array(
            'timings' => $microtime_array,
            'increments' => $microtime_difference_array,
            'cumulative' => $microtime_cumulative_array,
            'model_timing_increment' => $this->model_timing_increment
            ) ; 
        
        return $microtime_result ; 
        }
        
    public function Compile_Global_Timings() {
        
        global $microtime_array ;
        
        $model_timings = $this->model_timings_array ; 
            
        foreach ($model_timings as $key => $value) {
            $microtime_array[$key] = $value ; 
            }

        return $microtime_array ; 
        }
        
        
    public function Get_System_Plurals_List() {
        
        return $this->system_plurals_list ; 
        }
        
        
    public function Set_System_Billing_Status_Levels($query_options = array()) {
        
        $result = $this-> Retrieve_System_Billing_Status_Levels($query_options) ; 
        
        if (!$result['error']) {
            
            $this->system_billing_status_levels = $result['results'] ;
            } else {
                $this->system_billing_status_levels = 'error' ; 
                }         
        
        return $this ; 
        } 
        
    
    public function Get_System_Billing_Status_Levels() {
        
        return $this->system_billing_status_levels ; 
        }
        
        
    public function Set_System_View_List_Fields($query_options = array()) {
    
        
        $result = $this->Retrieve_System_View_List_Fields($query_options) ; 
        
        if (!$result['error']) {
            
            $i = 0 ; 
            foreach ($result['results'] as $field) {
                
                $result['results'][$i]['field_name_compiled'] = Utilities::Process_Comma_Separated_String($field['field_name_compiled']) ; 
                $i++; 
                }
            
            $this->system_view_list_fields = $result['results'] ;
            } else {
                $this->system_view_list_fields = 'error' ; 
                }     
        
        return $this ; 
        }
    
    
    public function Get_System_View_List_Fields() {
        
        return $this->system_view_list_fields ; 
        }
    
    public function Retrieve_System_View_List_Fields($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_view_field_names',
            'fields' => "system_view_field_names.*",
            'where' => "system_view_field_names.system_view_field_id>'0' ",
            'order_by' => "system_view_field_names.default_order",
            'order' => "ASC"
            );        
        

        if (isset($query_options->view_name)) {
            $query_array['where'] .= " AND system_view_field_names.view_name='$query_options->view_name'" ; 
            }
        
        if (isset($query_options->default_column)) {
            $query_array['where'] .= " AND system_view_field_names.default_column='$query_options->default_column'" ; 
            }
        
        if (isset($query_options->filter_by_system_view_field_id)) {
            $field_id_query_string = '' ; 
            foreach ($query_options->filter_by_system_view_field_id as $field_id) {
                $field_id_query_string .= "system_view_field_names.system_view_field_id='$field_id' OR " ; 
                }
            $field_id_query_string = rtrim($field_id_query_string," OR ") ;
            
            $query_array['where'] .= " AND ($field_id_query_string) " ; 
            }        
        
        $result = $this->DB->Query('SELECT',$query_array,'force');
        $this->system_query_result = $result ;         
        
        return $result ; 
        }
        
        
        
        
        
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    ////////////////////// 
        
        
    public function Set_Page_Increment($page_increment) {
        $this->page_increment = $page_increment ; 
        return $this ; 
        }
        
        
    public function Set_System_Auth_List($query_options = array()) {

        if (!isset($query_options['role_auth_type'])) {
            $query_options['role_auth_type'] = 'all' ; 
            }
        
        $result = $this->Retrieve_System_Auth_List($query_options) ; 
        
        if (count($result['result_count']) > 0) {            
            $i = 0 ; 
            foreach ($result['results'] as $role) {
                // Convert json encoded string into array of additional action data            
                $permissions_set = json_decode($role['auth_role_permissions'],true) ; 
                
                foreach ($permissions_set as $key => $value) {                        
                    $result['results'][$i][$key] = $value ; 
                    }                 
                $i++ ; 
                }
            }        
        
        $this->system_auth_list = $result['results'] ; 

        return $this ;          
        }
        
    public function Get_System_Permissions_List() {
        
        return $this->system_permissions_list ; 
        }    
        
     
    public function Get_System_Operations_List() {
        
        return $this->system_operations_list ; 
        }
        
        
    public function Set_System_Permissions_List($query_options = array()) {

        $result = $this->Retrieve_System_Permissions_List($query_options) ; 
        $this->system_permissions_list = $result['results'] ; 

        return $this ;          
        }
        
    public function Set_System_Operations_List($query_options = array()) {

        $result = $this->Retrieve_System_Operations_List($query_options) ; 
        $this->system_operations_list = $result['results'] ; 

        return $this ;          
        }
        
    public function Set_System_Action_Key($action_key_id) {

        $result = $this->Retrieve_System_Action_Key($action_key_id) ; 
        $this->action_key_id = $action_key_id ; 
        $result['results'] = $this->Action_Time_Territorialize_Dataset($result['results']) ;
        $this->action_key = $result['results'] ; 

        return $this ;          
        }
        
    public function Set_System_Action($action_id) {

        $result = $this->Retrieve_System_Action($action_id) ; 
        $this->action = $result['results'] ; 

        return $this ;          
        } 
    
        
    public function Set_System_Version_List($version_options = array()) {

        if (!isset($version_options['stable'])) {
            $version_options['stable'] = 'yes' ; 
            }
        if (!isset($version_options['supported'])) {
            $version_options['supported'] = 'yes' ; 
            }        
        
        if (!isset($version_options['order_by'])) {
            $version_options['order_by'] = "system_version_control.timestamp_version_created" ; 
            $version_options['order'] = 'DESC' ; 
            }

        
        $result = $this->Retrieve_System_Versions($version_options) ; 
        
        if ($result['result_count'] == 0) {
            $this->system_version_list = array() ; 
            } else {
                $c = 0 ; 
                foreach ($result['results'] as $item) {
                    $result['results'][$c] = $this->Action_Time_Territorialize_Dataset($item) ;
                    $c++ ; 
                    }
                $this->system_version_list = $result['results'] ; 
                }         


        return $this ;          
        } 
        
    public function Set_System_List($list_name,$query_options = array()) {

        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'yes' ; // By default we override paging on system lists
            }

        $result = $this->Retrieve_System_List($list_name,$query_options) ; 
        $this->$list_name = $result['results'] ; 
 
        return $this ;          
        }         
    
        
    // Set app_view_history with a specific view_history_id
    public function Set_App_View_History_By_ID($view_history_id = 'internal') {
        
        if ('internal' === $view_history_id) {
            $view_history_id = $this->app_view_history_id ; 
            } else {
                $this->app_view_history_id = $view_history_id ;      
                }
        
        $query_options['view_history_id'] = $view_history_id ; 
        
        $this->Set_App_View_History($query_options) ; 
        return $this ; 
        }
      
        
        
    // app_view_history can be used to recall a specific filtered view of a list, and return to that exact 
    // position when using the back button.  Can also referene the view_history_id to download list results
    public function Set_App_View_History($query_options = array()) {
        
        if (isset($query_options['view_saved_id']) OR isset($query_options['view_saved_name']) OR ($query_options['retrieve_saved_views'] == 'yes')) {
            $result = $this->Retrieve_App_View_Saved($query_options) ; 
            } else {
                $result = $this->Retrieve_App_View_History($query_options) ;             
                }
        
        $this->system_query_result = $result ; 
        
        if ($result['result_count'] == 0) {
            $this->app_view_history_list = array() ; 
            } else {
            
                $i = 0 ; 
                foreach ($result['results'] as $history) {
                    $view_history = json_decode($history['view_parameters']) ; 
                    $history['view_parameters'] = (array) $view_history ;
                    $display_columns = json_decode($history['display_columns']) ; 
                    $history['display_columns'] = (array) $display_columns ;


//                        (array) $display_columns ;
//                        foreach ($display_columns as $column) {
//                            $history['display_columns'][] = (array) $column ; 
//                            }


                    $result['results'][$i] = $history ; 
                    $i++ ; 
                    } 
            
                $this->app_view_history = $result['results'][0] ; 
                $this->app_view_history_list = $result['results'] ; 
                }
        
        return $this ; 
        }
        
        
    public function Get_System_Setting() {
        
        return $this->system_setting ; 
        }    
        
        
    public function Set_System_Setting_By_ID($setting_id,$additional_parameters = array()) {
        

        if (!isset($additional_parameters['set_by'])) {
            $additional_parameters['set_by'] = 'id' ; 
            }        

        switch ($additional_parameters['set_by']) { 
            case 'setting_name':
                $setting = $this->Retrieve_System_Setting_By_Name($setting_id) ; 
                if ($setting['result_count'] == 0) {
                    $this->system_setting_id = 'error' ; 
                    } else {
                        $this->system_setting_id = $setting['results']['setting_id'] ;
                        }  
                
                $this->Set_System_Setting($this->system_setting_id,$additional_parameters) ; 
                break ;                
            case 'id':
            default:
                $this->system_setting_id = $setting_id ;
                $this->Set_System_Setting($this->system_setting_id,$additional_parameters) ; 
            }        
        
        return $this ;
        }
        
    public function Set_System_Setting($setting_id,$additional_parameters = array()) {
        
        $query_options['filter_by_setting_id'] = $setting_id ; 
        
        $result = $this->Retrieve_System_Setting_List($query_options) ; 

        
        if ($result['result_count'] == 0) {
            $this->system_setting = 'error' ; 
            } else {
                $system_setting = $result['results'][0] ;
            
                $system_setting['setting_structured_data_array'] = json_decode($system_setting['setting_structured_data'],1) ; 
                if (is_array($system_setting['setting_structured_data_array'])) {
                    foreach ($system_setting['setting_structured_data_array'] as $key => $value) {
                        $system_setting[$key] = $value ; 
                        }
                    }
            
                $system_setting = $this->Action_Time_Territorialize_Dataset($system_setting) ; 
            
                $this->system_setting = $system_setting ; 
                }

        
        return $this ;
        }
        
    public function Action_Update_System_Setting($setting_input) {
        
        $continue = 1 ; 

        $system_setting_record = $this->Get_System_Setting() ; 
        
        if ($setting_input['setting_id'] != $system_setting_record['setting_id']) {
            $continue = 0 ; 
            } 
        
        if ($continue == 1) {
            
            // Add an unset here if a datetime_ came through with no entry 
            $setting_input = $this->Action_Convert_Datetime_To_Timestamp($setting_input) ;

            $system_values = array() ; 
            
            if (isset($setting_input['setting_id'])) {
                $system_values['setting_id'] = $setting_input['setting_id'] ;
                }            
            if (isset($setting_input['setting_name'])) {
                $system_values['setting_name'] = $setting_input['setting_name'] ;
                }
            if (isset($setting_input['setting_status'])) {
                switch ($setting_input['setting_status']) {
                    case 1:
                    case 'true':      
                        $system_values['setting_status'] = 1 ; 
                        break ; 
                    case 0:
                    case 'false':
                        $system_values['setting_status'] = 0 ; 
                    }
                }            
            unset($setting_input['setting_id'],$setting_input['setting_name'],$setting_input['setting_status']) ;
            unset($setting_input['form_type'],$setting_input['uniqueclass_id'],$setting_input['form_response'],$setting_input['show_response'],$setting_input['user_id'],$setting_input['account_id'],$setting_input['validation_response']) ;
                
            
            $system_values['setting_structured_data'] = $setting_input['setting_structured_data'] ;
            
            foreach ($setting_input as $key => $value) {
                $system_values['setting_structured_data'][$key] = $value ; 
                }
            $system_values['setting_structured_data'] = json_encode($system_values['setting_structured_data']) ; 
            
            $update_result = $this->Update_System_Setting($system_values) ; 
            
            $this->Set_System_Setting_By_ID($setting_input['setting_id']) ; 
            }
            
        return $this ; 
        }    
        
        
        
        
    public function Update_System_Setting($query_values = array()) {
        
        $update_setting_id = $query_values['setting_id'] ; 
        unset($query_values['setting_id']) ; 
        
        $query_array = array(
            'table' => "system_settings",
            'values' => $query_values,
            'where' => "setting_id='$update_setting_id'"
            );                        
        $result = $this->DB->Query('UPDATE',$query_array) ; 
        
        return $result ; 
        }
        
        
        
    public function Retrieve_System_Setting_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_settings',
            'fields' => "system_settings.*",
            'where' => ""
            );        
        
        if (isset($query_options->filter_by_setting_id)) {
            $query_array['where'] .= " AND system_settings.setting_id='$query_options->filter_by_setting_id'" ;
            }
        
        
        if (isset($query_array['where'])) {
            $query_array['where'] = ltrim($query_array['where']," AND ") ;            
            }        
        
        $result = $this->DB->Query('SELECT',$query_array,'force');
        
        $this->system_query_result = $result ;
        
        return $result ; 
        }
        
        
    public function Retrieve_System_Setting_By_Name($setting_name) {
        
        $query_array = array(
            'table' => 'system_settings',
            'fields' => "system_settings.setting_id",
            'where' => "system_settings.setting_name='$setting_name' AND system_settings.setting_name!=''"
            );        
        
        $result = $this->DB->Query('SELECT',$query_array);
        
        $this->system_query_result = $result ;
        
        return $result ; 
        }    
        
        
        
    public function Set_Timezone_List() {

        $utilities = new Utilities() ; 
        
        $timezones = [];
        $offsets = [];
        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        foreach (DateTimeZone::listIdentifiers() as $timezone) {
            $now->setTimezone(new \DateTimeZone($timezone));
            $offsets[] = $offset = $now->getOffset();
            $current_timestamp = $now->getTimestamp();
            $name_array = $utilities->Time_Format_Timezone_Name($timezone) ; 
            
            $timezones[] = array(
                'timezone_name' => $timezone, 
                'offset' => $utilities->Time_Format_GMT_Offset($offset), 
                'timezone_pretty' => $name_array['timezone_pretty'], 
                'timezone_continent' => $name_array['timezone_continent'], 
                'timezone_city' => $name_array['timezone_city'], 
                'current_date' => $now->format('M j, Y'),           
                'current_time' => $now->format('g:i A T')
                );
        }

        
        $continent  = array_column($timezones, 'timezone_continent');
        $city = array_column($timezones, 'timezone_city');

        // Sort the data with continent ascending, city ascending
        // Add $timezones as the last parameter, to sort by the common key
        array_multisort($continent, SORT_ASC, $city, SORT_ASC, $timezones);        
        
        $this->list_timezones = $timezones ; 
        
        return $this ;          
        }        
        

        
    // Set metadata type based on metadata_type_id or metadata_type
    public function Set_Metadata_Type($query_options) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        
        $result = $this->Retrieve_Metadata_Type_List($query_options) ;         
        
        if (isset($result['error'])) {
            $this->Set_Alert_Response(15) ; // Generic fail
            } else {
                if ($result['result_count'] == 0) {
                    $this->metadata_type = 'error' ; 
                    } else {
                        $this->metadata_type = $result['results'][0] ;
                        }        
                }        
        
        return $this ; 
        }
        
        
        
    // Set a list of metadata with filter parameters
    public function Set_Metadata_List($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
//        print_r($query_options) ; 
        if (isset($query_options['page_increment'])) {
            $this->Set_Page_Increment($query_options['page_increment']) ; 
            }
        
        if (!isset($query_options['filter_by_visibility_id']) AND (!isset($query_options['filter_by_visibility_name']))) {
            $query_options['filter_by_visibility_name'] = array('all') ; // All metadata eligible except hidden (which is admin only)
            }
            
        if (isset($query_options['filter_by_related_assets'])) {
            if (!isset($query_options['filter_by_asset_visibility_id']) AND (!isset($query_options['filter_by_asset_visibility_name']))) {
                $query_options['filter_by_asset_visibility_name'] = array('all') ; // All assets eligible except hidden (which is admin only)
                }        
            }
        
        if (!isset($query_options['order_by'])) {
            $query_options['order_by'] = 'metadata.metadata_name' ;
            $query_options['order'] = 'ASC' ;
            }
        
        $result = $this->Retrieve_Metadata_List($query_options) ;         

        if (isset($result['error'])) {
            $this->Set_Alert_Response(15) ; // Generic fail
            $this->metadata_list = 'error' ; 
            } else {
                if ($result['result_count'] == 0) {
                    $this->metadata_list = 'error' ; 
                    } else {
                        $this->metadata_list = $result['results'] ;
                        }        
                }        
        
        return $this ; 
        }
        
        
        
    // Set a specific value based on the list_defaults query set
    public function Set_System_Default_Value($default_name_query) {
        
        global $server_config ; 
        
        if (!is_array($server_config['system_defaults'])) {
            
            $this->Set_System_List('list_defaults') ; 
            $server_config['system_defaults'] = $system->Get_System_List('list_defaults') ; 
            } 
        
        $default_array = Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', $default_name_query) ; 
        
        if ($default_array['value']) {
            $this->system_default_value = $default_array['value'] ; 
            } else {
                $this->system_default_value = 'error' ; 
                }
        
        return $this ; 
        }        
       
        
   
        
        
        
    public function Retrieve_Metadata_Type_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        

        $query_array = array(
            'table' => 'metadata_type',
            'join_tables' => array(),
            'fields' => "metadata_type.*",
            'where' => "metadata_type.metadata_type_id>'0' "
            );
    
        if (isset($query_options->filter_by_metadata_type)) {
            
            $query_array['where'] .= " AND metadata_type.metadata_type='$query_options->filter_by_metadata_type'" ; 
            
            }
        
        if (isset($query_options->filter_by_metadata_type_id)) {
            
            $query_array['where'] .= " AND metadata_type.metadata_type_id='$query_options->filter_by_metadata_type_id'" ; 
            
            }
    

        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $this->system_query_result = $result ;
        
        return $result ;               
        }
        
        
    public function Retrieve_Metadata_List($query_options = array()) {
        
        if (!isset($query_options['override_paging'])) {
            $query_options['override_paging'] = 'no' ;
            }
        
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        

        $query_array = array(
            'table' => 'metadata',
            'join_tables' => array(),
            'fields' => "metadata.*, metadata_type.metadata_type_name, metadata_type.metadata_type, 
                system_visibility_levels.visibility_name, system_visibility_levels.visibility_name, system_visibility_levels.visibility_title",
            'where' => "metadata.metadata_id>'0' ",
            'group_by' => "metadata.metadata_id"
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'metadata.visibility_id'
            );         
        
           

        // Determine how account_id & global_metadata should be filtered
        if (isset($query_options->allow_global_metadata)) {
            
            switch ($query_options->allow_global_metadata) {
                case 'ignore':
                    // Ignore adding an account_id parameter to this query
                    $query_array['where'] .= "" ; 
                    break ;                    
                case 'only': // If we're looking at global metadata and no account_id filter added, then we can only show shared & published status 
                case 'globals_only':
                    $query_array['where'] .= " AND (
                        (metadata.account_id='$this->account_id' AND metadata.global_metadata='1') 
                        OR (metadata.global_metadata='1'
                            AND (metadata.visibility_id='4' OR metadata.visibility_id='5')
                            )
                        ) " ;
                    break ;
                case 'account_nonglobals':
                    $query_array['where'] .= " AND 
                        (metadata.account_id='$this->account_id' AND metadata.global_metadata='0') 
                         " ;
                    break ;                    
                case 'no':
                case 'account': 
                case 'account_only': 
                    $query_array['where'] .= " AND metadata.account_id='$this->account_id' " ; 
                    break ;
                case 1:
                case 'yes':
                case 'all':
                default: // If we're looking at global metadata and no account_id filter added, then we can only show shared & published status 
                    $query_array['where'] .= " AND (metadata.account_id='$this->account_id' OR 
                        (metadata.global_metadata='1'
                            AND (metadata.visibility_id='4' OR metadata.visibility_id='5')
                            )
                        ) " ; 
                }            
            } else {
                $query_array['where'] .= " AND metadata.account_id='$this->account_id' " ; 
                } 
        
        
        
        // FOR ASSETS ONLY: ADD RELATIONSHIP FILTERS...
        if (isset($query_options->filter_by_asset_id)) {

            $query_array['fields'] .= ", asset_metadata_relationships.relationship_id" ;            

            $query_array['join_tables'][] = array(
                'table' => 'asset_metadata_relationships',
                'on' => 'asset_metadata_relationships.metadata_id',
                'match' => 'metadata.metadata_id'
                );             
            
            $query_array['join_tables'][] = array(
                'table' => 'assets',
                'on' => 'assets.asset_id',
                'match' => 'asset_metadata_relationships.asset_id'
                );
            
            
            $query_options->filter_by_asset_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_asset_id) ;
 
            if (count($query_options->filter_by_asset_id) > 0) {   
                $asset_id_query_string = '' ; 
                foreach ($query_options->filter_by_asset_id as $asset_id) {
                    $asset_id_query_string .= "asset_metadata_relationships.asset_id='$asset_id' OR " ; 
                    }
                $asset_id_query_string = rtrim($asset_id_query_string," OR ") ;
                $query_array['where'] .= " AND ($asset_id_query_string)" ;
                }            

            }
        
        
        if (isset($query_options->filter_by_related_assets)) {            
            

            $related_table = 'related_assets' ; 
            $query_array['fields'] .= ", $related_table.asset_title AS related_asset_title" ;            



            // PROCESS ASSET TYPE FILTER
            if ((isset($query_options->filter_by_type_id)) OR (isset($query_options->filter_by_type_name))) {

                $type_id_array = $this->Retrieve_Asset_Type_ID_Array($query_options) ; 

                // Add asset type filter if there are type_id's to process
                if (count($type_id_array) > 0) {
                    $master_filter_by_asset_type_string = '' ; 
                    foreach ($type_id_array as $type_id) {
                        $master_filter_by_asset_type_string .= "assets.type_id='$type_id' OR " ; 
                        }
                    $master_filter_by_asset_type_string = rtrim($master_filter_by_asset_type_string," OR ") ;
                    }                                
                }
            
            
            
            
            // Pre-process related assets filter
            // If filter_by_related_assets was submitted as array, it allows us to set different global filters
            // depending on the asset type
            // If simply set as a string, then assume all asset types will get the same global filter
            // Regardless, this section translates filter_by_related_assets into an array even if submitted as string            
            if (is_array($query_options->filter_by_related_assets)) {
                
                $query_options->filter_by_related_assets_array = $query_options->filter_by_related_assets ; 
                $query_options->filter_by_related_assets = array() ;                
                
                foreach ($query_options->filter_by_related_assets_array as $related_setting) {

                    $related_setting = (object) $related_setting ;
                    $this_related_type_id_array = $this->Retrieve_Asset_Type_ID_Array($related_setting) ; 


                    // Add asset type filter if there are type_id's to process
                    if (count($this_related_type_id_array) > 0) {
                        $filter_by_asset_type_string = '' ; 
                        foreach ($this_related_type_id_array as $type_id) {
                            $filter_by_asset_type_string .= "assets.type_id='$type_id' OR " ; 
                            }
                        $filter_by_asset_type_string = rtrim($filter_by_asset_type_string," OR ") ;
                        $filter_by_asset_type_string = " ($filter_by_asset_type_string) " ;                
                        }

                    $this_related_array = array(
                        'filter_by_related_assets' => $related_setting->filter_by_related_assets,
                        'filter_by_asset_type_string' => $filter_by_asset_type_string
                        ) ;
                    $query_options->filter_by_related_assets[] = $this_related_array ; 

                    }                

                } else {
                
                    // With this option, all asset types will be submitted to the same filter_by_related_assets filter
                
                    $filter_by_related_assets_string = $query_options->filter_by_related_assets ; 
                    $query_options->filter_by_related_assets = array() ; 

                    $this_related_array = array(
                        'filter_by_related_assets' => $filter_by_related_assets_string,
                        'filter_by_asset_type_string' => $master_filter_by_asset_type_string
                        ) ;
                    $query_options->filter_by_related_assets[] = $this_related_array ;     
                
                    }

            
            
            // PROCESS RELATED ASSET GLOBALS FILTER
            // Uses the new filter_by_related_assets array created above
            foreach ($query_options->filter_by_related_assets as $related_asset_set) {            
            
                $relationship_query_array = array(
                    'table' => 'assets',
                    'join_tables' => array(),
                    'fields' => "assets.*, asset_metadata_relationships.metadata_id",
                    'skip_query' => 1
                    );
                $relationship_query_array['join_tables'][] = array(
                    'table' => 'asset_metadata_relationships',
                    'on' => 'asset_metadata_relationships.asset_id',
                    'match' => 'assets.asset_id'
                    ) ;
                

                $filter_by_related_assets = $related_asset_set['filter_by_related_assets'] ;
                $filter_by_asset_type_string = $related_asset_set['filter_by_asset_type_string'] ;
                
                if ($filter_by_asset_type_string) {
                    $relationship_query_array['where'] .= "AND ($filter_by_asset_type_string) " ;                
                    }
                

                switch ($filter_by_related_assets) {

                    case 'unselected_globals': 

                        // Not written yet

                        break ; 
                    case 'selected_globals': 

                        // Not written yet

                        break ;                 
                    case 'globals_only':    
                    case 'only':

                        $relationship_query_array['where'] .= "AND assets.global_asset='1' " ; 
                        break ;
                    case 'account': 
                    case 'account_only':                 

                        $relationship_query_array['where'] .= "AND assets.account_id='$this->account_id' " ; 
                        break ;
                    case 'all':    

                        // Should account for account owned assets, selected globals AND unselected globals

                        break ;                
                    case 1:
                    case 'yes':                
                    case 'account+selected_globals':   
                    default:    

                        $relationship_query_array['where'] .= "AND (assets.account_id='$this->account_id' OR assets.global_asset='1') " ; 
                        break ;      
                    }



                // PROCESS ASSET VISIBILITY FILTER  
                $asset_query_options['filter_by_visibility_id'] = $query_options->filter_by_asset_visibility_id ; 
                $asset_query_options['filter_by_visibility_name'] = $query_options->filter_by_asset_visibility_name ;
                $asset_visibility_array = $this->Retrieve_Visibility_ID_Array($asset_query_options) ;


                // Add asset visibility filter if there are asset visibility_id's to process
                $filter_by_asset_visibility_id_string = '' ; 
                if (count($asset_visibility_array) > 0) {

                    foreach ($asset_visibility_array as $asset_visibility) {

                        $this_asset_visibility_id = $asset_visibility['visibility_id'] ; 
                        $filter_by_asset_visibility_id_string .= "assets.visibility_id='$this_asset_visibility_id' OR " ;               
                        }

                    $filter_by_asset_visibility_id_string = rtrim($filter_by_asset_visibility_id_string," OR ") ;
                    $relationship_query_array['where'] .= "AND ($filter_by_asset_visibility_id_string) " ;  
                    }



                // Finalize asset relationship query
                if (isset($relationship_query_array['where'])) {
                    $relationship_query_array['where'] = ltrim($relationship_query_array['where']," AND ") ;            
                    }                
                
                $relationship_query = $this->DB->Query('SELECT_JOIN',$relationship_query_array);
                $relationship_query_statement = $relationship_query['query'] ; 


                $uber_relationship_join_array[] = $relationship_query_statement ;                 
                }
            
            
            
            // Splice all of the relationship_query_statement's together, using UNION in order to eliminate duplicates
            $uber_relationship_join_statement = '' ;
            foreach ($uber_relationship_join_array as $uber_statement) {
                $uber_relationship_join_statement .= $uber_statement.' UNION ' ; 
                }
            $uber_relationship_join_statement = rtrim($uber_relationship_join_statement," UNION ") ;
            $uber_relationship_join_statement = "($uber_relationship_join_statement) AS $related_table " ;
            

            $query_array['join_tables'][] = array(
                'table' => $uber_relationship_join_statement,
                'statement' => "(metadata.metadata_id=$related_table.metadata_id)"
                );            
            
            }
        
        
        
        // FOR NONASSETS ONLY: ADD RELATIONSHIP FILTERS...
        if (isset($query_options->filter_by_nonasset_relationship_type)) {
            
            $nonasset_related_table = 'related_nonassets' ; 
            
            switch ($query_options->filter_by_nonasset_relationship_type) {
                case 'user':
                    
                    $query_array['fields'] .= ", $nonasset_related_table.user_id AS related_user_id" ;            

                    $nonasset_relationship_query_array = array(
                        'table' => 'users',
                        'join_tables' => array(),
                        'fields' => "users.*, nonasset_metadata_relationships.metadata_id",
                        'skip_query' => 1,
                        'where' => "nonasset_metadata_relationships.relationship_type='$query_options->filter_by_nonasset_relationship_type'"
                        );
                    $nonasset_relationship_query_array['join_tables'][] = array(
                        'table' => 'nonasset_metadata_relationships',
                        'on' => 'nonasset_metadata_relationships.item_id',
                        'match' => 'users.user_id'
                        ) ;                     
                    
                    break ;
                case 'profile':
                    
                    $query_array['fields'] .= ", $nonasset_related_table.profile_id AS related_profile_id" ;            

                    $nonasset_relationship_query_array = array(
                        'table' => 'user_profiles',
                        'join_tables' => array(),
                        'fields' => "user_profiles.*, nonasset_metadata_relationships.metadata_id",
                        'skip_query' => 1,
                        'where' => "nonasset_metadata_relationships.relationship_type='$query_options->filter_by_nonasset_relationship_type'"
                        );
                    $nonasset_relationship_query_array['join_tables'][] = array(
                        'table' => 'nonasset_metadata_relationships',
                        'on' => 'nonasset_metadata_relationships.item_id',
                        'match' => 'user_profiles.profile_id'
                        ) ;                     
                    
                    break ; 
                case 'campaign':
                    
                    $query_array['fields'] .= ", $nonasset_related_table.campaign_id AS related_campaign_id" ;            

                    $nonasset_relationship_query_array = array(
                        'table' => 'asset_campaigns',
                        'join_tables' => array(),
                        'fields' => "asset_campaigns.*, nonasset_metadata_relationships.metadata_id",
                        'skip_query' => 1,
                        'where' => "nonasset_metadata_relationships.relationship_type='$query_options->filter_by_nonasset_relationship_type'"
                        );
                    $nonasset_relationship_query_array['join_tables'][] = array(
                        'table' => 'nonasset_metadata_relationships',
                        'on' => 'nonasset_metadata_relationships.item_id',
                        'match' => 'asset_campaigns.campaign_id'
                        ) ;                     
                    
                    break ;
                case 'contact':
                    
                    $query_array['fields'] .= ", $nonasset_related_table.contact_id AS related_contact_id" ;            

                    $nonasset_relationship_query_array = array(
                        'table' => 'contacts',
                        'join_tables' => array(),
                        'fields' => "contacts.*, nonasset_metadata_relationships.metadata_id",
                        'skip_query' => 1,
                        'where' => "nonasset_metadata_relationships.relationship_type='$query_options->filter_by_nonasset_relationship_type' AND contacts.user_id='$this->user_id'"
                        );
                    $nonasset_relationship_query_array['join_tables'][] = array(
                        'table' => 'nonasset_metadata_relationships',
                        'on' => 'nonasset_metadata_relationships.item_id',
                        'match' => 'contacts.contact_id'
                        ) ;  
                    
                    break ;                    
                }
            
            
                $nonasset_relationship_query = $this->DB->Query('SELECT_JOIN',$nonasset_relationship_query_array);
                $nonasset_relationship_query_statement = $nonasset_relationship_query['query'] ; 

                $query_array['join_tables'][] = array(
                    'table' => "($nonasset_relationship_query_statement) AS $nonasset_related_table",
                    'statement' => "(metadata.metadata_id=$nonasset_related_table.metadata_id)"
                    );             

            }
        
        
        
        // PROCESS METADATA VISIBILITY FILTER        
        $visibility_array = $this->Retrieve_Visibility_ID_Array($query_options) ; 
        
        // Add visibility filter if there are visibility_id's to process
        $filter_by_visibility_id_string = '' ; 
        if (count($visibility_array) > 0) {

            foreach ($visibility_array as $visibility) {
                
                $this_visibility_id = $visibility['visibility_id'] ; 
                $filter_by_visibility_id_string .= "metadata.visibility_id='$this_visibility_id' OR " ;               
                }
            
            $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," OR ") ;
            $query_array['where'] .= "AND ($filter_by_visibility_id_string) " ;  
            }
        
        
        
        // PROCESS METADATA TYPE FILTER
        $metadata_type_id_array = array() ; 
         
        // Submitted as an array of type_id values.
        if (isset($query_options->filter_by_metadata_type_id)) {
            // If an array wasn't submitted, turn it into an array
            $metadata_type_id_array = Utilities::Array_Force($query_options->filter_by_metadata_type_id) ;             
            }
                
        // Submitted as an array of type_name's which is converted into, and added to the type_id array
        if (isset($query_options->filter_by_metadata_type)) {

            $metadata_type_array = Utilities::Array_Force($query_options->filter_by_metadata_type) ; 

            $query_type_array = array(
                'table' => 'metadata_type',
                'fields' => "*",
                'where' => ""
                );
                        
            foreach ($metadata_type_array as $metadata_type_name) {
                $query_type_array['where'] .= "metadata_type='$metadata_type_name' OR " ; 
                }
            $query_type_array['where'] = rtrim($query_type_array['where']," OR ") ;
            $type_result = $this->DB->Query('SELECT',$query_type_array,'force');   
 
            foreach ($type_result['results'] as $type) {
                $metadata_type_id_array[] = $type['metadata_type_id'] ; 
                }
            }        
        
        
        // Add metadata type filter if there are type_id's to process
        if (count($metadata_type_id_array) > 0) {
            
            $filter_by_metadata_type_string = '' ; 
            foreach ($metadata_type_id_array as $type_id) {
                $filter_by_metadata_type_string .= "metadata.metadata_type_id='$type_id' OR " ; 
                }
            $filter_by_metadata_type_string = rtrim($filter_by_metadata_type_string," OR ") ;
            $query_array['where'] .= "AND ($filter_by_metadata_type_string) " ;                
            }        
        
        
        
        // Add metadata id filter
        if (isset($query_options->filter_by_metadata_id)) {
            // If an array wasn't submitted, turn it into an array
            
            $query_options->filter_by_metadata_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_metadata_id) ;
            
            $metadata_id_array = Utilities::Array_Force($query_options->filter_by_metadata_id) ;             
            
            if (count($metadata_id_array) > 0) {

                $filter_by_metadata_id_string = '' ; 
                foreach ($metadata_id_array as $metadata_id) {
                    $filter_by_metadata_string .= "metadata.metadata_id='$metadata_id' OR " ; 
                    }
                $filter_by_metadata_string = rtrim($filter_by_metadata_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_metadata_string) " ;                
                }             
            
            }        
        
        
        // Add metadata slug filter
        if (isset($query_options->filter_by_metadata_slug)) {
            // If an array wasn't submitted, turn it into an array
            
            $query_options->filter_by_metadata_slug = Utilities::Process_Comma_Separated_String($query_options->filter_by_metadata_slug) ;
            
            $metadata_slug_array = Utilities::Array_Force($query_options->filter_by_metadata_slug) ;             
            
            if (count($metadata_slug_array) > 0) {

                $filter_by_metadata_slug_string = '' ; 
                foreach ($metadata_slug_array as $metadata_slug) {
                    $filter_by_metadata_slug_string .= "metadata.metadata_slug='$metadata_slug' OR " ; 
                    }
                $filter_by_metadata_slug_string = rtrim($filter_by_metadata_slug_string," OR ") ;
                $query_array['where'] .= "AND ($filter_by_metadata_slug_string) " ;                
                }             
            
            }        
        
        
//        if (isset($query_options->filter_by_metadata_type)) {            
//            $query_array['where'] .= " AND metadata_type.metadata_type='$query_options->filter_by_metadata_type'" ;             
//            }
//       
//        if (isset($query_options->filter_by_metadata_type_id)) {            
//            $query_array['where'] .= " AND metadata_type.metadata_type_id='$query_options->filter_by_metadata_type_id'" ;             
//            }
        
        
        if (isset($query_options->filter_by_search_parameter)) {

            $search_parameter_token = explode(",",$query_options->filter_by_search_parameter) ; 
            
            $compiled_search_string = '' ; 
            
            foreach ($search_parameter_token as $search_token) {
                
                $this_search_token = $this->DB->_connection->real_escape_string($search_token);
        
                $this_search_string = " (metadata.metadata_name LIKE '%$this_search_token%' OR 
                metadata.metadata_slug LIKE '%$this_search_token%' OR 
                metadata.metadata_description LIKE '%$this_search_token%' 
                ) " ; 
                
                $compiled_search_string .= $this_search_string.' OR ' ; 
                }
            
            $compiled_search_string = rtrim($compiled_search_string," OR ") ;     
            $query_array['where'] .= " AND ($compiled_search_string) " ;                                    
            }
        
        
        
        
//        if (isset($query_options->filter_by_search_parameter)) {
//            $query_array['where'] .= " AND (metadata.metadata_name LIKE '%$query_options->filter_by_search_parameter%' OR 
//                metadata.metadata_slug LIKE '%$query_options->filter_by_search_parameter%' OR 
//                metadata.metadata_description LIKE '%$query_options->filter_by_search_parameter%' 
//                )" ;
//            }
            

        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            } 
        
        

        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        if (($query_array['order_by_relevance'] == 'yes') AND (isset($query_options->filter_by_search_parameter))) {
            $relevance_options = array(
                'fields' => array('metadata_name','metadata_description') 
                ) ; 
            $result = $this->Sort_Search_Relevance($result,$query_array,$query_options,$relevance_options) ;  
            } 
        
        $query_options->value_name = 'metadata_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ;
        $this->system_query_result = $result ; 
 
        $this->Set_System_Paging($result) ; 
        
        return $result ; 
        }
        
        
        
    // Set the metadata
    public function Set_Metadata($metadata_id = 'internal') {
        
        if ('internal' === $metadata_id) {
            
            } else {
                $this->metadata_id = $metadata_id ;      
                } 
        
        $result = $this->Retrieve_Metadata($this->metadata_id) ;         
        
        if ($result['result_count'] == 0) {
            $this->metadata = 'error' ; 
            } else {
                $this->metadata = $result['results'] ;
                }

        return $this ; 
        
        }
        
    public function Get_Metadata() {
        
        return $this->metadata ;         
        }    
        
        
    // Set the tag
    public function Set_Tag($tag_id = 'internal') {
        
        if ('internal' === $tag_id) {
            
            } else {
                $this->tag_id = $tag_id ;      
                } 
        
        $tag = $this->Retrieve_Metadata($this->tag_id) ;         
        
        if ($tag['result_count'] == 0) {
            $this->tag = 'error' ; 
            } else {
                $this->tag = $tag['results'] ;
                }

        return $this ; 
        
        }
    
    
    // Set tag_id property
    public function Set_Tag_ID($tag_identification = 'internal', $set_by = 'id') {
        
        switch ($set_by) {
            case 'slug':
                $tag = $this->Retrieve_Metadata_ID_By_Slug('tag',$tag_identification) ; 
                if ($tag['result_count'] == 0) {
                    $this->tag_id = 'error' ; 
                    } else {
                        $this->tag_id = $tag['results']['metadata_id'] ;
                        }                
                break ;
            case 'id':
            default:
                if ('internal' === $tag_identification) {

                    } else {
                        $this->tag_id = $tag_identification ;      
                        } 
            }        

        return $this ;         
        }
    
    
    // Set the category
    public function Set_Category($category_id = 'internal') {
        
        if ('internal' === $category_id) {
            
            } else {
                $this->category_id = $category_id ;      
                } 
        
        $category = $this->Retrieve_Metadata($this->category_id) ;         
        
        if ($category['result_count'] == 0) {
            $this->category = 'error' ; 
            } else {
                $this->category = $category['results'] ;
                }

        return $this ; 
        
        }
    
    
    // Set category_id property
    public function Set_Category_ID($category_identification = 'internal', $set_by = 'id') {
        
        switch ($set_by) {
            case 'slug':
                $category = $this->Retrieve_Metadata_ID_By_Slug('category',$category_identification) ; 
                if ($category['result_count'] == 0) {
                    $this->category_id = 'error' ; 
                    } else {
                        $this->category_id = $category['results']['metadata_id'] ;
                        }                
                break ;
            case 'id':
            default:
                if ('internal' === $category_identification) {

                    } else {
                        $this->category_id = $category_identification ;      
                        } 
            }        

        return $this ;         
        }    
    

        
    // Set app pages & descriptions
    public function Set_App_Page_List($query_options) {

        if (!isset($query_options['visibility_id'])) {
            $query_options['visibility_id'] = 4 ; 
            }
        
        $pages = $this->Retrieve_App_Page_List($query_options) ;         
        
        if ($pages['result_count'] == 0) {
            $this->app_page_list = 'error' ; 
            } else {
                $this->app_page_list = $pages['results'] ;
                }

        return $this ; 
        
        }
        

        
        
    // Set app pages & descriptions
    public function Set_App_Page_By_ID($app_page_id = 'internal') {

        if ('internal' === $app_page_id) {
            
            } else {
                $this->app_page_id = $app_page_id ;      
                }
        
        $query_options['filter_by_app_page_id'] = $this->app_page_id ; 
        
        $pages = $this->Retrieve_App_Page_List($query_options) ;         
        
        if ($pages['result_count'] == 0) {
            $this->app_page = 'error' ; 
            } else {
                $this->app_page = $pages['results'][0] ;
                }

        return $this ; 
        
        }        
        
        
    public function Set_Alert_Response($alert_id) {
        
        $this->alert_id = $alert_id ; 
        $this->response = Utilities::System_Alert($this->alert_id) ;

        return $this;
        }
        
        
        
    public function Set_Default_Paging_Object() {

        $paging = array(
            'first' => 1,
            'previous' => 1,
            'current' => 1,
            'next' => 1,
            'last' => 1,
            'current_count' => 0,
            'total_pages' => 0,
            'total_count' => 0,
            'url_hash' => '',
            'full_list' => '', // All results within the current "page"
            'uber_list' => '' // All results from all pages 
            ) ;
        
        return $paging = (object) $paging ;         
        }
        
        
        
    // Process the paging array for a model based on the results that the model compiled    
    // Incoming $paging is an object. $results_array is the count and offset settings of the model
    // Full Result Set = Only the primary ID's associated with the current page view (e.g. full set of 12)
    // Uber Result Set = All the primary ID's associated with the entire query if there was no pagination (e.g. all 150+ results)
    public function Action_Process_Paging($paging,$results_array = array()) {
        
        $full_result_set_compiled = '' ; 
        $uber_result_set_compiled = '' ;
        
        $paging->value_name = $results_array['value_name'] ; // The primary id name for the result set
        $paging->current_count = $results_array['result_count'] ;
        
        if ($results_array['total_count'] == 1) {
//            $paging->total_count = $results_array['count_results']['total_count'] ;
            $paging->total_count = $results_array['total_count'] ; 
            } else {
                $paging->total_count = $results_array['total_count'] ; 
                }
        
        if (isset($results_array['url_hash'])) {
            $paging->url_hash = '#'.$results_array['url_hash'] ; 
            }        
        
        $paging->current = Utilities::Offset_To_Start_Page($results_array['offset_page']) ; 
        $paging->last = ceil($paging->total_count / $paging->page_increment) ;
        $paging->total_pages = $paging->last ;
        
        if (($paging->current - 1) < 1) {
            $paging->previous = 1 ; 
            } else {
                $paging->previous = $paging->current - 1 ; 
                }

        if (($paging->current + 1) > $paging->last) {
            $paging->next = $paging->last ; 
            } else {
                $paging->next = $paging->current + 1 ; 
                }
           

        if (isset($results_array['result_count'])) { 
            
            $i = 0 ; 
            foreach ($results_array['results'] as $value) {
                $full_result_set_compiled .= $value[$paging->value_name].',' ; 
                $i++ ; 
                }

            if (isset($results_array['uber_result_set'])) {
                $i = 0 ;                 
                foreach ($results_array['uber_result_set'] as $value) {
                    $uber_result_set_compiled .= $value[$paging->value_name].',' ; 
                    $i++ ; 
                    }
                }
            
            $paging->full_list = rtrim($full_result_set_compiled,",") ; // Primary IDs only
            $paging->uber_list = rtrim($uber_result_set_compiled,",") ; // Primary IDs only        
            }
        
        unset($results_array) ;
        return $paging ;         
        }        
        

    // Add paging constraints prior to running a list query
    // $query_options comes in as an object from the model    
    public function Action_Process_Paging_Constraints($query_array,$query_options,$page_increment = 12) {
        
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            $query_array['offset'] = $query_options->offset_page ; 
            } else {
                $query_array['limit'] = $page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }        
        
        
        $result = array(
            'query_array' => $query_array,
            'query_options' => $query_options
            ) ; 
        
        return $result ; 
        }
        
        
    public function Action_Process_Query_Results($result,$query_options,$paging_constraints) {


        $result['total_count'] = $result['result_count'] ;
        $data['initial_result_set'] = $result['results'] ;  
//        $result['uber_result_set'] = $result['results'] ;

        $result['value_name'] = $query_options->value_name ;
        $result['offset_page'] = $query_options->offset_page ;
        
        $result['results'] = array() ; 
        $result['limit_values'] = '' ;
        
        $l = 0 ; // limit counter start
        
        if (!isset($paging_constraints['query_array']['offset'])) {
            $o = 0 ; 
            } else {
                $o = $paging_constraints['query_array']['offset'] ; // offset key
                }
        
        
        if ($paging_constraints['query_array']['limit']) {
            $limit = $paging_constraints['query_array']['limit'] ; 
            } else {
                $limit = $result['total_count'] ; 
                }
        
        $result['uber_result_set'] = array() ;
        foreach ($data['initial_result_set'] as $initial) {
            $result['uber_result_set'][][$query_options->value_name] = $initial[$query_options->value_name] ; 
            }
        
        do {
            
            if ($data['initial_result_set'][$o]) {
                $result['results'][] = $data['initial_result_set'][$o] ; 
                $result['limit_values'] .= $data['initial_result_set'][$o][$result['value_name']].',' ; 
                } 
            
            $o++ ;
            $l++ ; 
            } while ($l < $limit) ; 
        
        unset($data['initial_result_set']) ; 
        
        return $result ; 
        }
        
        
        
    // Trigger automation execution for a specific task automation ID
    public function Action_Trigger_Cron_Task_Automation($task_automation_id) { 
        
        if ($task_automation_id > 0) {
            
            $additional_params['task_automation_id'] = $task_automation_id ; 
            $additional_params['avs'] = APP_VERSION['version_name'] ;
            $data['trigger_result'] = System::Cron_Execute('one',9,$additional_params) ;
            }
        
        return $data['trigger_result'] ;     
        }
        
        
    public function Get_Task_Automation_ID() {
        
        return $this->task_automation_id ; 
        }    
        
        
    public function Action_Create_Cron_Task_Automation($query_options = array()) {
        
        $continue = 1 ; 
        
        if (!isset($query_options['task_action'])) {
            $continue = 0 ;
            $this->Set_Alert_Response(265) ; // Error creating cron task
            $this->Append_Alert_Response($personalize,array(
                'location' => __METHOD__,
                'admin_context' => 'Task action not supplied'
                )) ;             
            }
        
        if ($continue == 1) {
            
            $query_options['structured_data'] = json_encode($query_options['structured_data']) ; 
            $result = $this->Create_Cron_Task_Automation($query_options) ;
            
            
            
            
            if (!$result['error']) {

                $this->task_automation_id = $result['insert_id'] ;
                
                switch ($query_options['task_action']) {
                    case 'contact_asset_add_member':
                    case 'update_asset_member':    
                        $this->Set_Alert_Response(264) ; // Asset members queued for cron task automation
                        break ;
                    case 'update_bulk_contact_post_import':  
                        $this->Set_Alert_Response(273) ; // Contact import queued for task automation
                        break ;
                    case 'save_asset_template':      
                        $this->Set_Alert_Response(274) ; // Asset duplication request submitted to cron
                        break ;
                    case 'create_campaign': 
                    case 'duplicate_campaign':    
                        $this->Set_Alert_Response(275) ; // Campaign create requested submitted to cron
                        break ;                        
                    default:    
                    }
                
                } else {
                    $this->Set_Alert_Response(265) ; // Error creating cron task
                    $this->Append_Alert_Response($personalize,array(
                        'location' => __METHOD__,
                        'admin_context' => json_encode(
                                array('request' => $query_options,
                                     'database' => $result['error']))
                        )) ;                
                    }                        
            }
                
        return $this ; 
        }
                
                   
    public function Create_Cron_Task_Automation($query_options = array()) {
        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ;             
            }
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_cron_task_automation',
            'values' => $values_array
            ) ; 
        
        $result = $this->DB->Query('INSERT',$query_array) ;        
        $this->system_query_result = $result ;
        
        return $result ;         
        }

        
    public function Action_Update_Cron_Task_Automation($query_options = array()) {
        
        $continue = 1 ; 
        
        if (!isset($query_options['task_automation_id'])) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            
            if (isset($query_options['structured_data'])) {
                
                $incoming_structured_data = $query_options['structured_data'] ; 
                
                $list_query['filter_by_task_automation_id'] = $query_options['task_automation_id'] ; 
            
                $this->Set_Cron_Task_Automation_List($list_query) ; 
                $data['cron_task_automation_list'] = $this->Get_Cron_Task_Automation_List() ; 
                $query_options['structured_data'] = $data['cron_task_automation_list'][0]['structured_data'] ; 
                
                foreach ($incoming_structured_data as $key => $value) {
                    $query_options['structured_data'][$key] = $value ; 
                    }
                
                $query_options['structured_data'] = json_encode($query_options['structured_data']) ;                 
                }
            
            $result = $this->Update_Cron_Task_Automation($query_options) ;
            error_log($result) ; 
            }
                
        return $this ; 
        }
        

    public function Update_Cron_Task_Automation($query_options) {
        
        $values_array = array() ; 
        
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ; 
            }
            
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        
        // Updates the new account account in the database
        $query_array = array(
            'table' => 'system_cron_task_automation',
            'values' => $values_array,
            'where' => "system_cron_task_automation.task_automation_id='$query_options->task_automation_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array) ;
        $this->system_query_result = $result ;

        return $result ; 
        }
        
        
        
        
    public function Set_Cron_Task_Automation_List($query_options = array()) {
        
        
        $result = $this->Retrive_Cron_Task_Automation_List($query_options) ; 

        $this->cron_task_automation_list = array('count' => $result['result_count']) ;
        
        if ($result['result_count'] > 0) {
                        
            $i = 0 ; 
            foreach ($result['results'] as $task) {
                
                $structured_data = json_decode($task['structured_data'],true) ; 
            
                foreach ($structured_data as $key => $value) {
                    $task[$key] = $value ; 
                    }
                
                $task = $this->Action_Time_Territorialize_Dataset($task) ;
                $task['structured_data'] = $structured_data ; 
                
                $result['results'][$i] = $task ; 
                    
                $i++ ; 
                }
            
            
            $this->cron_task_automation_list = $result['results'] ; 
            
            } else {
                $this->cron_task_automation_list = $result['results'] ; 
                }
        
        
        return $this ; 
        }
     
    public function Get_Cron_Task_Automation_List() {
        
        return $this->cron_task_automation_list ; 
        }
        
        
    public function Retrive_Cron_Task_Automation_List($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => 'system_cron_task_automation',
            'where' => ''
            );
        
        
        if (isset($query_options->filter_by_task_automation_id)) {
            $query_array['where'] .= "AND system_cron_task_automation.task_automation_id='$query_options->filter_by_task_automation_id'" ; 
            }
              
        
        if ($query_options->timestamp_action_filter_status == 'active') { 
            
            switch ($query_options->filter_by_timestamp_action_filter_type) {                    
                case 'range_between':
                    $query_array['where'] .= "AND (system_cron_task_automation.timestamp_action>=$query_options->timestamp_action_filter_range_between_begin AND system_cron_task_automation.timestamp_action<=$query_options->timestamp_action_filter_range_between_end) " ; 
                    break ;            
                }            
            }
        
        
        if (isset($query_options->filter_by_processed)) { 
            switch ($query_options->filter_by_processed) {
                case 'yes':
                    $query_array['where'] .= "AND system_cron_task_automation.timestamp_processed>'1'" ;
                    break ; 
                case 'no':
                    $query_array['where'] .= "AND system_cron_task_automation.timestamp_processed='0'" ; 
                    break ; 
                case 'processing':
                    $query_array['where'] .= "AND system_cron_task_automation.timestamp_processed='1' " ; 
                    break ; 
                case 'unprocessed_processing':
                    $query_array['where'] .= "AND (system_cron_task_automation.timestamp_processed='0' OR system_cron_task_automation.timestamp_processed='1')" ; 
                    break ;                     
                }
            }
        
        if (isset($query_options->filter_by_task_action)) {
            $query_array['where'] .= "AND system_cron_task_automation.task_action='$query_options->filter_by_task_action'" ; 
            }
        
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ;           
            }

        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
        

        $query_array['where'] = ltrim($query_array['where'],"AND ") ; 
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force'); 
        $this->system_query_result = $result  ;
        
        $query_options->value_name = 'task_automation_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_System_Paging($result) ;         
        
        return $result ; 
        }
        
        
    // Takes an incoming result set and original query from any model
    // Then runs it again with no paging constraints to get total result count
    // Used for knowing paging requirements.
    // $result is the original Retrieve_ result from the incomign model
    // $query_options comes in as object
    public function Retrieve_Total_Results($result,$query_array,$query_options) {
        
        // Get the total count of users in this query
        if (isset($query_options->total_count)) { 
            
            $result['total_count'] = $query_options->total_count ; 
            
            } else {                
                    
                
                unset($query_array['limit'],$query_array['offset']) ;
            
                $count = $this->DB->Query('SELECT_JOIN',$query_array,'force'); 

                $result['value_name'] = $query_options->value_name ;
                $result['count_results'] = $count['results'] ;
                $result['count_query'] = $count['query'] ;
                $result['total_count'] = $count['result_count'] ;
                $result['uber_result_set'] = $count['results'] ;             
                }

 
        
        $result['offset_page'] = $query_options->offset_page ;

        return $result ; 
        }    
        
        
        
    // Process a set of system results and separate into paging components to use for site navigation
    // ***** IMPORTANT ****** For this to work properly, a group_by field must be set in the original query
    public function Set_System_Paging($results_array) {

//        $this->test_data = $results_array ;         
        
        if (!isset($this->system_paging)) {
            $this->system_paging = $this->Set_Default_Paging_Object() ; 
            }       
        
        $this->system_paging->page_increment = $this->Get_Page_Increment() ;
        
        $this->system_paging = $this->Action_Process_Paging($this->system_paging,$results_array) ; 
        
        return $this ; 
        }        
        
        
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////        
        
        
    public function Get_Page_Increment() {
        
        return $this->page_increment ; 
        } 
        
    public function Get_Response() {
        
        return $this->response;
        
        }
        
    public function Get_System_Default_Value() {
        
        return $this->system_default_value ; 
        } 
        
    public function Get_Auth_Permissions() {
        
        return $this->auth_permissions;        
        }
        
    public function Get_Asset_Auth_Permissions() {
        
        return $this->asset_auth_permissions;        
        }
        
    public function Get_Physical_Address_Validation() {
        return $this->physical_address_validation ;
        }
    
    public function Get_Email_Address_Validation() {
        return $this->email_address_validation ;
        }
    
    public function Get_Phone_Number_Validation() {
        return $this->phone_number_validation ;
        }
        
        
    public function Get_System_List($list_name) {

        return $this->$list_name ;             
        }
        
    public function Get_Timezone_List() {

        return $this->list_timezones ;             
        }
        
    public function Get_System_Auth_List() {

        return $this->system_auth_list ;             
        }
        
    public function Get_System_Action_Key() {

        return $this->action_key ;             
        }
        
    public function Get_System_Action() {

        return $this->system_action ;             
        }        
        
    public function Get_Search_ID() {

        return $this->search_id ;             
        }        
        
    public function Get_App_View_History() {

        return $this->app_view_history ;             
        }
     
    public function Get_App_View_History_List() {

        return $this->app_view_history_list ;             
        }
        
    public function Get_System_Version_List() {
        
        return $this->system_version_list ;         
        }
        
        
    public function Get_Metadata_Type() {
        
        return $this->metadata_type ; 
        }
        
    public function Get_Metadata_List() {
        
        return $this->metadata_list ; 
        }
        
        
    public function Get_Tag_List() {
        
        return $this->tag_list ;
        
        }
    
    public function Get_Tag() {
        
        return $this->tag ;
        
        } 
    
    public function Get_Category() {
        
        return $this->category ;
        
        }    
    
    public function Get_Category_List() {
        
        return $this->category_list ;
        
        }        
        
    public function Get_App_Page_List() {
        
        return $this->app_page_list ;
        
        }
     
    public function Get_App_Page() {
        
        return $this->app_page ;
        
        }
        
    public function Get_System_Paging() {
        
        return $this->system_paging ;
        
        }
        
    public function Get_Test_Data() {
        
        return $this->test_data ;
        
        }        
        
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    ////////////////////// 
      

        
    public function Sort_Search_Relevance($result,$query_array,$query_options,$relevance_options) {

        
        if ($result['result_count'] > 0) {
            
            $search_parameter_token_array = explode(" ",$query_options->filter_by_search_parameter) ;
            $token_array = array() ; 
            
            $t = count($search_parameter_token_array) ;
            $s = $t ; 
            
            $i = 0 ; 
            foreach ($search_parameter_token_array as $token) {
                
                $token_array[] = array(
                    'token' => $token,
                    'score_base' => $s--
                    ) ; 
                
                if ($i > 0) {
                    $r = 0 ; 
                    $this_compound_token = array() ; 
                    
                    do {
                        $this_compound_token[] = $search_parameter_token_array[$r] ; 
                        $r++ ;
                        } while ($r <= $i) ;
                        
                    $token_array[] = array(
                        'token' => implode(" ",$this_compound_token),
                        'score_base' => $i * 10 
                        ) ;                    

                    
                    if ($i < ($t-1)) {
                        $r = $i ; 
                        $this_compound_token = array() ; 

                        do {
                            $this_compound_token[] = $search_parameter_token_array[$r] ; 
                            $r++ ;
                            } while ($r < $t) ;

                        $token_array[] = array(
                            'token' => implode(" ",$this_compound_token),
                            'score_base' => $i * 10 
                            ) ;  
                        }
                    
                    }
                
                
                $i++ ; 
                }


            
            $i = 0 ; 
            foreach ($result['results'] as $record) {
                
                $record['relevance_score'] = 0 ; 
                $record['relevance_array'] = array() ; 
                
                
                $f = count($relevance_options['fields']) ; // Total field count and top field score
                
                foreach ($relevance_options['fields'] as $field) {
                    
                    foreach ($token_array as $token) {
                        
                        // Count the occurence of each token in each field
                        $occurence_count = substr_count(strtolower($record[$field]), strtolower($token['token'])) ; 
                        $record['relevance_score'] = $record['relevance_score'] + ($token['score_base'] * $occurence_count * $f) ; 

                        $record['relevance_array'][] = array(
                            'field' => $field,
                            'token' => $token['token'],
                            'score_base' => $token['score_base'],
                            'relevance_score' => $record['relevance_score'],
                            'occurence_count' => $occurence_count,
                            'field_count' => $f
                            ) ; 
                        
                        }
                     
                    
                    $f-- ; 
                    }
                
                
                $result['results'][$i]['relevance_score'] = $record['relevance_score'] ;
                $result['results'][$i]['relevance_array'] = $record['relevance_array'] ;
                $i++ ; 
                }
            
            

            if ($query_array['order_by_relevance'] == 'yes') {
                $sorted_results = $result['results'] ; 
                //Pass the object of the class and method name
                $compare = new System();      
                
                switch ($query_array['order']) {
                    case 'ASC':
                        usort($sorted_results, array($compare, 'Sort_Relevance')); 
                        break ;
                    case 'DESC':
                    default:    
                        usort($sorted_results, array($compare, 'Sort_Relevance_Desc')); 
                    }
                                               
                $result['results'] = $sorted_results ;
                }
            
            }        
        
        return $result ; 
        }
        
        
    public function Sort_Relevance( $a, $b ){
        if ( $a['relevance_score'] == $b['relevance_score'] )
            return 0;
        if ( $a['relevance_score'] < $b['relevance_score'] )
            return -1;
        return 1;
        }  
        
      public function Sort_Relevance_Desc( $b,$a ){
        if ( $a['relevance_score'] == $b['relevance_score'] )
            return 0;
        if ( $a['relevance_score'] < $b['relevance_score'] )
            return -1;
        return 1;
        } 


        
        
        
    public function Action_Update_App_Default($default_details) {
        
        $result = $this->Update_App_Default($default_details) ; 
            
        if (!$result['error']) {
            $this->Set_Alert_Response(233) ; // Metadata successfully updated
            } else {
                $this->Set_Alert_Response(234) ; // Metadata create error
                }        
        
        return $this ; 
        }
        
    public function Update_App_Default($input) {

         
        $values_array = array() ; 
        
        if (count($input) > 0) {
            
            foreach ($input as $key => $value) {
                $values_array[$key] = $value ; 
                }
            }
        
        $defaults_name = $input['defaults_name'] ; 
        
        // Updates the contact in the database
        $query_array = array(
            'table' => 'list_defaults',
            'values' => $values_array,
            'where' => "defaults_name='$defaults_name'"
            );

        $result = $this->DB->Query('UPDATE',$query_array);
        $this->system_query_result = $result ; 
        
        return $result ; 
        }
    
    
    public function Action_Create_Metadata_Relationship($relationship_details = array()) {
        
        $continue = 1 ; 
        
        if (!isset($relationship_details['item_id'])) {
            $continue = 0 ; 
            }
        if (!isset($relationship_details['metadata_id'])) {
            $continue = 0 ; 
            }
        if (!isset($relationship_details['relationship_type'])) {
            $continue = 0 ; 
            }
        
        if ($continue == 1) {
            
            $result = $this->Create_Metadata_Relationship($relationship_details) ; 
            
            if (!$result['error']) {
                $this->Set_Alert_Response(233) ; // Metadata successfully updated
                } else {
                    $this->Set_Alert_Response(234) ; // Metadata create error
                    }
            }
                
        return $this ; 
        }
        
        
        
    public function Create_Metadata_Relationship($query_options) {
    
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ;             
            }
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => "nonasset_metadata_relationships",
            'values' => $values_array,
            'where' => "nonasset_metadata_relationships.item_id='$query_options->item_id' 
                AND nonasset_metadata_relationships.metadata_id='$query_options->metadata_id' 
                AND nonasset_metadata_relationships.relationship_type='$query_options->relationship_type'"
            );

        if (isset($query_options->relationship_id)) {
            $query_array['where'] = "nonasset_metadata_relationships.relationship_id='$query_options->relationship_id'" ; 
            }
        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        
        $this->system_query_result = $result ; 
        
        return $result ; 
        }
    
    
    public function Action_Delete_Metadata_Relationship($relationship_query = array()) {
        

        $item_id_array = Utilities::Process_Comma_Separated_String($relationship_query['item_id']) ;
        
        if (count($item_id_array) > 1) {
            
            foreach ($item_id_array as $item_id) {
                
                $relationship_delete_query['item_id'] = $item_id ; 
                $relationship_delete_query['metadata_id'] = $relationship_query['metadata_id'] ; 
                $relationship_delete_query['relationship_type'] = $relationship_query['relationship_type'] ; 
                $result = $this->Delete_Metadata_Relationship($relationship_delete_query) ;
                
                }
                        
            } elseif ($relationship_query['relationship_id']) {
            
                $relationship_delete_query['relationship_id'] = $relationship_query['relationship_id'] ; 
                $result = $this->Delete_Metadata_Relationship($relationship_delete_query) ; 
            
                }
        

        return $this ; 
        }
        
    public function Delete_Metadata_Relationship($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
                
        $query_array = array(
            'table' => "nonasset_metadata_relationships",
            'where' => "nonasset_metadata_relationships.relationship_id='$metadata_relationship_id'"
            );
        
        if (isset($query_options->item_id)) {
            $query_array['where'] = "nonasset_metadata_relationships.item_id='$query_options->item_id' 
                AND nonasset_metadata_relationships.relationship_type='$query_options->relationship_type' 
                AND nonasset_metadata_relationships.metadata_id='$query_options->metadata_id' " ; 
            } else {
                $query_array['where'] = "nonasset_metadata_relationships.relationship_id='$query_options->relationship_id'" ; 
                }
        
        
        $result = $this->DB->Query('DELETE',$query_array);
            
        $this->system_query_result = $result ;        
        
        return $result ;
        }
        
        
       
    public function Set_Metadata_Relationship_List_By_Metadata_ID($asset_query_options = array(),$nonasset_query_options = array()) {
        
        
        $asset = new Asset($this->user_id) ; 
        $asset->Set_Account_By_ID($this->account_id)->Set_Profile_By_User_Account() ; 
        
        
        // Set asset relationships
        $asset_query_options_compiled = array(
            'filter_by_profile_id' => 'yes',
            'allow_global_assets' => 'all',
            'filter_by_metadata_id' => $this->metadata_id
            ) ; 
        if (isset($asset_query_options['override_paging'])) {
            $asset_query_options_compiled['override_paging'] = $asset_query_options['override_paging'] ; 
            }
        
        
        
        $asset->Set_Asset_List($asset_query_options_compiled) ; 
        $data['asset_list'] = $asset->Get_Asset_List() ; 
        
        if ($data['asset_list'] == 'error') {
            $data['asset_list'] = array() ; 
            }
        
        
        // Set non-asset relationships
        $nonasset_query_options_compiled = array() ;         
        if (isset($nonasset_query_options['override_paging'])) {
            $nonasset_query_options_compiled['override_paging'] = $nonasset_query_options['override_paging'] ; 
            }        
        
        $result = $this->Retrieve_Metadata_Relationship_List_By_Metadata_ID($nonasset_query_options_compiled) ; 
        
        
        $result_set = array(
            'assets' => $data['asset_list'],
            'non_assets' => $result['results']
            ) ; 
        
        
        $this->metadata_id_relationship_list = $result_set ; 
        
        return $this ; 
        }
    
        
        
    public function Set_Metadata_Relationship_List($query_options = array()) {
        
        if (!isset($query_options['group_by'])) {
            $query_options['group_by'] = "nonasset_metadata_relationships.metadata_id" ; 
            }
        if ($query_options['group_by'] == 'ignore') {
            unset($query_options['group_by']) ; 
            }
        
        $result = $this->Retrieve_Metadata_Relationship_List($query_options['item_id'],$query_options) ; 
        
        
        if ($result['result_count'] > 0) {
            $this->metadata_relationship_list = $result['results'] ; 
            } else {
                $this->metadata_relationship_list = array() ; 
                }
        

        
        return $this ; 
        }
        
        
    public function Get_Metadata_ID_Relationship_List() {
        
        return $this->metadata_id_relationship_list ; 
        }
        
    public function Get_Metadata_Relationship_List() {
        
        return $this->metadata_relationship_list ; 
        }
        
      
        
    // This gets the relationships between a metadata_id and other items
    public function Retrieve_Metadata_Relationship_List_By_Metadata_ID($query_options = array()) { 
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        $query_array = array(
            'table' => 'metadata',
            'fields' => "metadata.*, metadata_type.*, nonasset_metadata_relationships.*,
                users.user_id, users.first_name AS user_first_name, users.last_name AS user_last_name,
                user_profiles.user_id AS profile_user_id, user_profiles.first_name AS profile_first_name, user_profiles.last_name AS profile_last_name,
                asset_campaigns.campaign_title,
                contacts.first_name AS contact_first_name, contacts.last_name AS contact_last_name ",
            'where' => "metadata.metadata_id='$this->metadata_id' ",
            ) ; 
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            );

        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'metadata.visibility_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'nonasset_metadata_relationships',
            'on' => 'nonasset_metadata_relationships.metadata_id',
            'match' => 'metadata.metadata_id'
            );
               
        $query_array['join_tables'][] = array(
            'table' => 'users',
            'statement' => "(users.user_id=nonasset_metadata_relationships.item_id AND nonasset_metadata_relationships.relationship_type='user')",
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_profiles',
            'statement' => "(user_profiles.profile_id=nonasset_metadata_relationships.item_id AND nonasset_metadata_relationships.relationship_type='profile')",
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'asset_campaigns',
            'statement' => "(asset_campaigns.campaign_id=nonasset_metadata_relationships.item_id AND nonasset_metadata_relationships.relationship_type='campaign')",
            'type' => 'left'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'contacts',
            'statement' => "(contacts.contact_id=nonasset_metadata_relationships.item_id AND nonasset_metadata_relationships.relationship_type='contact')",
            'type' => 'left'
            );
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');

        $this->system_query_result = $result ;
        
        return $result ; 
        }
        
        
    // This takes a set of items (contacts, campaigns) and finds the metadata associated with that item    
    // CURRENTLY FOR NONASSETS ONLY -- you can get assets through Set_Asset_List=>filter_by_metadata_id
    // item_id and relationship_type required
    // item_id = the value of the primary key (e.g. contact_id, campaign_id, etc)    
    public function Retrieve_Metadata_Relationship_List($item_id_input,$query_options = array()) { 
        
        $item_id_array = Utilities::Process_Comma_Separated_String($item_id_input) ; // Turn the item_id into an array to enable searching multiple items
            
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => 'nonasset_metadata_relationships',
            'fields' => "nonasset_metadata_relationships.*, metadata.*, metadata_type.*",
            'where' => "nonasset_metadata_relationships.relationship_type='$query_options->relationship_type' ",
            'order_by' => "metadata.metadata_name",
            'order' => "ASC"
            );        
        
        if (isset($query_options->group_by)) {
            $query_array['group_by'] = $query_options->group_by ; 
            }
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata',
            'on' => 'metadata.metadata_id',
            'match' => 'nonasset_metadata_relationships.metadata_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            );

        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'metadata.visibility_id'
            );        
        
        
        $item_id_query_string = '' ; 
        foreach ($item_id_array as $item_id) {
            $item_id_query_string .= "nonasset_metadata_relationships.item_id='$item_id' OR " ; 
            }

        $item_id_query_string = rtrim($item_id_query_string," OR ") ;
        $query_array['where'] .= ' AND ('.$item_id_query_string.') ' ;            
            
            
            
        if (isset($query_options->metadata_id)) {
            $query_array['where'] .= "AND nonasset_metadata_relationships.metadata_id='$query_options->metadata_id' " ; 
            }
        
        if (isset($query_options->metadata_type_id)) {
            $query_array['where'] .= "AND metadata_type.metadata_type_id='$query_options->metadata_type_id' " ; 
            }

        if (isset($query_options->metadata_type)) {
            
            $query_options->metadata_type = Utilities::Process_Comma_Separated_String($query_options->metadata_type) ;

            foreach ($query_options->metadata_type as $metadata_type) {
                $metadata_type_query_string .= "metadata_type.metadata_type='$metadata_type' OR " ; 
                }

            $metadata_type_query_string = rtrim($metadata_type_query_string," OR ") ;
            $query_array['where'] .= ' AND ('.$metadata_type_query_string.') ' ; 
            }    
        
        
        
        // Determine how account_id & global_metadata should be filtered
        if ((isset($query_options->allow_global_assets)) OR (isset($query_options->allow_global_metadata))) {
            
            if (isset($query_options->allow_global_assets)) {
                $query_options->allow_global_metadata = $query_options->allow_global_assets ; 
                }
            
            switch ($query_options->allow_global_metadata) {
                case 'ignore':
                    // Ignore adding an account_id parameter to this query
                    $query_array['where'] .= "" ; 
                    break ;                    
                case 'only':
                    $query_array['where'] .= " AND metadata.global_metadata='1' " ; 
                    break ;
                case 'no':
                    $query_array['where'] .= " AND metadata.account_id='$this->account_id' " ; 
                    break ;
                case 1:
                case 'yes':
                default:
                    $query_array['where'] .= " AND (metadata.account_id='$this->account_id' OR metadata.global_metadata='1') " ; 
                }            
            } else {
                $query_array['where'] .= " AND metadata.account_id='$this->account_id' " ; 
                }        
        
        
        
        
        // PROCESS METADATA VISIBILITY FILTER        
        $visibility_array = $this->Retrieve_Visibility_ID_Array($query_options) ; 

        
        // Add visibility filter if there are visibility_id's to process
        $filter_by_visibility_id_string = '' ; 
        if (count($visibility_array) > 0) {

            foreach ($visibility_array as $visibility) {
                
                $this_visibility_id = $visibility['visibility_id'] ; 
                $filter_by_visibility_id_string .= "metadata.visibility_id='$this_visibility_id' 
                    OR " ; 
                
                }


            $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," 
                OR ") ; 
            $query_array['where'] .= " AND ($filter_by_visibility_id_string) " ;            
            }        
        
        
                
        $query_array['order_by'] = "metadata.metadata_name" ;
        $query_array['order'] = 'ASC' ;
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $this->system_query_result = $result ;         
        
        return $result ; 
        }        
        
        
        
    // Used to test operations as listed in system_auth_operations     
    // Compares the Users user_auth_id with allowed operations based on the feature set of their product    
    public function Action_Test_Authorization_Control($operation_name_to_test,$options = array()) {

        $find_coming_soon_feature = 0 ;
        $contextual_result = array() ;  // Initalize for showing a result with context
        
        
        // USER PERMISSION TEST
        if (!isset($options['permissions_array']) OR ($options['permissions_array'] == 'internal')) {
            $permissions_array = $this->Get_Auth_Permissions() ; 
            } else {
                $permissions_array = $options['permissions_array'] ;
                }
        
        
        $context = array(
            'operation_name_to_test' => $operation_name_to_test
//            'permissions' => $permissions_array
            ) ;
        
        
        if (!isset($permissions_array[$operation_name_to_test]['effect'])) {
            $permissions_array[$operation_name_to_test]['effect'] = 'deny' ;  // Not Authorized
           
            } else {
                $current_response = $this->Get_Response() ; // I'm not sure what the use of this could be. It could be too lenient and allow too much access ... but removing could be too restrictive
                }
        
        
        // UPGRADE TEST
        // If the operation is denied, test whether or not the user is allowed to upgrade to access the operation
        if (($options['show_upgrade_eligible'] == 'yes') AND ($permissions_array[$operation_name_to_test]['effect'] == 'deny')) {
            $options['show_context'] = 'yes' ; 
            
            $plan_edit_eligible = $this->Action_Test_Authorization_Control('billing_plan_edit') ; 
            $contextual_result['user_plan_edit'] = $plan_edit_eligible ; 
            $contextual_result['allow_via_upgrade'] = 'deny' ; // Initalize as not being allowed via upgrade
            
            switch ($contextual_result['user_plan_edit']) {
                case 'allow':
                    
                    $data['user_record'] = $this->Get_User() ; 
                    $data['account_record'] = $this->Get_Account() ; 
                    
                    $stripe = new Stripe($data['user_record']['user_id']) ;     
                    $stripe->Set_Account_By_ID($data['account_record']['account_id'])->Set_Profile_By_User_Account() ; 
                    
                    $current_product_id = $data['account_record']['product_id'] ; 
                    $stripe->Set_Product_By_ID($current_product_id) ;                     
                    $data['current_product'] = $stripe->Get_Product() ; 
                    
//                    $contextual_result['upgrade_features'] = $data['current_product'] ; 
                        
                    $find_upgrade_product = 1 ; 
                    do {
                    
                        if ($data['current_product']['upgrade_product_id']) {
                        
                            $data['upgrade_features'] = $stripe->Set_Product_By_ID($data['current_product']['upgrade_product_id'])->Action_Map_Feature_Set($data['current_product']['upgrade_product_id']) ; 
                            $data['current_product'] = $stripe->Get_Product() ; 
                            
                            $stripe->account['operations_allowed'] = $data['upgrade_features']['operations_allowed'] ; 
                            $stripe->Action_Set_Auth_Permissions() ; // Reset the auth permissions taking into account the new over-ridden account['operations_allowed'], based on the product now being tested

                            // Retest auth control for the new product permissions
                            $contextual_result['allow_via_upgrade'] = $stripe->Action_Test_Authorization_Control($operation_name_to_test) ; 
                        
                            
                            if ($contextual_result['allow_via_upgrade'] == 'allow') {
                                $find_upgrade_product = 0 ; // Found an upgradable product, so stop the loop
                                }
                            
                            } else {
                                $find_upgrade_product = 0 ; // Cannot find an upgradable product, so stop the loop
                                $find_coming_soon_feature = 1 ;
                                }

                        } while ($find_upgrade_product == 1) ; 
                    
                        $contextual_result['tested_product'] = $data['current_product'] ;
                    
                    break ; 
                default:    
                }

            // Someday build out a way to promote features that are coming soon here...
            if ($find_coming_soon_feature == 1) {


                }
            
            
            }
    
        
        
        
        // Additional authorization tests based on specific action...
        // Assume initial test passed, now testing for specific access to a record
        if ($permissions_array[$operation_name_to_test]['effect'] == 'allow') {
            switch ($permissions_array[$operation_name_to_test]['operation']['operation_name']) {
                case 'contact_import_export': // Ensure the import file belongs to the user/account
                             
                    $user_record = $this->Get_User() ; 
                    $account_record = $this->Get_Account() ; 
                    
                    $context['user_record'] = $user_record ; 
                    $context['account_record'] = $account_record ; 
                    $context['file_task_id'] = $options['file_task_id'] ; 
                    
                    
                    
                    if ($options['file_task_id']) {   
                        
                        $file_task = $this->Set_File_Task_By_ID($options['file_task_id'])->Get_File_Task() ; 
                        $context['file_task'] = $file_task ; 

                        if (($user_record['user_id'] != $file_task['user_id']) OR ($account_record['account_id'] != $file_task['account_id'])) {
//                            $permissions_array[$operation_name_to_test]['effect'] = 'deny' ;
                            }                        
                        } 
                    
                    break  ;
                }            
            }
        
        
        if ($options['show_context'] == 'yes') {
                    
            $contextual_result['auth_result'] = $permissions_array[$operation_name_to_test]['effect'] ; 
            $contextual_result[$operation_name_to_test] = $permissions_array[$operation_name_to_test]['effect'] ; 
            $contextual_result['context'] = $context ; 
            return $contextual_result ; 
            
            } else {
                return $permissions_array[$operation_name_to_test]['effect'] ;                    
                }

        }
       
        
        
    // This tests contributors authorization to edit / view assets
    public function Action_Test_Asset_Authorization_Control($asset_operation_name_to_test,$permissions_array = 'internal',$options = array()) {
    
        if ('internal' === $permissions_array) {
            $permissions_array = $this->Get_Asset_Auth_Permissions() ; 
            } 
        
        
        if (!isset($permissions_array[$asset_operation_name_to_test]['effect'])) {
            $permissions_array[$asset_operation_name_to_test]['effect'] = 'deny' ; 
            $this->Set_Alert_Response(199) ; // Not Authorized
            } else {
                $current_response = $this->Get_Response() ; 
                if ($current_response['alert_id'] != 200) {
                    $this->Set_Alert_Response(200) ; // OK - Authorized
                    }
                }

        
        // Test to see if the asset is a published global, and allow view
        if (($asset_operation_name_to_test == 'view_content') AND ($permissions_array[$asset_operation_name_to_test]['effect'] == 'deny') AND ($this->asset['asset_id'])) {
            
            $permissions_array[$asset_operation_name_to_test]['effect'] = $this->Action_Test_Asset_Public_View_Control() ; 
            $this->Set_Alert_Response(200) ; // OK - Authorized
            } 
        
        return $permissions_array[$asset_operation_name_to_test]['effect'] ; 
        }        
        
        
    // Determines if the asset SHOULD be edited based on current status.  If not, set it as view_only and 
    // skip the editing block and show a the view_content block below
    public function Action_Test_Asset_View_Type($incoming_asset,$data = array()) {
    
        $data['asset_record'] = $incoming_asset ; 
        
        if ($data['asset_record']['visibility_name'] == 'visibility_draft') {
                
            $view_type = 'asset_edit_allowed' ; 
            
            } elseif ($data['asset_record']['current_history_id'] == $data['asset_record']['active_history_id']) {
//                $view_type = 'asset_edit_allowed' ; 
                switch ($data['asset_record']['visibility_name']) {
                    case 'visibility_deleted':
                    case 'visibility_draft':
                        $view_type = 'asset_edit_allowed' ;                                 
                        break ; 
                    default: 
                        switch ($data['asset_record']['type_name']) {
                            case 'blog_post':
                            case 'product':    
                                $view_type = 'asset_edit_allowed' ; 
                                break ;
                            default:
                                $view_type = 'asset_view_only' ; 
                            }
                    }
            } elseif ($data['asset_record']['latest_history_id'] == $data['asset_record']['active_history_id']) {
//                $view_type = 'asset_edit_allowed' ;
                switch ($data['asset_record']['visibility_name']) {
                    case 'visibility_deleted':
                    case 'visibility_draft':
                        $view_type = 'asset_edit_allowed' ;                                 
                        break ; 
                    default:    
                        switch ($data['asset_record']['type_name']) {
                            case 'blog_post':
                            case 'product':    
                                $view_type = 'asset_edit_allowed' ; 
                                break ;
                            default:
                                $view_type = 'asset_view_only' ; 
                            } 
                    }
                
            } else {
                $view_type = 'asset_view_only' ; 
                }        
        
        return $view_type ; 
        
        }

        
        
        
    public function Action_Update_System_Auth_Role($query_options = array()) {
        
        $continue = 1 ; 
        
        if (!isset($query_options['auth_id'])) {
            $continue = 0 ; 
            // No Auth ID set error
            }
        
        if ($continue == 1) {
            
            if (isset($query_options['user_role_auth'])) {
                switch ($query_options['user_role_auth']) {
                    case 'true':
                        $query_options['user_role_auth'] = 1; 
                        break ;
                    case 'false':
                        $query_options['user_role_auth'] = 0; 
                        break ; 
                    }
                }
            if (isset($query_options['account_role_auth'])) {
                switch ($query_options['account_role_auth']) {
                    case 'true':
                        $query_options['account_role_auth'] = 1; 
                        break ;
                    case 'false':
                        $query_options['account_role_auth'] = 0; 
                        break ; 
                    }
                }            
            
            if (isset($query_options['permission_id'])) {
                $auth_role_permissions['permissions'] = Utilities::Process_Comma_Separated_String($query_options['permission_id']) ; 
                $query_options['auth_role_permissions'] = json_encode($auth_role_permissions) ; 
                unset($query_options['permission_id']) ;
                }

            $result = $this->Update_System_Auth_Role($query_options) ;   
            
            if (!$result['error']) {
                $this->Set_Alert_Response(225) ; // Role updated
                } else {
                    $this->Set_Alert_Response(224) ; // Role updated error
                    } 
            }
        
        return $this ; 
        }
        
        
    public function Action_Update_System_Auth_Permission($query_options = array()) {
        
        $continue = 1 ; 
        
        if (!isset($query_options['permission_id'])) {
            $continue = 0 ; 
            // No Auth ID set error
            }
        
        if ($continue == 1) {            
            
            if ($query_options['permission_id'] == 'new') {
                
                unset($query_options['permission_id']) ; 
                
                $query_options['permission_name'] = Utilities::Format_Convert_Lowercase($query_options['permission_name']) ; // Use the title as the slug
                $query_options['permission_name'] = str_replace(" ","_",$query_options['permission_name']) ; // Replace space w/ period                
                
                $result = $this->Create_System_Auth_Permission($query_options) ;  
                
                if (!$result['error']) {
                    $this->Set_Alert_Response(221) ; // Permission updated
                    } else {
                        $this->Set_Alert_Response(220) ; // Permission updated error
                        }
                
                } else {
                
                    // Decided not to use bulk-edit for selecting / de-selecting operations 
                    // at this time, because a deslected operation needs to be assigned to a different permission
                    // Convert the string of operations id's into an array
                    // Then update each operation with the matching permission ID.
                    if (isset($query_options['operation_id'])) {
                        // But what do we do with operations that were DE-selected??
                        }

                    $result = $this->Update_System_Auth_Permission($query_options) ;                   
                    }
            
            if (!$result['error']) {
                $this->Set_Alert_Response(221) ; // Permission updated
                } else {
                    $this->Set_Alert_Response(220) ; // Permission updated error
                    }            

            }
        
        return $this ; 
        }
        
        
    public function Action_Update_System_Auth_Operation($query_options = array()) {
        
        $continue = 1 ; 
        
        if (!isset($query_options['operation_id'])) {
            $continue = 0 ; 
            // No Auth ID set error
            }
        
        if ($continue == 1) {            
            
            if ($query_options['operation_id'] == 'new') {
                
                unset($query_options['operation_id']) ; 
                
                $query_options['operation_name'] = Utilities::Format_Convert_Lowercase($query_options['operation_name']) ; // Use the title as the slug
                $query_options['operation_name'] = str_replace(" ","_",$query_options['operation_name']) ; // Replace space w/ period                
                
                $result = $this->Create_System_Auth_Operation($query_options) ;  
                
                } else {
                
                    $result = $this->Update_System_Auth_Operation($query_options) ;   
                
                    } 
            
            if (!$result['error']) {
                $this->Set_Alert_Response(222) ; // Operation updated
                } else {
                    $this->Set_Alert_Response(223) ; // Operation updated error
                    }             
            }
        
        return $this ; 
        }
        
        
    // Create a new app page entry
    public function Action_Create_App_Page($query_options) {

        if (!isset($query_options['visibility_id'])) {
            $query_options['visibility_id'] = 4 ; 
            }
        
        $result = $this->Create_App_Page($query_options) ;         
        
        if ($result['insert_id']) {
            $this->app_page_id = $result['insert_id'] ; 
            $this->Set_App_Page_By_ID($result['insert_id']) ; 
            $this->Set_Alert_Response(114) ; // Success
            } else {
                if ($result['result_count'] > 0) {
                    $this->Set_Alert_Response(112) ; // Error - Duplicate entry.
                    } else {
                        $this->Set_Alert_Response(113) ; // Error - Failed entry.
                        $this->Append_Alert_Response('',array(
                            'admin_context' => json_encode($page),
                            'location' => __METHOD__
                            )) ;    
                        }                
                }

        return $this ;         
        }
        
        
    // Update an existing app page entry
    public function Action_Update_App_Page($query_options) {

        $result = $this->Update_App_Page($query_options) ;         
        
        if (!$result['error']) {
            
            $this->Set_Alert_Response(115) ; // App page updated

            } else {
            
                $this->Set_Alert_Response(116) ; // App page update failed
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($result),
                    'location' => __METHOD__
                    )) ;             
            
                }

        return $this ;         
        }
       
        
    // Update an existing app version entry
    public function Action_Update_App_Version($query_options) {

        switch ($query_options['version_id']) {
            case 'new':
                
                $result = $this->Create_App_Version($query_options) ;         

                if (!$result['error']) {

                    $this->Set_Alert_Response(214) ; // App version updated

                    } else {

                        $this->Set_Alert_Response(215) ; // App version update failed

                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($result),
                            'location' => __METHOD__
                            )) ;             

                        }
                
                break ; 
            default:
                
                $result = $this->Update_App_Version($query_options) ;         

                if (!$result['error']) {

                    $this->Set_Alert_Response(214) ; // App version updated

                    } else {

                        $this->Set_Alert_Response(215) ; // App version update failed

                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($result),
                            'location' => __METHOD__
                            )) ;             

                        }                
            }

        return $this ;         
        }
        
        
    public function Action_Create_Protected_Value($query_options) {
        
        $result = $this->Create_Protected_Value($query_options) ; 
        
        if (!$result['error']) {
            
//            $this->Set_Alert_Response(115) ; // App page updated

            } else {
            
//                $this->Set_Alert_Response(116) ; // App page update failed
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($result),
                    'location' => __METHOD__
                    )) ;             
            
                }
        
        return $this ; 
        }
        
        
    public function Action_Update_Protected_Value($query_options) {
        
        $result = $this->Update_Protected_Value($query_options) ; 
        
        if (!$result['error']) {
            
//            $this->Set_Alert_Response(115) ; // App page updated

            } else {
            
//                $this->Set_Alert_Response(116) ; // App page update failed
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($result),
                    'location' => __METHOD__
                    )) ;             
            
                }
        
        return $this ; 
        }
        
        
    public function Action_Delete_Protected_Value($query_options) {
        
        $result = $this->Delete_Protected_Value($query_options) ; 
        
        if (!$result['error']) {
            
//            $this->Set_Alert_Response(115) ; // App page updated

            } else {
            
//                $this->Set_Alert_Response(116) ; // App page update failed
            
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($result),
                    'location' => __METHOD__
                    )) ;             
            
                }
        
        return $this ; 
        }
        
        
        
        
    // Make slugs lowercase, remove spaces, only allow alphanumeric, dash, and underscore
    public function Action_Format_Slug($title_submission,$slug_submission = '') {
        
        global $server_config ; 
        
        // Create an asset slug
        if (!$slug_submission) {
            $slug_submission = $title_submission ; 
            }
        
        // If there was no title and we still need a slug...
        if (!$slug_submission) {
            $slug_submission = $server_config['timestamp_system_compiled']['mon_d_yyyy_h_mm_a'] ; 
            }    
        
        $slug_submission = Utilities::Format_Convert_Lowercase($slug_submission) ; // Use the title as the slug
        $slug_submission = str_replace(" ","-",$slug_submission) ; // Replace space w/ dash
        $slug_submission = preg_replace('/[^\w-]/', '', $slug_submission); // Allow alphanumeric, dash, and underscore
        
        return $slug_submission ;        
        }
        
        
    public function Action_Create_Metadata($metadata_input = array()) {
        
        $continue = 1 ;
        
        if (!isset($metadata_input['account_id'])) {
            $metadata_input['account_id'] = 0 ; 
            }
        
        if (!isset($metadata_input['visibility_id'])) {
            $metadata_input['visibility_id'] = 5 ; 
            }
        
        if (!isset($metadata_input['metadata_name'])) {
            $continue = 0 ;
            }
        
        
        // Determine the metadata type
        if ($continue == 1) {
            
            if (!isset($metadata_input['metadata_type_id'])) {
                
                if (isset($metadata_input['metadata_type'])) {
                    
                    $query_options = array(
                        'filter_by_metadata_type' => $metadata_input['metadata_type']
                        ) ; 
                    
                    $this->Set_Metadata_Type($query_options) ; 
                    $this->metadata = $this->Get_Metadata_Type() ; 
                    
                    $metadata_input['metadata_type_id'] = $this->metadata['metadata_type_id'] ; 
                    unset($metadata_input['metadata_type']) ; 
                    
                    } else {
                        $continue = 0 ; 
                    
                        // Add error. No metadata type available
                        }                
                }          
            }
        
        
        if ($continue == 1) {
            
            // Format and validate the slug for new metadata
            $slug_result = $this->Action_Validate_Metadata_Slug($metadata_input) ; 
            $metadata_input['metadata_slug'] = $slug_result['metadata_slug'] ; 
            
            $result = $this->Create_Metadata($metadata_input) ;
            $this->asset_query_result = $result ; 
            
            if ($result['insert_id']) {
                $this->Set_Metadata($result['insert_id']) ; 
                $this->Set_Alert_Response(120) ; // System: Success creating metadata
                } else {
                    $this->Set_Alert_Response(119) ; // System: Error creating metadata
                    $this->Append_Alert_Response('none',array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;                
                    }            
            }
                
        return $this ;         
        }
        
        
        
    // This accepts bulk updating of Metadata in the form of a comma separated string of metadata id's (metadata_id field)    
    // You cannot update title, description, or slug in bulk - only account, visiblity, and global assignment    
    public function Action_Update_Metadata($metadata_input = array()) {
        
        $continue = 1 ;
        
        
        if (isset($metadata_input['global_metadata'])) {
            switch ($metadata_input['global_metadata']) {
                case 'true':
                case 1:
                    $metadata_input['global_metadata'] = 1 ; 
                    break ; 
                case 'false':
                case 0 ; 
                    $metadata_input['global_metadata'] = 0 ; 
                    break ; 
                }
            }        
        
        if (isset($metadata_input['metadata_visible'])) {
            switch ($metadata_input['metadata_visible']) {
                case 'true':
                case 1:
                    
                    $visibility_query_options['filter_by_visibility_name'] = 'visibility_published' ; 
                    $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
                    $metadata_input['visibility_id'] = $visibility_result['visibility_id'] ;

                    break ; 
                case 'false':
                case 0 ; 
                    $visibility_query_options['filter_by_visibility_name'] = 'visibility_deleted' ; 
                    $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
                    $metadata_input['visibility_id'] = $visibility_result['visibility_id'] ;
                    break ; 
                }
            unset($metadata_input['metadata_visible']) ; 
            }
        
        
        if (isset($metadata_input['visibility_name'])) {
            $visibility_query_options['filter_by_visibility_name'] = $metadata_input['visibility_name'] ; 
            $visibility_result = $this->Retrieve_Visibility_ID_Array($visibility_query_options,'single') ; 
            $metadata_input['visibility_id'] = $visibility_result['visibility_id'] ;
            unset($metadata_input['visibility_name']) ; 
            }
        
        
//        if (!isset($metadata_input['visibility_id'])) {
//            $metadata_input['visibility_id'] = 5 ; 
//            }
//        
//        if (!isset($metadata_input['metadata_name'])) {
//            $continue = 0 ;
//            $this->Set_Alert_Response(122) ; // Error: Metadata name missing
//            $this->Append_Alert_Response($personalize,array(
//                'admin_context' => json_encode($metadata_input),
//                'location' => __METHOD__
//                )) ;            
//            }
//        
//

        
        
        if ($continue == 1) {
            
            $metadata_input['metadata_id_array'] = Utilities::Process_Comma_Separated_String($metadata_input['metadata_id']) ;


            // Format input for single submission requests
            if (count($metadata_input['metadata_id_array']) == 1) {
                
                $this->Set_Metadata($metadata_input['metadata_id']) ;
                $existing_metadata = $this->Get_Metadata() ;                 
                
                $metadata_input['metadata_type_id'] = $existing_metadata['metadata_type_id'] ; 
                
                if (isset($metadata_input['metadata_slug'])) {

                    $slug_result = $this->Action_Validate_Metadata_Slug($metadata_input) ;
                    
                    $this->system_query_result = $slug_result ; 

                    if ($slug_result['metadata_slug_edited'] == 1) { 
                        // Slug was either a duplicate or incorrectly formatted
                        // Alert was set in Action_Validate_Metadata_Slug() 
                        $continue = 0 ; 
                        } 

                    }                 
                }
            
            
            
            }
        
        
        if (!isset($metadata_input['metadata_id_array']) OR (!$metadata_input['metadata_id'])) {
            $continue = 0 ;
            $this->Set_Alert_Response(121) ; // Error: Metadata_type_id missing
            $this->Append_Alert_Response($personalize,array(
                'admin_context' => json_encode($metadata_input),
                'location' => __METHOD__
                )) ;
            }
        

        if ($continue == 1) {
            
            
            
            $result = $this->Update_Metadata($metadata_input) ;
            $this->test_data = $result ; 
            
            
            // If only one metadata_id submitted, return a success/error for that one ID.
            if (count($metadata_input['metadata_id_array']) == 1) {
                
                if (!$result['error']) {
                    $data['metadata_record'] = $this->Set_Metadata($metadata_input['metadata_id'])->Get_Metadata() ; 
                    $this->Set_Alert_Response(125) ; // System: Updated metadata success

                    $personalize['metadata_record']['metadata_type'] = $data['metadata_record']['metadata_type'] ; 

                    $this->Append_Alert_Response($personalize,array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;                
                    } else {
                        $this->Set_Alert_Response(126) ; // System: Error updating metadata
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($result),
                            'location' => __METHOD__
                            )) ;                
                        }                
                }
            
            if (count($metadata_input['metadata_id_array']) > 1) {
                
                if (!$result['error']) {
                    $data['metadata_record'] = $this->Set_Metadata($metadata_input['metadata_id_array'][0])->Get_Metadata() ; 
                    $this->Set_Alert_Response(125) ; // System: Updated metadata success

                    $personalize['metadata_record']['metadata_type'] = $data['metadata_record']['metadata_type'] ; 

                    $this->Append_Alert_Response($personalize,array(
                        'admin_context' => json_encode($result),
                        'location' => __METHOD__
                        )) ;                
                    } else {
                        $this->Set_Alert_Response(126) ; // System: Error updating metadata
                        $this->Append_Alert_Response('none',array(
                            'admin_context' => json_encode($result),
                            'location' => __METHOD__
                            )) ;                
                        }                
                }
            
            }
                
        return $this ; 
        }        
        
        
    public function Action_Validate_Metadata_Slug($metadata_input) {
        
        $query_options = (object) $metadata_input ; // Cast the options array as an object to make queries easier to write
        
        $continue = 1 ;
        $metadata_slug_edited = 0 ;
        $metadata_slug_submitted = $query_options->metadata_slug ;
        
         

        // Preformat the slug
        if (isset($query_options->metadata_slug)) {
            $query_options->metadata_slug = $this->Action_Format_Slug($query_options->metadata_name,$query_options->metadata_slug) ; 
            } else {
                $query_options->metadata_slug = $this->Action_Format_Slug($query_options->metadata_name) ; 
                }
        
        if (!isset($query_options->metadata_id)) {
            $query_options->metadata_id = 'new' ; 
            }
        
        
        $metadata_slug_formatted_submitted = $query_options->metadata_slug ;
        
        if (($metadata_slug_submitted != $metadata_slug_formatted_submitted) AND ($query_options->metadata_id != 'new')) {
            $continue = 0 ; 
            $metadata_slug_edited = 1 ; 
            $this->Set_Alert_Response(123) ; // Asset updated
            
            $personalize['metadata_record']['metadata_slug'] = $metadata_slug_submitted ;
            $personalize['metadata_record']['metadata_slug_alternate'] = $metadata_slug_formatted_submitted ; 
            
            $this->Append_Alert_Response($personalize,array(
                'admin_context' => json_encode($personalize),
                'location' => __METHOD__
                )) ;
            }
        
        
        if ($continue == 1) {
                   
                    
            // Loop through alternatives for the slug to ensure the DB only holds unique slugs
            // Slugs within type and account should be unique
            // Slugs of different types don't need to be unique
            $i = 1; // Inititalize the alternative slug title counter
        
            
            // Inititalize the alternative slug title counter
            if ($query_options->metadata_id != 'new') {
                $end_of_slug_submitted = end(Utilities::Process_Comma_Separated_String($metadata_slug_formatted_submitted,'-')) ; 
                }

            if (is_numeric($end_of_slug_submitted) == 'true') {
                $i = $end_of_slug_submitted ; 
                $metadata_slug_root = rtrim($metadata_slug_formatted_submitted,'-'.$end_of_slug_submitted) ; 
                } else {
                    $i = 1 ;
                    $metadata_slug_root = $metadata_slug_formatted_submitted ; 
                    } 
            
            do {

                $query_array = array(
                    'table' => 'metadata',
                    'values' => $query_options,
                    'where' => "metadata.metadata_slug='$query_options->metadata_slug' AND metadata.metadata_type_id='$query_options->metadata_type_id'"
                    );

//                switch ($query_options->account_id) {
//                    case 0:
//                        // Don't add a filter for account_id because we want to make sure
//                        // if we're creating global metadata that we don't duplicate ANY slug
//                        break ; 
//                    default:
//                        $query_array['where'] .= " AND (metadata.account_id='$query_options->account_id' OR metadata.global_metadata='1')" ; 
//                    }


                $result = $this->DB->Query('SELECT',$query_array) ;
                $this->system_query_result = $result ;            

                
                if ($result['result_count'] > 0) {
                    if ($query_options->metadata_id != 'new') {

                        if ($result['results']['metadata_id'] == $query_options->metadata_id) {
                            $i = 0 ; // Submitted slug matches the current slug. Allow to pass

                            } else {                        
                                $query_options->metadata_slug = $metadata_slug_root.'-'.$i ; // Add an integer to the slug if there is a duplicate
                                $metadata_slug_edited = 1 ; 
                                $i++ ;                       
                                }

                        } else {

                            // Increment the slug for a NEW metadata
                            $query_options->metadata_slug = $metadata_slug_root.'-'.$i ; // Add an integer to the slug if there is a duplicate
                            $metadata_slug_edited = 1 ; 
                            $i++ ;                    

                            }


                    } else {
                        $i = 0 ; 
                    
                        if ($metadata_slug_edited == 1) {
                            $this->Set_Alert_Response(124) ; // Slug requires unique name

                            $personalize['metadata_record']['metadata_slug'] = $metadata_slug_formatted_submitted ;
                            $personalize['metadata_record']['metadata_slug_alternate'] = $query_options->metadata_slug ; 

                            $this->Append_Alert_Response($personalize,array(
                                'admin_context' => json_encode($personalize),
                                'location' => __METHOD__
                                )) ;
                            }                    
                    
                        }                  
                } while ($i > 0);        
                    
            }
            
        $result = array(
            'metadata_name' => $query_options->metadata_name,
            'metadata_slug' => $query_options->metadata_slug,
            'metadata_slug_submitted' => $metadata_slug_submitted,
            'metadata_slug_edited' => $metadata_slug_edited,
            'input' => $metadata_input
            ) ; 
        
        return $result ;         
        }        
        
        
        
    public function Append_Alert_Response($personalization_arrays = 'none',$additional_parameters = array()) {
        
        $this->response['master_user_id'] = $this->master_user_id ; 
        $this->response['user_id'] = $this->user_id ; 
        $this->response['account_id'] = $this->account_id ; 
        $this->response['profile_id'] = $this->profile_id ;         
        
        if (isset($additional_parameters['admin_context'])) {
            $this->response['alert_admin_notes'] .= '|| CONTEXT: '.$additional_parameters['admin_context'] ; 
            }
        if (isset($additional_parameters['location'])) {
            $this->response['location'] .= $additional_parameters['location'] ; 
            }
        
        
        // Script to personalize the public facing alert messages
        if ('none' !== $personalization_arrays) {
            $this->response['alert_message_clean'] = $this->Action_Personalize_String($this->response['alert_message_clean'],$personalization_arrays) ; 
            $this->response['alert_message_code'] = $this->response['alert_message_clean'].' (Ref Code: '.$this->response['alert_id'].')' ;                    
            }
        
        
        return $this;
        }
       
  
        
    public function Action_Personalize_String($string_input,$personalization_array) {

        if (isset($personalization_array['personalization_overrides'])) {

            foreach ($personalization_array['personalization_overrides'] as $set_title => $set) {
                foreach ($set as $key => $value) {
                    $personalization_array[$set_title][$key] = $value ; 
                    }                
                }            
            }
        
        
        // SYSTEM
        $string_input = str_replace('*|system_image_delivery_url|*',CLOUDINARY_SECURE_DELIVERY_URL,$string_input) ; 
        $string_input = str_replace('*|system_image_delivery_folder|*',CLOUDINARY_DELIVERY_FOLDER,$string_input) ; 
        
        
        // SERVER
        $string_input = str_replace('*|active_domain_name|*',$personalization_array['active_domain']['name'],$string_input) ;
        
        // OPERATIONS
        $string_input = str_replace('*|success_count|*',$personalization_array['success_count'],$string_input) ;
        $string_input = str_replace('*|alert_count|*',$personalization_array['alert_count'],$string_input) ;
        $string_input = str_replace('*|error_count|*',$personalization_array['error_count'],$string_input) ;
        $string_input = str_replace('*|duplicate_count|*',$personalization_array['duplicate_count'],$string_input) ;
        $string_input = str_replace('*|field_name|*',$personalization_array['field_name'],$string_input) ;
        
        
        
        // ASSET
        $string_input = str_replace('*|asset_id|*',$personalization_array['asset_record']['asset_id'],$string_input) ; 
        $string_input = str_replace('*|asset_title|*',$personalization_array['asset_record']['asset_title'],$string_input) ; 
        $string_input = str_replace('*|asset_slug|*',$personalization_array['asset_record']['asset_slug'],$string_input) ; 
        $string_input = str_replace('*|asset_slug_original|*',$personalization_array['asset_record']['asset_slug_original'],$string_input) ; 
        $string_input = str_replace('*|asset_slug_alternate|*',$personalization_array['asset_record']['asset_slug_alternate'],$string_input) ; 

        $string_input = str_replace('*|asset_type_display_name|*',$personalization_array['asset_record']['type_display_name'],$string_input) ;
        
//        $string_input = str_replace('*|text_01|*',$personalization_array['asset_record']['text_01'],$string_input) ; 
//        $string_input = str_replace('*|text_02|*',$personalization_array['asset_record']['text_02'],$string_input) ; 
//        $string_input = str_replace('*|image_asset_01.image_delivery_url|*',$personalization_array['asset_record']['image_asset_01']['image_delivery_url'],$string_input) ;
//        $string_input = str_replace('*|image_asset_01.image_folder_filename_extension|*',$personalization_array['asset_record']['image_asset_01']['image_folder_filename_extension'],$string_input) ;
        
        
        // ACCOUNT
        $string_input = str_replace('*|account_website_address|*','*|account_public_domain|*/*|account_username|*',$string_input) ; 
        
        $string_input = str_replace('*|account_display_name|*',$personalization_array['account_record']['account_display_name'],$string_input) ; 
        $string_input = str_replace('*|account_username|*',$personalization_array['account_record']['account_username'],$string_input) ; 
        $string_input = str_replace('*|account_username_alternate|*',$personalization_array['account_record']['account_username_alternate'],$string_input) ; 

        $string_input = str_replace('*|account_tagline|*',$personalization_array['account_record']['account_tagline'],$string_input) ; 
        
        $string_input = str_replace('*|account_public_domain|*',$personalization_array['account_record']['account_public_domain']['hostname'],$string_input) ; 
        $string_input = str_replace('*|account_public_domain_hostname|*',$personalization_array['account_record']['account_public_domain']['hostname'],$string_input) ;
        $string_input = str_replace('*|account_email_domain_display_name|*',$personalization_array['account_record']['email_domain']['domain_display_name'],$string_input) ;
        
        $string_input = str_replace('*|young_living_member_id|*',$personalization_array['account_record']['young_living_id'],$string_input) ;
        
        // USER         
        $string_input = str_replace('*|user_id|*',$personalization_array['user_record']['user_id'],$string_input) ; 
        $string_input = str_replace('*|user_first_name|*',$personalization_array['user_record']['first_name'],$string_input) ; 
        $string_input = str_replace('*|user_last_name|*',$personalization_array['user_record']['last_name'],$string_input) ;
        $string_input = str_replace('*|user_email_address|*',$personalization_array['user_record']['email_address'],$string_input) ;
        $string_input = str_replace('*|user_phone_number|*',$personalization_array['user_record']['phone_number'],$string_input) ;
        $string_input = str_replace('*|registration_token|*',$personalization_array['user_record']['registration_token'],$string_input) ;
        
        // PROFILE        
        $string_input = str_replace('*|profile_username|*',$personalization_array['profile_record']['profile_username'],$string_input) ; 
        $string_input = str_replace('*|profile_username_alternate|*',$personalization_array['profile_record']['profile_username_alternate'],$string_input) ; 
        $string_input = str_replace('*|profile_first_name|*',$personalization_array['profile_record']['first_name'],$string_input) ; 
        $string_input = str_replace('*|profile_last_name|*',$personalization_array['profile_record']['last_name'],$string_input) ;
        $string_input = str_replace('*|profile_display_name|*',$personalization_array['profile_record']['profile_display_name'],$string_input) ;
        $string_input = str_replace('*|profile_email_address|*',$personalization_array['profile_record']['email_address'],$string_input) ;
        $string_input = str_replace('*|profile_role|*',$personalization_array['profile_record']['role'],$string_input) ;
        
        
        $string_input = str_replace('*|profile_image_filename|*',$personalization_array['profile_record']['profile_image']['filename'],$string_input) ;
        $string_input = str_replace('*|profile_image_filename_extension|*',$personalization_array['profile_record']['profile_image']['filename_extension'],$string_input) ;
        
        $string_input = str_replace('*|profile_phone_number|*',$personalization_array['profile_record']['phone_number_compiled']['local_pretty'],$string_input) ;
        $string_input = str_replace('*|profile_messaging_phone_number|*',$personalization_array['profile_record']['messaging_phone_number'],$string_input) ;
        $string_input = str_replace('*|profile_sending_email_address|*',$personalization_array['profile_record']['profile_username'].'@'.$personalization_array['account_record']['email_domain']['domain_display_name'],$string_input) ;
                        


        // GENERIC
        if (is_array($personalization_array['recipient_record'])) {
            $string_input = str_replace('*|contact_id|*',$personalization_array['recipient_record']['contact_id'],$string_input) ;
            $string_input = str_replace('*|first_name|*',$personalization_array['recipient_record']['first_name'],$string_input) ; 
            $string_input = str_replace('*|last_name|*',$personalization_array['recipient_record']['last_name'],$string_input) ;
            $string_input = str_replace('*|email_address|*',$personalization_array['recipient_record']['email_address'],$string_input) ;
            $string_input = str_replace('*|phone_number_international_pretty|*',$personalization_array['recipient_record']['phone_number_international_pretty'],$string_input) ;
            $string_input = str_replace('*|yl_member_number|*',$personalization_array['recipient_record']['yl_member_number'],$string_input) ;
            
            $string_input = str_replace('*|contact_token|*',$personalization_array['recipient_record']['contact_token'],$string_input) ;
            
            $string_input = str_replace('*|member_id|*',$personalization_array['recipient_record']['member_id'],$string_input) ;
            $string_input = str_replace('*|member_token|*',$personalization_array['recipient_record']['member_token'],$string_input) ;
            
            $string_input = str_replace('*|marketing_credit_balance|*',$personalization_array['recipient_record']['marketing_credit_balance'],$string_input) ;            
            
            $string_input = str_replace('*|email_id|*',$personalization_array['recipient_record']['email_id'],$string_input) ; 
            $string_input = str_replace('*|message_id|*',$personalization_array['recipient_record']['message_id'],$string_input) ; 
            
            } 
        
        if ($personalization_array['recipient_temporary'] == 'yes') {
            $string_input = str_replace('*|recipient_first_name|*','FIRST NAME',$string_input) ; 
            $string_input = str_replace('*|recipient_last_name|*','LAST NAME',$string_input) ;
            $string_input = str_replace('*|recipient_email_address|*','EMAIL ADDRESS',$string_input) ;
            $string_input = str_replace('*|recipient_phone_number_international_pretty|*','PHONE NUMBER',$string_input) ;
            }
        if ($personalization_array['recipient_temporary'] == 'remove') {
            $string_input = str_replace('*|recipient_first_name|*','',$string_input) ; 
            $string_input = str_replace('*|recipient_last_name|*','',$string_input) ;
            $string_input = str_replace('*|recipient_email_address|*','',$string_input) ;
            $string_input = str_replace('*|recipient_phone_number_international_pretty|*','',$string_input) ;
            }        
        
        // CONTACTS  
        $string_input = str_replace('*|contact_id|*',$personalization_array['contact_record']['contact_id'],$string_input) ;
        $string_input = str_replace('*|contact_first_name|*',$personalization_array['contact_record']['first_name'],$string_input) ; 
        $string_input = str_replace('*|contact_last_name|*',$personalization_array['contact_record']['last_name'],$string_input) ;
        $string_input = str_replace('*|contact_email_address|*',$personalization_array['contact_record']['email_address'],$string_input) ;
        $string_input = str_replace('*|contact_phone_number_international_pretty|*',$personalization_array['contact_record']['phone_number_international_pretty'],$string_input) ;
        $string_input = str_replace('*|contact_yl_member_number|*',$personalization_array['contact_record']['yl_member_number'],$string_input) ;
        $string_input = str_replace('*|contact_token|*',$personalization_array['contact_record']['contact_token'],$string_input) ;
                
        
        // COMMENTS        
        $string_input = str_replace('*|comment_id|*',$personalization_array['comment_record']['comment_id'],$string_input) ; 
        $string_input = str_replace('*|comment_first_name|*',$personalization_array['comment_record']['first_name'],$string_input) ; 
        $string_input = str_replace('*|comment_last_name|*',$personalization_array['comment_record']['last_name'],$string_input) ;
        $string_input = str_replace('*|comment_content|*',$personalization_array['comment_record']['comment_content'],$string_input) ;
        $string_input = str_replace('*|timestamp_comment_created_compiled.mon_d_yyyy_h_mm_a|*',$personalization_array['comment_record']['timestamp_comment_created_compiled']['mon_d_yyyy_h_mm_a'],$string_input) ;
             
        
        // CAMPAIGN        
        $string_input = str_replace('*|campaign_id|*',$personalization_array['campaign_record']['campaign_id'],$string_input) ; 
        $string_input = str_replace('*|campaign_title|*',$personalization_array['campaign_record']['campaign_title'],$string_input) ; 
        $string_input = str_replace('*|campaign_content_asset_title|*',$personalization_array['campaign_record']['content_asset_title'],$string_input) ;
        $string_input = str_replace('*|campaign_content_asset_type_display_name|*',$personalization_array['campaign_record']['content_asset_type_display_name'],$string_input) ;
        $string_input = str_replace('*|campaign_content_asset_type_display_name_lc|*',$personalization_array['campaign_record']['content_asset_type_display_name_lc'],$string_input) ;
        $string_input = str_replace('*|campaign_content_asset_public_link.public_link|*',$personalization_array['campaign_record']['content_asset_public_link']['public_link'],$string_input) ;
        $string_input = str_replace('*|campaign_delivery_asset_title|*',$personalization_array['campaign_record']['delivery_asset_title'],$string_input) ;
        $string_input = str_replace('*|campaign_timestamp_next_action_compiled.mon_d_yyyy_h_mm_a|*',$personalization_array['campaign_record']['timestamp_next_action_compiled']['mon_d_yyyy_h_mm_a'],$string_input) ;
             
        
        
        
        // METADATA
        $string_input = str_replace('*|metadata_type|*',$personalization_array['metadata_record']['metadata_type'],$string_input) ;
        $string_input = str_replace('*|metadata_slug|*',$personalization_array['metadata_record']['metadata_slug'],$string_input) ;
        $string_input = str_replace('*|metadata_slug_alternate|*',$personalization_array['metadata_record']['metadata_slug_alternate'],$string_input) ;
                
        
        // CUSTOM
        // Format as of July 2021
        // www.youngliving.com/us/en/referral/1404920
        
        $young_living_signup_url = 'www.youngliving.com/us/en/referral/*|young_living_id|*' ; 
//        if (($personalization_array['account_record']['young_living_sponsor_id']) AND ($personalization_array['account_record']['young_living_sponsor_assigned'] == 'true')) {
//            $young_living_signup_url .= 'sponsorid=*|young_living_sponsor_id|*' ; 
//            } else {
//                $young_living_signup_url .= 'sponsorid=*|young_living_id|*' ; 
//                }        
//        if ($personalization_array['account_record']['young_living_id']) {
//            $young_living_signup_url .= '&enrollerid=*|young_living_id|*' ; 
//            }        
//        $young_living_signup_url .= '&isocountrycode=US&culture=en-US&type=member' ;
        $string_input = str_replace('*|young_living_signup_url|*',$young_living_signup_url,$string_input) ;
        
        
        
        $young_living_retail_url = 'www.youngliving.com/en_US/products?' ; 
        if (($personalization_array['account_record']['young_living_sponsor_id']) AND ($personalization_array['account_record']['young_living_sponsor_assigned'] == 'true')) {
            $young_living_retail_url .= 'sponsorid=*|young_living_sponsor_id|*' ; 
            } else {
                $young_living_retail_url .= 'sponsorid=*|young_living_id|*' ; 
                }        
        if ($personalization_array['account_record']['young_living_id']) {
            $young_living_retail_url .= '&enrollerid=*|young_living_id|*' ; 
            }   
        $string_input = str_replace('*|young_living_retail_url|*',$young_living_retail_url,$string_input) ;
        
        
        
        $young_living_id_string = '' ; 
        if (($personalization_array['account_record']['young_living_sponsor_id']) AND ($personalization_array['account_record']['young_living_sponsor_assigned'] == 'true')) {
            $young_living_id_string .= 'sponsorid=*|young_living_sponsor_id|*' ; 
            } else {
                $young_living_id_string .= 'sponsorid=*|young_living_id|*' ; 
                }        
        if ($personalization_array['account_record']['young_living_id']) {
            $young_living_id_string .= '&enrollerid=*|young_living_id|*' ; 
            }        
        $string_input = str_replace('*|young_living_id_string|*',$young_living_id_string,$string_input) ;        
        
        
        
        
        $string_input = str_replace('*|young_living_id|*',$personalization_array['account_record']['young_living_id'],$string_input) ;
        $string_input = str_replace('*|young_living_sponsor_id|*',$personalization_array['account_record']['young_living_sponsor_id'],$string_input) ;
            
            
        if ($personalization_array['akid_temporary'] == 'yes') {
            $key_array_input['akid_temporary'] = 'yes' ; 
            $string_input = $this->Action_Personalize_Action_Keys($string_input,$key_array_input) ; 
            }
        
        
        return $string_input ;     
        }        
        

        
        
        
        
    // Running script creates an action key associated w/ a system defined action
    // The action_id determines the script type (verifying an email address, changing list subscription)
    // target in system_actions table maps to the target_id value in system_action_keys; it is the object that you're taking action upon
    // key_id is a value (such as email_verified = 1 or list subscription metadata_id = 46) that corresponds to the key name in system_actions table
    // key_id is the value that will be attributed to the target once the AKID link is clicked:
        // Email Verification Example
        // System Action is 1
        // The Key is email_verified, and the key_id for a verified email is 1
        // Therefore, the akid is 1_1
        // The target is the User, and the item being acted upon is their email_address_id
        // A built AKID link will convert akid:1_1 into akid=608&verify=19823747398132  <- example strings
        // Clicking that link will look up the appropriate system_action_key, verify it with the verify string, and take action upon it
        // as defined by the action_id, key_id, and target
    // The map in system_actions table will map to value_01, value_02 columns in system_action_keys
    public function Action_Create_System_Action($action_id,$user_input) { 


        $action = $this->Retrieve_Action($action_id) ; 
        $action_record = $action['results'] ;         
        $action_record['map_array'] = json_decode($action_record['map'],true) ; 

        $action_input = array(
            'action_id' => $action_id,
            'action_verify' => $this->DB->Simple_Hash(array($action_id,microtime())),
            'target_id' => $user_input[$action_record['target']],
            'key_id' => $user_input[$action_record['key_name']],
            'timestamp_created' => TIMESTAMP,
            'timestamp_expiration' => TIMESTAMP + $action_record['expiration_seconds']
            ) ; 
        
        
        if (isset($user_input['structured_data_01'])) {
            $action_input['structured_data_01'] = json_encode($user_input['structured_data_01']) ; 
            }
        
        // This assigns the master key=>value relationship of the targeted value
        foreach ($action_record['map_array'] as $key => $value) {
            $action_input[$key] = $user_input[$value] ; 
            } 
        
        $action_create = $this->Create_Action_Key($action_input) ; 
                
        if ($action_create['insert_id']) {
            $action_key = $this->Retrieve_Action_Key($action_create['insert_id']) ;
            $this->system_action = $action_key['results'] ;     
            }
        
        
        return $this;
        
        }
        
        
    // Processes a string for action key identifiers and replaces them with a clickable URL
    // Parses the string for AKIDs in the form of *|akid:##_###|* where ## = an action_id and ### = a key_id
    // It's assumed that this is part of the message compilation process coming off of an automation queue
    // so an array of action_keys will be provided as part of the campaign as $key_array_input
    public function Action_Personalize_Action_Keys($string_input,$key_array_input) {
        
        
        // Extract AKIDs from the string and place in a placeholder array $extracted_akid_array
        // Searching for *|akid:##_###|* and putting a temporary replacement *|__akid:##_###__|* in its place
        $extracted_akid_array = array() ; 
        $i = 0 ;
        do {

            $extracted_akid = Utilities::Extract_Text_Block($string_input, '*|akid:', '|*'); // 
            $string_input = str_replace('*|akid:'.$extracted_akid['extraction'].'|*','*|__akid:'.$extracted_akid['extraction'].'__|*',$string_input) ;  // Removes the file string

            $extracted_akid_array[] = explode("_",$extracted_akid['extraction']) ; 

            $i++ ;         
            } while (strpos($string_input, '*|akid:') !== false) ; 


        // Now attempt to associate each extracted AKID with a matching $key_array_input by means of action_id and key_id
        $i = 0 ; 
        foreach ($extracted_akid_array as $message_action_key) {

            $this_action_id = $message_action_key[0] ; 
            $this_key_id = $message_action_key[1] ; 

            foreach ($key_array_input as $key_input) {
                if (($this_action_id == $key_input['action_id']) AND ($this_key_id == $key_input['key_id'])) {
                    $this_target_key = $key_input ; 
                    } 
                }


            // Retrieve the System Action Key
            $action_key = $this->Set_System_Action_Key($this_target_key['action_key_id'])->Get_System_Action_Key() ; 
            
            // Format a destination url based on the retrieved action
            if ($action_key) {
                $url_destination = str_replace('*|akid|*',$action_key['action_key_id'],$action_key['url_destination']) ;  
                $url_destination = str_replace('*|akid_verify|*',$action_key['action_verify'],$url_destination) ;
                
                $extracted_akid_array[$i]['url_destination'] = $url_destination ; 
                
                } 

            $i++ ; 
            unset($action_key) ; 
            }                

        
        // Replace the AKID with the destination URL
        foreach ($extracted_akid_array as $akid) {

            if ($key_array_input['akid_temporary'] == 'yes') {
                $string_input = str_replace('*|__akid:'.$akid[0].'_'.$akid[1].'__|*','PLACEHOLDER_LINK',$string_input) ;            
                } else {
                    $string_input = str_replace('*|__akid:'.$akid[0].'_'.$akid[1].'__|*',$akid['url_destination'],$string_input) ;            
                    }
            
            }
                
        // Output the string
        return $string_input ;             
        }
        
        
    public function Action_Verify_System_Action_Key($input_action_key_id,$input_action_key_verify) {
        
        
        
        $action_key = $this->Get_System_Action_Key() ; 
        
        $pass = 1 ; 
        
        if (!isset($input_action_key_id) OR ($input_action_key_id != $action_key['action_key_id'])) {
            $pass = 0 ;
            $alert_id = 43 ; // action_key_id mismatch
            }
        
        if (!isset($input_action_key_verify) OR ($input_action_key_verify != $action_key['action_verify'])) {
            $pass = 0 ;
            $alert_id = 42 ; // Verification key incorrect
            }        
        
        if ($action_key['timestamp_expiration'] < TIMESTAMP) {
            $pass = 0 ; 
            $alert_id = 41 ; // Link expired
            }
        
        if ($action_key['used'] == 1) {
            $pass = 0 ; 
            $alert_id = 44 ; // Link has been used
            }
        
        if ($pass == 1) {
            $alert_id = 40 ; // Verification passed
            }
        
        $response = $this->Set_Alert_Response($alert_id)->Get_Response() ; 
        $response['verified'] = $pass ; 
        $this->action_key['verification'] = $response ; 
        
        return $this ; 
        
        }
       
    public function Action_Complete_System_Action_Key() {
        
        $input = array() ; 
        $input['used'] = 1 ; 
        
        $result = $this->Update_System_Action_Key($input) ;

        return $this ; 
        
        }
        
    public function Action_Uncomplete_System_Action_Key() {
        
        $input = array() ; 
        $input['used'] = 0 ; 
        
        $result = $this->Update_System_Action_Key($input) ;

        return $this ; 
        
        }
        
        
    public function Action_Convert_Datetime_To_Timestamp($dataset) {
        
        foreach ($dataset as $key => $value) {
            $key_array = explode("_", $key) ; 
            if ($key_array[0] == 'datetime') {
                $new_key = str_replace("datetime_", "",$key) ;
                
                switch ($value) {
                    case 'remove':
                        $dataset[$new_key] = 0 ; 
                        break ;
                    default:    
                        $new_time = new \DateTime($value,new \DateTimeZone($this->user['timezone'])) ; 
                        $dataset[$new_key] = $new_time->getTimestamp() ;                        
                    }
                unset($dataset[$key]) ; 
                }                    
            }
        return $dataset ;
        }    
        
        
    public function Action_Time_Territorialize_Dataset($dataset) { 
        
        
        if ($this->user['timezone']) {
            $timezone = $this->user['timezone'] ; 
            } else {
                $timezone = $this->system_timezone ; 
                }
        
        
//        error_log('System.Action_Time_Territorialize_Dataset() user timezone '.$this->user['timezone'])  ;
//        error_log('System.Action_Time_Territorialize_Dataset() system timezone '.$this->system_timezone)  ;
                
        
        switch ($this->user['country_id']) {                   
            case 'GB':
            case 'UK':                
                $mmdd = 'd/m' ; 
                $mdyy = 'j/n/y' ; 
                $mdyyyy = 'j/n/Y' ;
                $mmddyy = 'd/m/y' ; 
                $mon_d = 'j M' ;
                $mon_d_yyyy = 'j M, Y' ;
                $month_d_yyyy = 'j F, Y' ;
                break ; 
            case 'US': 
            default:
                $mmdd = 'm/d' ; 
                $mdyy = 'n/j/y' ; 
                $mdyyyy = 'n/j/Y' ;
                $mmddyy = 'm/d/Y' ; 
                $mon_d = 'M j' ;
                $mon_d_yyyy = 'M j, Y' ;
                $month_d_yyyy = 'F j, Y' ;
                break ;                
            }
        
        $day = 'l' ; // Thursday
        $day_abbr = 'D' ; // Thurs.
        
        $yyyy = 'Y' ;
        $yy = 'y' ;
        
        $hh_mm = 'G:i' ; // military time
        $h_mm_a = 'g:i A' ; // hours an minutes, no leading 0 on hours, show AM/PM
        $hh_mm_a = 'G:i A' ;   // hours an minutes, include leading 0 on hours, show AM/PM      
        
        
//        print_r('asset name  '.$dataset['asset_id'].' '.$dataset['asset_title']) ; 
        
        foreach ($dataset as $key => $value) {
            
            if ((substr($key, 0, strlen('timestamp_')) === 'timestamp_') OR (substr($key, 0, strlen('microtimestamp_')) === 'microtimestamp_') OR (substr($key, 0, strlen('date_')) === 'date_')) {
                if ($value == '') {
                    $value = 0 ; 
                    }                

                
                // for timestamps
                if ((substr($key, 0, strlen('timestamp_')) === 'timestamp_') OR (substr($key, 0, strlen('microtimestamp_')) === 'microtimestamp_')) {
                   
                    $new_time = new \DateTime() ; 
                    $new_time->setTimestamp($value) ; 
                    $new_time->setTimezone(new \DateTimeZone($timezone)) ;
                    }
            
                // for dates
                if (substr($key, 0, strlen('date_')) === 'date_') {
                    
                    if ($value == null) {
                        $value = '' ; 
                        }
                    
                    $new_time = new \DateTime($value,new \DateTimeZone($timezone)) ; 
                    }                
                

                // https://code.tutsplus.com/tutorials/working-with-date-and-time-in-php--cms-31768
                $dataset[$key.'_compiled'] = array(
                    'unix' => $new_time->getTimestamp(),
                    'day' => $new_time->format($day),
                    'day_abbr' => $new_time->format($day_abbr),
                    'dd' => $new_time->format('d'),
                    'month' => $new_time->format('F'),
                    'mm' => $new_time->format('m'),
                    'year' => $new_time->format($yyyy),
                    'yy' => $new_time->format($yy),
                    'mm/dd' => $new_time->format($mmdd),
                    'm/d/yy' => $new_time->format($mdyy),                    
                    'm/d/yyyy' => $new_time->format($mdyyyy),
                    'mm/dd/yyyy' => $new_time->format($mmddyy),
                    'mon_d' => $new_time->format($mon_d),
                    'mon_d_yyyy' => $new_time->format($mon_d_yyyy),
                    'mon_d_yyyy_h_mm_a' => $new_time->format($mon_d_yyyy).' '.$new_time->format($h_mm_a),
                    'month_d_yyyy' => $new_time->format($month_d_yyyy),
                    'hh_mm' => $new_time->format($hh_mm), 
                    'h_mm_a' => $new_time->format($h_mm_a), 
                    'hh_mm_a' => $new_time->format($hh_mm_a), 
                    'h' => $new_time->format('h'),
                    'i' => $new_time->format('i'), 
                    'timezone' => $new_time->format('e'),
                    'timezone_abbr' => $new_time->format('T'),
                    'admin' => $new_time->format('M j, Y').' '.$new_time->format('g:i A T')
                    ) ; 

                
                $now_time = new \DateTime();
                $now_time->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
//                $now_time->setTimestamp(TIMESTAMP)->setTimezone(new \DateTimeZone($timezone));
                
                $new_time->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
                
                $interval = $now_time->diff($new_time);
                $elapsed_days = $interval->format("%R%a");

                $dataset[$key.'_compiled']['relative_days'] = $elapsed_days ; 
                
                switch( $elapsed_days ) {
                    case 0:
                        $dataset[$key.'_compiled']['relative_name'] = 'Today' ; 
                        break;
                    case -1:
                        $dataset[$key.'_compiled']['relative_name'] = 'Yesterday' ; 
                        break;
                    case +1:
                        $dataset[$key.'_compiled']['relative_name'] = 'Tomorrow' ; 
                        break;
                    default:
                        if (($elapsed_days >= -7) AND ($elapsed_days <= -2)) {
                            $dataset[$key.'_compiled']['relative_name'] = $dataset[$key.'_compiled']['day_abbr'] ;     
                            } else {
                                 $dataset[$key.'_compiled']['relative_name'] = $dataset[$key.'_compiled']['m/d/yy'] ; 
                                }
                        
                }
                
                
                
                
                if ($value == 0) {
                    foreach ($dataset[$key.'_compiled'] as $subkey => $subvalue) {
                        $dataset[$key.'_compiled'][$subkey] = '' ; 
                        }                
                    }
                }                        
            }
        
        return $dataset ;
        }
        
        
    // https://stackoverflow.com/questions/13637145/split-text-string-into-first-and-last-name-in-php
    // Had to add an additional \' to the end of preg_replace range to allow single apostrophes in names
    public function Action_Split_Names($full_name) {
        
        $full_name = trim($full_name);
        $last_name = (strpos($full_name, ' ') === false) ? '' : preg_replace('#.*\s([\w-\']*)$#', '$1', $full_name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $full_name ) );
        
        $result = array(
            'first_name' => $first_name,
            'last_name' => $last_name
            ) ; 
        
        return $result ;
        }
        
        
    public function Action_Phone_Territorialize_Dataset($dataset,$country_dialing_code = '') { 
        
                    
        
        
        foreach ($dataset as $key => $value) {        
            if ((substr($key, 0, strlen('phone_number')) === 'phone_number') AND ($key != 'phone_number_id')) {
                if ($value == '') {
                    $value = 0 ; 
                    }

                // Maybe at some point we can extract the dialing code from the raw inernational number
//                if ((substr($value, 0, strlen('+')) === '+')) {
//                    $phone_number_options['phone_country_dialing_code'] = '+1' ;
//                    }                
                if (isset($dataset['country_dialing_code'])) {
                    $phone_number_options['phone_country_dialing_code'] = $dataset['country_dialing_code'] ; 
                    }
                if (isset($dataset['phone_country_dialing_code'])) {
                    $phone_number_options['phone_country_dialing_code'] = $dataset['phone_country_dialing_code'] ; 
                    }                
                if (isset($dataset['country_id'])) {
                    $phone_number_options['country_id'] = $dataset['country_id'] ; 
                    }
                
                $this->Action_Validate_Phone_Number($value,$phone_number_options) ; 
                $phone_number_formatted = $this->Get_Phone_Number_Validation() ; 
                    
                $dataset[$key.'_compiled'] = array() ; 
                foreach ($phone_number_formatted as $phone_key => $phone_value) {
                    $dataset[$key.'_compiled'][$phone_key] = $phone_value ; 
                    }
                
                if ($value == 0) {
                    foreach ($dataset[$key.'_compiled'] as $subkey => $subvalue) {
                        switch ($subkey) {
                            case 'valid':
                                $dataset[$key.'_compiled'][$phone_key] = $phone_value ; 
                                break ; 
                            default:
                                $dataset[$key.'_compiled'][$subkey] = '' ;     
                            }                        
                        }                
                    }

                }
            }
        
        return $dataset ;
        }
        
        
    // Make sure this is a valid phone number. Really just looking for length of digits
    public function Action_Validate_Phone_Number($phone_number,$query_parameters = array()) {
        
        $input_array = $query_parameters ; 
        $input_array['phone_number'] = $phone_number ; 
        $data = array() ; 
        $data['input_array'] = $input_array ; 
        
        
        $continue = 1 ; 
        
//        $country_dialing_code = 'null' ; 
        
        // Set the default country dialing code 
        if ($query_parameters['phone_country_dialing_code']) {
            $country_dialing_code = $query_parameters['phone_country_dialing_code'] ;
            
            } elseif ($query_parameters['country_id']) {
                
                // Consider using this as backup in the event we need to pull the country_dialing_code based off a supplied parameter
                if ($query_parameters['country_id']) {

                    $country_query = array(
                        'country_id' => $query_parameters['country_id']
                        ) ; 
                    $this->Set_System_List('list_countries',$country_query) ; 
                    $country_list = $this->Get_System_List('list_countries') ;

                    if (count($country_list) > 0) {
                        $country_dialing_code = $country_list[0]['country_dialing_code'] ; 
                        } 
                    
                    }
                        
            } elseif (!$country_dialing_code) {
                $country_dialing_code = $this->user['country_dialing_code'] ; 
            } else {
                $country_dialing_code = '+1' ; // Default to US if none can be provided.
                }         
        
        
        
        
        $phone_validate = Utilities::Validate_Phone_Number($phone_number,$country_dialing_code) ; 
        
        $phone_formatted = Utilities::Format_Phone_Number($phone_validate['phone_number'],$country_dialing_code) ; 
        $phone_formatted['valid'] = $phone_validate['valid'] ; 
            
        switch ($phone_formatted['valid']) {               
            case 'yes':
                
                $phone_query_array = array(
                    'phone_number' => $phone_validate['phone_number'],
                    'phone_country_dialing_code' => $country_dialing_code
                    ) ;
                if (isset($input_array['phone_owner'])) {
                    $phone_query_array['phone_owner'] = $input_array['phone_owner'] ; 
                    }
                if (isset($input_array['user_id'])) {
                    $phone_query_array['user_id'] = $input_array['user_id'] ; 
                    }
                
                $result = $this->Retrieve_Phone_Number($phone_query_array) ; 

                
                if ($result['result_count'] > 0) {
                    
                    switch ($phone_query_array['phone_owner']) {
                        case 'none':
                            
                            break ; 
                        case 'contact':
                            
                            if ($result['results'][0]['user_id'] == 0) {
                                $this->Set_Alert_Response(76) ; // Phone is valid
                                $phone_formatted['duplicate'] = 'unused' ;
                                
                                } elseif ($result['results'][0]['contact_id'] == $phone_query_array['contact_id']) {

                                // Phone belongs to another THIS contact for THIS user
                                $this->Set_Alert_Response(76) ; // Phone is valid
                                $phone_formatted['duplicate'] = 'unused' ;
                                
                                } else {
                                
                                    $continue = 0 ; 
                                    $this->Set_Alert_Response(196) ; // Phone number is taken by another contact from this user
                                    $phone_formatted['duplicate'] = 'yes' ;                        
                                    $phone_formatted['duplicate_of'] = array(
                                        'contact_id' => $result['results'][0]['contact_id']
                                        ) ; 
                                
                                    }                            

                            break ; 
                        case 'user':
                        default:    
                            
                            if ($result['results'][0]['user_id'] == 0) {
                                $this->Set_Alert_Response(76) ; // Phone is valid
                                $phone_formatted['duplicate'] = 'unused' ;
                                $phone_formatted['phone_number_id'] = $result['results'][0]['phone_number_id'] ;
                                } elseif ($result['results'][0]['user_id'] == $this->user_id) {

                                // Phone belongs to current user
                                $this->Set_Alert_Response(76) ; // Phone is valid
                                $phone_formatted['duplicate'] = 'unused' ;
                                $phone_formatted['phone_number_id'] = $result['results'][0]['phone_number_id'] ;

                                } else {
                                    $continue = 0 ; 
                                    $this->Set_Alert_Response(196) ; // Phone number is taken
                                    $phone_formatted['duplicate'] = 'yes' ;                        
                                    }                            
                            
                        }
                                        
                    } else {
                        $this->Set_Alert_Response(76) ; // Phone is valid
                        $phone_formatted['duplicate'] = 'no' ;
                        }                 
                                
                break ;
            case 'no':
            default:
                $this->Set_Alert_Response(77) ; // Phone number is not valid
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($data),
                    'location' => __METHOD__
                    )) ;
                break ;                 
            }
        
        
        $phone_formatted['original_input'] = $input_array ; 
        
        $this->phone_number_validation = $phone_formatted ; 
            
        return $this ;     
        }
        
        
    public function Action_Process_History_List($history_list,$supporting_data = array()) {
        
        $final_history_list = array() ;
        
        foreach ($history_list as $history) {
            
            $start = json_decode($history['notes']) ; 
            
            $history['notes_pretty'] = json_encode($start, JSON_PRETTY_PRINT);
                
            switch ($history['function']) {
                case 'create_contact_note':    
                    $history['function_display_name']  = 'Contact Note Created' ;
                    break ;                     
                case 'update_contact':    
                    $history['function_display_name']  = 'Contact Updated' ;
                    break ;
                case 'update_contact_note':    
                    $history['function_display_name']  = 'Contact Note Updated' ;
                    break ;                    
                }
            
            $history['timestamp_formatted'] = date("n/j/y",$history['timestamp_created']).' '.date("h:i:s A",$history['timestamp_created']) ; 
                
            $final_history_list[] = $history ; 
            } 
        
        return $final_history_list ; 
        }
        
        
        
    // Save a json_encode set of parameters associated with an in-app view
    // Use the saved set to reference in the future when returning to the same filtered spot in view 
    // pages (e.g. within Contacts list, filtered for "mary" on page 2... can return to same spot using back button)
    // Use in conjunction with Set_App_View_History($view_history_query)->Get_App_View_History() ;
    public function Action_Save_App_View_History($view_input) {

        if (!isset($view_input['profile_id'])) {
            
            if (!isset($this->profile_id)) {
                $this->Set_Profile_By_User_Account() ; 
                }
            
            $view_input['profile_id'] = $this->profile_id ; 
            }        
        
        if (isset($view_input['view_parameters'])) {
            $view_input['view_parameters'] = json_encode($view_input['view_parameters']) ; 
            }
        if (isset($view_input['display_columns'])) {
            $view_input['display_columns'] = json_encode($view_input['display_columns']) ; 
            }
        
        $result = $this->Create_App_View_History($view_input) ; 

        if ($result['insert_id']) {
            $this->Set_App_View_History_By_ID($result['insert_id']) ;             
            } else {
                $this->Set_App_View_History_By_ID($result['original_results']['view_history_id']) ; 
                }
        
        return $this ;             
        }
        
        
    public function Action_Create_Saved_App_View($view_input) {

        $continue = 1 ; 
        if (isset($view_input['view_parameters'])) {
            $view_input['view_parameters'] = json_encode($view_input['view_parameters']) ; 
            }
        if (isset($view_input['display_columns'])) {
            $view_input['display_columns'] = json_encode($view_input['display_columns']) ; 
            }
        
            
        if (!$view_input['view_title']) {
            $continue = 0 ; 
            $this->Set_Alert_Response(238) ; // Error creating view. No title.
            }
        
        if ($continue == 1) {
            $result = $this->Create_Saved_App_View($view_input) ;
            
            if ($result['insert_id']) {
                $this->Set_Alert_Response(235) ; // New view created
                } else {
                    $this->Set_Alert_Response(237) ; // Error creating view
                    }             
            }
        
        
        return $this ;             
        }
        
        
    public function Action_Update_Saved_App_View($view_input) {

        if (isset($view_input['view_parameters'])) {
            $view_input['view_parameters'] = json_encode($view_input['view_parameters']) ; 
            }
        if (isset($view_input['display_columns'])) {
            $view_input['display_columns'] = json_encode($view_input['display_columns']) ; 
            }
        
                
        $result = $this->Update_Saved_App_View($view_input) ; 

        
        if (!$result['error']) {
            $this->Set_Alert_Response(236) ; // View saved
            } else {
                $this->Set_Alert_Response(237) ; // Error updating view
                }
        
        return $this ;             
        }
        
        
    public function Action_Search_Recall($search_id,$session_public_id) {

        
        
        // Retrieve the previously saved search record
        $search_recall = $this->Retrieve_Saved_Search($search_id,$session_public_id) ;
        $search_result_decoded = json_decode($search_recall['results']['search_result']) ;           
        
        $search_recall['saved_search_total_count'] = count($search_result_decoded) ; 
        
        // Create a temporary table to hold the search result keys
        $query_array = array(
            'new_table' => "temporary_".$session_public_id.'_'.TIMESTAMP.'_'.$search_id,
            'like_table' => "template_saved_search"
            );
            
        $result = $this->DB->Query('CREATE_TABLE',$query_array) ;  
        $search_recall['create_table'] = $result ; 
        
        $query_array = array(
            'table' => $result['new_table_name'],
            'values' => array(
                'key_id' => $search_result_decoded
                )
            );

        $search_recall['set_new_table'] = $this->DB->Query('BULK_INSERT',$query_array) ;       
        
        return $search_recall ;             
        }
        
    public function Action_Close_Search_Recall($new_table_name) {

        
        
        $query_array = array(
            'table' => $new_table_name
            );
        $drop_table = $this->DB->Query('DROP_TABLE',$query_array) ;      
        
        return this ;             
        }
        
        
    public function Action_Search_Save($search_input) {

        if (!isset($search_input['session_public_id'])) {
            $search_input['session_public_id'] = SESSION_PUBLIC_ID ; 
            }        
        
        if (isset($search_input['search_parameters'])) {
            $search_input['search_parameters'] = json_encode($search_input['search_parameters']) ; 
            }
        
        $search_create = $this->Create_Search($search_input) ; 
        $this->search_id = $search_create['insert_id'] ; 

        return $this ;             
        }
        
        
        
    
        
        
        static public function Cron_Open($cron_id) {

            global $DB ;            
            
            $query_array = array(
                'table' => "system_cron_jobs",
                'values' => array(
                    'microtimestamp_last_run' => microtime(true)
                    ),
                'where' => "cron_id='$cron_id'"
                );
            
            $cron_update = $DB->Query('UPDATE',$query_array) ;
            
            
            $query_array = array(
                'table' => "system_log_cron_history",
                'values' => array(
                    'cron_id' => $cron_id,
                    'microtimestamp_opened' => microtime(true)
                    )
                );
            
            $log_create = $DB->Query('INSERT',$query_array) ;
            
            $return_array = array(
                'log_id' => $log_create['insert_id'],
                'log_create' =>  $log_create,
                'cron_update' =>  $cron_update
                ) ; 
            
            return $return_array ; 

            }

        static public function Cron_Update($log_id,$notes = '') {

            global $DB ;           
            
            $query_array = array(
                'table' => "system_log_cron_history",
                'fields' => "system_log_cron_history.microtimestamp_opened",
                'where' => "log_id='$log_id'"
                );
            
            $result = $DB->Query('SELECT',$query_array) ;
            $seconds_elapsed = microtime(true) - $result['results']['microtimestamp_opened'] ; 
            
            $query_array = array(
                'table' => "system_log_cron_history",
                'values' => array(
                    'notes' => json_encode($notes),
                    'seconds_elapsed' => $seconds_elapsed
                    ),
                'where' => "log_id='$log_id'"
                );
            
            $result = $DB->Query('UPDATE',$query_array) ;
            
            $return_array = array(
                'result' =>  $result
                ) ; 
            
            return $return_array ; 
            }
        
        static public function Cron_Close($log_id,$notes = '') {

            global $DB ;           
            
            $query_array = array(
                'table' => "system_log_cron_history",
                'fields' => "system_log_cron_history.microtimestamp_opened",
                'where' => "log_id='$log_id'"
                );
            
            $result = $DB->Query('SELECT',$query_array) ;
            $seconds_elapsed = microtime(true) - $result['results']['microtimestamp_opened'] ; 
            
            $query_array = array(
                'table' => "system_log_cron_history",
                'values' => array(
                    'notes' => json_encode($notes),
                    'microtimestamp_closed' => microtime(true),
                    'seconds_elapsed' => $seconds_elapsed
                    ),
                'where' => "log_id='$log_id'"
                );
            
            $result = $DB->Query('UPDATE',$query_array) ;
            
            $return_array = array(
                'result' =>  $result
                ) ; 
            
            return $return_array ; 
            } 

        
        // Execute a cron via CURL
        // EXAMPLE: $run_a_cron = System::Cron_Execute('fifteen',2) ; 
        static public function Cron_Execute($script_action,$cron_id = 'none',$additional_params = array()) {

             
            global $version ;
            
            
            // Setting cron_id enables running an individual script. Ignoring runs entire parent script_action
            if ('none' === $cron_id) {
                $cron_id_param = '' ; 
                } else {
                    $cron_id_param = '&cron_id='.$cron_id ;
                    }
            
            if (!isset($additional_params['avs'])) {
                $additional_params['avs'] = $version['version_name'] ; 
                }
            if (!isset($additional_params['system_mode'])) {
                $additional_params['system_mode'] = $_SESSION['system_mode'] ; 
                if ($additional_params['system_mode'] == 'system_test') {
                    $additional_params['verify'] = $version['version_test_key'] ;
                    }
                }
            
            
            // These should be key value pairs to pass in the URL
            if (count($additional_params) > 0) {
                foreach ($additional_params as $key => $value) {
                    $cron_id_param .= '&'.$key.'='.$value ;
                    }
                }
            

            
            $keys_to_hash = array(API_KEY_INTERNAL_APP_API_KEY,$script_action) ; 
            $key_hash = DAL::Simple_Hash($keys_to_hash) ; 
            
            $url = PUBLIC_HOME_PATH.'/cron/'.$script_action.'?key='.$key_hash.$cron_id_param ; 
            $fields = array() ; 
            $result = Utilities::CurlExecute($url, $fields) ; 
            
            $return_array = array(
                'result' =>  $result
                ) ; 
            
            return $return_array ; 

            } 
        
        
        
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    //////////////////////         
        
        
    // Run a DB query outside of models    
    public function DAL_Query($query_type,$query_array,$force_result_array = 0) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        // Run query
        $result = $this->DB->Query($query_type,$query_array,$force_result_array) ;                 
        
        return $result ;  
        }
        
        
        
    public function Create_Metadata($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => 'metadata',
            'values' => $query_options
            ) ; 
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->asset_query_result = $result ;            
        
        return $result ;  
        } 
        
        
        
        
    public function Create_Action_Key($action_input) {
        
         

        $query_array = array(
            'table' => 'system_action_keys',
            'values' => $action_input
            );
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        return $result ; 
        
        }
        
    public function Create_App_Page($entry_input) {

        $entry_input = (object) $entry_input ; // Cast the input array as an object to make queries easier to write        

        $query_array = array(
            'table' => "app_pages",
            'values' => array(
                'page_url' => $entry_input->page_url,
                'page_title' => $entry_input->page_title,
                'page_description' => $entry_input->page_description,
                'page_keywords' => $entry_input->page_keywords,
                'visibility_id' => $entry_input->visibility_id
                ),
            'where' => "app_pages.page_url='$entry_input->page_url'"
            );
        
        $result = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;   
        $this->system_query_result = $result ; 
        
        return $result ;            
        }
        
        
    public function Create_App_Version($entry_input) {

        $entry_input = (object) $entry_input ; // Cast the input array as an object to make queries easier to write        

        $query_array = array(
            'table' => "system_version_control",
            'values' => array(
                'version' => $entry_input->version,
                'version_title' => $entry_input->version_title,
                'admin_notes' => $entry_input->admin_notes,
                'timestamp_version_created' => TIMESTAMP,
                'timestamp_version_updated' => TIMESTAMP
                )
            );
        
        $result = $this->DB->Query('INSERT',$query_array) ;   
        $this->system_query_result = $result ; 
        
        return $result ;            
        }
        
        
    public function Create_Protected_Value($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
         
        
        $query_array = array(
            'table' => 'list_protected_values',
            'values' => $query_options
            ) ; 
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->asset_query_result = $result ;            
        
        return $result ;  
        }
        
        
        
    public function Update_App_Page($input) {

         
        $values_array = array() ; 
        
        if (count($input) > 0) {
            
            foreach ($input as $key => $value) {
                $values_array[$key] = $value ; 
                }
            }
        
        // Updates the contact in the database
        $query_array = array(
            'table' => 'app_pages',
            'values' => $values_array,
            'where' => "app_page_id='$this->app_page_id'"
            );

        $result = $this->DB->Query('UPDATE',$query_array);
        $this->system_query_result = $result ; 
                
        return $result ;         
        }        
        
        
    public function Update_App_Version($input) {

        
        $version_id = $input['version_id'] ; 

        if (isset($input['supported'])) {
            switch ($input['supported']) {
                case 'true':   
                case 1:                    
                    $input['supported'] = 1 ; 
                    break ; 
                case 'false':   
                case 0:                    
                    $input['supported'] = 0 ; 
                    break ;                    
                }
            }

        if (isset($input['stable'])) {
            switch ($input['stable']) {
                case 'true':
                case 1:                    
                    $input['stable'] = 1 ; 
                    break ; 
                case 'false':  
                case 0:                    
                    $input['stable'] = 0 ; 
                    break ;                    
                }
            }        
        

        $values_array = array() ; 
        
        if (count($input) > 0) {
            
            foreach ($input as $key => $value) {
                $values_array[$key] = $value ; 
                }
            
            $values_array['timestamp_version_updated'] = TIMESTAMP ; 
            
            
            // Updates the item in the database
            $query_array = array(
                'table' => 'system_version_control',
                'values' => $values_array,
                'where' => "system_version_control.version_id='$version_id'"
                );

            $result = $this->DB->Query('UPDATE',$query_array);
            $this->system_query_result = $result ;            
            }
         
                
        return $result ;         
        }
        
        
    public function Update_Protected_Value($input) {

         
        $values_array = array() ; 
        
        if (count($input) > 0) {
            
            $value_id = $input['value_id'] ; 
            unset($input['value_id']) ; 
            
            foreach ($input as $key => $value) {
                $values_array[$key] = $value ; 
                }
            }
        
        // Updates the contact in the database
        $query_array = array(
            'table' => 'list_protected_values',
            'values' => $values_array,
            'where' => "list_protected_values.value_id='$value_id'"
            );

        $result = $this->DB->Query('UPDATE',$query_array);
        $this->system_query_result = $result ; 
                
        return $result ;         
        } 
        
    public function Delete_Protected_Value($input) {

         
        
        $value_id = $input['value_id'] ;
        
        $query_array = array(
            'table' => "list_protected_values",
            'where' => "list_protected_values.value_id='$value_id'"
            );
        
        $result = $this->DB->Query('DELETE',$query_array);
        $this->asset_query_result = $result ;
        
        return $result ;         
        }
        
        
    public function Update_Metadata($input) {

        
        // Create WHERE statement, allowing multiple metadata_id
        $where_statement = '' ; 
        foreach ($input['metadata_id_array'] as $metadata_id) {
            $where_statement .= "metadata.metadata_id='$metadata_id' OR " ; 
            }
        $where_statement = '('.$this->DB->Query_Cleanup($where_statement,'operators').')' ;
        
        
        // Metadata updates can only be performed on account owned records. 
        // Set account_id in where clause
        $where_statement .= " AND (metadata.account_id='$this->account_id')" ; 
        
        
        unset($input['metadata_id'],$input['metadata_id_array']) ;
        
        $values_array = array() ; 
        
        if (count($input) > 0) {
            
            foreach ($input as $key => $value) {
                $values_array[$key] = $value ; 
                }
            }
        
        // Updates the contact in the database
        $query_array = array(
            'table' => 'metadata',
            'values' => $values_array,
            'where' => $where_statement
            );

        $result = $this->DB->Query('UPDATE',$query_array);
    
        return $result ;         
        }
        
        
        
    public function Create_App_View_History($view_input) {

        $view_input = (object) $view_input ; // Cast the input array as an object to make queries easier to write        
        
        $query_array = array(
            'table' => "system_app_view_history",
            'values' => array(
                'user_id' => $view_input->user_id,
                'profile_id' => $view_input->profile_id,
                'view_name' => $view_input->view_name,
                'view_parameters' => $view_input->view_parameters,
                'display_columns' => $view_input->display_columns,
                'timestamp_created' => array(
                    'value' => TIMESTAMP,
                    'statement' => "CASE WHEN timestamp_created = 0 THEN 'VALUE' ELSE timestamp_created END"
                    ),
                'timestamp_updated' => TIMESTAMP
                ),
            'where' => "system_app_view_history.profile_id='$view_input->profile_id' AND system_app_view_history.view_name='$view_input->view_name'"
            );
        
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;   
        $this->system_query_result = $result ; 
        
        return $result ;            
        }
        
        
    public function Create_Saved_App_View($view_input) {

        $view_input = (object) $view_input ; // Cast the input array as an object to make queries easier to write        
        
        $query_array = array(
            'table' => "system_app_saved_views",
            'values' => array(
                'profile_id' => $view_input->profile_id,
                'visibility_id' => $view_input->visibility_id,
                'view_title' => $view_input->view_title,
                'view_name' => $view_input->view_name,
                'view_parameters' => $view_input->view_parameters,
                'display_columns' => $view_input->display_columns,
                'timestamp_created' => TIMESTAMP,
                'timestamp_updated' => TIMESTAMP
                )
            );
        
        $result = $this->DB->Query('INSERT',$query_array) ;   
        $this->system_query_result = $result ; 
        
        return $result ;            
        }
        
        
    public function Update_Saved_App_View($input_array) {
        
        $values_array = array() ; 
        foreach ($input_array as $key => $value) {            
            $values_array[$key] = $value ;             
            }
        
        $view_saved_id = $values_array['view_saved_id'] ; 
        unset($values_array['view_saved_id']) ; 
        
        // Updates the auth role
        $query_array = array(
            'table' => 'system_app_saved_views',
            'values' => $values_array,
            'where' => "view_saved_id='$view_saved_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array); 
        
        $history_input = array(
            'function' => 'update_saved_app_view',
            'notes' => json_encode($values_array)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        }
        
        
    public function Create_Search($search_input) {

         

        $query_array = array(
            'table' => "system_search_history",
            'values' => array(
                'session_public_id' => $search_input['session_public_id'],
                'search_name' => $search_input['search_name'],
                'search_parameter' => $search_input['search_parameter'],
                'search_parameters' => $search_input['search_parameters'],
                'search_result' => $search_input['search_result'],
                'timestamp' => TIMESTAMP
                )
            );

        $result = $this->DB->Query('INSERT',$query_array) ;   
        $this->system_query_result = $result ; 
        
        return $result ;            

        }
        
        
    public function Create_User_History($user_id,$input) {
        
        global $data_browser ; 
        
        $values_array = array() ;
        
        $values_array['user_id'] = $user_id ;        
        $values_array['master_user_id'] = $_SESSION['master_user_id'] ; 
        $values_array['timestamp_created'] = TIMESTAMP ; 
         
        foreach ($input as $key => $value) {            
            $values_array[$key] = $value ;             
            }
        
        $query_array = array(
            'table' => 'system_log_app_history',
            'values' => $values_array
            );
        
        $query_array['values']['notes'] = json_decode($query_array['values']['notes'],1) ; 
        $query_array['values']['notes']['browser'] = $data_browser ;
        
        $elapsed_calc = microtime(true) - MICRO_TIMESTAMP ; 
        
        $query_array['values']['notes']['elapsed_time'] = number_format($elapsed_calc,2) ;
        $query_array['values']['notes'] = json_encode($query_array['values']['notes']) ; 
        
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->system_query_result = $result ;
        return $result ;         
        }
        
        
        
    public function Retrieve_App_View_History($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => 'system_app_view_history',
            'fields' => "system_app_view_history.*",
            'where' => "system_app_view_history.view_history_id>'0' "
            );

        if (isset($query_options->view_history_id)) {
            $query_array['where'] .= "AND system_app_view_history.view_history_id='$query_options->view_history_id' " ; 
            }
        
        if (isset($query_options->user_id)) {
            $query_array['where'] .= "AND system_app_view_history.user_id='$query_options->user_id' " ; 
            }
        
        if (isset($query_options->profile_id)) {
            $query_array['where'] .= "AND system_app_view_history.profile_id='$query_options->profile_id' " ; 
            }
        
        if (isset($query_options->view_name)) {
            $query_array['where'] .= "AND system_app_view_history.view_name='$query_options->view_name' " ; 
            }
        
        // Timestamp of the date the history was updated before
        if (isset($query_options->updated_before)) {
            $query_array['where'] .= "AND system_app_view_history.timestamp_updated<='$query_options->updated_before' " ;            
            }
        
        // Timestamp of the date the history was updated after
        if (isset($query_options->updated_after)) {
            $query_array['where'] .= "AND system_app_view_history.timestamp_updated>='$query_options->updated_after' " ;            
            }
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        $result = $this->DB->Query('SELECT',$query_array,'force');
        
        $this->system_query_result = $result ;
        
        return $result ;
        
        }
        

    public function Retrieve_App_View_Saved($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => 'system_app_saved_views',
            'fields' => "system_app_saved_views.*",
            'where' => "system_app_saved_views.view_saved_id>'0' "
            );

        if (isset($query_options->view_saved_id)) {
            $query_array['where'] .= "AND system_app_saved_views.view_saved_id='$query_options->view_saved_id' " ; 
            }
        
        if (isset($query_options->view_saved_name)) {
            $query_array['where'] .= "AND system_app_saved_views.view_saved_name='$query_options->view_saved_name' " ; 
            }
        
        if (isset($query_options->profile_id)) {
            $query_array['where'] .= "AND system_app_saved_views.profile_id='$query_options->profile_id' " ; 
            }
        
            if (isset($query_options->global_view)) {
            $query_array['where'] .= "AND system_app_saved_views.global_view='$query_options->global_view' " ; 
            }
        
        if (isset($query_options->view_name)) {
            $query_array['where'] .= "AND system_app_saved_views.view_name='$query_options->view_name' " ; 
            }
        
        // Timestamp of the date the saved view was updated before
        if (isset($query_options->updated_before)) {
            $query_array['where'] .= "AND system_app_saved_views.timestamp_updated<='$query_options->updated_before' " ;            
            }
        
        // Timestamp of the date the saved view was updated after
        if (isset($query_options->updated_after)) {
            $query_array['where'] .= "AND system_app_saved_views.timestamp_updated>='$query_options->updated_after' " ;            
            }
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            } else {
                $query_array['order_by'] = "system_app_saved_views.view_title" ;  
                $query_array['order'] = "ASC" ;  
                }
        
        
        // PROCESS VISIBILITY FILTER        
        $visibility_array = $this->Retrieve_Visibility_ID_Array($query_options) ; 
        
        // Add visibility filter if there are visibility_id's to process
        $filter_by_visibility_id_string = '' ; 
        if (count($visibility_array) > 0) {

            foreach ($visibility_array as $visibility) {
                
                $this_visibility_id = $visibility['visibility_id'] ; 
                $filter_by_visibility_id_string .= "system_app_saved_views.visibility_id='$this_visibility_id' OR " ;               
                }
            
            $filter_by_visibility_id_string = rtrim($filter_by_visibility_id_string," OR ") ;
            $query_array['where'] .= "AND ($filter_by_visibility_id_string) " ;  
            }
        
        $result = $this->DB->Query('SELECT',$query_array,'force');
        
        $this->system_query_result = $result ;
        
        return $result ;
        
        }
        
        
    public function Retrieve_App_Page_List($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
         
        
        $query_array = array(
            'table' => 'app_pages',
            'fields' => "app_pages.*",
            'where' => "app_pages.app_page_id>'0' "
            );

        
        // Add filter by app_page_id
        if (isset($query_options->filter_by_app_page_id)) {
            $query_array['where'] .= " AND app_pages.app_page_id='$query_options->filter_by_app_page_id'" ;              
            }
        
        // Add filter by visibility id
        if (isset($query_options->visibility_id)) {
            $query_array['where'] .= " AND app_pages.visibility_id='$query_options->visibility_id'" ;              
            }
        
        
        // Add filter by search parameter
        if (isset($query_options->filter_by_search_parameter)) {
            $query_array['where'] .= " AND (app_pages.page_description LIKE '%$query_options->filter_by_search_parameter%' OR 
                app_pages.page_keywords LIKE '%$query_options->filter_by_search_parameter%' 
                )" ;              
            }  

        
        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        $result = $this->DB->Query('SELECT',$query_array,'force');

        
        // Get the total count of assets in this query
        if (isset($query_options->total_count)) {        
            $result['total_count'] = $query_options->total_count ; 
            
            } else {
                $query_array['fields'] = "COUNT(app_pages.page_id) as total_count, app_pages.page_id" ; 
                unset($query_array['limit'],$query_array['offset']) ; 
                $count = $this->DB->Query('SELECT',$query_array,'force');

                $total_list = array() ; 
                foreach ($count['results'] as $item) {
                    $total_list[] = array(
                        'page_id' => $item['page_id']
                        ) ; 
                    }
                $result['total_count'] = count($count['results']) ;
                }
        
        
        $result['offset_page'] = $query_options->offset_page ; 
        
        $this->system_query_result = $result ;   
        
        $this->Set_System_Paging($result) ;        
        
        
        return $result ;
        
        }
        
        
    public function Retrieve_Metadata($metadata_id = 'internal') {
        
        if ('internal' === $metadata_id) {
            $metadata_id = $this->metadata_id ; 
            } 
        

        $query_array = array(
            'table' => 'metadata',
            'join_tables' => array(),
            'fields' => "metadata.*, metadata_type.*, system_visibility_levels.*",
            'where' => "metadata.metadata_id='$metadata_id'"
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            );        
        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_visibility_levels',
            'on' => 'system_visibility_levels.visibility_id',
            'match' => 'metadata.visibility_id'
            );
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array);
        
        $this->system_query_result = $result ;
        
        return $result ;               
        }
    
    
    public function Retrieve_Metadata_ID_By_Slug($metadata_type_name,$metadata_slug) {
        
         
        
        $query_array = array(
            'table' => 'metadata',
            'join_tables' => array(),
            'fields' => "metadata.metadata_id",
            'where' => "(metadata.account_id='$this->account_id' OR metadata.global_metadata='1') AND metadata.metadata_slug='$metadata_slug' AND metadata_type.metadata_type_name='$metadata_type_name'"
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'metadata_type',
            'on' => 'metadata_type.metadata_type_id',
            'match' => 'metadata.metadata_type_id'
            );
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array);
        
        $this->system_query_result = $result ;

        return $result ;       
        }
        
        
        
    public function Retrieve_Saved_Search($search_id,$session_public_id) {

        $query_array = array(
            'table' => 'system_search_history',
            'fields' => "system_search_history.*",
            'where' => "system_search_history.search_id='$search_id' AND system_search_history.session_public_id='$session_public_id'"
            );    
        
        $result = $this->DB->Query('SELECT',$query_array);
        return $result ; 
        }        
        

        
    public function Update_System_Auth_Role($input_array) {
        
        $values_array = array() ; 
        foreach ($input_array as $key => $value) {            
            $values_array[$key] = $value ;             
            }
        
        $auth_id = $values_array['auth_id'] ; 
        unset($values_array['auth_id']) ; 
        
        // Updates the auth role
        $query_array = array(
            'table' => 'system_auth_roles',
            'values' => $values_array,
            'where' => "auth_id='$auth_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array); 
        
        $history_input = array(
            'function' => 'update_system_auth_role',
            'notes' => json_encode($values_array)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        }    
        
      
        
    public function Create_System_Auth_Permission($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_auth_permissions',
            'values' => $query_options
            ) ; 
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->system_query_result = $result ; 
        
        $history_input = array(
            'function' => 'create_system_auth_permission',
            'notes' => json_encode($query_options)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        }
        
        
    public function Create_System_Auth_Operation($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_auth_operations',
            'values' => $query_options
            ) ; 
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->system_query_result = $result ; 
        
        $history_input = array(
            'function' => 'create_system_auth_operation',
            'notes' => json_encode($query_options)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        }
        
        
    public function Update_System_Auth_Permission($input_array) {
        
        $values_array = array() ; 
        foreach ($input_array as $key => $value) {            
            $values_array[$key] = $value ;             
            }
        
        $permission_id = $values_array['permission_id'] ; 
        unset($values_array['permission_id']) ; 
        
        // Updates the auth role
        $query_array = array(
            'table' => 'system_auth_permissions',
            'values' => $values_array,
            'where' => "permission_id='$permission_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array); 
        
        $history_input = array(
            'function' => 'update_system_auth_permission',
            'notes' => json_encode($values_array)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        }
        
       
        
    public function Update_System_Auth_Operation($input_array) {
        
        $values_array = array() ; 
        foreach ($input_array as $key => $value) {            
            $values_array[$key] = $value ;             
            }
        
        $operation_id = $values_array['operation_id'] ; 
        unset($values_array['operation_id']) ; 
        
        // Updates the auth role
        $query_array = array(
            'table' => 'system_auth_operations',
            'values' => $values_array,
            'where' => "operation_id='$operation_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array); 
        
        $history_input = array(
            'function' => 'update_system_auth_operation',
            'notes' => json_encode($values_array)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ;         
        }
        
        
    // Differentiate between user_auth_role's and account_auth_roles
    public function Retrieve_System_Auth_List($query_options = array()) {
                
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_auth_roles',
            'fields' => "system_auth_roles.*",
            'order_by' => "system_auth_roles.auth_role_order",
            'order' => "ASC"
            );    
        
        switch ($query_options->role_auth_type) {
            case 'user':
                $query_array['where'] = "system_auth_roles.user_role_auth='1'" ; 
                break ; 
            case 'account':        
                $query_array['where'] = "system_auth_roles.account_role_auth='1'" ; 
                break ;
            case 'all':
            default:
                $query_array['where'] = "system_auth_roles.auth_id>'0'" ; 
            }
        
        if (isset($query_options->auth_id)) {
            $query_array['where'] .= " AND system_auth_roles.auth_id='$query_options->auth_id'" ;
            }
        if (isset($query_options->auth_role_name)) {
            $query_array['where'] .= " AND system_auth_roles.auth_role_name='$query_options->auth_role_name'" ;
            }        
        
        $result = $this->DB->Query('SELECT',$query_array,'force');
        return $result ;        
        }
        
        
        
    public function Retrieve_System_Permissions_List($query_options = array()) {
                
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_auth_permissions',
            'fields' => "system_auth_permissions.*",
            'order_by' => "system_auth_permissions.permission_id",
            'order' => "ASC",
            'where' => "system_auth_permissions.permission_id>0 "
            );    
        
        
        
        if (isset($query_options->permission_id)) {
            $query_array['where'] .= " AND system_auth_permissions.permission_id='$query_options->permission_id'" ;
            }
        if (isset($query_options->permission_name)) {
            $query_array['where'] .= " AND system_auth_permissions.permission_name='$query_options->permission_name'" ;
            }        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        

        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 
        $query_options = $paging_constraints['query_options'] ;
        $query_array = $paging_constraints['query_array'] ; 
        
        
        // Pull queried results
        $result = $this->DB->Query('SELECT',$query_array,'force');
    
        
        // Pull total results
        $query_options->value_name = 'permission_id' ; 
        $query_array['fields'] = "system_auth_permissions.$query_options->value_name" ; 
        $total = $this->Retrieve_Total_Results($result,$query_array,$query_options) ; 
        
        $this->Set_System_Paging($total) ;         
        $this->system_query_result = $total ; 
        
        return $result ;        
        }
        
        
        
    public function Retrieve_System_Operations_List($query_options = array()) {
                
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
        
        $query_array = array(
            'table' => 'system_auth_operations',
            'fields' => "system_auth_operations.*, 
                system_auth_permissions.permission_name, system_auth_permissions.permission_title, system_auth_permissions.permission_description",
            'order_by' => "system_auth_operations.operation_id",
            'order' => "ASC",
            'where' => "system_auth_operations.operation_id>0"
            );    
        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_auth_permissions',
            'on' => 'system_auth_permissions.permission_id',
            'match' => 'system_auth_operations.permission_id',
            'type' => 'left'
            );
        
        
        if (isset($query_options->operation_id)) {
            $query_array['where'] .= " AND system_auth_operations.operation_id='$query_options->operation_id'" ;
            }
        if (isset($query_options->permission_name)) {
            $query_array['where'] .= " AND system_auth_operations.operation_name='$query_options->operation_name'" ;
            }        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            }
        
        
        if (isset($query_options->filter_by_search_parameter)) {
            $query_array['where'] .= " AND (system_auth_operations.operation_name LIKE '%$query_options->filter_by_search_parameter%' OR 
                system_auth_operations.operation_title LIKE '%$query_options->filter_by_search_parameter%') "  ; 
                }
        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->system_query_result = $result ; 
        return $result ;        
        }
        
        
    public function Retrieve_System_Action_Key($action_key_id) {
        
         
        
        $query_array = array(
            'table' => 'system_action_keys',
            'join_tables' => array(),
            'fields' => "system_action_keys.*, 
                system_actions.action_title, system_actions.target, system_actions.key_name, 
                system_actions.expiration_seconds, system_actions.map, system_actions.url_destination",
            'where' => "system_action_keys.action_key_id='$action_key_id'"
            );    
        
        $query_array['join_tables'][] = array(
            'table' => 'system_actions',
            'on' => 'system_actions.action_id',
            'match' => 'system_action_keys.action_id'
            );
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array);
        
        // Convert json encoded string into array of additional action data            
        $action_map = json_decode($result['results']['map'],true) ; 
        if (is_array($action_map)) {
            foreach ($action_map as $key => $value) {
                $result['results'][$value] = $result['results'][$key] ;
                unset($result['results'][$key]) ; 
                }
            }        
        
        
        $result['results'][$result['results']['target']] = $result['results']['target_id'] ; 
        
        $structured_data_01 = json_decode($result['results']['structured_data_01'],true) ; 
        $result['results']['structured_data_01'] = $structured_data_01 ; 
        
        if ($result['results']['key_name']) {
            $result['results'][$result['results']['key_name']] = $result['results']['key_id'] ;
            }
        
        return $result ;
        
        }
        
        
           
    public function Retrieve_System_Billing_Status_Levels($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        $query_array = array(
            'table' => 'system_billing_status_levels',
            'fields' => "system_billing_status_levels.*",
            'order_by' => 'system_billing_status_levels.billing_status_order',
            'order' => 'DESC'
            );    
        


        // Set order parameters
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = "$query_options->order_by" ;  
            $query_array['order'] = $query_options->order ;  
            } 
        
        

        if (isset($query_options->filter_by_search_parameter)) {
//            switch ($list_name) {
//                case 'list_protected_values':
//                    $query_array['where'] .= "AND ($list_name.value_name='$query_options->filter_by_search_parameter') " ; 
//                    break ;
//                }
            }
        

        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        $result = $this->DB->Query('SELECT',$query_array,'force');

        
        // Get the total count of assets in this query
        if (isset($query_options->total_count)) {        
            $result['total_count'] = $query_options->total_count ; 
            
            } else {
                $query_array['fields'] = "system_billing_status_levels.*" ; 
                unset($query_array['limit'],$query_array['offset']) ; 
                $count = $this->DB->Query('SELECT',$query_array,'force');

                $result['total_count'] = $count['result_count'] ;
                }
        
        
        $result['offset_page'] = $query_options->offset_page ; 
        
        $this->system_query_result = $result ;   
        
        $this->Set_System_Paging($result) ;        
        
        return $result ;        
        }
        
        
    public function Retrieve_System_List($list_name,$query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        $query_array = array(
            'table' => $list_name,
            'fields' => "$list_name.*"
            );    
        
        
        if (isset($query_options->value_type)) {
            $query_array['where'] .= "AND $list_name.value_type='$query_options->value_type' " ;
            }
        
        if (isset($query_options->country_id)) {
            $query_array['where'] .= "AND $list_name.country_id='$query_options->country_id' " ;
            }
        
        if (isset($query_options->country_supported)) {
            $query_array['where'] .= "AND $list_name.country_supported='$query_options->country_supported' " ;
            }
        
        if (isset($query_options->country_priority)) {
            $query_array['where'] .= "AND $list_name.country_priority='$query_options->country_priority' " ;
            }        
        
        if (isset($query_options->country_dialing_code)) {
            $query_array['where'] .= "AND $list_name.country_dialing_code='$query_options->country_dialing_code' " ;
            }
        
        if (isset($query_options->country_phone_only)) {
            $query_array['where'] .= "AND $list_name.phone_only='$query_options->country_phone_only' " ;
            }
        
        // list_defaults
        if (isset($query_options->defaults_id)) {
            $query_array['where'] .= "AND $list_name.defaults_id='$query_options->defaults_id' " ;
            } 
        
        if (isset($query_options->defaults_name)) {
            $query_array['where'] .= "AND $list_name.defaults_name='$query_options->defaults_name' " ;
            }
        
        
        // app_email_domains
        if (isset($query_options->account_default_domain)) {
            $query_array['where'] .= "AND $list_name.account_default_domain='$query_options->account_default_domain' " ;
            } 
        
        if (isset($query_options->app_default_domain)) {
            $query_array['where'] .= "AND $list_name.app_default_domain='$query_options->app_default_domain' " ;
            }
        
        
        // list_domains
        if (isset($query_options->domain_id)) {
            $query_array['where'] .= "AND $list_name.domain_id='$query_options->domain_id' " ;
            } 
        
        if (isset($query_options->filter_by_account_id)) {
            
            $query_options->filter_by_account_id = Utilities::Process_Comma_Separated_String($query_options->filter_by_account_id) ;

            foreach ($query_options->filter_by_account_id as $account_id) {
                $account_id_query_string .= "account_id='$account_id' OR " ; 
                }

            $account_id_query_string = rtrim($account_id_query_string," OR ") ;            
            
            $query_array['where'] .= "AND ($account_id_query_string) " ;
            }
        
        if (isset($query_options->hostname)) {
            $query_array['where'] .= "AND $list_name.hostname='$query_options->hostname' " ;
            }
        
        if (isset($query_options->app_domain)) {
            $query_array['where'] .= "AND $list_name.app_domain='$query_options->app_domain' " ;
            }
        
        if (isset($query_options->account_public_domain)) {
            $query_array['where'] .= "AND $list_name.account_public_domain='$query_options->account_public_domain' " ;
            } 
        
        if (isset($query_options->url_live)) {
            $query_array['where'] .= "AND $list_name.url_live='$query_options->url_live' " ;
            }         
        
        if (isset($query_options->default_domain)) {
            $query_array['where'] .= "AND $list_name.default_domain='$query_options->default_domain' " ;
            }        
        
        
        
        // list_protected_values
        if (isset($query_options->value_name)) {
            $query_array['where'] .= "AND $list_name.value_name='$query_options->value_name' " ;
            }
        
        if (isset($query_options->value_type)) {
            $query_array['where'] .= "AND $list_name.value_type='$query_options->value_type' " ;
            }
        
        
        if (isset($query_options->filter_by_search_parameter)) {
            switch ($list_name) {
                case 'list_protected_values':
                    $query_array['where'] .= "AND ($list_name.value_name='$query_options->filter_by_search_parameter') " ; 
                    break ;
                }
            }
        
        // list_contact_fields
        if (isset($query_options->active)) {
            $query_array['where'] .= "AND $list_name.active='$query_options->active' " ;
            }
        

        // Remove a prefix AND from the where clause
        if (isset($query_array['where'])) {
            $query_array['where'] = ltrim($query_array['where'],"AND ");
            }


        // Add sort filter 
        if (isset($query_options->order_by)) {
            $query_array['order'] = $query_options->order ;            
            $query_array['order_by'] = $query_options->order_by ;           
            }
        
        
        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        $result = $this->DB->Query('SELECT',$query_array,'force');

        
        // Get the total count of assets in this query
        if (isset($query_options->total_count)) {        
            $result['total_count'] = $query_options->total_count ; 
            
            } else {
                $query_array['fields'] = "$list_name.*" ; 
                unset($query_array['limit'],$query_array['offset']) ; 
                $count = $this->DB->Query('SELECT',$query_array,'force');

                $result['total_count'] = $count['result_count'] ;
                }
        
        
        $result['offset_page'] = $query_options->offset_page ; 
        
        $this->system_query_result = $result ;   
        
        $this->Set_System_Paging($result) ;        
        
        return $result ;
        
        }
        
        
    public function Retrieve_System_Versions($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write


        $query_array = array(
            'table' => 'system_version_control',
            'fields' => "system_version_control.*"
            );    
        
        switch ($query_options->stable) {
            case 'both':
                $query_array['where'] .= " AND system_version_control.stable>=0 " ; 
                break ;                
            case 'no':
                $query_array['where'] .= " AND system_version_control.stable=0 " ; 
                break ;
            case 'yes':
            default:
                $query_array['where'] .= " AND system_version_control.stable=1 " ; 
            }
        
        switch ($query_options->supported) {
            case 'both':
                $query_array['where'] .= " AND system_version_control.supported>=0 " ; 
                break ;                
            case 'no':
                $query_array['where'] .= " AND system_version_control.supported=0 " ; 
                break ;
            case 'yes':
            default:
                $query_array['where'] .= " AND system_version_control.supported=1 " ; 
            }
        
    
        
        
        if (isset($query_options->filter_by_search_parameter)) {
            $query_array['where'] .= " AND (system_version_control.version_title LIKE '%$query_options->filter_by_search_parameter%' OR 
                system_version_control.release_notes LIKE '%$query_options->filter_by_search_parameter%' OR 
                system_version_control.admin_notes LIKE '%$query_options->filter_by_search_parameter%' 
                )" ;
            }
            

        $query_array['where'] = ltrim($query_array['where']," AND ")  ;
        
        
        // Add sort filter
        if (isset($query_options->order_by)) {
            $query_array['order_by'] = $query_options->order_by ;  
            $query_array['order'] = $query_options->order ;  
            } 
        
        

        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        
        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        if (($query_array['order_by_relevance'] == 'yes') AND (isset($query_options->filter_by_search_parameter))) {
            $relevance_options = array(
                'fields' => array('metadata_name','metadata_description') 
                ) ; 
            $result = $this->Sort_Search_Relevance($result,$query_array,$query_options,$relevance_options) ;  
            } 
        
        $query_options->value_name = 'version_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ;
        $this->system_query_result = $result ; 
 
        $this->Set_System_Paging($result) ; 
        
        return $result ;
        }        
        
        
        
    public function Update_System_Action_Key($input) {

         
        
        $values_array = array() ; 
        foreach ($input as $key => $value) {
            
            $values_array[$key] = $value ; 
            
            }
            
        $query_array = array(
            'table' => 'system_action_keys',
            'values' => $values_array,
            'where' => "action_key_id='$this->action_key_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array) ;
        $this->system_query_result = $result ;
        return $result ;        
        
        }
       
        
    public function Retrieve_Asset_Type_ID_Array($query_options) {
        
        if (is_array($query_options)) {
            $query_options = (object) $query_options  ;
            }
        
        // PROCESS ASSET TYPE FILTER
        $type_id_array = array() ; 
         
        // Submitted as an array of type_id values.
        if (isset($query_options->filter_by_type_id)) {
            // If an array wasn't submitted, turn it into an array
            $type_id_array = Utilities::Array_Force(Utilities::Process_Comma_Separated_String($query_options->filter_by_type_id)) ;             
            }
                
        // Submitted as an array of type_name's which is converted into, and added to the type_id array
        if (isset($query_options->filter_by_type_name)) {

            $type_name_array = Utilities::Array_Force(Utilities::Process_Comma_Separated_String($query_options->filter_by_type_name)) ; 

            $query_type_array = array(
                'table' => 'asset_type',
                'fields' => "*",
                'where' => ""
                );
            
            
            foreach ($type_name_array as $type_name) {
                switch ($type_name) {
                    case 'broadcast_all': // We should provide special treatment for hiddden since it's admin only...
                        $query_type_array['where'] .= "type_name='email' OR " ; 
                        $query_type_array['where'] .= "type_name='sms' OR " ; 
                    default:
                        $query_type_array['where'] .= "type_name='$type_name' OR " ; 
                    }
                }
            $query_type_array['where'] = rtrim($query_type_array['where']," OR ") ;
            $type_result = $this->DB->Query('SELECT',$query_type_array,'force');            
  
            
            foreach ($type_result['results'] as $type) {
                $type_id_array[] = $type['type_id'] ; 
                }
            }         
        
        return $type_id_array ;
        }
        
        
    public function Retrieve_Visibility_ID_Array($query_options,$single = 'no') {
        
        if (is_array($query_options)) {
            $query_options = (object) $query_options  ;
            }
        
        
        // PROCESS ASSET VISIBILITY FILTER
        $visibility_array = array() ; 
        
        // Submitted as an array of type_id values.
        if (isset($query_options->filter_by_visibility_id)) {
            // If an array wasn't submitted, turn it into an array
            $visibility_id_array = Utilities::Array_Force(Utilities::Process_Comma_Separated_String($query_options->filter_by_visibility_id)) ; 
            
            $query_visibility_array = array(
                'table' => 'system_visibility_levels',
                'fields' => "*",
                'where' => ""
                ) ;

            foreach ($visibility_id_array as $visibility_id) {
                $query_visibility_array['where'] .= "system_visibility_levels.visibility_id='$visibility_id' OR " ;
                }
            
            $query_visibility_array['where'] = rtrim($query_visibility_array['where']," OR ") ;
            $visibility_id_result = $this->DB->Query('SELECT',$query_visibility_array,'force');             
            
            foreach ($visibility_id_result['results'] as $visibility) {
                $visibility_array[] = $visibility ; 
                }            
            }
        

        // Submitted as an array of visibility_names's which is converted into, and added to the visibility_name array
        if (isset($query_options->filter_by_visibility_name)) {

            
            // If an array wasn't submitted, turn it into an array
            $visibility_name_array = Utilities::Array_Force(Utilities::Process_Comma_Separated_String($query_options->filter_by_visibility_name)) ; 
            
            $query_visibility_array = array(
                'table' => 'system_visibility_levels',
                'fields' => "*",
                'where' => ""
                ) ;

            foreach ($visibility_name_array as $visibility_name) {
                switch ($visibility_name) {
                    case 'admin_all': // We should provide special treatment for hiddden since it's admin only...
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_hidden' OR " ;  
                    case 'all':
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_deleted' OR " ; 
                    case 'visible':    
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_draft' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_shared' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_published' OR " ; 
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_review' OR " ; 
                        break ;
                    case 'visibility_scheduled': // used for assets   
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='visibility_published' OR " ; 
                        break ;                        
                    default:
                        $query_visibility_array['where'] .= "system_visibility_levels.visibility_name='$visibility_name' OR " ; 
                    }
                }
            $query_visibility_array['where'] = rtrim($query_visibility_array['where']," OR ") ;
            $visibility_result = $this->DB->Query('SELECT',$query_visibility_array,'force');               
                
            
            foreach ($visibility_result['results'] as $visibility) {
                $visibility_array[] = $visibility ; 
                }
            }        
        
        if ('single' === $single) {
            $visibility_array = $visibility_array[0] ; 
            }
        
        return $visibility_array ;
        } 
      
        
        
        
    }