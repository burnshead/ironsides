<?php

namespace Ragnar\Ironsides ;

class User extends System {
    
     
    
    public $master_user_id;
    public $master_user;
    
    public $admin_user_id;
    public $admin_user;
    
    public $user_id;
    public $user;
    
    public $email_address_id; 
    public $user_email ;
    public $user_email_list ;
    
    
    public $phone_number_id;    
    public $user_phone_number ; 
    public $user_phone_number_list ; 
    
    public $username;
    public $active_account_id;
    private $password;

    public $user_create_blacklist = array(
        'email_domain' => array(
            'secureservices.33mail',
            'anonaddy.com',
            'dmarkholdings.c'
            ),
        'card_country' => array(
            'NG'
            )
        ) ; 
    
    public $authorization ;     
    
    public $user_paging ; 
    public $page_increment = 12 ; // # of account items to be pulled per page (e.g. Users)
    
    public $user_query_result ;
    
    
    
    public function __construct($user_id = 'ignore') {
        
        global $DB ;  
        $this->DB = $DB ;
        
        if ('ignore' !== $user_id) {            
            $this->Set_Admin_User_By_ID($user_id) ;   
            $this->Set_User_By_ID($user_id) ; 
            $this->Set_Master_User_By_ID($user_id) ; 
            }    

        }
    
    
    //////////////////////
    //                  //
    // SETTERS          //
    //                  //
    //////////////////////
    
    // WE WANT TO GET RID OF THIS
    // PERMISSIONS RBAC HAS TAKEN ITS PLACE
    public function Set_Authorization($authorization) {
        
        $this->authorization = $authorization ;
        return $this;
        } 
    

    public function Set_Page_Increment($page_increment) {
        $this->page_increment = $page_increment ; 
        return $this ; 
        }
    
    public function Set_User_By_Phone_Number($query_parameters) {
        
        $this->Set_User_Phone_Number($query_parameters) ; 
        $data['user_phone_number'] = $this->Get_User_Phone_Number() ; 
        
        $user_query_options = array(
            'filter_by_account_id' => 'no',
            'filter_by_user_phone_number' => 'yes',
            'phone_country_dialing_code' => $data['user_phone_number']['phone_country_dialing_code'],
            'phone_number' => $data['user_phone_number']['local']
            ) ; 

        $this->Set_Account_User_List($user_query_options) ; 
        $data['account_user_list'] = $this->Get_Account_User_List() ; 

        if (count($data['account_user_list']) > 0) {
            $this->Set_User_By_ID($data['account_user_list'][0]['user_id']) ; 
            } else {
                $this->user = 'error' ; 
                }
        
        
        return $this ; 
        }
    
    
    
    public function Set_User_Phone_Number($query_parameters) {
        
        if (isset($query_parameters['phone_number'])) {           
            
            if (isset($query_parameters['phone_country_dialing_code'])) {
                $phone_number_options['phone_country_dialing_code'] = $query_parameters['phone_country_dialing_code'] ; 
                }
            if (isset($query_parameters['country_id'])) {
                $phone_number_options['country_id'] = $query_parameters['country_id'] ; 
                }            
            
            $this->Action_Validate_Phone_Number($query_parameters['phone_number'],$phone_number_options) ; 
            $phone_number_formatted = $this->Get_Phone_Number_Validation() ; 
            
            $this->user_phone_number = $phone_number_formatted ; 
            } else {
                $this->user_phone_number = 'error' ; 
                }

        return $this ; 
        }
    
    public function Get_User_Phone_Number() {
        
        return $this->user_phone_number ;
        }
    
    
    
    public function Set_Master_User_By_ID($user_id) {
        
        if ($user_id != MASTER_USER_ID) {
            $master_user = new User() ; 
            $master_user->Set_User_By_ID(MASTER_USER_ID) ; 
            $this->master_user_id = MASTER_USER_ID ; 
            $this->master_user = $master_user->Get_User() ; 
            } else {
                $this->master_user_id = $user_id ; 
                $this->master_user = $this->Get_User() ; 
                }
        }
    
    
    public function Set_User_By_Email($email_address) {
        
         
        
        $email_address = Utilities::Escape($email_address) ; 
        
        $query_array = array(
            'table' => 'user_emails',
            'fields' => "user_id, email_address_id",
            'where' => "email_address='$email_address'"
            );
        
        $result = $this->DB->Query('SELECT',$query_array);
        
        if ($result['results']['user_id']) {
            $this->user_id = $result['results']['user_id'];
            $this->email_address_id = $result['results']['email_address_id'];
            } else {
                $this->user_id = '' ; 
                $this->alert_id = 1 ; // No email found
                $this->user_query_result = $result ; 
                }
        
        return $this; 
        
        }

    
    public function Set_User_ID($user_id) {
        
        $this->user_id = $user_id;
        return $this;
        }
    
    public function Set_Admin_User_By_ID($user_id) {
        $this->admin_user_id = $user_id;
        $this->Set_Admin_User() ; 
        return $this;
        }
    
    public function Set_User_By_ID($user_id = 'internal') {
        
        if ('internal' === $user_id) {
            $user_id = $this->user_id ; 
            } else {
                $this->user_id = $user_id ; 
                }
        
        $this->Set_User($this->user_id) ;         
        return $this;
        }
    
    
    public function Set_Email_Address_ID($email_address_id) {
        
        $this->email_address_id = $email_address_id;
        
        return $this;
        }
    
    public function Set_Phone_Number_ID($phone_number_id) {
        
        $this->phone_number_id = $phone_number_id;
        return $this;
        }
    
    public function Set_User_Email_By_Email_Address_ID($email_address_id) {
        
        $this->email_address_id = $email_address_id;
        $user_email = $this->Retrieve_User_Email_By_ID() ; 
        $this->user_email = $user_email['results'] ; 
        
        return $this;
        }
    
    
    public function Set_Admin_User($user_id = 'internal') {
        
        if ('internal' === $user_id) {
            $user_id = $this->admin_user_id ; 
            } else {
                $this->admin_user_id = $user_id ; 
                }
        
        $admin_user = new User() ; 
        $admin_user->Set_User_By_ID($this->admin_user_id) ; 
        $admin_user_record = $admin_user->Get_User() ; 
        $this->admin_user = $admin_user_record ;
        return $this;
        
        }
    
    public function Set_User($user_id = 'internal') {
        
        $this->Set_Model_Timings('User.Set_User_initialize') ;
        
        if ('internal' === $user_id) {
            $user_id = $this->user_id ; 
            } else {
                $this->user_id = $user_id ; 
                }
        
        $result = $this->Retrieve_User($this->user_id) ;
        $this->Set_Model_Timings('User.Set_User_user_record_retrieved') ;
        
        if ($result['result_count'] == 1) {
            
            $user_record = $result['results'] ; 
            
            // Territorialize timestamps and phone numbers...
            $user_record['timestamp_current'] = time() ; 
            $user_record = $this->Action_Time_Territorialize_Dataset($user_record) ;            
            $user_record = $this->Action_Phone_Territorialize_Dataset($user_record) ; 
                      
      
            if ($user_record['registration_token']) {
                $user_record['registration_link'] = '/register?user_id='.$user_record['user_id'].'&registration_token='.$user_record['registration_token'] ; 
                }
            
            $metadata_query = array(
                'relationship_type' => 'user',
                'metadata_type' => 'tag,category',
                'filter_by_visibility_name' => 'visible',
                'allow_global_metadata' => 'yes'
                ) ; 
            
            $user_record['tag'] = array() ;
            $user_record['category'] = array() ;
            
            $metadata_results = $this->Retrieve_Metadata_Relationship_List($this->user_id,$metadata_query) ; 
//            $user_record['metadata_query'] = $metadata_results ; 
            if ($metadata_results['result_count'] > 0) {
                
                foreach ($metadata_results['results'] as $metadata_result) {
                    switch ($metadata_result['metadata_type']){
                        case 'tag':
                            $user_record['tag'][] = $metadata_result ; 
                            break ;
                        case 'category':
                            $user_record['category'][] = $metadata_result ; 
                            break ;                            
                        }
                    }
                }
            
            
            if ($user_record['referring_profile_id']) {
                
                $referrer = new Account() ; 
                $referrer->Set_Profile_By_ID($user_record['referring_profile_id']) ; 
                $user_record['referrer_record'] = $referrer->Get_Profile() ; 
                $this->Set_Model_Timings('User.Set_User_referrer_set') ;
                }
            
            
            
            $this->user = $user_record ;
            $this->active_account_id = $user_record['active_account_id'];
            $this->Set_User_Timezone() ; 
            $this->Action_Set_Auth_Permissions() ;
            
            $this->Set_Alert_Response(118) ; // Success - User found.
            } else {
                $this->Set_Alert_Response(117) ; // Error - Could not find user.
                $this->Append_Alert_Response('none',array(
                    'admin_context' => json_encode($result),
                    'location' => __METHOD__
                    )) ;              
                }

        return $this;        
        }
    
    

    
    public function Set_User_Timezone() {
        
        $continue = 1 ; 

        if (isset($this->user['timezone'])) {
            $continue = 0 ;             
            }
        
        if (($continue == 1) AND ($this->user['country_id'])) {
            
            $country_query = array(
                'country_id' => $this->user['country_id']
                ) ; 
            $this->Set_System_List('list_countries',$country_query) ; 
            $country_list = $this->Get_System_List('list_countries') ; 
            $this->user['timezone'] = $country_list[0]['country_timezone'] ; 
            }
        
        if ($continue == 1) { 
            $country_query = array(
                'country_id' => 'US'
                ) ; 
            $this->Set_System_List('list_countries',$country_query) ; 
            $country_list = $this->Get_System_List('list_countries') ; 
            $this->user['country_id'] = $country_query['country_id'] ;
            $this->user['timezone'] = $country_list[0]['country_timezone'] ;            
            }
         
       
        
        // FUTURE, replace with Set_User_Time() ; 
        try {
            $new_time = new \DateTime() ; 
            $new_time->setTimestamp(TIMESTAMP)->setTimezone(new \DateTimeZone($this->user['timezone']));            
            $this->user['user_time'] = $new_time->format('M j, Y').' '.$new_time->format('g:i A T') ; 
            } catch(Exception $e) {

                }
        
        return $this;
        } 
    
 
    
    
    public function Set_User_Email_List($verified = 'both') {
        
        $result = $this->Retrieve_User_Email_List($verified) ; 
        $this->test_data = $result ; 
        if ($result['result_count'] > 0) {
            $this->user_email_list = $result['results'] ; 
            } else {
                $this->user_email_list = 'error' ; 
                }
        
        return $this;
        }    
    
    
    public function Set_User_Phone_Number_List($query_options = array()) {
        
        if (!isset($query_options['verified'])) {
            $query_options['verified'] = 'both' ; 
            }
        
        $result = $this->Retrieve_User_Phone_Number_List($query_options) ; 
        
        if ($result['result_count'] == 0) {
            $this->user_phone_number_list = array() ; 
            } else {
                $c = 0 ; 
                foreach ($result['results'] as $item) {
                    $result['results'][$c] = $this->Action_Phone_Territorialize_Dataset($item) ;
                    $c++ ; 
                    }
                $this->user_phone_number_list = $result['results'] ; 
                } 
        
        return $this;
        }
    
    
    // Process a set of user results and separate into paging components to use for site navigation
    public function Set_User_Paging($results_array) {

        if (!isset($this->user_paging)) {
            $this->user_paging = $this->Set_Default_Paging_Object() ; 
            }       
        
        $this->user_paging->page_increment = $this->Get_Page_Increment() ;
        
        $this->user_paging = $this->Action_Process_Paging($this->user_paging,$results_array) ; 
        
        return $this ; 
        }
    
    
    //////////////////////
    //                  //
    // GETTERS          //
    //                  //
    //////////////////////
    
    
    public function Get_Active_Account_ID() {
        
        return $this->active_account_id;
        
        }
    
    
    // Not used.  See Get_Auth_Permissions() in System model
//    public function Get_Authorization() {
//        
//        if (!$this->authorization['privilege']) {
//            $this->Action_Test_User_Authorization('error') ;             
//            }
//        
//        return $this->authorization ;
//        } 
    
    
    public function Get_Master_User() {
        return $this->master_user;
        }
    
    public function Get_Master_User_Auth_Level() {
        return $this->master_user['user_auth_id'];
        }    
    
    public function Get_Admin_User() {
        return $this->admin_user;
        }
    
    public function Get_User_ID() {
        return $this->user_id ;
        }
    
    public function Get_User() {
        return $this->user;
        }
       
    public function Get_User_Auth_Level() {
        return $this->user['user_auth_id'];
        } 
    
    public function Get_Email_Address_ID() {
        return $this->email_address_id ;
        }
    
    public function Get_User_Email_Address() {
        return $this->user_email ;
        } 
    
    public function Get_User_Email_List() {
        return $this->user_email_list ;
        }    
    
    public function Get_Phone_Number_ID() {
        
        return $this->phone_number_id;
        }
    
    public function Get_User_Phone_Number_List() {
        return $this->user_phone_number_list ;
        }
    
    public function Get_Password_Validation() {
        return $this->password_validation ;
        }
    
    public function Get_User_Paging() {
        return $this->user_paging ;        
        }
    
    public function Get_Page_Increment() {
        
        return $this->page_increment ; 
        }
    
    // Use this as a QA funciton to return a result from any variable within scope
    // Particularly helpful for "Retrive" methods that have assigned a result to the "query_result" temporarily
    public function Get_Query_Result($variable) {
        
        $result = $this->$variable ;
        return $result ;        
        }
    
    //////////////////////
    //                  //
    // ACTIONS          //
    //                  //
    //////////////////////     
    
    
    
    public function Set_User_Registration($user_id,$registration_token) {
        
        // Check existing account usernames
        $query_array = array(
            'table' => 'user_registration',
            'values' => '*',
            'where' => "user_registration.user_id='$user_id' AND user_registration.registration_token='$registration_token'"
            );

        $result = $this->DB->Query('SELECT',$query_array) ;          
        
        $user_registration = $result['results'] ;
        $user_registration['registration_data'] = json_decode($user_registration['registration_data'],1) ;
            
        $this->user_registration = $user_registration ;
        
        return $this ; 
        }
    
    public function Get_User_Registration() {
        
        return $this->user_registration ; 
        }
    
    
    
    // registration_token is a hash of user_id and timestamp_created of the user
    public function Action_Save_User_Registration($user_input) {
        
        $new_registration_data = array() ;
        
        $past_registration_result = $this->Retrieve_User_Registration() ; 

        if ($past_registration_result['result_count'] > 0) {
                        
            $past_registration_result['results']['registration_data'] = json_decode($past_registration_result['results']['registration_data'],1) ; 
            $new_registration_data = $past_registration_result['results']['registration_data'] ; 
            }
        

        foreach ($user_input as $key => $value) {
            
            $new_registration_data[$key] = $value ; 
            }
             
        
        $registration_array = array(
            'user_id' => $this->user_id,
            'registration_token' => $this->DB->Simple_Hash(array($this->user_id,$this->user['timestamp_created'])),
            'registration_data' => json_encode($new_registration_data)
            ) ; 
        
        $save_registration_result = $this->Create_User_Registration($registration_array) ;
        
        $this->Set_User_Registration($this->user_id,$registration_array['registration_token']) ;
        
        return $this ; 
        }
    
    
    public function Create_User_Registration($query_options) {
        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            $values_array[$key] = $value ;             
            }
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        
        $query_array = array(
            'table' => "user_registration",
            'values' => $values_array,
            'where' => "user_registration.user_id='$this->user_id'"
            );

        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        
        return $result ;
        }
        
        
    public function Retrieve_User_Registration() {
        
        $query_array = array(
            'table' => 'user_registration',
            'fields' => "user_registration.*",
            'where' => "user_registration.user_id='$this->user_id'"
            );                

        $result = $this->DB->Query('SELECT',$query_array);         

        return $result ; 
        }
    
    
    
    
    
    // $do_not_contact_status should be 1 or 0. Will default to 1, which means do not contact this entry
    public function Action_Update_User_Restriction($restriction_input,$do_not_contact_status = 1) {
        
        $continue = 1 ; 
        
        if (!$this->user_id) {            
            $continue = 0 ;             
            }
        
        if ((!isset($restriction_input['email_address'])) AND (!isset($restriction_input['phone_number_international']))) {            
            $continue = 0 ;             
            }
        
        
        if ($continue == 1) {                        
            $restriction_input['do_not_contact'] = $do_not_contact_status ; 
            $result = $this->Create_User_Restriction($restriction_input) ;                         
            }
        
        return $this ; 
        }
    
    
    // 1 = do_not_contact is active (not allowed to contact this destination); 0 = free to contact
    public function Create_User_Restriction($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
                
        $query_array = array(
            'table' => "user_restrictions",
            'values' => array(
                'user_id' => $this->user_id,
                'do_not_contact' => $query_options->do_not_contact
                ),
            'where' => "user_restrictions.user_id='$this->user_id'"
            ) ;

        if (isset($query_options->email_address)) {
            $query_array['values']['email_address'] = $query_options->email_address ;
            $query_array['where'] .= " AND user_restrictions.email_address='$query_options->email_address'" ;
            }
        if (isset($query_options->phone_number_international)) {
            $query_array['values']['phone_number_international'] = $query_options->phone_number_international ;
            $query_array['where'] .= " AND user_restrictions.phone_number_international='$query_options->phone_number_international'" ;
            }
                
        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array) ;
        $this->user_query_result = $result ; 
        
        
        if (!isset($result['error'])) {
            
            // Create History Entry
            $history_input = array(
                'account_id' => $this->account_id,
                'function' => 'update_user_restrictions',
                'notes' => json_encode($query_array['values'])
                ) ; 
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;        
            }
        
        return $result ;         
        }
    
    
    
    
    public function Action_Set_Auth_Permissions() {
        
        // Permissions will be pulled for the active user UNLESS the master_user_id is different than the user_id
        // If there is a difference, then the master_user_id will override the current user (assuming admin privileges)
        if (($this->user_id != $this->master_user_id) AND ($this->master_user_id > 0)) {
            $data['user_record'] = $this->Get_Master_User() ; 
            } else {
                $data['user_record'] = $this->Get_User() ; 
                }

        $permissions_json_string = $data['user_record']['auth_role_permissions'] ; 
        $permission_role = array(
            'auth_id' => $data['user_record']['auth_id'],
            'auth_role_name' => $data['user_record']['auth_role_name'],
            'auth_role_title' => $data['user_record']['auth_role_title']
            ) ; 
        
        // Will attempt to associate Profile permissions with the user, however, if the user 
        // privileges are of higher order (or profile permissions don't exist) then the user privileges take precedence
        if ((isset($this->profile)) AND ($this->profile != 'error')) {
            
            $data['profile_record'] = $this->Get_Profile() ;             
            if ($data['profile_record']['auth_role_order'] > $data['user_record']['auth_role_order']) {
                
                $permissions_json_string = $data['profile_record']['auth_role_permissions'] ; 
                $permission_role = array(
                    'auth_id' => $data['profile_record']['auth_id'],
                    'auth_role_name' => $data['profile_record']['auth_role_name'],
                    'auth_role_title' => $data['profile_record']['auth_role_title']
                    ) ;
                }            
            }        
        

        // Will look for an account and attempt to associate features with the account, 
        // however, will skip if an account doesn't exist
        if (isset($this->account)) {
            $data['account_record'] = $this->Get_Account() ; 
            } elseif (isset($data['profile_record']['account_id'])) {            
                $account = new Account() ; 
                $account->Set_Account_By_ID($data['profile_record']['account_id']) ; 
                $data['account_record'] = $account->Get_Account() ;            
                } else {
                    // Cannot find account 
                    }
        
        
        $permissions_array = json_decode($permissions_json_string,1) ; 
        $permissions_id_array = $permissions_array['permissions'] ; 

        $permission_results = $this->Retrieve_Permission_List($permissions_id_array) ;
        $permissions = array() ; 

        if (count($permission_results['result_count'] > 0)) {
            foreach ($permission_results['results'] as $operation) {

                $operation_set = array(
                    'operation_id' => $operation['operation_id'],
                    'operation_title' => $operation['operation_title'],
                    'operation_name' => $operation['operation_name'],
                    'operation_description' => $operation['operation_description']
                    ) ;
                $permission_set = array(
                    'permission_id' => $operation['permission_id'],
                    'permission_name' => $operation['permission_name'],
                    'permission_title' => $operation['permission_title'],                    
                    'permission_description' => $operation['permission_description'],
                    'auth_role' => $permission_role
                    ) ;
                $feature_set = array(
                    'feature_id' => $operation['feature_id'],
                    'feature_title' => $operation['feature_title'],
                    'feature_name' => $operation['feature_name'],
                    'feature_description' => $operation['feature_description'],
                    'feature_set' => array(
                        'feature_set_id' => $operation['feature_set_id'],
                        'feature_set_name' => $operation['feature_set_name'],
                        'feature_set_title' => $operation['feature_set_title'],
                        'feature_set_description' => $operation['feature_set_description'],
                        'billing_status_order' =>  $operation['billing_status_order']
                        )
                    ) ;

                $operation_array = array(
                    'effect' => 'allow',
                    'operation' => $operation_set,
//                    'permission' => $permission_set,
                    'feature' => $feature_set
                    ) ; 

                $permissions[$operation['operation_name']] = $operation_array ; 

                }
            }
        

        
        // Scrub the allowed User-Profile operations to see if they are also allowed on the Account level
        // If the Account doesn't allow it, deny access. 
        // If a Profile wasn't pulled, then the User permissions are used instead
        foreach ($permissions as $user_operation) {      
             
            if (!isset($data['account_record']['operations_allowed'][$user_operation['operation']['operation_name']])) {
                $permissions[$user_operation['operation']['operation_name']]['effect'] = 'deny' ; 
                } elseif ((isset($data['account_record']['billing_status_order'])) AND ($data['account_record']['billing_status_order'] < $user_operation['feature']['feature_set']['billing_status_order']) AND ($data['account_record']['billing_status_order'] != null)) {
                    $permissions[$user_operation['operation']['operation_name']]['effect'] = 'deny' ; 
                    }

            }
        
        
        $this->auth_permissions = $permissions ; // Sets this as a System property outside of the User / Master User array so one set of permissions rules
        return $this ;     
        }
    
    
    
    // Shared asset test
    public function Action_Test_Asset_Public_View_Control() {
        
        // It's published
        // Or It's Shared and the account_id matched current account_id
        // Or it's shared and global and the organization_id matches current organization_id
        // Or it's shared and global and the organization_id is 0
        
        $effect = 'deny' ; 
        
        if (isset($this->profile['profile_id'])) {
            $data['profile_record'] = $this->Get_Profile() ; 
            }   
        
        $data['asset_record'] = $this->asset ; 
        
        if ($data['asset_record']['visibility_name'] == 'visibility_published') {
            $effect = 'allow' ; 
            
            } elseif (($data['asset_record']['visibility_name'] == 'visibility_shared') AND ($data['asset_record']['account_id'] == $data['profile_record']['account_id'])) {
                $effect = 'allow' ; 
            
            } elseif (($data['asset_record']['visibility_name'] == 'visibility_shared') 
                AND ($data['asset_record']['global_asset'] == 1) 
                AND ($data['asset_record']['organization_id'] == $data['profile_record']['organization_id'])) {
                    $effect = 'allow' ; 
            
            } elseif (($data['asset_record']['visibility_name'] == 'visibility_shared') 
                AND ($data['asset_record']['global_asset'] == 1)) {
                    $effect = 'allow' ; 
            
            } else {
                $effect = 'deny' ; 
                }
        
        return $effect ;
        }
    
    
    
    // Placing this at the User level so it can also be used to manage Contact permissions and visibility
    public function Action_Set_Asset_Auth_Permissions() {
        
        $data['asset_record'] = $this->Get_Asset() ;
        if (isset($this->profile['profile_id'])) {
            $data['profile_record'] = $this->Get_Profile() ; 
            }
        
        // Will attempt to associate Profile permissions with the user, however, if the user 
        // privileges are of higher order (or profile permissions don't exist) then the user privileges take precedence
        
        
        // Need a method for this on assets so super admin can override.
        // Permissions will be pulled for the active user UNLESS the master_user_id is different than the user_id
        // If there is a difference, then the master_user_id will override the current user (assuming admin privileges)        
        if ($this->Action_Test_Authorization_Control('admin_asset_edit') == 'allow') {

            // Use global app admin privileges
            $role_query_options['filter_by_asset_auth_role_name'] = 'asset_app_admin' ; 
            
            } elseif (isset($data['profile_record']['profile_id']) AND ($data['profile_record']['profile_id'] == $data['asset_record']['active_profile']['profile_id'])) {

            // Get profile specific permissions
            $role_query_options['filter_by_asset_auth_role_name'] = $data['asset_record']['active_profile']['asset_auth_role_name'] ;                 
          
            } elseif ($this->Action_Test_Asset_Public_View_Control() == 'allow') {
 
                // It's published
                // Or It's Shared and the account_id matched current account_id
                // Or it's shared and global and the organization_id matches current organization_id
                // Or it's shared and global and the organization_id is 0
            
                $role_query_options['filter_by_asset_auth_role_name'] = 'asset_viewer' ; 
            
                } else {
                    // Block all
                    $role_query_options['filter_by_asset_auth_role_name'] = 'asset_blocked' ;             
                    }    


            $role_list = $this->Retrieve_Asset_Auth_Role_List($role_query_options) ; 
            
            $permissions_json_string = $role_list['results'][0]['asset_auth_role_permissions'] ; 
            $permission_role = array(
                'asset_auth_id' => $role_list['results'][0]['asset_auth_id'],
                'asset_auth_role_name' => $role_list['results'][0]['asset_auth_role_name'],
                'asset_auth_role_title' => $role_list['results'][0]['asset_auth_role_title']
                ) ;        

        
        // Will look for an account and attempt to associate features with the account, 
        // however, will skip if an account doesn't exist
//        if (isset($this->account)) {
//            $data['account_record'] = $this->Get_Account() ; 
//            } elseif (isset($data['profile_record']['account_id'])) {            
//                $account = new Account() ; 
//                $account->Set_Account_By_ID($data['profile_record']['account_id']) ; 
//                $data['account_record'] = $account->Get_Account() ;            
//                } else {
//                    // Cannot find account 
//                    }
//        
//        
        $permissions_array = json_decode($permissions_json_string,1) ; 
        $permissions_id_array = $permissions_array['permissions'] ; 

        
        $permission_results = $this->Retrieve_Asset_Permission_List($permissions_id_array) ;
        $permissions = array() ;  
        
        if (count($permission_results['result_count'] > 0)) {
            foreach ($permission_results['results'] as $operation) {

                $operation_set = array(
                    'asset_operation_id' => $operation['asset_operation_id'],
                    'asset_operation_title' => $operation['asset_operation_title'],
                    'asset_operation_name' => $operation['asset_operation_name'],
                    'asset_operation_description' => $operation['asset_operation_description']
                    ) ;
                $permission_set = array(
                    'asset_permission_id' => $operation['asset_permission_id'],
                    'asset_permission_name' => $operation['asset_permission_name'],
                    'asset_permission_title' => $operation['asset_permission_title'],                    
                    'asset_permission_description' => $operation['asset_permission_description'],
                    'asset_auth_role' => $permission_role
                    ) ;
                $feature_set = array(
                    'asset_feature_id' => $operation['asset_feature_id'],
                    'asset_feature_title' => $operation['asset_feature_title'],
                    'asset_feature_name' => $operation['asset_feature_name'],
                    'asset_feature_description' => $operation['asset_feature_description'],
                    'asset_feature_set' => array(
                        'asset_feature_set_id' => $operation['asset_feature_set_id'],
                        'asset_feature_set_name' => $operation['asset_feature_set_name'],
                        'asset_feature_set_title' => $operation['asset_feature_set_title'],
                        'asset_feature_set_description' => $operation['asset_feature_set_description'],
                        )
                    ) ;

                $operation_array = array(
                    'effect' => 'allow',
                    'asset_operation' => $operation_set,
//                    'permission' => $permission_set,
//                    'feature' => $feature_set
                    ) ; 

                $permissions[$operation['asset_operation_name']] = $operation_array ; 

                }
            }

        
//        // Scrub the allowed User-Profile operations to see if they are also allowed on the Account level
//        // If the Account doesn't allow it, deny access. 
//        // If a Profile wasn't pulled, then the User permissions are used instead
//        foreach ($permissions as $user_operation) {      
//             
//            if (!isset($data['account_record']['operations_allowed'][$user_operation['operation']['operation_name']])) {
//                $permissions[$user_operation['operation']['operation_name']]['effect'] = 'deny' ; 
//                }            
//            }
        
        
        $this->asset_auth_permissions = $permissions ; // Sets this as a System property outside of the User / Master User array so one set of permissions rules
        return $this ;     
        }
    
    
    
    
// SHOULDN'T NEED AFTER RBAC STYLE VERIFICATIONS     
//    public function Action_Validate_Master_User_Authorization($test,$master_user) {
//        
//        $continue = 1 ; 
//        $result = array(
//            'pass' => 0,
//            'privilege' => 'none'
//            ) ; 
//        
//        $master_user_auth_level = $this->Get_Master_User_Auth_Level() ; 
//
//        if ($master_user_auth_level >= $master_user['view_level']) {
//            $continue = 0 ; 
//            $result['pass'] = 1 ; 
//            $result['privilege'] = 'view' ; 
//            }
//        if ($master_user_auth_level >= $master_user['edit_level']) {
//            $continue = 0 ; 
//            $result['pass'] = 1 ; 
//            $result['privilege'] = 'edit' ; 
//            }
//        if ($master_user_auth_level >= $master_user['owner_level']) {
//            $continue = 0 ; 
//            $result['pass'] = 1 ; 
//            $result['privilege'] = 'owner' ; 
//            }        
//
//        if ($result['pass'] == 0) {
//            $result['fail_reason'] = 'No master user privileges met.' ; 
//            }
//        
//        $result['master_authorization'] = $master_user_auth_level ; 
//        
//        return $result ; 
//        }
    
 
    
// SHOULDN'T NEED AFTER RBAC STYLE VERIFICATIONS    
//    public function Action_Test_User_Authorization($auth_name,$auth_input = array()) {
//
//        $validate_result['pass'] = 1 ; // Default pass
//        $validate_result['privilege'] = 'none' ;         
//        $reroute_url = '/app' ; 
//        $rights = 'none' ; 
//        
//        switch ($auth_name) {
//            case 'error':
//                $validate_result['pass'] = 0 ; 
//                break ;
//            case 'admin':
//                $reroute_url = '/app'  ; 
//                $master_user = array(
//                    'view_level' => 7,
//                    'edit_level' => 7
//                    ) ;
//                $validate_result = $this->Action_Validate_Master_User_Authorization($auth_name,$master_user) ; 
//                break ;
//            case 'admin_billing':
//            case 'admin_content':                
//                $reroute_url = '/app/admin/accounts' ; 
//                $master_user = array(
//                    'view_level' => 8,
//                    'edit_level' => 9
//                    ) ;
//                $validate_result = $this->Action_Validate_Master_User_Authorization($auth_name,$master_user) ; 
//                break ;                 
//            case 'app_mode_toggle':
//                $reroute_url = '/app'  ; 
//                $validate_result = $this->Action_Validate_Master_User_Authorization($auth_name,$auth_input) ; 
//                break ;               
//            }
//           
//        
//        switch ($validate_result['pass']) {
//            case 1:
//                $this->Set_Alert_Response(34) ; // Pass - Authorized
//                break ; 
//            case 0:
//            default:
//                $this->Set_Alert_Response(35) ; // Fail - Not Authorized
//            }
//
//        
//        $result = $this->Get_Response() ; 
//        $another = $result ; 
//        $result['reroute_url'] = $reroute_url ; 
//        $result['pass'] = $validate_result['pass'] ; 
//        $result['privilege'] = $validate_result['privilege'] ; 
//        $result['test'] = $validate_result ; 
//
//        
//        $this->Set_Authorization($result) ; 
//        
//        return $this ; 
//        }
    
    
    
    
    // Make sure this is a valid email format and it's not a duplicate
    public function Action_Validate_Email_Address($email_address,$confirm_email_address = null) {
        
        $continue = 1 ; 
        
        if ((null != $confirm_email_address) AND ($email_address != $confirm_email_address)) {
            
            $this->Set_Alert_Response(213) ; // Mismatched email address            
            $continue = 0 ; 
            }
        
        
        if ($continue == 1) {
            
            $email_validate = Utilities::Validate_Email_Address($email_address) ;
        
            switch ($email_validate['valid']) {
                case 'no':
                    $continue = 0 ; 
                    $this->Set_Alert_Response(31) ; // Improperly formatted email address
                    break ;
                case 'yes':    
                    $result = $this->Retrieve_Email_Address($email_validate['email_address']) ; 

                    if ($result['result_count'] > 0) {
                        if ($result['results']['user_id'] == 0) {
                            $continue = 0 ; 
                            $this->Set_Alert_Response(75) ; // Email address is valid
                            $email_validate['duplicate'] = 'unused' ; 
                            $email_validate['email_address_id'] = $result['results']['email_address_id'] ; 
                            } else {
                                $continue = 0 ; 
                                $this->Set_Alert_Response(11) ; // Email address is taken
                                $email_validate['duplicate'] = 'yes' ;                    
                                }                     
                        } else {
                            $this->Set_Alert_Response(75) ; // Email address is valid
                            $email_validate['duplicate'] = 'no' ;
                            }                
                    break ;
                }

            $this->email_address_validation = $email_validate ;             
            }
        
            
        return $this ;     
        }
    

    
    
    // Make sure this is a valid password
    public function Action_Validate_Password($password,$password_confirm = 'none') {
        
        $continue = 1 ; 
        
        $password_validate = Utilities::Validate_Password($password,$password_confirm) ; 
        
        switch ($password_validate['valid']) {
            case 'yes':
                $this->Set_Alert_Response(78) ; // Password is valid
                break ;
            case 'no':
                switch ($password_validate['error']) {
                    case 'length':
                        $this->Set_Alert_Response(5) ; // Password is valid
                        break ; 
                    case 'mismatch':
                        $this->Set_Alert_Response(6) ; // Password is valid                        
                        break ;    
                    }
                break ;
            }
        
        $this->password_validation = $password_validate ; 
            
        return $this ;     
        }

    
    // Make sure this is a valid physical address
    public function Action_Validate_Physical_Address($address_input) {
        
       $result = array(
            'address' => $address_input,
            'valid' => 'yes'
            ) ; 
        
        if (strlen($result['address']['street_address_1']) < 6) {
            $result['valid'] = 'no' ; // Entry is less than 6 characters long
            $this->Set_Alert_Response(79) ; // Street address invalid
            }
        
        if (strlen($result['address']['state']) < 2) {
            $result['valid'] = 'no' ; // Entry is less than 2 characters long
            $this->Set_Alert_Response(80) ; // State invalid
            }

        if (strlen($result['address']['postal_code']) < 5) {
            $result['valid'] = 'no' ; // Entry is less than 5 characters long
            $this->Set_Alert_Response(81) ; // Postal code invalid
            }        
        
        if ($result['valid'] == 'yes') {
            $this->Set_Alert_Response(82) ; // Address is valid
            }
        
        $this->physical_address_validation = $result ; 
            
        return $this ;     
        }
    
    
    

    
    public function Action_Update_User($user_input) {
        
        if (isset($user_input['user_auth_name'])) {
            $auth_query_options = array(
                'auth_role_name' =>  $user_input['user_auth_name']
                ) ; 
            $user_auth_result = $this->Retrieve_System_Auth_List($auth_query_options) ; 
            $user_input['user_auth_id'] = $user_auth_result['results'][0]['auth_id'] ;            
            unset($user_input['user_auth_name']) ; 
            }
        
        if (isset($user_input['full_name'])) {            
            $split_name_array = $this->Action_Split_Names($user_input['full_name']) ; 
            $user_input['first_name'] = $split_name_array['first_name'] ; 
            $user_input['last_name'] = $split_name_array['last_name'] ; 
            unset($user_input['full_name']) ;             
            }
        
        
        $result = $this->Update_User($user_input) ; 
        $this->Set_Model_Timings('User.Action_Update_User_update_query_complete') ;

        if ($result['results'] == true) {
            
            $user_record = $this->Get_User() ;
            
            $update_subscription_lists = 1 ; 
//            foreach ($user_input as $input_key => $input_value) {
//                switch ($input_key) {
//                    case 'first_name':
//                    case 'last_name':
//                    case 'email_address_id':
//                    case 'phone_number_id':    
//                    case 'user_auth_id':
//                        $update_subscription_lists = 1 ; 
//                        break ; 
//                    }
//                }
            
            
            if ($update_subscription_lists == 1) {
                
                    // Pull broadcast & series list subscriptions
                    $user_record['user_subscriptions_list'] = array() ; 

                    $subscriptions_query = array(
                        'filter_by_asset_id' => 'no',
                        'filter_by_user_id' => 'no',
                        'filter_by_account_id' => 'no',
                        'filter_by_type_name' => array('distribution_list_users','series_list_users'),
                        'filter_by_member_id_value' => $user_record['user_id'],
                        'filter_by_visibility_name' => 'visible',
                        'override_paging' => 'yes'
                        ) ;

                    $subscriptions = new Asset() ; 
                    $subscriptions->Set_Asset_Members_List($subscriptions_query) ; 
                    $subscriptions_list = $subscriptions->Get_Asset_Members_List() ;

                    $user_record['user_subscriptions_list'] = array() ; 
                    foreach ($subscriptions_list as $list_record) {
                        $user_record['user_subscriptions_list'][] = $list_record['asset_id'] ;                        
                        }
                                        
                    $task_values = array(
                        'task_action' => 'admin_distribution_list_timestamp_last_member_update',
                        'timestamp_action' => TIMESTAMP - 3,
                        'structured_data' => array(
                            'user_id' => $user_record['user_id'],
                            'asset_id' => $user_record['user_subscriptions_list']
                            )        
                        ) ;
                    $this->Action_Create_Cron_Task_Automation($task_values) ; 
                    $this->Set_Model_Timings('User.Action_Update_User_admin_cron_created') ;                
                                
                }
            
            
            
            $this->Set_Alert_Response(23) ; // User update successful
            } else {
                $this->Set_Alert_Response(24) ; // User update error
                }
        
        return $this ; 
        }
    
    
    
    
    public function Action_Create_Phone_Number($new_phone_number,$phone_number_options) {
        
        $continue_script = 1 ; 
        
        if (isset($new_phone_number)) {

            if (!isset($phone_number_options['phone_country_dialing_code'])) {
                // Need to grab the users country dialing code here... 
                $phone_number_options['phone_country_dialing_code'] = '+1' ; 
                }

            $phone_number_validate_options = array(
                'phone_country_dialing_code' => $phone_number_options['phone_country_dialing_code']
                ) ;                 
            $this->Action_Validate_Phone_Number($new_phone_number,$phone_number_validate_options) ; 
            $data['phone_validation'] = $this->Get_Phone_Number_Validation() ; 

            switch ($data['phone_validation']['valid']) {
                case 'no':
                    $continue_script = 0 ;                 
                    break ; 
                }

            // Ensure phone number is not duplicate. Create new if none exists.
            if ($continue_script == 1) {

                switch ($data['phone_validation']['duplicate']) {                    
                    case 'yes':
                        $continue_script = 0 ;                 
                        break ;
                    case 'unused':
                        $phone_number_options['phone_number_id'] = $data['phone_validation']['phone_number_id'] ; 
                        break ;                    
                    case 'no':
                    default:

                        $phone_input = array(
                            'phone_number' => $data['phone_validation']['local'],
                            'phone_country_dialing_code' => $data['phone_validation']['phone_country_dialing_code'],
                            'user_id' => $this->user_id
                            ) ; 

                        if (isset($phone_number_options['phone_country_id'])) {
                            $phone_input['phone_country_id'] = $phone_number_options['phone_country_id'] ; 
                            } else {
                                $phone_input['phone_country_id'] = $this->user['country_id'] ; 
                                }

                        $data['phone_number_record'] = $this->Create_Phone_Number($phone_input) ;                     

                        $phone_number_options['phone_number_id'] = $data['phone_number_record']['insert_id'] ;


                        if ($data['phone_number_record']['error']) {
                            $this->Set_Alert_Response(77) ; // Phone invalid or blank. Saving did not work
                            $this->Append_Alert_Response('none',array(
                                'admin_context' => json_encode($data),
                                'location' => __METHOD__
                                )) ;                            
                            $continue_script = 0 ;             
                            } else {
                                $this->Set_Phone_Number_ID($phone_number_options['phone_number_id']) ; 
                                $this->Set_Alert_Response(210) ; // Phone valid and added
                                }                  
                    }            
                } 
            }        
        
        return $this ;
        }
    
    
    
    
    public function Action_Create_Email_Address($new_email_address,$email_options) {
        
        $email_validate = Utilities::Validate_Email_Address($new_email_address) ; 
        
        if ($email_validate['valid'] == 'yes') {
            
            $email_record = $this->Create_Email_Address($new_email_address,$email_options) ; 

            if ($email_record['insert_id']) {
                if ($email_options['trigger_verification'] == 'yes') {
                    $verification = $this->Action_Trigger_Email_Verification($email_record['insert_id']) ;                   
                    
                    $this->Set_Alert_Response(12) ; // Email verification triggered                    
                    $personalization = array(
                        'user_record' => array(
                            'email_address' => $new_email_address
                            )
                        ) ;
                    $this->Append_Alert_Response($personalization,array(
                        'location' => __METHOD__
                        )) ;                     
                    
                    } else {
                        $this->Set_Alert_Response(32) ; // New address added
                        }
                } else {
                    if ($email_record['results']['user_id'] == $this->user_id) {
                        
                        // Found the email address assigned to THIS user
                        if ($email_record['results']['email_verified'] == 1) {
                            $this->Set_Alert_Response(30) ; // Email address in use by THIS user and it's already verified
                            } else {
                                // Email address added by THIS user but not yet verified. Retrigger verification.
                                if ($email_options['trigger_verification'] == 'yes') {
                                    $verification = $this->Action_Trigger_Email_Verification($email_record['results']['email_address_id']) ;
                                    $this->Set_Alert_Response(12) ; // Email verification triggered
                                    
                                    $personalization = array(
                                        'user_record' => array(
                                            'email_address' => $new_email_address
                                            )
                                        ) ;
                                    $this->Append_Alert_Response($personalization,array(
                                        'location' => __METHOD__
                                        )) ;                                    
                                    }  else {
                                        $this->Set_Alert_Response(30) ; // Email address in use by THIS user (BUT IT IS NOT VERIFIED)
                                        }                         
                                }
                        
                        
                        
                        
                        
                        } else {
                        
                            $data['user_record'] = $this->Get_User() ; 
                            switch ($data['user_record']['auth_role_name']) {
                                case 'user_pending':   
                                    $this->Set_Alert_Response(11) ; // Email address in use by different user. Submitting user pending.
                                    $personalization = array(
                                        'user_record' => array(
                                            'email_address' => $new_email_address
                                            )
                                        ) ;
                                    $this->Append_Alert_Response($personalization,array(
                                        'location' => __METHOD__
                                        )) ;                                    
                                    break ;
                                default: 
                                    $this->Set_Alert_Response(244) ; // Email address in use by different user. Submitting user active.
                                    $personalization = array(
                                        'user_record' => array(
                                            'email_address' => $new_email_address
                                            )
                                        ) ;
                                    $this->Append_Alert_Response($personalization,array(
                                        'location' => __METHOD__
                                        )) ;                                     
                                }
                        
                        
                            
                            }
                    }
                } else {
                    $this->Set_Alert_Response(31) ; // Improperly formatted email address
                    }
                
        
        return $this ;
        }
    
    
    
    public function Action_Transfer_Email_Address($transfer_parameters) {
        
        // Set the email address record
        $this->Set_User_Email_By_Email_Address_ID($transfer_parameters['email_address_id']) ; 
        $data['user_record'] = $this->Get_User() ; 
        
        // Set the email address as being owned by the target user
        $transfer_input = array(
            'user_id' => $transfer_parameters['transfer_user_id']
            ) ;         
        $transfer_result = $this->Update_Email($transfer_input) ; 
        
        $transfer_result['results'] = true ; 
        
        // If the update is successful, then run rest of updates
        if ($transfer_result['results'] == true) {
            
            // Attempt to replace the transferring user's email address with another 
            // one in their list for user record and profiles ... otherwise, reset to 0
            
            // Get the user's email address list
            $this->Set_User_Email_List() ; 
            $email_address_list = $this->Get_User_Email_List() ;
            
            // Check to see if the transferring user's default email address is the transferred one
            if ($data['user_record']['email_address_id'] == $transfer_parameters['email_address_id']) {

                $user_input = array() ; 

                if ($email_address_list == 'error') {
                    $user_input['email_address_id'] = 0 ; 
                    } else {
                        $user_input['email_address_id'] = $email_address_list[0]['email_address_id'] ; 
                        }
                
                $this->Action_Update_User($user_input) ;                                
                }
             
            
            // Pull the transferring user's profile list
            $this->Set_Profile_List() ; 
            $user_profile_list = $this->Get_Profile_List() ;
             
            // For each profile, set a replacement email address id if available and 
            // the transferring email ID is currently set as the default
            if (count($user_profile_list) > 0) {
                foreach ($user_profile_list as $user_profile) {

                    if ($user_profile['email_address_id'] == $transfer_parameters['email_address_id']) {

                        if ($email_address_list == 'error') {
                            $user_profile_update['email_address_id'] = 0 ; 
                            } else {
                                $user_profile_update['email_address_id'] = $email_address_list[0]['email_address_id'] ; 
                                }                    

                        $this->Set_Profile_By_ID($user_profile['profile_id'])->Action_Update_Profile($user_profile_update) ;
                        }                
                    }
                }
            
            // If selected, set the transferred email address as the default email 
            // for the target user as well as all their associated profiles.
            if ($transfer_parameters['set_as_default'] == 'true') {
                
                // Set email address as default for user
                $transfer_user_input['email_address_id'] = $transfer_parameters['email_address_id'] ; 
                
                $transfer_user = new Account ($transfer_parameters['transfer_user_id']) ;
                $transfer_user->Action_Update_User($transfer_user_input) ; 
                
                // Pull a profile list
                $transfer_user->Set_Profile_List() ; 
                $transfer_profile_list = $transfer_user->Get_Profile_List() ; 
                
                // For each profile, set the email address as the default
                foreach ($transfer_profile_list as $transfer_profile) {
                    
                    $profile_update = array(
                        'email_address_id' => $transfer_parameters['email_address_id']
                        ) ; 
                    
                    $transfer_user->Set_Profile_By_ID($transfer_profile['profile_id'])->Action_Update_Profile($profile_update) ;
                    }
                
                }
            
            
            $this->Set_Alert_Response(23) ; // User update successful
            } else {
                $this->Set_Alert_Response(24) ; // User update error
                }
        
        
        return $this ; 
        }
    
    
    
    // Delete email address by first replacing all active uses of that email_address_id w/ the user primary email_address_id
    // If the user primary email_address_id is the one being deleted, don't allow it.
    public function Action_Delete_Email_Address($email_address_id_to_delete) {
        
        $continue_script = 1 ; 
        
        // Don't allow script to continue if primary email_address_id is being deleted
        if ($email_address_id_to_delete == $this->user['email_address_id']) {
            
            $continue_script = 0 ; 
            $this->Set_Alert_Response(36) ; // Improperly formatted email address

            } 
        
        if (($continue_script == 1) AND ($this->user_email['user_id'] != $this->user_id)) {
            
            $continue_script = 0 ; 
            $this->Set_Alert_Response(37) ; // user_id mismatch          
            
            }

        if (($continue_script == 1)) {
            
            $query_options = array(
                'override_paging' => 'yes',
                'filter_by_user_auth' => 'yes'
                ) ; 
            
            $account = new Account($this->user_id) ; 
            $account->Set_Profile_List() ; 
            $profile_list = $account->Get_Profile_List() ; 
            
            foreach ($profile_list as $profile) {
                
                if ($profile['email_address_id'] == $email_address_id_to_delete) {
                    
                    // Update the profile with the default user email_address_id
                    $profile_input = array(
                        'email_address_id' => $this->user['email_address_id']
                        ) ; 
                    
                    $profile_update = $account->Set_Profile_ID($profile['profile_id'])->Action_Update_Profile($profile_input) ;   
                    
                    }                
                }
                
            
            // Delete the email following changing all of the old email_address_id's
            $delete_email = $this->Delete_Email($email_address_id_to_delete) ; 
            if ($delete_email['results'] == 1) {
                $this->Set_Alert_Response(38) ; // Successful delete
                } else {
                    $this->Set_Alert_Response(39) ; // Delete error
                    }
            
            }
        
         
    
        return $this ; 
        }    
    
    

    
    
    public function Action_Verify_Email($verification_status) {
        
        $email_input = array() ; 
        $email_input['email_verified'] = $verification_status ; 
        
        $result = $this->Update_Email($email_input) ; 

        
        if ($result['results'] == true) {
            $this->alert_id = 23 ; 
            $alert = Utilities::System_Alert($this->alert_id) ; 
            $this->response = $alert ;
            } else {
                $this->alert_id = 24 ; 
                $alert = Utilities::System_Alert($this->alert_id) ; 
                $this->response = $alert ;
                }
        
        return $this ; 
        }    
    
    
    public function Action_Trigger_Email_Verification($email_address_id,$verification_options = array()) {
        
        switch ($verification_options['verification_type']) {
            case 'new_user_registration':
                $email_address_verification_asset_id = $this->Set_System_Default_Value('new_user_registration')->Get_System_Default_Value() ;
                $campaign_title = 'New User Registration Email: '.$this->user_email['email_address'] ;
                break ;
            case 'new_user_welcome':
                
                $asset_query_parameters['set_by'] = 'slug' ;
                
                $asset = new Asset() ; 
                $asset->Set_Asset_By_ID('essenty-new-user-welcome-eml',$asset_query_parameters) ;
                $data['asset_welcome_email'] = $asset->Get_Asset() ;
                
                $email_address_verification_asset_id = $data['asset_welcome_email']['asset_id'] ;
                
                $campaign_title = 'New User Welcome Email: '.$this->user_email['email_address'] ;
                break ; 
            case 'account_marketing_credit_alert':
//                $email_address_verification_asset_id = $this->Set_System_Default_Value('user_password_reset')->Get_System_Default_Value() ;
                $email_address_verification_asset_id = 41620 ; 
                $campaign_title = 'Account Marketing Credit Alert: '.$this->user_email['email_address'] ;
                break ;                
            case 'user_password_reset':
                $email_address_verification_asset_id = $this->Set_System_Default_Value('user_password_reset')->Get_System_Default_Value() ;
                $campaign_title = 'User Password Reset: '.$this->user_email['email_address'] ;
                break ; 
            case 'starter_content_bundle':
                $email_address_verification_asset_id = $this->Set_System_Default_Value('starter_content_bundle')->Get_System_Default_Value() ;
                $campaign_title = 'Starter Content Bundle Request: '.$this->user_email['email_address'] ;
                break ; 
            case 'live_training_session_info':
                $email_address_verification_asset_id = $this->Set_System_Default_Value('live_training_session_info')->Get_System_Default_Value() ;
                $campaign_title = 'User Live Training Session: '.$this->user_email['email_address'] ;
                break ;                 
            case 'email_address_verification':
            default: // New email address
                $email_address_verification_asset_id = $this->Set_System_Default_Value('email_address_verification')->Get_System_Default_Value() ;
                $campaign_title = 'User Email Verification: '.$this->user_email['email_address'] ; 
        
            }        
        
        
        $app_account_support_profile_id = $this->Set_System_Default_Value('app_account_support')->Get_System_Default_Value() ;
        
        $user_input = array(
            'user_id' => $this->user_id,
            'email_verified' => 1,
            'email_address_id' => $email_address_id
            ) ;
        

        $this->Action_Create_System_Action(1,$user_input) ; 
        
        $this->Set_User_Email_By_Email_Address_ID($email_address_id) ; 


        
        // Get app default email domain...
        $domain_query_options = array(
            'app_default_domain' => 1
            ) ;
        
        $this->Set_System_List('app_email_domains',$domain_query_options) ; 
        $email_domain = $this->Get_System_List('app_email_domains')[0] ; // Email app domain
        
        $email = new Email() ;
        $email->Set_Sending_Domain($email_domain['domain_display_name'])->Set_User_By_Profile_ID($app_account_support_profile_id) ; 
        $email->Action_Compile_Email($email_address_verification_asset_id)->Action_Personalize_Email() ;
        
 

        $campaign_options = array(
            'asset_id' => $email_address_verification_asset_id,
            'campaign_status_name' => 'deleted',
            'automation_status_name' => 'scheduled',
            'timestamp_campaign_scheduled' => TIMESTAMP,
            'campaign_title' => $campaign_title,
            'timestamp_next_action' => TIMESTAMP,
            'delivery_trigger' => 'static'
            ) ; 

        $email->Action_Create_Campaign($campaign_options) ;
        $email_campaign = $email->Get_Campaign() ;
        
        // Update the campaign with this specific static recipient...
        $recipient_variables = array() ; 
        $recipient_variables = array(
            'email_address' => $this->user_email['email_address'],
            'first_name' => $this->user['first_name'],
            'last_name' => $this->user['last_name']
            ) ;
        
        
        $campaign_update_options['campaign_id'] = $email_campaign['campaign_id'] ; 
        $campaign_update_options['structured_data_01'] = $email_campaign['structured_data_01'] ; 
        $campaign_update_options['structured_data_01']['recipients']['to']['static']['email_address'][] = $this->user_email['email_address'] ;
        $campaign_update_options['structured_data_01']['recipients']['to']['static']['recipient_variables'][$this->user_email['email_address']] = $recipient_variables ;
        $campaign_update_options['structured_data_01']['action_keys'][] = array(
            'action_key_id' => $this->system_action['action_key_id'],
            'action_id' => $this->system_action['action_id'],
            'key_id' => $this->system_action['key_id'],
            'action_verify' => $this->system_action['action_verify']
            ) ;
        
        if ($verification_options['verification_type'] == 'new_user_registration') {
            $campaign_update_options['structured_data_01']['personalization_overrides']['user_record']['user_id'] = $this->user['user_id'] ;
            $campaign_update_options['structured_data_01']['personalization_overrides']['user_record']['registration_token'] = $this->user['registration_token'] ; 
            }
        
                
        $email->Action_Update_Campaign($campaign_update_options) ; 
        
        $automation_record = $email->Get_Automation() ; 
        $email->Action_Trigger_Automation_Queue($automation_record['automation_id']) ; 
        
        
//        $verification_email = $email->Action_Queue_Email(
//            array(
//                'from_email_address' => $email->profile['email_address'],
//                'from_name' => $email->profile['first_name'].' '.$email->profile['last_name'],
//                'reply_to_address' => $email->profile['email_address']
//                ), 
//            $recipient_to_array, $recipient_cc_array, $recipient_bcc_array, $recipient_variables,
//            'internal', 'internal', 'now' 
//            ) ;
        
        
        $delivery_response = $email->Get_Response() ;
        $this->Set_Alert_Response($delivery_response['alert_id']) ; 
        
        return $this ; 
        
        }
    
    
    
    // $distribution_list_name = shortcut name in defaults
    public function Action_Update_User_Distro_List($distribution_list_name,$invite_input = array(),$options_input = array()) {
        
        // Add or unsubscribe a user to a distribution_list_users asset
        if (!isset($invite_input['user_id'])) {
            
            $invite_input['user_id'] = $this->user_id ; 
            } 
        
        if (!isset($options_input['require_optin'])) {
            
            $options_input['require_optin'] = 'false' ;    
            }
        
        if (!$options_input['subscribe_type_email']) {
            $options_input['subscribe_type_email'] = 'false' ; 
            }
        if (!$options_input['subscribe_type_message']) {
            $options_input['subscribe_type_message'] = 'false' ; 
            }
        
        // Get the asset ID for the distro list...
        $distro_list = $this->Set_System_Default_Value($distribution_list_name)->Get_System_Default_Value() ;

        $new_user_distro = new Asset() ; 
        $new_user_distro->Set_Asset_By_ID($distro_list) ; 
        $new_user_distro->Action_Asset_Member_Process($invite_input,$options_input) ; 

        return $this ; 
        } 
    
    
    public function Action_Create_New_User($user_input,$email_options = array()) {

        global $server_config ; // Need this to grab the default user distro list
        
        
        $continue_script = 1 ; 
        
        // First check to see if this email address has been used
        $this->user_id = 0 ; 
        
        if (!isset($email_options['password_selection'])) {
            $email_options['password_selection'] = 'primary' ; 
            }
        if (!isset($email_options['trigger_verification'])) {
            $email_options['trigger_verification'] = 'yes' ; 
            }
        if (!isset($user_input['user_auth_id'])) {
            $auth_query_options = array(
                'auth_role_name' =>  'user_pending'
                ) ; 
            $user_auth_result = $this->Retrieve_System_Auth_List($auth_query_options) ; 
            $user_input['user_auth_id'] = $user_auth_result['results'][0]['auth_id'] ;
            }           
        
        
        
        // Ensure country_d was submitted and pull approrpriate country record
        if (isset($user_input['country_id'])) {
            $country_query = array(
                'country_id' => $user_input['country_id']
                ) ; 
            $this->Set_System_List('list_countries',$country_query) ; 
            $country_list = $this->Get_System_List('list_countries') ; 
            $data['country_record'] = $country_list[0] ;  
                        
            $user_input['timezone'] = $data['country_record']['country_timezone'] ; 
            } else {
                $continue = 0 ;    
                $this->alert_id = 208 ; // Missing country submission
                }

    
        
        // Validate the user email address
        if ($continue_script == 1) {
            $this->Action_Validate_Email_Address($user_input['email_address']) ; 
            $data['email_validation'] = $this->Get_Email_Address_Validation() ; 

            switch ($data['email_validation']['valid']) {
                case 'no':
                    $continue_script = 0 ;                 
                    break ; 
                }                        
            }
        
        
        // Ensure email address is not duplicate. Create if duplicate does not exist
        if ($continue_script == 1) {
            switch ($data['email_validation']['duplicate']) {                    
                case 'yes':
                    $continue_script = 0 ;                 
                    break ;
                case 'unused':
                    $user_input['email_address_id'] = $data['email_validation']['email_address_id'] ;
                    break ;                    
                case 'no':
                default:
                    $data['email_record'] = $this->Create_Email_Address($user_input['email_address'],$email_options) ;
                    $user_input['email_address_id'] = $data['email_record']['insert_id'] ;                    
                    
                    if ($data['email_record']['error']) {
                        $this->alert_id = 11 ; 
                        $continue_script = 0 ;             
                        }                    
                }        
            }
        
        
        if ($continue_script == 1) {
            
            $blacklist = $this->user_create_blacklist ; 
            foreach ($blacklist['email_domain'] as $unallowed) {

                if (str_contains($data['email_validation']['email_domain'],$unallowed)) {
                    $this->alert_id = 278 ; 
                    $continue_script = 0 ; 
                    sleep(3) ; 
                    break ; 
                    }
    
                }
            }
        
        
        // Validate and Create the user phone number
        if ($continue_script == 1) {
            
            if (isset($user_input['phone_number'])) {
                
                if (!isset($user_input['phone_country_dialing_code'])) {
                    $user_input['phone_country_dialing_code'] = $data['country_record']['country_dialing_code'] ; 
                    }

                $phone_number_validate_options = array(
                    'phone_country_dialing_code' => $user_input['phone_country_dialing_code']
                    ) ;                 
                $this->Action_Validate_Phone_Number($user_input['phone_number'],$phone_number_validate_options) ; 
                $data['phone_validation'] = $this->Get_Phone_Number_Validation() ; 
                
                switch ($data['phone_validation']['valid']) {
                    case 'no':
                        $continue_script = 0 ;                 
                        break ; 
                    }
                
                // Ensure phone number is not duplicate. Create new if none exists.
                if ($continue_script == 1) {

                    switch ($data['phone_validation']['duplicate']) {                    
                        case 'yes':
                            $continue_script = 0 ;                 
                            break ;
                        case 'unused':
                            $user_input['phone_number_id'] = $data['phone_validation']['phone_number_id'] ; 
                            break ;                    
                        case 'no':
                        default:

                            $phone_input = array(
                                'phone_number' => $data['phone_validation']['local'],
                                'phone_country_dialing_code' => $data['phone_validation']['phone_country_dialing_code']
                                ) ; 

                            if (isset($user_input['phone_country_id'])) {
                                $phone_input['phone_country_id'] = $user_input['phone_country_id'] ; 
                                } else {
                                    $phone_input['phone_country_id'] = $user_input['country_id'] ; 
                                    }

                            $data['phone_number_record'] = $this->Create_Phone_Number($phone_input) ;                     

                            $user_input['phone_number_id'] = $data['phone_number_record']['insert_id'] ;


                            if ($data['phone_number_record']['error']) {
                                $this->alert_id = 77 ; // Phone invalid or blank. Saving did not work
                                $continue_script = 0 ;             
                                }                    
                        }            
                    }                
                
                
                }             
            }
        
            
        
        if ($continue_script == 1) {
            
            // Create custom variables for items that will be unset
            $password = $user_input['password'] ;
            $email_address = $user_input['email_address'] ; // Saving from original input
            $phone_number = $user_input['phone_number'] ; // Saving from original input
            unset($user_input['email_address'],$user_input['confirm_email_address'],$user_input['password']) ; // removes from the array being added to User record
            unset($user_input['phone_number'],$user_input['phone_country_dialing_code'],$user_input['phone_country_id']) ; 
            

            // Create the user record   
            $data['user_record'] = $this->Create_User($user_input) ;            
            
            if (!$data['user_record']['error']) {
             
                            
                $this->Set_User_By_ID($data['user_record']['insert_id']) ; 
            
                // Initiate the user's registration entry and generate a token
                $save_registration = array(
                    'registration_step' => 1
                    ) ;
                $this->Action_Save_User_Registration($save_registration) ;
                $this->Set_User_By_ID($data['user_record']['insert_id']) ; 
                
                
                
                // Updates email record with the new user_id
                $email_input['user_id'] = $data['user_record']['insert_id'] ; 
                $update_email_record = $this->Set_Email_Address_ID($user_input['email_address_id'])->Update_Email($email_input) ; 
                                
                // Update the email record w/ the new password
                $password_update = array(
                    'new_password' => $password,
                    'confirm_new_password' => $password,
                    'require_authentication' => 'no'
                    ) ;
                $new_password = $this->Action_Update_Password($password_update) ;
                
                // Updates phone number record with the new user_id
                if ($user_input['phone_number_id']) {
                    $phone_update_input['user_id'] = $data['user_record']['insert_id'] ; 
                    $update_phone_record = $this->Set_Phone_Number_ID($user_input['phone_number_id'])->Update_Phone_Number($phone_update_input) ;            
                    $data['update_phone_record'] = $update_phone_record ;                    
                    }

                
                // Add to all active user distribution list
                $invite_input['user_id'] = $data['user_record']['insert_id'] ; 
                $options_input['require_optin'] = 'false' ;   
                $options_input['subscribe_type_email'] = 'true' ;
                
                // Get the asset ID for the distro list...
                $distro_list_all_users = $this->Set_System_Default_Value('distro_list_all_users')->Get_System_Default_Value() ;
                
                $new_user_distro = new Asset() ; 
                $new_user_distro->Set_Asset_By_ID($distro_list_all_users) ; 
                $new_user_distro->Action_Asset_Member_Process($invite_input,$options_input) ; 
                
                // Trigger verification for new email address
                if ($email_options['trigger_verification'] == 'yes') {
                    $this->Action_Trigger_Email_Verification($user_input['email_address_id'],$email_options) ; 
                    }                
                                
                // Set the user so the record can be pulled
                $this->Set_User_By_ID($data['user_record']['insert_id']) ; 
                $data['post_user_record'] = $this->user ; 

                $this->alert_id = 14 ; // Success - new user created is actually
                $continue_script = 1 ;
                
                } else {
                    $this->alert_id = 15 ;  // Error creating account
                    $continue_script = 0 ;
                    }        
            }        
    
        $this->test_data = $data ; 
        $this->Set_Alert_Response($this->alert_id) ; 
        $this->Append_Alert_Response('none',array(
            'admin_context' => json_encode($data),
            'location' => __METHOD__
            )) ;        
        
        return $this;
        }
    
    
    public function Action_Update_Email_Address($new_email_address,$current_email_address_id,$profile_id = 'none',$set_primary = 'false') {
        
         

        if (!isset($email_options['password_selection'])) {
            $email_options['password_selection'] = 'primary' ; 
            }
        if (!isset($email_options['trigger_verification'])) {
            $email_options['trigger_verification'] = 'yes' ; 
            }
        
        $email_record = $this->Create_Email_Address($new_email_address,$email_options) ;
        
        // If new email record was created, then associate new email_address_id w/ the submitted profile, and if necessary,
        // update the default email_address_id for the User level
        if ($email_record['insert_id']) {

            // Email address did not exist in database; ok to assign to User and/or Profile
            $new_email_address_id = $email_record['insert_id'] ; 
            
            // If profile_id submitted, swap new email_address_id in Profile
            if ('none' !== $profile_id) {
                
                $profile_input = array(
                    'email_address_id' => $new_email_address_id
                    );
                
                $account = new Account($this->user_id);
                $account->Set_Profile_By_ID($profile_id) ; 
                $account_record = $account->Update_Profile($profile_input) ;
                
                }
            
            // If User selected to set as primary email, then update the primary User email id
            if ('true' === $set_primary) {
                
                $user_input = array(
                    'email_address_id' => $new_email_address_id
                    );
                
                $user_record = $this->Update_User($user_input) ; 
                
                }            
            
            
            // Trigger verification for new email address
            if ($email_options['trigger_verification'] == 'yes') {
                $this->Action_Trigger_Email_Verification($new_email_address_id) ; 
                }
            
            
            $this->alert_id = 12 ; 
            
            
            } else {

                switch ($email_record['results']['user_id']) {
                    case $this->user_id:
                        
                        // If profile_id submitted, swap new email_address_id in Profile
                        if ('none' !== $profile_id) {
                            $profile_input = array(
                                'email_address_id' => $email_record['results']['email_address_id']
                                );

                            $account = new Account($this->user_id);
                            $account->Set_Profile_By_ID($profile_id) ; 
                            $account_record = $account->Update_Profile($profile_input) ;
                            }

                        // If User selected to set as primary email, then update the primary User email id
                        if ('true' === $set_primary) {
                            $user_input = array(
                                'email_address_id' => $email_record['results']['email_address_id']
                                );

                            $user_record = $this->Update_User($user_input) ; 
                            }                        
                        
                        switch ($email_record['results']['email_verified']) {
                            case 1:
                                $this->alert_id = 13 ; 
                                break ;
                            default:
                                // Must trigger verification
                                if ($email_options['trigger_verification'] == 'yes') {
                                    $this->Action_Trigger_Email_Verification($new_email_address_id) ; 
                                    $this->alert_id = 12 ; 
                                    } 
                            }
                        
                        
                        break ;
                    default:
                    
                        $this->alert_id = 11 ; 
                        
                    }
                }
        
        $this->Set_Alert_Response($this->alert_id) ; 
        
        return $this ;
        
        }    
    

    
    // This is set to update ALL associated email address's with the same password based on User ID
    // require_authentication = yes means the user needs to put in their current password and validate before you can change the existing one
    public function Action_Update_Password($password_input = array()) {

        $new_password = $password_input['new_password'] ; 
        $confirm_new_password = $password_input['confirm_new_password'] ; 
        
        $this->Set_User_Email_List() ; 
        $email_address_list = $this->Get_User_Email_List() ;
                
        $this->alert_id = 0 ;


        switch ($password_input['require_authentication']) {
            case 'no':
                
                break ; 
            case 'yes':
            default:
                $verify_result = $this->Action_Password_Verify($password_input['username'],$password_input['current_password']) ; 

                switch ($verify_result['result']) {
                    case 'error':
                        $this->alert_id = $verify_result['alert_id'] ; 
                        break ; 
                    default:
                        $this->alert_id = 0 ; 
                    }                    
            }
            

        if ((strlen($new_password) < 7) AND ($this->alert_id == 0)) { 
            $this->alert_id = 5;             
            }

        if (($new_password != $confirm_new_password) AND ($this->alert_id == 0)) {
            $this->alert_id = 6; 
            }
        
        if ($this->alert_id == 0) {
            
            $new_hash = Password::hashPassword($new_password);
            $continue = Password::checkPassword($new_password,$new_hash);
            
            switch ($continue) {
                case true:
                    
                    foreach ($email_address_list as $email_address) {
                        
                        $password_update_array = array(
                            'password' => $new_hash,
                            'email_address_id' => $email_address['email_address_id']
                            ) ; 
                        $result = $this->Create_Password($password_update_array) ;                                                
                        }
                    
                    switch ($result['results']) {
                        case false:
                            $this->alert_id = 8 ; 
                            break ;
                        case true:
                            $this->alert_id = 9 ; 
                            break ;                            
                        }
                    
                    break ;
                case false:
                    $this->alert_id = 7 ; 
                    break ;
                }
            }

        $this->Set_Alert_Response($this->alert_id) ; 

        return $this ; 
        
        } 
    
    
    
    
    
    //////////////////////
    //                  //
    // UTILITIES        //
    //                  //
    //////////////////////
    
    
    public function Admin_Login($master_user_id,$session_token,$target_user_id) {

        
        global $server_config ; 
        global $_SERVER ; 
        global $app_recent_stable ; 
        
        $this->alert_id = 0 ;
        $continue = 0 ; 
        
        $access = $this->Login_Verify($master_user_id,$session_token) ;

//        print_r($access) ; 
        
        if ($access['login_verified'] == 'YES') {
            
            $target_user = new User($target_user_id) ; 
            $target_user_record = $target_user->Get_User() ; 
            
            if (($target_user_record['session_token'] == '') OR ($target_user_record['session_token'] == 'logged_out')) {
                $session_token = $this->DB->Simple_Hash(array($target_user_id,TIMESTAMP)) ; 
                $last_login = TIMESTAMP ; 
                } else {
                    $session_token = $target_user_record['session_token'] ; 
                    $last_login = $target_user_record['last_login'] ; 
                    }
            
            $_SESSION['user_id'] = $target_user_id ;
            $_SESSION['master_user_id'] = $master_user_id ; 
//            $_SESSION['session_token'] = $session_token;
            $_SESSION['last_login'] = TIMESTAMP ;
            $_SESSION["alert_id"] = 0 ; 
            $_SESSION["alert_message"] = '' ; 
            
//            $query_array = array(
//                'table' => 'users',
//                'values' => array(
//                    'last_login' => $last_login,
//                    'session_token' => $session_token
//                    ),
//                'where' => "user_id='$target_user_id'"
//                );
//            
//            $update_session_token = $this->DB->Query('UPDATE',$query_array) ; 
            
            $history_input = array(
                'function' => 'login_admin'
                ) ; 
            $user_history = $this->Create_User_History($target_user_id,$history_input) ; 
            
            }
        

            $result = array(
                'user_id' => $target_user_id,
                'master_user_id' => $_SESSION['master_user_id'],
                'session_token' => $_SESSION['session_token'],
                'last_login' => $_SESSION['last_login'],
                'alert_id' => $alert['alert_id'],
                'alert_message' => $_SESSION["alert_message"],
                'target_user_record' => $target_user_record
                );

            return $result;        
        
        }
    
    public function Admin_Logout() {
        
        $history_input = array(
            'function' => 'logout_admin'
            ) ; 
        $user_history = $this->Create_User_History($_SESSION['user_id'],$history_input) ;
        
        $master_user_record = $this->Get_User() ; 
        
        $_SESSION['user_id'] = $master_user_record['user_id'] ;
        $_SESSION['master_user_id'] = $master_user_record['user_id'] ; 
        $_SESSION['session_token'] = $master_user_record['session_token'] ;
        $_SESSION['last_login'] = $master_user_record['last_login'] ;        
        
        return $this ; 
        }
    
    
    
    public function Action_Password_Verify($username,$password) {
        
        $set_user = $this->Set_User_By_Email($username);
        
        if ($this->email_address_id) {
            
            // Username validated. Pull up user password.
            $query_array = array(
                'table' => 'user_secure',
                'join_tables' => array(),
                'fields' => "users.user_id, users.active_account_id, 
                    user_secure.password, user_secure.email_address_id,
                    user_emails.email_verified",
                'where' => "user_secure.email_address_id='$this->email_address_id'"
                );

            $query_array['join_tables'][] = array(
                'table' => 'user_emails',
                'on' => 'user_emails.email_address_id',
                'match' => 'user_secure.email_address_id'
                );
            
            $query_array['join_tables'][] = array(
                'table' => 'users',
                'on' => 'user_emails.user_id',
                'match' => 'users.user_id'
                );

            $result = $this->DB->Query('SELECT_JOIN',$query_array);
            
            $stored_password = $result['results']['password'];
            
            $new_hash = Password::hashPassword($password);
            $continue = Password::checkPassword($password,$stored_password); // Returns 1 if passwords match; 0 if they do not match
                        
            // Password failed. Set the error alert.
            if ($continue == 0) {
                $this->Set_Alert_Response(2) ; // password failed
                } else {
                    $this->Set_Alert_Response(78) ; // password matched
                    }
                
            } else {
                $continue = 0 ;
                $this->Set_Alert_Response(1) ; // Email not in use
                }
        
        $response = $this->Get_Response() ; 
        
        return $response ; 
        }
    
    public function Login($username,$password,$options = array()) {
    
          
        global $server_config ; 
        global $_SERVER ; 
        global $app_recent_stable ; 
        
        $this->alert_id = 0 ;
        $continue = 0 ; 
        
        $set_user = $this->Set_User_By_Email($username);
        
        
        if ($this->email_address_id) {
            
            // Username validated. Pull up user password.
            $query_array = array(
                'table' => 'user_secure',
                'join_tables' => array(),
                'fields' => "users.user_id, users.active_account_id, users.user_auth_id, 
                    user_secure.password, user_secure.email_address_id,
                    user_emails.email_verified",
                'where' => "user_secure.email_address_id='$this->email_address_id'"
                );

            $query_array['join_tables'][] = array(
                'table' => 'user_emails',
                'on' => 'user_emails.email_address_id',
                'match' => 'user_secure.email_address_id'
                );
            
            $query_array['join_tables'][] = array(
                'table' => 'users',
                'on' => 'user_emails.user_id',
                'match' => 'users.user_id'
                );

            $result = $this->DB->Query('SELECT_JOIN',$query_array);
            
            $stored_password = $result['results']['password'];
            
            $active_account_id = $result['results']['active_account_id'];
            
            if ($active_account_id > 0) {
                $account = new Account() ; 
                $account->Set_Account_By_ID($active_account_id) ; 
                $account_record = $account->Get_Account() ;
                $version_input = $account_record['version'] ; 
                $version = System_Config::Create_Version_Path($version_input) ;
                $_SESSION['version'] = $version['version']['version'] ; 

                } else {
                    // Switch to the most current version of the site
                    $_SESSION['version'] = $app_recent_stable ; 
                    }
            
            
            if ($options['secure_override'] == 'yes') {
                $continue = 1 ; 
                } else {
                
                    $new_hash = Password::hashPassword($password);
                    $continue = Password::checkPassword($password,$stored_password); // Returns 1 if passwords match; 0 if they do not match                            
                    }
            
            // Password failed. Set the error alert.
            if ($continue == 0) {
                
                $this->Set_Alert_Response(2) ; // password failed
                
                // Since password failed, check for a dictionary attack
                $values_array = array(
                    'request_url' => $server_config['page_url_array']['url'],
                    'user_ip' => $_SERVER['HTTP_X_REAL_IP']
                    ) ; 

                $dictionary_test = Utilities::Prevent_Dictionary($values_array) ; 

                if ($dictionary_test['status'] == 'fail') {
                    $this->Set_Alert_Response(10) ; // Potential dictionary attack; don't allow user to log in and block IP.
                    }
                }
            
            } else {
                $continue = 0 ;
                $this->Set_Alert_Response(1) ; // Email not in use
                }
                    
        
        // Check to see if the email address is verified
        if ($continue == 1) {
            if ($result['results']['email_verified'] == 0) {
                $continue = 0 ;
                $this->Set_Alert_Response(92) ; // Email not verified
                }            
            }
        
        // Check to see if the User is active.
        if ($continue == 1) {
            if ($result['results']['user_auth_id'] <= 3) {
                $continue = 0 ;
                $this->Set_Alert_Response(219) ; // User record not in active state.
                }            
            }
        
        if ($continue == 1) {
            
            // Password matched stored password in database
            // Session token created by hashing the email address 
            $session_token = $this->DB->Simple_Hash(array($this->user_id,TIMESTAMP));
            
            $_SESSION['user_id'] = $this->user_id;
            $_SESSION['master_user_id'] = $this->user_id;
            $_SESSION['session_token'] = $session_token;
            $_SESSION['last_login'] = TIMESTAMP ;
            $_SESSION["alert_id"] = 0 ; 
            $_SESSION["alert_message"] = '' ; 
            
            $query_array = array(
                'table' => 'users',
                'values' => array(
                    'last_login' => TIMESTAMP,
                    'session_token' => $session_token
                    ),
                'where' => "user_id='$this->user_id'"
                );
            
            $update_session_token = $this->DB->Query('UPDATE',$query_array);
            
            $result = array(
                'session_token' => $session_token
                );            
            
            $history_input = array(
                'function' => 'login'
                ) ; 
            $user_history = $this->Create_User_History($this->user_id,$history_input) ; 
                        
            } else {
                
                $result = $this->Get_Response() ; 
                $_SESSION["alert_id"] = $result['alert_id'] ;
                $_SESSION["alert_message"] = $result['alert_message_code']  ;
            
                $_SESSION['user_id'] = '';
                $_SESSION['master_user_id'] = '';
                $_SESSION['last_login'] = '';
                $_SESSION['version'] = '';
                $_SESSION['account_id'] = '';
                $_SESSION['session_token'] = '' ;  
            
                $this->user_id = '' ; 
                }

            $result = array(
                'user_id' => $_SESSION['user_id'],
                'session_token' => $_SESSION['session_token'],
                'last_login' => $_SESSION['last_login'],
                'alert_id' => $alert['alert_id'],
                'alert_message' => $_SESSION["alert_message"]
                );

            return $result;        
        } 
    
    
    public function Login_Verify($user_id,$submitted_session_token) {

        
        global $server_config ; 
        
        $earliest_login_allowed = TIMESTAMP - (60 * 60 * 24 * $server_config['session_days']) ; 
        
        $user = '';

        if ($user_id > 0) {
            $query_array = array(
                'table' => 'users',
                'fields' => "*",
                'where' => "users.user_id='$user_id'"
                );
       
            $result = $this->DB->Query('SELECT',$query_array);

            $last_login = $result['results']['last_login'];
            $stored_session_token = $result['results']['session_token'];

            $verify_session_token = $this->DB->Simple_Hash(array($user_id,$last_login));
            
            }
        
        $tokens = array(
            'submitted_session_token' => $submitted_session_token,
            'verify_session_token' => $verify_session_token,
            'stored_session_token' => $stored_session_token,
            'user_id' => $user_id,
            'last_login_in_db' => $last_login
            ) ; 
        

        if (($submitted_session_token != null) AND ($verify_session_token == $submitted_session_token) AND ($stored_session_token == $submitted_session_token)) {
            
            $set_user = $this->Set_User_By_ID($user_id) ; 
                
            if ($last_login > $earliest_login_allowed) {
                
                // Re-up the login
                $verified = 'YES';
                $_SESSION['user_id'] = $user_id;
                if (!$_SESSION['master_user_id']) {
                    $_SESSION['master_user_id'] = $user_id ; 
                    } ;
                $_SESSION['session_token'] = $submitted_session_token;
                $_SESSION["alert_id"] = 0 ; 
                $_SESSION["alert_message"] = '' ; 
                
                } else {
                
                    // Logout
                    $verified = 'NO';
                    $_SESSION['user_id'] = '';
                    $_SESSION['master_user_id'] = '';
                    $_SESSION['session_token'] = '';

                    $this->Set_Alert_Response(4) ; // Session expired
                    $result = $this->Get_Response() ; 
                    
                    $_SESSION["alert_id"] = $result['alert_id'] ;
                    $_SESSION["alert_message"] = $result['alert_message_clean']  ;
                    }
                        
            } else {
            
                // Logout
                $verified = 'NO';
                $_SESSION['user_id'] = '';
                $_SESSION['master_user_id'] = '';
                $_SESSION['session_token'] = '';
                
                $this->alert_id = 4 ; 
                $alert = Utilities::System_Alert($this->alert_id) ;
                $_SESSION["alert_id"] = $this->alert_id ;
                $_SESSION["alert_message"] = $alert['alert_message_clean']  ;
            
//                $history_input = array(
//                    'function' => 'logout'
//                    ) ; 
//                $user_history = $this->Create_User_History($user_id,$history_input) ;
            
            
                }

        $result = array(
            'user_id' => $user_id,
            'session_token' => $submitted_session_token,
            'last_login' => $last_login,
            'login_verified' => $verified,
            'alert_id' => $_SESSION["alert_id"],
            'alert_message' => $_SESSION["alert_message"],
            'tokens' => $tokens
            );
        
        return $result;
        
        }
    
    
    public function Logout() {

        $history_input = array(
            'function' => 'logout'
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ; 
        
        $this->Set_Alert_Response(3) ; // Session logout
        $result = $this->Get_Response() ; 
        
        $_SESSION["alert_id"] = $result['alert_id'] ;
        $_SESSION["alert_message"] = $result['alert_message_clean']  ;
        
        $_SESSION['user_id'] = '';
        $_SESSION['master_user_id'] = '';
        $_SESSION['last_login'] = '';
        $_SESSION['session_token'] = '';

        $query_array = array(
            'table' => 'users',
            'values' => array(
                'session_token' => 'logged_out'
                ),
            'where' => "user_id='$this->user_id'"
            );
            
        $update_session_token = $this->DB->Query('UPDATE',$query_array);
        
        $result = array(
            'user_id' => $_SESSION['user_id'],
            'last_login' => $_SESSION['last_login']
            );
        
        return $result;
        
        }    
    
    
    
    //////////////////////
    //                  //
    // DAL OPERATIONS   //
    //                  //
    //////////////////////    

    
    public function Retrieve_Phone_Number($query_options = array()) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        switch ($query_options->phone_owner) {
            case 'contact': // To find a true duplicate on this you must submit a user_id
                
                $query_array = array(
                    'table' => "contacts",
                    'field' => "contacts.contact_id, contacts.user_id, contacts.account_id, 
                        contacts.phone_country_dialing_code, contacts.phone_number",
                    'where' => "contacts.phone_number='$query_options->phone_number' 
                        AND contacts.phone_country_dialing_code='$query_options->phone_country_dialing_code'"
                    );

                if (isset($query_options->user_id)) {
                    $query_array['where'] .= " AND contacts.user_id='$query_options->user_id'" ; 
                    }
                
                break ; 
            case 'user':
            default:    
                
                $query_array = array(
                    'table' => "user_phone_numbers",
                    'field' => "user_phone_numbers.*",
                    'where' => "user_phone_numbers.phone_number='$query_options->phone_number' 
                        AND user_phone_numbers.phone_country_dialing_code='$query_options->phone_country_dialing_code'"
                    );

                if (isset($query_options->user_id)) {
                    $query_array['where'] .= " AND user_phone_numbers.user_id='$query_options->user_id'" ; 
                    }                
            }
        
        
        
        $result = $this->DB->Query('SELECT',$query_array,'force') ;
        
        return $result ;     
        }
    

    
    
    
    public function Create_Phone_Number($query_options) {
        
         
        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            
            $values_array[$key] = $value ; 
            
            }
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write

        $query_array = array(
            'table' => "user_phone_numbers",
            'values' => $values_array,
            'where' => "user_phone_numbers.phone_number='$query_options->phone_number' AND user_phone_numbers.phone_country_dialing_code='$query_options->phone_country_dialing_code'"
            );

        
        $phone_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
        $this->user_query_result = $result ;
        
        if ($phone_record['insert_id']) {
        
//            $history_input = array(
//                'email_address_id' => $email_record['insert_id'],
//                'function' => 'create_email',
//                'notes' => json_encode($query_array['values'])
//                ) ; 
//            
//            $user_history = $this->Create_User_History($this->user_id,$history_input) ;
                        
            }  
        
        return $phone_record ; 
        
        }
    
    
    
    public function Retrieve_Permission_List($permission_id_array = array(),$options = array()) {
        
        
        $query_array = array(
            'table' => 'system_auth_operations',
            'join_tables' => array(),
            'fields' => "system_auth_operations.*, 
                system_auth_permissions.*,
                system_features.*,
                system_feature_sets.*",
            'where' => "system_auth_permissions.permission_id>'0'"
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_auth_permissions',
            'on' => 'system_auth_permissions.permission_id',
            'match' => 'system_auth_operations.permission_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_features',
            'on' => 'system_features.feature_id',
            'match' => 'system_auth_operations.feature_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_feature_sets',
            'on' => 'system_feature_sets.feature_set_id',
            'match' => 'system_features.feature_set_id'
            );
        
        
        $permission_id_query_string = '' ; 
        if (count($permission_id_array) > 0) {
            foreach ($permission_id_array as $permission_id) {
                $permission_id_query_string .= "system_auth_permissions.permission_id='$permission_id' OR " ;             
                }
            $permission_id_query_string = rtrim($permission_id_query_string," OR ") ;  
            $query_array['where'] .= "AND ($permission_id_query_string)" ;
            } else {
//                $query_array['where'] .= "AND ($permission_id_query_string)" ;
                }
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->user_query_result  = $result ; 
        return $result ;          
        }
    
    
    
    public function Retrieve_Asset_Permission_List($permission_id_array = array(),$options = array()) {
        
        
        $query_array = array(
            'table' => 'system_asset_auth_operations',
            'join_tables' => array(),
            'fields' => "system_asset_auth_operations.*, 
                system_asset_auth_permissions.*,
                system_asset_features.*,
                system_asset_feature_sets.*",
            'where' => ""
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_asset_auth_permissions',
            'on' => 'system_asset_auth_permissions.asset_permission_id',
            'match' => 'system_asset_auth_operations.asset_permission_id'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_asset_features',
            'on' => 'system_asset_features.asset_feature_id',
            'match' => 'system_asset_auth_operations.asset_feature_id'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'system_asset_feature_sets',
            'on' => 'system_asset_feature_sets.asset_feature_set_id',
            'match' => 'system_asset_features.asset_feature_set_id'
            );
        
        
        $permission_id_query_string = '' ; 
        if (count($permission_id_array) > 0) {
            foreach ($permission_id_array as $permission_id) {
                $permission_id_query_string .= "system_asset_auth_permissions.asset_permission_id='$permission_id' OR " ;             
                }
            $permission_id_query_string = rtrim($permission_id_query_string," OR ") ;  
            $query_array['where'] .= "($permission_id_query_string)" ;
            } else {
//                $query_array['where'] .= "AND ($permission_id_query_string)" ;
                }
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        $this->user_query_result  = $result ; 
        return $result ;         
        }    
    
    
    public function Update_Phone_Number($query_options) {
        
         
        
        $values_array = array() ; 
        foreach ($query_options as $key => $value) {
            
            $values_array[$key] = $value ; 
            
            }
            
        $query_array = array(
            'table' => 'user_phone_numbers',
            'values' => $values_array,
            'where' => "user_phone_numbers.phone_number_id='$this->phone_number_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array) ;
        $this->user_query_result = $result ;
        
        
//        $history_input = array(
//            'email_address_id' => $this->email_address_id,
//            'function' => 'update_email',
//            'notes' => json_encode($values_array)
//            ) ; 
//        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        
        return $result ;
        
        }
    
    
    
    public function Retrieve_Email_Address($email_address) {
        
         
        
        $query_array = array(
            'table' => "user_emails",
            'field' => "user_emails.*",
            'where' => "user_emails.email_address='$email_address'"
            );

        $result = $this->DB->Query('SELECT',$query_array) ;
        
        return $result ;     
        }
    
    
    public function Create_Email_Address($new_email_address,$email_options) {
        
        if (!isset($email_options['password_selection'])) {
            $email_options['password_selection'] = 'primary' ; 
            }
        
         
        
        $query_array = array(
            'table' => "user_emails",
            'values' => array(
                'user_id' => $this->user_id,
                'email_address' => $new_email_address,
                'email_verified' => 0
                ),
            'where' => "user_emails.email_address='$new_email_address'"
            );

        $email_record = $this->DB->Query('SELECT_ELSE_INSERT',$query_array) ;
        
        
        if ($email_record['insert_id']) {
        
            $history_input = array(
                'email_address_id' => $email_record['insert_id'],
                'function' => 'create_email',
                'notes' => json_encode($query_array['values'])
                ) ; 
            
            $user_history = $this->Create_User_History($this->user_id,$history_input) ;
            
            
            // Set a password associated w/ the email
            switch ($email_options['password_selection']) {
                case 'new': // If new email_address_id record was created, then copy the password associated with the current_email_address_id to the new one
                    $add_password = $this->Action_Update_Password($email_options) ; 
                    break ;
                    
                case 'primary':
                default: // Should be set as an email_address_id
                    
                    if ($email_options['password_selection'] == 'primary') {
                        $selected_email_address_id = $this->user['email_address_id'] ; // Will retrieve the password for the current primary email address
                        } else {
                            $selected_email_address_id =  $email_options['password_selection'] ; // Option to pass in an email_address_id within options array to retrieve a password
                            } 
                    
                    $result = $this->Set_Email_Address_ID($selected_email_address_id)->Retrieve_User_Email_Password_By_ID() ; 
                    
                    $new_hashed_password = $result['results']['password'] ;  
                    
                    $new_password_input = array(
                        'email_address_id' => $email_record['insert_id'],
                        'password' => $new_hashed_password
                        ) ; 
                    
                    $add_password = $this->Set_Email_Address_ID($email_record['insert_id'])->Create_Password($new_password_input) ; 
                }            
            }  
        
        return $email_record ; 
        
        }
    
    
    // Create a new password associated with an email id
    // If a password already exists for the email_address_id, update the existing password    
    // Hash the password before running this method
    public function Create_Password($input_array) {
        
        $email_address_id = $input_array['email_address_id'] ; 
        
        $values_array = array(
            'password' => $input_array['password'],
            'email_address_id' => $email_address_id
            ) ;

        // Updates the password associated with email_address_id
        $query_array = array(
            'table' => 'user_secure',
            'values' => $values_array,
            'where' => "email_address_id='$email_address_id'"
            );

        $result = $this->DB->Query('UPDATE_ELSE_INSERT',$query_array); 
        
        $history_input = array(
            'email_address_id' => $email_address_id,
            'function' => 'create_password',
            'notes' => json_encode($values_array)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ; 
        
        return $result ;
        }
    
    
    
    public function Create_User($user_input) {
        
         
        
        $values_array = array() ; 
        foreach ($user_input as $key => $value) {            
            $values_array[$key] = $value ;             
            }

        $values_array['timestamp_created'] = TIMESTAMP ; 
        $values_array['timestamp_updated'] = TIMESTAMP ; 
        
        $query_array = array(
            'table' => 'users',
            'values' => $values_array
            );
        
        $result = $this->DB->Query('INSERT',$query_array) ;
        $this->user_query_result = $result ;
        

        if (!isset($result['error'])) {
            
            // Create History Entry
            $history_input = array(
                'user_id' => $result['insert_id'],
                'function' => SCRIPT_ACTION,
                'notes' => json_encode($result)
                ) ; 
            $user_history = $this->Create_User_History($result['insert_id'],$history_input) ;        
            }
        
        return $result ; 
        
        }
    
    
    public function Retrieve_Action($action_id) {
        
         
        
        $query_array = array(
            'table' => 'system_actions',
            'fields' => "*",
            'where' => "system_actions.action_id='$action_id'"
            );

        
        $result = $this->DB->Query('SELECT',$query_array) ;
        return $result ; 
        
        }
    
    public function Retrieve_Action_Key($action_key_id) {
        
         
        
        $query_array = array(
            'table' => 'system_action_keys',
            'fields' => "*",
            'where' => "system_action_keys.action_key_id='$action_key_id'"
            );

        
        $result = $this->DB->Query('SELECT',$query_array) ;
        return $result ; 
        
        }
    
    public function Retrieve_User($user_id) {
        
        //  
        
        $query_array = array(
            'table' => 'users',
            'join_tables' => array(),
            'fields' => "users.*, 
                CONCAT(users.first_name,' ',users.last_name) AS full_name, 
                system_auth_roles.*, 
                user_phone_numbers.phone_country_dialing_code, user_phone_numbers.phone_number, user_phone_numbers.phone_verified, 
                user_emails.email_address, user_emails.email_verified, 
                list_countries.country_value, list_countries.country_dialing_code, 
                user_registration.registration_token",
            'where' => "users.user_id='$user_id'"
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'system_auth_roles',
            'on' => 'system_auth_roles.auth_id',
            'match' => 'users.user_auth_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_emails',
            'on' => 'users.email_address_id',
            'match' => 'user_emails.email_address_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'user_phone_numbers',
            'on' => 'users.phone_number_id',
            'match' => 'user_phone_numbers.phone_number_id',
            'type' => 'left'
            );
        
        $query_array['join_tables'][] = array(
            'table' => 'list_countries',
            'on' => 'users.country_id',
            'match' => 'list_countries.country_id',
            'type' => 'left'
            );        
        
        $query_array['join_tables'][] = array(
            'table' => 'user_registration',
            'on' => 'users.user_id',
            'match' => 'user_registration.user_id',
            'type' => 'left'
            );        
        
        $result = $this->DB->Query('SELECT_JOIN',$query_array);
        $this->user_query_result  = $result ; 
        return $result ; 
        
        }
    
    
    public function Retrieve_User_Email_Password_By_ID() {

        $query_array = array(
            'table' => 'user_secure',
            'fields' => "*",
            'where' => "email_address_id='$this->email_address_id'"
            );

        $result = $this->DB->Query('SELECT',$query_array);
        $this->user_query_result = $result ;
        
        return $result ; 
        
        }
    
    
    public function Retrieve_User_Email($email_address) {
        
         
        
        $query_array = array(
            'table' => 'user_emails',
            'fields' => "user_emails.*",
            'where' => "user_emails.email_address='$email_address'"
            );

        
        $result = $this->DB->Query('SELECT',$query_array) ;
        $this->user_query_result = $result ;
        return $result ; 
        
        }    
    
    
    public function Retrieve_User_Email_By_ID() {
        
         
        
        $query_array = array(
            'table' => 'user_emails',
            'fields' => "user_emails.*",
            'where' => "user_emails.email_address_id='$this->email_address_id'"
            );

        
        $result = $this->DB->Query('SELECT',$query_array) ;
        $this->user_query_result = $result ;
        return $result ; 
        
        }
    
    public function Retrieve_User_Email_List($verified) {
        
        $query_options = array() ; 
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
         
        
        $query_array = array(
            'table' => 'user_emails',
            'fields' => "user_emails.*",
            'where' => "user_emails.user_id='$this->user_id'"
            );

        $query_array['join_tables'][] = array(
            'table' => 'users',
            'on' => 'users.user_id',
            'match' => 'user_emails.user_id'
            ); 
                
        switch ($verified) {
            case 'verified':
                $query_array['where'] .= " AND user_emails.email_verified='1'" ;
                break ;
            case 'unverified':
                $query_array['where'] .= " AND user_emails.email_verified='0'" ; 
                break ;
            case 'both':
            default:
                $query_array['where'] .= " AND (user_emails.email_verified='0' OR user_emails.email_verified='1')" ;
            }
        
    
        
        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $query_options->value_name = 'email_address_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_User_Paging($result) ;         
        $this->user_query_result = $result ;

        return $result ;        
        }

    
    public function Retrieve_User_Phone_Number_List($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
         
        
        $query_array = array(
            'table' => 'user_phone_numbers',
            'fields' => "user_phone_numbers.*",
            'where' => "user_phone_numbers.user_id='$this->user_id'",
            'group_by' => "user_phone_numbers.phone_number_id"
            );

        $query_array['join_tables'][] = array(
            'table' => 'users',
            'on' => 'users.user_id',
            'match' => 'user_phone_numbers.user_id'
            ); 
                
        switch ($query_options->verified) {
            case 'verified':
                $query_array['where'] .= " AND user_phone_numbers.phone_verified='1'" ;
                break ;
            case 'unverified':
                $query_array['where'] .= " AND user_phone_numbers.phone_verified='0'" ; 
                break ;
            case 'both':
            default:
                $query_array['where'] .= " AND (user_phone_numbers.phone_verified='0' OR user_phone_numbers.phone_verified='1')" ;
            }
        
     

        
        // NEW PAGING PROCESS
        // Add paging constraints
        $paging_constraints = $this->Action_Process_Paging_Constraints($query_array,$query_options,$this->page_increment) ; 

        // Pull visible results
        $result = $this->DB->Query('SELECT_JOIN',$query_array,'force');
        
        $query_options->value_name = 'phone_number_id' ;
        $result = $this->Action_Process_Query_Results($result,$query_options,$paging_constraints) ; 

        $this->Set_User_Paging($result) ;         
        $this->user_query_result = $result ;
        
        
        return $result ;        
        }

    
    
    
    public function Retrieve_User_History($query_options) {
        
        $query_options = (object) $query_options ; // Cast the options array as an object to make queries easier to write
            
        
         
        
        $query_array = array(
            'table' => 'system_log_app_history',
            'fields' => "system_log_app_history.*",
            'where' => "system_log_app_history.history_id>0 ",
            'order_by' => "system_log_app_history.timestamp_created",
            'order' => "DESC"
            );

        if (isset($query_options->user_id)) {
            $query_array['where'] .= "system_log_app_history.user_id='$query_options->user_id' " ;
            }
        
        if (isset($query_options->account_id)) {
            $query_array['where'] .= "AND system_log_app_history.account_id='$query_options->account_id' " ;
            }
        
        if (isset($query_options->contact_id)) {
            $query_array['where'] .= "AND system_log_app_history.contact_id='$query_options->contact_id' " ;
            }
        
        switch ($query_options->error) {
            case 'yes':
                $query_array['where'] .= "AND system_log_app_history.error='1' " ;
                break ;
            case 'all':
                $query_array['where'] .= "AND (system_log_app_history.error='0' OR system_log_app_history.error='1') " ;
                break ; 
            case 'no':                
            default:
                $query_array['where'] .= "AND system_log_app_history.error='0' " ;
                break ;                
            }

        if ($query_options->filter_by_search_parameter) {
            $query_array['where'] .= "AND (system_log_app_history.function LIKE '%$query_options->filter_by_search_parameter%' OR 
                system_log_app_history.notes LIKE '%$query_options->filter_by_search_parameter%') " ; 
            }
        
        

        // Add paging constraints
        if ($query_options->override_paging == 'yes') {
            $query_options->offset_page = 0 ; 
            } else {
                $query_array['limit'] = $this->page_increment ; 
                if (isset($query_options->start_page)) {
                    $query_options->offset_page = Utilities::Start_Page_To_Offset($query_options->start_page) ; 
                    $query_array['offset'] = $query_options->offset_page * $this->page_increment ; 
                    } else {
                        $query_options->offset_page = 0 ; 
                        $query_array['offset'] = $query_options->offset_page ; 
                        }
                }
        
        
        $result = $this->DB->Query('SELECT',$query_array,'force') ;
        $this->user_query_result = $result ; 
        
        
        // Get the total count of records in this query
        if (isset($query_options->total_count)) { 
            
            $result['total_count'] = $query_options->total_count ; 
            
            } else {                
                    
                $query_array['fields'] = "COUNT(system_log_app_history.history_id) as total_count" ; 
                unset($query_array['limit'],$query_array['offset']) ;
            
                $count = $this->DB->Query('SELECT',$query_array) ;
                $result['total_count'] = $count['results']['total_count'] ;

                }
        
        
        $result['offset_page'] = $query_options->offset_page ; 
        
        if (isset($query_options->url_hash)) {
            $result['url_hash'] = $query_options->url_hash ; 
            }
        $this->Set_User_Paging($result) ;        

        return $result ;         
        }
    
    
    public function Update_User($input) {
        
         
        
        $values_array = array() ; 
        foreach ($input as $key => $value) {
            
            $values_array[$key] = $value ; 
            
            }
            
        // Updates the new account account in the database
        $query_array = array(
            'table' => 'users',
            'values' => $values_array,
            'where' => "user_id='$this->user_id'"
            );
            
        $query_array['values']['timestamp_updated'] = TIMESTAMP ; 
            
        $result = $this->DB->Query('UPDATE',$query_array) ;
        $this->user_query_result = $result ;
        
        $history_input = array(
            'function' => 'update_user',
            'notes' => json_encode($values_array)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        return $result ; 
        
        }   
 
    
    public function Update_Email($input) {
        
         
        
        $values_array = array() ; 
        foreach ($input as $key => $value) {
            
            $values_array[$key] = $value ; 
            
            }
            
        $query_array = array(
            'table' => 'user_emails',
            'values' => $values_array,
            'where' => "email_address_id='$this->email_address_id'"
            );
            
        $result = $this->DB->Query('UPDATE',$query_array) ;
        $this->user_query_result = $result ;
        
        
        $history_input = array(
            'email_address_id' => $this->email_address_id,
            'function' => 'update_email',
            'notes' => json_encode($values_array)
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;         
        
        
        return $result ;
        
        }
    
    public function Delete_Email($email_address_id) {
        
         
        
        $query_array = array(
            'table' => "user_emails",
            'where' => "user_emails.email_address_id='$email_address_id'"
            );
        
        $result = $this->DB->Query('DELETE',$query_array);
            
        $this->user_query_result = $result ;
        
        

        $history_input = array(
            'email_address_id' => $email_address_id,
            'function' => 'delete_email'
            ) ; 
        $user_history = $this->Create_User_History($this->user_id,$history_input) ;
        
        return $result ;
        
        }
    
    }