<?php
namespace Ragnar\Ironsides;

class Ironsides {
    
    // Return the launch string for Ironsides
    public function Launch() {

        // In index.php you need to require_once(__DIR__.'/config/Config.php') ; 
        $launch_string = __DIR__.'/../config/Config.php' ; 
        return $launch_string ; 

        } 
    }
