<?php

	// Session is started, and cookie length is defined at the top of index.php

    if (!isset($_SESSION['session_token'])) {
        $_SESSION['session_token'] = '' ;
        }
    if (!isset($_SESSION['session_public_id'])) {
        $_SESSION['session_public_id'] =  $DB->Simple_Hash(array(rand(),MICRO_TIMESTAMP));
        }
    define('SESSION_PUBLIC_ID', $_SESSION['session_public_id']);