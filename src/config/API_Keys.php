<?php

    $query_array = array(
            'table' => 'system_keys',
            'fields' => "*"
            );
        
    $server_config['api_keys'] = $DB->Query('SELECT',$query_array)['results'];

    // STRIPE API KEY
//    \Stripe\Stripe::setApiKey(Utilities::Deep_Array($server_config['api_keys'],'key_name','Stripe_Secret_Key')['key_value']);
//    

    $stripe_secret_key = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','Stripe_Secret_Key')['key_value'] ; 
    define('STRIPE_SECRET_KEY', $stripe_secret_key);

    $stripe_secret_key_test = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','Stripe_Secret_Key_Test')['key_value'] ; 
    define('STRIPE_SECRET_KEY_TEST', $stripe_secret_key_test);

    $stripe_public_key = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','Stripe_Public_Key')['key_value'];
    $stripe_public_key_test = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','Stripe_Public_Key_Test')['key_value'];


    // UPLOADCARE - Image Processing
    $uploadcare_public_key = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','uploadcare_public_key')['key_value'];
    $uploadcare_secret_key = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','uploadcare_secret_key')['key_value'];
    

    // EMAIL - SMS Messaging
    $mailgun_api_key = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','Mailgun_API_Key')['key_value'];
    define('MAILGUN_API_KEY', $mailgun_api_key);


    // TWILIO - Messaging
    $twilio_account_sid = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','Twilio_Account_SID')['key_value'];
    $twilio_auth_token = Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','Twilio_Auth_Token')['key_value'];
    define('TWILIO_ACCOUNT_SID', $twilio_account_sid);
    define('TWILIO_AUTH_TOKEN', $twilio_auth_token);


    // Internal System API hashed
    $internal_app_api_key = DAL::Simple_Hash(Ragnar\Ironsides\Utilities::Deep_Array($server_config['api_keys'],'key_name','Internal_App_API_Key')['key_value']);
    define('API_KEY_INTERNAL_APP_API_KEY', $internal_app_api_key);
