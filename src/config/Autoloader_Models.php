<?php
/**
 * Example 2: Using a string as the single parameter for `spl_autoload_register`
 *
 * We need to define a function with the same name as the string we provide
 *
 * @see	http://php.net/manual/en/language.types.callable.php
 */
/**
 * This function has the same name as the string we passed in above, and acts 
 * exactly the same as the anonymous function from Example 1.
 */
function Model_Class_Autoload( $class_name ) {
	/**
	 * Note that actual usage may require some string operations to specify the filename
	 */
    
    global $REGISTER ; 
	$file_name = $class_name . '.php';
    $full_folder = MODELS_PATH.'/'.$file_name ; 
    $full_folder = str_replace("\\", DIRECTORY_SEPARATOR, $full_folder); // Adapt for namespace slashes    
	if( file_exists( $full_folder ) ) {
		require $full_folder ;
	}
    
}

/**
 * ... anywhere in our code, we can now get a new My_Class object without caring whether
 * the class's file has been required or not
 * 
 * Note in this case we need to have a file called `My_Class.php` in the same directory as this script.
 */


function Package_Class_Autoload( $class_name ) {
	/**
	 * Note that actual usage may require some string operations to specify the filename
	 */
    
    $class_array = explode('\\',$class_name) ;

    global $REGISTER ; 
	$file_name = end($class_array) . '.php';
    $full_folder = __DIR__.'/../'.VERSION_PATH.'/models/'.$file_name ; 
    if( file_exists( $full_folder ) ) {
		require $full_folder ;
	} 
}