<?php

/*
 * ---------------------------------------------------------------
 *  SERVER CONFIG SETTINGS
 * ---------------------------------------------------------------
 */
    

    $server_config['session_days'] = 30 ; 
    $server_config['maintenance'] = 'no' ;  
    
    $data['version'] = $version ;



/*
 * ---------------------------------------------------------------
 *  LOAD SYSTEM CONFIG CLASSES
 * ---------------------------------------------------------------
 */
    // Load the system configuration classes
    require_once 'System_Config.php' ; 

    // Parse the incoming url to configure rest of system 
    $config_url_parsed = System_Config::Config_Parse_URL($_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]) ;     
    $config_url_queries = $config_url_parsed['queries'] ;
//    print_r($config_url_parsed) ;



/*
 *---------------------------------------------------------------
 * LAUNCH DATABASE
 *---------------------------------------------------------------
 *
 *
 * NO TRAILING SLASH!
 */


    // Determine live or test version of database & billing system
    switch ($config_url_queries["system_mode"]) {
        case 'system_test':
            $version = System_Config::Create_Version_Path() ; 
            if ($config_url_queries["verify"] == $version['version_test_key']) {
                $_SESSION['system_mode'] = 'system_test' ; 
                } else {
                    $_SESSION['system_mode'] = 'system_live' ;  
                    }
            break ;
        case 'system_live': 
            $_SESSION['system_mode'] = 'system_live' ; 
        }

    switch ($config_url_parsed['tld']) {
        case 'test':
        case 'dev':
        case 'local':            
            $_SESSION['system_mode'] = 'system_test' ; 
            $_SESSION['url_mode'] = 'system_test' ; 
            break ;
        default:
            $_SESSION['system_mode'] = 'system_live' ; 
            $_SESSION['url_mode'] = 'system_live' ; 
        }

    if (!isset($_SESSION['system_mode'])) {
        $_SESSION['system_mode'] = 'system_live' ;
//        $_SESSION['system_mode'] = 'system_test' ;         
        }
    
    if (isset($_POST['system_mode'])) {
        $_SESSION['system_mode'] = $_POST['system_mode'] ; 
        }


//    define('BILLING_MODE',$_SESSION['system_mode']) ;
    $temporary_billing_mode = $_SESSION['system_mode'] ; 
    define('DATABASE_MODE',$_SESSION['system_mode']) ;
    define('URL_MODE',$_SESSION['url_mode']) ;


    // Instantiate database
    require_once 'Database.php' ; 
    $DB = DAL::getInstance();



/*
 *---------------------------------------------------------------
 * SET VERSION
 *---------------------------------------------------------------
 *
 *
 * NO TRAILING SLASH!
 */
 
    $app_recent_stable_result = System_Config::Test_Version(999999999999.99) ;

    // Look for a version_path passed in a URL segment (should probably only be used for webhook testing)
    foreach ($config_url_parsed['segments'] as $segment) {
        $segment .= '/' ;
        $version_extract_result = System_Config::Extract_Text_Block_System($segment, 'version_path_', '/') ; 
        if ($version_extract_result['position'] === 0) { // If strpos finds a segment match beginning w/ version_path_ the result will be 0, otherwise blank
            $version_path_input = $version_extract_result['extraction'] ; 
            }
        }

    switch ($config_url_parsed['tld']) {
        case 'test':
        case 'dev':
        case 'local':              
            $app_recent_stable = 0.00 ;  // local dev version    
            $_SESSION['version'] = $app_recent_stable ; 
            $_SESSION['system_mode'] = 'system_test' ;
            unset($config_url_queries["avs"]) ; 
            break ;
        default:
            $app_recent_stable = $app_recent_stable_result['results']['version'] ;  // live site version    
        }


    // Parse the app version
    if ($_POST['version_path'] OR $version_path_input) {
        
        if ($_POST['version_path']) {
            $version_path_input = $_POST['version_path'] ; 
            } 

        $version_input = System_Config::Convert_Version_Path($version_path_input) ; 
        $version = System_Config::Create_Version_Path($version_input) ; 

        } elseif ($config_url_queries["avs"]) {  // Get app version (avs) from the URL. Passed as decimal x.xx
            switch ($config_url_queries["avs"]) {
                case 'dev':
                    $config_url_queries["avs"] = 0.00 ; // Force to the development site
                    break ;
                case 'test':
                    $config_url_queries["avs"] = 0.50 ; // Force to the testing site
                    break ;
                case 'stable':
                    $config_url_queries["avs"] = $app_recent_stable ;  // Force to the most recent stable version
                    break ;                    
                }
            $version_input = System_Config::Convert_Version_Path($config_url_queries["avs"]) ; 
            $version = System_Config::Create_Version_Path($version_input) ; 
            
        } elseif ($_SESSION['version']) {
            $version = System_Config::Create_Version_Path($_SESSION['version']) ; 
            
        } else {
            $version = System_Config::Create_Version_Path($app_recent_stable) ; 
            }

    $version_path = $version['version_path'] ; 
    $_SESSION['version'] = $version['version']['version'] ;



    // TEST VERSION ID
    // Make sure the version is valid. If not, dump out...
    $app_version = System_Config::Test_Version($_SESSION['version'])['results'] ; 

    $system_version_test = new System_Config();
    $version_test = $system_version_test->Test_Version($_SESSION['version']) ;
    define('VERSION_PATH',$version_path) ;


/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 */






    if ($version_test['result_count'] == 1) {
        
        // AUTOLOAD THE PACKAGE CORE MODELS
        require_once 'Autoloader_Models.php' ; // Loads the autoloader classes
        spl_autoload_register( 'Package_Class_Autoload' ); // Loads the classes associated with this PHP Package


        // Set system domains
        
        // Get the current domain
        $system = new Ragnar\Ironsides\System() ; 

        $query_options = array(
            'hostname' => $public_home_host
            ) ;

        $system->Set_System_List('list_domains',$query_options) ; 
        $server_config['domain_query'] = $system->system_query_result ; 
        $server_config['domain']['active_domain'] = $system->Get_System_List('list_domains')[0] ; 


        // Get default app domain
        $query_options = array(
            'app_domain' => 1,
            'default_domain' => 1
            ) ;

        switch (URL_MODE) {
            case 'system_live':
                $query_options['url_live'] = 1 ; 
                break ;
            case 'system_test':
                $query_options['url_live'] = 0 ;  
                break ;
            }

        $system->Set_System_List('list_domains',$query_options) ; 

        $server_config['domain']['app_domain'] = $system->Get_System_List('list_domains')[0] ; // Default app domain (depenedent on live or test)

        
        // Get default account public domain
        $query_options = array(
            'account_public_domain' => 1,
            'default_domain' => 1
            ) ;

        switch (URL_MODE) {
            case 'system_live':
                $query_options['url_live'] = 1 ; 
                break ;
            case 'system_test':
                $query_options['url_live'] = 0 ;  
                break ;
            }

        $system->Set_System_List('list_domains',$query_options) ; 

        $server_config['domain']['account_public_domain'] = $system->Get_System_List('list_domains')[0] ; // Default account public domain (depenedent on live or test)
            
        $system->Set_System_List('list_defaults') ; 
        $server_config['system_defaults'] = $system->Get_System_List('list_defaults') ;         
        
        

        
        // API KEYS
        require_once 'API_Keys.php' ; // 

        
        // Parse the URL...
        $server_config['page_url_array'] = System_Config::Config_Parse_URL($_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]) ;
        
        
        
        
        
        
        } else {
            // If $version_test['result_count'] != 1 ... 
        
            $api_result = array(
                'http_response_code' => http_response_code(), // PHP 5.4 or greater
                'message' => 'Invalid request. System version could not be verified.',
                'environment' => ENVIRONMENT,
                'version' => $_SESSION['version'] 
                );
        
            echo json_encode($api_result);
        
            }


 