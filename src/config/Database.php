<?php


    define("DB_USERNAME", $Server_Config['Database_Users'][0]['Username']);
    define("DB_PASSWORD", $Server_Config['Database_Users'][0]['Password']);
    switch (DATABASE_MODE) {
        case 'system_test':
            define("DB_HOST", $Server_Config['Database']['Test']['Host']);
            define("DB_NAME", $Server_Config['Database']['Test']['DB_Name']);
            break ; 
        default:   
            define("DB_HOST", $Server_Config['Database']['Production']['Host']);
            define("DB_NAME", $Server_Config['Database']['Production']['DB_Name']);
        }

    /*
    // Mysql database class - only one connection alowed
    // https://gist.github.com/jonashansen229/4534794
    // With help from https://codereview.stackexchange.com/questions/132842/php-mysql-connection-class
    // http://www.pontikis.net/blog/how-to-use-php-improved-mysqli-extension-and-why-you-should
    // READ THIS: http://www.phptherightway.com/
    */

class DAL {
	public $_connection;
	private static $_instance; //The single instance
    private $logs = array();
	private $_host = DB_HOST;
	private $_username = DB_USERNAME;
	private $_password = DB_PASSWORD;
	private $_database = DB_NAME;
    public $query;
    public $query_results ; 
    
	/*
	Get an instance of the Database
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
		}
        return self::$_instance;
	}
    
	// Constructor
	private function __construct() {
		$this->_connection = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);
	
		// Error handling
		if(mysqli_connect_error()) {
			trigger_error("Failed to conenct to MySQL: " . mysqli_connect_error(), E_USER_ERROR);
            } else {
                $this->logs[] = "A database connection has been established.";
                }
	   }
    
	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }
	
    // Get mysqli connection
	public function getConnection() {
        return $this->_connection;
	   }
    
    
    public function Query_Cleanup($string,$cleanup_type = 'operators') {
        
        switch ($cleanup_type) {
            case 'operators':
                
                
                break ;
            case 'commas': // Remove trailing commas
                $string = rtrim($string,", ") ;
                $string = rtrim($string,",") ;
                break ; 
            }
                
        return $string ; 
        }
    
    public function Query($query_type,$query_array,$force_result_array = 0) {
        
        $RUN_QUERY = 1 ; 
        
        switch($query_type) {
            case 'SELECT':
                
                // Example SELECT query_array
                // $query_array = array(
                //    'table' => "",
                //    'fields' => "*",
                //    'where' => "Email_Address=''",
                //    'order_by' => "",
                //    'order' => "ASC",
                //    'limit' => '10',
                //    'offset' => '2',
                //    'group_by' => 'email_address'
                //    );
                
                
                if (isset($query_array['table'])) {
                    
                    $table = $this->Get_Table($query_array['table']);
                    
                    } else {
                        $RUN_QUERY = 0 ;
                        }
                
                if (isset($query_array['fields'])) {
                    $fields = $this->Query_Cleanup($query_array['fields'],'commas') ;
                    } else {
                        $fields = '*';
                        }
                
                if (isset($query_array['where'])) {
                    $where = 'WHERE '.$query_array['where'];
                    } else {
                        $where = '';
                        }                
                
                if (isset($query_array['order_by'])) {

                    // Multiple order_by requests...
                    if (is_array($query_array['order_by'])) {
                        $order_statement = 'ORDER BY ' ; 
                        
                        $i = 0 ; 
                        foreach ($query_array['order_by'] as $order_set) {
                            
                            switch ($query_array['order'][$i]) {
                                case 'DESC':
                                case 'DESCENDING':
                                    $order = 'DESC';
                                    break ;
                                case 'ASC':
                                case 'ASCENDING':
                                default:
                                    $order = 'ASC';
                                }                            
                            
                            $order_statement .= $order_set.' '.$order.', ' ; 
                            $i++ ; 
                            }
                        
                        $order_statement = rtrim($order_statement,", ") ;                        
                        
                        } else {
                        
                            // Singular order_by request...
                            $order_by = $query_array['order_by'];
                            switch ($query_array['order']) {
                                case 'DESC':
                                case 'DESCENDING':
                                    $order_statement = 'ORDER BY '.$order_by.' DESC';
                                    break ;
                                case 'ASC':
                                case 'ASCENDING':
                                default:
                                    $order_statement = 'ORDER BY '.$order_by.' ASC';
                                }                        
                            }                    
                    
                    } else {
                        $order_statement = '' ; 
                        }
                
                if (isset($query_array['limit'])) {
                    $limit = 'LIMIT '.$query_array['limit'];
                    if ($query_array['offset']) {
                        $offset = 'OFFSET '.$query_array['offset'];
                        } else {
                            $offset = 'OFFSET 0';
                            }
                    } else {
                        $limit = '';
                        $offset = '';
                        }
                
                if (isset($query_array['group_by'])) {
                    $group_by = ' GROUP BY '.$query_array['group_by'];
                    } else {
                        $group_by = '';
                        }
                
                    $query = "SELECT $fields FROM $table $where $group_by $order_statement $limit $offset";
                    
                    if ($query_array['skip_query'] != 1) {                
                        $results = $this->_connection->query($query);
                        $result_count = $results->num_rows ;        

                        $result_array = array() ; 
                        if ($result_count > 0) {
                            while($item = mysqli_fetch_assoc($results)) {   
                                $result_array[] = $item ; 
                                }
                            }

                        if (($result_count == 1) AND ('force' !== $force_result_array)) {
                            $result_array = $result_array[0];
                            }
                        }
                
                
                    $query_results = array(
                        'result_count' => $result_count,
                        'results' => $result_array,
                        'query' => $query
                        );
                
                    if ($this->_connection->error) {
                        $query_results['error'] = $this->_connection->error ; 
                        }                
                
                    $this->query_results = $query_results['results'];
                    return $query_results ;
                
                break ;
                
            // SELECT_JOIN can run a join on a number of tables provided there is an array for 
            // the joining table in the Join_Tables key
            case 'SELECT_JOIN':
                
                // Example SELECT query_array
                // $query_array = array(
                //    'table' = > 'Table',
                //    'join_tables' => [0] => array(
                //            'table' => 'Table_Name_Here'
                //            'on' => 'Table_Name_Here.Key_Value',
                //            'match' => 'Other_Table_Here.Key_Value'
                //            ),
                //    'fields' => "*",
                //    'where' => "Email_Address=''",
                //    'order_by' => "",
                //    'order' => "ASC",
                //    'limit' => '10',
                //    'offset' => '2',
                //    'group_by' => 'Email_Address'
                //    );
                
                
                // Table is the primary table we're searching against
                if (isset($query_array['table'])) {
                    
                    $table = $this->Get_Table($query_array['table']);
                    
                    } else {
                        $RUN_QUERY = 0 ;
                        }
                
                
                // JOIN TABLES and MATCH VALUES
                // Start with a blank JOIN statement, and loop through each Join Table Array
                $join_statement = '' ; 
                $table_replace_array = array();
                if (isset($query_array['join_tables'])) {
                    
                    foreach ($query_array['join_tables'] as $this_join) {
                        
                        $join_table = $this_join['table']; // This is a shortname value
                        $join_statement_value = $this_join['statement']; // A conditional statment that overrides the on / match values if present
                        $join_on_value = $this_join['on']; // the table.key to join on 
                        $join_match_value = $this_join['match']; // and the table.key to match the join on against
                        $join_type = $this_join['type'] ; // Define as left, right, etc
                        switch ($this_join['type']) {
                            case 'left':
                                $join_type = "LEFT JOIN" ;
                                break ;
                            default:
                                $join_type = "JOIN" ;
                            }
                        
                        
                        $table_sql_name = $this->Get_Table($join_table); // Get the actual SQL name of the table. This will be used immediately in the JOIN statement
                        $table_replace_array[] = array('find' => $join_table.'.', 'replace' => $table_sql_name.'.'); // Create a find / replace array that will be looped through later on in the method to scrub out table shortcut names. Trailing period necessary.
                        
                        // Prep the JOIN statement for this table.
                        if (isset($this_join['statement'])) {
                            $join_statement .= "$join_type $table_sql_name ON $join_statement_value " ; 
                            } else {
                                $join_statement .= "$join_type $table_sql_name ON $join_on_value = $join_match_value " ;     
                                }
                        
                        
                        }
                    } 
                                
                if (isset($query_array['fields'])) {
                    $fields = $this->Query_Cleanup($query_array['fields'],'commas') ;
                    } else {
                        $fields = '*';
                        }
                
                if (isset($query_array['where'])) {
                    $where = 'WHERE '.$query_array['where'];
                    } else {
                        $where = '';
                        }                
                
                // Add in order_start and order_operator statements to where clause
                if (($query_array['order_field']) AND ($query_array['order_start']) AND ($query_array['order_operator'])) {
                    
                    $order_operator_statement = $query_array['order_field'].$query_array['order_operator'].$query_array['order_start'] ;
                    
                    if ($where) {
                        $where .= ' AND '.$order_operator_statement ; 
                        } else {
                            $where = 'WHERE '.$order_operator_statement ; 
                            }
                    }
                
                if (isset($query_array['order_by'])) {
                    
                    // Multiple order_by requests...
                    if (is_array($query_array['order_by'])) {
                        $order_statement = 'ORDER BY ' ; 
                        
                        $i = 0 ; 
                        foreach ($query_array['order_by'] as $order_set) {
                            
                            switch ($query_array['order'][$i]) {
                                case 'DESC':
                                case 'DESCENDING':
                                    $order = 'DESC';
                                    break ;
                                case 'ASC':
                                case 'ASCENDING':
                                default:
                                    $order = 'ASC';
                                }                            
                            
                            $order_statement .= $order_set.' '.$order.', ' ; 
                            $i++ ; 
                            }
                        
                        $order_statement = rtrim($order_statement,", ") ;                        
                        
                        } else {
                        
                            // Singular order_by request...
                            $order_by = $query_array['order_by'];
                            switch ($query_array['order']) {
                                case 'DESC':
                                case 'DESCENDING':
                                    $order_statement = 'ORDER BY '.$order_by.' DESC';
                                    break ;
                                case 'ASC':
                                case 'ASCENDING':
                                default:
                                    $order_statement = 'ORDER BY '.$order_by.' ASC';
                                }                        
                            }
                    } else {
                        $order_statement = '' ; 
                        }
                
                if (isset($query_array['limit'])) {
                    $limit = 'LIMIT '.$query_array['limit'];
                    if ($query_array['offset']) {
                        $offset = 'OFFSET '.$query_array['offset'];
                        } else {
                            $offset = 'OFFSET 0';
                            }
                    } else {
                        $limit = '';
                        $offset = '';
                        }
                
                if (isset($query_array['group_by'])) {
                    $group_by = ' GROUP BY '.$query_array['group_by'];
                    } else {
                        $group_by = '';
                        }
                
                    $query = "SELECT $fields 
                                FROM $table 
                                $join_statement 
                                $where $group_by $order_statement $limit $offset";
                    
                    
                    // Scrub all of the table shortcut names and replacement them with their actual SQL table name
                    foreach ($table_replace_array as $scrub) {
                        
                        $query = str_replace($scrub['find'],$scrub['replace'],$query) ; 
                        
                        }
                
                    if ($query_array['skip_query'] != 1) {
                        // Execute the query and count results...
                        $results = $this->_connection->query($query);
                        $result_count = $results->num_rows ;        

                        $result_array = array() ; 
                        if ($result_count > 0) {
                            while($item = mysqli_fetch_assoc($results)) {   
                                $result_array[] = $item ; 
                                }
                            }

                        if (($result_count == 1) AND ('force' !== $force_result_array)) {
                            $result_array = $result_array[0];
                            }
                        }
                
                
                    $query_results = array(
                        'result_count' => $result_count,
                        'results' => $result_array,
                        'query' => $query
                        );
                
                    if ($this->_connection->error) {
                        $query_results['error'] = $this->_connection->error ; 
                        }
                
                    return $query_results ;
                break ;                
            case 'UPDATE':
                
                // Example UPDATE query_array
                // $query_array = array(
                //    'table' => "",
                //    'values' => array(
                //      'Field_1' => "Value 1",
                //      'Field_2' => "Value 2"
                //      ),
                //    'where' => "email_address=''"
                //    );
                
                
                if (isset($query_array['table'])) {
                    
                    $table = $this->Get_Table($query_array['table']);
                    
                    } else {
                        $RUN_QUERY = 0 ;
                        }
                
                
                // JOIN TABLES and MATCH VALUES
                // Start with a blank JOIN statement, and loop through each Join Table Array
                $join_statement = '' ; 
                $table_replace_array = array();
                if (isset($query_array['join_tables'])) {
                    
                    foreach ($query_array['join_tables'] as $this_join) {
                        
                        $join_table = $this_join['table']; // This is a shortname value
                        $join_statement_value = $this_join['statement']; // A conditional statment that overrides the on / match values if present
                        $join_on_value = $this_join['on']; // the table.key to join on 
                        $join_match_value = $this_join['match']; // and the table.key to match the join on against
                        $join_type = $this_join['type'] ; // Define as left, right, etc
                        switch ($this_join['type']) {
                            case 'left':
                                $join_type = "LEFT JOIN" ;
                                break ;
                            default:
                                $join_type = "JOIN" ;
                            }
                        
                        
                        $table_sql_name = $this->Get_Table($join_table); // Get the actual SQL name of the table. This will be used immediately in the JOIN statement
                        $table_replace_array[] = array('find' => $join_table.'.', 'replace' => $table_sql_name.'.'); // Create a find / replace array that will be looped through later on in the method to scrub out table shortcut names. Trailing period necessary.
                        
                        // Prep the JOIN statement for this table.
                        if (isset($this_join['statement'])) {
                            $join_statement .= "$join_type $table_sql_name ON $join_statement_value " ; 
                            } else {
                                $join_statement .= "$join_type $table_sql_name ON $join_on_value = $join_match_value " ;     
                                }
                        
                        
                        }
                    } 
                
                
                if (isset($query_array['values']) AND count($query_array['values'] > 0)) {
                    $field_values = '';
                    foreach ($query_array['values'] as $key=>$value) {
                        if (is_array($value)) {
                            $statement = $value['statement'] ;
                            $value = $value['value'] ;
                            }
                        $value = $this->_connection->real_escape_string($value) ; 
                        if ($statement) {
                            $statement = str_replace("VALUE",$value,$statement) ;
                            $field_values .= "$key=$statement, ";
                            } else {
                                $field_values .= "$key='$value', ";
                                }
                        unset($statement) ; 
                        }
                    $field_values = rtrim($field_values,", ");
                    if ($field_values == '') {
                        $RUN_QUERY = 0 ;
                        }
                    } else {
                        $RUN_QUERY = 0 ;
                        $field_values = '';
                        }
                
                if (isset($query_array['where'])) {
                    $where = 'WHERE '.$query_array['where'];
                    } else {
                        $RUN_QUERY = 0 ;
                        }               

                if ($RUN_QUERY == 1) {
                    $query = "UPDATE $table $join_statement SET $field_values $where";
                    $results = $this->_connection->query($query);
                    $result_count = $this->_connection->affected_rows;
                    }
                
                    $query_results = array(
                        'result_count' => $result_count,
                        'results' => $results,
                        'query' => $query
                        ) ;

                    if ($query_results['results'] != 'true') {
                        $query_results['error'] = $this->_connection->error ; 
                        }
                
                    return $query_results ;
                break ;
            case 'INSERT':
                
                // Example INSERT query_array
                // $query_array = array(
                //    'table' => "",
                //    'values' => array(
                //      'field_1' => "Value 1",
                //      'field_2' => "Value 2"
                //      );
                
                
                if (isset($query_array['table'])) {
                    
                    $table = $this->Get_Table($query_array['table']);
                    
                    } else {
                        $RUN_QUERY = 0 ;
                        }
                
                if (isset($query_array['values'])) {
                    $fields = '';
                    $values = '';
                    foreach ($query_array['values'] as $key=>$value) {
                        if (is_array($value)) {
                            $value = $value['value'] ;
                            }

                        $fields .= "$key, ";
                        $value = $this->_connection->real_escape_string($value) ; 
                        $values .= "'$value', ";
                        }
                    $fields = rtrim($fields,", ");
                    $values = '('.rtrim($values,", ").')';
                    
                    } else if (isset($query_array['values_set'])) {
                    
                        $fields = '';
                        $values = '';
                        $i = 0 ; 
                        foreach ($query_array['values_set'] as $set) {
                            
                           
                            $set_values = '' ; 
                            foreach ($set as $key => $value) {
                                
                                if ($i == 0) {
                                    $fields .= "$key, ";
                                    }

                                if (is_array($value)) {
                                    $value = $value['value'] ;
                                    }

                                $value = $this->_connection->real_escape_string($value) ;                                
                                $set_values .= "'$value', ";
                                
                                   
                                }
                            
                            $set_values = '('.rtrim($set_values,", ").')' ;
                            $values .= "$set_values, ";
                            $i++ ;  
                            }
                    
                        $values = rtrim($values,", ");
                        $fields = rtrim($fields,", ");
                                             

                        } else {
                        $fields = '';
                        $values = '';
                        }              

                    $query = "INSERT INTO $table ($fields) VALUES $values";
                    
                    $results = $this->_connection->query($query);
                    $insert_id = $this->_connection->insert_id ; // Get the new Insert ID    
                
                    $query_results = array(
                        'results' => $results,
                        'insert_id' => $insert_id,
                        'query' => $query
                        ) ;

                    if ($query_results['results'] != 'true') {
                        $query_results['error'] = $this->_connection->error ; 
                        }
                
                    return $query_results ;
                break ; 
            case 'BULK_INSERT':
                
                // Example BULK_INSERT query_array
                // $query_array = array(
                //    'table' => "",
                //    'values' => array(
                //      'field_1' => "Value 1",
                //      'field_2' => "Value 2"
                //      );
                
                
                if (isset($query_array['table'])) {
                    
                    $table = $this->Get_Table($query_array['table']);
                    
                    } else {
                        $RUN_QUERY = 0 ;
                        }
                
                if (isset($query_array['values'])) {
                    $fields = '';
                    $values = '';
                    $value_sets = array() ; 
                    foreach ($query_array['values'] as $key => $value) {
                        $i = 0 ; 
                        if (is_array($value)) {
                            foreach ($value as $sub_key => $item) {
                                $item = $this->_connection->real_escape_string($item) ; 
                                $value_sets[$i] .= "'$item', ";
                                $i++ ; 
                                }
                            }
                       
                        
                        
                        $fields .= "$key, ";
                        $i = 0 ;
                        $values_string = 'VALUES ' ; 
                        foreach ($value_sets as $values) {
                            $values = rtrim($values,", ");
                            $values_string .= "($values), " ; 
                            $i++ ; 
                            }
                        }
                    $fields = rtrim($fields,", ");
                    $values_string = rtrim($values_string,", ");
                    } else {
                        $fields = '';
                        $values = '';
                        }              

                    $query = "INSERT INTO $table ($fields) $values_string";
                    
                    $results = $this->_connection->query($query);
                    $insert_id = $this->_connection->insert_id ; // Get the new Insert ID    
                
                    $query_results = array(
                        'results' => $results,
                        'insert_id' => $insert_id,
                        'query' => $query
                        ) ;

                    return $query_results ;
                break ;                 
            case 'UPDATE_ELSE_INSERT':
                
                // Example UPDATE_ELSE_INSERT query_array
                // $query_array = array(
                //    'table' => "",
                //    'values' => array(
                //      'field_1' => "Value 1",
                //      'field_2' => "Value 2"
                //      ),
                //    'where' => "email_address=''"
                //    );
                
                
                $check_result = $this->Query('SELECT',$query_array);
                if ($check_result['result_count'] > 0) {
                    $query_results = $this->Query('UPDATE',$query_array);
                    $query_results['original_results'] = $check_result['results'] ; 
                    } else {
                        $query_results = $this->Query('INSERT',$query_array);
                        }
                
                    return $query_results ;
                break ;
                
            case 'SELECT_ELSE_INSERT':
                
                // Example SELECT_ELSE_INSERT query_array
                // $query_array = array(
                //    'table' => "",
                //    'values' => array(
                //      'field_1' => "Value 1",
                //      'field_2' => "Value 2"
                //      ),
                //    'where' => "email_address=''"
                //    );
                
                
                $check_result = $this->Query('SELECT',$query_array);
                if ($check_result['result_count'] > 0) {
                    $query_results = $check_result ;
                    } else {
                        $query_results = $this->Query('INSERT',$query_array) ;
                        }
                
                    return $query_results ;
                break ;                
                
            case 'DELETE':
                
                // Example DELETE query_array
                // $query_array = array(
                //    'table' => "",
                //    'where' => "email_address=''"
                //    );
                
                $result_count = 0 ; 
                
                if (isset($query_array['table'])) {
                    
                    $table = $this->Get_Table($query_array['table']);
                    
                    } else {
                        $RUN_QUERY = 0 ;
                        }
                
                if (isset($query_array['where'])) {
                    $where = 'WHERE '.$query_array['where'];
                    } else {
                        $RUN_QUERY = 0 ;
                        }                

                
                if ($RUN_QUERY == 1) {
                    $query = "DELETE FROM $table $where";
                    $results = $this->_connection->query($query);
                    $result_count = $this->_connection->affected_rows;
                    } 
                
                    
                    $query_results = array(
                        'results' => $result_count,
                        'query' => $query
                        ) ;

                    if ($query_results['results'] < 1) {
                        $query_results['error'] = $this->_connection->error ; 
                        }                
                
                    return $query_results ;
                break ; 
            case 'CREATE_TABLE':
                
                // Example CREATE_TABLE query_array
                // $query_array = array(
                //    'new_table_name' => "table_0101",
                //    'like_table_name' => "email_address=''"
                //    );
                
                $result_count = 0 ; 
                
                if (isset($query_array['new_table'])) {
                    $new_table_name = $query_array['new_table'] ; 
                    } else {
                        $RUN_QUERY = 0 ;
                        }
                
                if (isset($query_array['like_table'])) {
                    $like_table_name = $query_array['like_table'] ; 
                    } else {
                        $RUN_QUERY = 0 ;
                        }                

                
                if ($RUN_QUERY == 1) {
                    $query = "CREATE TABLE $new_table_name LIKE $like_table_name";
                    $results = $this->_connection->query($query);
                    $result_count = $this->_connection->affected_rows;
                    
                    $query_update = "ALTER TABLE $new_table_name CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci" ;
                    $update_results = $this->_connection->query($query_update) ;
                    } 
                
                    
                    $query_results = array(
                        'results_count' => $results,
                        'query' => $query,
                        'new_table_name' => $new_table_name
                        ) ;

                    return $query_results ;
                break ;                
            case 'DROP_TABLE':
                
                // Example DROP_TABLE query_array
                // $query_array = array(
                //    'table' => "table_0101"
                //    );
                
                $result_count = 0 ; 
                
                if (isset($query_array['table'])) {
                    $table_name = $query_array['table'] ; 
                    } else {
                        $RUN_QUERY = 0 ;
                        }               

                
                if ($RUN_QUERY == 1) {
                    $query = "DROP TABLE $table_name";
                    $results = $this->_connection->query($query);
                    $result_count = $this->_connection->affected_rows;
                    } 
                
                    
                    $query_results = array(
                        'results' => $results,
                        'results_count' => $result_count,
                        'query' => $query
                        ) ;

                    return $query_results ;
                break ;                

            }
        } 

    
    public function Simple_Hash($variable_array) {
        $string_to_hash = '';

        if (is_array($variable_array)) {
            foreach ($variable_array as $variable) {
                $string_to_hash .= $variable;
                }  
            } else {
                $string_to_hash = $variable_array ; 
                }

        $Hash = md5(strtolower($string_to_hash));
        return $Hash;
        
        }

    
    public function Get_Table($short_name) {
        switch($short_name) {
            case 'Accounts':
                return 'Accounts';
                break;
            case 'Account_Users':
                return 'Account_Users';
                break;                    
            case 'Users':
                return 'Users';
                break;
            case 'User_Emails':
                return 'User_Emails';
                break;                    
            case 'Users_Secure':
                return 'Users_Secure';
                break;                    
            case 'Events':
                return 'Events';
                break;
            case 'Vendors':
                return 'Vendors';
                break; 
            case 'Vendor_Authorization':
                return 'Vendor_Authorization';
                break;
            case 'Vendor_Keys':                    
                return 'Vendor_Keys';
                break; 
            case 'Vendor_Key_Association':
                return 'Vendor_Key_Association';
                break; 
            case 'System_Keys':
                return 'System_Keys';
                break;
            default:
                return $short_name ; 
            }
        }    
}
