<?php

/*
 *---------------------------------------------------------------
 * CONFIGURATION CLASSES
 *---------------------------------------------------------------
 *
 */
    class System_Config {
        
        static public function Extract_Text_Block_System($haystack, $needle_start, $needle_end, $start_position = 0) {
 

            $unit = '' ;

            $pos = stripos($haystack, $needle_start, $start_position);

            if ($pos !== FALSE) { // Had to make this !== for the System_Config version instead of != like it is in Utilities.
                $str = substr($haystack, $pos);
                $str_two = substr($str, strlen($needle_start));
                $second_pos = stripos($str_two, $needle_end);
                $str_three = substr($str_two, 0, $second_pos);
                $unit = trim($str_three); // remove whitespaces
                }

            $result = array(
                'extraction' => $unit,
                'position' => $pos,
                'next_position' => $pos + 1
                ) ; 

            return $result;
            }
        
        // Converts a version path input in format /v_x_xx into a decimal id in format x.xx
        static public function Convert_Version_Path($version_url_path) {

            // $version_url_path input like /v_x_xx
            $version_process = str_replace("/","",$version_url_path) ; // Remove / 
            $version_process = str_replace("v_","",$version_process) ; // Remove v_

            $version_parts = explode('_',$version_process) ; 
            $version_decimal_id = rtrim($version_parts[0].'.'.$version_parts[1],'.') ;  

            return $version_decimal_id ; 
            }

        // Converts a decimal id in format x.xx into a version path in format /v_x_xx
        static public function Create_Version_Path($version_decimal_id = 'bypass') {

            // $version_decimal_id input as x.xx
            // $version_array = System_Config::Test_Version($version_decimal_id) ; 
            // $version_decimal_id = $version_array['results']['version'] ;
            if ('bypass' !== $version_decimal_id) {
                $version_test = System_Config::Test_Version($version_decimal_id) ;

                $version_parts = explode('.',$version_test['results']['version']) ; 
                $version_path = '/v_'.$version_parts[0].'_'.$version_parts[1] ;  
                
                $result = array(
                    'version' => $version_test['results'],
                    'version_path' => $version_path,
                    'version_name' => $version_parts[0].'_'.$version_parts[1]
                    ) ;                
                }
            
            $result['version_test_key'] = 'f0b40eaa13bce9785b0c018da6e34ff3' ; // This is a double hash of API_KEY_INTERNAL_APP_API_KEY
            
            return $result ;             
            }
        
        static public function Test_Version($version_decimal_id) {

            global $DB ; 

            $query_array = array(
                'table' => 'system_version_control',
                'fields' => "*",
                'where' => "system_version_control.version='$version_decimal_id'"
                );       

            $result = $DB->Query('SELECT',$query_array);
 
            // If input version cannot be found, then retrieve the most recent stable version
            if ($result['result_count'] == 0) {
                $query_array = array(
                    'table' => 'system_version_control',
                    'fields' => "*",
                    'where' => "system_version_control.stable='1'",
                    'order_by' => 'timestamp_version_created',
                    'order' => 'DESC',
                    'limit' => "1"
                    );       

                $result = $DB->Query('SELECT',$query_array);
                }
            
            // If input version is no longer supported, then return the next version greater that is supported
            if ($result['results']['supported'] == 0) {
                $queried_version = $result['results']['version'] ; 
                
                $query_array = array(
                    'table' => 'system_version_control',
                    'fields' => "*",
                    'where' => "system_version_control.supported='1' AND version>'$queried_version'",
                    'order_by' => 'timestamp_version_created',
                    'order' => 'ASC',
                    'limit' => "1"
                    );       

                $result = $DB->Query('SELECT',$query_array);
                }            
            
            return $result ; 
            } 
        
        
        static public function Config_Parse_URL($input_url) {

            // PARSE THE URL
            $pageurl_scheme = parse_url($input_url, PHP_URL_SCHEME) ; 
            
            $input_url = str_replace("https://","",$input_url) ; 
            $input_url = str_replace("http://","",$input_url) ; 

            $pageurl_full = 'https://'.$input_url ;     // http://www.fyzzbee.com/resources/subscribers?query=xxxx
            $pageurl_host = parse_url($pageurl_full, PHP_URL_HOST) ; // returns  www.fyzzbee.com
            $pageurl_url = strtolower(parse_url($pageurl_full, PHP_URL_PATH)) ;  // returns  /resources/subscribers
            $pageurl_nox = rtrim(strtolower(explode(".",parse_url($pageurl_full, PHP_URL_PATH))[0]),"/") ;

            $extension = pathinfo($pageurl_url, PATHINFO_EXTENSION); // get ext from path
            $file  = pathinfo($pageurl_url, PATHINFO_FILENAME);  // get name from path
            $file_name = $file.'.'.$extension ;



            $pageurl_segments_array = array() ; 
            if ($pageurl_nox) {
                $pageurl_segments = explode("/",$pageurl_nox) ; 

                foreach ($pageurl_segments as $pageurl_segment_single) {
                    if ($pageurl_segment_single != '') {
                        $pageurl_segments_array[] = $pageurl_segment_single ; 
                        }
                    }
                }
            $pageurl_query = parse_url($pageurl_full, PHP_URL_QUERY);					// returns  query=xxxx

            $page_query_array = array() ; 
            if ($pageurl_query) {

                $page_queries = explode("&",$pageurl_query) ;

                foreach ($page_queries as $page_query_single) {
                    $page_query_single_parts = explode("=",$page_query_single) ;
                    $this_query_id = $page_query_single_parts[0] ;
                    $this_query_string = $page_query_single_parts[1] ;                
                    $page_query_array[$this_query_id] = $this_query_string ;
                    }
                
                $pageurl_full_query = $pageurl_nox.'?'.$pageurl_query ; 
                } else {
                    $pageurl_full_query = $pageurl_nox ; 
                    }

            
            
            $pageurl_hash = parse_url($input_url, PHP_URL_FRAGMENT);
            
            
            end(explode(".", parse_url($url, PHP_URL_HOST))); 
            
            $pageurl_array = array(
                'scheme' => $pageurl_scheme,
                'host' => $pageurl_host,
                'tld' => end(explode(".",$pageurl_host)),        
                'url_full' => $pageurl_full,
                'url_full_query' => $pageurl_full_query,
                'url' => $pageurl_url,
                'url_nox' => $pageurl_nox,
                'file_name' => $file_name,
                'segments' => $pageurl_segments_array,
                'query' => $pageurl_query,
                'queries' => $page_query_array,
                'hash' => $pageurl_hash
                );

            return $pageurl_array ;
            }
        
        
        // Auto-versioning CSS and JS files, sourced from:
        // https://stackoverflow.com/questions/118884/how-to-force-the-browser-to-reload-cached-css-js-files
        static public function File_Auto_Version($file) {
            if(strpos($file, '/') !== 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file))
            return $file;

            $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] . $file);
            return preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
            }        
        }