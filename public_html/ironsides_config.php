<?php

/**
 * 
 * IRONSIDES
 * 
 * The Ragnar Group
 *
 * SAAS development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2018, Matthew C. Burns
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	Ironsides
 * @author	Matthew C. Burns, The Ragnar Group
 * @copyright	Copyright (c) 2018, Matthew C. Burns
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://burnshead.com
 * @since	Version 1.0.0
 * @filesource
 */

/*
 *---------------------------------------------------------------
 * SESSION
 *---------------------------------------------------------------
 *
 */
    $data['microtime'] = array() ; 
    $data['microtime']['index_start'] = microtime(true) ;


    // Cookie lifetime - seconds, minutes, hours, days
	ini_set('session.cookie_lifetime', 60 * 60 * 24 * 30); 
    ini_set('session.gc_maxlifetime', 60 * 60 * 24 * 30); 

	// Start the session
	session_start() ;



/*
 *---------------------------------------------------------------
 * CROSS BROWSER ORIGIN SETTINGS
 *---------------------------------------------------------------
 *
 */

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Max-Age: 1000");
    header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding, dataType");
    header("Access-Control-Allow-Methods: PUT, POST, GET, OPTIONS, DELETE");





/*
 *---------------------------------------------------------------
 * DEFINE GLOBAL CONSTANTS
 *---------------------------------------------------------------
 *
 */

    ini_set('default_charset', 'UTF-8') ;

    $TIMESTAMP = time() ;
    $MICRO_TIMESTAMP = microtime(true) ;  // Initialize second counter

    define('GLOBAL_NO_NETWORK_CONNECTION', 0); // Set to 1 if you're off internet and need to run the app  

    define('TIMESTAMP', $TIMESTAMP);   
    define('MICRO_TIMESTAMP', $MICRO_TIMESTAMP);   
    define('SYSTEM_TIMEZONE', 'America/Chicago');   
    define('APP_VERSION', $version);
    $server_config['app_version'] = $version ; 
    // Client Side Unique Class IDs
    $ucid_i = 1 ; // Identification number that can be incremented within a single PHP instances
    $ucid = MICRO_TIMESTAMP.'_' ;   // Concatentated Unique Class ID number that forces uniqueness between PHP instances leveraging the microtimestamp
    // Within view, concatenate these two variables, and then increment $ucid_i to create truly unique IDs
    // EXAMPLE: $ucid.$ucid_i 
    // Should be added as a class as: 'uniqueclass_'.$ucid




/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */

    switch ($_SESSION['version']) {
        case 0.00:
            define('BILLING_MODE','system_test') ;
            define('ENVIRONMENT', 'development');
            ini_set('display_errors', 1);            
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
            break;
        case 0.50:
            define('BILLING_MODE',$temporary_billing_mode) ;
            define('ENVIRONMENT', 'testing');
            ini_set('display_errors', 1);            
            error_reporting(E_ERROR | E_WARNING | E_PARSE);
            break;
        default:
            define('BILLING_MODE',$temporary_billing_mode) ;
            define('ENVIRONMENT', 'production');            
            ini_set('display_errors', 0);
            error_reporting(0);
            break;           
        }


/*
 *---------------------------------------------------------------
 * SET APPLICATION DIRECTORIES
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * directory than the default one you can set its name here. The directory
 * can also be renamed or relocated anywhere on your server. If you do,
 * use an absolute (full) server path.
 * For more info please see the user guide:
 *
 * https://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 */



    $public_home_host = $_SERVER["HTTP_HOST"] ; // domain.com when on live server

    switch ($config_url_parsed['tld']) {
        case 'test':        
        case 'dev': 
        case 'local': 
            $public_home_path = $public_home_host ; // Skip https:// for test
            $system_home_path = __DIR__.'/..' ;  // local dev root folder
            break ;
        default:
            $public_home_path = 'https://'.$public_home_host ; // include https for live  
            $system_home_path = __DIR__.'/..' ;  // live root folder. was originally '/home2/zw4jcc9l'
        } 


	// The path to the "app/config" directory
    // The config folder falls outside of versioning. It never changes so we don't have multiple versions of how we're accessing the database, etc out there.
    $config_folder = $system_home_path.'/app/config'; 

	$system_core_path = $system_home_path.'/app'.$version_path ;
	$models_folder = $system_core_path.'/models';
	$controllers_folder = $system_core_path.'/controllers';
    $views_folder = $system_core_path.'/views' ;


	

/*
 *---------------------------------------------------------------
 * PUBLIC DIRECTORIES
 *---------------------------------------------------------------
 *
 *
 * NO TRAILING SLASH!
 */


    $public_templates_folder = $public_home_path.'/admin'.$version_path.'/templates' ;
    $rel_templates_folder = '/admin'.$version_path.'/templates' ;
    $absolute_templates_folder = $public_home_path.'/admin'.$version_path.'/templates' ;


    $rel_css_folder = '/admin'.$version_path.'/css' ;
    $absolute_css_folder = $public_home_path.'/admin'.$version_path.'/css' ;

	$rel_images_folder = '/admin/images' ;
    $absolute_images_folder = '/admin/images' ;

    $rel_fonts_folder = '/admin'.$version_path.'/fonts' ;
    $absolute_fonts_folder = '/admin'.$version_path.'/fonts' ;
        
    $rel_js_folder = '/admin'.$version_path.'/js' ;
    $absolute_js_folder = $public_home_path.$rel_js_folder ;
    $system_js_folder = $system_home_path.'/public_html/admin'.$version_path.'/js';

    $rel_js_manual_folder = '/admin'.$version_path.'/js_manual' ;
    $absolute_js_manual_folder = $public_home_path.$rel_js_manual_folder ;
    $system_js_manual_folder = $system_home_path.'/public_html/admin'.$version_path.'/js_manual';


/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

    // Public website
    define('PUBLIC_HOME_PATH', $public_home_path); 

	// Path to the system directory
	define('HOMEPATH', $system_home_path);    
	define('BASEPATH', $system_core_path);

	// Path to the front controller (this file) directory
	define('FCPATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

	// Name of the "system" directory
	define('SYSDIR', basename(BASEPATH));   

    define('CONFIG_PATH', $config_folder); 

    // The path to the "app/v_#_##/xxx" directories
    define('MODELS_PATH', $models_folder);
    define('VIEWS_PATH', $views_folder);
    define('CONTROLLERS_PATH', $controllers_folder);

    // Path to publiclly accessible folders
	define('RELATIVE_CSS_PATH', $rel_css_folder); // CSS path relative to site
	define('ABSOLUTE_CSS_PATH', $absolute_css_folder); // CSS with leading https://

	define('RELATIVE_IMAGES_PATH', $rel_images_folder); // Fonts path relative to site
	define('ABSOLUTE_IMAGES_PATH', $absolute_images_folder); // Fonts with leading https://

	define('RELATIVE_FONTS_PATH', $rel_fonts_folder); // Fonts path relative to site
	define('ABSOLUTE_FONTS_PATH', $absolute_fonts_folder); // Fonts with leading https://

	define('RELATIVE_JS_PATH', $rel_js_folder); // JS path relative to site
	define('RELATIVE_JS_MANUAL_PATH', $rel_js_manual_folder); // JS manual path relative to site; probably not needed long term

	define('ABSOLUTE_JS_PATH', $absolute_js_folder); // JS path with leading https://
	define('ABSOLUTE_JS_MANUAL_PATH', $absolute_js_manual_folder); // JS manual path with leading https://

	define('SYSTEM_JS_PATH', $system_js_folder); // JS path from /home/cpanel_username/
	define('SYSTEM_JS_MANUAL_PATH', $system_js_manual_folder); // JS manual path from /home/cpanel_username/

    






        // Autoload public javascript files
//        $app_js_file_array = scandir(SYSTEM_JS_MANUAL_PATH);
//        
//        $data['app_js_files'] = array(); 
//        foreach ($app_js_file_array as $file) {
//            $split_file = explode(".",$file) ; 
//            if ($split_file[1] == 'js') {
//                $data['app_js_files'][] = array(
//                    'full_path' => $file,
//                    'filename' => str_replace(SYSTEM_JS_MANUAL_PATH,"",$file)
//                    );
//                }
//            }





        // Administrator Account Profiles (Profile ID Numbers)
//        define('APP_ACCOUNT_ADMIN', Ragnar\Ironsides\Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'app_account_admin')['value']);
//        define('APP_ACCOUNT_SUPPORT', Ragnar\Ironsides\Utilities::Deep_Array($server_config['system_defaults'], 'defaults_name', 'app_account_support')['value']);            
        
        

        // Get Cloudinary URL constants        
//        $cloudinary_images = new Asset() ; 
//        $cloudinary_images->Set_System_Vendor(5) ; 
//        define('CLOUDINARY_SECURE_DELIVERY_URL', $cloudinary_images->vendor_keys->Cloudinary_Secure_Delivery_URL); 
//        define('CLOUDINARY_DELIVERY_FOLDER', $cloudinary_images->vendor_keys->Cloudinary_Delivery_Folder); 
//
//        $system = new System() ; 
//        $server_config['timestamp_system'] = TIMESTAMP ; 
//        $server_config = $system->Action_Time_Territorialize_Dataset($server_config) ;
//        $data['timestamp_system_compiled'] = $server_config['timestamp_system_compiled'] ; 
//        
//        // Log the URL & IP address request pair
//        
//        
////        echo '<pre>' ; print_r($server_config) ; echo '</pre>' ;
//        $data['server_config'] = $server_config ; 
//        // LOAD CONTROLLERS        
//        require_once CONTROLLERS_PATH.'/controller_master.php'; 